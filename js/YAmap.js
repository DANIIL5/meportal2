//  Yandex map - worker
jQuery(document).ready(function(){
    ymaps.ready(mapY.eInit);
})

var mapY  = {
    jDefaulMapOption : {
        center: [55.751574, 37.573856],
        zoom: 9,
        controls: ['zoomControl']
    },
    jMap: [],
    jClaster: [],
    nType: 0,
    sQuery:"",
    isReady: true,
    isMap: false,
    aData:[],
    eInit: function () {
    // body...
    // console.log('.eInit');

    if(jQuery("#map").length > 0 && !jQuery("#map").hasClass('map_FH')){
        if(!mapY.isMap){
            mapY.eCreateMap('map');

            $(document).off('submit.mapSearch'); // вешаем реакцию на "найти"
            $(document).on('submit.mapSearch', '.search_form form', function(e){
                e.preventDefault();
                var curVal = $(e.currentTarget).find('.for_input input').val();
                if(curVal !== mapY.sQuery){
                    mapY.sQuery = curVal;
                    mapY.eGet(mapY.nType, mapY.sQuery);
                }
            })
        }
    } else {
        if(jQuery("#map").length > 0 && jQuery("#map").hasClass('map_FH')){
            mapY_FH.eInit();
        }
    }
},
eCreateMap: function(idM){
    var curIDmap =
        mapY.jMap = new ymaps.Map(idM,
            mapY.jDefaulMapOption
        );
    mapY.jMap.behaviors.disable('scrollZoom');

    jQuery(document).on('click', '.map_types', function(e){
        e.preventDefault();

        var curAttr = parseInt(jQuery(e.currentTarget).attr('attr-id'));

        if(curAttr >= 0 && mapY.nType !== curAttr){
            mapY.nType = curAttr;
            mapY.eGet(mapY.nType);
        }

    });

    if(jQuery("#map").find('img').length > 0){
        jQuery("#map").find('img').hide();
    }

    mapY.isMap = true;
    mapY.eGet(mapY.nType);
},
eGet: function(type_id, sQuery){
    mapY.isReady = false;

    var curParam = {};

    if(type_id >= 0 || sQuery){
        if(type_id >= 0 && sQuery){
            curParam = {
                'id': type_id,
                'query': sQuery
            }
        }
        if(type_id>=0 && !sQuery){
            curParam['id']= type_id;
        }
        if(type_id<0 && sQuery){
            curParam['query']= sQuery;
        }
    } else {
        curParam={}
    }

    console.log('curParam = ',curParam);

    jQuery.post( "/ajax/on_map", curParam, function(json){
        mapY.aData = json;
        if(mapY.aData.length > 0){
            mapY.eCreatPM();
        }
    }, "json").fail( function(){
            mapY.aData = [];
            console.log('error JSON');
        }
    );

    /*mapY.aData = [];
     mapY.aData = [
     {"NAME":null,
     "LOGO":"\/images\/b735eec5d832476309a47e6fc1764bd2.jpg",
     "PHONE1":"+7 (134) 123 41 23","PHONE2":"+7 (134) 214 12 34",
     "PHONE3":"+7 (123) 412 34 12",
     "WEBSITE1":"12341234123412",
     "WEBSITE2":"\u0444\u044b\u0443\u0430\u0444\u044b\u0432\u0430\u043f\u0432\u044b\u0430\u043f",
     "WEBSITE3":"\u0432\u0430\u043f\u044b\u0432\u0430\u043f",
     "LATITUDE":"52.738699",
     "LONGITUDE":"37.518352",
     "ADDRESS_REGION":null,
     "ADDRESS_CITY":null,
     "ADDRESS_STREET":null,
     "ADDRESS_HOUSE":null,
     "ADDRESS_BUILDING":null,
     "ADDRESS_OFFICE":null,
     "CLASS":"b-i_skras"
     },
     {
     "NAME":"q2",
     "LOGO":null,
     "PHONE1":"+7 (123) 123 13 12",
     "PHONE2":"+7 (123) 123 12 31",
     "PHONE3":"+7 (123) 123 13 12",
     "WEBSITE1":"cffda","WEBSITE2":"cfec",
     "WEBSITE3":"sdczs",
     "LATITUDE":"54.690323",
     "LONGITUDE":"37.486613",
     "ADDRESS_REGION":"fgdxzfgxd",
     "ADDRESS_CITY":null,
     "ADDRESS_STREET":null,
     "ADDRESS_HOUSE":null,
     "ADDRESS_BUILDING":null,
     "ADDRESS_OFFICE":null,
     "CLASS":"b-i_tren"
     },
     {
     "NAME":"q3",
     "LOGO":null,
     "PHONE1":"+7 (123) 123 13 12",
     "PHONE2":"+7 (123) 123 12 31",
     "PHONE3":"+7 (123) 123 13 12",
     "WEBSITE1":"cffda",
     "WEBSITE2":"cfec",
     "WEBSITE3":"sdczs",
     "LATITUDE":"55.690323",
     "LONGITUDE":"33.486613",
     "ADDRESS_REGION":"fgdxzfgxd",
     "ADDRESS_CITY":"dxfgfxd",
     "ADDRESS_STREET":"dfgfdxg",
     "ADDRESS_HOUSE":"gdx",
     "ADDRESS_BUILDING":null,
     "ADDRESS_OFFICE":"sdf",
     "TIME":"9.00 - 18.00 сб-вс",
     "CLASS":"b-i_tzdorov"
     },
     {"NAME":"q4",
     "LOGO":null,
     "PHONE1":"+7 (123) 123 13 12",
     "PHONE2":"+7 (123) 123 12 31",
     "PHONE3":"+7 (123) 123 13 12",
     "WEBSITE1":"cffda",
     "WEBSITE2":"cfec",
     "WEBSITE3":"sdczs",
     "LATITUDE":"58.690323",
     "LONGITUDE":"37.486613",
     "ADDRESS_REGION":"fgdxzfgxd",
     "ADDRESS_CITY":"dxfgfxd",
     "ADDRESS_STREET":"dfgfdxg",
     "ADDRESS_HOUSE":"gdx",
     "ADDRESS_BUILDING":null,
     "ADDRESS_OFFICE":"sdf",
     "TIME":"9.00 - 18.00 сб-вс",
     "CLASS":""
     },
     {"NAME":"q5",
     "LOGO":null,
     "PHONE1":"+7 (123) 123 13 12",
     "PHONE2":"+7 (123) 123 12 31",
     "PHONE3":"+7 (123) 123 13 12",
     "WEBSITE1":"site 1",
     "WEBSITE2":"site 1",
     "WEBSITE3":"site 1",
     "LATITUDE":"53.690323",
     "LONGITUDE":"37.486613",
     "ADDRESS_REGION":"fgdxzfgxd",
     "ADDRESS_CITY":"dxfgfxd",
     "ADDRESS_STREET":"dfgfdxg",
     "ADDRESS_HOUSE":"gdx",
     "ADDRESS_BUILDING":null,
     "ADDRESS_OFFICE":"sdf",
     "METRO":"Спортивная",
     "TIME":"9.00 - 18.00 сб-вс",
     "CLASS":""
     }
     ]
     if(mapY.aData.length > 0){
     mapY.eCreatPM();
     }*/

},
eClearMap: function(){
    if(mapY.isMap && mapY.jMap.geoObjects.getLength() > 0){
        mapY.jMap.geoObjects.removeAll();
    }
},
eCreatPM : function(){
    var getPointData, points,geoObjects;
    var clasterIconTPl, balloonTPL, balloonTPL__inner;

    mapY.eClearMap();

    // Описываем макеты содержимого кластера
    clasterIconTPl = ymaps.templateLayoutFactory.createClass(
        '<div class="b-YA-cluster__content">$[properties.geoObjects.length]</div>'
    );

    balloonTPL = ymaps.templateLayoutFactory.createClass(
        '<div class="b-YA-mark js-b '+
        '{{ properties.type }}'+
        '"><div class="b-YA-mark_close js-b-x">X</div>'+
        '<div class="b-YA-mark__item">'+
        '$[[options.contentLayout]]'+
        '</div></div></div>'
        /**
         вешаем методы
         */
        ,{
            build: function () {
                this.constructor.superclass.build.call(this);
                this._$element = $('.b-YA-mark__item', this.getParentElement());
                this.applyElementOffset();
                this._$element.find('.js-b-x').on('click', $.proxy(this.onCloseClick, this));
            },
            /**
             * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
             * @see
             * @function
             * @name applyElementOffset
             */
            applyElementOffset: function () {
                this._$element = $('.b-YA-mark', this.getParentElement());
                this._$element.css({
                    left: -(this._$element[0].offsetWidth - 113),
                    top: -(this._$element[0].offsetHeight)
                });
            },
            clear: function () {
                this._$element.find('.js-b-x')
                    .off('click');

                this.constructor.superclass.clear.call(this);
            },
            onCloseClick: function (e) {
                e.preventDefault();
                this.events.fire('userclose');
            },
            _isElement: function (element) {
                return element && element[0];
            }
        }
    );

    wtf = function(array, type){
        var curStrFld = "";
        var curFld = 0;
        for(var i = 0, len = array.length; i < len; i++) {
            if(array[i]){
                curFld++;

                if(type == 'site'){
                    curStrFld+= '<a class="g-link b-baloon-array-i" href="'+array[i]+'">'+array[i]+'</a>'
                } else {
                    curStrFld+= '<span class="b-baloon-array-i">'+array[i] + '</span>';
                }
            }
        }

        (curFld > 0)? curStrFld : 0;
        return curStrFld;
    }

    createTPl = function(index){ // создаём макет внутренностей балуна для каждой отдельной метки
        var curPasteAdr = "";
        var curPasteTel = "";
        var curPasteWEB = "";
        var curPasteMetro = "";
        var curPasteTime = "";
        var c_aAdr = [ // адреса. которые надо проверять на наличие, если всех нет, то блок с адресами не выводится
            mapY.aData[index].ADDRESS_REGION,
            mapY.aData[index].ADDRESS_CITY,
            mapY.aData[index].ADDRESS_REGION,
            mapY.aData[index].ADDRESS_CITY,
            mapY.aData[index].ADDRESS_STREET,
            mapY.aData[index].ADDRESS_HOUSE,
            mapY.aData[index].ADDRESS_BUILDING,
            mapY.aData[index].ADDRESS_OFFICE
        ]
        var c_tel = [ // телефоны
        ]
        var c_metro = [ // метро
            mapY.aData[index].METRO
        ]
        var c_time = [ // рабочее время
            mapY.aData[index].TIME
        ]
        var c_web = [ // сайты
            mapY.aData[index].WEBSITE1,
            mapY.aData[index].WEBSITE2,
            mapY.aData[index].WEBSITE3
        ]

        if(wtf(c_aAdr)){
            curPasteAdr = '<span class="balloon-info__adr">'+wtf(c_aAdr)+' </span>';
        } else {
            curPasteAdr = "";
        }

        if(wtf(c_tel)){
            curPasteTel = '<span class="balloon-info__tel">'+wtf(c_tel)+' </span>';
        } else {
            curPasteTel = "";
        }

        if(wtf(c_metro)){
            curPasteMetro = '<span class="balloon-info__metro">'+wtf(c_metro)+' </span>';
        } else {
            curPasteMetro = "";
        }

        if(wtf(c_time)){
            curPasteTime = '<span class="balloon-info__time-t">'+wtf(c_time)+' </span>';
        } else {
            curPasteTime = '<span class="balloon-info__time-t">---</span>';
        }

        if(wtf(c_web, 'site')){
            curPasteWEB = '<span class="balloon-info__sites">'+wtf(c_web, 'site')+' </span>';
        } else {
            curPasteWEB = "";
        }

        balloonTPL__inner = ymaps.templateLayoutFactory.createClass(
            '<div class="b-YA-mark__title balloon-ttl">'+
            '<span class="balloon-ttl__ico"></span>'+
            '<span class="balloon-ttl__name">'+
            '<a class="g-link" href="#">{{ properties.name }}</a>'+
            '</span>'+
            '</div>'+
            '<div class="b-YA-mark__info balloon-info">'+
            curPasteAdr +
            curPasteWEB+
            curPasteTel+
            curPasteMetro +
            '<div class="balloon-info__time">'+
            '<span class="balloon-info__time-ttl">График работы:</span>'+
            curPasteTime+
            '</div>'+
            '</div>'
        );

        return balloonTPL__inner;
    }


    // описываем кластер
    mapY.jClaster = new ymaps.Clusterer({
        // hasBalloon: false,
        hasHint: false,
        clusterIcons: [{
            href: 'images/def-ico.png',
            size: [42, 44],
            offset: [-21, -22]
        }],
        clusterOpenBalloonOnClick: false,
        clusterNumbers: [100],
        clusterIconContentLayout: clasterIconTPl
    });

    getParam = function (index) {
        return {
            name: mapY.aData[index].NAME,
            type: mapY.aData[index].CLASS,
        }
    }

    var placeMarkTPL = ymaps.templateLayoutFactory.createClass(
        '<div class="b-YA-placeMArk {{ properties.type }}"></div>'
    );

    getPointData = function (index) {
        return {
            iconLayout : placeMarkTPL,
            iconShape: {
                type: 'Circle',
                // Круг описывается в виде центра и радиуса
                coordinates: [18, 18],
                radius: 18
            },
            balloonLayout: balloonTPL,
            balloonContentLayout: createTPl(index)
        };
    };

    points = [
        [55.831903, 37.411961], [52.763338,37.565466], [53.763338,37.565466], [54.744522,37.616378], [56.780898,37.642889]
    ];

    geoObjects = [];

    for(var i = 0, len = mapY.aData.length; i < len; i++) {
        var curPoint = [mapY.aData[i].LATITUDE,mapY.aData[i].LONGITUDE];

        // geoObjects[i] = new ymaps.Placemark(curPoint, getParam(i), getPointData(i));
        geoObjects[i] = new ymaps.Placemark(curPoint, getParam(i), getPointData(i));
    }

    mapY.jClaster.add(geoObjects);
    mapY.jMap.geoObjects.add(mapY.jClaster);

    mapY.jMap.setBounds(mapY.jClaster.getBounds(), {
        checkZoomRange: true
    });
    mapY.jMap.behaviors.disable('scrollZoom');

    mapY.jMap.geoObjects.events.add('click', function(e){mapY.jMap.panTo(e.get('coords'))});

    mapY.isReady = true
}
}

var mapY_FH  = { // для страницы "первая помощшь" - ИЗМЕНЕНЫ - макет метки и отключены балуны
    jDefaulMapOption : {
        center: [55.751574, 37.573856],
        zoom: 9,
        controls: ['zoomControl']
    },
    jMap: [],
    jClaster: [],
    nType: 0,
    isReady: true,
    isMap: false,
    aData:[],
    eInit: function () {
        // body...
        // console.log('.eInit');

        if(jQuery("#map").length > 0 && jQuery("#map").hasClass('map_FH')){
            if(!mapY_FH.isMap){
                mapY_FH.eCreateMap('map');
            }
        }
    },
    eCreateMap: function(idM){
        var curIDmap =
            mapY_FH.jMap = new ymaps.Map(idM,
                mapY_FH.jDefaulMapOption
            );
        mapY_FH.jMap.behaviors.disable('scrollZoom');

        jQuery(document).on('click', '.map_types', function(e){
            e.preventDefault();

            var curAttr = parseInt(jQuery(e.currentTarget).attr('attr-id'));

            if(curAttr >= 0 && mapY_FH.nType !== curAttr){
                mapY_FH.nType = curAttr;

                //  если не надо выделение в списке - то закоментить этот кусок
                jQuery(e.currentTarget).closest('.show_list').find('.selected').removeClass("selected");
                jQuery(e.currentTarget).closest('li').addClass("selected");
                // ------------------------------------------------------------

                mapY_FH.eGet(mapY_FH.nType);
            }

        });

        if(jQuery("#map").find('img').length > 0){
            jQuery("#map").find('img').hide();
        }

        mapY_FH.isMap = true;
        mapY_FH.eGet(mapY_FH.nType);
    },
    eGet: function(type_id){
        mapY_FH.isReady = false;

        jQuery.post( "/ajax/on_map", {type_id : type_id}, function(json){
            mapY_FH.aData = json;
            if(mapY_FH.aData.length > 0){
                mapY_FH.eCreatPM();
            }
        }, "json").fail( function(){
                mapY_FH.aData = [];
                console.log('error JSON');
            }
        );
    },
    eClearMap: function(){
        if(mapY_FH.isMap && mapY_FH.jMap.geoObjects.getLength() > 0){
            mapY_FH.jMap.geoObjects.removeAll();
        }
    },
    eCreatPM : function(){
        var getPointData, points,geoObjects;
        var clasterIconTPl;

        mapY_FH.eClearMap();

        // Описываем макеты содержимого кластера
        clasterIconTPl = ymaps.templateLayoutFactory.createClass(
            '<div class="b-YA-cluster__content">$[properties.geoObjects.length]</div>'
        );



        // описываем кластер
        mapY_FH.jClaster = new ymaps.Clusterer({
            // hasBalloon: false,
            hasHint: false,
            clusterIcons: [{
                href: 'images/def-ico.png',
                size: [42, 44],
                offset: [-21, -22]
            }],
            clusterOpenBalloonOnClick: false,
            clusterNumbers: [100],
            clusterIconContentLayout: clasterIconTPl
        });

        getParam = function (index) {
            return {
                name: mapY_FH.aData[index].NAME,
                type: mapY_FH.aData[index].CLASS,
                logo: mapY_FH.aData[index].LOGO,
                price: mapY_FH.aData[index].PRICE,
                distance: mapY_FH.aData[index].DISTANCE,
                items_h: mapY_FH.aData[index].ITEMS_H
            }
        }

        var placeMarkTPL = ymaps.templateLayoutFactory.createClass(
            '<div class="map__marker">'
            +'<img src=" {{ properties.logo }}" alt="">'
            +'<span class="price">{{ properties.price }}</span> Р <br>'
            +'<span>есть {{ properties.items_h }} шт</span>'
            + '<span class="car"><img src="images/map-marker-icon-1.png" alt="">~ {{ properties.distance }} мин</span>'
            +'</div>'
        );

        getPointData = function (index) {
            return {
                iconLayout : placeMarkTPL,
                iconShape: {
                    type: 'Circle',
                    // Круг описывается в виде центра и радиуса
                    coordinates: [18, 18],
                    radius: 18
                }
                ,
                hasBalloon : false
            };
        };

        points = [
            [55.831903, 37.411961], [52.763338,37.565466], [53.763338,37.565466], [54.744522,37.616378], [56.780898,37.642889]
        ];

        geoObjects = [];

        for(var i = 0, len = mapY_FH.aData.length; i < len; i++) {
            var curPoint = [mapY_FH.aData[i].LATITUDE,mapY_FH.aData[i].LONGITUDE];

            geoObjects[i] = new ymaps.Placemark(curPoint, getParam(i), getPointData(i));
        }

        mapY_FH.jClaster.add(geoObjects);
        mapY_FH.jMap.geoObjects.add(mapY_FH.jClaster);

        mapY_FH.jMap.setBounds(mapY_FH.jClaster.getBounds(), {
            checkZoomRange: true
        });
        mapY_FH.jMap.behaviors.disable('scrollZoom');

        mapY_FH.jMap.geoObjects.events.add('click', function(e){mapY_FH.jMap.panTo(e.get('coords'))});

        mapY_FH.isReady = true
    }
}

