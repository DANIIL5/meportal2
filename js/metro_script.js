$(function(){
	 
	/*$('.subwayPopUpMap_checkbox input').prop('checked',false);
	$('.subwayPopUpMap_checkbox i').removeClass('checked');*/
	$(document).on('click','#reset_all',function(e){
		alert("ok");
		$('.subwayPopUpMap_checkbox input').prop('checked',false);
		$('.subwayPopUpMap_checkbox .checked').removeClass('checked');
		$(this).removeClass('checked');
	});
	
	$('.subwayPopUpMap_checkbox input[type="checkbox"]').each(function(index, element) {
	  if ($(this).is(":checked")){
	   $(this).parents('.subwayPopUpMap_checkbox').find('i').addClass('checked');
	  }
	 });
	 $(document).on('change','.subwayPopUpMap_checkbox input[type="checkbox"]',function(e){
	  if ($(this).is(":checked")){
	   $(this).parents('.subwayPopUpMap_checkbox').find('i').addClass('checked');
	  }
	  else $(this).parents('.subwayPopUpMap_checkbox').find('i').removeClass('checked');
	 });

	 
	 /*check metro*/
	 $('.subwayPopUpMap_filter a, .subwayPopUpMap_filter_top a:not(a#selected)').click(function(e){
		e.preventDefault();
		var link = $(this).attr('id');
		if($(this).attr('class')=='checked'){
			$('label.'+link+' input').prop('checked',false);
			$('label.'+link+' i').removeClass('checked');
			$(this).removeClass('checked');	
		}else{
			$('label.'+link+' input').prop('checked',true);
			$('label.'+link+' i').addClass('checked');
			$(this).addClass('checked');
		}
	 });
	 
	 $('.subwayPopUpMap_filter_right a').click(function(e){
		e.preventDefault();
		var link = $(this).attr('id');
		if($(this).attr('class')=='checked'){
			$('label.'+link+' input').prop('checked',false);
			$('label.'+link+' i').removeClass('checked');
			$('.subwayPopUpMap_filter_map #'+link).removeClass('checked');	
			$('.subwayPopUpMap_filter_names #'+link).removeClass('checked');
		}else{
			$('label.'+link+' input').prop('checked',true);
			$('label.'+link+' i').addClass('checked');
			$('.subwayPopUpMap_filter_map #'+link).addClass('checked');
			$('.subwayPopUpMap_filter_names #'+link).addClass('checked');
		}
	 });
	 
	$(".subwayPopUpMap_metro_block").selectable({
		disabled: false,
		/*autoRefresh: false,*/
		distance: 30,
		filter:'label i',
		start:function(event,ui){
			$('.subwayPopUpMap_checkbox input').prop('checked',false);
			$('.subwayPopUpMap_checkbox i').removeClass('checked');
		},
		selected:function(event,ui){
			$(ui.selected).addClass('checked');
			$(ui.selected).prev('input').prop('checked',true);
		}
	});
});

function get_selected_metro()
{

	console.log($('#metro_map_form > div > label > i.checked').length);

	metro_name = '';

	$('#metro_map_form > div > label > i.checked').parent().children('input').each(function(i, elem) {
		console.log(elem);
		metro_name += "<li>" + elem.getAttribute('data-name') + "</li>\r\n";
	});

	$('#metro_list').html(metro_name);
}