var mfp = {
    test:function()
    {
        console.log('test');
    },
    /*передаём новый пароль*/
    changeProfilePassword:function(instance, e)
    {
        $.ajax({
            url      : '/profile/changePassword',
            type     : 'POST',
            dataType : 'json',
            data     : {'password':$(instance).parent().find('.box-fields__input').val()}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
                pUp_m.eGetId('.js-pUp__openeer',e.msg);
            else
                pUp_m.eGetId('.js-pUp__openeer',e.error);
        });
        return false;
    },/*передаём данные с полей формы*/
    ajaxLoader:function(instance)
    {
        var sendPath = $(instance).attr('action');
     
        $.ajax({
            url      : sendPath,
            type     : 'POST',
            dataType : 'json',
            data     : $(instance).serialize()
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                if(sendPath=='/profile/sendPost' || sendPath=='/profile/sendPostToAdmin'
                   || sendPath=='/profile/resendSpecialist')
                        $('.pUps.js-pUp_m').data('redirect','/profile/posts');
                if(sendPath=='/profile/updateTarget' || sendPath=='/profile/sendTarget')
                    $('.pUps.js-pUp_m').data('redirect','/profile/targets');
                if(sendPath=='/profile/addBranch' || sendPath=='/profile/updateBranch')
                    $('.pUps.js-pUp_m').data('redirect','/profile/branches');
                if(sendPath=='/profile/editBranch')
                    $('.pUps.js-pUp_m').data('redirect','/profile/branches');
                pUp_m.eGetId('.js-pUp__openeer',e.msg);
            }
            else
                pUp_m.eGetId('.js-pUp__openeer',e.error);
        });
        return false;
    },/*передаём данные с полей формы пересылаемого специалиста*/
    ajaxResendLoader:function(instance)
    {
        $(instance).find('.g-visNot.g-heightNot.js-msg-paste-Input__input').val($(instance).find('.box-fields__message-reg-i.js-msg-paste-Input__text').html());
        this.ajaxLoader(instance);
        return false;
    },
    /*передаём новую аватарку для загрузки во временную папку и на страницу*/
    changeAva:function(instance)
    {
        //alert(instance);
        var instanceParent=$(instance).parent();
        var imgSelector=instanceParent.find('.js-avatar__img img');
        imgSelector.attr('src', '/images/bg/preloader.gif');
        if(typeof instance.files != 'undefined')
        {
            /*объявляем новый объект данных*/
            var formData = new FormData();
            /*готовим передаваемые данные - вводим в них файл аватары*/
            formData.append('userfile', instance.files[0], instance.files[0].name);
            var xhr = new XMLHttpRequest();
            /*отправляем данные*/
            xhr.open('POST', '/index/addDocument/images/2048', true);
            xhr.onload = function(e)
            {
                var res = JSON.parse(e.currentTarget.response);
                if(typeof res.success != 'undefined')
                {
                    //alert(pageVariables.controllerMethod);
                    var imgPath='/images/'+res.msg.upload_data.file_name;
                    if(pageVariables.controllerMethod=='/profile/index'){
                       
                         var sendPath='/profile/changeAva';
                         // alert(sendPath);
                    }
                       
                    if(pageVariables.controllerMethod=='/profile/organisation')
                    {
                        //alert(pageVariables.controllerMethod);
                      var sendPath='/profile/changeLogo';
                      //alert(sendPath);  
                    }
                        
                    if(pageVariables.controllerMethod=='/profile/specialist'){
                       // alert(pageVariables.controllerMethod);
                       var sendPath='/profile/changeSpecialistPhoto';
                      // alert(sendPath); 
                    }
                        
                    $.ajax({
                        url      : sendPath,
                        type     : 'POST',
                        dataType : 'json',
                        data     : {'imagePath':imgPath}
                    }).done(function(e){/*получаем ответ контроллера*/
                        if(typeof e.success != 'undefined')
                        {  
                            imgSelector.attr('src', imgPath);
                            instanceParent.find('._path-input').val(imgPath);
                            instanceParent.find('.b-add-f-list__i-delete').show();
                        }
                    });
                }
                else
                    pUp_m.eGetId('.js-pUp__openeer',res.error.error);
            }
            //alert('send');
            xhr.send(formData);
        }
        return false;
    },
    /*удаление аватарки*/
    avaDelete:function()
    {
        if(pageVariables.controllerMethod=='/profile/index')
            var sendPath='/profile/deleteAva';
        if(pageVariables.controllerMethod=='/profile/institution')
            var sendPath='/profile/deleteLogo';
        if(pageVariables.controllerMethod=='/profile/specialist')
            var sendPath='/profile/deleteSpecialistPhoto';
        var imgBox=$('.box_avar__image.js-avatar__img.js-avatar_init');
        var pathInput=imgBox.find('._path-input');
        $.ajax({
            url      : sendPath,
            dataType : 'json'
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                pathInput.val('');
                pathInput.prev('img').attr('src', '');
                imgBox.removeClass('notEmpty');
                imgBox.find('.b-add-f-list__i-delete').hide();
            }
        });
        return false;
    },
    /*добавляем документ на страницу и во временную папку*/
    addDocument:function(instance,idi)
    {
        if(pageVariables.controllerMethod=='/profile/institutionPhotos')
        {
            var sendPath='/profile/addInstitutionPhoto';
            var addPath='/index/addDocument/images/2048';
        }
        else
            var addPath='/index/addDocument/images/20480';
        if(pageVariables.controllerMethod=='/profile/institution')
            var sendPath='/profile/addInstitutionDocument';
        if(pageVariables.controllerMethod=='/profile/specialist')
            var sendPath='/profile/addSpecialistDocument';
        if(pageVariables.controllerMethod=='/profile/med_card')
            var sendPath='/profile/addMedCardDocument';
        var imgLiSelector=$(instance).parents().find('li[data-id_idi="'+idi+'"]');
        imgLiSelector.find('img').attr('src', '/images/bg/preloader.gif');
        if(typeof instance.files != 'undefined')
        {
            /*объявляем новый объект данных*/
            var formData = new FormData();
            /*готовим передаваемые данные - вводим в них файл*/
            formData.append('userfile', instance.files[0], instance.files[0].name);
            var xhr = new XMLHttpRequest();
            /*отправляем данные*/
            xhr.open('POST', addPath, true);
            xhr.onload = function(e)
            {
                var res = JSON.parse(e.currentTarget.response);
                if(typeof res.success != 'undefined')
                {
                    var imgPath='/images/'+res.msg.upload_data.file_name;
                    $.ajax({
                        url      : sendPath,
                        type     : 'POST',
                        dataType : 'json',
                        data     : {'imagePath':imgPath}
                    }).done(function(e){/*получаем ответ контроллера*/
                        if(typeof e.success != 'undefined')
                        {
                            imgLiSelector.find('img').attr('src', imgPath);
                            imgLiSelector.find('._hidden-id').val(imgPath);
                            imgLiSelector.attr('data-img-path',imgPath);
                        }
                    });
                }
                else
                    pUp_m.eGetId('.js-pUp__openeer',res.error.error);
            }
            xhr.send(formData);
        }
        return false;
    },

    deleteDocument:function(imgPath)
    {
        if(pageVariables.controllerMethod=='/profile/institution')
            var sendPath='/profile/deleteInstitutionDocument';
        if(pageVariables.controllerMethod=='/profile/institutionPhotos')
            var sendPath='/profile/deleteInstitutionPhoto';
        if(pageVariables.controllerMethod=='/profile/specialist')
            var sendPath='/profile/deleteSpecialistDocument';
        if(pageVariables.controllerMethod=='/profile/med_card')
            var sendPath='/profile/deleteMedCardDocument';
        $.ajax({
            url      : sendPath,
            type     : 'POST',
            dataType : 'json',
            data     : {'imagePath':imgPath}
        }).done(function(e){
            $('.b-add-f-list__i.js-add-li[data-img-path="'+imgPath+'"]').remove();
        });
        return false;
    },
    /*добавляем документ на страницу написания письма и во временную папку*/
    addPostDocument:function(instance,idi)
    {
        var imgLiSelector=$(instance).parents().find('[data-id_idi="'+idi+'"]');
        imgLiSelector.find('img').attr('src', '/images/bg/preloader.gif');
        if(typeof instance.files != 'undefined')
        {
            /*объявляем новый объект данных*/
            var formData = new FormData();
            /*готовим передаваемые данные - вводим в них файл*/
            formData.append('userfile', instance.files[0], instance.files[0].name);
            var xhr = new XMLHttpRequest();
            /*отправляем данные*/
            xhr.open('POST', '/index/addDocument/temp/20480', true);
            xhr.onload = function(e)
            {
                var res = JSON.parse(e.currentTarget.response);
                if(typeof res.success != 'undefined')
                {
                    imgLiSelector.find('img').attr('src', '/temp/'+res.msg.upload_data.file_name);
                    imgLiSelector.find('._hidden-id').val('/temp/'+res.msg.upload_data.file_name);
                }
                else
                    pUp_m.eGetId('.js-pUp__openeer',res.error.error);
            }
            xhr.send(formData);
        }
        return false;
    },
    /*добавление новых полей ввода повышения квалификации*/
    addRefresher:function(instance)
    {
        var refresherContent=$(instance).prev('.refresher').html();
        $(instance).before('<div class="refresher"></div>');
        $(instance).prev('.refresher').html(refresherContent);
        return false;
    },
    /*восстановление письма из корзины*/
    postRestore:function(instance,sendPath)
    {
        $.ajax({
            url      : sendPath,
            type     : 'POST',
            dataType : 'json',
            data     : {'target-id':$(instance).parent('.b-msg-operation').find('._hidden-id').val()}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
                $(instance).parents('.b-msg__row.js-mail').remove();
        });
        return false;
    },

    postTargetDelete:function(targetID,sendPath)
    {
        $.ajax({
            url      : sendPath,
            type     : 'POST',
            dataType : 'json',
            data     : {'target-id':targetID}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
                $('.b-msg__row.js-mail[data-target-id="'+targetID+'"]').remove();
        });
        return false;
    },/*добавление цели/письма в избранное*/
    targetPostLiked:function(instance,sendPath)
    {
        $.ajax({
            url      : sendPath,
            type     : 'POST',
            dataType : 'json',
            data     : {'target-id':$(instance).parent('.b-msg-operation').find('._hidden-id').val()}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                $(instance).prev('.b-msg-operation__i.b-msg-operation__i_del').show();
                $(instance).hide();
            }
        });
        return false;
    },/*удаление цели/письма из избранного*/
    targetPostLikedDelete:function(instance,sendPath,liked)
    {
        $.ajax({
            url      : sendPath,
            type     : 'POST',
            dataType : 'json',
            data     : {'target-id':$(instance).parent('.b-msg-operation').find('._hidden-id').val()}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                if(liked)
                    $(instance).parents('.b-msg__row.js-mail').remove();
                else
                {
                    $(instance).next('.b-msg-operation__i.b-msg-operation__i_rep').show();
                    $(instance).hide();
                }
            }
        });
        return false;
    },
    /*фильтр писем по категориям*/
    filterMail:function(instance)
    {
        if($(instance).find('option:selected').val()=='all')
            window.location.pathname='/profile/posts';
        if($(instance).find('option:selected').val()=='in')
            window.location.pathname='/profile/posts/in';
        if($(instance).find('option:selected').val()=='out')
            window.location.pathname='/profile/posts/out';
        if($(instance).find('option:selected').val()=='delete')
            window.location.pathname='/profile/posts/delete';
        if($(instance).find('option:selected').val()=='liked')
            window.location.pathname='/profile/posts/liked';
    },
    /*отметка письма как прочтённого*/
    postRead:function(instance)
    {
        if ($(instance).hasClass('b-msg__row_new-msg'))
        {
            $.ajax({
                url      : '/profile/readPost/',
                type     : 'POST',
                dataType : 'json',
                data     : {'target-id':$(instance).find('._hidden-id').val()}
            }).done(function(e){/*получаем ответ контроллера*/
                if(typeof e.success != 'undefined')
                {
                    $(instance).removeClass('b-msg__row_new-msg');
                    $('.mytime' + $(instance).find('._hidden-id').val()).css({'color':'#777778','font-weight':'normal'});
                    $('.mytitle' + $(instance).find('._hidden-id').val()).css({'color':'#777778','font-weight':'normal'});
                    $unread=parseInt($('.main-menu-new-item').html());
                    if($unread!=0){
                      $unread=$unread-1;
                      if($unread==0){
                          
                             $('.main-menu-new-item').remove();
                          
                      }  
                    }
                    
                       
                       
                        
                   
                    
                    $('.main-menu-new-item').html($unread);
                }
            });
        }
        return false;
    },
    /*поиск в адресной книге*/
    searchAddress:function(instance)
    {
        $.ajax({
            url      : '/profile/searchAddress/',
            type     : 'POST',
            dataType : 'json',
            data     : {'search':$(instance).val()}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
                $(instance).parent().parent().find('.b-RP-adress-list').html(e.addressBook);
        });
        return false;
    },
    /*удаление из избранного*/
    likedDelete:function(idLiked,sendPath)
    {
        $.ajax({
            url      : sendPath,
            type     : 'POST',
            dataType : 'json',
            data     : {'target-id':idLiked}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                if(sendPath=='/profile/deleteLikedInstitution')
                    $('.b-tab-content__i.institution[data-target-id="'+idLiked+'"]').remove();
                if(sendPath=='/profile/deleteLikedSpecialist' || sendPath=='/profile/institution_specialists_delete')
                    $('.b-tab-content__i.specialist[data-target-id="'+idLiked+'"]').remove();
                if(sendPath=='/profile/deleteLikedPrep')
                    $('.b-tab-content__i.prep[data-target-id="'+idLiked+'"]').remove();
                if(sendPath=='/profile/deleteLikedArticle')
                    $('.b-tab-content__i.article[data-target-id="'+idLiked+'"]').remove();
            }
        });
        return false;
    },
    /*удаление отделения*/
    branchDelete:function(idBranch)
    {
        $.ajax({
            url      : '/profile/deleteBranch',
            type     : 'POST',
            dataType : 'json',
            data     : {'target-id':idBranch}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
                $('.js-b-tgl-r__i.b-branches[data-target-id="'+idBranch+'"]').remove();
        });
        return false;
    },
    /*удаление соцсети, где авторизован пользователь*/
    userSocNetDelete:function(targetId)
    {
        $.ajax({
            url      : '/profile/userSocNetDelete',
            type     : 'POST',
            dataType : 'json',
            data     : {'target-id':targetId}
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                var liSocNet=$('.box_socio__popup-list-i').find('li[data-id="'+targetId+'"]');
                liSocNet.removeClass('disable');
                liSocNet.html(e.msg);
                $('.box_socio-list__i[data-target-id="'+targetId+'"]').remove();
            }
        });
        return false;
    },
    scheduleMonthHeight:function(dataMonth)
    {
        if(pageVariables.controllerMethod=='/profile/schedule_month')
        {
            var daySelectors=$('.b-calend__stage[data-monthi="'+dataMonth+'"]').find('.b-calend__days_i');
            daySelectors.height(127);
            daySelectors.each(function(i)
            {
                var maxLength=$(this).find('.b-calend__event').length;
                if(maxLength > 4)
                {
                    var curWeekDay=$(this).data('weeki');
                    var difI=6-curWeekDay;
                    var dayI=$(this).data('dayi');
                    for(forI=(-curWeekDay); forI<=difI; forI++)
                    {
                        var curLength=$('.b-calend__days_i[data-dayi="'+(dayI+forI)+'"]').find('.b-calend__event').length;
                        if(curLength>maxLength)
                            maxLength=curLength;
                    }
                    var curI=dayI-curWeekDay;
                    var maxI=dayI+difI;
                    for(curI;curI<=maxI;curI++)
                    {
                        $('.b-calend__days_i[data-dayi="'+curI+'"]').height(127+(maxLength-4)*30);
                    }
                }
            });
            var viewportHeight=70;
            $('.b-calend__stage[data-monthi="'+dataMonth+'"]').find('.b-calend__days_i[data-weeki="0"]').each(function(i)
            {
                viewportHeight=viewportHeight+$(this).height();
            });
            $('.bx-viewport').height(viewportHeight);
        }
        return false;
    },
    payFill:function(instance)
    {
        $.ajax({
            url      : '/profile/payFill',
            dataType : 'json'
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                var form=$(instance).parents('.b-print');
                form.find('._input_name_institution_pay').html(e.name);
                form.find('._input_inn_institution_pay').html(e.inn);
                form.find('._input_kpp_institution_pay').html(e.kpp);
                form.find('._input_address_institution_pay').html(e.address);
                form.find('._input_phone_institution_pay').val(e.phone);
                form.find('._input_fax_institution_pay').val(e.phone);
            }
        });
        return false;
    },
    payAutoFill:function(instance)
    {
        $.ajax({
            url      : '/profile/payAutoFill',
            dataType : 'json'
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                $(instance).find('._check_id_input').html(e.id);
                $(instance).find('._check_date_input').html(e.date);
            }
        });
        return false;
    },
    checkPayAsMSWordDocx:function(instance)
    {
        $('.b-DIV-input').each(function(i)
        {
            $(this).next('input').val($(this).html());
        });
        $('._pay_data_cell').each(function(i)
        {
            $(this).next('input').val($(this).html());
        });
        $.ajax({
            url      : $(instance).attr('action'),
            type     : 'POST',
            dataType : 'json',
            data     : $(instance).serialize()
        }).done(function(e){/*получаем ответ контроллера*/
            if(typeof e.success != 'undefined')
            {
                window.location.pathname='/pay-check.docx';
            }
        });
        return false;
    }
}