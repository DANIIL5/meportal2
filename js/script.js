function is_array( mixed_var ) {
    return ( mixed_var instanceof Array );
}

function empty( mixed_var ) {
    return ( mixed_var === "" || mixed_var === 0   || mixed_var === "0" || mixed_var === null  || mixed_var === false  ||  ( is_array(mixed_var) && mixed_var.length === 0 ) );
}

function reload_map( collection, id ) {
	$.post('/ajax/on_map', { "type_id" : id}, function(data) {

		collection.removeAll();

		for (var i in data) {
			var coords = [];
			coords[0] = data[i].LATITUDE;
			coords[1] = data[i].LONGITUDE;
			collection.add(new ymaps.Placemark(coords));
		}

		//for (var i = 0; i < coords.length; i++) {
		//	myCollection.add(new ymaps.Placemark(coords[i]));
		//}

		myMap.geoObjects.add(collection);
	},'json');

}
 $(window).on('load', function(){ // дожидаемся окончания загрузки всех картинок
// reviews - отзывы " показать целиком" 

$(document).on('click.metro', '#metro_wrapper2', eHidePup);
        
})

 function eHidePup(){ // скрываем ранее открытые попапы - модальных окон
      
        $('#metro_wrapper').toggle();
        $('#metro_wrapper2').toggle();
         isVisible = $( "#metro_wrapper" ).is( ":visible" );
        console.log(isVisible);
        if (isVisible) {
            $('body').css('overflow-y','hidden');
        } else {
            $('body').css('overflow-y','auto');
        }
      
    }
    
    
function show_on_map(id, latitude, longitude, image, name ) {
	ymaps.ready(function(){
		var mapCell = $('#id'+id);
		mapCell.toggle(200, function() {
			if (typeof MedicineMap == 'undefined') {
				MedicineMap = new ymaps.Map('id'+id, {
					center: [latitude, longitude],
					zoom: 15
				});

				MedicineMap.controls.remove('geolocationControl');
				MedicineMap.controls.remove('searchControl');
				//myMap.controls.remove('zoomControl');
				MedicineMap.controls.remove('fullscreenControl');
				MedicineMap.controls.remove('scaleLine');
				MedicineMap.controls.remove('typeSelector');
				MedicineMap.controls.remove('trafficControl');

				myPlacemark = new ymaps.Placemark([latitude, longitude], {
					hintContent: name,
					balloonContent: '<div class="map_baloon" style="width:220px">'+
					'<div class="left" style="float:left">'+
					'<img src="'+ image +'" alt="image" style="width:60px;" />' +
					'</div>'+
					'<div class="right" style="float:left">'+
					'<a href="/dugstores/item/1">'+ name +'</a>' +
					'</div>'+
					'</div>'
				});

				MedicineMap.geoObjects.add(myPlacemark, 0);
			}
		});
	});
}

$(function() {
	navigator.geolocation.getCurrentPosition(function(position) {
		ymaps.ready(function(){
			jQuery('#map').find('img').remove();

			myMap = new ymaps.Map("map", {
				center: [position.coords.latitude, position.coords.longitude],
				zoom: 15
			});

			myMap.controls.remove('geolocationControl');
			myMap.controls.remove('searchControl');
			//myMap.controls.remove('zoomControl');
			myMap.controls.remove('fullscreenControl');
			myMap.controls.remove('scaleLine');
			myMap.controls.remove('typeSelector');
			myMap.controls.remove('trafficControl');

			myCollection = new ymaps.GeoObjectCollection({}, {
				preset: 'twirl#redIcon', //все метки красные
				draggable: false // и их можно перемещать
			});

			$('.map_types').on('click', function() {
				var attr_id = $(this).attr('attr-id');
				reload_map(myCollection, attr_id);
			});

			//$.post('/ajax/on_map', { "type_id" : 0}, function(data) {
			//	console.log(data);
			//
			//	for (var i in data) {
			//		var coords = [];
			//		coords[0] = data[i].LATITUDE;
			//		coords[1] = data[i].LONGITUDE;
			//		myCollection.add(new ymaps.Placemark(coords));
			//		console.log(data[i]);
			//	}
            //
			//	//for (var i = 0; i < coords.length; i++) {
			//	//	myCollection.add(new ymaps.Placemark(coords[i]));
			//	//}
            //
			//	myMap.geoObjects.add(myCollection);
			//},'json');

			//myCollection.getMap().events.add('click', function() {
			//	myCollection.removeAll();
			//});
		});
	});

	//кастомные элементы форм
	$('select:not(".city select"), .file, .checkbox, .radio').styler({
		filePlaceholder:"",
		fileBrowse:"Вибрать файл"
	});

	//города в шапке
	$('.city select').select2();

	//показать все формы лекарств
	$('.other-link').click(function(){
		$(this).hide();
		$(this).next('.other').fadeIn();
		return false;
	});

	//рез-ты голосования
	$('.vote .results a').click(function(){
		$(this).parent().next('.results-block').fadeToggle();
		return false;
	});

	//форма обратной связи
	$('.ask-question .chat_top .ava .image').click(function(){
	   var element = $(this);
	   $('.ask-question .chat_top .ava .image').removeClass('active');
	   if (!element.hasClass('active')) {
			element.addClass('active');
		}
	});

	//поиск по аптекам
	// $('.results-table tr > td:not(".map-line")').click(function(){
	// 	var mapCell = $(this).parent().next().find('.map-cell');
	// 	$('.results-table .map-cell').slideUp(500);
	// 	if (!mapCell.is(':visible')) {
	// 		mapCell.slideDown(500);
	// 	}
	// });

	// //блок совет и записаться к доктору
	// $('.advice .link, .order2doctor .btn').click(function(){
	// 	$(this).next('.popup').fadeToggle(500);
	// 	return false;
	// });

	/*$('.advice .popup, .order2doctor .popup').mouseleave(function() {
	  $(this).fadeOut(500);
	});*/

	//блоки подбора лучшей аптеки
	$('.pharmacy-card .list-all').click(function(){
		$('.pharmacy-table').slideToggle(500);
		return false;
	});

	//блоки поиска аптеки и учреждения
	$('.search-result-list .results-table .name a').click(function(){
		var linkElement = $(this).parent().parent().next().find('.two-cols');
		$('.results-table .list-detail .two-cols').hide();
		$('.search-result-list .no-active').removeClass('best');
		if (!linkElement.is(':visible')) {
			linkElement.show();
			$(this).parent().parent().parent().parent().parent().addClass('best');
		}
		return false;
	});
	$('.best .list-detail .two-cols').show();

	//плюсы и минус инпутов
	$('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
	$('.product-selected .delete').click(function(){
		$(this).parent().fadeOut(500);
		return false;
	});

		if(jQuery('.js-pUp__openeer').length > 0 && jQuery('.js-pUp_m').length > 0){
			pUp_m.eInit();
		}

	if(jQuery('.js-select-date_week'.length > 0)){

		function render_select(){ // генерируем селект
			var curNowDat = new Date();
			var curDate = curNowDat.getDate();
			var curMonth = curNowDat.getMonth();
			var curYear = curNowDat.getFullYear();
			var curStrMonth, curStrDate,curNextStrDate,curNextStrMonth;
			var curStrHtml ="";

			curStrDate = 'Завтра'+curDate +' '+ curStrMonth+' '+curYear;

			for(var i = 1, len = 8; i < len; i++) {
				var curNextNowDat = new Date();
				var curNextDate = new Date(curNextNowDat.setDate(curNowDat.getDate()+i));
				var curNextDay = curNextDate.getDate();
				var curNextMonth = curNextDate.getMonth();
				var curNextYear = curNextDate.getFullYear();

	        switch (curNextMonth) {
	          case (curNextMonth = 0):
	          	curNextStrMonth = "Января"
	              break;
	          case (curNextMonth = 1):
	             curNextStrMonth = "Февраля"
	              break;
	          case (curNextMonth = 2):
	             curNextStrMonth = "Марта"
	              break;
	          case (curNextMonth = 3):
	             curNextStrMonth = "Апреля"
	              break;
	          case (curNextMonth = 4):
	             curNextStrMonth = "Мая"
	              break;
	          case (curNextMonth = 5):
	             curNextStrMonth = "Июня"
	              break;
	          case (curNextMonth = 6):
	             curNextStrMonth = "Июля"
	              break;
	          case (curNextMonth = 7):
	             curNextStrMonth = "Августа"
	              break;
	          case (curNextMonth = 8):
	             curNextStrMonth = "Сентября"
	              break;
	          case (curNextMonth = 9):
	             curNextStrMonth = "Октября"
	              break;
	          case (curNextMonth = 10):
	             curNextStrMonth = "Ноября"
	              break;
	          case (curNextMonth = 11):
	             curNextStrMonth = "Декабря"
	              break;
						default:
							curNextStrMonth = curNextMonth + " месяца"
							break
					}

				var curStrSel = ((i==1)?'Завтра, '+curNextDay:curNextDay) +' '+curNextStrMonth+' '+curNextYear;
				var curStrAttr = curNextDay +'-'+((curNextMonth<10)?'0'+curNextMonth : curNextMonth)+'-'+curNextYear;

				var curNextStrHtml = '<option value="'+ curStrAttr +'">'+curStrSel+'</option>';

				curStrHtml+= curNextStrHtml;
      }

            var gg = '<select class="js-select-date_week--select">'+curStrHtml+'</select>';

            jQuery('.js-select-date_week').html(gg);
            jQuery(jQuery('.js-select-date_week option')[0]).prop("selected", true);
		}

		render_select();

		// выбор даты в записи к врачу - ВНУТРИ ПЛАГИНА СТИЛИЗАЦИИ СЕЛКТА
			if (jQuery('.js-select-date_week--select').length) {
				jQuery('.js-select-date_week--select').styler({
					selectSmartPositioning:false,
					onSelectClosed:function(){
						var curInd = jQuery(this).find('li.selected').index();
						var curI = jQuery(jQuery(this).find('select option')[curInd]).val();

						jQuery('#js-time-get').find('input[name="date"]').val(curI);
					}
			});
			};
	}

	// выбор даты в записи к врачу
	jQuery(document).on('change', '.js-select-date_week--select', function(e){
		var curE = e.currentTarget;
		var curD = jQuery(curE).val()

		jQuery('#js-time-get').find('input[name="date"]').val(curD);
	})

	// выбор времени в записи к врачу
	jQuery(document).on('click', '.js-time-get li', function(e){
		var curE= e.currentTarget;
		var cuI = jQuery(curE).find('span').text()

		if(!jQuery(curE).hasClass('__active')){
			jQuery(curE).parents('.js-time-get').find('.__active').removeClass('__active');
			jQuery(curE).addClass('__active');
			jQuery('#js-time-get').find('input[name="time"]').val(cuI);
		}

	})

	// rating stars
	var rStar = {
		eInit:function(){
			jQuery(document).on('mouseenter', '.j-rating-star li', rStar.eEnt);
			jQuery(document).on('mouseleave', '.j-rating-star', rStar.eLV);
			jQuery(document).on('mouseleave', '.j-rating-star li', rStar.eLVs);
			jQuery(document).on('click', '.j-rating-star li', rStar.eSetR);
		},
		eEnt: function(e){ // мыша над звездой
			var curE = e.currentTarget;
			var curIndx = jQuery(curE).index();
			jQuery(curE).prevAll().addClass('__hov');
			jQuery(curE).addClass('__hov');
		},
		eLVs: function(e){ // мыша ушла со строки со звёздами
			var curE = e.currentTarget;
			jQuery(curE).nextAll().removeClass('__hov');
			jQuery(curE).removeClass('__hov');
		},
		eLV: function(e){ // мыша ушла со звезды
			var curE = e.currentTarget;
			jQuery(curE).find('li').removeClass('__hov');

		}
		,eSetR: function(e){ // закрепляем значение
			var curE = e.currentTarget;
			var curIndx = jQuery(curE).index();
			var curVal = curIndx;
			var curElemIndex = jQuery(curE).parents('.j-rating-star').find('li')[curIndx];
			var curElements = jQuery(curElemIndex).prevAll();

			jQuery(curE).parents('.j-rating-star').find('li').removeClass('__active');
			curElements.addClass('__active');
			jQuery(curE).addClass('__active');
			jQuery(curE).parents('.j-rating-star').find('.b-rating__input').val(curVal);
		}
	}

	if(jQuery('.j-rating-star').length > 0){
		rStar.eInit();
	}

});

//выравнивание по высоте
function equalHeight(group) {
    tallest = 0;
    group.each(function() {
        thisHeight = $(this).innerHeight();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.innerHeight(tallest);
}

//выбор города
$('.city select').on('change', function(){
	city_id = $(this).val();
	console.log(city_id);

	$.post('/ajax/change_city', {city_id : city_id}, function () {});
});

$(window).load(function(){
	//слайдер на главной
	$(".slider_top").owlCarousel({
		autoPlay:true,
		transitionStyle:"fade",
		navigation : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true
	});


	equalHeight($(".publication .publication-item h3"));
	equalHeight($(".actions-list .list .item"));

	$(".b-logos-i").owlCarousel({
		transitionStyle:"fade",
		navigation : true,
		slideSpeed : 300,
		pagination:false,
		singleItem:false,
		items : 6,
		itemsDesktop : [1170,6], //5 items between 1000px and 901px
		itemsDesktopSmall : false, // betweem 900px and 601px
		itemsTablet: false, //2 items between 600 and 0
		itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
	});

});

// Если успешно - записывает, если нет, то выкидывает алерт

$('#send_entry_form').on('click', function(){
    form = $('#entry_form').serialize();

    $.post('/ajax/entry', form, function(data) {
        if (data.status == 'error') {
			b_al.strMsg = data.error;
			b_al.eOpen();
		}

        return false;
    });
});

// pUp - modal
	var pUp_m = { // работаем с попапами модальных окон
			eInit: function(){
				jQuery(document).off('click.openPup');
				jQuery(document).on('click.openPup', '.js-pUp__openeer', pUp_m.eGetId);
				jQuery(document).off('click.closePuppp');
				jQuery(document).on('click.closePuppp', '.js-pUp_m .js-pUp_m__x', pUp_m.eHidePup);
				jQuery(document).off('click.closePupBG');
				jQuery(document).on('click.closePupBG', '.js-pUp_m .js-pUp_m__bg', pUp_m.eHidePup);
			},
			eGetId: function(e){ // берём  ID попапа из атрибута data
				e.preventDefault();

				var curID = (jQuery(e.currentTarget).data().pupId) ? jQuery(e.currentTarget).data().pupId : false;

				if(curID){
					pUp_m.eShowPup(curID)
				}
			},
			eHidePup: function(e){ // скрываем ранее открытые попапы - модальных окон
				if(jQuery(e.currentTarget).hasClass('js-pUp_m__bg_not-x')){
					return;
				}

				jQuery('#body').removeClass('OVF_not');
				jQuery('.js-pUp_m__item').hide();
				jQuery('.js-pUp_m__item').removeClass('loading');
				jQuery('.js-pUp_m').hide();
			},
			eShowPup: function(id_pup){ // показываем нужный нам попап
				var curHeightBody = jQuery(document).height();
				pUp_m.eHidePup(id_pup);
				jQuery('#body').addClass('OVF_not');
				jQuery('.pUps').height(curHeightBody);
				jQuery('.js-pUp_m').show();
				jQuery('#'+id_pup).fadeIn();
			}
	}
	// END - pUp - modal

//  date UI - русифицируем календарик

jQuery(document).ready(function(){
	if(jQuery('.js-date-i').length > 0){
		(function( factory ) {
			if ( typeof define === "function" && define.amd ) {

				// AMD. Register as an anonymous module.
				define([ "js/jquery-ui" ], factory );
			} else {

				// Browser globals
				factory( jQuery.datepicker );
			}
		}(function( datepicker ) {
			if (datepicker) {
				datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: 'Предыдущий',
					nextText: 'Следующий',
					currentText: 'Сегодня',
					monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
					monthNamesShort: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
					dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
					dayNamesShort: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
					dayNamesMin: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
					weekHeader: 'Sem.',
					firstDay: 1,
					showOtherMonths: true,
					selectOtherMonths: true,
					yearRange:"1940:+0",
					yearSuffix: ''};
				datepicker.setDefaults(datepicker.regional['ru']);

				return datepicker.regional['ru'];
			}
		}));


		if(jQuery('.js-date-i').length > 0){
			jQuery('.js-date-i').datepicker({
				changeMonth: true,
				changeYear: true,
				minDate: "-365000",
				maxDate: "0",
				showOn: "button",
				dateFormat: "dd-mm-yy",
				onSelect: function(i, v){
					jQuery(this).parents('.js-date_box').find('.js-date-i').val(i);
				}
			});
		}
	}


	// включаем маску на поле ввода "номер телефона"
	if(jQuery('.js-tel').length > 0){
		jQuery( ".js-tel" ).mask("+7 (999) 999 99 99");
	}

	//  ХИНТ - подсказка вариантов запроса при поиске по сайту
	if(jQuery('.js-search-box').length > 0 &&
			jQuery('.js-search-box .js-search-hint').length > 0 &&
			jQuery('.js-search-box .js-search-input').length > 0  &&
			jQuery('.js-search-box .js-search-btn').length > 0){

		// показываем попап - хинт
		function openSearchHint(){
			jQuery('.js-search-box .js-search-hint').addClass('_active');
		}
		// скрываем попап - хинт
		function closeSearchHint(){
			jQuery('.js-search-box .js-search-hint').removeClass('_active');
		}
		// ставим фокус на кнопку "найти"
		function setFocusSearchHint(){
			jQuery('.js-search-box .js-search-btn').focus();
		}
		// нажимаем на кнопку "найти" - запускается поиск
		function eSearchHintGO(){
			jQuery('.js-search-box .js-search-btn').click();
		}

		// Если нажали на один из результатов в хинта результатов
		jQuery(document).off('click.search-hint');
		jQuery(document).on('click.search-hint', '.js-search-hint .js-search-hint-i', function(e){
			e.preventDefault();

			var curE = e.currentTarget;
			var curTxt = jQuery(curE).text();

			jQuery(curE).parents('.js-search-box').find('.js-search-input').val(curTxt);
			setFocusSearchHint(); // если надо поставить фокус на кнопку войти
			// eSearchHintGO(); // если надо сэмулировать клик по кнопке найти
			closeSearchHint();
			// если после выбора значения из хинта будет срабатыватть клик на "НАЙТИ" и
			// не подгружаться аяксом содержимое, то можно закоментить закрытие
			// всё-равно до него не дойдёт, переход по кнопку случится раньше
		})

		// Если нажали "Enter" не выбрав результата из хинта результатов
		jQuery('.js-search-input').off('keypress.go-search');
		jQuery('.js-search-input').on('keypress.go-search', function(e){
			if(e.keyCode==13){
				e.preventDefault();
				eSearchHintGO();
			}
		});
	}
	//  КОНЕЦ ХИНТ - подсказка вариантов запроса при поиске по сайту

	//  photho-galery
	if(jQuery(".js-fb-bgalery").length > 0){
		jQuery(".js-fb-bgalery").fancybox({
			prevEffect: 'none',
			nextEffect: 'none',
			padding : 0,
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
	}
})

//Отправа формы обратной связи
$('.submit_question').click(function(){
	form = $(this).parent().find("textarea");
	form_parametrs = $(this).parent().serialize();
	$.post('/ajax/ask_question', form_parametrs, function(data) {
		if (data.status == "success") {
			b_al.strMsg = 'Вопрос отправлен. В ближайшее время вам ответят на него';
			b_al.eOpen();
			form.val("");
		} else {
			b_al.strMsg = 'Ошибка: ' + data.data.error;
			b_al.eOpen();
		}
	},'json');
	return false;
});

//Отправа формы обратной связи
$('.submit_review').click(function(){
	form = $(this).parent().find("textarea");
	form_parametrs = $(this).parent().serialize();
	$.post('/ajax/send_review', form_parametrs, function(data) {
		if (data.status == "success") {
			b_al.strMsg = 'Отзыв отправлен. После модерации он появится на сайте';
			b_al.eOpen();
			form.val("");
		} else {
			b_al.strMsg = data.data.error;
			b_al.eOpen();
		}
	},'json');
	return false;
});

$('.in-development').click(function(e){
	e.preventDefault();
	// replace_message_in_popup('#pUp-alert', 'Данный сервис находиться в разработке и будет доступен позже');
	// pUp_m.eShowPup('pUp-alert');

	b_al.strMsg = "Данный сервис находиться в разработке и будет доступен позже";
	b_al.eOpen();

	return false;
});

function leaf_suggestion( element )
{
    switch (element) {
        case 'next' :
            $(".search_suggestion");
            break;
        case 'prev' :
            $(".search_suggestion");
            break;
    }
}

// Автодополнение
$("#search_query").keyup(function(pressed_key){
    pressed_key = pressed_key.keyCode;
	switch(pressed_key) {
		// игнорирвание управляющих клавиш
		case 13:  // enter
		case 27:  // escape
		case 16:  // shift
		case 17:  // ctrl
		case 18:  // alt
		case 9:   // tab
		case 37:  // стрелка влево
		case 38:  // стрелка вверх
		case 39:  // стрелка вправо
		case 40:  // стрелка вниз
		case 93:  // контекстное меню
		case 91:  // клавиша windows
		case 112:  // f1
		case 113:  // f2
		case 114:  // f3
		case 115:  // f4
		case 116:  // f5
		case 117:  // f6
		case 118:  // f7
		case 119:  // f8
		case 120:  // f9
		case 121:  // f10
		case 122:  // f11
		case 123:  // f12
        case 33:   // PgUp
        case 34:   // PgDn
			break;

		default:
			if ($(this).val().length > 2) {
                query_string = $(this).val();

                $.get("/ajax/search", { "query" : query_string },function(data) {
                    if (!empty(data)) {
                        suggest = '<ul class="search_suggestion_block b-pUp-search-hint _active js-search-hint">'
                        for (var i in data) {
                            suggest += '<li class="b-pUp-search-hint__i js-search-hint-i">' + data[i]['NAME'] + '</li>';
                        }
                        suggest += '</ul>';

                        $(".search_suggestion_block").remove();
                        $("#main_search").append(suggest);
                    } else {
                        $(".search_suggestion_block").remove();
                    }
                }, "json");
			} else {
                $(".search_suggestion_block").remove();
            }
        break;
	}

    switch(pressed_key.keyCode) {
        case 13:  // enter
        case 27:  // escape
            $('.search_suggestion_block').remove();
            return false;
            break;
        case 38: // стрелка вверх
        case 40: // стрелка вниз
            pressed_key.preventDefault();
            key_activate( pressed_key.keyCode-39 );
            break;
        case 16:  // shift
        case 17:  // ctrl
        case 18:  // alt
        case 9:   // tab
        case 37:  // стрелка влево
        case 39:  // стрелка вправо
        case 93:  // контекстное меню
        case 91:  // клавиша windows
        case 112:  // f1
        case 113:  // f2
        case 114:  // f3
        case 115:  // f4
        case 116:  // f5
        case 117:  // f6
        case 118:  // f7
        case 119:  // f8
        case 120:  // f9
        case 121:  // f10
        case 122:  // f11
        case 123:  // f12
        case 33:   // PgUp
        case 34:   // PgDn
    }
});


//Автодополнение для аптек и т.д.
$("#organisation_search_query").keyup(function(pressed_key){
	pressed_key = pressed_key.keyCode;
	switch(pressed_key) {
		// игнорирвание управляющих клавиш
		case 13:  // enter
		case 27:  // escape
		case 16:  // shift
		case 17:  // ctrl
		case 18:  // alt
		case 9:   // tab
		case 37:  // стрелка влево
		case 38:  // стрелка вверх
		case 39:  // стрелка вправо
		case 40:  // стрелка вниз
		case 93:  // контекстное меню
		case 91:  // клавиша windows
		case 112:  // f1
		case 113:  // f2
		case 114:  // f3
		case 115:  // f4
		case 116:  // f5
		case 117:  // f6
		case 118:  // f7
		case 119:  // f8
		case 120:  // f9
		case 121:  // f10
		case 122:  // f11
		case 123:  // f12
		case 33:   // PgUp
		case 34:   // PgDn
			break;

		default:
			if ($(this).val().length > 2) {
				query_string = $(this).val();
				organisation_id = $(this).attr('attr-id');

				$.get("/ajax/search_in_organisation", { "id" : organisation_id, "query" : query_string },function(data) {
					console.log(data);
					if (!empty(data)) {
						suggest = '<ul class="search_suggestion">'
						for (var i in data) {
							suggest += '<li>' + data[i]['NAME'] + '</li>';
						}
						suggest += '</ul>';

						$(".search_suggestion").remove();
						$("#organisation_search").append(suggest);
					} else {
						$(".search_suggestion").remove();
					}
				}, "json");
			} else {
				$(".search_suggestion").remove();
			}
			break;
	}

	switch(pressed_key.keyCode) {
		case 13:  // enter
		case 27:  // escape
			$('.search_suggestion').remove();
			return false;
			break;
		case 38: // стрелка вверх
		case 40: // стрелка вниз
			pressed_key.preventDefault();
			key_activate( pressed_key.keyCode-39 );
			break;
		case 16:  // shift
		case 17:  // ctrl
		case 18:  // alt
		case 9:   // tab
		case 37:  // стрелка влево
		case 39:  // стрелка вправо
		case 93:  // контекстное меню
		case 91:  // клавиша windows
		case 112:  // f1
		case 113:  // f2
		case 114:  // f3
		case 115:  // f4
		case 116:  // f5
		case 117:  // f6
		case 118:  // f7
		case 119:  // f8
		case 120:  // f9
		case 121:  // f10
		case 122:  // f11
		case 123:  // f12
		case 33:   // PgUp
		case 34:   // PgDn
	}
});

$('html').click(function(){
    $('.search_suggestion_block').fadeOut(350);
    $('.search_suggestion_block').remove();
});

$('#main_search').on('click', '.search_suggestion_block li', function(){
    $('#search_query').val($(this).text());
    // прячем слой подсказки
    $('.search_suggestion_block').fadeOut(350);
    $('.search_suggestion_block').remove();
});

$('#organisation_search').on('click', '.search_suggestion li', function(){
	$('#organisation_search_query').val($(this).text());
	// прячем слой подсказки
	$('.search_suggestion').fadeOut(350);
	$('.search_suggestion').remove();
});

function load_economical_buy_list() {
	$.get("/ajax/economical_buy?GOODS[38]=1&GOODS[123362]=1", /*{ "query" : query_string }*/null, function(data) {
		$(".economical").html(data);
	}, "html");
}

function add_to_economical() {
	if (!$("*").is("#new_eco_buy_element")) {
		var eco_buy_element =
				'<div class="product-selected" id="new_eco_buy_element">' +
					'<input type="text" placeholder="Начните вводить название, а затем выберите лекарство из списка">' +
					'<div class="value">' +
						'<span class="minus arrow">&lt;</span>' +
						'<input type="text" class="text" value="1" size="5"/>' +
						'<span class="plus arrow">&gt;</span>' +
					'</div>' +
					'<a class="delete" href="#">Удалить</a>' +
				'</div>'

		$("#eco_buy_list").append(eco_buy_element);
	} else {
		replace_message_in_popup('#pUp-alert', 'Уже добавлено');
		pUp_m.eShowPup('pUp-alert');
	}
}

$("#eco_buy_list").on('keyup', '#new_eco_buy_element', function(pressed_key){
	pressed_key = pressed_key.keyCode;
	switch(pressed_key) {
		// игнорирвание управляющих клавиш
		case 13:  // enter
		case 27:  // escape
		case 16:  // shift
		case 17:  // ctrl
		case 18:  // alt
		case 9:   // tab
		case 37:  // стрелка влево
		case 38:  // стрелка вверх
		case 39:  // стрелка вправо
		case 40:  // стрелка вниз
		case 93:  // контекстное меню
		case 91:  // клавиша windows
		case 112:  // f1
		case 113:  // f2
		case 114:  // f3
		case 115:  // f4
		case 116:  // f5
		case 117:  // f6
		case 118:  // f7
		case 119:  // f8
		case 120:  // f9
		case 121:  // f10
		case 122:  // f11
		case 123:  // f12
		case 33:   // PgUp
		case 34:   // PgDn
			break;

		default:
			if ($(this).children('input').val().length > 2) {
				query_string = $(this).children('input').val();
				$.get("/ajax/search", { "query" : query_string },function(data) {
					if (!empty(data)) {
						suggest = '<ul class="search_suggestion">'
						for (var i in data) {
							suggest += '<li>' + data[i]['value'] + '</li>';
						}
						suggest += '</ul>';

						$(".search_suggestion").remove();
						$("#new_eco_buy_element input").append(suggest);
					} else {
						$(".search_suggestion").remove();
					}
				}, "json");
			} else {
				$(".search_suggestion").remove();
			}
			break;
	}

	switch(pressed_key.keyCode) {
		case 13:  // enter
		case 27:  // escape
			$('.search_suggestion').remove();
			return false;
			break;
		case 38: // стрелка вверх
		case 40: // стрелка вниз
			pressed_key.preventDefault();
			key_activate( pressed_key.keyCode-39 );
			break;
		case 16:  // shift
		case 17:  // ctrl
		case 18:  // alt
		case 9:   // tab
		case 37:  // стрелка влево
		case 39:  // стрелка вправо
		case 93:  // контекстное меню
		case 91:  // клавиша windows
		case 112:  // f1
		case 113:  // f2
		case 114:  // f3
		case 115:  // f4
		case 116:  // f5
		case 117:  // f6
		case 118:  // f7
		case 119:  // f8
		case 120:  // f9
		case 121:  // f10
		case 122:  // f11
		case 123:  // f12
		case 33:   // PgUp
		case 34:   // PgDn
	}
});


$('.js-select-date_week').on('change', 'select', function() {
	text = $(this).val();
	$('#entry_date').val(text);
});

$('#entry_time_choose > ul > li').on('click', function() {
	text = $(this).children('span').text();
	$('#entry_time').val(text);
});

$('#add_to_favorites').on('click', function(){
    id = $(this).attr('attr-id');
    type = $(this).attr('attr-type');
    $.post('/ajax/add_favorites', { id: id, type: type }, function(data) {
        if (data.status == 'error') {
			b_al.strMsg = data.error;
			b_al.eOpen();
        } else if (data.status == 'success') {
			b_al.strMsg = data.message;
			b_al.eOpen();
            $('#add_to_favorites').val(data.button_value);
        } else {
			b_al.strMsg = 'Неизвестная ошибка';
			b_al.eOpen();
        }
    });
});

$('#search_query').on('focus', function(){
    var scroll_el = $('#search'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
    if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
        $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 300); // анимируем скроолинг к элементу scroll_el
    }
    return false; // выключаем стандартное действие
});

$('#search_query').on('click', function(){
    var scroll_el = $('#search'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
    if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
        $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 300); // анимируем скроолинг к элементу scroll_el
    }
    return false; // выключаем стандартное действие
});

$('#change_place').on('click', function(event){
	event.preventDefault();
	var scroll_el = $('#map_search'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
	if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
		$('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 300); // анимируем скроолинг к элементу scroll_el
	}
	return false; // выключаем стандартное действие
});

$('.send_rating').on('click', function (e) {
	e.preventDefault();

	form = $('.b-comments-f').serialize();
   // alert(form);
	$.post('/ajax/rate', form, function(data) {
		if (data.status == 'error') {
			var errors = "";

			errors = errors + "<ul style='list-style-type: none'>";

			for (var key in data.errors) {
				errors = errors + "<li>" + data.errors[key] + "</li>";
			}

			errors = errors + "</ul>";

			b_al.strMsg = errors;
			b_al.eOpen();
		} else if (data.status == 'success') {
			b_al.strMsg = data.message;
			b_al.eOpen();
		} else {
			b_al.strMsg = 'Неизвестная ошибка';
			b_al.eOpen();
		}
	});
});

$('.booking').on('click', function (e) {
	e.preventDefault();
	obj = $(this);
	id = obj.attr('attr-id');
	name = obj.parent().parent().children('td.name').children('a').html();
	
	$('#good_name').html(name);
	$('#good_id').val(id);

	pUp_m.eShowPup('pUp-bron');
});

$('.booking_submit').on('click', function (e) {
	e.preventDefault();
	$.post('/ajax/booking', $('#booking_form').serialize(), function(data) {
		if (data.status == 'error') {
			var errors = "";

			for (var key in data.errors) {
				errors = errors + data.errors[key] + "\r\n";
			}

			b_al.strMsg = errors;
			b_al.eOpen();
		} else if (data.status == 'success') {
			b_al.strMsg = 'Вы успешно забронировали товар';
			b_al.eOpen();
		} else {
			b_al.strMsg = 'Неизвестная ошибка';
			b_al.eOpen();
		}
	});
});

$('.pUps__bg.js-pUp_m__bg').on('click', function() {
	jQuery('#body').removeClass('OVF_not');
	jQuery('.js-pUp_m__item').hide();
	jQuery('.js-pUp_m__item').removeClass('loading');
	jQuery('.js-pUp_m').hide();
});

$('.b-ALERT__ov').on('click', function() {
	if($('.js-alert').hasClass('___act')){
		$('.js-alert').removeClass('___act');
		$('.js-alert .b-ALERT__inner').html("");
	}
});

function replace_message_in_popup(popup, text)
{
	$(popup + " .b-comments__info").html(text);
	console.log(popup + " b-comments__info : " + text);
}

function on_click_make_active(element)
{
	$('.map_search_categories').children().removeClass('active');
	$(element).parent().addClass('active');
}

// Если успешно - записывает, если нет, то выкидывает алерт

$('#send_entry_form').on('click', function(){
	form = $('#entry_form').serialize();

	$.post('/ajax/entry', form, function(data) {
		if (data.status == 'error') {
			b_al.strMsg = data.error;
			b_al.eOpen();
		}

		return false;
	});
});

function inst_entry(e, form)
{
	e.preventDefault();

	$.post('/ajax/inst_entry', $(form).serialize(), function(data) {
		if (data.status == 'error') {
			b_al.strMsg = data.error;
			b_al.eOpen();
		} else {
			b_al.strMsg = 'Ваша заявка принята!<br> В ближайшее время с вами свяжется наш специалист';
			b_al.eOpen();
		}

		return false;
	});
}

function send_vote(e, form)
{
	e.preventDefault();

	$.post('/ajax/vote', $(form).serialize(), function(data) {
		if (data.status == 'error') {
			b_al.strMsg = data.error;
			b_al.eOpen();
		} else {
			$('#vote').children('ul').remove();
			$('#vote').append(data.html);
		}

		return false;
	});
}