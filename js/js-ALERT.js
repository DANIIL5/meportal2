//  ALERT
var b_al = {
  strMsg: "", 
  // strMsg - тут хранится контент алерта, если пусто - то алерт не откроется - 
  //строковая переменная - она может хранить и HTML
  eInit: function(){ // init
    $(document).on('click.alert_x', '.js-alert .b-ALERT__x', b_al.closeX);
    $(document).on('click.alert_x', '.js-alert__e', b_al.eOpen);
  },
  eOpen: function(){ //  strMSG - текст алерта, который заменяет собой содержимое внутри алерта

    if($('.js-alert').hasClass('___act')){ // закрываем открытые алерты
       b_al.closeX();
    }

    if(b_al.strMsg){ // вставляем содержимое алерта внуть бокса -- если содержимого нет, алерт не появится
      $('.js-alert .b-ALERT__inner').html("");
      $('.js-alert .b-ALERT__inner').html(b_al.strMsg);
    } else {
      return;
    }

    // закрываем все модальные окна
      jQuery('#body').removeClass('OVF_not');

      if(jQuery('.js-pUp_m__item').length){
        jQuery('.js-pUp_m__item').hide();
      }
      if(jQuery('.js-pUp_m__item').length){
        jQuery('.js-pUp_m__item').removeClass('loading');
      }
      if(jQuery('.js-pUp_m').length){
        jQuery('.js-pUp_m').hide();
      }
    // закрываем все модальные окна
    
    if(!$('.js-alert').hasClass('___act')){ // показываем алерт
      $('.js-alert').addClass('___act');
      // var curT = ($(window).height()/2) - ($('.b-ALERT__box').outerHeight()/2); // позиуионирование по-вертикали - по центру
      var curT = Math.ceil(($(window).height()/10)); // позиуионирование по-вертикали - по центру
      var curL = ($(window).width()/2) - ($('.b-ALERT__box').outerWidth()/2);
      $('.b-ALERT__box').animate({'top': curT, 'left': curL}, 300)
    }  

  },
  closeX: function(){
    if($('.js-alert').hasClass('___act')){
      $('.js-alert').removeClass('___act');
      $('.js-alert .b-ALERT__inner').html("");
    }    
  }
}

$(function(){
  b_al.eInit();
})