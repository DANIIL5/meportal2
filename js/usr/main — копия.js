// my js
jQuery(document).ready(function(){

	var curDocH = jQuery(window).height();
	var curT = [];

	function autoSetHeight(){
		clearTimeout(curT);
		curT = setTimeout(function(){
			curDocH = jQuery(window).height();

			jQuery('.body').css('min-height', curDocH);
		}, 100);
	}

	jQuery(window).off('resize.res-w');
	jQuery(window).on('resize.res-w', autoSetHeight);

	autoSetHeight();

//  change password
	if (jQuery('.js-change-pass .js-change-pass__loop').length) {
		jQuery(document).on('click', '.js-change-pass .js-change-pass__loop', showPass);

		function showPass(e){
			var curP = jQuery(e.currentTarget).parents('.js-change-pass');
			var curInput = curP.find('.js-change-pass__input');

			if(curInput.attr('type') == 'password'){
				curInput.attr('type', 'text');
				jQuery(e.currentTarget).addClass('showPass');
			} else {
				curInput.attr('type', 'password');
				jQuery(e.currentTarget).removeClass('showPass');
			}
		}
	}
// =============

//  стилизация элементов форм
	if (jQuery('.js-mail').length) {
		jQuery('.js-mail').off('click.open-msg');
		jQuery('.js-mail').on('click.open-msg', '.js-mail__opener', openMsg);

		jQuery('.js-mail').on('click.close-msg', '.js-mail__close', closeMsg);

		function openMsg(e){
			var curE = e.currentTarget;

			if(jQuery('.b-msg__row_view').length){
				jQuery('.b-msg__row_view').removeClass('b-msg__row_view');
			}

			if(!jQuery(curE).parents('.js-mail').hasClass('b-msg__row_view')){
				jQuery(curE).parents('.js-mail').addClass('b-msg__row_view');
			}
		}

		function closeMsg(e){
			var curEl = e.currentTarget;
			if(jQuery(curEl).parents('.js-mail').hasClass('b-msg__row_view')){
				jQuery(curEl).parents('.js-mail').removeClass('b-msg__row_view');
			}
		}
	};

	if (jQuery('.js-select').length) {
		jQuery('.js-select').styler();
	};
	if (jQuery('.js-checkbox').length) {
		jQuery('.js-checkbox').styler();
	};
	if (jQuery('.js-def-select').length) {
		jQuery('.js-def-select').styler();
	};

// карусель
	if (jQuery('#js-slider-bx').length) {
		jQuery('#js-slider-bx').bxSlider({ // инициализация карусели
			// mode: 'fade',
			speed: 500,
			nextSelector: '#crsl_next',
			prevSelector: '#crsl_prew',
			nextText: '',
			prevText: '',
			infiniteLoop: false
		});
	}

// маски
	if(jQuery('.js-date-i')){
		(function( factory ) {
			if ( typeof define === "function" && define.amd ) {

				// AMD. Register as an anonymous module.
				define([ "com/jquery-ui.min" ], factory );
			} else {

				// Browser globals
				factory( jQuery.datepicker );
			}
		}(function( datepicker ) {
			if (datepicker) {
				datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: 'Предыдущий',
					nextText: 'Следующий',
					currentText: 'Сегодня',
					monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
					monthNamesShort: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
					dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
					dayNamesShort: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
					dayNamesMin: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
					weekHeader: 'Sem.',
					firstDay: 1,
					showOtherMonths: true,
					selectOtherMonths: true,
					yearRange:"1940:+0",
					yearSuffix: ''};
				datepicker.setDefaults(datepicker.regional['ru']);

				return datepicker.regional['ru'];
			}
		}));

		jQuery( ".js-date-i" ).mask(
			"99/99/9999",
			{
				completed: function(){
					var curD = jQuery(this).val();
					jQuery(this).closest('.box-fields').find('.js-date-i').datepicker( "setDate", curD);
				}

			}
		);

		jQuery('.js-date-i').datepicker({
			changeMonth: true,
			changeYear: true,
			minDate: "-365000",
			maxDate: "0",
			showOn: "button",
			dateFormat: "dd/mm/yy",
			onSelect: function(i, v){
				jQuery(this).parents('.js-date_box').find('.js-date-i').val(i);
			}
		});

		jQuery(document).on('click', '.js-date-e', function(e){
			jQuery(e.currentTarget).closest('.js-date_box').find('.ui-datepicker-trigger').click();
		})
	}

	if(jQuery('.js-tel')){
		jQuery( ".js-tel" ).mask("+7 (999) 999 99 99");
	}



// добавление файла
	var addF = { // добавление файла и создание превью
		nIndex:0,
		nPar:[],
		jAvaPar: [],
		eInit: function(){ // инициалищация
			jQuery(document).off('click.emulcl');
			jQuery(document).off('click.delF');
			jQuery(document).off('change.addF');

			jQuery(document).on('click.emulcl', '.js-add-f', addF.eCreatInput);
			jQuery(document).on('click.delF', '.jd-add-f_delete', addF.eDeleteFile);
			jQuery(document).on('change.addF','.js-add-f_i', function(){
				addF.eGetUrl(this);
			});

			jQuery(document).off('click.emulava');
			jQuery(document).on('click.emulava', '.js-avatar .js-avatar_init', addF.eInitChangeAva);

			jQuery(document).off('change.chlava');
			jQuery(document).on('change.chlava', '.js-avatar .js-avatar__form-i', function(){
				addF.eChangeAva(this)
			});
		},
		eCreatInput: function(e){ // создаём инпут в скрытой форме для выбора файла и эмулируем клик по нему, для выбора файла
			var curStIn="";
			addF.nPar = jQuery(e.currentTarget).parents('.js-add-files');

			if(!jQuery(e.currentTarget).parents('.js-add-files').find('[data-id_f="'+addF.nIndex+'"]').length){
				curStIn = '<input name="filename" type="file" accept="image/*" class="js-add-f_i" data-id_f = "'
					+ addF.nIndex
					+ '" style="margin-left:300px;display:none;">';
				addF.nPar.append(curStIn);
			}
			jQuery(e.currentTarget).parents('.js-add-files').find('[data-id_f="'+addF.nIndex+'"]').click();
		},
		eGetUrl: function(input){ // абираем адрес картиныки и создаём превьюху файла
			if (input.files && input.files[0]) {

				var reader = new FileReader();
				var curElem;
				var file = input.files[0];

				reader.onload = function (e) { // как только файл загрузился, выводим превьюшку
					curElem = '<li class="b-add-f-list__i js-add-li" data-id_prew ="'
						+ addF.nIndex+'" >'
						+ '<img src="'
						+ e.target.result
						+'"\\>'
						+ '<span class="b-add-f-list__i-delete jd-add-f_delete"></span></li>'

					jQuery(addF.nPar).find('.js-add-list').prepend(curElem);
					addF.nIndex++;
				}
				reader.readAsDataURL(file);
			}
		},
		eDeleteFile: function(e){ // удаляем файл
			var curEd = e.currentTarget;
			var curIndFile = jQuery(curEd).parents('.js-add-li').data().id_prew;

			jQuery(e.currentTarget).parents('.js-add-files').find('[data-id_f="'+curIndFile+'"]').remove();
			jQuery(e.currentTarget).parents('[data-id_prew="'+curIndFile+'"]').remove();
		},
		eInitChangeAva: function(e){ // меняем аватар
			addF.jAvaPar = jQuery(e.currentTarget).parents('.js-avatar');

			var curInput = addF.jAvaPar.find('.js-avatar__form-i');

			curInput.click();
		},
		eChangeAva: function(inp){ // меняем аватар
			if (inp.files && inp.files[0]) {
				var curHRC = "";
				var readerAva = new FileReader();

				readerAva.onloadstart = function (e) {
					addF.jAvaPar.find('.js-avatar__img').removeClass('notEmpty');
					addF.jAvaPar.find('.js-avatar__img').addClass('loader');
				}

				readerAva.onload = function (e) {
					curHRC =  e.target.result;
				}

				readerAva.onloadend = function (e) {
					var curT;

					addF.jAvaPar.find('.js-avatar__img').addClass('notEmpty');
					if(addF.jAvaPar.find('.js-avatar__img .js-avatar_i').length > 0){
						addF.jAvaPar.find('.js-avatar__img .js-avatar_i')[0].src = curHRC;
						curT = setTimeout(function(){
							addF.jAvaPar.find('.js-avatar__img').removeClass('loader');
						}, 2000);
					}
				}

				readerAva.readAsDataURL(inp.files[0]);
			}
		}
	}

	if(jQuery('.js-avatar').length || jQuery('.js-add-files').length){
		addF.eInit();
	}



// календарь
	var clnd = { // работа с календарём
		jEvCont:[],
		jParCont: [],
		nEvWidth:0,
		nMargin:0,
		jArr:[],
		isWork: false,
		oAjaxResult:[],
		eInit: function(){ // инициализируем работу с календарём


			jQuery('.js-clndr-e').off('click.ocE');
			jQuery('.js-clndr-e').on('click.ocE', clnd.eCreateGet);

			jQuery(document).off('click.reset-clnd');
			jQuery(document).off('click.reset-clndW');
			jQuery(document).on('click.reset-clnd', '.js-nav-calend .js-nav-calend__nav-i', clnd.eCloseE);
			jQuery(document).on('click.reset-clndW', '.js-nav-calend .js-nav-calend__dWeek', clnd.eCloseE);
		},
		eCreateGet: function(e){
			var curId = jQuery(e.currentTarget).parents('.js-clndr-b').data().idM;

			if(jQuery(e.currentTarget).parents('.js-clndr-b').hasClass('b-calend__event_active')){
				clnd.eCloseE();
			} else {
				// проверяем, есть ли ID данных из json чтобы вставить в выпадашку, если нет - выходим
				if(curId && !clnd.isWork){
					jQuery(e.currentTarget).parents('.js-clndr-b').addClass('b-calend__event_active');
					jQuery(e.currentTarget).addClass('__load');
					clnd.isWork = true;
					clnd.eAjax(e, curId);
				}
			}
		},
		eVar: function(e){
			clnd.jEvCont = jQuery('#js-event-desc');
			clnd.jParCont = jQuery('.js-calend-reg');
			clnd.jArr = clnd.jEvCont.find('.js-event-arr');

			clnd.nEvWidth = clnd.jEvCont.outerWidth();
			clnd.nMargin = (clnd.jParCont.data().typeCalend == 1 || clnd.jParCont.data().typeCalend == 7) ? 50 : 7;

			jQuery('.js-clndr-b.b-calend__event_active').removeClass('b-calend__event_active');
			jQuery(e.currentTarget).parents('.js-clndr-b').addClass('b-calend__event_active');
			clnd.eOpenD(e,  jQuery(e.currentTarget).parents('.js-clndr-b').data().idM);
		},
		eAjax: function(e, nId){
			jQuery.post( "f/js/usr/graph.php", {id: nId}, function(json){
				clnd.eSuccAjax(e,json);
			}, "json")
				.fail(clnd.eCloseE(e, status));
		},
		eSuccAjax: function(e, json){
			clnd.oAjaxResult = json;

			// имитация задержки ответа с сервера  - когда будет на сервере, таймаут удалить,
			clearTimeout(clnd.gg);
			clnd.gg = setTimeout(function(){
				console.log('Имитируем запрос аякса');
				clnd.eVar(e);
			}, 3000);

			// clnd.eVar(e); // а эту строчку раскоментить
		},
		eCloseE: function(e,status){ // закрыть описание мероприятия
			jQuery('.js-clndr-b').removeClass('b-calend__event_active');
			jQuery('#js-event-desc').removeClass('b-calend__event_content_active');
		},
		eOpenD: function(e, idJ){ // открываем описание мероприятия
			// вставляем данные в выпадашку
			clnd.jEvCont.find('.b-calend-e__title').text(clnd.oAjaxResult["title"]);
			clnd.jEvCont.find('.__time').text((clnd.oAjaxResult["time"])?clnd.oAjaxResult["time"]:"");
			clnd.jEvCont.find('.__date').text((clnd.oAjaxResult["date"])?clnd.oAjaxResult["date"]:"");
			clnd.jEvCont.find('.__place').text((clnd.oAjaxResult["place"])?clnd.oAjaxResult["place"]:"");
			clnd.jEvCont.find('.__desc').text((clnd.oAjaxResult["desc"])?clnd.oAjaxResult["desc"]:"");
			//  -----


			// считаем где появляться выпадашке
			var curW = jQuery(e.currentTarget).outerWidth(); // ширина кнопки с мероприятием
			var curH = jQuery(e.currentTarget).outerHeight(); // высота кнопки с мероприятием
			var curL = jQuery(e.currentTarget).offset().left; // отступ слева - кнопки с мероприятием
			var curt = jQuery(e.currentTarget).offset().top; // тступ сверзу -  с мероприятием
			var curLP = clnd.jParCont.offset().left; // ширина оттуп слева - родителя кнопки с мероприятием
			var curTP = clnd.jParCont.offset().top; // ширина оттуп сверху - родителя кнопки с мероприятием
			var curWP = clnd.jParCont.outerWidth(); // ширина  родителя кнопки с мероприятием
			var curHP = clnd.jParCont.outerHeight();  // высота родителя кнопки с мероприятием
			var curHD = clnd.jEvCont.outerHeight(); // высота контейнера с мероприятием
			var curP = jQuery(e.currentTarget).parents('.js-clndr-b'); // родитель кнопки мероприятия

			//  определяем координаты ЛЕВого края подсказки

			var curCoordL; // координаты X папапа мероприятия
			var curArrL; // координаты стрелочки;


			if(curP.hasClass('b-calend__event_active-revert')){
				curP.removeClass('b-calend__event_active-revert');
			}

			if (clnd.nEvWidth/2 > (curL - curLP)  + curW/2 - clnd.nMargin) {
				curCoordL = clnd.nMargin;
			} else{
				if(clnd.nEvWidth/2 > (curLP+curWP) - (curL+curW/2)){
					curCoordL = curWP - clnd.nEvWidth - 9;
				} else{
					curCoordL = (curL - curLP)+curW/2 -(clnd.nEvWidth/2);
				}

			}

			//  определяем координаты ВЕРХнего края подсказки

			var curCoordT;
			var deltaTop = curTP+curHP +  - curH - curt-4;

			if(deltaTop < 280){
				curCoordT = curt-curTP-4-curHD;
				curP.addClass('b-calend__event_active-revert');
				clnd.jEvCont.addClass('__revert');
			} else{
				if(clnd.jEvCont.hasClass('__revert')){
					clnd.jEvCont.removeClass('__revert');
				}
				curCoordT = (curt-curTP) + curH + 4;
			}

			// определяем координаты стрелочки
			curArrL = curL - curLP -curCoordL+ curW/2;


			// проставляем найденные значения

			clnd.eSetLeft(curCoordL, curArrL);
			clnd.eSetTop(curCoordT);

			//  показываем подсказку
			jQuery(e.currentTarget).removeClass('__load');
			clnd.jEvCont.addClass('b-calend__event_content_active');
			clnd.isWork = false;

		},
		gg:[],
		eSetLeft: function(nLeft, nArrL){ // выставляем левую координату описания мероприятия
			clnd.jEvCont.css({'left': nLeft});
			clnd.jArr.css({'left': nArrL});
		},
		eSetTop:function(nTop){ // выставляем верхнюю координату описания мероприятия
			clnd.jEvCont.css({'top': nTop });
		}
	}

	if(jQuery('.js-calend-reg').length){
		clnd.eInit();
	}

// попап выбора соцсети
	var socio = {
		eInit: function(){ // инициализация
			jQuery(document).off('click.socioPup');
			jQuery(document).on('click.socioPup', '.js-socio-opener',socio.eViewP);

			jQuery(document).off('click.socioOK');
			jQuery(document).on('click.socioOK', '.js-socio-ok',socio.eCloseP);
		},
		eViewP: function(e){ // показываем попап
			jQuery(e.currentTarget).parents('.js-socio').find('.js-socio-pUp').addClass('active');
			jQuery(e.currentTarget).addClass('pUp-act');

			jQuery(document).on('click.closeNOTdiv', function(e){ // создаём обработчик клика мимо открытого попапа
				if(jQuery(e.target).closest('.js-socio-pUp').length <= 0  // если клик мимо открытого попапа
					&& jQuery('.js-socio-pUp').hasClass('active')){
					socio.eCloseP();
				}
			});
		},
		eCloseP: function(){ // скрываем попап
			jQuery('.js-socio-pUp').removeClass('active');

			jQuery(document).off('click.closeNOTdiv'); // убиваем обработчик клика мимо открытого попапа
		}
	}

	if(jQuery('.js-socio').length > 0){
		socio.eInit();
	}

//  table sort

	var SortTb = { // СОРТИРОВКА ТАБЛИЦЫ ПО ФИЛЬТРАМ ВНЕ ТАБЛИЦЫ
		aIdFilters: [],
		eInit: function(){ // инит сортировки таблиц
			// сортировка сделана на два столбца
			// если будет необходисоть добавить ешё фильтров, нужно будет сделать по аналогии
			// добавить одинаковые класс в хедер таблицы и в id значения фильтра
			// а также разблокировать возможность сортировки в методе "eInitPlugin" - в месте инициализации плагина сортировки
			// jQuery('#js-sort-table').tablesorter({
			// 		headers: {
			//				     1: {  // указать номер столбца (нумерация начинается с "0" )
			// 									sorter: false  // поставить в "sorter" тип сортировки или вообще убрать - то будет сортировка по алфавиту
			//								}
			//		}
			//	})
			var curFiltersItems = [];

			curFiltersItems = jQuery('.js-sort').find('.js-sort-table__param');
			if(curFiltersItems.length > 0){ // собираем массив ID - фильтров

				SortTb.aIdFilters = [];
				jQuery.each(curFiltersItems, function(i,v){
					if(v.id){
						SortTb.aIdFilters.push(v.id);
					}
				})
			}

			SortTb.eInitPlugin();
			SortTb.eVARevent();
		},
		eVARevent: function(){ // навешиваем события
			jQuery(document).off('click.clk-param');
			jQuery(document).on('click.clk-param', '.js-sort-table__param', SortTb.eClkParamFilter);

			jQuery("#js-sort-table").bind("sortEnd", SortTb.eSortEnd);
		},
		eSortEnd: function(e, table){ // действия после окончания сортировки
			jQuery.each(SortTb.aIdFilters ,function(i,v){
				var curCl = '.'+v;
				var curIDd = '#'+v;

				if(jQuery(e.currentTarget).find(curCl).length > 0){

					if(jQuery(e.currentTarget).find(curCl).hasClass('headerSortDown')){
						if(!jQuery(curIDd).parents('.js-sort-table__param-p').hasClass('__down')){
							if(jQuery(curIDd).parents('.js-sort-table__param-p').hasClass('__up')){
								jQuery(curIDd).parents('.js-sort-table__param-p').removeClass('__up');
							}
							jQuery(curIDd).parents('.js-sort-table__param-p').addClass('__down');
						}
						if(!jQuery(curIDd).parents('.js-sort-table__param-p').hasClass('__active')){
							jQuery(curIDd).parents('.js-sort-param').find('.js-sort-table__param-p').removeClass('__active');
							jQuery(curIDd).parents('.js-sort-table__param-p').addClass('__active')
						}
					}

					if(jQuery(e.currentTarget).find(curCl).hasClass('headerSortUp')){
						if(!jQuery(curIDd).parents('.js-sort-table__param-p').hasClass('__up')){
							if(jQuery(curIDd).parents('.js-sort-table__param-p').hasClass('__down')){
								jQuery(curIDd).parents('.js-sort-table__param-p').removeClass('__down');
							}
							jQuery(curIDd).parents('.js-sort-table__param-p').addClass('__up');
						}

						if(!jQuery(curIDd).parents('.js-sort-table__param-p').hasClass('__active')){
							jQuery(curIDd).parents('.js-sort-param').find('.js-sort-table__param-p').removeClass('__active');
							jQuery(curIDd).parents('.js-sort-table__param-p').addClass('__active')
						}
					}

				}
			});
		},
		eClkParamFilter: function(e){ // эмулируем клик по хедеру таблицы при клике по фильтру
			e.preventDefault();

			if(!jQuery(e.currentTarget).parents('.js-sort-table__param-p').hasClass('__active')){
				jQuery(e.currentTarget).parents('.js-sort-param').find('.js-sort-table__param-p').removeClass('__active')
				jQuery(e.currentTarget).parents('.js-sort-table__param-p').addClass('__active');
			}

			if(e.currentTarget.id){
				var curClassElem = '.'+e.currentTarget.id;
				jQuery("#js-sort-table").find(curClassElem).click();
			}
		},
		eInitPlugin: function(){ // инициализируем плагин сортировки
			jQuery.tablesorter.addParser({
				// добавляем обработку сортировки даты с разделителем точкой, если дата будет другого формата,
				// то необходимо или изменить этот метод или указать метод сортировки зашитый в плагин
				// иначе будет сортироваться по алфавиту или вообще валить ошибки в логи
				id: 'pointDate',
				is: function(s) {
					return /^\d{1,2}[\.\/-]\d{1,2}[\.\/-]\d{4}$/.test(s);
				},
				format: function(s) {
					var e = s.split('.');
					return $.tablesorter.formatFloat((s != "") ? new Date(e[2]+'/'+e[1]+'/'+e[0]).getTime() : "0");
					// new Date(s.replace(new RegExp(/-/g),"/")).getTime();
				},
				type: 'numeric'
			});

			jQuery('#js-sort-table').tablesorter({  // сама инициализация плагина сортировки
				cancelSelection: false,
				sortList: [[0,0]],
				dateFormat : "mmddyyyy",
				headers: {
					1: {
						sorter: false // отключаем сортировку в столбцах
					},
					2: {
						sorter: false
					},
					3: {
						sorter: false
					},
					4:  { sorter: 'pointDate' },
					5: {
						sorter: false
					}
				}
			});
		}
	}

	if(jQuery('.js-sort .js-sort-table').length > 0){
		SortTb.eInit();
	}
//  END - table sort

//  open toogle-row
	var toogleRow = { // разворачиваем строки под "открывашкой""
		eInit:function(){
			// console.log('eInit');
			jQuery(document).off('click.open-tgr');
			jQuery(document).on('click.open-tgr', '.js-b-tgl-r .js-b-tgl-r__o', toogleRow.eOpenRow);
			jQuery(document).off('click.close-tgr');
			jQuery(document).on('click.close-tgr', '.js-b-tgl-r .js-b-tgl-r__x', toogleRow.eCloseRow);
		},
		eOpenRow: function(e){ // разворачиваем строки

			e.preventDefault();

			var curE = e.currentTarget;

			if(jQuery(curE).parents('.js-b-tgl-r__i').find('.js-b-tgl-r__c').length > 0){
				if(!jQuery(curE).parents('.js-b-tgl-r__i').hasClass('__t-on')){
					console.log('eOpenRow');
					toogleRow.eCloseRow(e);
					jQuery(curE).parents('.js-b-tgl-r__i').addClass('__t-on');
				}
			}
		},
		eCloseRow:  function(e){ // сворачиваем строки
			// console.log('eCloseRow');
			var curE = e.currentTarget;
			jQuery(curE).parents('.js-b-tgl').find('.js-b-tgl-r__i.__t-on').removeClass('__t-on');
		}
	}

	if (jQuery('.js-b-tgl-r').length > 0) {
		toogleRow.eInit();
	}
//  END  - open toogle-row
})
//  END - jREADY

jQuery(window).on('load', function(){ // дожидаемся окончания загрузки всех картинок и всей страницы
// reviews - отзывы " показать целиком"
// строка с текстом отзыва уже должна быть избавлена от пробелов вначале и в конце строки, а также быть без переводов строк
	var rV = { // работем с отзывами
		allRV:[],
		eInit: function(){ // инитим обработку отзывов
			rV.allRV = [];
			rV.allRV = jQuery('.js-reviews');

			if(rV.allRV.length > 0){
				if(jQuery(rV.allRV[0]).closest('.js-reviews-ldr').length > 0){
					rV.eOnLdr(jQuery(rV.allRV[0]).closest('.js-reviews-ldr'));
				}
				rV.eIfCut();

				if(jQuery(rV.allRV[0]).closest('.js-reviews-ldr').length > 0){
					rV.eOffLdr(jQuery(rV.allRV[0]).closest('.js-reviews-ldr'));
				}

			}
		},
		eIfCut: function(){ // смотрим не "какой толщины отзыв"
			jQuery('.js-reviews-ldr').removeClass('__off');
			jQuery.each(rV.allRV, function(i,v){
				if(jQuery(v).find('.js-reviews__P').length > 0){
					if(jQuery(v).find('.js-reviews__P').outerHeight() > 90){ // если больше 5 строк отзыва, то обрезаем
						jQuery(v).addClass('__enabled');
					}
				}
			})

			jQuery(document).off('click.openRV');
			jQuery(document).on('click.openRV', '.js-reviews .js-reviews__VM', rV.eOpenRV);
		},
		eOffLdr: function(elem){ // скрываем лоадер
			if(!elem.hasClass('__off')){
				elem.addClass('__off');
			}
		},
		eOnLdr:function(elem){ // ставим лоадер
			if(elem.hasClass('__off')){
				elem.removeClass('__off');
			}
		},
		eOpenRV: function(e){ // открываем большой отзыв
			jQuery(e.currentTarget).parents('.js-reviews').removeClass('__enabled');
		}
	}

	if(jQuery('.js-reviews').length > 0){ // если нужна обработка отзыва
		rV.eInit();
	}

	var msgPasteInput ={ // отправка контактов врача в сообщении, вставка текста сообщения (без карточки доктора ) в скрытый textArea"

		aAlltxt : [], // все контейнеры с сообщениями
		//можно и удалить "aAlltxt", если известно что поле ввода сообщения всегда пустое

		jOldHtml:[], // хранилище для верстки текста письма,
		jOldTxt:[], // хранилище для текста письма
		eInit: function(){ // инициализация


			//можно и удалить, если сообщения всегда будут пустые
			msgPasteInput.aAlltxt = jQuery('.js-msg-paste-Input__text'); // собираем поля с сообщениями

			if(jQuery('.js-msg-paste-Input__text').text().length > 0){ // если есть сообщения
				msgPasteInput.eStart();
			}
			// -----

			jQuery(document).off('focus.oF');
			jQuery(document).off('blur.oB');
			jQuery(document).on('focus.oF', '.js-msg-paste-Input__text', msgPasteInput.eFocus);
			jQuery(document).on('blur.oB', '.js-msg-paste-Input__text', msgPasteInput.eBlur);
		},
		eStart: function(){ // если сообщения уже заполнены при загрузке страницы, то копируем их в скрытые поля -
			//можно и удалить этот метод, если известно что поле ввода сообщения всегда пустое
			jQuery.each(msgPasteInput.aAlltxt, function(i,v){
				if(jQuery(v).parents('.js-msg-paste-Input').find('.js-msg-paste-Input__input').length > 0){
					jQuery(v).parents('.js-msg-paste-Input').find('.js-msg-paste-Input__input').val(jQuery(v).text());
				}
			})
		},
		eFocus: function(e){ // поймали начало редактирования ссобщения
			msgPasteInput.jOld = [];
			msgPasteInput.jOld = jQuery(e.currentTarget).html();
		},
		eBlur: function(e){ // посмотрели изменилось ли сообщение после окончания редактирования
			if(jQuery(e.currentTarget).html() !== msgPasteInput.jOld){
				msgPasteInput.jOld = jQuery(e.currentTarget).html();
				msgPasteInput.jOldTxt = jQuery(e.currentTarget).text();
				msgPasteInput.ePaste(e);
			}
		},
		ePaste:function(e){ // скопировали текст сообщения в скрытый инпут
			jQuery(e.currentTarget).parents('.js-msg-paste-Input').find('.js-msg-paste-Input__input').val(msgPasteInput.jOldTxt);
		}
	}

	if(jQuery('.js-msg-paste-Input').length > 0){
		msgPasteInput.eInit(); // инитим, если есть поле ввода сообщения на странице
	}

	//  init modal pUp
	if(jQuery('.js-pUp__openeer').length > 0 && jQuery('.js-pUp_m').length > 0){
		pUp_m.eInit();
	}
})


// global js
//  !!!!!!!!!!!!!!!!


// pUp - modal
var pUp_m = { // работаем с попапами модальных окон
	eInit: function(){
		jQuery(document).off('click.openPup');
		jQuery(document).on('click.openPup', '.js-pUp__openeer', pUp_m.eGetId);
		jQuery(document).off('click.closePup');
		jQuery(document).on('click.closePup', '.js-pUp_m .js-pUp__x', pUp_m.eHidePup);
		jQuery(document).off('click.closePupBG');
		jQuery(document).on('click.closePupBG', '.js-pUp_m .js-pUp_m__bg', pUp_m.eHidePup);
	},
	eGetId: function(e){ // берём  ID попапа из атрибута data
		// e.preventDefault();

		var curID = (jQuery(e.currentTarget).data().pupId) ? jQuery(e.currentTarget).data().pupId : false;

		if(curID){
			pUp_m.eShowPup(curID)
		}
	},
	eHidePup: function(e){ // скрываем ранее открытые попапы - модальных окон
		if(jQuery(e.currentTarget).hasClass('js-pUp_m__bg_not-x')){
			return;
		}

		jQuery('#body').removeClass('OVF_not');
		jQuery('.js-pUp_m__item').hide();
		jQuery('.js-pUp_m__item').removeClass('loading');
		jQuery('.js-pUp_m').hide();
	},
	eShowPup: function(id_pup){ // показываем нужный нам попап
		pUp_m.eHidePup(id_pup);
		jQuery('#body').addClass('OVF_not');
		jQuery('.js-pUp_m').show();
		jQuery('#'+id_pup).fadeIn();
	}
}
// END - pUp - modal

//  validate
var validI = {
	err1: "Электронная почта введена некорректно",
	err2: "Данный электронный адрес не зарегистрирован на портале",
	err3: "Неккоректный логин/пароль",
	eInit: function(){
		// валидация мыла
		jQuery(document).off('click.onValid');
		jQuery(document).on('click.onValid', '.js-valid .js-valid-e', validI.eValidE);

		// валидация формы авторизации
		jQuery(document).off('click.onValiduth');
		jQuery(document).on('click.onValiduth', '.js-valid .js-valid-auth', validI.eValidAuth);
	},
	eValidE: function(e){
		var curE_j = jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-i[name="email"]');

		if(curE_j.length > 0){
			var curVal = curE_j.val();
			var curReg = new RegExp("^[-A-z_.0-9]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$");
			var curIsVal = curReg.test(curVal);

			jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-err').html('');
			if(curIsVal){
				validI.eGetB(curVal,e);
			} else {
				jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-err').html(validI.err1);
			}
		}
	},
	eGetB: function(mail, e){
		var curIsGetB = false;
		jQuery.post( "/ajax/forgot_password", {email: mail}, function(arg){

			if(arg.status == "error"){
				curIsGetB = false;
			} else {
				if(arg.status == "success"){
					curIsGetB = true;
				}
			}

			if(curIsGetB){
				validI.eFoundMail(e);
			} else {
				validI.eNotFound(e);
			}
		})
			.fail(function(){
				console.log('ajax - fail');
			});
	},
	eFoundMail: function(e){
		pUp_m.eShowPup('pUp-sent-info-mail');
		validI.eBackFunc(1);
	},
	eNotFound: function(e){
		jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-err').html(validI.err2);
	},
	eBackFunc: function(arg){
		if(arg == 1){
			console.log('прошла валидация только мыла')
		}

		if(arg == 2){ // авторизация успешна
			console.log('авторизация успешна')
		}
	},
	//  validate form auth
	eValidAuth: function(e){
		var curE_j = jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-i[name="email"]');
		var curP_j = jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-i[name="password"]');

		if(curE_j.length > 0 && curP_j.length > 0){
			var curValE = jQuery(curE_j).val();
			var curValP = jQuery(curP_j).val();

			// var curRegE = new RegExp("^[A-z]+$");
			var curRegE = new RegExp("^[-A-z_.0-9]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$");	 // мыло
			var curRegP = new RegExp("^[-A-z_.0-9]"); // только цифры и латинские буквы

			var curIsValE = curRegE.test(curValE);
			var curIsValP = curRegP.test(curValP);

			jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-err').html('');
			if( curIsValE && curIsValP){
				validI.eGetBAuth(curValE, curValP, e);
			} else{
				validI.eNotAuth(e);
			}
		}
	},
	eGetBAuth: function(mail, pass, e){
		var curIsGetB = false;

		jQuery.post( "f/js/usr/e-mail.php", {adr: mail, pasw: pass}, function(arg){

			if(arg == 0){
				curIsGetB = false;
			} else {
				if(arg == 1){
					curIsGetB = true;
				}
			}

			if(curIsGetB){
				validI.eBackFunc(2);
			} else {
				validI.eNotAuth(e);
			}
		})
			.fail(function(){
				console.info('ajax - fail');
			});
	},
	eNotAuth:function(e){
		jQuery(e.currentTarget).parents('.js-valid').find('.js-valid-err').html(validI.err3);
	}
}
//  END validate