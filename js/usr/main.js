/*my js*/
$(document).ready(function(){
	var curDocH = $(window).height();
	var curT = [];
	function autoSetHeight(){
		clearTimeout(curT);
		curT = setTimeout(function(){
			curDocH = $(window).height();
			$('.body').css('min-height', curDocH);
		}, 100);
	}
	$(window).off('resize.res-w');
	$(window).on('resize.res-w', autoSetHeight);
	autoSetHeight();
/*change password*/
	if ($('.js-change-pass .js-change-pass__loop').length){
		$(document).on('click', '.js-change-pass .js-change-pass__loop',function showPass(e){
			var curP = $(e.currentTarget).parents('.js-change-pass');
			var curInput = curP.find('.js-change-pass__input');
			if(curInput.attr('type') == 'password'){
				curInput.attr('type', 'text');
				$(e.currentTarget).addClass('showPass');
			}else{
				curInput.attr('type', 'password');
				$(e.currentTarget).removeClass('showPass');
			}
		});
	}
/*=============
стилизация элементов форм*/
	if ($('.js-mail').length){
		$('.js-mail').off('click.open-msg');
		$('.js-mail').on('click.open-msg', '.js-mail__opener', openMsg);

		$('.js-mail').on('click.close-msg', '.js-mail__close', closeMsg);

		function openMsg(e){
			var curE = e.currentTarget;
             //alert(curE);
             
			if($('.b-msg__row_view').length)
				$('.b-msg__row_view').removeClass('b-msg__row_view');
               
			if(!$(curE).parents('.js-mail').hasClass('b-msg__row_view'))
			{
				$(curE).parents('.js-mail').addClass('b-msg__row_view');
				mfp.postRead($(curE).parents('.js-mail'));
			}
            
		}

		function closeMsg(e){
			var curEl = e.currentTarget;
			if($(curEl).parents('.js-mail').hasClass('b-msg__row_view'))
				$(curEl).parents('.js-mail').removeClass('b-msg__row_view');
		}
	};
    $('.menu').click(function(){
        
  $('.js-mail').off('click.open-msg');
});
	if($('.js-select').length)
		$('.js-select').styler();
	if($('.js-checkbox').length)
		$('.js-checkbox').styler();
	if($('.js-def-select').length)
		$('.js-def-select').styler();
/*карусель*/
	if($('#js-slider-bx').length) {
		$('#js-slider-bx').bxSlider({ // инициализация карусели
		  	// mode: 'fade',
		  	speed: 500,
			nextSelector: '#crsl_next',
			prevSelector: '#crsl_prew',
			nextText: '',
	  		prevText: '',
	  		infiniteLoop: false
		});
	}
/*маски*/
	if($('.js-date-i')){
		(function( factory ) {
			if ( typeof define === "function" && define.amd ){
				/*AMD. Register as an anonymous module.*/
				define([ "com/$-ui.min" ], factory );
			}else/*Browser globals*/
				factory( $.datepicker );
		}(function( datepicker ){
			if (datepicker){
				datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: 'Предыдущий',
					nextText: 'Следующий',
					currentText: 'Сегодня',
					monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
					monthNamesShort: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
					dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
					dayNamesShort: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
					dayNamesMin: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
					weekHeader: 'Sem.',
					firstDay: 1,
					showOtherMonths: true,
					selectOtherMonths: true,
					yearRange:"1940:+10",
					yearSuffix: ''};
				datepicker.setDefaults(datepicker.regional['ru']);
				return datepicker.regional['ru'];
			}
		}));
		$( ".js-date-i" ).mask(
			"99/99/9999",
			{
				completed: function(){
					var curD = $(this).val();
					$(this).closest('.box-fields').find('.js-date-i').datepicker( "setDate", curD);
				}

			}
		);
		$( ".js-time-input" ).mask(
			"99:99",
			{
				completed: function(){
					var curD = $(this).val();
					$(this).closest('.box-fields').find('.js-date-i').datepicker( "setDate", curD);
				}

			}
		);
	/*для целей на будущее*/
		$('.js-date-i.future').datepicker({
	      changeMonth: true,
	      changeYear: true,
	      minDate: "0",
	      maxDate: "365000",
	      showOn: "button",
	      dateFormat: "dd/mm/yy",
	      onSelect: function(i, v){
	      	$(this).parents('.js-date_box').find('.js-date-i').val(i);
	      }
	    });
	/*для дат из прошлого*/
		$('.js-date-i').datepicker({
	      changeMonth: true,
	      changeYear: true,
	      minDate: "-365000",
	      maxDate: "0",
	      showOn: "button",
	      dateFormat: "dd/mm/yy",
	      onSelect: function(i, v){
	      	$(this).parents('.js-date_box').find('.js-date-i').val(i);
	      }
	    });
		$(document).on('click', '.js-date-e', function(e){
	    	$(e.currentTarget).closest('.js-date_box').find('.ui-datepicker-trigger').click();
	    });
	}
	if($('.js-tel'))
		$( ".js-tel" ).mask("+7 (999) 999 99 99");
	if($('.js-avatar').length || $('.js-add-files').length)
		addF.eInit();
	if($('.js-calend-reg').length)
		clnd.eInit();
	if($('.js-socio').length)
		socio.eInit();
	if($('.js-sort .js-sort-table').length)
		SortTb.eInit();
	if($('.js-b-tgl-r').length)
		toogleRow.eInit();
	//  table - PAY - b-faq
	var payTimer;
	jQuery(document).on('click', '.js-t-pay-faq .js-t-pay-faq__e', function(e){
		var curPayE = e.currentTarget;
		var curPayInfo = jQuery(curPayE).parents('.js-t-pay-faq').find('.js-t-pay-faq__i');
		if( curPayInfo.length > 0){
			clearTimeout(payTimer);
			payTimer = setTimeout(function(){
				if(!jQuery(curPayE).parents('.js-t-pay-faq').hasClass('___active')){
					if(jQuery('.js-t-pay-faq.___active').length > 0){
						jQuery('.js-t-pay-faq.___active').find('.js-t-pay-faq__i').hide().css("opacity",0);
						jQuery('.js-t-pay-faq.___active').removeClass('___active')
					}

					curPayInfo.show();
					curPayInfo.animate({
						"opacity": 1
					},200);
					jQuery(curPayE).parents('.js-t-pay-faq').addClass('___active');
				} else {
					curPayInfo.hide().css("opacity",0);
					jQuery(curPayE).parents('.js-t-pay-faq').removeClass('___active');
				}
			},100);
		}
	})
	/*END table - PAY - b-faq*/
	/*марат комментировал*/
	/*if($('.js-emul-input').length){
		$(document).on('blur', '.js-emul-input', function(e){
			var curText = $(e.currentTarget).html();
			$(e.currentTarget).next('input').val(curText);
		})
	}*/
})
/*календарь*/
var clnd = { /*работа с календарём*/
	jEvCont:[],
	jParCont: [],
	nEvWidth:0,
	nMargin:0,
	jArr:[],
	eInit: function(){ /*инициализируем работу с календарём*/
		$('.js-clndr-e').off('click.ocE');
		$('.js-clndr-e').on('click.ocE', function(e){
		clnd.jEvCont = $('#js-event-desc');
			clnd.jParCont = $('.js-calend-reg');
			clnd.jArr = clnd.jEvCont.find('.js-event-arr');
			clnd.nEvWidth = clnd.jEvCont.outerWidth();
			clnd.nMargin = (clnd.jParCont.data().typeCalend == 1 || clnd.jParCont.data().typeCalend == 7) ? 50 : 7;
			if($(e.currentTarget).parent('.js-clndr-b').hasClass('b-calend__event_active')){
				$(e.currentTarget).parent('.js-clndr-b').toggleClass('b-calend__event_active');
				clnd.jEvCont.toggleClass('b-calend__event_content_active');
			}else{
				$('.js-clndr-b.b-calend__event_active').removeClass('b-calend__event_active');
				$(e.currentTarget).parent('.js-clndr-b').toggleClass('b-calend__event_active');
				clnd.eOpenD(e,  $(e.currentTarget).parents('.js-clndr-b').data().idM);
			}
		});

		$(document).off('click.reset-clnd');
		$(document).off('click.reset-clndW');
		$(document).on('click.reset-clnd', '.js-nav-calend .js-nav-calend__nav-i', clnd.eCloseE);
		$(document).on('click.reset-clndW', '.js-nav-calend .js-nav-calend__dWeek', clnd.eCloseE);
	},
	eCloseE: function(){/*закрыть описание мероприятия*/
		$('.js-clndr-b').removeClass('b-calend__event_active');
		if(clnd.jEvCont.length)
			clnd.jEvCont.removeClass('b-calend__event_content_active');
		$(document).off('click.closeNOTvypadashka'); /*убиваем обработчик клика мимо открытого попапа*/
	},
	eOpenD: function(e, idJ){/*открываем описание мероприятия*/
		/*вставляем данные в выпадашку*/
		clnd.jEvCont.find('.b-calend-e__title').html($(e.currentTarget).parent('.js-clndr-b').find('.target-name').val());
		clnd.jEvCont.find('.__time').html($(e.currentTarget).parent('.js-clndr-b').find('.target-time').val());
		clnd.jEvCont.find('.__date').html($(e.currentTarget).parent('.js-clndr-b').find('.target-date').val());
		clnd.jEvCont.find('.__desc').html($(e.currentTarget).parent('.js-clndr-b').find('.target-text').val());
		/* -----
		считаем где появляться выпадашке*/
		var curW = $(e.currentTarget).outerWidth(); /*ширина кнопки с мероприятием*/
		var curH = $(e.currentTarget).outerHeight(); /*высота кнопки с мероприятием*/
		var curL = $(e.currentTarget).offset().left; /*отступ слева - кнопки с мероприятием*/
		var curt = $(e.currentTarget).offset().top; /*отступ сверзу -  с мероприятием*/
		var curLP = clnd.jParCont.offset().left; /*ширина оттуп слева - родителя кнопки с мероприятием*/
		var curTP = clnd.jParCont.offset().top; /*ширина оттуп сверху - родителя кнопки с мероприятием*/
		var curWP = clnd.jParCont.outerWidth(); /*ширина  родителя кнопки с мероприятием*/
		var curHP = clnd.jParCont.outerHeight();  /*высота родителя кнопки с мероприятием*/
		var curHD = clnd.jEvCont.outerHeight(); /*высота контейнера с мероприятием*/
		var curP = $(e.currentTarget).parents('.js-clndr-b'); /*родитель кнопки мероприятия */
		/*определяем координаты ЛЕВого края подсказки*/
		var curCoordL; /*координаты X папапа мероприятия*/
		var curArrL; /*координаты стрелочки;*/
		if(curP.hasClass('b-calend__event_active-revert'))
			curP.removeClass('b-calend__event_active-revert');
		if (clnd.nEvWidth/2 > (curL - curLP)  + curW/2 - clnd.nMargin)
			curCoordL = clnd.nMargin;
		else{
			if(clnd.nEvWidth/2 > (curLP+curWP) - (curL+curW/2))
				curCoordL = curWP - clnd.nEvWidth - 9;
			else
				curCoordL = (curL - curLP)+curW/2 -(clnd.nEvWidth/2);
		}
		/*определяем координаты ВЕРХнего края подсказки*/
		var curCoordT;
		var deltaTop = curTP+curHP +  - curH - curt-4;
		if(deltaTop < 280){
			curCoordT = curt-curTP-4-curHD;
			curP.addClass('b-calend__event_active-revert');
			clnd.jEvCont.addClass('__revert');
		}
		else
		{
			if(clnd.jEvCont.hasClass('__revert'))
				clnd.jEvCont.removeClass('__revert');
			curCoordT = (curt-curTP) + curH + 4;
		}
		/*определяем координаты стрелочки*/
		curArrL = curL - curLP -curCoordL+ curW/2;
		/*проставляем найденные значения*/
		clnd.eSetLeft(curCoordL, curArrL);
		clnd.eSetTop(curCoordT);
		/* показываем подсказку*/
		clnd.jEvCont.addClass('b-calend__event_content_active');
		$(document).on('click.closeNOTvypadashka', function(e){ /*создаём обработчик клика мимо открытого попапа*/
			if($(e.target).closest('#js-event-desc').length <= 0  /*если клик мимо открытого попапа*/
				&& $('#js-event-desc').hasClass('b-calend__event_content_active')
				&& $(e.target).closest('.js-clndr-b').length <= 0 )
					clnd.eCloseE();
		});
	},
	eSetLeft: function(nLeft, nArrL){/*выставляем левую координату описания мероприятия*/
		clnd.jEvCont.css({'left': nLeft});
		clnd.jArr.css({'left': nArrL});
	},
	eSetTop:function(nTop){/*выставляем верхнюю координату описания мероприятия*/
		clnd.jEvCont.css({'top': nTop });
	}
}
/*добавление файла*/
var addF = {/*добавление файла и создание превью*/
 	nIndex:0,
 	nPar:[],
 	jAvaPar: [],
 	eInit: function(){/*инициалищация*/
 		$(document).off('click.emulcl');
 		$(document).off('click.delF');
 		$(document).off('change.addF');
		$(document).on('click.emulcl', '.js-add-f', addF.eCreatInput);
 		$(document).on('click.delF', '.jd-add-f_delete', addF.eDeleteFile);
 		$(document).on('change.addF','.js-add-f_i', function(){
		    addF.eGetUrl(this);
		});
		$(document).off('click.emulava');
		$(document).on('click.emulava', '.js-avatar .js-avatar_init', addF.eInitChangeAva);
		$(".js-avatar .b-add-f-list__i-delete").click(function(event){ 
		    event.stopPropagation(); 
		});
		$(document).off('change.chlava');
		$(document).on('change.chlava', '.js-avatar .js-avatar__form-i', function(){
			addF.eChangeAva(this)
		});
 	},
 	eCreatInput: function(e){/*создаём инпут в скрытой форме для выбора файла и эмулируем клик по нему, для выбора файла*/
 		var curStIn="";
 		addF.nPar = $(e.currentTarget).parents('.js-add-files');
		if(!$(e.currentTarget).parents('.js-add-files').find('[data-id_f="'+addF.nIndex+'"]').length){
			if(pageVariables.controllerMethod=='/profile/institutionPhotos')
	 			curStIn = '<input name="filename" type="file" accept="image/*" class="js-add-f_i" data-id_f="'
		 		+addF.nIndex
		 		+'" style="margin-left:300px;display:none;">';
		 	else
		 		curStIn = '<input name="filename" type="file" accept="image/*,application/*" class="js-add-f_i" data-id_f="'
		 		+addF.nIndex
		 		+'" style="margin-left:300px;display:none;">';
	 		addF.nPar.append(curStIn);
 		}
 		$(e.currentTarget).parents('.js-add-files').find('[data-id_f="'+addF.nIndex+'"]').click();
 	},
 	eGetUrl: function(input,callback){/*забираем адрес картинки и создаём превьюху файла*/
 		if (input.files && input.files[0]){
	        var reader = new FileReader();
	        var curElem;
	        var file = input.files[0];
			reader.onload = function (e) {/*как только файл загрузился, выводим превьюшку*/
	        	curElem = '<li class="b-add-f-list__i js-add-li" data-id_idi="'
		        	+addF.nIndex
		        	+'" data-img-path="">'
	    			+'<img src=""/>'
	    			+'<input type="hidden" class="_hidden-id" name="images[]"/>'
	    			+'<span class="b-add-f-list__i-delete jd-add-f_delete" data-pup-id="pUp-del-document"></span></li>'
	        	$(addF.nPar).find('.js-add-list .js-add-f').before(curElem);
	        	if(pageVariables.controllerMethod=='/profile/post'
	        	   || pageVariables.controllerMethod=='/profile/resendPost'
	        	   || pageVariables.controllerMethod=='/profile/respondPost'
	        	   || pageVariables.controllerMethod=='/profile/resend_specialist')
						mfp.addPostDocument(input,addF.nIndex);
				else
					mfp.addDocument(input,addF.nIndex);
	        	addF.nIndex++;
	        }
	        reader.readAsDataURL(file);
  	  	}
 	},
 	eDeleteFile: function(e){/*удаляем файл*/
 		var curEd = e.currentTarget;
 		if(pageVariables.controllerMethod != '/profile/post' && pageVariables.controllerMethod != '/profile/resend_specialist')
 			pUp_m.eGetId(curEd,'Удалить документ?');
 		else
 		{
	 		var curIndFile = $(curEd).parents('.js-add-li').data().id_prew;
			$(curEd).parents('.js-add-files').find('[data-id_f="'+curIndFile+'"]').remove();
	 		$(curEd).parent().remove();
	 	}
 	},
 	eInitChangeAva: function(e)
 	{/*меняем аватар*/
   // alert('меняем аватар');
 		addF.jAvaPar = $(e.currentTarget).parents('.js-avatar');
		var curInput = addF.jAvaPar.find('.js-avatar__form-i');
		curInput.click();
 	},
 	eChangeAva: function(inp)
 	{/*меняем аватар*/
    //alert('меняем аватар2');
 		if (inp.files && inp.files[0]){
 			var readerAva = new FileReader();
			readerAva.onload = function (e)
			{/*показываем картинку вместо кнопки добавления*/
				addF.jAvaPar.find('.js-avatar__img').addClass('notEmpty');
			}
			readerAva.readAsDataURL(inp.files[0]);
			mfp.changeAva(inp);
 		}
 	}
}
/*попап выбора соцсети*/
var socio = {
	eInit: function(){ /*инициализация*/
		$(document).off('click.socioPup');
		$(document).on('click.socioPup', '.js-socio-opener',socio.eViewP);
		$(document).off('click.socioOK');
		$(document).on('click.socioOK', '.js-socio-ok',socio.eCloseP);
	},
	eViewP: function(e){ /*показываем попап*/
		$(e.currentTarget).parents('.js-socio').find('.js-socio-pUp').addClass('active');
		$(e.currentTarget).addClass('pUp-act');
		$(document).on('click.closeNOTdiv', function(e){ /*создаём обработчик клика мимо открытого попапа*/
			if($(e.target).closest('.js-socio-pUp').length <= 0  /*если клик мимо открытого попапа*/
				&& $('.js-socio-pUp').hasClass('active'))
					socio.eCloseP();
		});
	},
	eCloseP: function(){ /*скрываем попап*/
		$('.js-socio-pUp').removeClass('active');
		$(document).off('click.closeNOTdiv'); /*убиваем обработчик клика мимо открытого попапа*/
	}
}
//  table sort
var SortTb = { // СОРТИРОВКА ТАБЛИЦЫ ПО ФИЛЬТРАМ ВНЕ ТАБЛИЦЫ
	aIdFilters: [],
	eInit: function(){ // инит сортировки таблиц
		// сортировка сделана на два столбца
		// если будет необходисоть добавить ешё фильтров, нужно будет сделать по аналогии
		// добавить одинаковые класс в хедер таблицы и в id значения фильтра
		// а также разблокировать возможность сортировки в методе "eInitPlugin" - в месте инициализации плагина сортировки
		// jQuery('#js-sort-table').tablesorter({
		// 		headers: { 
		//				     1: {  // указать номер столбца (нумерация начинается с "0" )
		// 									sorter: false  // поставить в "sorter" тип сортировки или вообще убрать - то будет сортировка по алфавиту
		//								}
		//		}
		//	}) 
		var curFiltersItems = [];

		curFiltersItems = $('.js-sort').find('.js-sort-table__param');
		if(curFiltersItems.length > 0){ // собираем массив ID - фильтров
			SortTb.aIdFilters = [];
			jQuery.each(curFiltersItems, function(i,v){
				if(v.id){
					SortTb.aIdFilters.push(v.id);
				}
			})
		}

		SortTb.eInitPlugin();
		SortTb.eVARevent();
	},
	eVARevent: function(){ // навешиваем события
		$(document).off('click.clk-param');
		$(document).on('click.clk-param', '.js-sort-table__param', SortTb.eClkParamFilter);
    
    $("#js-sort-table").bind("sortEnd", SortTb.eSortEnd);
	},
	eSortEnd: function(e, table){ // действия после окончания сортировки
		jQuery.each(SortTb.aIdFilters ,function(i,v){
			var curCl = '.'+v;
			var curIDd = '#'+v;

			if($(e.currentTarget).find(curCl).length > 0){

				if($(e.currentTarget).find(curCl).hasClass('headerSortDown')){
					if(!$(curIDd).parents('.js-sort-table__param-p').hasClass('__down')){
						if($(curIDd).parents('.js-sort-table__param-p').hasClass('__up')){
							$(curIDd).parents('.js-sort-table__param-p').removeClass('__up');
						}
						$(curIDd).parents('.js-sort-table__param-p').addClass('__down');
					}
					if(!$(curIDd).parents('.js-sort-table__param-p').hasClass('__active')){
						$(curIDd).parents('.js-sort-param').find('.js-sort-table__param-p').removeClass('__active');
						$(curIDd).parents('.js-sort-table__param-p').addClass('__active')
					}
				}

				if($(e.currentTarget).find(curCl).hasClass('headerSortUp')){
					if(!$(curIDd).parents('.js-sort-table__param-p').hasClass('__up')){
						if($(curIDd).parents('.js-sort-table__param-p').hasClass('__down')){
							$(curIDd).parents('.js-sort-table__param-p').removeClass('__down');
						}
						$(curIDd).parents('.js-sort-table__param-p').addClass('__up');
					}

					if(!$(curIDd).parents('.js-sort-table__param-p').hasClass('__active')){
						$(curIDd).parents('.js-sort-param').find('.js-sort-table__param-p').removeClass('__active');
						$(curIDd).parents('.js-sort-table__param-p').addClass('__active')
					}
				}

			}
		});
	},
	eClkParamFilter: function(e){ // эмулируем клик по хедеру таблицы при клике по фильтру
		e.preventDefault();

		if(!$(e.currentTarget).parents('.js-sort-table__param-p').hasClass('__active')){
			$(e.currentTarget).parents('.js-sort-param').find('.js-sort-table__param-p').removeClass('__active')
			$(e.currentTarget).parents('.js-sort-table__param-p').addClass('__active');
		}

		if(e.currentTarget.id){
			var curClassElem = '.'+e.currentTarget.id;
			$("#js-sort-table").find(curClassElem).click();				
		}
	},
	eInitPlugin: function(){ // инициализируем плагин сортировки
		jQuery.tablesorter.addParser({  
			// добавляем обработку сортировки даты с разделителем точкой, если дата будет другого формата, 
			// то необходимо или изменить этот метод или указать метод сортировки зашитый в плагин
			// иначе будет сортироваться по алфавиту или вообще валить ошибки в логи
		    id: 'pointDate', 
		    is: function(s) { 
		        return /^\d{1,2}[\.\/-]\d{1,2}[\.\/-]\d{4}$/.test(s);
		    }, 
		    format: function(s) { 
		         var e = s.split('.');
								return $.tablesorter.formatFloat((s != "") ? new Date(e[2]+'/'+e[1]+'/'+e[0]).getTime() : "0");
								new Date(s.replace(new RegExp(/-/g),"/")).getTime();
		    }, 
		    type: 'numeric' 
   		}); 

		$('#js-sort-table').tablesorter({  // сама инициализация плагина сортировки
	        cancelSelection: false,
	        sortList: [[0,0]],
	        dateFormat : "mmddyyyy",
	        headers: { 
	            1: {sorter: false}, // отключаем сортировку в столбцах
	            2: {sorter: false},
	            3: {sorter: false},
	            4:  {sorter: 'pointDate' },
	            5: {sorter: false}  
	        }
	    }); 
	}
}
//  open toogle-row
var toogleRow = { // разворачиваем строки под "открывашкой""
	eInit:function(){
		$(document).off('click.open-tgr');
		$(document).on('click.open-tgr', '.js-b-tgl-r .js-b-tgl-r__o', toogleRow.eOpenRow);
		$(document).off('click.close-tgr');
		$(document).on('click.close-tgr', '.js-b-tgl-r .js-b-tgl-r__x', toogleRow.eCloseRow);
	},
	eOpenRow: function(e){ // разворачиваем строки
		
		e.preventDefault();

		var curE = e.currentTarget;

		if($(curE).parents('.js-b-tgl-r__i').find('.js-b-tgl-r__c').length > 0){
			if(!$(curE).parents('.js-b-tgl-r__i').hasClass('__t-on')){
				toogleRow.eCloseRow(e);
				$(curE).parents('.js-b-tgl-r__i').addClass('__t-on');
			}
		}
	},
	eCloseRow:  function(e){ // сворачиваем строки
		var curE = e.currentTarget;
		$(curE).parents('.js-b-tgl').find('.js-b-tgl-r__i.__t-on').removeClass('__t-on');
	}
}

$(window).on('load', function(){ // дожидаемся окончания загрузки всех картинок
// reviews - отзывы " показать целиком" 
pUp_m.eInit();
	if($('.js-reviews').length) // если нужна обработка отзыва
		rV.eInit();
	if($('.js-msg-paste-Input').length)
		msgPasteInput.eInit(); // инитим, если есть поле ввода сообщения на странице
	//if($('.js-pUp__openeer').length && $('.js-pUp_m').length)
		
})

// строка с текстом отзыва уже должна быть избавлена от пробелов вначале и в конце строки, а также быть без переводов строк
var rV = { // работем с отзывами
	allRV:[],
	eInit: function(){ // инитим обработку отзывов
		rV.allRV = [];
		rV.allRV = $('.js-reviews');

		if(rV.allRV.length){
			if($(rV.allRV[0]).closest('.js-reviews-ldr').length)
				rV.eOnLdr($(rV.allRV[0]).closest('.js-reviews-ldr'));
			rV.eIfCut();

			if($(rV.allRV[0]).closest('.js-reviews-ldr').length)
				rV.eOffLdr($(rV.allRV[0]).closest('.js-reviews-ldr'));
			
		}
	},
	eIfCut: function(){ // смотрим на "какой толщины отзыв"
		$('.js-reviews-ldr').removeClass('__off');
		jQuery.each(rV.allRV, function(i,v){
			if($(v).find('.js-reviews__P').length)
				if($(v).find('.js-reviews__P').outerHeight() > 90) // если больше 5 строк отзыва, то обрезаем
					$(v).addClass('__enabled');
		})

		$(document).off('click.openRV');
		$(document).on('click.openRV', '.js-reviews .js-reviews__VM', rV.eOpenRV);
	},
	eOffLdr: function(elem){ // скрываем лоадер
		if(!elem.hasClass('__off'))
			elem.addClass('__off');
	},
	eOnLdr:function(elem){ // ставим лоадер
		if(elem.hasClass('__off'))
			elem.removeClass('__off');
	},
	eOpenRV: function(e){ // открываем большой отзыв
		$(e.currentTarget).parents('.js-reviews').removeClass('__enabled');
	}
}
var msgPasteInput ={ // отправка контактов врача в сообщении, вставка текста сообщения (без карточки доктора ) в скрытый textArea"
		
	aAlltxt : [], // все контейнеры с сообщениями
	//можно и удалить "aAlltxt", если известно что поле ввода сообщения всегда пустое
	
	jOldHtml:[], // хранилище для верстки текста письма,
	jOldTxt:[], // хранилище для текста письма
	eInit: function(){ // инициализация
		

		//можно и удалить, если сообщения всегда будут пустые
		msgPasteInput.aAlltxt = jQuery('.js-msg-paste-Input__text'); // собираем поля с сообщениями
		
		if(jQuery('.js-msg-paste-Input__text').text().length > 0){ // если есть сообщения
			 msgPasteInput.eStart();
		}
		// ----- 

		jQuery(document).off('focus.oF');
		jQuery(document).off('blur.oB');
		jQuery(document).on('focus.oF', '.js-msg-paste-Input__text', msgPasteInput.eFocus);
		jQuery(document).on('blur.oB', '.js-msg-paste-Input__text', msgPasteInput.eBlur);
	},
	eStart: function(){ // если сообщения уже заполнены при загрузке страницы, то копируем их в скрытые поля - 
		//можно и удалить этот метод, если известно что поле ввода сообщения всегда пустое
		jQuery.each(msgPasteInput.aAlltxt, function(i,v){
			if(jQuery(v).parents('.js-msg-paste-Input').find('.js-msg-paste-Input__input').length > 0){
				jQuery(v).parents('.js-msg-paste-Input').find('.js-msg-paste-Input__input').val(jQuery(v).text());
			}
		})
	},
	eFocus: function(e){ // поймали начало редактирования ссобщения
		msgPasteInput.jOld = [];
		msgPasteInput.jOld = jQuery(e.currentTarget).html();
	},
	eBlur: function(e){ // посмотрели изменилось ли сообщение после окончания редактирования
		if(jQuery(e.currentTarget).html() !== msgPasteInput.jOld){
			msgPasteInput.jOld = jQuery(e.currentTarget).html();
			msgPasteInput.jOldTxt = jQuery(e.currentTarget).text();
			msgPasteInput.ePaste(e);
		}
	},
	ePaste:function(e){ // скопировали текст сообщения в скрытый инпут
		jQuery(e.currentTarget).parents('.js-msg-paste-Input').find('.js-msg-paste-Input__input').val(msgPasteInput.jOldTxt);
	}
}
// pUp - modal
var pUp_m = { // работаем с попапами модальных окон
	eInit: function(){
		$(document).off('click.closePup');
		$(document).on('click.closePup', '.js-pUp_m .js-pUp__x', pUp_m.eHidePup);
		$(document).off('click.closePupBG');
        $(document).on('click.closePupBG', '.js-pUp_m .js-pUp_m__bg', pUp_m.eHidePup);
		$(document).on('click.metro', '.metro_wrapper', pUp_m.eHidePup);
	},
	eGetId: function(e,msg){ // берём  ID попапа из атрибута data
        var curID = (typeof $(e).data('pup-id') != 'undefined') ? $(e).data('pup-id') : false;
		var postID = (typeof $(e).data('post-id') != 'undefined') ? $(e).data('post-id') : false;
		if(curID)
		{   
        
        if(curID=='pUp-del' )
            {
                
              $('._btn-del').attr('onclick','window.location.pathname=' + '\'/profile/deleteposts/'+postID +'\'');
            } 
            
			if(curID=='pUp-del-post' || curID=='pUp-finaldel-post' || curID=='pUp-del-target')
			{
				$('.pUps__item.delete-popup').attr('id',curID);
				$('#'+curID).find('._btn-del-target').data('target-id',$(e).parent('.b-msg-operation').find('._hidden-id').val());
			}
			if(curID=='pUp-del-branch' || curID=='pUp-del-liked-institution'
			   || curID=='pUp-del-liked-specialist' || curID=='pUp-del-liked-prep' || curID=='pUp-del-liked-article'
			   || curID=='pUp-del-soc-net' || curID=='pUp-del-document' || curID=='pUp-del-institution-specialist')
				{
					$('.pUps__item.delete-popup').attr('id',curID);
					$('#'+curID).find('._btn-del-target').data('target-id',$(e).prev('._hidden-id').val());
				}
			if(curID=='pUp-del-user-ava')
				$('.pUps__item.delete-popup').attr('id',curID);
			if(curID=='pUp-view-ORDER')
			{
				var servicePayPriceTr = $(e).parents('._service_pay_price_tr');
                var payPopup = $('#'+curID);
				window.payPopup = $('#'+curID);
				mfp.payAutoFill(payPopup);
				payPopup.find('._name_cell').html($(e).parents('._service_pay_tr').find('._name_cell').html());
				payPopup.find('._months_cell').html(servicePayPriceTr.find('._months_cell').html());
				var pay_price = parseInt(servicePayPriceTr.find('._price_cell').html());
                var special_pay_price = parseInt(servicePayPriceTr.find('._special_price_cell').html());
				window.special_pay_price = parseInt(servicePayPriceTr.find('._special_price_cell').html());
				// var pay_price_nds = pay_price*0.18+pay_price;
                $(".myclass").val("1");
				var pay_price_nds = 0;
				// payPopup.find('._price_cell').html(pay_price);
				payPopup.find('._price_cell').html(special_pay_price);
				payPopup.find('._special_price_cell').html(special_pay_price);
				payPopup.find('._price_input').html(special_pay_price);
				payPopup.find('._price_input_nds').html(pay_price_nds);
			}
			pUp_m.eShowPup(curID,msg);
		}
	},
	eHidePup: function(redirect){ // скрываем ранее открытые попапы - модальных окон
		$('#body').removeClass('OVF_not');
		$('.js-pUp_m__item').hide();
		$('.js-pUp_m__item').removeClass('loading');
		$('.js-pUp_m').hide();
        $('#metro_wrapper').toggle();
		var redirectPath=$('.pUps.js-pUp_m').data('redirect');
		if(redirect !='not' && redirectPath != '_')
			window.location.pathname=redirectPath;
	},
	eHidePupDel: function(e){ // скрываем ранее открытые попапы - модальных окон
		$('#body').removeClass('OVF_not');
		$('.js-pUp_m__item').hide();
		$('.js-pUp_m__item').removeClass('loading');
		$('.js-pUp_m').hide();
		var btnDel=$(e);
		var targetId=btnDel.data('target-id');
		if(targetId != 'not-id')
		{
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-branch')
				mfp.branchDelete(targetId);
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-post')
				mfp.postTargetDelete(targetId,'/profile/deletePost');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-finaldel-post')
				mfp.postTargetDelete(targetId,'/profile/deletePostFinal');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-target')
				mfp.postTargetDelete(targetId,'/profile/deleteTarget');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-liked-institution')
				mfp.likedDelete(targetId,'/profile/deleteLikedInstitution');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-liked-specialist')
				mfp.likedDelete(targetId,'/profile/deleteLikedSpecialist');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-institution-specialist')
				mfp.likedDelete(targetId,'/profile/institution_specialists_delete');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-liked-prep')
				mfp.likedDelete(targetId,'/profile/deleteLikedPrep');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-liked-article')
				mfp.likedDelete(targetId,'/profile/deleteLikedArticle');
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-soc-net')
				mfp.userSocNetDelete(targetId);
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-document')
				mfp.deleteDocument(targetId);
		}
		else
			if(btnDel.parent('.pUps__item').attr('id')=='pUp-del-user-ava')
				mfp.avaDelete();
	},
	eShowPup: function(id_pup,msg){ // показываем нужный нам попап
		pUp_m.eHidePup('not');
		$('#body').addClass('OVF_not');
		if(id_pup != 'pUp-logout' && id_pup != 'pUp-del' && id_pup != 'pUp-view-ORDER'){
           $('#'+id_pup+' p').html(msg); 
        }
			
		$('.js-pUp_m').show();
		$('#'+id_pup).fadeIn(); 
		return false;
	}
}

function download_bill(){
	html2canvas($('#pUp-view-ORDER'), {
		onrendered: function (canvas) {
			var img = canvas.toDataURL('image/png').replace("image/png", "image/octet-stream");

			window.location.href = img;
		}
	});
}



$(".myclass").keyup(function(){
     str = $( this ).val();
     // alert(  str );
      price=str*window.special_pay_price;
      window.payPopup.find('._price_input').html(price);
       payPopup.find('._special_price_cell').html(price);
});


//$( ".myclass" ).change(function() {

   //  alert( "Handler for .change() called." );
  //   alert( window.special_pay_price );
    // str = $( this ).val();
     // alert(  str );
    //  price=str*window.special_pay_price;
    //  window.payPopup.find('._price_input').html(price);
    //   payPopup.find('._special_price_cell').html(price);
    //})