// YA - map - js
$(document).ready(function(){
	if($('.js-map.js-map-box').length > 0){
		pMap.eInit(); // инитим отображение на карте
	}
})
//  END - jREADY
var pMap = { // работа с картой
	jMap: [],
	jPMark: [],
	aCoord: [55.753559, 37.609218],
	isMap: false,
	nZoom: 12,
	jTimer:[],
	eInit:function(){
		if($('.js-map-lon').val() && $('.js-map-lat').val()){ // если есть сохранённые координаты
			pMap.eLookCoord();
		} else {
			pMap.eValidate(pMap.aCoord);
		}
	},
	eLookCoord: function(){ // смотрим координаты после загрузки страницы
		pMap.aCoord = [parseFloat($('.js-map-lat').val()), parseFloat($('.js-map-lon').val())];
		pMap.nZoom = 16;
		pMap.eValidate(pMap.aCoord); // проверяем полученные координаты из инпутов
	},
	eReadyMap: function(){ // смотрим, готово ли апи карт
		ymaps.ready(pMap.eCreateMap);
	},
	eValidate: function(coord){ // смотрим правильность введённых координат
		if(jQuery.isNumeric(coord[0]) && jQuery.isNumeric(coord[1])){
			pMap.eReadyMap();
		}
	},
	eCreateMap: function(){ // создаём экземпляр карты
		pMap.jMap = new ymaps.Map("b-map", {
			center: pMap.aCoord,
			zoom: pMap.nZoom
		}); 
		pMap.jMap.controls.add('smallZoomControl'); // добавляем контролл зума
		pMap.jMap.events.add('click', pMap.eGetCoords);

		pMap.eCreatPlaceMArk();
	},
	eGetCoords: function(e){ // берём координаты клика
		clearTimeout(pMap.jTimer); // защищаемся от "задрота" с множественным кликом
		pMap.jTimer = setTimeout(function(){
			pMap.ePrintCoords(e.get('coords'));
		},500);
	},
	ePrintCoords: function(coord){ // записываем координаты в инпут
		$('.js-map-lat').val(parseFloat(coord[0]));
		$('.js-map-lon').val(parseFloat(coord[1]));
		pMap.aCoord = coord;
		pMap.eDeleteMark();
		pMap.eCreatPlaceMArk();
	},
	eCreatPlaceMArk: function(){ // создаём метку и добавляем на карту
		pMap.jPMark = new ymaps.Placemark(pMap.aCoord);
		pMap.jMap.geoObjects.add(pMap.jPMark); // добавляем метку
		pMap.jMap.panTo([pMap.aCoord]); // плавно перемещаем к метке

		pMap.isMap = true; // помечаем что карта создана
	},
	eDeleteMark: function(){ // удаляем метку с карты
		if(pMap.isMap){
			pMap.jMap.geoObjects.remove(pMap.jPMark);
		}
	}
}