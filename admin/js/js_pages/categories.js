/***  Удаления/изменения/добавления Категорий ****/
jQuery(document).ready(function($) {
	
	$('.cat_dell').on('click', function() {
		var $this = $(this),
				idCat = $this.parents('li').attr('id');
		id = $(this).attr('attr-id');
		$( "#dialog-confirm" ).dialog({
      resizable: false,
      height:200,
      modal: true,
      buttons: {
        "Да": function() {
        	//$.ajax({
				//	  url: 'ajax.php',
				//	  data: {users:idCat}
				//	}).done(function(data){
				//		if(data)  {
				//    	$this.closest('li').remove();
				//    	$( "#dialog-message" ).dialog({
				//	      modal: true,
				//	      buttons: {
				//	        Ok: function() {
				//	          $( this ).dialog( "close" );
				//	        }
				//	      }
				//	    });
				//    } else  {
				//    	alert('Попробуйте ещё раз')
				//    }
				//	})
			window.location.href = '/cp/articles_category_delete/' + id;
        	
        	$( this ).dialog( "close" );
          
        },
        "Нет": function() {
          $( this ).dialog( "close" );
        }
      }
    });
	})

	//$('.cat_remove').on('click', function() {
	//	var $this = $(this),
	//			$parent = $this.closest('li');
	//	$parent.find('.car_title').hide();
	//	$parent.find('input[type="hidden"]').attr('type', 'text')
	//});
	$('.sub_cat').on('click',function() {
		var $this = $(this),
				$parent = $this.closest('li'),
				idCat = $this.parents('li').attr('id'),
				valCat = $this.parents('li').find('.form-control').val();
		id = $(this).attr('attr-id');
		$( "#dialog-confirm_2" ).dialog({
      resizable: false,
      height:200,
      modal: true,
      buttons: {
        "Да": function() {
        	//$.ajax({
				//	  url: 'ajax.php',
				//	  data: {users:idCat, valCat:valCat}
				//	}).done(function(data){
				//		if(data)  {
				//    	$parent.find('input[type="text"].form-control')
				//    				.attr('type', 'hidden')
				//    				.val('data');
				//    	$parent.find('.car_title')
				//    			.css('display', 'inline-block')
				//    			.html(data);
				//    	$( "#dialog-message_2" ).dialog({
				//	      modal: true,
				//	      buttons: {
				//	        Ok: function() {
				//	          $( this ).dialog( "close" );
				//	        }
				//	      }
				//	    });
				//    } else  {
				//    	alert('Попробуйте ещё раз')
				//    }
				//	})
			window.location.href = '/cp/articles_category_edit/' + id;
        	
        	$( this ).dialog( "close" );
          
        },
        "Нет": function() {
          $( this ).dialog( "close" );
        }
      }
    });
	});

	$('.add_cat').on('click', function(){
		$('.add_categ').show();
	});
	$('.add_new_categ').on('click', function () {
		var $this = $(this),
				$parent = $this.closest('.add_categ'); 
		var val = $parent.find('input.form-control').val();

		if(val.length < 2) 
			return false;

		$( "#dialog-confirm_3" ).dialog({
      resizable: false,
      height:200,
      modal: true,
      buttons: {
        "Да": function() {
        	$.ajax({
					  url: 'ajax.php',
					  data: {name_cat:val}
					}).done(function(data){
						if(data)  {
				    	$('.add_categ').hide();

				    	var result = "<li class='list-group-item'><span class='icon_event cat_dell'><span class='icon_event_title'>Удалить</span></span><span class='icon_event cat_remove'><span class='icon_event_title'>Изменить</span></span><span class='car_title'>" + data + "</span><input type='hidden' value='Категория 6' class='form-control'><input type='button' class='sub_cat btn btn-warning' value='Изменить'></li>";
				    	$parent.find('input.form-control').val('');
				    	$(".categories").prepend(result);

				    	$( "#dialog-message_3" ).dialog({
					      modal: true,
					      buttons: {
					        Ok: function() {
					          $( this ).dialog( "close" );
					        }
					      }
					    });
				    } else  {
				    	alert('Попробуйте ещё раз')
				    }
					})
        	
        	
        	$( this ).dialog( "close" );
          
        },
        "Нет": function() {
          $( this ).dialog( "close" );
        }
      }
    });
	})

});

	/***  Удаления/изменения/добавления Категорий ****/