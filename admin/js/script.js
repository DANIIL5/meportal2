function preventDef(v){
	v = v || event;
	v.preventDefault ? v.preventDefault() : (v.returnValue = false);
}
/*
 * 1. выподающее подменю
 * 2. Удаления пользователя
 * 3. date input - выбор даты - jquery ui
 * 4. input file
 * 5. проверка форм на заполненость полей
 * 6. Input Mask's
 * 7. Автокомплит
 * 8. Превью расположения на карте
 * 9.
 */
jQuery(document).ready(function($) {

	/*** 1. выподающее подменю ****/
	$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		//$('li.dropdown.open').removeClass('open');
		$(this).parent().parent().children('li').removeClass('open');
		$(this).parent().addClass('open');
		var menu = $(this).parent().find("ul");
		var menupos = menu.offset();
		if ((menupos.left + menu.width()) + 30 > $(window).width()) {
			var newpos = - menu.width();
		} else {
			var newpos = $(this).parent().width();
		}
		menu.css({ left:newpos });
	});
	/*** end 1. выподающее подменю ****/


	/*** 2. Удаления пользователя ****/
	$(function() {
		$('.dell_user').on('click', function() {
			var $this = $(this),
					idUsers = $this.parents('tr').attr('id');
			$( "#dialog-confirm" ).dialog({
				resizable: false,
				height:200,
				modal: true,
				buttons: {
					"Да": function() {
						$.ajax({
							url: 'ajax.php',
							data: {users:idUsers}
						}).done(function(data){
							if(data)  {
								$this.closest('tr').remove();
								$( "#dialog-message" ).dialog({
									modal: true,
									buttons: {
										Ok: function() {
											$( this ).dialog( "close" );
										}
									}
								});
							} else  {
								alert('Попробуйте ещё раз')
							}
						})


						$( this ).dialog( "close" );

					},
					"Нет": function() {
						$( this ).dialog( "close" );
					}
				}
			});
		})

	});
	/*** end 2. Удаления пользователя ****/

	/*** 3. date input - выбор даты - jquery ui ***/
	$('.datepicker').datepicker({
		defaultDate: 0,
		// minDate: "+1D",
		changeMonth: true,
		changeYear: true,
		firstDay: 0,
		dateFormat: "dd.mm.yy",
		hideIfNoPrevNext: true,
		showOtherMonths: true,
		dayNamesMin    :  ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
		monthNames     :  ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		monthNamesShort:  ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
	});

	$( ".datepicker_from" ).datepicker({
		defaultDate: 0,
		minDate: "+1D",
		firstDay: 0,
		dateFormat: "dd.mm.yy",
		changeMonth: true,
		hideIfNoPrevNext: true,
		showOtherMonths: true,
		dayNamesMin    :  ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
		monthNames     :  ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		monthNamesShort:  ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		onClose: function( selectedDate ) {
			$(this).closest('tr').find('.datepicker_to').datepicker( "option", "minDate", selectedDate );
		}
	});

	$( ".datepicker_to" ).datepicker({
		defaultDate: 0,
		minDate: "+1D",
		firstDay: 0,
		dateFormat: "dd.mm.yy",
		changeYear: true,
		hideIfNoPrevNext: true,
		showOtherMonths: true,
		dayNamesMin    :  ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
		monthNames     :  ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		monthNamesShort:  ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		onClose: function( selectedDate ) {
			$(this).closest('tr').find('.datepicker_from').datepicker( "option", "maxDate", selectedDate );
		}
	});

	$('.hasDatepicker').keypress(function(e){
		var keycode =  e.keyCode ? e.keyCode : e.which;
		var val_in = $(this).val();
		if(e.keyCode === 46) {
			return false;
		}
	});
	$('.hasDatepicker').keyup(function(e){
		var keycode =  e.keyCode ? e.keyCode : e.which;
		var val_in = $(this).val();

		if(keycode != 8) {
			switch (val_in.length) {
				case 2:
					$(this).val(val_in + '.');
					break;
				case 3:
					if(val_in[2] != ".") {
						$(this).val(val_in.slice(0, -1) + '.');
					}
					break;
				case 5:
					$(this).val(val_in + '.');
					break;
				case 6:
					if(val_in[5] != ".") {
						$(this).val(val_in.slice(0, -1) + '.');
					}
					break;
				default:
					break;
			}
		}

	});
	/*** end 3. date input - выбор даты - jquery ui ***/

	/*** 4. input file  ***/
	$(document).on('change', ':file', function() {
		var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	});
	$(document).ready( function() {
		$(':file').on('fileselect', function(event, numFiles, label) {
			$(this).closest('td').find('.res_file').html(label)
		});
	});
	/*** end 4. input file  ***/

	/*** 5. проверка форм на заполненость полей ***/
	function obligatory_filling ($body) {
		var arr_inp = $body.find('.check_in'); // если в форме есть поля - обязательные для заполнения
		var dataInp, this_el;
		var sub_stop = false;

		// проверка input checkbox and radio
		var $inp_rad = $body.find('.inp_check_radio_surely input[type="radio"]');
		if($inp_rad.length && !$inp_rad.is(':checked')) {
			sub_stop = true;
			$inp_rad.not(':checked')
					.addClass('error')
					.closest('.jq-radio').addClass('error');
		}
		var $inp_check = $body.find('.inp_check_radio_surely input[type="checkbox"]');
		if($inp_check.length &&  !$inp_check.is(':checked')) {
			sub_stop = true;
			$inp_check.not(':checked')
					.addClass('error')
					.closest('.jq-checkbox').addClass('error');
		}

		$body.find('select.check_in').each(function (ind, el) {
			var $el_sel = $(el);
			if(el.value == "") {
				sub_stop = true;
				$el_sel.addClass('error').next('.jq-selectbox__select').addClass('error');
			} else {
				if($el_sel.hasClass('error')) {
					$el_sel.removeClass('error').next('.jq-selectbox__select').removeClass('error');
				}
			}
		});

		arr_inp.each(function(index, el) {
			this_el = $(el);
			if(this_el.hasClass('jq-selectbox')) {
				return true;
			}

			dataInp = this_el.data('input') || 'text';
			if(!checInp(this_el.val(), dataInp, this_el.data('quan'))) {
				this_el.addClass('error');
				sub_stop = true;
			} else {
				this_el.removeClass('error');
			}

		});
		return !sub_stop;

	}
	window.obligatory_filling = obligatory_filling;

	function checInp (val, typ, quantity) {
		var val = val || '';
		var typ =  typ || 'text';
		var quantity_2 = quantity - 1 || 2;
		var quantity_num = quantity || 9999999999999999;
		if(typ === "text") 	return (val.length >= quantity_2);
		else if(typ === "password")  return (val.length > quantity_2);
		else if(typ === "mail"){
			var checkMail = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*/;
			return (val.search(checkMail)) ? false : true;
		}	else if(typ === "number") {
			var checkNumber = /\d+/;
			return (val.search(checkNumber) || val.length < quantity_num) ? false : true;
		}	else if(typ === "phone") {
			var checkNumber = /\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}/;
			var resVal = (val.search(checkNumber)) ? false : true;
			if(resVal)
			{
				// проверка на одинаковость всех символов
				var phoneStr =  val.replace(/[\+7\(\) -]/g,'');
				var phoneStrLength = phoneStr.length;
				resVal = false;
				for (var i = 1; i < phoneStrLength; i++)
				{
					if(phoneStr[i-1] != phoneStr[i])
					{
						resVal = true;
						return true;
					}
				}
				return resVal;
			}
			// return
		}	else return true;
	}

	function checInp_key (val, typ) {
		var typ = typ;
		if(typ === "rus") {
			var rus = /[А-ЯЁа-яё ]/;
			return (val.search(rus)) ? false : true;
		}
		else if(typ === "ang") {
			var ang = /[A-Za-z ]/;
			return (val.search(ang)) ? false : true;
		}
		else if(typ === "mail"){
			var checkMail = /[\w-.@]/;
			return (val.search(checkMail)) ? false : true;
		}	else if(typ === "number") {
			var checkNumber = /\d/;
			return (val.search(checkNumber)) ? false : true;
		}	else if(typ === "phone") {
			var checkNumber = /\d/;
			return (val.search(checkNumber)) ? false : true;
		}	else return true;
	}
	// удаляем класс у SELECT - при выборе
	$('form').on('change', 'select.error', function () {
		if($(this).val().length) {
			$(this).removeClass('error').next('.jq-selectbox__select').removeClass('error');
		}
	});

	$('.inp_check_radio_surely').on('change', 'input[type="radio"].error, input[type="checkbox"].error', function () {
		$(this)
				.closest('.inp_check_radio_surely')
				.find('input[type="radio"], input[type="checkbox"]').removeClass('error')
				.closest('.jq-checkbox, .jq-radio').removeClass('error');
	});

	// провереям при отправки
	$('form').submit(function(e) {
		if(!obligatory_filling($(this))) {
			return false;
		}
		else
		{
			var successCallback = $(this).data('success');
			if(typeof successCallback != 'undefined')
				return eval(successCallback)(this);
		}
	});

	// проверяем при нажатии клавишь
	$('body').on('keypress', '.check_in.error', function(event) {
		var this_el = $(this);
		if(checInp(this_el.val(), this_el.data('input'), this_el.data('quan'))) {
			this_el.removeClass('error');
		}
	});
	$('.check_in').focusout(function() {
		var this_el = $(this);
		if(!checInp(this_el.val(), this_el.data('input'), this_el.data('quan'))) {
			this_el.addClass('error');
		}
	});

	// event.type должен быть keypress
	function getChar(event) {
		if (event.which == null) { // IE
			if (event.keyCode < 32) return null; // спец. символ
			return String.fromCharCode(event.keyCode)
		}
		if (event.which != 0 && event.charCode != 0) { // все кроме IE
			if (event.which < 32) return null; // спец. символ
			return String.fromCharCode(event.which); // остальные
		}
		return null; // спец. символ
	}

	// Допустимые символы в input
	$('input[data-regex]').keypress(function (e) {
		var charCode = e.charCode || e.keyCode;
		if(getChar(e) && !e.ctrlKey) {
			if(!checInp_key (String.fromCharCode(charCode), $(this).data('regex')))
				preventDef(e);
		}
	});
	/*** end 5. проверка форм на заполненость полей ***/

	/*** 6. Input Mask's. ***/
	$('.phone_inp').inputmask('+7 (999) 999-99-99');
	$('.decimal_inp').inputmask('99.999999');
	/*** end 6. Input Mask's. ***/

	/*** 7. Автокомплит. ***/
	var availableTags = [
		["sadsadsadas", "id_1"],
		["gdffdsfsd", "id_2"],
		["asdsadsa", "id_3"],
		["молоко", "id_4"],
		["Снег", "id_5"],
		["Аптека", "id_6"],
		["Оптика", "id_7"],
		["Курс", "id_8"],
		["Пончик", "id_9"],
		["Автомобиль", "id_10"],
		["Психолог", "id_11"],
		["Пушкин", "id_12"],
		["Work", "id_13"],
		["zero", "id_14"],
		["ready", "id_15"]
	];

	var data = [
		{ label: "anders", category: "" },
		{ label: "andreas", category: "" },
		{ label: "antal", category: "" },
		{ label: "annhhx10", category: "Products" },
		{ label: "annk K12", category: "Products" },
		{ label: "annttop C13", category: "Products" },
		{ label: "anders andersson", category: "People" },
		{ label: "andreas andersson", category: "People" },
		{ label: "andreas johnson", category: "People" }
	];

	var $input = $( ".autoComp" );

	$input.autocomplete({
		delay: 0,
		source: data,
		select: function(event, ui ){
			var $parent = $(this).parents('.autoCompWR');
			$parent.find('input[type="hidden"]').val(ui.item.category);
			$parent.next('td').html(ui.item.label);
		}
	});

	/*** end 7. Автокомплит. ***/

	//  by Rybikov Oleg
	/*** 8. Превью расположения на карте. ***/



	if( jQuery('.js-map.js-map-box').length) {
		pMap.eInit(); // инитим отображение на карте
	}

	/*** end 8. Превью расположения на карте. ***/
	//  END - by Rybikov Oleg


	// Маска на время
	$('.js-date-time').mask("99:99");
}); // ready

var pMap = { // работа с картой
	jMap: [],
	jPMark: [],
	aCoord: [55.753559, 37.609218],
	isMap: false,
	nZoom: 12,
	jTimer:[],
	eInit:function(){
		// console.log('eInit');
		if( jQuery('.js-map-lon').val() && jQuery('.js-map-lat').val()){ // если есть сохранённые координаты
			pMap.eLookCoord();
		} else {
			pMap.eValidate(pMap.aCoord);
		}
	},
	eLookCoord: function(){ // смотрим координаты после загрузки страницы
		// console.log('eLookCoord');
		pMap.aCoord = [parseFloat(jQuery('.js-map-lat').val()), parseFloat(jQuery('.js-map-lon').val())];
		pMap.nZoom = 16;
		pMap.eValidate(pMap.aCoord); // проверяем полученные координаты из инпутов
	},
	eReadyMap: function(){ // смотрим, готово ли апи карт
		// console.log('eReadyMap');
		ymaps.ready(pMap.eCreateMap);
	},
	eValidate: function(coord){ // смотрим правильность введённых координат
		// console.log('eValidate', coord);
		if(jQuery.isNumeric(coord[0]) && jQuery.isNumeric(coord[1])){
			pMap.eReadyMap();
		}
	},
	eCreateMap: function(){ // создаём экземпляр карты
		// console.log('eCreateMap');
		pMap.jMap = new ymaps.Map("b-map", {
			center: pMap.aCoord,
			zoom: pMap.nZoom
		});
		pMap.jMap.controls.add('smallZoomControl'); // добавляем контролл зума
		pMap.jMap.events.add('click', pMap.eGetCoords);

		pMap.eCreatPlaceMArk();
	},
	eGetCoords: function(e){ // берём координаты клика
		// console.log('eGetCoords');
		clearTimeout(pMap.jTimer); // защищаемся от "задрота" с множественным кликом
		pMap.jTimer = setTimeout(function(){
			var cuCoord = e.get('coords');
			pMap.ePrintCoords(cuCoord);
		},500);
	},
	ePrintCoords: function(coord){ // записываем координаты в инпут
		// console.log('ePrintCoords',coord);
		jQuery('.js-map-lat').val(parseFloat(coord[0]));
		jQuery('.js-map-lon').val(parseFloat(coord[1]));
		pMap.aCoord = coord;
		pMap.eDeleteMark();
		pMap.eCreatPlaceMArk()
	},
	eCreatPlaceMArk: function(){ // создаём метку и добавляем на карту
		// console.log('eCreatPlaceMArk');
		pMap.jPMark = new ymaps.Placemark(pMap.aCoord);
		pMap.jMap.geoObjects.add(pMap.jPMark); // добавляем метку
		// pMap.jMap.setCenter(pMap.aCoord); // центрируем
		pMap.jMap.panTo([pMap.aCoord]); // плавно перемещаем к метке

		pMap.isMap = true; // помечаем что карта создана
	},
	eDeleteMark: function(){ // удаляем метку с карты
		// console.log('eDeleteMark');
		if(pMap.isMap){
			pMap.jMap.geoObjects.remove(pMap.jPMark);
		}
	}
}

function new_category(e) {
	e.preventDefault();
	$('#category').hide();
	$('#category_link').hide();


	$('#category_input').show();
	$('#category_button').show();
	// $.ajax('/ajax/new_category');
}

$('#category_button').on('click', function() {
	$.post('/ajax/new_category', { category_name : $('#category_input').val()}, function(data) {
		console.log(data);
		$('#category_input').val('');
		$('#category_input').hide();
		$('#category_button').hide();
		$('#category').show();
		$('#category_link').show();

		$('#category').parent().html(data.html);
	});
});
