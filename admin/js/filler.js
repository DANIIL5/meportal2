function empty( mixed_var ) {
    return ( mixed_var === "" || mixed_var === 0   || mixed_var === "0" || mixed_var === null  || mixed_var === false  ||  ( is_array(mixed_var) && mixed_var.length === 0 ) );
}

function is_array( mixed_var ) {
    return ( mixed_var instanceof Array );
}

$("#linked_organisation").on('change', function() {
    id = $(this).val();
    $.post("/cp/ajax_get_organisation_fields", { id: id }, function(data) {
        if (!empty(data)) {
            console.log(data);

            console.log(data.TYPE_ID);
            console.log(data.CATEGORY_ID);
            $("#category").val(data.CATEGORY_ID);
            $("#type").val(data.TYPE_ID);
            $("#brand").val(data.NAME);
            $("#description").val(data.DESCRIPTION);
            $("#legal_index").val(data.LEGAL_ADDRESS_INDEX);
            $("#legal_region").val(data.LEGAL_ADDRESS_REGION);
            $("#legal_city").val(data.LEGAL_ADDRESS_CITY);
            $("#legal_street").val(data.LEGAL_ADDRESS_STREET);
            $("#legal_house").val(data.LEGAL_ADDRESS_HOUSE);
            $("#legal_building").val(data.LEGAL_ADDRESS_BUILDING);
            $("#actual_index").val(data.ACTUAL_ADDRESS_INDEX);
            $("#actual_region").val(data.ACTUAL_ADDRESS_REGION);
            $("#actual_city").val(data.ACTUAL_ADDRESS_CITY);
            $("#actual_street").val(data.ACTUAL_ADDRESS_STREET);
            $("#actual_house").val(data.ACTUAL_ADDRESS_HOUSE);
            $("#actual_building").val(data.ACTUAL_ADDRESS_BUILDING);
            $("#license").val(data.LICENSE);
            $("#phone1").val(data.PHONE1);
            $("#phone2").val(data.PHONE2);
            $("#phone3").val(data.PHONE3);
            $("#website1").val(data.WEBSITE1);
            $("#website2").val(data.WEBSITE2);
            $("#website3").val(data.WEBSITE3);
            $("#email").val(data.EMAIL);
            $("#latitude").val(data.LATITUDE);
            $("#longitude").val(data.LONGITUDE);
            $("#ogrn").val(data.OGRN);
            $("#inn").val(data.INN);
            $("#okved").val(data.OKVED);
            $("#okpo").val(data.OKPO);
            $("#kpp").val(data.KPP);
            $("#oktmo").val(data.OKTMO);
            $("#account").val(data.ACCOUNT);
            $("#bank").val(data.BANK);
            $("#bik").val(data.BIK);
            $("#physician").val(data.CHIEF_PHYSICIAN);
            $("#accountant").val(data.CHIEF_ACCOUNTANT);
            $("#ceo").val(data.CEO);
            $("#monday_from").val(data.MONDAY_WORK_FROM);
            $("#monday_to").val(data.MONDAY_WORK_FROM);
            $("#tuesday_from").val(data.TUESDAY_WORK_FROM);
            $("#tuesday_to").val(data.TUESDAY_WORK_TO);
            $("#wednesday_from").val(data.WEDNESDAY_WORK_FROM);
            $("#wednesday_to").val(data.WEDNESDAY_WORK_TO);
            $("#thursday_from").val(data.THURSDAY_WORK_FROM);
            $("#thursday_to").val(data.THURSDAY_WORK_TO);
            $("#friday_from").val(data.FRIDAY_WORK_FROM);
            $("#friday_to").val(data.FRIDAY_WORK_TO);
            $("#saturday_from").val(data.SATURDAY_WORK_FROM);
            $("#saturday_to").val(data.SATURDAY_WORK_TO);
            $("#sunday_from").val(data.SUNDAY_WORK_FROM);
            $("#sunday_to").val(data.SUNDAY_WORK_TO);
            $("#monday_break_from").val(data.MONDAY_BREAK_FROM);
            $("#monday_break_to").val(data.MONDAY_BREAK_TO);
            $("#tuesday_break_from").val(data.TUESDAY_BREAK_FROM);
            $("#tuesday_break_to").val(data.TUESDAY_BREAK_TO);
            $("#wednesday_break_from").val(data.WEDNESDAY_BREAK_FROM);
            $("#wednesday_break_to").val(data.WEDNESDAY_BREAK_TO);
            $("#thursday_break_from").val(data.THURSDAY_WORK_FROM);
            $("#thursday_break_to").val(data.THURSDAY_WORK_TO);
            $("#friday_break_from").val(data.FRIDAY_BREAK_FROM);
            $("#friday_break_to").val(data.FRIDAY_BREAK_TO);
            $("#saturday_break_from").val(data.SATURDAY_BREAK_FROM);
            $("#saturday_break_to").val(data.SATURDAY_BREAK_TO);
            $("#sunday_break_from").val(data.SUNDAY_WORK_FROM);
            $("#sunday_break_to").val(data.SUNDAY_WORK_TO);
        }
    });
});