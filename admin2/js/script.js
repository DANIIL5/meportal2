function empty(value) {
    if(typeof value === "undefined"){
        return true;
    }
    switch(value) {
        case "":
        case " ":
        case 0:
        case "0":
        case null:
        case false:
            return true;
        default : return false;
    }
}

function in_array(needle, arr) {
    return (arr.indexOf(needle) !== -1);
}


function remove_selected(object, e)
{
    e.preventDefault();
    $(object).prop('selectedIndex',0);
}

function inplace_input(object)
{
    var text = $(object).text();
    $(object).removeAttr('onclick');
    $(object).html('<form class="uk-form"><input type="text" value="'+ text +'" onblur="inplace_input_save(this)"></form>');
    $(object).children('form').children('input').focus();
}

function inplace_input_save(object)
{
    var text = $(object).val();
    var parent = $(object).parent().parent();
    $(object).remove();
    parent.attr('onclick', 'inplace_input(this)');
    parent.html(text);
}

function link_input_text(input_object, text_object, placeholder)
{
    text = $(input_object).val();
    console.log(text);
    console.log(text.replace(/\s+/g,' '));
    if (empty(text.replace(/\s+/g,' '))) {
        text = placeholder;
    }
    $(text_object).html(text);
}