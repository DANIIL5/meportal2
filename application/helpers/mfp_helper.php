<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function encodePassword($password)
{
    $salt           = salt();
    $hash           = hash("sha224",$password.$salt).$salt;

    return $hash;
}

function checkPassword($password,$storedPasswordHash)
{
    $passwordLength = strlen($storedPasswordHash);
    $salt           = substr($storedPasswordHash, $passwordLength-8, $passwordLength);
    $passwordHash   = hash("sha224", $password.$salt).$salt;
   
  
    return ($passwordHash == $storedPasswordHash) ? true : false ;
}

function checkEmail($email)
{
    return filter_var($email,FILTER_VALIDATE_EMAIL);
}

function salt($length = 8)
{
    return randString($length, "hash");
}

function randString($length, $imitate = null)
{
    $characters     = '0123456789abcdefghijklmnopqrstuvwxyz';
    $randomString   = '';

    if ($imitate == "hash")
        $characters = substr($characters,0,16);
    elseif($imitate == "numbers")
        $characters = substr($characters,0,10);
    elseif($imitate != null)
        throw new Exception('Неизвестный тип $imitate');


    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $randomString;
}