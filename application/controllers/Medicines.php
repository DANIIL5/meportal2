<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medicines extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->data['controller']   = $this->router->fetch_class();
        $this->data['method']       = $this->router->fetch_method();

        $this->user_id = $this->session->userdata('USER_ID');

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Carousel_model');
        $this->load->model('Action_model');
        $this->load->model('Banner_model');
        $this->load->model('Branches_model');
        $this->load->model('News_model');
        $this->load->model('Institution_model');
        $this->load->model('Articles_model');
        $this->load->model('Geo_model');
        $this->load->model('Logos_model');
        $this->load->model('Post_model');
        $this->load->model('Voting_model');

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $this->data['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $this->data['current_city'] = $cookie_city_id;
        } else {
            $this->data['current_city'] = 4400;
        }

        //Получаем две случайные акции для блока на главной
        $actions['actions'] = $this->Action_model->get_random_list(2, 11);

        foreach ($actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $actions['actions'][$key]['DESCRIPTION'] = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $actions['actions'][$key]['PRICE'] = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        // Получаем акции для вывода на карту
        $map_actions['actions'] = $this->Action_model->get_random_map(4, 11);

        foreach ($map_actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $map_actions['actions'][$key]['DESCRIPTION']    = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $map_actions['actions'][$key]['PRICE']              = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        $carousel = $this->Carousel_model->get_images(1);
        $actions_quantity        = $this->Action_model->count();
        $actions['quantity']     = $actions_quantity;
        $map_actions['quantity'] = $actions_quantity;
        $map_actions['green'] = false;
        $popular_institutions = $this->Institution_model->get_popular();
        $first_banners  = $this->Banner_model->get_banners(1);
        $second_banners = $this->Banner_model->get_banners(1);
        $news = $this->News_model->get_last();
        $map_actions['types'] = $this->Institution_model->get_types();

        // выбираем три рандомные категории
        $popular_themes_category = $this->Articles_model->get_random_themes();

        $popular_themes_category_ids = null;

        foreach ($popular_themes_category as $item) {
            $popular_themes_category_ids[] = $item['ID'];
        }

        $popular_themes = $this->Articles_model->get_articles_by_ids_array($popular_themes_category_ids);

        foreach ($popular_themes as $popular_theme) {
            $popular_themes_data['themes'][$popular_theme['CATEGORY_ID']][] = [
                'ID'        => $popular_theme['ID'],
                'NAME'      => $popular_theme['NAME']
            ];
        }

        $popular_themes_data['category'] = $popular_themes_category;

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);
        $cities['current_city'] = $this->data['current_city'];

        $logos = $this->Logos_model->get_logos();

        $voting = $this->Voting_model->get_vote();
        $voting['is_vote'] = $this->Voting_model->is_vote($voting['info']['ID'], $this->user_id);

        $this->data['templates'] = [
            'city_selector'     => $this->load->view('tmp/city_selector', $cities, true),
            'auth'              => ($this->user_id)
                ? $this->load->view('tmp/auth_entered', [
                    'data' => $this->User_model->get_by_id($this->user_id),
                    'msg_count' => $this->Post_model->get_by_user_id_in_count_unread($this->user_id)[0]['COUNT(*)']
                ], true)
                : $this->load->view('tmp/auth', null, true),
            'carousel'          => $this->load->view('tmp/carousel', ['images' => $carousel], true),
            'menu'              => $this->load->view('tmp/menu', ['controller' => $this->data['controller']], true),
            'search'            => $this->load->view('tmp/search', null, true),
            'ask_question'      => $this->load->view('tmp/ask_question', ['is_auth' => (!empty($this->user_id)) ? true : false], true),
//            'actions'           => $this->load->view('tmp/actions', null, true),
            'logos'             => $this->load->view('tmp/logos', ['logos' => $logos], true),
            'map_search'        => $this->load->view('tmp/map_search', $map_actions, true),
            'popular_institutions'  => $this->load->view('tmp/popular_institutions', ['institutions' => $popular_institutions], true),
            'first_banners'     => $this->load->view('tmp/banners', ['banners' => $first_banners], true),
            'second_banners'    => $this->load->view('tmp/banners', ['banners' => $second_banners], true),
            'popular_themes'    => $this->load->view('tmp/popular_themes', $popular_themes_data, true),
            'voiting'           => $this->load->view('tmp/voiting', $voting, true),
            'news'              => $this->load->view('tmp/news', ['news' => $news], true),
            'bottom_menu'       => $this->load->view('tmp/bottom_menu', null, true),
            'footer'            => $this->load->view('tmp/footer', ['user' => $this->user_id], true)
        ];
    }

    public function catalog()
    {
        $per_page = 10;
        $sphinx = new mysqli('127.0.0.1', null, null, null, 9306);
        $where = "";
        $where_array = [];
        $pages = 0;

        if (!empty($_GET['pages']))
            $pages = ((int)$_GET['pages']) * ($per_page);

        if (!empty($_GET['per_page']))
            $per_page = (int)$_GET['per_page'];
        
        $all = false;

        $this->data['all'] = false;
        if (!empty($_GET['all']) && $_GET['all'] == 'true')
            $this->data['all'] = true;

        $this->data['get_replacer'] = function ($params) {
            parse_str($_SERVER['QUERY_STRING'], $get_params);

            foreach ($params as $param => $value) {
                if (!empty($value)) {
                    $get_params[$param] = $value;
                } else {
                    if (isset($get_params[$param]))
                        unset($get_params[$param]);
                }
            }
            return ((!empty($get_params)) ? "?" : "" ).http_build_query($get_params);
        };
//            $query = mysqli_real_escape_string($this->db->conn_id, $_GET['query']);

//        while ($row = $resource->fetch_assoc()) {
//
//        }


        $this->db->limit($per_page, $pages);
        $this->db->select('TRADE_BALANCES.*');
        $this->db->select('ORGANIZATIONS.BRAND')->select('ORGANIZATIONS.LOGO')->select('ORGANIZATIONS.LATITUDE')->select('ORGANIZATIONS.LONGITUDE');
        $this->db->select('ORGANIZATION_ADDRESSES.*');

        $this->db->join('ORGANIZATIONS', 'ORGANIZATIONS.ID = TRADE_BALANCES.BRANCH_ID', 'left');
        $this->db->join('ORGANIZATION_ADDRESSES', 'ORGANIZATION_ADDRESSES.ORGANIZATION_ID = TRADE_BALANCES.BRANCH_ID AND ORGANIZATION_ADDRESSES.TYPE_ID = 2', 'left');

        if (!empty($_GET['id']))
            $this->db->where('TRADE_BALANCES.NOMEN_ID', (int)$_GET['id']);
        if (!empty($_GET['institution_id']))
            $this->db->where('TRADE_BALANCES.BRANCH_ID', (int)$_GET['institution_id']);

        $this->data['goods'] = $this->db->get('TRADE_BALANCES')->result_array();

        if (!empty($_GET['id']))
            $this->db->where('NOMEN_ID', (int)$_GET['id']);
        if (!empty($_GET['institution_id']))
            $this->db->where('BRANCH_ID', (int)$_GET['institution_id']);

        $catalog_quantity = $this->db->count_all_results('TRADE_BALANCES');

        $this->load->library('pagination');

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 3;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<span class="pages">';
        $config['full_tag_close'] = '</span>';
        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['prev_tag_open'] = '';
        $config['prev_tag_close'] = '';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';
        $config['next_tag_open'] = '';
        $config['next_tag_close'] = '';

        $this->pagination->initialize($config);

        $this->data['pagination'] = $this->pagination->create_links();

        $this->data['organisations'] = $this->db->where('PARENT_ID', NULL)->or_where('PARENT_ID', '')->get('ORGANIZATIONS')->result_array();

        $branch_id = (!empty($_GET['branch_id'])) ? (int)$_GET['branch_id'] : null;
        $organisation_id = (!empty($_GET['institution_id'])) ? (int)$_GET['institution_id'] : null;

        $this->data['branch'] = null;

        if (!empty($branch_id)) {
            $branch = $this->Branches_model->new_get_item($branch_id);
            if (!empty($branch)) {

                $months = [
                    1   => 'Январь',
                    2   => 'Февраль',
                    3   => 'Март',
                    4   => 'Апрель',
                    5   => 'Май',
                    6   => 'Июнь',
                    7   => 'Июль',
                    8   => 'Август',
                    9   => 'Сентябрь',
                    10  => 'Октябрь',
                    11  => 'Ноябрь',
                    12  => 'Декабрь'
                ];

                $month = $months[(int)date('m')];
                $monday = strtotime("last Monday");
                $week['mon'] = date("d", $monday);
                $week['tue'] = date("d", $monday + 86400);
                $week['wed'] = date("d", $monday + 2*86400);
                $week['thu'] = date("d", $monday + 3*86400);
                $week['fri'] = date("d", $monday + 4*86400);
                $week['sat'] = date("d", $monday + 5*86400);
                $week['sun'] = date("d", $monday + 6*86400);

                if (!empty($branch)) {
                    $branch['MONDAY_WORK_FROM']       = date("H:i", strtotime($branch['MONDAY_WORK_FROM']));
                    $branch['TUESDAY_WORK_FROM']      = date("H:i", strtotime($branch['TUESDAY_WORK_FROM']));
                    $branch['WEDNESDAY_WORK_FROM']    = date("H:i", strtotime($branch['WEDNESDAY_WORK_FROM']));
                    $branch['THURSDAY_WORK_FROM']     = date("H:i", strtotime($branch['THURSDAY_WORK_FROM']));
                    $branch['FRIDAY_WORK_FROM']       = date("H:i", strtotime($branch['FRIDAY_WORK_FROM']));
                    $branch['SATURDAY_WORK_FROM']     = date("H:i", strtotime($branch['SATURDAY_WORK_FROM']));
                    $branch['SUNDAY_WORK_FROM']       = date("H:i", strtotime($branch['SUNDAY_WORK_FROM']));

                    $branch['MONDAY_WORK_TO']         = date("H:i", strtotime($branch['MONDAY_WORK_TO']));
                    $branch['TUESDAY_WORK_TO']        = date("H:i", strtotime($branch['TUESDAY_WORK_TO']));
                    $branch['WEDNESDAY_WORK_TO']      = date("H:i", strtotime($branch['WEDNESDAY_WORK_TO']));
                    $branch['THURSDAY_WORK_TO']       = date("H:i", strtotime($branch['THURSDAY_WORK_TO']));
                    $branch['FRIDAY_WORK_TO']         = date("H:i", strtotime($branch['FRIDAY_WORK_TO']));
                    $branch['SATURDAY_WORK_TO']       = date("H:i", strtotime($branch['SATURDAY_WORK_TO']));
                    $branch['SUNDAY_WORK_TO']         = date("H:i", strtotime($branch['SUNDAY_WORK_TO']));

                    $branch['MONDAY_BREAK_FROM']      = date("H:i", strtotime($branch['MONDAY_BREAK_FROM']));
                    $branch['TUESDAY_BREAK_FROM']     = date("H:i", strtotime($branch['TUESDAY_BREAK_FROM']));
                    $branch['WEDNESDAY_BREAK_FROM']   = date("H:i", strtotime($branch['WEDNESDAY_BREAK_FROM']));
                    $branch['THURSDAY_BREAK_FROM']    = date("H:i", strtotime($branch['THURSDAY_BREAK_FROM']));
                    $branch['FRIDAY_WORK_FROM']       = date("H:i", strtotime($branch['FRIDAY_WORK_FROM']));
                    $branch['SATURDAY_WORK_FROM']     = date("H:i", strtotime($branch['SATURDAY_WORK_FROM']));
                    $branch['SUNDAY_WORK_FROM']       = date("H:i", strtotime($branch['SUNDAY_WORK_FROM']));

                    $branch['MONDAY_BREAK_TO']        = date("H:i", strtotime($branch['MONDAY_BREAK_TO']));
                    $branch['TUESDAY_BREAK_TO']       = date("H:i", strtotime($branch['TUESDAY_BREAK_TO']));
                    $branch['WEDNESDAY_BREAK_TO']     = date("H:i", strtotime($branch['WEDNESDAY_BREAK_TO']));
                    $branch['THURSDAY_BREAK_TO']      = date("H:i", strtotime($branch['THURSDAY_BREAK_TO']));
                    $branch['FRIDAY_BREAK_TO']        = date("H:i", strtotime($branch['FRIDAY_BREAK_TO']));
                    $branch['SATURDAY_BREAK_TO']      = date("H:i", strtotime($branch['SATURDAY_BREAK_TO']));
                    $branch['SUNDAY_BREAK_TO']        = date("H:i", strtotime($branch['SUNDAY_BREAK_TO']));

                    $this->data['branch'] = $this->load->view('tmp/institution_catalog', ['item' => $branch, 'week' => $week, 'month' => $month], true);
                }
            }
        }

        if (!empty($organisation_id)) {
            $this->data['id'] = $organisation_id;

            $months = [
                1   => 'Январь',
                2   => 'Февраль',
                3   => 'Март',
                4   => 'Апрель',
                5   => 'Май',
                6   => 'Июнь',
                7   => 'Июль',
                8   => 'Август',
                9   => 'Сентябрь',
                10  => 'Октябрь',
                11  => 'Ноябрь',
                12  => 'Декабрь'
            ];

            $this->data['month'] = $months[(int)date('m')];
            $monday = strtotime("last Monday");
            $this->data['week']['mon'] = date("d", $monday);
            $this->data['week']['tue'] = date("d", $monday + 86400);
            $this->data['week']['wed'] = date("d", $monday + 2*86400);
            $this->data['week']['thu'] = date("d", $monday + 3*86400);
            $this->data['week']['fri'] = date("d", $monday + 4*86400);
            $this->data['week']['sat'] = date("d", $monday + 5*86400);
            $this->data['week']['sun'] = date("d", $monday + 6*86400);

            // Получить отделения
            $this->load->model('New_organisations_model');
            $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $organisation_id]);

            $this->data['branches']     = [];
            $this->data['specialists']  = [];
            $this->data['articles']     = [];
            $this->data['photos']       = [];
            $this->data['videos']       = [];
            $this->data['feedbacks']    = [];

            $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);
        }

        $this->load->view('main/medicines', $this->data);
    }

    public function item($id)
    {
        $this->data['nomen'] = $this->db->query('
            SELECT 
                   NOMEN.ID NOMENID,      # идентификатор в номенклатуре
                   PPACK.NAME PPACKNAME,  # название в начальной упаковке 
                   NOMEN.DRUGSINPPACK,    # количество в начальной упаковке
                   NOMEN.PPACKMASS,       # масса упаковки
                   PPACKMASS.FULLNAME PPACKMASSNAME,          # название массы упаковки
                   PPACKMASS.SHORTNAME PPACKMASSSHORTNAME,    # короткое название массы упаковки
                   NOMEN.PPACKVOLUME,     # объем упаковки
                   PPACKVOLUME.FULLNAME PPACKVOLUMENAME,      # название объема упаковки
                   PPACKVOLUME.SHORTNAME PPACKVOLUMESHORTNAME,# короткое название объема упаковки
                   DRUGSET.FULLNAME DRUGSETNAME,              # название комплекта
                   DRUGSET.SHORTNAME DRUGSETSHORTNAME,        # короткое название комплекта
                   NOMEN.PPACKINUPACK,    # количество первичных упаковок во вторичной
                   UPACK.NAME UPACKNAME,  # название вторичной упаковки
                   NOMEN.INANGRO,         # признак АНГРО
                   NOMEN.EANCODE,         # ШКП (штрихкод производителя)
                   NOMEN.DRUGSTORCOND NOMENDRUGSTORCOND,      # условия хранения из общей таблицы
                   DRUGSTORCOND.TEXT DRUGSTORCOND,            # условия хранения
                   NOMEN.DRUGLIFETIME NOMENDRUGLIFETIME,      # срок годности из общей таблицы
                   DRUGLIFETIME.TEXT DRUGLIFETIME,            # срок годности
                   NOMEN.UPACKINSPACK,    # количество вторичных упаковок в третьичной
                   SPACK.NAME SPACKNAME,  # название третьичной упаковки
                   PREP.ID PREPID,        # идентификатор препарата
                   TRADENAMES.NAME,       # название препарата
                   TRADENAMES.INAME,      # название препарата (без спецсимволов)
                   LATINNAMES.NAME LNAME, # название препарата (латинское)
                   CLSDRUGFORMS.FULLNAME DRUGFORMNAME,    # название лекарственной формы
                   CLSDRUGFORMS.NAME DRUGFORMSHORTNAME,   # короткое название лекарственной формы
                   PREP.DFMASS,           # масса лекарственной формы
                   MASSUNITS.FULLNAME DFMASSNAME,         # название единицы измерения
                   MASSUNITS.SHORTNAME DFMASSSHORTNAME,   # короткое название единицы измерения
                   PREP.DFCONC,           # концентрация лекарственной формы
                   CONCENUNITS.FULLNAME DFCONCNAME,       # название единицы измерения
                   CONCENUNITS.SHORTNAME DFCONCSHORTNAME, # короткое название единицы измерения
                   PREP.DFACT,            # количество единиц действия
                   ACTUNITS.FULLNAME DFACTNAME,           # название единицы измерения
                   ACTUNITS.SHORTNAME DFACTSHORTNAME,     # короткое название единицы измерения
                   PREP.DFSIZE,           # размеры лекарственной формы
                   SIZEUNITS.FULLNAME DFSIZENAME,         # название единицы измерения
                   SIZEUNITS.SHORTNAME DFSIZESHORTNAME,   # короткое название единицы измерения
                   PREP.DRUGDOSE,         # количество доз в упаковке 
                   PREP.NORECIPE,         # признак безрецептурного отпуска 
                   FIRMS.ID FIRMID,                       # идентификатор фирмы
                   FIRMS.FULLNAME FIRMFULLNAME,           # полное название фирмы (может отсутствовать)
                   FIRMNAMES.NAME FIRMNAME,               # название фирмы
                   COUNTRIES.NAME COUNTRY,                # название страны фирмы
                   NOMENFIRMS.ID NOMENFIRMID,             # идентификатор фирмы
                   NOMENFIRMS.FULLNAME NOMENFIRMFULLNAME, # полное название фирмы (может отсутствовать)
                   NOMENFIRMNAMES.NAME NOMENFIRMNAME,     # название фирмы
                   NOMENCOUNTRIES.NAME NOMENCOUNTRY,      # название страны фирмы
                   FIRMS.ADRMAIN,         # адрес основного офиса
                   FIRMS.ADRRUSSIA,       # адрес в россии
                   FIRMS.ADRUSSR,         # адрес в странах ближнего зарубежья
                   DESCTEXTES.COMPOSITION,                # состав и форма выпуска
                   DESCTEXTES.DRUGFORMDESCR,              # описание лекарственной формы
                   DESCTEXTES.PHARMAACTIONS,              # фармакологическое действия
                   DESCTEXTES.PHARMAKINETIC,              # фармакинетика
                   DESCTEXTES.PHARMADYNAMIC,              # фармадинамика
                   DESCTEXTES.INDICATIONS,                # показания
                   DESCTEXTES.CONTRAINDICATIONS,          # противопоказания
                   DESCTEXTES.PREGNANCYUSE,               # применение при беременности и кормлении грудью
                   DESCTEXTES.SIDEACTIONS,                # побочные действия
                   DESCTEXTES.INTERACTIONS,               # взаимодействие
                   DESCTEXTES.USEMETHODANDDOSES,          # способ применения и дозы
                   DESCTEXTES.OVERDOSE,                   # передозировка
                   DESCTEXTES.SPECIALGUIDELINES,          # особые указания
                   IDENT_WIND_STR.IWID IMAGE              # изображение
              FROM NOMEN
         LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
         LEFT JOIN FIRMS ON FIRMS.ID = PREP.FIRMID
         LEFT JOIN FIRMNAMES ON FIRMNAMES.ID = FIRMS.NAMEID
         LEFT JOIN COUNTRIES ON COUNTRIES.ID = FIRMS.COUNTID
         LEFT JOIN TRADENAMES ON TRADENAMES.ID = PREP.TRADENAMEID
         LEFT JOIN LATINNAMES ON LATINNAMES.ID = PREP.LATINNAMEID
         LEFT JOIN CLSDRUGFORMS ON CLSDRUGFORMS.ID = PREP.DRUGFORMID
         LEFT JOIN MASSUNITS ON MASSUNITS.ID = PREP.DFMASSID
         LEFT JOIN CONCENUNITS ON CONCENUNITS.ID = PREP.DFCONCID
         LEFT JOIN ACTUNITS ON ACTUNITS.ID = PREP.DFACTID
         LEFT JOIN SIZEUNITS ON SIZEUNITS.ID = PREP.DFSIZEID
         LEFT JOIN DRUGFORMCHARS ON DRUGFORMCHARS.ID = PREP.DFCHARID
         LEFT JOIN FIRMS NOMENFIRMS ON NOMENFIRMS.ID = NOMEN.FIRMID
         LEFT JOIN FIRMNAMES NOMENFIRMNAMES ON NOMENFIRMNAMES.ID = NOMENFIRMS.NAMEID
         LEFT JOIN COUNTRIES NOMENCOUNTRIES ON NOMENCOUNTRIES.ID = NOMENFIRMS.COUNTID
         LEFT JOIN DRUGPACK PPACK ON PPACK.ID = NOMEN.PPACKID
         LEFT JOIN DRUGPACK UPACK ON UPACK.ID = NOMEN.UPACKID
         LEFT JOIN DRUGPACK SPACK ON SPACK.ID = NOMEN.SPACKID
         LEFT JOIN MASSUNITS PPACKMASS ON PPACKMASS.ID = NOMEN.PPACKMASSUNID
         LEFT JOIN CUBICUNITS PPACKVOLUME ON PPACKVOLUME.ID = NOMEN.PPACKCUBUNID
         LEFT JOIN DRUGSET ON DRUGSET.ID = NOMEN.SETID
         LEFT JOIN DRUGSTORCOND ON DRUGSTORCOND.ID = NOMEN.CONDID
         LEFT JOIN DRUGLIFETIME ON DRUGLIFETIME.ID = NOMEN.LIFEID
         LEFT JOIN DESCRIPTIONS ON DESCRIPTIONS.FIRMID = FIRMS.ID AND DESCRIPTIONS.FPREPNAME = TRADENAMES.INAME
         LEFT JOIN DESCTEXTES ON DESCTEXTES.DESCID = DESCRIPTIONS.ID 
         LEFT JOIN IDENT_WIND_STR ON IDENT_WIND_STR.NOMENID = NOMEN.ID
             WHERE NOMEN.ID = '.$id.'
             LIMIT 1')->row_array();

        $this->data['in_fav'] = false;

        $this->data['drugstores'] = $this->db->where('NOMEN_ID', $id)->count_all_results('TRADE_BALANCES');

        $fav = $this->db->where('USER_ID', $this->user_id)->where('PREPS_ID', $id)->get('LIKED_PREPS')->result_array();

        if (!empty($fav))
            $this->data['in_fav'] = true;

        $this->data['prices'] = $this->db->query('SELECT MIN(PRICE) MIN, MAX(PRICE) MAX FROM TRADE_BALANCES WHERE NOMEN_ID = '.$id.' GROUP BY NOMEN_ID')->row_array();

        $this->load->view('main/medicine', $this->data);
    }
}