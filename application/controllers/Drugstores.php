<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drugstores extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->data['controller']   = $this->router->fetch_class();
        $this->data['method']       = $this->router->fetch_method();

        $this->user_id = $this->session->userdata('USER_ID');

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Carousel_model');
        $this->load->model('Action_model');
        $this->load->model('Banner_model');
        $this->load->model('News_model');
        $this->load->model('Institution_model');
        $this->load->model('Articles_model');
        $this->load->model('Geo_model');
        $this->load->model('Logos_model');
        $this->load->model('Post_model');
        $this->load->model('Voting_model');

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $this->data['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $this->data['current_city'] = $cookie_city_id;
        } else {
            $this->data['current_city'] = 4400;
        }

        //Получаем две случайные акции для блока на главной
        $actions['actions'] = $this->Action_model->get_random_list(2, 4);

        foreach ($actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $actions['actions'][$key]['DESCRIPTION'] = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $actions['actions'][$key]['PRICE'] = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        // Получаем акции для вывода на карту
        $map_actions['actions'] = $this->Action_model->get_random_map(4, 4);

        foreach ($map_actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $map_actions['actions'][$key]['DESCRIPTION']    = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $map_actions['actions'][$key]['PRICE']              = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        $carousel = $this->Carousel_model->get_images(1);
        $actions_quantity        = $this->Action_model->count();
        $actions['quantity']     = $actions_quantity;
        $map_actions['quantity'] = $actions_quantity;
        $map_actions['green'] = false;
        $popular_institutions = $this->Institution_model->get_popular();
        $first_banners  = $this->Banner_model->get_banners(1);
        $second_banners = $this->Banner_model->get_banners(1);
        $news = $this->News_model->get_last();
        $map_actions['types'] = $this->Institution_model->get_types();

        // выбираем три рандомные категории
        $popular_themes_category = $this->Articles_model->get_random_themes();

        $popular_themes_category_ids = null;

        foreach ($popular_themes_category as $item) {
            $popular_themes_category_ids[] = $item['ID'];
        }

        $popular_themes = $this->Articles_model->get_articles_by_ids_array($popular_themes_category_ids);

        foreach ($popular_themes as $popular_theme) {
            $popular_themes_data['themes'][$popular_theme['CATEGORY_ID']][] = [
                'ID'        => $popular_theme['ID'],
                'NAME'      => $popular_theme['NAME']
            ];
        }

        $popular_themes_data['category'] = $popular_themes_category;

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);
        $cities['current_city'] = $this->data['current_city'];

        $logos = $this->Logos_model->get_logos();

        $voting = $this->Voting_model->get_vote();
        $voting['is_vote'] = $this->Voting_model->is_vote($voting['info']['ID'], $this->user_id);

        $this->data['templates'] = [
            'city_selector'     => $this->load->view('tmp/city_selector', $cities, true),
            'auth'              => ($this->user_id)
                ? $this->load->view('tmp/auth_entered', [
                    'data' => $this->User_model->get_by_id($this->user_id),
                    'msg_count' => $this->Post_model->get_by_user_id_in_count_unread($this->user_id)[0]['COUNT(*)']
                ], true)
                : $this->load->view('tmp/auth', null, true),
            'carousel'          => $this->load->view('tmp/carousel', ['images' => $carousel], true),
            'menu'              => $this->load->view('tmp/menu', ['controller' => $this->data['controller']], true),
            'search'            => $this->load->view('tmp/search', null, true),
            'ask_question'      => $this->load->view('tmp/ask_question', ['is_auth' => (!empty($this->user_id)) ? true : false], true),
            'actions'           => $this->load->view('tmp/actions', $actions, true),
            'logos'             => $this->load->view('tmp/logos', ['logos' => $logos], true),
            'map_search'        => $this->load->view('tmp/map_search', $map_actions, true),
            'popular_institutions'  => $this->load->view('tmp/popular_institutions', ['institutions' => $popular_institutions], true),
            'first_banners'     => $this->load->view('tmp/banners', ['banners' => $first_banners], true),
            'second_banners'    => $this->load->view('tmp/banners', ['banners' => $second_banners], true),
            'popular_themes'    => $this->load->view('tmp/popular_themes', $popular_themes_data, true),
            'voiting'           => $this->load->view('tmp/voiting', $voting, true),
            'news'              => $this->load->view('tmp/news', ['news' => $news], true),
            'bottom_menu'       => $this->load->view('tmp/bottom_menu', null, true),
            'footer'            => $this->load->view('tmp/footer', ['user' => $this->user_id], true)
        ];
    }

    public function index()
    {
        $this->load->view('main/drugstores', $this->data);
    }

    public function catalog()
    {
        $per_page = 10;
        $pages = 0;
        $this->data['type'] = 'Все мед.учреждения';
        $type_id = null;

        if (!empty($_GET['type_id'])) {
            $type_id = (int)$_GET['type_id'];
            $type_name = $this->db->query("SELECT NAME FROM INSTITUTION_TYPES WHERE ID = $type_id")->row_array()["NAME"];
            if (!empty($type_name))
                $this->data['type'] = $type_name;
        }
        if (!empty($_GET['pages']))
            $pages = (int)$_GET['pages'];

        $this->data['catalog'] = $this->Institution_model->get_catalog(4, $type_id, $per_page, $pages);
        $catalog_quantity = $this->Institution_model->get_catalog_quantity(4, $type_id);

        $this->load->library('pagination');

//        $data['url_with_params'] = $_SERVER['PATH_INFO']."?";

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = 'Следующая';
        $config['prev_link'] = 'Предыдущая';
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<p style="margin-top: 10px;">';
        $config['full_tag_close'] = '</p>';
        $config['num_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['num_tag_close'] = '</span>';
        $config['prev_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['prev_tag_close'] = '</span>';
        $config['cur_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['cur_tag_close'] = '</span>';
        $config['last_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['last_tag_close'] = '</span>';
        $config['next_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['next_tag_close'] = '</span>';


        $this->pagination->initialize($config);

        $this->data['pagination'] = $this->pagination->create_links();

        $this->load->view('main/drugstores_catalog', $this->data);
    }

    public function item($id = null)
    {
        $id = (int)$id;
        $this->data['item'] = $this->Institution_model->get_drugstore($id);

        if (empty($this->data['item']))
            show_404();

        $where = "BRANCHES.ID != $id AND BRANCHES.INSTITUTION_ID = ".$this->data['item']['ID'];

        $this->data['another_drugstores'] = $this->Institution_model->get_catalog(4, null, null, null, $where);
        $this->data['branchid'] = (int)$id;

        $this->load->view('main/drugstore', $this->data);
    }
}