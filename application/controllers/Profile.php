<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{

    private $user_id;
    private $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        /*подключаем хэлперы - они содержат нужные функции*/
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('mfp');
        /*загружаем модель, чтоб можно было использовать её методы*/
        $this->load->model('User_model');
        $this->load->model('Institution_model');
        $this->load->model('Institution_files_model');
        $this->load->model('Institution_photos_model');
        $this->load->model('Doctor_model');
        $this->load->model('Doctor_files_model');
        $this->load->model('Doctor_refreshers_model');
        $this->load->model('Med_card_model');
        $this->load->model('Med_card_files_model');
        $this->load->model('Post_model');
        $this->load->model('Orgnisations');
        $this->load->model('Specialist');
        $this->load->model('Post_files_model');
        $this->load->model('Post_destinations_model');
        $this->load->model('Target_model');
        $this->load->model('Address_book_model');
        $this->load->model('Liked_model');
        $this->load->model('Institution_specialists_model');
        $this->load->model('Specialist_comments_model');
        $this->load->model('Institution_comments_model');
        $this->load->model('New_organisations_model');
        $this->load->model('Soc_net_model');
        $this->load->model('Patients_model');
        $this->load->model('Branches_model');
        $this->load->model('Metro_model');
        $this->load->model('Pays_model');
        $this->load->model('Checks_model');
        $this->load->model('Geo_model');
        $this->load->model('Institution_metros_model');
        $this->load->model('Branch_metros_model');
        $this->load->model('Geo_model');
        /*получаем данные сессии пользователя*/
        $this->user_id = $this->session->userdata('USER_ID');
    }
    /**
     * [_mainMenuData - формирует данные для главного меню]
     * @param  [array] $unreaded [запись из БД о числе непрочтённых писем]
     * @return [array] $data [массив данных для формирования главного меню]
     */
    private function _mainMenuData($unreaded)
    {
        $data['user']=$this->User_model->get_by_id($this->user_id);
        if($unreaded)
            $unread=$unreaded[0]['COUNT(*)'];
        else
            $unread=0;
        /*пункты могут отображаться у всех типов пользователей (А) или у конкретных - 1, 2, 3 - пользоваетли, врачи и учреждения соответственно*/
        $items=[
            'личные данные' =>['style'=>0,'user_type'=>'A','user_type2'=>'A','href'=>'/profile/index','search'=>'N'],
            'об учреждении' =>['style'=>1,'user_type'=>3,'user_type2'=>3,'href'=>'/profile/organisation','search'=>'N'],
            'О СПЕЦИАЛИСТЕ' =>['style'=>1,'user_type'=>2,'user_type2'=>2,'href'=>'/profile/specialist','search'=>'N'],
            'сообщения'     =>['style'=>2,'user_type'=>'A','user_type2'=>'A','href'=>'/profile/posts','search'=>'/profile/ajaxSearchPosts','submenu'=>[
                    'Входящие'     =>['href'=>'/profile/posts/in','unreaded'=>$unread],
                    'Отправленные' =>['href'=>'/profile/posts/out'],
                    'Важные'       =>['href'=>'/profile/posts/liked'],
                    'Удаленные'    =>['href'=>'/profile/posts/delete']
                ]],
             'ОТЗЫВЫ'     =>['style'=>7,'user_type'=>2,'user_type2'=>2,'href'=>'/profile/specialist_comments','search'=>'N'],
            'ОТЗЫВЫ '    =>['style'=>7,'user_type'=>3,'user_type2'=>3,'href'=>'/profile/institution_comments','search'=>'N'],    
            'КОНТЕНТ'    =>['style'=>3,'user_type'=>3,'user_type2'=>2,'href'=>'/profile/organisation_content','search'=>'N'],
            'ОТДЕЛЕНИЯ'  =>['style'=>4,'user_type'=>3,'user_type2'=>3,'href'=>'/profile/organisation_branches','search'=>'N'],
            'СПЕЦИАЛИСТЫ'      =>['style'=>5,'user_type'=>3,'user_type2'=>3,'href'=>'/profile/institution_specialists','search'=>'/profile/institution_specialists_search'],
            'КЛИЕНТЫ'    =>['style'=>6,'user_type'=>2,'user_type2'=>2,'href'=>'/profile/specialist_patients','search'=>'N'],
            'КЛИЕНТЫ '   =>['style'=>6,'user_type'=>3,'user_type2'=>3,'href'=>'/profile/institution_patients','search'=>'N'],
            'АКЦИИ'      =>['style'=>6,'user_type'=>3,'user_type2'=>3,'href'=>'/profile/actions','search'=>'N'],
           
            'ИЗБРАННОЕ'  =>['style'=>8,'user_type'=>1,'user_type2'=>2,'href'=>'/profile/liked','search'=>'/profile/liked_search'],
            'МЕДКАРТА'   =>['style'=>9,'user_type'=>1,'user_type2'=>1,'href'=>'/profile/med_card','search'=>'N'],
            'ЦЕЛИ'       =>['style'=>4,'user_type'=>1,'user_type2'=>2,'href'=>'/profile/targets','search'=>'N','submenu'=>[
                    'Избранное'=>['href'=>'/profile/targets/liked']
                ]],
            'ГРАФИК' =>['style'=>10,'user_type'=>1,'user_type2'=>2,'href'=>'/profile/schedule_day','search'=>'N'],
            'оплата сервиса'=>['style'=>1,'user_type'=>2,'user_type2'=>3,'href'=>'/profile/pay','search'=>'N'],
            'ВЫХОД'  =>['style'=>'exit','user_type'=>'A','user_type2'=>'A','href'=>'javascript:void(0);','search'=>'N']
        ];/*определение того, какой метод контроллера вызвал страницу*/
        $data['controller_method']='/'.$this->router->fetch_class().'/'.$this->router->fetch_method();
        $data['mainMenuItems']=[];
        foreach ($items as $key => $item)/*выбор пунктов по типам пользователей*/
            if($data['user']['TYPE']==$item['user_type'] or $data['user']['TYPE']==$item['user_type2'] or $item['user_type']=='A')
                $data['mainMenuItems'][$key]=$item;
        return $data;
    }
    /**
     * [_formatTime - форматирует дату и время из записи]
     * @param  [array] $post [запись из БД]
     * @return [array] $post [запись из БД с отформатированными временными полями]
     */
    private function _formatTime($post)
    {    /*в базе дата и время хранятся в виде - 0000-00-00 00:00 - а на сайте - 00/00/0000 00:00*/
        $dateTimeToArray=explode(' ',!empty($post['START']) ? $post['START'] : $post['TIME']);
        $dateToArray=explode('-',$dateTimeToArray[0]);
        $post['DATE']=$dateToArray[2].'/'.$dateToArray[1].'/'.$dateToArray[0];
        $post['TIME']=substr($dateTimeToArray[1],0,-3);
        return $post;
    }
    /**
     * [_formatPost - приводит письмо к форме вывода]
     * @param  [array] $post [письмо]
     * @return [array] $post [письмо с нужными полями]
     */
    private function _formatPost($post)
    {
        $post=$this->_formatTime($post);
        /*файлы, приложенные к письму*/
        $post['FILES']=$this->Post_files_model->get_by_post_id($post['ID']);
        if($post['TO_ADMIN'] == 1)
        {/*если получатель - администратор, то выводятся его данные*/
            $post['ID_DESTINATION']='admin';
            $post['DESTINATION_NAME']='Администратор';
            if($post['ID_DESTINATION']!=$this->user_id)
                $post['DESTINATION_READED']=1;
        }/*если получатель - это мы, то выводятся данные отправителя*/
        elseif($post['ID_DESTINATION']==$this->user_id)
        {
            $post['ID_DESTINATION']=$post['ID_SENDER'];
            $post['DESTINATION_NAME']=$post['NAME_SENDER'];
        }
        else
            $post['DESTINATION_READED']=1;
        $have_resp=$post['ID_RESPONDED_POST'];
        $resps=[];
        $respsI=0;
        while(!empty($have_resp))
        {    /*формирование цепочки писем, ответом на которые является данное письмо*/
            $resps[$respsI]=$this->Post_model->get_by_id($have_resp);
            $resps[$respsI]=$this->_formatTime($resps[$respsI]);
            $have_resp=$resps[$respsI]['ID_RESPONDED_POST'];
            $respsI=$respsI+1;
        }
        $post['RESPONDEDS']=$resps;
        return $post;
    }
    /**
     * [_imagesPost в базу вносятся приложенные к письму файлы]
     * @param  [int] $insert_post_id [ИД письма в базе]
     * @return [json]                [отчёт об успешной отправке письма]
     */
    private function _imagesPost($insert_post_id)
    {
        if(!empty($_POST['images']))
            foreach ($_POST['images'] as $imagePath)
                if(is_file(FCPATH.$imagePath))
                {    /*если файл есть - из временной папки он перекидывается в постоянную*/
                    $fileName=strrchr($imagePath,'/');
                    rename(FCPATH.$imagePath, FCPATH.'/images'.$fileName);
                    $imageFields = [
                        'POST_ID' =>$insert_post_id,
                        'FILE'    =>"/images".$fileName
                    ];/*файлы добавляются в БД*/
                    $insert_image_id = $this->Post_files_model->create($imageFields);
                }
    }
    /**
     * [_postsData - формирует данные писем]
     * @param  [string] $category [категория писем - входящие, исходящие, корзина, избранное]
     * @param  [string] $search   [поисковый запрос]
     * @return [array] $data        [итоговый массив писем]
     */
    private function _postsData($category,$search)
    {    /*число непрочтённых писем*/
        $unreaded=$this->Post_model->get_by_user_id_in_count_unread($this->user_id);
        $data=$this->_mainMenuData($unreaded);
        /*данные для адресной книги*/
        $data['addressUsers']=$this->Address_book_model->get_by_user_id_users($this->user_id);
        $data['addressSpecialists']=$this->Address_book_model->get_by_user_id_specialists($this->user_id);
        $data['addressInstitutions']=$this->Address_book_model->get_by_user_id_institutions($this->user_id);
        /*подгружаем кусок страницы с данными для адресной книги*/
        $data['addressBook'] = $this->load->view("tmp/profile-templates/address_book", $data, true);
        $data['category']=$category;
        if($category=='out')
            $data['posts']=$this->Post_model->get_by_user_id_out($this->user_id,$search);
        if($category=='in')
            $data['posts']=$this->Post_model->get_by_user_id_in($this->user_id,$search);
        if($category=='all')
            $data['posts']=$this->Post_model->get_by_user_id_all($this->user_id,$search);
        if($category=='delete')
            $data['posts']=$this->Post_model->get_by_user_id_delete($this->user_id,$search);
        if($category=='liked')
            $data['posts']=$this->Post_model->get_by_user_id_liked($this->user_id,$search);
        $posts=[];
        foreach ($data['posts'] as $key=>$post)
            $posts[$key]=$this->_formatPost($post);
        $data['posts']=$posts;
        return $data;
    }
    /**
     * [_postSenderData формирование данных отправителя письма]
     * @return [array] $data [массив данных отправителя письма]
     */
    private function _postSenderData($id='')
    {
          
        $user=$this->User_model->get_by_id($this->user_id);
        $fields = [
            'ID_SENDER'     =>$this->user_id,
            'MESSAGE'       =>$_POST['message'],
            'TIME'          =>mdate("%Y-%m-%d %H:%i:%s",time()),
            'THEME'         =>$_POST['theme'],
            'DELETED'       =>0,
            'FINAL_DELETED' =>0
        ];/*в зависимости от типа пользователя в письме он отображается как юзер, учреждение или врач*/
       
        if($user['TYPE']==3)
        {
            
            //$idsender=$this->db->query("SELECT ID_SENDER FROM POSTS WHERE ID =".(int)$id)->row_array();
            
            
        
        $namesender=$this->db->query("SELECT FIRSTNAME,LASTNAME,MIDDLENAME FROM USERS WHERE ID =".(int)$this->user_id)->row_array();
       
       
           // $institutionThis=$this->Institution_model->get_by_user_id($this->user_id);
            $fields['NAME_SENDER']=$namesender['LASTNAME'].' '.$namesender['FIRSTNAME'].' '.$namesender['MIDDLENAME'];
           
           
            
        }
        elseif($user['TYPE']==2)
        {
             
            //$specialistThis=$this->Doctor_model->get_by_user_id($this->user_id);
            //$fields['NAME_SENDER']=$specialistThis['LAST_NAME'].' '.$specialistThis['FIRST_NAME'];
              $namesender=$this->db->query("SELECT FIRSTNAME,LASTNAME,MIDDLENAME FROM USERS WHERE ID =".(int)$this->user_id)->row_array();
       
       
           // $institutionThis=$this->Institution_model->get_by_user_id($this->user_id);
            $fields['NAME_SENDER']=$namesender['LASTNAME'].' '.$namesender['FIRSTNAME'].' '.$namesender['MIDDLENAME'];
        }
        else
        {
            
            $fields['NAME_SENDER']=$user['LASTNAME'].' '.$user['FIRSTNAME']; 
        }
           
        if(!empty($_POST['responded_post_id']))
            $fields['ID_RESPONDED_POST']=$_POST['responded_post_id'];
        $data['user']=$user;
        $data['fields']=$fields;
        return $data;
    }
    /**
     * [_createAddressUser ввод пользователя в адресную книгу]
     * @param  [aaray] $destination [кого вводят]
     * @param  [array] $user        [чья адресная книга]
     * @return              [запись в БД]
     */
    private function _createAddressUser($destination,$user)
    {
        if($this->Address_book_model->get_user_by_user_id($destination['ID'],$user['ID'])==NULL)
        {
            $fieldsAddress = [
                'ID_USER_ADDRESS' =>$destination['ID'],
                'USER_ID'         =>$user['ID'],
                'NAME_USER'       =>$destination['LASTNAME'].' '.$destination['FIRSTNAME']
            ];
            $insert_address_id=$this->Address_book_model->create_user($fieldsAddress);
        }
    }
    /**
     * [_createAddressUser ввод врача в адресную книгу]
     * @param  [aaray] $destination [кого вводят]
     * @param  [array] $user        [чья адресная книга]
     * @return              [запись в БД]
     */
    private function _createAddressSpecialist($destination,$user)
    {
        $specialist=$this->Doctor_model->get_by_user_id($destination['ID']);
        if($this->Address_book_model->get_specialist_by_user_id($specialist['ID'],$user['ID'])==NULL)
        {
            $fieldsAddress = [
                'ID_SPECIALIST'  =>$specialist['ID'],
                'USER_ID'        =>$user['ID'],
                'NAME_SPECIALIST'=>$specialist['LAST_NAME'].' '.$specialist['FIRST_NAME']
            ];
            $insert_address_id=$this->Address_book_model->create_specialist($fieldsAddress);
        }
    }
    /**
     * [_createAddressUser ввод учреждения в адресную книгу]
     * @param  [array] $destination [кого вводят]
     * @param  [array] $user        [чья адресная книга]
     * @return              [запись в БД]
     */
    private function _createAddressInstitution($destination,$user)
    {
        $institution=$this->Institution_model->get_by_user_id($destination['ID']);
        if($this->Address_book_model->get_institution_by_user_id($institution['ID'],$user['ID'])==NULL)
        {
            $fieldsAddress = [
                'ID_INSTITUTION'  =>$institution['ID'],
                'USER_ID'         =>$user['ID'],
                'NAME_INSTITUTION'=>$institution['NAME']
            ];
            $insert_address_id=$this->Address_book_model->create_institution($fieldsAddress);
        }
    }
    /**
     * [_likedData - формирует общие для избранного данные]
     * @return [array] $data [итоговый массив данных]
     */
    private function _likedData()
    {
        $data=$this->_mainMenuData(false);
        $data['count_liked_specialists'] = $this->Liked_model->get_by_count_liked($this->user_id, 'LIKED_SPECIALISTS');
        $data['count_liked_institutions'] = $this->Liked_model->get_by_count_liked($this->user_id, 'LIKED_INSTITUTIONS');
        $data['count_liked_preps'] = $this->Liked_model->get_by_count_liked($this->user_id, 'LIKED_PREPS');
        $data['count_liked_articles'] = $this->Liked_model->get_by_count_liked($this->user_id, 'LIKED_ARTICLES');
        return $data;
    }
    /**
     * [_institutionSpecialistsData - формирует общие для специалистов учреждения данные]
     * @param  [string] $q - поисковый запрос
     * @return [array] $data [итоговый массив данных]
     */
    private function _institutionSpecialistsData($q=' ')
    {
        if(!empty($q))
        {
            $institution = $this->Institution_specialists_model->get_by_institution_id($this->user_id);
            $data=$this->_mainMenuData(false);
            $data['count_institution_specialists_specialization'] = $this->Institution_specialists_model->get_by_count_institution_specialists_specialization($institution['ID']);
            $data['count_institution_specialists_specialization_id'] = $this->Institution_specialists_model->get_by_count_institution_specialists_specialization_id($institution['ID']);
            $data['institution_specialists'] = $this->Institution_specialists_model->get_by_institution_specialists($institution['ID'],$q);
            return $data;
        }
        else
            show_404();
    }
    /**
     * [_userSocNet - вносит в БД данные профиля юзера в соцсети]
     * @param  [int] $netID - ИД соцсети
     * @param  [int] $userID - ИД юзера в соцсети
     * @return [запись о странице соцсети юзера вносится в БД]
     */
    private function _userSocNet($netID,$userID)
    {
        $soc_net=$this->Soc_net_model->get_by_id($netID);
        $fields=[
            'USER_ID'=>$this->user_id,
            'SOC_PAGE'=>$soc_net['SOC_PAGE'].$userID,
            'ICON'=>$soc_net['ICON'],
            'SOC_NET_ID'=>$netID
        ];
        if($insert_id=$this->Soc_net_model->create($fields))
            redirect('/profile');
    }
    /**
     * [_fetchLinkedin получает данные профиля пользоваетля в Linkedin]
     * @param  [string] $method       [в данном случае - GET]
     * @param  [string] $resource     [раздел сайта]
     * @param  [string] $access_token [токен доступа]
     * @return [string] $response     [данные юзера]
     */
    private function _fetchLinkedin($method, $resource, $access_token)
    {
        $opts = array(
            'http' => array(
                'method' => $method,
                'header' => "Authorization: Bearer " . 
                $access_token."\r\n" . 
                "x-li-format: json\r\n"
            )
        );
        $url = 'https://api.linkedin.com' . $resource;
        $context = stream_context_create($opts);
        $response = file_get_contents($url, false, $context);
        return json_decode($response);
    }
    /**
     * [_curlSocNetToken получение токена доступа через curl]
     * @param  [string] $get_code      [код от соцсети]
     * @param  [string] $auth_url      [ссылка авторизации]
     * @param  [string] $client_id     [ИД приложения]
     * @param  [string] $client_secret [секретный ключ приложения]
     * @param  [string] $token_url     [ссылка получкения токена]
     * @return [string] $result        [строка с токеном]
     */
    private function _curlSocNetToken($get_code,$auth_url,$client_id,$client_secret,$token_url)
    {
        $params = [
            'code' => $get_code,
            'redirect_uri' => 'https://'.$_SERVER['SERVER_NAME'].$auth_url,
            'grant_type' => 'authorization_code',
            'client_id' => $client_id,
            'client_secret' => $client_secret
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $token_url); /*url, куда будет отправлен запрос*/
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params))); /*передаём параметры*/
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
    /**
     * [_specialistDetail страница отзывов на врача]
     * @param  [int] $specialist_id [ИД специалиста]
     * @return [array] $data        [массив отзывов]
     */
    private function _specialistDetail($specialist_id)
    {
        $data=$this->_mainMenuData(false);
        $data['comments']=$this->Specialist_comments_model->get_by_specialist_id($specialist_id);
        $comments=[];
        foreach ($data['comments'] as $comment)
        {
            $dateComment=strtotime($comment['DATE']);
            $comment['DATE']=mdate("%d-%m-%Y",$dateComment);
            $comments[]=$comment;
        }
        $data['comments']=$comments;
        return $data;
    }
    /**
     * [_patients страница данных пациентов]
     * @param  [array] $patientsData [массив данных пациентов]
     * @return [загрузка страницы пациентов]
     */
    private function _patients($patientsData)
    {
        $data=$this->_mainMenuData(false);
        $patients=[];
        if(!empty($patientsData)){
            foreach ($patientsData as $patient)
        {
            $datePatient=strtotime($patient['RECEPTION_DATE']);
            $patient['RECEPTION_DATE']=mdate("%d.%m.%Y",$datePatient);
            $patients[]=$patient;
        } 
        }
       
        $data['patients']=$patients;
        //print_r($data['patients']);
         $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view("profile/patients",$data);
        $this->load->view("tmp/profile-templates/footer");
    }
    /**
     * [_twitterConstants установка констант для параметров авторизации в твиттере]
     */
    private function _twitterConstants()
    {
        define('CONSUMER_KEY', 'To8zyAYs9MjsvfS77rqgrg2dJ');
        define('CONSUMER_SECRET', '0AFHPHIcwAuXbK8vUxvR1wbmOewn8kngnR3inZN4FW1BcZy5Nw');
        define('REQUEST_TOKEN_URL', 'https://api.twitter.com/oauth/request_token');
        define('AUTHORIZE_URL', 'https://api.twitter.com/oauth/authorize');
        define('ACCESS_TOKEN_URL', 'https://api.twitter.com/oauth/access_token');
        define('ACCOUNT_DATA_URL', 'https://api.twitter.com/1.1/users/show.json');
        define('CALLBACK_URL', 'https://'.$_SERVER['SERVER_NAME'].'/profile/tw_auth');
        define('URL_SEPARATOR', '&');
    }
    /**
     * [_twitterAuthLink формирование ссылки для авторизации в твиттере]
     * @return [string] [ссылка для авторизации в твиттере]
     */
    private function _twitterAuthLink()
    {
        $this->_twitterConstants();
        $oauth_nonce = md5(uniqid(rand(), true));
        $oauth_timestamp = time();
        $params = [
            'oauth_callback=' . urlencode(CALLBACK_URL) . URL_SEPARATOR,
            'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
            'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
            'oauth_signature_method=HMAC-SHA1'.URL_SEPARATOR,
            'oauth_timestamp='.$oauth_timestamp.URL_SEPARATOR,
            'oauth_version=1.0'
        ];
        $oauth_base_text = implode('', array_map('urlencode', $params));
        $oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(REQUEST_TOKEN_URL) . URL_SEPARATOR . $oauth_base_text;
        $params = [
            URL_SEPARATOR . 'oauth_consumer_key=' . CONSUMER_KEY,
            'oauth_nonce=' . $oauth_nonce,
            'oauth_signature=' . urlencode(base64_encode(hash_hmac('sha1', $oauth_base_text, CONSUMER_SECRET.URL_SEPARATOR, true))),
            'oauth_signature_method=HMAC-SHA1',
            'oauth_timestamp='.$oauth_timestamp,
            'oauth_version=1.0'
        ];
        $url = REQUEST_TOKEN_URL.'?oauth_callback='.urlencode(CALLBACK_URL).implode('&', $params);
        parse_str(file_get_contents($url), $response);
        $oauth_token = $response['oauth_token'];
        $oauth_token_secret = $response['oauth_token_secret'];
        return AUTHORIZE_URL.'?oauth_token='.$oauth_token;
    }
    /**
     * [_updateProfile изменение профиля пользователя]
     * @param  [string] $email [емэйл]
     * @param  [string] $phone [телефон]
     * @return [string]        [отчёт об успешном изменении данных]
     */
    private function _updateProfile($email,$phone)
    {
        $fields = [
            'EMAIL'      =>$email,
            'PHONE'      =>$phone,
            'FIRSTNAME'  =>$_POST['firstname'],
            'LASTNAME'   =>$_POST['lastname'],
            'MIDDLENAME' =>$_POST['middlename']
        ];
        $update_id = $this->User_model->update($fields,$this->user_id);
        /*у специалистов нет отдельного поля ввода емэйла, но емэйл в базе им нужен*/
        if($specialist_exist=$this->Doctor_model->get_by_user_id($this->user_id))
            $update_id = $this->Doctor_model->update(['EMAIL'=>$email],$this->user_id);
        echo json_encode(['success'=>true,'msg'=>'Ваши данные успешно изменены!']);
    }
    /**
     * [_allScheduleData общие данные графиков целей]
     * @param  [array] $data    [исходный массив данных]
     * @param  [array] $targets [массив целей]
     * @param  [string] $dateCur [момент, разделяющий время на текущий и следующий (день, неделю, месяц)]
     * @return [description]
     */
    private function _allScheduleData($data,$targets,$dateCur)
    {
        $targetsCurView=[];
        $targetsNextView=[];
        foreach ($targets as $value)
            if($value['START']<=$dateCur)
                $targetsCurView[]=$this->_formatTime($value);
            else
                $targetsNextView[]=$this->_formatTime($value);
        /*массивы целей текущего и следующего периода соответственно*/
        $data['targetsCur']=$targetsCurView;
        $data['targetsNext']=$targetsNextView;
        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view("tmp/profile-templates/schedule_nav");
    }

    private function _likedOrDeletePost($liked,$result)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $post=$this->Post_model->get_by_id($_POST['target-id']);
            if ($post['ID_SENDER']==$this->user_id)
                $update_id = $this->Post_model->update(['LIKED'=>$liked],$_POST['target-id']);
            else
                $update_id = $this->Post_destinations_model->update(['LIKED'=>$liked],$_POST['target-id'],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Письмо '.$result]);
        }
        else
            echo json_encode(['error'=>'Выберите письмо!']);
    }

    private function _deleteOrRestorePost($liked,$result)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $post=$this->Post_model->get_by_id($_POST['target-id']);
            if ($post['ID_SENDER']==$this->user_id)
                $update_id = $this->Post_model->update(['DELETED'=>$liked],$_POST['target-id']);
            else
                $update_id = $this->Post_destinations_model->update(['DELETED'=>$liked],$_POST['target-id'],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Письмо '.$result]);
        }
        else
            echo json_encode(['error'=>'Выберите письмо!']);
    }

    private function _addressBookData()
    {
        $data=$this->_mainMenuData(false);
        /*данные для адресной книги*/
        $data['addressUsers']=$this->Address_book_model->get_by_user_id_users($this->user_id);
        $data['addressSpecialists']=$this->Address_book_model->get_by_user_id_specialists($this->user_id);
        $data['addressInstitutions']=$this->Address_book_model->get_by_user_id_institutions($this->user_id);
        /*кусок страницы для адресной книги*/
        $data['addressBook'] = $this->load->view("tmp/profile-templates/address_book", $data, true);
        return $data;
    }

    private function _addressDestinationPost($insert_post_id,$email_destination,$data)
    {    /*вбиваем получателя*/
        $fieldsDestination = [
            'ID_POST'            =>$insert_post_id,
            'DELETED'            =>0,
            'FINAL_DELETED'      =>0,
            'DESTINATION_READED' =>0,
            'TO_ADMIN'           =>0
        ];
        if($institution_exist=$this->Institution_model->get_by_email($email_destination) != NULL)
        {
            
            $institutionDestination=$this->Institution_model->get_by_email($email_destination);
            $userDestination=$this->User_model->get_by_id($institutionDestination['USER_ID']);
            $fieldsDestination['ID_DESTINATION']=$userDestination['ID'];
            $fieldsDestination['DESTINATION_NAME']=$institutionDestination['NAME'];
        }
        elseif($specialist_exist=$this->Doctor_model->get_by_email($email_destination) != NULL)
        {
            $specialistDestination=$this->Doctor_model->get_by_email($email_destination);
            $userDestination=$this->User_model->get_by_id($specialistDestination['USER_ID']);
            $fieldsDestination['ID_DESTINATION']=$userDestination['ID'];
            $fieldsDestination['DESTINATION_NAME']=$specialistDestination['LAST_NAME'].' '.$specialistDestination['FIRST_NAME'];
        }
        else
        {
            $userDestination=$this->User_model->get_by_email($email_destination);
            $fieldsDestination['ID_DESTINATION']=$userDestination['ID'];
            $fieldsDestination['DESTINATION_NAME']=$userDestination['LASTNAME'].' '.$userDestination['FIRSTNAME'];
        }
        if($insert_destination_id = $this->Post_destinations_model->create($fieldsDestination))
        {
            $this->_imagesPost($insert_post_id);
            if($userDestination['TYPE']==1)
                $this->_createAddressUser($userDestination,$data['user']);
            elseif($userDestination['TYPE']==2)
            {
                $this->_createAddressSpecialist($userDestination,$data['user']);
                $this->_addSpecialistPatient($userDestination,$data['user']);
            }
            elseif($userDestination['TYPE']==3)
            {
                $this->_createAddressInstitution($userDestination,$data['user']);
                $this->_addInstitutionPatient($userDestination,$data['user']);
            }
            if($data['user']['TYPE']==1)
                $this->_createAddressUser($data['user'],$userDestination);
            elseif($data['user']['TYPE']==2)
                $this->_createAddressSpecialist($data['user'],$userDestination);
            elseif($data['user']['TYPE']==3)
                $this->_createAddressInstitution($data['user'],$userDestination);
        }
    }

    private function _addInstitutionPatient($destination,$user)
    {
        $institution=$this->Institution_model->get_by_user_id($destination['ID']);
        if($this->Patients_model->get_by_institution_id_user_id($institution['ID'],$user['ID'])==NULL)
        {
            $fieldsPatient = [
                'INSTITUTION_ID'  =>$institution['ID'],
                'USER_ID'         =>$user['ID'],
                'USER_NAME'       =>$user['LASTNAME'].' '.$user['FIRSTNAME'],
                'USER_EMAIL'      =>$user['EMAIL'],
                'USER_PHONE'      =>$user['PHONE'],
                'RECEPTION_DATE'  =>date('Y-m-d'),
                'USER_CITY'       =>'Не указан'
            ];
            $insert_patient_id=$this->Patients_model->create($fieldsPatient);
        }
    }

    private function _addSpecialistPatient($destination,$user)
    {
        $specialist=$this->Doctor_model->get_by_user_id($destination['ID']);
        $branch=$this->Branches_model->get_by_id($specialist['BRANCH_ID']);
        if($this->Patients_model->get_by_specialist_id_user_id($specialist['ID'],$user['ID'])==NULL)
        {
            $fieldsPatient = [
                'INSTITUTION_ID'  =>$branch['INSTITUTION_ID'],
                'SPECIALIST_ID'   =>$specialist['ID'],
                'USER_ID'         =>$user['ID'],
                'USER_NAME'       =>$user['LASTNAME'].' '.$user['FIRSTNAME'],
                'USER_EMAIL'      =>$user['EMAIL'],
                'USER_PHONE'      =>$user['PHONE'],
                'RECEPTION_DATE'  =>date('Y-m-d'),
                'USER_CITY'       =>'Не указан'
            ];
            $insert_patient_id=$this->Patients_model->create($fieldsPatient);
        }
    }

/******************************************************
 * Страница выбора выбора типа аккаунта и регистрации *
 ******************************************************/
    public function registration($type = null)
    {    /*если пользователь авторизован, оправляем его на личную страницу*/
        if ($this->user_id)
            redirect('/profile');

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $cities['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $cities['current_city'] = $cookie_city_id;
        } else {
            $cities['current_city'] = 4400;
        }

        $this->data['templates']['city_selector'] = $this->load->view("tmp/city_selector", $cities, true);
        /*выбираем тип пользователя*/
        if (empty($type))
            $this->load->view("profile/registration_first_step", $this->data);
        else
        {
            if (!empty($_GET['accept_user_agreement']) && $_GET['accept_user_agreement'] == 'true') {
                switch ($type) {
                    case 'user':
                        $type_id = 1;
                        $this->data['registration_title'] = "Регистрация пользователя";
                        break;
                    case 'doctor':
                        $type_id = 2;
                        $this->data['registration_title'] = "Регистрация специалиста";
                        break;
                    case 'company':
                        $type_id = 3;
                        $this->data['registration_title'] = "Регистрация компании";
                        break;
                    default:
                        show_404();
                }
                $this->data['forms']['registration']['email'] = (!empty($_POST['email'])) ? $_POST['email'] : "";
                /*ищем ошибки*/
                if (isset($_POST['register_button'])) {
                    if (empty($_POST['email']))
                        $error = "Поле \"Эл.почта\" не заполнено";
                    elseif (!checkEmail($email = $_POST['email']))          //Проверяем, корректна ли почта
                        $error = "Электронная почта введена некорректно";
                    elseif (empty($_POST['password']))
                        $error = "Поле \"Пароль\" не заполнено";
                    elseif (strlen($password = $_POST['password']) < 6)
                        $error = "Длина пароля должна быть 6 и более символов";
                    elseif (empty($_POST['confirmation']))
                        $error = "Пароль не подтвержден";
                    elseif ($password !== $confirmation = $_POST['confirmation'])
                        $error = "Введенные пароли не совпадают";
                    /*TODO Соглашение с договором офертой?
                    если ошибок нет*/
                    if (empty($error)) {    /*шифруем пароль*/
                        $password_hash = encodePassword($password);
                        $fields = [
                                'TYPE' => $type_id,
                                'EMAIL' => $email,
                                'PASSWORD' => $password_hash,
                                'CONFIRMATION' => randString(32, 'hash')
                        ];/*если емэйла в базе ещё нет*/
                        if ($this->User_model->get_by_email($email) == null) {
                            $insert_id = $this->User_model->create($fields);
                            /*добавляем в БД нового пользователя*/
                            $this->session->set_userdata(['USER_ID' => $insert_id]);
                            if ($type_id == 1)
                                $new_subject_id = $this->Med_card_model->create(['USER_ID' => $insert_id, 'BIRTH_DATE' => '2000-01-01']);
                            elseif ($type_id == 2)
                                $new_subject_id = $this->Doctor_model->create([
                                        'USER_ID' => $insert_id,
                                        'EMAIL' => $email,
                                        'DATE_OF_BIRTH' => '2000-01-01',
                                        'GRADUATION_DATE' => '2000-01-01',
                                        'INTERNSHIP_DATE_FROM' => '2000-01-01',
                                        'INTERNSHIP_DATE_TO' => '2000-01-01',
                                        'RESIDENCY_DATE_FROM' => '2000-01-01',
                                        'RESIDENCY_DATE_TO' => '2000-01-01']);
                            elseif ($type_id == 3) {
                                $this->load->model('New_organisations_model');


                                $contacts[] = ['type_id' => 2, 'value' => null];
                                $contacts[] = ['type_id' => 2, 'value' => null];
                                $contacts[] = ['type_id' => 2, 'value' => null];
                                $contacts[] = ['type_id' => 3, 'value' => null];
                                $contacts[] = ['type_id' => 3, 'value' => null];
                                $contacts[] = ['type_id' => 3, 'value' => null];

                                $addresses[] = [
                                    'type_id'   => 1,
                                    'index'     => null,
                                    'region'    => null,
                                    'city'      => null,
                                    'street'    => null,
                                    'house'     => null,
                                    'building'  => null,
                                    'office'    => null
                                ];
                                $addresses[] = [
                                    'type_id'   => 2,
                                    'index'     => null,
                                    'region'    => null,
                                    'city'      => null,
                                    'street'    => null,
                                    'house'     => null,
                                    'building'  => null,
                                    'office'    => null
                                ];

                                $working_times[] = [
                                    'day_id'        => 1,
                                    'work_from'     => null,
                                    'work_to'       => null,
                                    'break_from'    => null,
                                    'break_to'      => null
                                ];
                                $working_times[] = [
                                    'day_id'        => 2,
                                    'work_from'     => null,
                                    'work_to'       => null,
                                    'break_from'    => null,
                                    'break_to'      => null
                                ];
                                $working_times[] = [
                                    'day_id'        => 3,
                                    'work_from'     => null,
                                    'work_to'       => null,
                                    'break_from'    => null,
                                    'break_to'      => null
                                ];
                                $working_times[] = [
                                    'day_id'        => 4,
                                    'work_from'     => null,
                                    'work_to'       => null,
                                    'break_from'    => null,
                                    'break_to'      => null
                                ];
                                $working_times[] = [
                                    'day_id'        => 5,
                                    'work_from'     => null,
                                    'work_to'       => null,
                                    'break_from'    => null,
                                    'break_to'      => null
                                ];
                                $working_times[] = [
                                    'day_id'        => 6,
                                    'work_from'     => null,
                                    'work_to'       => null,
                                    'break_from'    => null,
                                    'break_to'      => null
                                ];
                                $working_times[] = [
                                    'day_id'        => 7,
                                    'work_from'     => null,
                                    'work_to'       => null,
                                    'break_from'    => null,
                                    'break_to'      => null
                                ];

                                $fields = [
                                    'main'  => [
                                        'category_id'   => null,
                                        'type_id'       => null,
                                        'specialization_id' => null,
                                        'brand'         => null,
                                        'entity'        => null,
                                        'taxation_id'   => null,
                                        'description'   => null,
                                        'license'       => null,
                                        'ogrn'          => null,
                                        'inn'           => null,
                                        'okved'         => null,
                                        'okpo'          => null,
                                        'kpp'           => null,
                                        'oktmo'         => null,
                                        'checking_account'  => null,
                                        'corr_account'  => null,
                                        'bank'          => null,
                                        'bik'           => null,
                                        'administrator' => null,
                                        'ceo'           => null,
                                        'chief_accountant'  => null,
                                        'latitude'      => null,
                                        'longitude'     => null,
                                        'in_carousel'   => null,
                                        'public'        => null,
                                        'active'        => null,
                                        'moderated'     => null,
                                        'parent_id'     => null,
                                        'popular'       => null,
                                    ],
                                    'filter'        => [
                                        'f_state'       => null,
                                        'f_private'     => null,
                                        'f_children'    => null,
                                        'f_ambulance'   => null,
                                        'f_house'       => null,
                                        'f_booking'     => null,
                                        'f_delivery'    => null,
                                        'f_daynight'    => null,
                                        'f_dms'         => null,
                                        'f_dlo'         => null,
                                        'f_optics'      => null,
                                        'f_rpo'         => null,
                                        'f_homeopathy'  => null,
                                    ],
                                    'contacts'      => $contacts,
                                    'addresses'     => $addresses,
                                    'working_times' => $working_times,
                                    'metros'        => []
                                ];

                                if (!empty($insert_id)) {
                                    $fields['main']['user_id'] = $insert_id;
                                } else {
                                    show_404();
                                }

//                                ?><!--<pre>--><?// var_dump($fields); exit();

                                $new_organisation_id = $this->New_organisations_model->create($fields);
                            }

                            if ($insert_id)
                                redirect('/profile');
                            else
                                show_404();

                            $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

                            if (!empty($cookie_city_id)) {
                                $this->User_model->update(['DEFAULT_CITY' => $cookie_city_id], $insert_id);
                                $this->session->unset_userdata('DEFAULT_CITY');
                            }
                        } else
                            $error = "Пользователь с такой эл.почтой уже зарегистрирован";
                    }
                }/*выводим ошибки если они есть*/
                $this->data['templates']['errors'] = (!empty($error)) ? $this->load->view("tmp/registration_errors", ['errors' => $error], true) : "";
                $this->load->view("profile/registration_second_step", $this->data);
            } else {
                $this->data['u_type'] = $type;

                $this->load->view("profile/user_agreement", $this->data);
            }
        }
    }
/******************************************************
 *                 Страница авторизации               *
 ******************************************************/
    public function auth()
    {    /*если пользователь авторизован, отправляем его в профиль*/
        if ($this->user_id)
            redirect('/profile');

        $password = null;

        if (!empty($_POST['password']))
            $password = $_POST['password'];

        if (isset($_POST['auth']))
            if (empty($_POST['email']))
                $error = 'Поле "Эл.почта" не заполнено!';
            elseif (!checkEmail($email = $_POST['email']))          /*Проверяем, корректна ли почта*/
                $error = "Электронная почта введена некорректно!";
            elseif (null == $user = $this->User_model->get_by_email($email))
                $error = "Пользователь с такой эл.почтой еще не зарегистрирован в системе!";
            elseif (empty($password))
                $error = 'Поле "Пароль" не заполнено!';
            elseif (checkPassword($password, $user['PASSWORD']))
            {
                $this->session->set_userdata(['USER_ID' => $user['ID']]);

                $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

                if (!empty($cookie_city_id)) {
                    $this->User_model->update(['DEFAULT_CITY' => $cookie_city_id], $user['ID']);
                    $this->session->unset_userdata('DEFAULT_CITY');
                }

                if (!empty($_GET['back_url']))
                    redirect($_GET['back_url']);
                else
                    redirect('/profile');
            }
            else
                $error = "Пароль введен неверно!";


        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $cities['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $cities['current_city'] = $cookie_city_id;
        } else {
            $cities['current_city'] = 4400;
        }

        $this->data['templates']['city_selector'] = $this->load->view("tmp/city_selector", $cities, true);

        $this->data['templates']['errors']            = (!empty($error)) ? $this->load->view("tmp/registration_errors", ['errors' => $error], true) : "";
        $this->data['forms']['registration']['email'] = (!empty($_POST['email'])) ? $_POST['email'] : "";
        $this->load->view('profile/authorization', $this->data);
    }
/**
 * [logout - разлогирование]
 * @return [переход на страницу]
 */
    public function logout()
    {
        $this->session->unset_userdata('USER_ID');
        /*возвращаем пользователя на предыдущую страницу*/
        if (!empty($_GET['back_url']))
            redirect($_GET['back_url']);
        /*если таковой не сохранилось, то на страницу авторизации*/
        else
            redirect('/profile/auth');
    }
/**
 * [index - грузит страницу личных данных пользователя]
 * @return [страница личных данных пользователя]
 */
    public function index()
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {    /*иначе загружаем страницу с его данными*/
            $data=$this->_mainMenuData(false);
            $data['password'] =$data['user']['PASSWORD'];
            /*данные о соцсетях*/
            $data['soc_nets']=$this->Soc_net_model->get();
            /*для Твиттера*/
            $data['soc_nets'][3]['AUTH_LINK']=$this->_twitterAuthLink();
            $data['user_nets']=$this->Soc_net_model->get_by_user_id($this->user_id);
             $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }  
            }
            
           
             //print_r($data['iduser'][0]['ID']);   
           
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/index",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [changePassword - меняет пароль пользователя]
 * @return [новый пароль в БД]
 */
    public function changePassword()
    {
        $password = null;

        if (!empty($_POST['password']))
            $password = $_POST['password'];

        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif (empty($password))
            echo json_encode(['error'=>'Поле "Пароль" не заполнено!']);
        elseif (strlen($password) < 6)
            echo json_encode(['error'=>"Длина пароля должна быть 6 и более символов!"]);
        else
        {    /*шифруем пароль*/
            $password_hash = encodePassword($password);
            $fields = ['PASSWORD' => $password_hash];
            /*меняем пароль в БД*/
            $update_id = $this->User_model->update($fields,$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Пароль успешно сохранен!']);
        }
    }
/**
 * [changeProfile - редактирование личных данных профиля]
 * @return [новые данные в БД]
 */
    public function changeProfile()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif (empty($_POST['email']))
            echo json_encode(['error'=>'Поле "Эл.почта" не заполнено!']);
        elseif (!checkEmail($email = $_POST['email']))
            echo json_encode(['error'=>'Электронная почта введена некорректно!']);
        elseif (empty($_POST['firstname']) or empty($_POST['lastname']) or empty($_POST['middlename']))
            echo json_encode(['error'=>'Заполните ФИО!']);
        elseif(!empty($_POST['phone']))
        {
            $phone=$_POST['phone'];
//            $user=$this->User_model->get_by_phone($phone);
//            if($user != [] and $user['ID'] != $this->user_id)
//                echo json_encode(['error'=>'Пользователь с таким номером телефона уже есть!']);
//            else
                $this->_updateProfile($email,$phone);
        }
        else
            $this->_updateProfile($email,NULL);
    }

    public function changeAva()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['PHOTO'] and file_exists(FCPATH.$user['PHOTO']))
                unlink(FCPATH.$user['PHOTO']);
            $update_id = $this->User_model->update(['PHOTO'=>$_POST['imagePath']],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Аватарка изменена!']);
        }
        else
            echo json_encode(['error'=>true,'msg'=>'Выберите аватарку!']);
    }

    public function deleteAva()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['PHOTO'] and file_exists(FCPATH.$user['PHOTO']))
                unlink(FCPATH.$user['PHOTO']);
            $update_id = $this->User_model->update(['PHOTO'=>NULL],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Аватарка удалена!']);
        }
    }
/**
 * [institution - грузит страницу данных учреждения]
 * @return [страница данных учреждения]
 */
    public function institution()
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($this->Institution_model->get_by_user_id($this->user_id) != NULL)
        {    /*иначе загружаем страницу с его данными учреждения*/
            $data=$this->_mainMenuData(false);
            $data['institution']=$this->Institution_model->get_by_user_id($this->user_id);
            $data['files']=$this->Institution_files_model->get_by_institution_id($data['institution']['ID']);
            $data['types']=$this->Institution_model->get_types();
            $data['metros']=$this->Metro_model->get_id_name();

            $current_metros = [];

            $temp_metros = $this->Institution_metros_model->get_institution_metro($data['institution']['ID']);

            foreach ($temp_metros as $metro) {
                $data['current_metros'][(int)$metro['METRO_ID']] = 1;
            }

            /*не нужно, там не все категории подходят*/
            /*$data['categories']=$this->Institution_model->get_categories();*/
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [changeInstitution - редактирование данных учреждения]
 * @return [новые данные учреждения в БД]
 */
    public function changeInstitution()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(empty($_POST['email']) or !checkEmail($email = $_POST['email']))
            echo json_encode(['error'=>'Введите корректный email организации!']);
        elseif(!empty($_POST['name']) and !empty($_POST['shortname']))
        {
            $user=$this->User_model->get_by_id($this->user_id);

            $institution = $this->db->query("SELECT ID FROM INSTITUTIONS WHERE USER_ID = ".$this->user_id." LIMIT 1")->row_array();
            $institution_id = $institution['ID'];

            if($_POST['category-id'] != 'Выберите...')
            {
                $category_id=$_POST['category-id'];
                $category=$this->Institution_model->get_category_name_by_id($category_id);
                $category_name=$category['NAME'];
            }
            else
            {
                $category_id=NULL;
                $category_name=NULL;
            }
            if($_POST['type-id'] != 'Без типа')
            {
                $type_id=$_POST['type-id'];
                $type=$this->Institution_model->get_type_name_by_id($type_id);
                $type_name=$type['NAME'];
            }
            else
            {
                $type_id=NULL;
                $type_name=NULL;
            }

            if (!empty($_POST['metro_id']) && is_array($_POST['metro_id'])) {

                $current_metros = [];

                $temp_metros = $this->Institution_metros_model->get_institution_metro($institution_id);

                foreach ($temp_metros as $metro) {
                    $current_metros[(int)$metro['METRO_ID']] = 1;
                }

                foreach ($_POST['metro_id'] as $metro_id) {
                    if (!empty($current_metros[$metro_id]))
                        unset($current_metros[$metro_id]);
                    else
                        $this->Institution_metros_model->add_metro($institution_id, $metro_id);
                }

                if (!empty($current_metros)) {
                    foreach ($current_metros as $current_metro => $dummy) {
                        $metros_to_delete[] = $current_metro;
                    }

                    $this->Institution_metros_model->delete_metros($institution_id, $metros_to_delete);
                }
            } else {
                $this->Institution_metros_model->drop_institution_metro($institution_id);
            }

            $fields = [
                'NAME'                    =>$_POST['name'],
                'SHORTNAME'               =>$_POST['shortname'],
                'ACTUAL_ADDRESS_INDEX'    =>!empty($_POST['actual_address_index']) ? $_POST['actual_address_index'] : NULL,
                'LEGAL_ADDRESS_INDEX'     =>!empty($_POST['legal_address_index']) ? $_POST['legal_address_index'] : NULL,
                'ACTUAL_ADDRESS_REGION'   =>$_POST['actual_address_region'],
                'LEGAL_ADDRESS_REGION'    =>$_POST['legal_address_region'],
                'ACTUAL_ADDRESS_CITY'     =>$_POST['actual_address_city'],
                'LEGAL_ADDRESS_CITY'      =>$_POST['legal_address_city'],
                'ACTUAL_ADDRESS_STREET'   =>$_POST['actual_address_street'],
                'LEGAL_ADDRESS_STREET'    =>$_POST['legal_address_street'],
                'ACTUAL_ADDRESS_HOUSE'    =>$_POST['actual_address_house'],
                'LEGAL_ADDRESS_HOUSE'     =>$_POST['legal_address_house'],
                'ACTUAL_ADDRESS_BUILDING' =>$_POST['actual_address_building'],
                'LEGAL_ADDRESS_BUILDING'  =>$_POST['legal_address_building'],
                'ACTUAL_ADDRESS_OFFICE'   =>$_POST['actual_address_office'],
                'LEGAL_ADDRESS_OFFICE'    =>$_POST['legal_address_office'],
                'LICENSE'                 =>$_POST['license'],
                'DESCRIPTION'             =>$_POST['description'],
                'PHONE'                   =>$_POST['phone'],
                'EMAIL'                   =>$email,
                'OGRN'                    =>$_POST['ogrn'],
                'OKPO'                    =>$_POST['okpo'],
                'INN'                     =>$_POST['inn'],
                'KPP'                     =>$_POST['kpp'],
                'OKVED'                   =>$_POST['okved'],
                'OKTMO'                   =>$_POST['oktmo'],
                'ACCOUNT'                 =>$_POST['account'],
                'BANK'                    =>$_POST['bank'],
                'BIK'                     =>$_POST['bik'],
                'CHIEF_PHYSICIAN'         =>$_POST['chief_physician'],
                'CHIEF_ACCOUNTANT'        =>$_POST['chief_accountant'],
                'CEO'                     =>$_POST['ceo'],
                'LATITUDE'                =>$_POST['latitude'],
                'LONGITUDE'               =>$_POST['longitude'],
                'CATEGORY_ID'             =>$category_id,
                'CATEGORY'                =>$category_name,
                'TYPE_ID'                 =>$type_id,
                'TYPE'                    =>$type_name,
                'MONDAY_WORK_FROM'        =>!empty($_POST['monday-work-from']) ? $_POST['monday-work-from'] : NULL,
                'MONDAY_WORK_TO'          =>!empty($_POST['monday-work-to']) ? $_POST['monday-work-to'] : NULL,
                'TUESDAY_WORK_FROM'       =>!empty($_POST['tuesday-work-from']) ? $_POST['tuesday-work-from'] : NULL,
                'TUESDAY_WORK_TO'         =>!empty($_POST['tuesday-work-to']) ? $_POST['tuesday-work-to'] : NULL,
                'WEDNESDAY_WORK_FROM'     =>!empty($_POST['wednesday-work-from']) ? $_POST['wednesday-work-from'] : NULL,
                'WEDNESDAY_WORK_TO'       =>!empty($_POST['wednesday-work-to']) ? $_POST['wednesday-work-to'] : NULL,
                'THURSDAY_WORK_FROM'      =>!empty($_POST['thursday-work-from']) ? $_POST['thursday-work-from'] : NULL,
                'THURSDAY_WORK_TO'        =>!empty($_POST['thursday-work-to']) ? $_POST['thursday-work-to'] : NULL,
                'FRIDAY_WORK_FROM'        =>!empty($_POST['friday-work-from']) ? $_POST['friday-work-from'] : NULL,
                'FRIDAY_WORK_TO'          =>!empty($_POST['friday-work-to']) ? $_POST['friday-work-to'] : NULL,
                'SATURDAY_WORK_FROM'      =>!empty($_POST['saturday-work-from']) ? $_POST['saturday-work-from'] : NULL,
                'SATURDAY_WORK_TO'        =>!empty($_POST['saturday-work-to']) ? $_POST['saturday-work-to'] : NULL,
                'SUNDAY_WORK_FROM'        =>!empty($_POST['sunday-work-from']) ? $_POST['sunday-work-from'] : NULL,
                'SUNDAY_WORK_TO'          =>!empty($_POST['sunday-work-to']) ? $_POST['sunday-work-to'] : NULL,
                'MONDAY_BREAK_FROM'       =>!empty($_POST['monday-break-from']) ? $_POST['monday-break-from'] : NULL,
                'MONDAY_BREAK_TO'         =>!empty($_POST['monday-break-to']) ? $_POST['monday-break-to'] : NULL,
                'TUESDAY_BREAK_FROM'      =>!empty($_POST['tuesday-break-from']) ? $_POST['tuesday-break-from'] : NULL,
                'TUESDAY_BREAK_TO'        =>!empty($_POST['tuesday-break-to']) ? $_POST['tuesday-break-to'] : NULL,
                'WEDNESDAY_BREAK_FROM'    =>!empty($_POST['wednesday-break-from']) ? $_POST['wednesday-break-from'] : NULL,
                'WEDNESDAY_BREAK_TO'      =>!empty($_POST['wednesday-break-to']) ? $_POST['wednesday-break-to'] : NULL,
                'THURSDAY_BREAK_FROM'     =>!empty($_POST['thursday-break-from']) ? $_POST['thursday-break-from'] : NULL,
                'THURSDAY_BREAK_TO'       =>!empty($_POST['thursday-break-to']) ? $_POST['thursday-break-to'] : NULL,
                'FRIDAY_BREAK_FROM'       =>!empty($_POST['friday-break-from']) ? $_POST['friday-break-from'] : NULL,
                'FRIDAY_BREAK_TO'         =>!empty($_POST['friday-break-to']) ? $_POST['friday-break-to'] : NULL,
                'SATURDAY_BREAK_FROM'     =>!empty($_POST['saturday-break-from']) ? $_POST['saturday-break-from'] : NULL,
                'SATURDAY_BREAK_TO'       =>!empty($_POST['saturday-break-to']) ? $_POST['saturday-break-to'] : NULL,
                'SUNDAY_BREAK_FROM'       =>!empty($_POST['sunday-break-from']) ? $_POST['sunday-break-from'] : NULL,
                'SUNDAY_BREAK_TO'         =>!empty($_POST['sunday-break-to']) ? $_POST['sunday-break-to'] : NULL,
                'PHONE1'                  =>$_POST['phone1'],
                'PHONE2'                  =>$_POST['phone2'],
                'PHONE3'                  =>$_POST['phone3'],
                'WEBSITE1'                =>$_POST['website1'],
                'WEBSITE2'                =>$_POST['website2'],
                'WEBSITE3'                =>$_POST['website3']
            ];

            if (!empty($_POST['state']) && $_POST['state'] == "true")
                $fields['STATE'] = 1;
            else
                $fields['STATE'] = 0;
            //------------------------
            if (!empty($_POST['private']) && $_POST['private'] == "true")
                $fields['PRIVATE'] = 1;
            else
                $fields['PRIVATE'] = 0;
            //------------------------
            if (!empty($_POST['children']) && $_POST['children'] == "true")
                $fields['CHILDREN'] = 1;
            else
                $fields['CHILDREN'] = 0;
            //------------------------
            if (!empty($_POST['ambulance']) && $_POST['ambulance'] == "true")
                $fields['AMBULANCE'] = 1;
            else
                $fields['AMBULANCE'] = 0;
            //------------------------
            if (!empty($_POST['house']) && $_POST['house'] == "true")
                $fields['HOUSE'] = 1;
            else
                $fields['HOUSE'] = 0;
            //------------------------
            if (!empty($_POST['booking']) && $_POST['booking'] == "true")
                $fields['BOOKING'] = 1;
            else
                $fields['BOOKING'] = 0;
            //------------------------
            if (!empty($_POST['delivery']) && $_POST['delivery'] == "true")
                $fields['DELIVERY'] = 1;
            else
                $fields['DELIVERY'] = 0;
            //------------------------
            if (!empty($_POST['daynight']) && $_POST['daynight'] == "true")
                $fields['DAYNIGHT'] = 1;
            else
                $fields['DAYNIGHT'] = 0;
            //------------------------
            if (!empty($_POST['dms']) && $_POST['dms'] == "true")
                $fields['DMS'] = 1;
            else
                $fields['DMS'] = 0;
            //------------------------
            if (!empty($_POST['dlo']) && $_POST['dlo'] == "true")
                $fields['DLO'] = 1;
            else
                $fields['DLO'] = 0;
            //------------------------
            if (!empty($_POST['optics']) && $_POST['optics'] == "true")
                $fields['OPTICS'] = 1;
            else
                $fields['OPTICS'] = 0;
            //------------------------
            if (!empty($_POST['rpo']) && $_POST['rpo'] == "true")
                $fields['RPO'] = 1;
            else
                $fields['RPO'] = 0;
            //------------------------
            if (!empty($_POST['homeopathy']) && $_POST['homeopathy'] == "true")
                $fields['HOMEOPATHY'] = 1;
            else
                $fields['HOMEOPATHY'] = 0;
            //------------------------

            $update_id = $this->Institution_model->update($fields, $this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Ваши данные успешно изменены!']);
        }
        else
            echo json_encode(['error'=>'Заполните данные организации!']);
    }

    public function changeLogo()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
             $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
             //print_r($data['iduser'][0]['LOGO']);
           // $institution=$this->Institution_model->get_by_user_id($this->user_id);
            if($data['iduser'][0]['LOGO'] and file_exists(FCPATH.$data['iduser'][0]['LOGO']))
                unlink(FCPATH.$data['iduser'][0]['LOGO']);
            $update_id = $this->Institution_model->update(['LOGO'=>$_POST['imagePath']],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Логотип изменен!']);
        }
        else
            echo json_encode(['error'=>true,'msg'=>'Выберите логотип!']);
    }

    public function deleteLogo()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $institution=$this->Institution_model->get_by_user_id($this->user_id);
            if($institution['LOGO'] and file_exists(FCPATH.$institution['LOGO']))
                unlink(FCPATH.$institution['LOGO']);
            $update_id = $this->Institution_model->update(['LOGO'=>NULL],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Логотип удалён!']);
        }
    }

    public function addInstitutionDocument()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            $institution=$this->Institution_model->get_by_user_id($this->user_id);
            $imageFields = [
                'INSTITUTION_ID'=>$institution['ID'],
                'FILE'            =>$_POST['imagePath']
            ];
            if($insert_id = $this->Institution_files_model->create($imageFields))
                echo json_encode(['success'=>true,'msg'=>'Документ добавлен!']);
            else
                echo json_encode(['error'=>'Не удалось добавить документ!']);
        }
        else
            echo json_encode(['error'=>'Выберите документ!']);
    }

    public function deleteInstitutionDocument()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            if(file_exists(FCPATH.$_POST['imagePath']))
                unlink(FCPATH.$_POST['imagePath']);
            $delete_id = $this->Institution_files_model->delete_by_file($_POST['imagePath']);
            echo json_encode(['success'=>true,'msg'=>'Документ удален!']);
        }
        else
            echo json_encode(['error'=>'Выберите документ!']);
    }

    public function organisation($id = null)
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
         
        $id = (int)$id;

        if (empty($id)) {
            if($this->New_organisations_model->get_item(['o.USER_ID' => $this->user_id], true) == NULL)
                show_404();
            $organisation=$this->New_organisations_model->get_item(['o.USER_ID' => $this->user_id], true);
        } else {
            if($this->New_organisations_model->get_item(['o.USER_ID' => $this->user_id, 'o.ID' => $id], true) == NULL)
                show_404();
            $organisation=$this->New_organisations_model->get_item(['o.USER_ID' => $this->user_id, 'o.ID' => $id], true);
        }

            $data=$this->_mainMenuData(false);
            $data['organisation']=$organisation;
            //print_r($data['organisation']);
            $data['branch_id']=$id;
            $data['files']=[]; // $this->Institution_files_model->get_by_institution_id($data['institution']['ID']);
            $data['types']= $this->Institution_model->get_types();
            $data['metros']=$this->Metro_model->get_id_name();
            //$data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
             $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/organisation",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
    }

    public function branch_add()
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data=$this->_mainMenuData(false);
        $data['organisation']=[];
        $data['files']=[]; // $this->Institution_files_model->get_by_institution_id($data['institution']['ID']);
        $data['types']=$this->Institution_model->get_types();
        $data['metros']=$this->Metro_model->get_id_name();

        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view("profile/branch_add",$data);
        /*грузим футер*/
        $this->load->view("tmp/profile-templates/footer");
    }

    public function organisation_branches()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        elseif(($organisation = $this->New_organisations_model->get_item(['o.USER_ID' => $this->user_id], true)) != NULL)
        {
            $data=$this->_mainMenuData(false);
            $data['metros']=$this->Metro_model->get_id_name();
            $data['branches']=$this->New_organisations_model->get_branches($organisation['ID'], true);
            $data['days']=['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
            $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/branches",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function organisation_save()
    {     
        $organisation = $this->New_organisations_model->get_item(['o.USER_ID' => $this->user_id], true);

        if (empty($organisation))
            show_404();

        $branch_id = $this->input->post('branch_id');

        if (!empty($branch_id))
            if ($this->New_organisations_model->get_item(['o.ID' => $branch_id, 'o.PARENT_ID' => $organisation['ID']], true) == NULL)
                show_404();

        $new = (int)$this->input->post('new');

        $button = $this->input->post('submit');
        if (isset($button)) {
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone1')];
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone2')];
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone3')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website1')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website2')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website3')];

            $input_address = $this->input->post('address');

            $addresses[] = [
                'type_id'   => 1,
                'index'     => $input_address['legal']['index'],
                'region'    => $input_address['legal']['region'],
                'city'      => $input_address['legal']['city'],
                'street'    => $input_address['legal']['street'],
                'house'     => $input_address['legal']['house'],
                'building'  => $input_address['legal']['building'],
                'office'    => $input_address['legal']['office']
            ];
            $addresses[] = [
                'type_id'   => 2,
                'index'     => $input_address['actual']['index'],
                'region'    => $input_address['actual']['region'],
                'city'      => $input_address['actual']['city'],
                'street'    => $input_address['actual']['street'],
                'house'     => $input_address['actual']['house'],
                'building'  => $input_address['actual']['building'],
                'office'    => $input_address['actual']['office']
            ];

            $input_working_times = $this->input->post('work');
            $input_breaking_times = $this->input->post('break');

            $working_times[] = [
                'day_id'        => 1,
                'work_from'     => $input_working_times[1]['from'],
                'work_to'       => $input_working_times[1]['to'],
                'break_from'    => $input_breaking_times[1]['from'],
                'break_to'      => $input_breaking_times[1]['to']
            ];
            $working_times[] = [
                'day_id'        => 2,
                'work_from'     => $input_working_times[2]['from'],
                'work_to'       => $input_working_times[2]['to'],
                'break_from'    => $input_breaking_times[2]['from'],
                'break_to'      => $input_breaking_times[2]['to']
            ];
            $working_times[] = [
                'day_id'        => 3,
                'work_from'     => $input_working_times[3]['from'],
                'work_to'       => $input_working_times[3]['to'],
                'break_from'    => $input_breaking_times[3]['from'],
                'break_to'      => $input_breaking_times[3]['to']
            ];
            $working_times[] = [
                'day_id'        => 4,
                'work_from'     => $input_working_times[4]['from'],
                'work_to'       => $input_working_times[4]['to'],
                'break_from'    => $input_breaking_times[4]['from'],
                'break_to'      => $input_breaking_times[4]['to']
            ];
            $working_times[] = [
                'day_id'        => 5,
                'work_from'     => $input_working_times[5]['from'],
                'work_to'       => $input_working_times[5]['to'],
                'break_from'    => $input_breaking_times[5]['from'],
                'break_to'      => $input_breaking_times[5]['to']
            ];
            $working_times[] = [
                'day_id'        => 6,
                'work_from'     => $input_working_times[6]['from'],
                'work_to'       => $input_working_times[6]['to'],
                'break_from'    => $input_breaking_times[6]['from'],
                'break_to'      => $input_breaking_times[6]['to']
            ];
            $working_times[] = [
                'day_id'        => 7,
                'work_from'     => $input_working_times[7]['from'],
                'work_to'       => $input_working_times[7]['to'],
                'break_from'    => $input_breaking_times[7]['from'],
                'break_to'      => $input_breaking_times[7]['to']
            ];

            $fields = [
                'id'            => $organisation['id'],
                'main'  => [
                    'category_id'   => $this->input->post('category_id'),
                    'type_id'       => $this->input->post('type_id'),
                    'brand'         => $this->input->post('brand'),
                    'entity'        => $this->input->post('entity'),
                    'taxation_id'   => 0, //$this->input->post('taxation_id'),
                    'description'   => $this->input->post('description'),
                    //'logo'          => $this->input->post('logo'),
                    'license'       => $this->input->post('license'),
                    'ogrn'          => $this->input->post('ogrn'),
                    'inn'           => $this->input->post('inn'),
                    'okved'         => $this->input->post('okved'),
                    'okpo'          => $this->input->post('okpo'),
                    'kpp'           => $this->input->post('kpp'),
                    'oktmo'         => $this->input->post('oktmo'),
                    'checking_account'  => $this->input->post('checking_account'),
                    'corr_account'  => $this->input->post('corr_account'),
                    'bank'          => $this->input->post('bank'),
                    'bik'           => $this->input->post('bik'),
                    'administrator' => $this->input->post('administrator'),
                    'ceo'           => $this->input->post('ceo'),
                    'chief_accountant'  => $this->input->post('chief_accountant'),
                    'latitude'      => $this->input->post('latitude'),
                    'longitude'     => $this->input->post('longitude'),
                    'in_carousel'   => 0,
                    'public'        => ($this->input->post('public') == 'on'),
                    'active'        => ($this->input->post('active') == 'on'),
                    'moderated'     => 0,
                ],
                'filter'        => [
                    'f_state'       => ($this->input->post('f_state') == 'on') ? 1 : 0,
                    'f_private'     => ($this->input->post('f_private') == 'on') ? 1 : 0,
                    'f_children'    => ($this->input->post('f_children') == 'on') ? 1 : 0,
                    'f_ambulance'   => ($this->input->post('f_ambulance') == 'on') ? 1 : 0,
                    'f_house'       => ($this->input->post('f_house') == 'on') ? 1 : 0,
                    'f_booking'     => ($this->input->post('f_booking') == 'on') ? 1 : 0,
                    'f_delivery'    => ($this->input->post('f_delivery') == 'on') ? 1 : 0,
                    'f_daynight'    => ($this->input->post('f_daynight') == 'on') ? 1 : 0,
                    'f_dms'         => ($this->input->post('f_dms') == 'on') ? 1 : 0,
                    'f_dlo'         => ($this->input->post('f_dlo') == 'on') ? 1 : 0,
                    'f_optics'      => ($this->input->post('f_optics') == 'on') ? 1 : 0,
                    'f_rpo'         => ($this->input->post('f_rpo') == 'on') ? 1 : 0,
                    'f_homeopathy'  => ($this->input->post('f_homeopathy') == 'on') ? 1 : 0,
                ],
                'contacts'  => $contacts,
                'addresses' => $addresses,
                'working_times' => $working_times
            ];

            if (empty($new)) {
                $fields['id'] = (!empty($branch_id)) ? $branch_id : $organisation['ID'];
                 $result1=$this->New_organisations_model->update($fields);
            } else {
                $fields['main']['parent_id'] = $organisation['ID'];
                $fields['main']['user_id'] = $this->user_id;
               $result2=$this->New_organisations_model->create2($fields);
            }
            if($result1==true || $result2==true){
               session_start();
               $_SESSION['save']=true;  
            }
           
            redirect('/profile/organisation');
        } else {
            show_404();
        }
    }

    public function institutionPhotos()
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($this->Institution_model->get_by_user_id($this->user_id) != NULL)
        {    /*иначе загружаем страницу с его фотками учреждения*/
            $data=$this->_mainMenuData(false);
            $institution=$this->Institution_model->get_by_user_id($this->user_id);
            $data['files']=$this->Institution_photos_model->get_by_institution_id($institution['ID']);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution_photos",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function addInstitutionPhoto()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            $institution=$this->Institution_model->get_by_user_id($this->user_id);
            $imageFields = [
                'INSTITUTION_ID'=>$institution['ID'],
                'FILE'            =>$_POST['imagePath']
            ];
            if($insert_id = $this->Institution_photos_model->create($imageFields))
                echo json_encode(['success'=>true,'msg'=>'Фото добавлено!']);
            else
                echo json_encode(['error'=>'Не удалось добавить фото!']);
        }
        else
            echo json_encode(['error'=>'Выберите фото!']);
    }

    public function deleteInstitutionPhoto()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            if(file_exists(FCPATH.$_POST['imagePath']))
                unlink(FCPATH.$_POST['imagePath']);
            $delete_id = $this->Institution_photos_model->delete_by_file($_POST['imagePath']);
            echo json_encode(['success'=>true,'msg'=>'Фото удалено!']);
        }
        else
            echo json_encode(['error'=>'Выберите фото!']);
    }

/**
 * [specialist - страница данных специалиста]
 * @return [грузим страницу данных специалиста]
 */
    public function specialist()
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($this->Doctor_model->get_by_user_id($this->user_id) != NULL)
        {    /*иначе загружаем страницу с его данными учреждения*/
            $data=$this->_mainMenuData(false);
            $specialist=$this->Doctor_model->get_by_user_id($this->user_id);
//            $data['branches']=$this->Branches_model->get_id_name();
            $this->load->model('New_organisations_model');
            $data['branches']=$this->New_organisations_model->get_list([], [], null);
            /*переводим строку из БД в timestamp и в строку нужную*/
            $dateFields=['DATE_OF_BIRTH','GRADUATION_DATE','INTERNSHIP_DATE_FROM','INTERNSHIP_DATE_TO','RESIDENCY_DATE_FROM','RESIDENCY_DATE_TO'];
            foreach ($dateFields as $dateField)
            {
                $dateToArray=explode('-',$specialist[$dateField]);
                $specialist[$dateField]=$dateToArray[2].'/'.$dateToArray[1].'/'.$dateToArray[0];
            }
            $data['files']=$this->Doctor_files_model->get_by_specialist_id($specialist['ID']);
            $data['refreshers']=$this->Doctor_refreshers_model->get_by_specialist_id($specialist['ID']);
            $data['specialisations'] = $this->db->query('SELECT * FROM DOCTOR_TYPES')->result_array();
            $refreshers=[];
            foreach ($data['refreshers'] as $refresher)
            {
                $dateRefresh=strtotime($refresher['GRADUATION_DATE']);
                $refresher['GRADUATION_DATE']=mdate("%d/%m/%Y",$dateRefresh);
                $refreshers[]=$refresher;
            }
            $data['refreshers']=$refreshers;
            $timeFrom=['MONDAY_WORK_FROM','TUESDAY_WORK_FROM','WEDNESDAY_WORK_FROM','THURSDAY_WORK_FROM','FRIDAY_WORK_FROM','SATURDAY_WORK_FROM','SUNDAY_WORK_FROM'];
            $timeTo=['MONDAY_WORK_TO','TUESDAY_WORK_TO','WEDNESDAY_WORK_TO','THURSDAY_WORK_TO','FRIDAY_WORK_TO','SATURDAY_WORK_TO','SUNDAY_WORK_TO'];
            foreach ($timeFrom as $key => $tf)
            {
                $specialist['timeFrom'][$tf]=substr($specialist[$tf],0,5);
                $specialist['timeTo'][$timeTo[$key]]=substr($specialist[$timeTo[$key]],0,5);
            }
            $data['specialist']=$specialist;
            $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/specialist",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
        }
        else
           {
               $data['specialist']=$specialist;
               $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
               $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/specialist",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
               
           }
    }
/**
 * [changeSpecialist - меняем данные специалиста]
 * @return [новые данные специалиста в БД]
 */
    public function changeSpecialist()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['second-name']) and !empty($_POST['first-name']) and !empty($_POST['last-name']) and !empty($_POST['position']))
        {
            $user=$this->User_model->get_by_id($this->user_id);
            $specialist=$this->Doctor_model->get_by_user_id($this->user_id);
            $dates=[];
            /*strtotime не принимает наш формат*/
            $dateNames=['graduation-date','internship-date-from','internship-date-to','residency-date-from','residency-date-to'];
            foreach ($dateNames as $dateName)
                if(!empty($_POST[$dateName]))
                    $dates[$dateName]=explode('/',$_POST[$dateName]);
                else
                    $dates[$dateName]=['01','01','2000'];
                if($_POST['branch-id'] != 'Выберите...')
                {
                    $branch_id=$_POST['branch-id'];
                    $branch=$this->Branches_model->get_by_id($branch_id);
                    $branch_name=$branch['NAME'];
                }
                else
                {
                    $branch_id=NULL;
                    $branch_name=NULL;
                }

//            $date_of_birth = explode('.', $_POST['date_of_birth']);

//            if (empty($date_of_birth[0])
//                || empty($date_of_birth[1])
//                || empty($date_of_birth[2])
//                || strlen((int)$date_of_birth[0]) != 2
//                || strlen((int)$date_of_birth[1]) != 2
//                || strlen((int)$date_of_birth[2]) != 4
//                ) {
//                $date_of_birth[0] = "";
//                $date_of_birth[1] = "";
//                $date_of_birth[2] = "";
//            }

            $fields = [
                'LAST_NAME'                           =>$_POST['last-name'],
                'FIRST_NAME'                          =>$_POST['first-name'],
                'SECOND_NAME'                         =>$_POST['second-name'],
//                'DATE_OF_BIRTH'                          =>$date_of_birth[2]."-".$date_of_birth[1]."-".$date_of_birth[0],
                'POSITION'                            =>$_POST['position'],
                'ADDRESS_INDEX'                          =>!empty($_POST['address_index']) ? $_POST['address_index'] : NULL,
                'ADDRESS_REGION'                      =>$_POST['address_region'],
                'ADDRESS_CITY'                          =>$_POST['address_city'],
                'ADDRESS_STREET'                      =>$_POST['address_street'],
                'ADDRESS_HOUSE'                          =>$_POST['address_house'],
                'ADDRESS_BUILDING'                      =>$_POST['address_building'],
                'ADDRESS_OFFICE'                      =>$_POST['address_office'],
//                'WEBSITES'                              =>$_POST['websites'],
                'SPECIALIZATION_ID'                      =>$_POST['specialization_id'],
                'SPECIALIZATION'                      =>$_POST['specialization'],
                'SPECIALIZATION_EXPERIENCE'           =>$_POST['specialization-experience'],
                'WORK_PHONE'                          =>$_POST['work-phone'],
                'TOTAL_EXPERIENCE'                    =>$_POST['total-experience'],
                'MOBILE_PHONE'                        =>$_POST['mobile-phone'],
                'UNIVERSITY'                          =>$_POST['university'],
                'DIPLOMA_SERIES_AND_NUMBER'           =>$_POST['diploma-series-and-number'],
                'FACULTY'                             =>$_POST['faculty'],
                'GRADUATION_DATE'                     =>$dates['graduation-date'][2].'-'.$dates['graduation-date'][1].'-'.$dates['graduation-date'][0],
                'INTERNSHIP_INSTITUTION'              =>$_POST['internship-institution'],
                'INTERNSHIP_SPECIALITY'               =>$_POST['internship-speciality'],
                'INTERNSHIP_DATE_FROM'                =>$dates['internship-date-from'][2].'-'.$dates['internship-date-from'][1].'-'.$dates['internship-date-from'][0],
                'INTERNSHIP_DATE_TO'                  =>$dates['internship-date-to'][2].'-'.$dates['internship-date-to'][1].'-'.$dates['internship-date-to'][0],
                'RESIDENCY_INSTITUTION'               =>$_POST['residency-institution'],
                'RESIDENCY_SPECIALITY'                =>$_POST['residency-speciality'],
                'RESIDENCY_DATE_FROM'                 =>$dates['residency-date-from'][2].'-'.$dates['residency-date-from'][1].'-'.$dates['residency-date-from'][0],
                'RESIDENCY_DATE_TO'                   =>$dates['residency-date-to'][2].'-'.$dates['residency-date-to'][1].'-'.$dates['residency-date-to'][0],
                'PROFESSIONAL_RETRAINING_INSTITUTION' =>$_POST['professional-retraining-institution'],
                'PROFESSIONAL_RETRAINING_SPECIALITY'  =>$_POST['professional-retraining-speciality'],
                'PROFESSIONAL_RETRAINING_YEARS'       =>$_POST['professional-retraining-years'],
                'DESCRIPTION'                         =>$_POST['description'],
                'MONDAY_WORK_FROM'                    =>!empty($_POST['monday-work-from']) ? $_POST['monday-work-from'] : "",
                'MONDAY_WORK_TO'                      =>!empty($_POST['monday-work-to']) ? $_POST['monday-work-to'] : "",
                'TUESDAY_WORK_FROM'                   =>!empty($_POST['tuesday-work-from']) ? $_POST['tuesday-work-from'] : "",
                'TUESDAY_WORK_TO'                     =>!empty($_POST['tuesday-work-to']) ? $_POST['tuesday-work-to'] : "",
                'WEDNESDAY_WORK_FROM'                 =>!empty($_POST['wednesday-work-from']) ? $_POST['wednesday-work-from'] : "",
                'WEDNESDAY_WORK_TO'                   =>!empty($_POST['wednesday-work-to']) ? $_POST['wednesday-work-to'] : "",
                'THURSDAY_WORK_FROM'                  =>!empty($_POST['thursday-work-from']) ? $_POST['thursday-work-from'] : "",
                'THURSDAY_WORK_TO'                    =>!empty($_POST['thursday-work-to']) ? $_POST['thursday-work-to'] : "",
                'FRIDAY_WORK_FROM'                    =>!empty($_POST['friday-work-from']) ? $_POST['friday-work-from'] : "",
                'FRIDAY_WORK_TO'                      =>!empty($_POST['friday-work-to']) ? $_POST['friday-work-to'] : "",
                'SATURDAY_WORK_FROM'                  =>!empty($_POST['saturday-work-from']) ? $_POST['saturday-work-from'] : "",
                'SATURDAY_WORK_TO'                    =>!empty($_POST['saturday-work-to']) ? $_POST['saturday-work-to'] : "",
                'SUNDAY_WORK_FROM'                    =>!empty($_POST['sunday-work-from']) ? $_POST['sunday-work-from'] : "",
                'SUNDAY_WORK_TO'                      =>!empty($_POST['sunday-work-to']) ? $_POST['sunday-work-to'] : "",
                'EMAIL'                               =>$user['EMAIL'],
                'BRANCH_ID'                           =>$branch_id,
                'BRANCH'                              =>$branch_name
            ];/*меняем данные cпециалиста*/
            $update_id = $this->Doctor_model->update($fields,$this->user_id);
            foreach($_POST['refresher-id'] as $key => $refreshID)
                if($refreshID=='e' and (!empty($_POST['refresher-training-institution'][$key]) or !empty($_POST['refresher-training-period'][$key]) or !empty($_POST['refresher-training-speciality'][$key]) or !empty($_POST['refresher-training-hours'][$key]) or !empty($_POST['refresher-training-document-number'][$key])))
                {    /*если новая - добавляем её*/
                    if(!empty($_POST['refresher-training-graduation-date'][$key]))
                        $refreshDate=explode('/',$_POST['refresher-training-graduation-date'][$key]);
                    else
                        $refreshDate=['01','01','2000'];
                    $refreshFields=[
                        'SPECIALIST_ID' =>$specialist['ID'],
                        'INSTITUTION'=>$_POST['refresher-training-institution'][$key],
                        'PERIOD'=>$_POST['refresher-training-period'][$key],
                        'SPECIALITY'=>$_POST['refresher-training-speciality'][$key],
                        'HOURS'=>$_POST['refresher-training-hours'][$key],
                        'DOCUMENT_NUMBER'=>$_POST['refresher-training-document-number'][$key],
                        'GRADUATION_DATE'=>$refreshDate[2].'-'.$refreshDate[1].'-'.$refreshDate[0]
                    ];
                    $insert_id = $this->Doctor_refreshers_model->create($refreshFields);
                }/*если не новая - уже есть в БД*/
                elseif((!empty($_POST['refresher-training-institution'][$key])) or (!empty($_POST['refresher-training-period'][$key])) or (!empty($_POST['refresher-training-speciality'][$key])) or (!empty($_POST['refresher-training-hours'][$key])) or (!empty($_POST['refresher-training-document-number'][$key])))
                {    /*если не пустая - обновляем*/
                    if(!empty($_POST['refresher-training-graduation-date'][$key]))
                        $refreshDate=explode('/',$_POST['refresher-training-graduation-date'][$key]);
                    else
                        $refreshDate=['01','01','2000'];
                    $refreshFields=[
                        'INSTITUTION'=>$_POST['refresher-training-institution'][$key],
                        'PERIOD'=>$_POST['refresher-training-period'][$key],
                        'SPECIALITY'=>$_POST['refresher-training-speciality'][$key],
                        'HOURS'=>$_POST['refresher-training-hours'][$key],
                        'DOCUMENT_NUMBER'=>$_POST['refresher-training-document-number'][$key],
                        'GRADUATION_DATE'=>$refreshDate[2].'-'.$refreshDate[1].'-'.$refreshDate[0]
                    ];
                    $update_id = $this->Doctor_refreshers_model->update($refreshFields,$refreshID);
                }/*пустая - удаляем*/
                else
                    $delete_id = $this->Doctor_refreshers_model->delete($refreshID);
            echo json_encode(['success'=>true,'msg'=>'Ваши данные успешно изменены!']);
        }
        else
            echo json_encode(['error'=>'Заполните данные специалиста!']);
    }

    public function changeSpecialistPhoto()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            $specialist=$this->Doctor_model->get_by_user_id($this->user_id);
            if($specialist['PHOTO'] and file_exists(FCPATH.$specialist['PHOTO']))
                unlink(FCPATH.$specialist['PHOTO']);
            $update_id = $this->Doctor_model->update(['PHOTO'=>$_POST['imagePath']],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Фото изменено!']);
        }
        else
            echo json_encode(['error'=>true,'msg'=>'Выберите фото!']);
    }

    public function deleteSpecialistPhoto()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $specialist=$this->Doctor_model->get_by_user_id($this->user_id);
            if($specialist['PHOTO'] and file_exists(FCPATH.$specialist['PHOTO']))
                unlink(FCPATH.$specialist['PHOTO']);
            $update_id = $this->Doctor_model->update(['PHOTO'=>NULL],$this->user_id);
            echo json_encode(['success'=>true,'msg'=>'Фото удалено!']);
        }
    }

    public function addSpecialistDocument()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            $specialist=$this->Doctor_model->get_by_user_id($this->user_id);
            $imageFields = [
                'SPECIALIST_ID'=>$specialist['ID'],
                'FILE'            =>$_POST['imagePath']
            ];
            if($insert_id = $this->Doctor_files_model->create($imageFields))
                echo json_encode(['success'=>true,'msg'=>'Документ добавлен!']);
            else
                echo json_encode(['error'=>'Не удалось добавить документ!']);
        }
        else
            echo json_encode(['error'=>'Выберите документ!']);
    }

    public function deleteSpecialistDocument()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            if(file_exists(FCPATH.$_POST['imagePath']))
                unlink(FCPATH.$_POST['imagePath']);
            $delete_id = $this->Doctor_files_model->delete_by_file($_POST['imagePath']);
            echo json_encode(['success'=>true,'msg'=>'Документ удален!']);
        }
        else
            echo json_encode(['error'=>'Выберите документ!']);
    }

/**
 * [liked - вывод избранного]
 * @return [грузит страницу с избранным]
 */
    public function liked()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {

            //TODO доделать вывод избранных учреждений и статей
            $data=$this->_likedData();
//            $data['liked_specialists'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_SPECIALISTS', ' LIMIT 7');
//            $data['liked_institutions'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_INSTITUTIONS', ' LIMIT 7');
//            $data['liked_preps'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_PREPS', ' LIMIT 7');
//            $data['liked_articles'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_ARTICLES', ' LIMIT 5');
            $data['liked_specialists'] = $this->Liked_model->get_liked_specialists($this->user_id);
            $data['liked_institutions'] = $this->Liked_model->get_liked_institutions($this->user_id);
            $data['liked_preps'] = $this->Liked_model->get_liked_preps($this->user_id);

            $data['liked_preps'] = $this->data['nomen'] = $this->db->query('
            SELECT 
                   LIKED_PREPS.ID,
                   NOMEN.ID NOMENID,      # идентификатор в номенклатуре
                   PPACK.NAME PPACKSHORTNAME,                 # название в начальной упаковке 
                   PPACK.FULLNAME PPACKNAME,                  # название в начальной упаковке 
                   NOMEN.DRUGSINPPACK,    # количество в начальной упаковке
                   NOMEN.PPACKMASS,       # масса упаковки
                   PPACKMASS.FULLNAME PPACKMASSNAME,          # название массы упаковки
                   PPACKMASS.SHORTNAME PPACKMASSSHORTNAME,    # короткое название массы упаковки
                   NOMEN.PPACKVOLUME,     # объем упаковки
                   PPACKVOLUME.FULLNAME PPACKVOLUMENAME,      # название объема упаковки
                   PPACKVOLUME.SHORTNAME PPACKVOLUMESHORTNAME,# короткое название объема упаковки
                   DRUGSET.FULLNAME DRUGSETNAME,              # название комплекта
                   DRUGSET.SHORTNAME DRUGSETSHORTNAME,        # короткое название комплекта
                   NOMEN.PPACKINUPACK,    # количество первичных упаковок во вторичной
                   UPACK.NAME UPACKNAME,  # название вторичной упаковки
                   NOMEN.INANGRO,         # признак АНГРО
                   NOMEN.EANCODE,         # ШКП (штрихкод производителя)
                   NOMEN.DRUGSTORCOND NOMENDRUGSTORCOND,      # условия хранения из общей таблицы
                   DRUGSTORCOND.TEXT DRUGSTORCOND,            # условия хранения
                   NOMEN.DRUGLIFETIME NOMENDRUGLIFETIME,      # срок годности из общей таблицы
                   DRUGLIFETIME.TEXT DRUGLIFETIME,            # срок годности
                   NOMEN.UPACKINSPACK,    # количество вторичных упаковок в третьичной
                   SPACK.NAME SPACKNAME,  # название третьичной упаковки
                   PREP.ID PREPID,        # идентификатор препарата
                   TRADENAMES.NAME,       # название препарата
                   TRADENAMES.INAME,      # название препарата (без спецсимволов)
                   LATINNAMES.NAME LNAME, # название препарата (латинское)
                   CLSDRUGFORMS.FULLNAME DRUGFORMNAME,    # название лекарственной формы
                   CLSDRUGFORMS.NAME DRUGFORMSHORTNAME,   # короткое название лекарственной формы
                   PREP.DFMASS,           # масса лекарственной формы
                   MASSUNITS.FULLNAME DFMASSNAME,         # название единицы измерения
                   MASSUNITS.SHORTNAME DFMASSSHORTNAME,   # короткое название единицы измерения
                   PREP.DFCONC,           # концентрация лекарственной формы
                   CONCENUNITS.FULLNAME DFCONCNAME,       # название единицы измерения
                   CONCENUNITS.SHORTNAME DFCONCSHORTNAME, # короткое название единицы измерения
                   PREP.DFACT,            # количество единиц действия
                   ACTUNITS.FULLNAME DFACTNAME,           # название единицы измерения
                   ACTUNITS.SHORTNAME DFACTSHORTNAME,     # короткое название единицы измерения
                   PREP.DFSIZE,           # размеры лекарственной формы
                   SIZEUNITS.FULLNAME DFSIZENAME,         # название единицы измерения
                   SIZEUNITS.SHORTNAME DFSIZESHORTNAME,   # короткое название единицы измерения
                   PREP.DRUGDOSE,         # количество доз в упаковке 
                   PREP.NORECIPE,         # признак безрецептурного отпуска 
                   FIRMS.ID FIRMID,                       # идентификатор фирмы
                   FIRMS.FULLNAME FIRMFULLNAME,           # полное название фирмы (может отсутствовать)
                   FIRMNAMES.NAME FIRMNAME,               # название фирмы
                   COUNTRIES.NAME COUNTRY,                # название страны фирмы
                   NOMENFIRMS.ID NOMENFIRMID,             # идентификатор фирмы
                   NOMENFIRMS.FULLNAME NOMENFIRMFULLNAME, # полное название фирмы (может отсутствовать)
                   NOMENFIRMNAMES.NAME NOMENFIRMNAME,     # название фирмы
                   NOMENCOUNTRIES.NAME NOMENCOUNTRY,      # название страны фирмы
                   FIRMS.ADRMAIN,         # адрес основного офиса
                   FIRMS.ADRRUSSIA,       # адрес в россии
                   FIRMS.ADRUSSR,         # адрес в странах ближнего зарубежья
                   DESCTEXTES.COMPOSITION,                # состав и форма выпуска
                   DESCTEXTES.DRUGFORMDESCR,              # описание лекарственной формы
                   DESCTEXTES.PHARMAACTIONS,              # фармакологическое действия
                   DESCTEXTES.PHARMAKINETIC,              # фармакинетика
                   DESCTEXTES.PHARMADYNAMIC,              # фармадинамика
                   DESCTEXTES.INDICATIONS,                # показания
                   DESCTEXTES.CONTRAINDICATIONS,          # противопоказания
                   DESCTEXTES.PREGNANCYUSE,               # применение при беременности и кормлении грудью
                   DESCTEXTES.SIDEACTIONS,                # побочные действия
                   DESCTEXTES.INTERACTIONS,               # взаимодействие
                   DESCTEXTES.USEMETHODANDDOSES,          # способ применения и дозы
                   DESCTEXTES.OVERDOSE,                   # передозировка
                   DESCTEXTES.SPECIALGUIDELINES,          # особые указания
                   IDENT_WIND_STR.IWID IMAGE              # изображение
              FROM LIKED_PREPS
         LEFT JOIN NOMEN ON NOMEN.ID = LIKED_PREPS.PREPS_ID
         LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
         LEFT JOIN FIRMS ON FIRMS.ID = PREP.FIRMID
         LEFT JOIN FIRMNAMES ON FIRMNAMES.ID = FIRMS.NAMEID
         LEFT JOIN COUNTRIES ON COUNTRIES.ID = FIRMS.COUNTID
         LEFT JOIN TRADENAMES ON TRADENAMES.ID = PREP.TRADENAMEID
         LEFT JOIN LATINNAMES ON LATINNAMES.ID = PREP.LATINNAMEID
         LEFT JOIN CLSDRUGFORMS ON CLSDRUGFORMS.ID = PREP.DRUGFORMID
         LEFT JOIN MASSUNITS ON MASSUNITS.ID = PREP.DFMASSID
         LEFT JOIN CONCENUNITS ON CONCENUNITS.ID = PREP.DFCONCID
         LEFT JOIN ACTUNITS ON ACTUNITS.ID = PREP.DFACTID
         LEFT JOIN SIZEUNITS ON SIZEUNITS.ID = PREP.DFSIZEID
         LEFT JOIN DRUGFORMCHARS ON DRUGFORMCHARS.ID = PREP.DFCHARID
         LEFT JOIN FIRMS NOMENFIRMS ON NOMENFIRMS.ID = NOMEN.FIRMID
         LEFT JOIN FIRMNAMES NOMENFIRMNAMES ON NOMENFIRMNAMES.ID = NOMENFIRMS.NAMEID
         LEFT JOIN COUNTRIES NOMENCOUNTRIES ON NOMENCOUNTRIES.ID = NOMENFIRMS.COUNTID
         LEFT JOIN DRUGPACK PPACK ON PPACK.ID = NOMEN.PPACKID
         LEFT JOIN DRUGPACK UPACK ON UPACK.ID = NOMEN.UPACKID
         LEFT JOIN DRUGPACK SPACK ON SPACK.ID = NOMEN.SPACKID
         LEFT JOIN MASSUNITS PPACKMASS ON PPACKMASS.ID = NOMEN.PPACKMASSUNID
         LEFT JOIN CUBICUNITS PPACKVOLUME ON PPACKVOLUME.ID = NOMEN.PPACKCUBUNID
         LEFT JOIN DRUGSET ON DRUGSET.ID = NOMEN.SETID
         LEFT JOIN DRUGSTORCOND ON DRUGSTORCOND.ID = NOMEN.CONDID
         LEFT JOIN DRUGLIFETIME ON DRUGLIFETIME.ID = NOMEN.LIFEID
         LEFT JOIN DESCRIPTIONS ON DESCRIPTIONS.FIRMID = FIRMS.ID AND DESCRIPTIONS.FPREPNAME = TRADENAMES.INAME
         LEFT JOIN DESCTEXTES ON DESCTEXTES.DESCID = DESCRIPTIONS.ID 
         LEFT JOIN IDENT_WIND_STR ON IDENT_WIND_STR.NOMENID = NOMEN.ID
             LIMIT 7')->result_array();

            $data['liked_articles'] = $this->Liked_model->get_liked_articles($this->user_id);
            $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_search - поиск в избранном]
 * @return [грузит страницу с избранным по поисковому запросу]
 */
    public function liked_search()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_specialists'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_SPECIALISTS', " AND SPECIALIST_LAST_NAME LIKE '%".$_POST['search']."%' LIMIT 7");
            $data['liked_institutions'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_INSTITUTIONS', " AND INSTITUTION_NAME LIKE '%".$_POST['search']."%' LIMIT 7");
            $data['liked_preps'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_PREPS', " AND PREPS_NAME LIKE '%".$_POST['search']."%' LIMIT 7");
            $data['liked_articles'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_ARTICLES', " AND (ARTICLES_NAME LIKE '%".$_POST['search']."%' OR ARTICLES_TEXT LIKE '%".$_POST['search']."%') LIMIT 5");
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_specialists - вывод избранных специалистов]
 * @return [грузит страницу с избранными специалистами]
 */
    public function liked_specialists()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_specialists'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_SPECIALISTS', ' ');
            $data['count_liked_specialists_specialization'] = $this->Liked_model->get_by_count_liked_specialists_specialization($this->user_id);
            $data['count_liked_specialists_specialization_id'] = $this->Liked_model->get_by_count_liked_specialists_specialization_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_specialists",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_specialists_search - поиск в избранных специалистах]
 * @return [грузит страницу с избранными специалистами по поисковому запросу]
 */
    public function liked_specialists_search()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_specialists'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_SPECIALISTS', " AND SPECIALIST_LAST_NAME LIKE '%".$_POST['search']."%' ");
            $data['count_liked_specialists_specialization'] = $this->Liked_model->get_by_count_liked_specialists_specialization($this->user_id);
            $data['count_liked_specialists_specialization_id'] = $this->Liked_model->get_by_count_liked_specialists_specialization_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_specialists",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_institutions - вывод избранных учреждений]
 * @return [грузит страницу с избранными учреждениями]
 */
    public function liked_institutions()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_institutions'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_INSTITUTIONS', ' ');
            $data['count_liked_institutions_type'] = $this->Liked_model->get_by_count_liked_institutions_type($this->user_id);
            $data['count_liked_institutions_type_id'] = $this->Liked_model->get_by_count_liked_institutions_type_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_institutions",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_institutions_search - поиск в избранных учреждениях]
 * @return [грузит страницу с избранными учреждениями по поисковому запросу]
 */
    public function liked_institutions_search()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_institutions'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_INSTITUTIONS', " AND INSTITUTION_NAME LIKE '%".$_POST['search']."%' ");
            $data['count_liked_institutions_type'] = $this->Liked_model->get_by_count_liked_institutions_type($this->user_id);
            $data['count_liked_institutions_type_id'] = $this->Liked_model->get_by_count_liked_institutions_type_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_institutions",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_preps - вывод избранных лекарств]
 * @return [грузит страницу с избранными лекарствами]
 */
    public function liked_preps()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_preps'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_PREPS', ' ');
            $data['count_liked_preps_type'] = $this->Liked_model->get_by_count_liked_preps_type($this->user_id);
            $data['count_liked_preps_type_id'] = $this->Liked_model->get_by_count_liked_preps_type_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_preps",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_preps_search - поиск в избранных лекарствах]
 * @return [грузит страницу с избранными лекарствами по поисковому запросу]
 */
    public function liked_preps_search()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_preps'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_PREPS', " AND PREPS_NAME LIKE '%".$_POST['search']."%' ");
            $data['count_liked_preps_type'] = $this->Liked_model->get_by_count_liked_preps_type($this->user_id);
            $data['count_liked_preps_type_id'] = $this->Liked_model->get_by_count_liked_preps_type_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_preps",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_articles - вывод избранных статей]
 * @return [грузит страницу с избранными статьями]
 */
    public function liked_articles()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_articles'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_ARTICLES', ' ');
            $data['count_liked_articles_category'] = $this->Liked_model->get_by_count_liked_articles_category($this->user_id);
            $data['count_liked_articles_category_id'] = $this->Liked_model->get_by_count_liked_articles_category_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_articles",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_articles_search - поиск в избранных статьях]
 * @return [грузит страницу с избранными статьями по поисковому запросу]
 */
    public function liked_articles_search()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_likedData();
            $data['liked_articles'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_ARTICLES', " AND (ARTICLES_NAME LIKE '%".$_POST['search']."%' OR ARTICLES_TEXT LIKE '%".$_POST['search']."%') ");
            $data['count_liked_articles_category'] = $this->Liked_model->get_by_count_liked_articles_category($this->user_id);
            $data['count_liked_articles_category_id'] = $this->Liked_model->get_by_count_liked_articles_category_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_articles",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [liked_specialists_filter - фильтр избранных специалистов]
 * @return [грузит страницу с избранными специалистами нужной категории]
 */
    public function liked_specialists_filter($filter=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($filter!==false)
        {
            $data=$this->_likedData();
            $data['liked_specialists'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_SPECIALISTS', ' AND SPECIALIST_SPECIALIZATION_ID='.$filter.' ');
            $data['count_liked_specialists_specialization'] = $this->Liked_model->get_by_count_liked_specialists_specialization($this->user_id);
            $data['count_liked_specialists_specialization_id'] = $this->Liked_model->get_by_count_liked_specialists_specialization_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_specialists",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [liked_institutions_filter - фильтр избранных учреждений]
 * @return [грузит страницу с избранными учреждениями нужной категории]
 */
    public function liked_institutions_filter($filter=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($filter!==false)
        {
            $data=$this->_likedData();
            $data['liked_institutions'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_INSTITUTIONS', ' AND INSTITUTION_TYPE_ID='.$filter.' ');
            $data['count_liked_institutions_type'] = $this->Liked_model->get_by_count_liked_institutions_type($this->user_id);
            $data['count_liked_institutions_type_id'] = $this->Liked_model->get_by_count_liked_institutions_type_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_institutions",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [liked_preps_filter - фильтр избранных препаратов]
 * @return [грузит страницу с избранными препаратами нужной категории]
 */
    public function liked_preps_filter($filter=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($filter!==false)
        {
            $data=$this->_likedData();
            $data['liked_preps'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_PREPS', ' AND PREPS_TYPE_ID='.$filter.' ');
            $data['count_liked_preps_type'] = $this->Liked_model->get_by_count_liked_preps_type($this->user_id);
            $data['count_liked_preps_type_id'] = $this->Liked_model->get_by_count_liked_preps_type_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_preps",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [liked_articles_filter - фильтр избранных статей]
 * @return [грузит страницу с избранными статьями нужной категории]
 */
    public function liked_articles_filter($filter=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($filter!==false)
        {
            $data=$this->_likedData();
            $data['liked_articles'] = $this->Liked_model->get_by_liked($this->user_id, 'LIKED_ARTICLES', ' AND ARTICLES_CATEGORY_ID='.$filter.' ');
            $data['count_liked_articles_category'] = $this->Liked_model->get_by_count_liked_articles_category($this->user_id);
            $data['count_liked_articles_category_id'] = $this->Liked_model->get_by_count_liked_articles_category_id($this->user_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/liked_articles",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [deleteLikedSpecialist - удаление специалистов из избранного]
 * @return [удаление специалиста из таблицы избранных в БД]
 */
    public function deleteLikedSpecialist()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Liked_model->deleteSpecialist($_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Врач удалён!']);
        }
        else
            echo json_encode(['error'=>'Выберите врача!']);
    }
/**
 * [deleteLikedInstitution - удаление учреждений из избранного]
 * @return [удаление учреждения из таблицы избранных в БД]
 */
    public function deleteLikedInstitution()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Liked_model->deleteInstitution($_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Учреждение удалено!']);
        }
        else
            echo json_encode(['error'=>'Выберите учреждение!']);
    }
/**
 * [deleteLikedArticle - удаление статей из избранного]
 * @return [удаление статьи из таблицы избранных в БД]
 */
    public function deleteLikedArticle()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Liked_model->deleteArticle($_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Статья удалена!']);
        }
        else
            echo json_encode(['error'=>'Выберите статью!']);
    }
/**
 * [deleteLikedPrep - удаление препаратов из избранного]
 * @return [удаление препарата из таблицы избранных в БД]
 */
    public function deleteLikedPrep()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Liked_model->deletePrep($_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Препарат удалён!']);
        }
        else
            echo json_encode(['error'=>'Выберите препарат!']);
    }
/**
 * [institution_specialists - вывод специалистов учереждения]
 * @return [грузит страницу с специалистами учереждения]
 */
    public function institution_specialists()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_institutionSpecialistsData(' ');
            $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution_specialists",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [institution_specialists_filter - фильтр специалистов учережения]
 * @return [грузит страницу с специалистами учереждения нужной категории]
 */
    public function institution_specialists_filter($filter=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($filter!==false)
        {
            $data=$this->_institutionSpecialistsData(' AND SPECIALIST_SPECIALIZATION_ID='.$filter.' ');
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution_specialists",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [institution_specialists_filter - фильтр специалистов учережения]
 * @return [грузит страницу с специалистами учереждения нужной категории]
 */
    public function institution_specialists_search()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_institutionSpecialistsData(" AND SPECIALIST_LAST_NAME LIKE '%".$_POST['search']."%' ");
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution_specialists",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
    
    public function institution_specialists_delete()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Institution_specialists_model->delete($_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Врач удален!']);
        }
        else
            echo json_encode(['error'=>'Выберите врача!']);
    }
/**
 * [med_card - страница данных медкарты]
 * @return [грузим страницу данных медкарты]
 */
    public function med_card()
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($this->Med_card_model->get_by_user_id($this->user_id) != NULL)
        {
            /*иначе загружаем страницу с его данными медкарты*/
            $data=$this->_mainMenuData(false);
            $data['med_card']=$this->Med_card_model->get_by_user_id($this->user_id);
            $dateToArray=explode('-',$data['med_card']['BIRTH_DATE']);
            $data['med_card']['BIRTH_DATE']=$dateToArray[2].'/'.$dateToArray[1].'/'.$dateToArray[0];
            $data['files']=$this->Med_card_files_model->get_by_med_card_id($data['med_card']['ID']);
            $data['user_data'] = $this->db->query("SELECT * FROM USERS WHERE ID = ".$this->user_id." LIMIT 1")->row_array();
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/med_card",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [changeMedCard - изменение данных медкарты]
 * @return [новые данные медкарты в БД]
 */
    public function changeMedCard()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['name']) and !empty($_POST['father-name']) and !empty($_POST['family']))
        {
            $med_card = $this->Med_card_model->get_by_user_id($this->user_id);
            if(!empty($_POST['birth-date']))
                $birthDate=explode('/',$_POST['birth-date']);
            else
                $birthDate=['01','01','2000'];
            $fields = [
                'NAME'               =>$_POST['name'],
                'ORGANIZATION'       =>$_POST['organization'],
                'OMS_NUMBER'         =>$_POST['oms-number'],
                'SNILS'              =>$_POST['snils'],
                'EXEMPTION_CODE'     =>$_POST['exemption-code'],
                'FAMILY'             =>$_POST['family'],
                'FATHER_NAME'        =>$_POST['father-name'],
                'BIRTH_DATE'         =>$birthDate[2].'-'.$birthDate[1].'-'.$birthDate[0],
                'LIFE_ADDR'          =>$_POST['life-addr'],
                'REG_ADDR'           =>$_POST['reg-addr'],
                'HOME_PHONE'         =>$_POST['home-phone'],
//                'MOBILE_PHONE'       =>$_POST['mobile-phone'],
                'EXEMPTION_DOCUMENT' =>$_POST['exemption-document'],
                'DISABILITY'         =>$_POST['disability'],
                'WORK_CHAR'          =>$_POST['work-char'],
                'PROFESSION'         =>$_POST['profession'],
                'POSITION'           =>$_POST['position'],
                'DEPENDENT'          =>$_POST['dependent'],
                'BLOOD_TYPE'         =>$_POST['blood_type'],
                'INTOLERANCE'        =>$_POST['intolerance'],
            ];

            if (!empty($_POST['mobile-phone'])) {
                $phone = $this->db->query('SELECT * FROM MED_CARDS WHERE MOBILE_PHONE = "'.$_POST['mobile-phone'].'" LIMIT 1')->row_array();

                if (empty($phone) || ($phone['USER_ID'] == $this->user_id)) {
                    $fields['MOBILE_PHONE'] = $_POST['mobile-phone'];
                } else {
                    echo json_encode(['error'=>'Данный телефон уже используется другим пользователем']);exit();
                }
            }

            /*меняем данные медкарты*/
            $update_id = $this->Med_card_model->update($fields,$med_card['ID']);
            echo json_encode(['success'=>true,'msg'=>'Ваши данные успешно изменены!']);exit();
        }
        else
            echo json_encode(['error'=>'Заполните данные медицинской карты!']);exit();
    }

    public function addMedCardDocument()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            $med_card=$this->Med_card_model->get_by_user_id($this->user_id);
            $imageFields = [
                'MED_CARD_ID' =>$med_card['ID'],
                'FILE'        =>$_POST['imagePath']
            ];
            if($insert_id = $this->Med_card_files_model->create($imageFields))
                echo json_encode(['success'=>true,'msg'=>'Документ добавлен!']);
        }
        else
            echo json_encode(['error'=>true,'msg'=>'Выберите документ!']);
    }

    public function deleteMedCardDocument()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['imagePath']))
        {
            if(file_exists(FCPATH.$_POST['imagePath']))
                unlink(FCPATH.$_POST['imagePath']);
            $delete_id = $this->Med_card_files_model->delete_by_file($_POST['imagePath']);
            echo json_encode(['success'=>true,'msg'=>'Документ удален!']);
        }
        else
            echo json_encode(['error'=>true,'msg'=>'Выберите документ!']);
    }

/**
 * [target - страница добавления новой цели]
 * @return [грузим страницу ввода новой цели]
 */
    public function target()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_mainMenuData(false);
            $data['target']['ID']=-1;
            $data['target']['NAME']='';
            $data['target']['DATE']='01/01/2000';
            $data['target']['TEXT']='';
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/target");
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [editTarget - грузим страницу редактирования цели]
 * @param  [string] $target_id [ИД редактируемой цели]
 * @return [страница редактирования цели]
 */
    public function editTarget($target_id=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($target_id)
        {
            $target=$this->Target_model->get_by_id($target_id);
            $data=$this->_mainMenuData(false);
            $data['target']=$target;
            if (empty($data['target']['DATE']))
                $data['target']['DATE']='01/01/2000';
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/target");
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [updateTarget - редактирование цели]
 * @return [новые данные цели в БД]
 */
    public function updateTarget()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['name']) and !empty($_POST['time']))
        {
            $time=explode('/',$_POST['time']);
            $start=explode('/',$_POST['start']);
            $time_from = '12:00';
            $time_to = '12:00';

            if (!empty($_POST['time_from']))
                $time_from = $_POST['time_from'];
            if (!empty($_POST['time_to']))
                $time_from = $_POST['time_to'];

            $fields = [
                'USER_ID' =>$this->user_id,
                'TEXT'    =>$_POST['text'],
                'TIME'    =>$time[2].'-'.$time[1].'-'.$time[0].' '.$_POST['time_to'].':00',
                'NAME'    =>$_POST['name'],
                'START'   =>$start[2].'-'.$start[1].'-'.$start[0].' '.$_POST['time_from'].':00'
            ];
            $update_id = $this->Target_model->update($fields,$_POST['_hidden-id']);
            echo json_encode(['success'=>true,'msg'=>'Цель успешно изменена!']);
        }
        else
            echo json_encode(['error'=>'Введите срок и название цели!']);
    }
/**
 * [sendTarget - добавление цели]
 * @return [новая цель в БД]
 */
    public function sendTarget()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['name']) and !empty($_POST['time']))
        {
            $time=explode('/',$_POST['time']);
            $start=explode('/',$_POST['start']);
            $time_from = '12:00';
            $time_to = '12:00';

            if (!empty($_POST['time_from']))
                $time_from = $_POST['time_from'];
            if (!empty($_POST['time_to']))
                $time_from = $_POST['time_to'];

            $fields = [
                'USER_ID' =>$this->user_id,
                'TEXT'    =>$_POST['text'],
                'TIME'    =>$time[2].'-'.$time[1].'-'.$time[0].' '.$_POST['time_to'].':00',
                'NAME'    =>$_POST['name'],
                'START'   =>$start[2].'-'.$start[1].'-'.$start[0].' '.$_POST['time_from'].':00'
            ];

            if($insert_id = $this->Target_model->create($fields))
                echo json_encode(['success'=>true,'msg'=>'Цель успешно добавлена!']);
            else
                echo json_encode(['error'=>'Не удалось добавить цель!']);
        }
        else
            echo json_encode(['error'=>'Введите срок и название цели!']);
    }
/**
 * [targets страница спика целей]
 * @param  [string] $liked [категория - все, избранные]
 * @return [type]        [грузим страницу с целями]
 */
    public function targets($liked="all")
    {    /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {    /*иначе загружаем страницу с его целями*/
            $data=$this->_mainMenuData(false);
            $data['liked']=$liked;
            if($liked=='liked')
                $data['targets']=$this->Target_model->get_by_user_id_liked($this->user_id);
            else
                $data['targets']=$this->Target_model->get_by_user_id($this->user_id);
            $targets=[];

            foreach ($data['targets'] as $key=>$target)
            {
//                $target=$this->_formatTime($target);
                $targets[$key]=$target;
            }
            $data['targets']=$targets;
            $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/targets",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [deleteTarget - удаление цели]
 * @return [удаление цели из БД]
 */
    public function deleteTarget()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Target_model->delete($_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Цель удалена!']);
        }
        else
            echo json_encode(['error'=>'Выберите цель!']);
    }
/**
 * [likedTarget - добавление цели в избранное]
 * @return [в БД цель отмечена как избранная]
 */
    public function likedTarget()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $update_id = $this->Target_model->update(['LIKED'=>1],$_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Цель добавлена!']);
        }
        else
            echo json_encode(['error'=>'Выберите цель!']);
    }
/**
 * [deleteLikedTarget - удаление цели из избранного]
 * @return [в БД цель больше не отмечена как избранная]
 */
    public function deleteLikedTarget()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $update_id = $this->Target_model->update(['LIKED'=>0],$_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Цель удалена из избранного!']);
        }
        else
            echo json_encode(['error'=>'Выберите цель!']);
    }
/**
 * [post - страница написания письма]
 * @return [грузим страницу с адресной книгой и формой написания письма]
 */
    public function post($destination_id='',$destination_type='user')
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_addressBookData();
            if($destination_id=='admin')
                $data['destination_email']='Администратор';
            elseif($destination_id != '')
            {
                if($destination_type=='institution')
                {
                    $destination=$this->Institution_model->get_by_id($destination_id);
                    $data['destination_email']=$destination['EMAIL'];
                }
                elseif($destination_type=='specialist')
                {
                    $destination=$this->Doctor_model->get_by_id($destination_id);
                    $data['destination_email']=$destination['EMAIL'];
                }
                else
                {
                    $destination=$this->User_model->get_by_id($destination_id);
                    $data['destination_email']=$destination['EMAIL'];
                }
            }
            else
                $data['destination_email']='';
            $data['post_theme']='';
            $data['post_message']='';
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/post",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }

    public function resendPost($post_id=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($post_id)
        {
            $post=$this->Post_model->get_by_id($post_id);
            $data=$this->_addressBookData();
            $data['post_theme']=$post['THEME'];
            $data['post_message']=$post['MESSAGE'];
            $data['destination_email']='';
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/post",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function respondPost($post_id=false,$destination_id='admin')
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($post_id and $destination_id)
        {
            $post=$this->Post_model->get_by_id($post_id);
            $data=$this->_addressBookData();
            $data['post_theme']=$post['THEME'];
            $data['post_message']='';
            if($destination_id=='admin')
                $data['destination_email']='Администратор';
            else
            {
                $destination_user=$this->User_model->get_by_id($destination_id);
               
                 
                if($destination_user['TYPE']==3)
                {
                   
                    $institutionThis=$this->Institution_model->get_by_user_id($destination_id);
                    $data['destination_email']=$institutionThis['EMAIL'];
                    if(empty($institutionThis)){
                        $data['destination_email']=$destination_user['EMAIL'];
                    }
                    
                   
                }
                else
                    $data['destination_email']=$destination_user['EMAIL'];
            }
            $data['responded_post_id']=$post_id;
            $this->load->view("tmp/profile-templates/mainmenu",$data);
           $this->load->view("profile/post",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

/**
 * [resend_specialist - страница написания письма]
 * @return [грузим страницу с адресной книгой и формой написания письма]
 */
    public function resend_specialist($specialist_id=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($specialist_id and $specialist_exist=$this->Doctor_model->get_by_id($specialist_id) != NULL)
        {
            $data=$this->_addressBookData();
            $data['specialist']=$this->Doctor_model->get_by_id($specialist_id);
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/resend_specialist",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function institutionHelpInstruction()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_addressBookData();

            $user = $this->db->where('ID',$this->user_id)->get('USERS')->row_array();
            if (!empty($user))
                $data['user_type'] = $user['TYPE'];
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution_help_instruction",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }

/**
 * [searchAddress - поиск в адресной книге]
 * @return [кусок страницы с результатами поиска]
 */
    public function searchAddress()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data['addressUsers']=$this->Address_book_model->get_by_user_id_users($this->user_id,$_POST['search']);
            $data['addressSpecialists']=$this->Address_book_model->get_by_user_id_specialists($this->user_id,$_POST['search']);
            $data['addressInstitutions']=$this->Address_book_model->get_by_user_id_institutions($this->user_id,$_POST['search']);
            echo json_encode(['success'=>true,'addressBook'=>$this->load->view("tmp/profile-templates/address_book", $data, true)]);
        }
    }
/**
 * [sendPost - отправка письма]
 * @return [новое письмо в БД]
 */
    public function sendPost()
    {
      
        if (!empty($_POST['destinations'][0]))
            $email_destination = $_POST['destinations'][0];

        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($email_destination))
        {
            if($destination_exist=$this->User_model->get_by_email($email_destination) != NULL
               or $institution_exist=$this->Institution_model->get_by_email($email_destination) != NULL)
                {
                  if(!empty($_POST['idsender'])){
                     
                    $data=$this->_postSenderData($_POST['idsender']);   
                      
                  }
                   else{
                        $data=$this->_postSenderData('');   
                       
                   }
 // PRINT_R( $data);
    
        
                    if (!empty($_POST['atach_med_card']) && $_POST['atach_med_card'] == 'on')
                        $data['fields']['ATACH_MED_CARD'] = 1;
                    else
                        $data['fields']['ATACH_MED_CARD'] = 0;

                    if($insert_post_id = $this->Post_model->create($data['fields']))
                    {
                        echo json_encode(['success'=>true,'msg'=>'Письмо успешно отправлено!']);
                        $this->_addressDestinationPost($insert_post_id,$email_destination,$data);
                    }
                    else
                        echo json_encode(['error'=>'Не удалось отправить письмо!']);
                }
                else
                    echo json_encode(['error'=>'Нет получателя с email '.$email_destination.'!']);
        }
        else
            echo json_encode(['error'=>'Введите email получателя!']);
    }

    public function resendSpecialist()
    {
        if (!empty($_POST['destinations'][0]))
            $email_destination = $_POST['destinations'][0];

        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($email_destination))
        {
            $user=$this->User_model->get_by_id($this->user_id);
            $this->email->from($user['EMAIL']);
            $this->email->to($_POST['destinations'][0]);
            $this->email->subject($_POST['theme']);
            $resend_specialist_id=$_POST['resend_specialist_id'];
            $specialist=$this->Doctor_model->get_by_id($resend_specialist_id);
            $message=$_POST['message']."\n".'ФИО: '.$specialist['LAST_NAME'].' '.$specialist['FIRST_NAME'].
            ' '.$specialist['SECOND_NAME']."\n".'Должность: '.$specialist['POSITION']."\n".'Ссылка: '.
            $_SERVER['SERVER_NAME'].'/profile/specialistDetail/'.$resend_specialist_id;
            $this->email->message($message);
            if(!empty($_POST['images']))
                foreach ($_POST['images'] as $imagePath)
                    if(is_file(FCPATH.$imagePath))
                        $this->email->attach(FCPATH.$imagePath);
            if($this->email->send())
            {
                echo json_encode(['success'=>true,'msg'=>'Письмо успешно отправлено!']);
                if($destination_exist=$this->User_model->get_by_email($email_destination) != NULL
                   or $institution_exist=$this->Institution_model->get_by_email($email_destination) != NULL)
                    {
                        $data=$this->_postSenderData();
                        $data['fields']['MESSAGE']=str_replace("\n",'<br>',$message);
                        if($insert_post_id = $this->Post_model->create($data['fields']))
                            $this->_addressDestinationPost($insert_post_id,$email_destination,$data);
                    }
            }
            else
                echo json_encode(['error'=>'Не удалось отправить письмо c данными специалиста!']);
        }
        else
            echo json_encode(['error'=>'Введите email получателя!']);

    }    

    public function sendPostToAdmin()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['destinations'][0]))
        {
            $data=$this->_postSenderData();
            if($insert_post_id = $this->Post_model->create($data['fields']))
            {    /*вбиваем получателей*/
                $fieldsDestination = [
                    'ID_POST'            =>$insert_post_id,
                    'DESTINATION_NAME'   =>'Администратор',
                    'DELETED'            =>0,
                    'FINAL_DELETED'      =>0,
                    'DESTINATION_READED' =>0,
                    'TO_ADMIN'           =>1
                ];
                if($insert_destination_id = $this->Post_destinations_model->create($fieldsDestination))
                {
                    echo json_encode(['success'=>true,'msg'=>'Письмо успешно отправлено!']);
                    $this->_imagesPost($insert_post_id);
                }
            }
            else
                echo json_encode(['error'=>'Не удалось отправить письмо!']);
        }
        else
            echo json_encode(['error'=>'Введите email получателя!']);
    }

/**
 * [posts - грузим письма нужной категории]
 * @param  [string] $category [фильтрация писем - входящие, исходящие, корзина, избранные]
 * @return [страница писем нужной категории]
 */
    public function posts($category='all')
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {    /*иначе загружаем страницу с его письмами*/
            $data=$this->_postsData($category,false);
            $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
           $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/posts",$data);
            /*грузим футер*/
            $this->load->view("tmp/profile-templates/footer");
        }
    }
    
  
       public function  opinions($id,$idsenders,$destination_id='',$destination_type='user')
    {   
        
        $idsender=$this->db->query("SELECT ID_SENDER FROM POSTS WHERE ID =".(int)$id)->row_array();
        
        $emailsender=$this->db->query("SELECT EMAIL FROM USERS WHERE ID =".(int)$idsender['ID_SENDER'])->row_array();
       
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_addressBookData();
             
            if($destination_id=='admin')
                $data['destination_email']='Администратор';
            elseif($destination_id != '')
            {
               
                if($destination_type=='institution')
                {
                    $destination=$this->Institution_model->get_by_id($destination_id);
                    $data['destination_email']=$destination['EMAIL'];
                }
                elseif($destination_type=='specialist')
                {
                    $destination=$this->Doctor_model->get_by_id($destination_id);
                    $data['destination_email']=$destination['EMAIL'];
                }
                else
                {
                    $destination=$this->User_model->get_by_id($destination_id);
                    $data['destination_email']=$destination['EMAIL'];
                }
            }
            else
                $data['destination_email']=$emailsender['EMAIL'];
            $data['post_theme']='';
            $data['post_message']='';
            $data['idsender']=$idsenders;
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/opinions",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
    }
    
/**
 * [ajaxSearchPosts - ищет письма]
 * @param  [string] $category [фильтрация писем - входящие, исходящие, корзина, избранные]
 * @return загружает страницу с результатами
 */
    public function ajaxSearchPosts($category='all')
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {    /*иначе загружаем страницу с его письмами*/
            $data=$this->_postsData($category,$_GET['get-search']);
            $data['getSearch']=$_GET['get-search'];
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/posts",$data);
            //грузим футер
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [deletePost - помещение письма в корзину]
 * @return [в БД письмо отмечено как удалённое]
 */
    public function deletePost()
    {
        $this->_deleteOrRestorePost(1,'в корзине!');
    }

    public function restorePost()
    {
        $this->_deleteOrRestorePost(0,'восстановлено!');
    }

/**
 * [deletePostFinal - полное удаление]
 * @return [в БД письмо отмечено как полностью удалённое либо удалено из БД]
 */
    public function deletePostFinal()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $post=$this->Post_model->get_by_id($_POST['target-id']);
            if ($post['ID_SENDER']==$this->user_id)
            {
                $destinations = $this->Post_destinations_model->get_by_post_id($_POST['target-id']);
                $destCount=0;
                foreach ($destinations as $destination)
                    if ($destination['FINAL_DELETED']==1)
                    {
                        $destCount=$destCount+1;
                        break(1);
                    }
                if ($destCount==0)
                    $update_id = $this->Post_model->update(['FINAL_DELETED'=>1],$_POST['target-id']);
                else
                {
                    $delete_id = $this->Post_model->delete($_POST['target-id']);
                    $filesOld=$this->Post_files_model->get_file_by_post_id($_POST['target-id']);
                    foreach ($filesOld as $potDel)
                        unlink(FCPATH.$potDel['FILE']);
                    $delete_files_id = $this->Post_files_model->delete($_POST['target-id']);
                    $delete_destinations_id=$this->Post_destinations_model->delete($_POST['target-id']);
                }
            }
            else
            {
                $update_id = $this->Post_destinations_model->update(['FINAL_DELETED'=>1],$_POST['target-id'],$this->user_id);
                $destinations = $this->Post_destinations_model->get_by_post_id($_POST['target-id']);
                $destCount=0;
                foreach ($destinations as $destination)
                    if ($destination['FINAL_DELETED']!=1)
                    {
                        $destCount=$destCount+1;
                        break(1);
                    }
                if($destCount==0 and $post['FINAL_DELETED']==1)
                {
                    $delete_id = $this->Post_model->delete($_POST['target-id']);
                    $filesOld=$this->Post_files_model->get_file_by_post_id($_POST['target-id']);
                    foreach ($filesOld as $potDel)
                        unlink(FCPATH.$potDel['FILE']);
                    $delete_files_id = $this->Post_files_model->delete($_POST['target-id']);
                    $delete_destinations_id=$this->Post_destinations_model->delete($_POST['target-id']);
                }
            }
            echo json_encode(['success'=>true,'msg'=>'Письмо удалено!']);
        }
        else
            echo json_encode(['error'=>'Выберите письмо!']);
    }
/**
 * [likedPost - добавление письма в избранное]
 * @return [в БД письмо отмечено как избранное]
 */
    public function likedPost()
    {
        $this->_likedOrDeletePost(1,'добавлено!');
    }
/**
 * [deleteLikedPost - удаление письма из избранного]
 * @return [в БД с письма снимается отметка избранного]
 */
    public function deleteLikedPost()
    {
        $this->_likedOrDeletePost(0,'удалено!');
    }
/**
 * [readPost - прочтение письма]
 * @return [в БД письмо отмечено как прочтённое]
 */
    public function readPost()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
           
                
                  $update_id = $this->Post_destinations_model->update(['DESTINATION_READED'=>1],$_POST['target-id'],$this->user_id); 
                   $this->db->set('DESTINATION_READED', '1', FALSE);
                   $this->db->where('ID_POST',$_POST['target-id']);
                   $this->db->update('POST_DESTINATIONS'); 
        
           
            echo json_encode(['success'=>true,'msg'=>'Письмо прочтено!']);
        }
        else
            echo json_encode(['error'=>'Выберите письмо!']);
    }
/**
 * [specialistDetail - грузит детальную страницу специалиста из избранного]
 * @param  [string] $specialist_id [ИД специалиста]
 * @return [детальная страница специалиста из избранного]
 */
    public function specialistDetail($specialist_id=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($specialist_id)
        {
            $data=$this->_specialistDetail($specialist_id);
            $data['specialist']=$this->Doctor_model->get_by_id($specialist_id);
            $timeFrom=['monday'=>'MONDAY_WORK_FROM','tuesday'=>'TUESDAY_WORK_FROM','wednesday'=>'WEDNESDAY_WORK_FROM','thursday'=>'THURSDAY_WORK_FROM','friday'=>'FRIDAY_WORK_FROM','saturday'=>'SATURDAY_WORK_FROM','sunday'=>'SUNDAY_WORK_FROM'];
            $timeTo=['monday'=>'MONDAY_WORK_TO','tuesday'=>'TUESDAY_WORK_TO','wednesday'=>'WEDNESDAY_WORK_TO','thursday'=>'THURSDAY_WORK_TO','friday'=>'FRIDAY_WORK_TO','saturday'=>'SATURDAY_WORK_TO','sunday'=>'SUNDAY_WORK_TO'];
            $weekDays=['monday'=>1,'tuesday'=>2,'wednesday'=>3,'thursday'=>4,'friday'=>5,'saturday'=>6,'sunday'=>7];
            $data['weekLetters']=['monday'=>'Пн','tuesday'=>'Вт','wednesday'=>'Ср','thursday'=>'Чт','friday'=>'Пт','saturday'=>'Сб','sunday'=>'Вс'];
            foreach ($weekDays as $key => $weekDay)
            {
                $data['weekDays'][$key]=date("d", mktime(0, 0, 0, date("m"), date("d")+$weekDay-date("w"), date("Y")));
                $data['timeFrom'][$key]=substr($data['specialist'][$timeFrom[$key]],0,5);
                $data['timeTo'][$key]=substr($data['specialist'][$timeTo[$key]],0,5);
            }
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/specialist_detail",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
/**
 * [specialistComments - грузит детальную страницу врача с отзывами]
 * @return [детальная страница специалиста с отзывами]
 */
    public function specialist_comments()
    {
         if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif( $this->user_id!= NULL) 
        {       
       
           // print_r($this->user_id);  
            $data=$this->_mainMenuData(false);
             $data['posts']=$this->Specialist-> get_by_specialistopinion($this->user_id);
            // print_r( $data['posts']); 
            //$data=$this->_mainMenuData(false);
            $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution_comments",$data);
           $this->load->view("tmp/profile-templates/footer");
        }
    }
    
     public function publication($id)
    {
         if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif( $this->user_id!= NULL) 
        {       
           
           $this->db->set('MODERATED', '1', FALSE);
           $this->db->where('ID', $id);
           $this->db->update('POSTS'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
               $idfeedback=$this->db->query('SELECT IDFEEDBACK FROM POSTS WHERE ID = '.$id);
        $result=$idfeedback->result();
        $feedback=$result[0]->IDFEEDBACK;
       
         $this->db->set('MODERATED', '1', FALSE);
           $this->db->where('ID', $feedback);
           $this->db->update('FEEDBACKS');
           header("Location: ".$_SERVER['HTTP_REFERER']);
          
        }
    }
    
       public function nopublication($id)
    {
         if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif( $this->user_id!= NULL) 
        {       
           
           $this->db->set('MODERATED', '0', FALSE);
           $this->db->where('ID', $id);
           $this->db->update('POSTS'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
               $idfeedback=$this->db->query('SELECT IDFEEDBACK FROM POSTS WHERE ID = '.$id);
        $result=$idfeedback->result();
        $feedback=$result[0]->IDFEEDBACK;
       
         $this->db->set('MODERATED', '0', FALSE);
           $this->db->where('ID', $feedback);
           $this->db->update('FEEDBACKS');
           header("Location: ".$_SERVER['HTTP_REFERER']);
          
        }
    }
    
    
    public function deleteposts($id)
    {
         if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif( $this->user_id!= NULL) 
        {  
        
      
    
        if (!empty($id)){
          $idfeedback=$this->db->query('SELECT IDFEEDBACK FROM POSTS WHERE ID = '.$id);
        $result=$idfeedback->result();
        $feedback=$result[0]->IDFEEDBACK;
             //exit();
            $this->db->query('DELETE FROM FEEDBACKS WHERE ID = '.$feedback);
            $this->db->query('DELETE FROM POSTS WHERE ID = '.$id);
            header("Location: ".$_SERVER['HTTP_REFERER']);
        }
            
                
          // PRINT_R($id);
  
           // $this->load->view("tmp/profile-templates/mainmenu",$data);
           // $this->load->view("profile/institution_comments",$data);
          // $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [institution_comments - грузит детальную страницу учреждения с отзывами]
 * @return [детальная страница учреждения с отзывами]
 */
    public function institution_comments()
    {
       
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif( $this->user_id!= NULL) 
        {    
            $data=$this->_mainMenuData(false);
             $data['posts']=$this->Orgnisations->get_by_organisations_id($this->user_id);   
             $data['iduser']=$this->Orgnisations->get_by_organisation_user_id($this->user_id);
            
            $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);
            //print_r($data['unreaded']);
             // print_r($data['posts']);
            
             
             $data['organization2']=1; 
             $data['user_id']=$this->user_id;
              
            //$data=$this->_mainMenuData(false);
   
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/institution_comments",$data);
           $this->load->view("tmp/profile-templates/footer");
        }
       
    }
/**
 * [schedule_day - график целей за текущий и следующий день]
 * @return [загружает страницу графика]
 */
    public function schedule_day()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_mainMenuData(false);
            $data['hours']=['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','24:00'];
            $dateFrom=date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
            $dateTo=date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d")+1, date("Y")));
            $targets=$this->Target_model->get_by_user_id_time($this->user_id,$dateFrom,$dateTo);
            $dateCur=date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d"), date("Y")));
            $this->_allScheduleData($data,$targets,$dateCur);
            $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            $user=$this->User_model->get_by_id($this->user_id);
            if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/schedule_day",$data);
            $this->load->view("tmp/profile-templates/schedule_event");
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [schedule_week - график целей за текущую и следующую неделю]
 * @return [загружает страницу графика]
 */
    public function schedule_week()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_mainMenuData(false);
            $data['weekLetters']=['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
            $data['hours']=['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','24:00'];
            $dateFrom=date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")+1-date("w"), date("Y")));
            $dateTo=date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d")+14-date("w"), date("Y")));
            $targets=$this->Target_model->get_by_user_id_time($this->user_id,$dateFrom,$dateTo);
            $dateCur=date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("d")+7-date("w"), date("Y")));
            $this->_allScheduleData($data,$targets,$dateCur);
            $this->load->view("profile/schedule_week",$data);
            $this->load->view("tmp/profile-templates/schedule_event");
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [schedule_month - график целей за текущий и следующий месяц]
 * @return [загружает страницу графика]
 */
    public function schedule_month()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $data=$this->_mainMenuData(false);
            $data['weekLetters']=['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
            $dateFrom=date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), 1, date("Y")));
            $dateTo=date("Y-m-d H:i:s", mktime(23, 59, 59, date("m")+2, 0, date("Y")));
            $targets=$this->Target_model->get_by_user_id_time($this->user_id,$dateFrom,$dateTo);
            $dateCur=date("Y-m-d H:i:s", mktime(23, 59, 59, date("m")+1, 0, date("Y")));
            $this->_allScheduleData($data,$targets,$dateCur);
            $this->load->view("profile/schedule_month",$data);
            $this->load->view("tmp/profile-templates/schedule_event");
            $this->load->view("tmp/profile-templates/footer");
        }
    }
/**
 * [vk_auth - формирует ссылку на профиль пользователя в социальной сети]
 * @return [в БД появляется ссылка на профиль пользователя в социальной сети]
 */
    public function vk_auth()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif (!empty($_GET['code']))
        {
            $params = [
                'client_id' => '5796160',
                'client_secret' => 'UhAFY9tWNuJ44aS5XreK',
                'code' => $_GET['code'],
                'redirect_uri' => 'http://'.$_SERVER['SERVER_NAME'].'/profile/vk_auth'
            ];
            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token?'.urldecode(http_build_query($params))), true);
            if (!empty($token['access_token']))
            {
                $params = [
                    'uids'         => $token['user_id'],
                    'fields'       => 'uid,screen_name',
                    'access_token' => $token['access_token']
                ];
                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get?'.urldecode(http_build_query($params))), true);
                if (!empty($userInfo['response'][0]['uid']))
                    $this->_userSocNet(1,$userInfo['response'][0]['uid']);
                else
                    show_404();
            }
            else
                show_404();
        }
        else
            show_404();
    }
    public function fb_auth()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif (!empty($_GET['code']))
        {
            $params = [
                'client_id' => '762207637266704',
                'client_secret' => '3576152e685311d27fcbd120fc34a518',
                'code' => $_GET['code'],
                'redirect_uri' => 'http://'.$_SERVER['SERVER_NAME'].'/profile/fb_auth'
            ];
            parse_str(file_get_contents('https://graph.facebook.com/oauth/access_token?'.http_build_query($params)), $token);
            if (!empty($token['access_token']))
            {
                $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me?'.urldecode(http_build_query(['access_token' => $token['access_token']]))),true);
                // print_r($userInfo);
                // exit();
                if(!empty($userInfo['id']))
                    $this->_userSocNet(3,$userInfo['id']);
                else
                    show_404();
            }
            else
                show_404();
        }
        else
            show_404();
    }
    public function ok_auth()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif (!empty($_GET['code']))
        {
            $client_secret='66D7CA5C071D6A9DDFA91593';
            $result=$this->_curlSocNetToken($_GET['code'],'/profile/ok_auth','1247414528',$client_secret,'http://api.odnoklassniki.ru/oauth/token.do');
            $tokenInfo = json_decode($result, true);
            if (!empty($tokenInfo['access_token']))
            {
                $application_key='CBAKALFLEBABABABA';
                $params = [
                    'method'          => 'users.getCurrentUser',
                    'access_token'    => $tokenInfo['access_token'],
                    'application_key' => $application_key,
                    'format'          => 'json',
                    'sig'             => md5('application_key='.$application_key.'format=jsonmethod=users.getCurrentUser'.md5($tokenInfo['access_token'].$client_secret))
                ];
                $userInfo = json_decode(file_get_contents('http://api.odnoklassniki.ru/fb.do?'.urldecode(http_build_query($params))), true);
                if (!empty($userInfo['uid']))
                    $this->_userSocNet(2,$userInfo['uid']);
                else
                    show_404();
            }
            else
                show_404();
        }
        else
            show_404();
    }
    public function tw_auth()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif (!empty($_GET['oauth_token']) and !empty($_GET['oauth_verifier']))
        {
            $this->_twitterConstants();
            $oauth_nonce = md5(uniqid(rand(), true));
            $oauth_timestamp = time();
            $oauth_token = $_GET['oauth_token'];
            $oauth_verifier = $_GET['oauth_verifier'];
            $oauth_base_text = "GET&";
            $oauth_base_text .= urlencode(ACCESS_TOKEN_URL)."&";
            $params = [
                'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
                'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
                'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
                'oauth_token=' . $oauth_token . URL_SEPARATOR,
                'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
                'oauth_verifier=' . $oauth_verifier . URL_SEPARATOR,
                'oauth_version=1.0'
            ];
            $oauth_base_text = 'GET'.URL_SEPARATOR.urlencode(ACCESS_TOKEN_URL).URL_SEPARATOR.implode('', array_map('urlencode', $params));
            /*получаем токен доступа*/
            $params = [
                'oauth_nonce='.$oauth_nonce,
                'oauth_signature_method=HMAC-SHA1',
                'oauth_timestamp='.$oauth_timestamp,
                'oauth_consumer_key='.CONSUMER_KEY,
                'oauth_token='.urlencode($oauth_token),
                'oauth_verifier='.urlencode($oauth_verifier),
                'oauth_signature='.urlencode(base64_encode(hash_hmac("sha1", $oauth_base_text, CONSUMER_SECRET.URL_SEPARATOR, true))),
                'oauth_version=1.0'
            ];
            parse_str(file_get_contents(ACCESS_TOKEN_URL . '?' . implode('&', $params)), $response);
            $oauth_nonce = md5(uniqid(rand(), true));
            $oauth_timestamp = time();
            $oauth_token = $response['oauth_token'];
            $screen_name = $response['screen_name'];
            $params = [
                'oauth_consumer_key=' . CONSUMER_KEY . URL_SEPARATOR,
                'oauth_nonce=' . $oauth_nonce . URL_SEPARATOR,
                'oauth_signature_method=HMAC-SHA1' . URL_SEPARATOR,
                'oauth_timestamp=' . $oauth_timestamp . URL_SEPARATOR,
                'oauth_token=' . $oauth_token . URL_SEPARATOR,
                'oauth_version=1.0' . URL_SEPARATOR,
                'screen_name=' . $screen_name
            ];
            $oauth_base_text = 'GET' . URL_SEPARATOR . urlencode(ACCOUNT_DATA_URL) . URL_SEPARATOR . implode('', array_map('urlencode', $params));
            /*получаем данные о пользователе*/
            $params = [
                'oauth_consumer_key=' . CONSUMER_KEY,
                'oauth_nonce=' . $oauth_nonce,
                'oauth_signature=' . urlencode(base64_encode(hash_hmac("sha1", $oauth_base_text, CONSUMER_SECRET.'&'.$response['oauth_token_secret'], true))),
                'oauth_signature_method=HMAC-SHA1',
                'oauth_timestamp=' . $oauth_timestamp,
                'oauth_token=' . urlencode($oauth_token),
                'oauth_version=1.0',
                'screen_name=' . $screen_name
            ];
            $userInfo = json_decode(file_get_contents(ACCOUNT_DATA_URL . '?' . implode(URL_SEPARATOR, $params)), true);
            if (!empty($userInfo['screen_name']))
                $this->_userSocNet(4,$userInfo['screen_name']);
            else
                show_404();
        }
        else
            show_404();
    }
    public function ig_auth()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif (!empty($_GET['code']))
        {
            $result=$this->_curlSocNetToken($_GET['code'],'/profile/ig_auth','1c07dcf5436445cd823ca6c348be1c5f ',' 0f014f68b03d4c6f8f3cb4bafffcb2d0','https://api.instagram.com/oauth/access_token');
            $tokenInfo = json_decode($result, true);
            if (!empty($tokenInfo['access_token']) and !empty($tokenInfo['user']['username']))
                $this->_userSocNet(5,$tokenInfo['user']['username']);
            else
                show_404();
        }
        else
            show_404();
    }
    public function in_auth()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_GET['code']))
        {
            $result=$this->_curlSocNetToken($_GET['code'],'/profile/in_auth','774l5ffqicjnl4','piRK9CeHLdxgqD2L','https://www.linkedin.com/oauth/v2/accessToken');
            $tokenInfo = json_decode($result, true);
            if (!empty($tokenInfo['access_token']))
            {
                $userInfo=$this->_fetchLinkedin('GET', '/v1/people/~', $tokenInfo['access_token']);
                $this->_userSocNet(6,$userInfo->siteStandardProfileRequest->url);
            }
            else
                show_404();
        }
        else
            show_404();
    }

    public function userSocNetDelete()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Soc_net_model->delete($this->user_id,$_POST['target-id']);
            if($_POST['target-id']==4)
                $authLink = $this->_twitterAuthLink();
            else
            {
                $soc_net = $this->Soc_net_model->get_by_id($_POST['target-id']);
                $authLink = $soc_net['AUTH_LINK'];
            }
            echo json_encode(['success'=>true,'msg'=>'<a href="'.$authLink.'"></a>']);
        }
        else
            echo json_encode(['error'=>'Выберите соцсеть!']);
    }

    public function specialist_patients()
    {
        if(!$this->user_id)
        {
           redirect('/profile/auth?back_url=/profile'); 
        }
            
        elseif($this->Doctor_model->get_by_user_id($this->user_id) != NULL)
        {
            $specialist=$this->Doctor_model->get_by_user_id($this->user_id);
            $patients=$this->Patients_model->get_by_specialist_id($specialist['ID']);
            $this->_patients($patients);
        }
        else{
           $patients=[];
            $this->_patients($patients); 
        }
        
    }
    
    public function institution_patients()
    {           //print_r($this->user_id);
        if(empty($this->user_id)){
          redirect('/profile/auth?back_url=/profile');  
        }
            
        elseif($this->Institution_model->get_by_user_id($this->user_id) != NULL)
        {         
            
            $institution=$this->Institution_model->get_by_user_id($this->user_id);
            //print_r($institution);
            $patients=$this->Patients_model->get_by_institution_id($institution['ID']);
            //print_r($patients);
            $this->_patients($patients);
        }
        else  {
               $patients=[];
            $this->_patients($patients);
            
        }
        
            //$institution=$this->Institution_model->get_by_user_id($this->user_id);
            //print_r($institution);
            //$patients=$this->Patients_model->get_by_institution_id($institution['ID']);
            //print_r($patients);
         
    }

    public function branches()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($this->Institution_model->get_by_user_id($this->user_id) != NULL)
        {
            $institution=$this->Institution_model->get_by_user_id($this->user_id);
            $data=$this->_mainMenuData(false);
            $branches=$this->Branches_model->get_by_institution_id($institution['ID']);
            $timeFrom=['MONDAY_WORK_FROM','TUESDAY_WORK_FROM','WEDNESDAY_WORK_FROM','THURSDAY_WORK_FROM','FRIDAY_WORK_FROM','SATURDAY_WORK_FROM','SUNDAY_WORK_FROM'];
            $timeTo=['MONDAY_WORK_TO','TUESDAY_WORK_TO','WEDNESDAY_WORK_TO','THURSDAY_WORK_TO','FRIDAY_WORK_TO','SATURDAY_WORK_TO','SUNDAY_WORK_TO'];
            $data['weekLetters']=['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
            $newBranches=[];
            foreach ($branches as $branch)
            {
                foreach ($timeFrom as $key => $tf)
                {
                    $branch['timeFrom'][$key]=substr($branch[$tf],0,5);
                    $branch['timeTo'][$key]=substr($branch[$timeTo[$key]],0,5);
                }
                $newBranches[]=$branch;
            }
            $data['branches']=$newBranches;
            $data['metros']=$this->Metro_model->get_id_name();

            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/branches",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }
    /**
     * [addBranch - добавление отделения]
     * @return [новое отделение в БД]
     */
    public function addBranch()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['name']))
        {
            $institution=$this->Institution_model->get_by_user_id($this->user_id);

            $fields = [
                'INSTITUTION_ID'      =>$institution['ID'],
                'PHONE1'              =>$_POST['phone1'],
                'PHONE2'              =>$_POST['phone2'],
                'PHONE3'              =>$_POST['phone3'],
                'LATITUDE'            =>$_POST['latitude'],
                'LONGITUDE'           =>$_POST['longitude'],
                'ADDRESS_INDEX'       =>!empty($_POST['actual_address_index']) ? $_POST['actual_address_index'] : NULL,
                'ADDRESS_REGION'      =>$_POST['actual_address_region'],
                'ADDRESS_CITY'        =>$_POST['actual_address_city'],
                'ADDRESS_STREET'      =>$_POST['actual_address_street'],
                'ADDRESS_HOUSE'       =>$_POST['actual_address_house'],
                'ADDRESS_BUILDING'    =>$_POST['actual_address_building'],
                'ADDRESS_OFFICE'      =>$_POST['actual_address_office'],
                'LEGAL_ADDRESS_INDEX'       =>!empty($_POST['legal_address_index']) ? $_POST['legal_address_index'] : NULL,
                'LEGAL_ADDRESS_REGION'      =>$_POST['legal_address_region'],
                'LEGAL_ADDRESS_CITY'        =>$_POST['legal_address_city'],
                'LEGAL_ADDRESS_STREET'      =>$_POST['legal_address_street'],
                'LEGAL_ADDRESS_HOUSE'       =>$_POST['legal_address_house'],
                'LEGAL_ADDRESS_BUILDING'    =>$_POST['legal_address_building'],
                'LEGAL_ADDRESS_OFFICE'      =>$_POST['legal_address_office'],
                'EMAIL'               =>$_POST['email'],
                'WEBSITE1'            =>$_POST['website1'],
                'WEBSITE2'            =>$_POST['website2'],
                'WEBSITE3'            =>$_POST['website3'],
                'OGRN'                    =>$_POST['ogrn'],
                'OKPO'                    =>$_POST['okpo'],
                'INN'                     =>$_POST['inn'],
                'KPP'                     =>$_POST['kpp'],
                'OKVED'                   =>$_POST['okved'],
                'OKTMO'                   =>$_POST['oktmo'],
                'ACCOUNT'                 =>$_POST['account'],
                'BANK'                    =>$_POST['bank'],
                'BIK'                     =>$_POST['bik'],
                'CHIEF_PHYSICIAN'         =>$_POST['chief_physician'],
                'CHIEF_ACCOUNTANT'        =>$_POST['chief_accountant'],
                'CEO'                     =>$_POST['ceo'],
                'MONDAY_WORK_FROM'        =>!empty($_POST['monday-work-from']) ? $_POST['monday-work-from'] : NULL,
                'MONDAY_WORK_TO'          =>!empty($_POST['monday-work-to']) ? $_POST['monday-work-to'] : NULL,
                'TUESDAY_WORK_FROM'       =>!empty($_POST['tuesday-work-from']) ? $_POST['tuesday-work-from'] : NULL,
                'TUESDAY_WORK_TO'         =>!empty($_POST['tuesday-work-to']) ? $_POST['tuesday-work-to'] : NULL,
                'WEDNESDAY_WORK_FROM'     =>!empty($_POST['wednesday-work-from']) ? $_POST['wednesday-work-from'] : NULL,
                'WEDNESDAY_WORK_TO'       =>!empty($_POST['wednesday-work-to']) ? $_POST['wednesday-work-to'] : NULL,
                'THURSDAY_WORK_FROM'      =>!empty($_POST['thursday-work-from']) ? $_POST['thursday-work-from'] : NULL,
                'THURSDAY_WORK_TO'        =>!empty($_POST['thursday-work-to']) ? $_POST['thursday-work-to'] : NULL,
                'FRIDAY_WORK_FROM'        =>!empty($_POST['friday-work-from']) ? $_POST['friday-work-from'] : NULL,
                'FRIDAY_WORK_TO'          =>!empty($_POST['friday-work-to']) ? $_POST['friday-work-to'] : NULL,
                'SATURDAY_WORK_FROM'      =>!empty($_POST['saturday-work-from']) ? $_POST['saturday-work-from'] : NULL,
                'SATURDAY_WORK_TO'        =>!empty($_POST['saturday-work-to']) ? $_POST['saturday-work-to'] : NULL,
                'SUNDAY_WORK_FROM'        =>!empty($_POST['sunday-work-from']) ? $_POST['sunday-work-from'] : NULL,
                'SUNDAY_WORK_TO'          =>!empty($_POST['sunday-work-to']) ? $_POST['sunday-work-to'] : NULL,
                'MONDAY_BREAK_FROM'       =>!empty($_POST['monday-break-from']) ? $_POST['monday-break-from'] : NULL,
                'MONDAY_BREAK_TO'         =>!empty($_POST['monday-break-to']) ? $_POST['monday-break-to'] : NULL,
                'TUESDAY_BREAK_FROM'      =>!empty($_POST['tuesday-break-from']) ? $_POST['tuesday-break-from'] : NULL,
                'TUESDAY_BREAK_TO'        =>!empty($_POST['tuesday-break-to']) ? $_POST['tuesday-break-to'] : NULL,
                'WEDNESDAY_BREAK_FROM'    =>!empty($_POST['wednesday-break-from']) ? $_POST['wednesday-break-from'] : NULL,
                'WEDNESDAY_BREAK_TO'      =>!empty($_POST['wednesday-break-to']) ? $_POST['wednesday-break-to'] : NULL,
                'THURSDAY_BREAK_FROM'     =>!empty($_POST['thursday-break-from']) ? $_POST['thursday-break-from'] : NULL,
                'THURSDAY_BREAK_TO'       =>!empty($_POST['thursday-break-to']) ? $_POST['thursday-break-to'] : NULL,
                'FRIDAY_BREAK_FROM'       =>!empty($_POST['friday-break-from']) ? $_POST['friday-break-from'] : NULL,
                'FRIDAY_BREAK_TO'         =>!empty($_POST['friday-break-to']) ? $_POST['friday-break-to'] : NULL,
                'SATURDAY_BREAK_FROM'     =>!empty($_POST['saturday-break-from']) ? $_POST['saturday-break-from'] : NULL,
                'SATURDAY_BREAK_TO'       =>!empty($_POST['saturday-break-to']) ? $_POST['saturday-break-to'] : NULL,
                'SUNDAY_BREAK_FROM'       =>!empty($_POST['sunday-break-from']) ? $_POST['sunday-break-from'] : NULL,
                'SUNDAY_BREAK_TO'         =>!empty($_POST['sunday-break-to']) ? $_POST['sunday-break-to'] : NULL,
                'NAME'                =>$_POST['name'],
                'PREMODERATE'         =>0
            ];

            if (!empty($_POST['state']) && $_POST['state'] == "true")
                $fields['STATE'] = 1;
            else
                $fields['STATE'] = 0;
            //------------------------
            if (!empty($_POST['private']) && $_POST['private'] == "true")
                $fields['PRIVATE'] = 1;
            else
                $fields['PRIVATE'] = 0;
            //------------------------
            if (!empty($_POST['children']) && $_POST['children'] == "true")
                $fields['CHILDREN'] = 1;
            else
                $fields['CHILDREN'] = 0;
            //------------------------
            if (!empty($_POST['ambulance']) && $_POST['ambulance'] == "true")
                $fields['AMBULANCE'] = 1;
            else
                $fields['AMBULANCE'] = 0;
            //------------------------
            if (!empty($_POST['house']) && $_POST['house'] == "true")
                $fields['HOUSE'] = 1;
            else
                $fields['HOUSE'] = 0;
            //------------------------
            if (!empty($_POST['booking']) && $_POST['booking'] == "true")
                $fields['BOOKING'] = 1;
            else
                $fields['BOOKING'] = 0;
            //------------------------
            if (!empty($_POST['delivery']) && $_POST['delivery'] == "true")
                $fields['DELIVERY'] = 1;
            else
                $fields['DELIVERY'] = 0;
            //------------------------
            if (!empty($_POST['daynight']) && $_POST['daynight'] == "true")
                $fields['DAYNIGHT'] = 1;
            else
                $fields['DAYNIGHT'] = 0;
            //------------------------
            if (!empty($_POST['dms']) && $_POST['dms'] == "true")
                $fields['DMS'] = 1;
            else
                $fields['DMS'] = 0;
            //------------------------
            if (!empty($_POST['dlo']) && $_POST['dlo'] == "true")
                $fields['DLO'] = 1;
            else
                $fields['DLO'] = 0;
            //------------------------
            if (!empty($_POST['optics']) && $_POST['optics'] == "true")
                $fields['OPTICS'] = 1;
            else
                $fields['OPTICS'] = 0;
            //------------------------
            if (!empty($_POST['rpo']) && $_POST['rpo'] == "true")
                $fields['RPO'] = 1;
            else
                $fields['RPO'] = 0;
            //------------------------
            if (!empty($_POST['homeopathy']) && $_POST['homeopathy'] == "true")
                $fields['HOMEOPATHY'] = 1;
            else
                $fields['HOMEOPATHY'] = 0;
            //------------------------

            if($insert_post_id = $this->Branches_model->create($fields)) {/*вбиваем получателей*/
                if (!empty($_POST['metro_id']) && is_array($_POST['metro_id'])) {

                    $current_metros = [];
                    $temp_metros = [];

                    if (!empty($inserted))
                        $temp_metros = $this->Branch_metros_model->get_branch_metro($insert_post_id);

                    foreach ($temp_metros as $metro) {
                        $current_metros[(int)$metro['METRO_ID']] = 1;
                    }

                    foreach ($_POST['metro_id'] as $metro_id) {
                        if (!empty($current_metros[$metro_id]))
                            unset($current_metros[$metro_id]);
                        else
                            $this->Branch_metros_model->add_metro($insert_post_id, $metro_id);
                    }

                    if (!empty($current_metros)) {
                        foreach ($current_metros as $current_metro => $dummy) {
                            $metros_to_delete[] = $current_metro;
                        }

                        $this->Branch_metros_model->delete_metros($insert_post_id, $metros_to_delete);
                    }
                } else {
                    $this->Branch_metros_model->drop_branch_metro($insert_post_id);
                }

                echo json_encode(['success' => true, 'msg' => 'Отделение успешно добавлено!']);
            } else
                echo json_encode(['error'=>'Не удалось добавить отделение!']);
        }
        else
            echo json_encode(['error'=>'Введите данные отделения!']);
    }

    public function deleteBranch()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['target-id']))
        {
            $delete_id = $this->Branches_model->delete($_POST['target-id']);
            echo json_encode(['success'=>true,'msg'=>'Отделение удалено!']);
        }
        else
            echo json_encode(['error'=>'Выберите отделение!']);
    }

    public function editBranch($branch_id=false)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($branch_id)
        {
            $branch=$this->Branches_model->get_by_id($branch_id);
            // print_r($branch);
            // exit();
            $data=$this->_mainMenuData(false);
            $timeFrom=['MONDAY_WORK_FROM','TUESDAY_WORK_FROM','WEDNESDAY_WORK_FROM','THURSDAY_WORK_FROM','FRIDAY_WORK_FROM','SATURDAY_WORK_FROM','SUNDAY_WORK_FROM'];
            $timeTo=['MONDAY_WORK_TO','TUESDAY_WORK_TO','WEDNESDAY_WORK_TO','THURSDAY_WORK_TO','FRIDAY_WORK_TO','SATURDAY_WORK_TO','SUNDAY_WORK_TO'];
            foreach ($timeFrom as $key => $tf)
            {
                $branch['timeFrom'][$tf]=substr($branch[$tf],0,5);
                $branch['timeTo'][$timeTo[$key]]=substr($branch[$timeTo[$key]],0,5);
            }
            $data['branch']=$branch;
            $data['metros']=$this->Metro_model->get_id_name();

            $current_metros = [];

            $temp_metros = $this->Branch_metros_model->get_branch_metro($branch_id);

            foreach ($temp_metros as $metro) {
                $data['current_metros'][(int)$metro['METRO_ID']] = 1;
            }

            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/edit_branch",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function updateBranch()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($_POST['name']))
        {
            $logo_path = NULL;

            if (!empty($_FILES['logo']) && is_uploaded_file($_FILES['logo']['tmp_name'])) {
                $name = $_FILES['logo']['name'];
                $filename_array = explode('.', $name);
                $extension = end($filename_array);
                $new_name = uniqid();
                $new_path = "/images/organisations/" . $new_name . "." . $extension;
                $new_thumb_path = "/images/organisations/" . $new_name . "_thumb." . $extension;
                $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                if (move_uploaded_file($_FILES["logo"]["tmp_name"], $path))
                    $logo_path = $new_path;
            }

            if (!empty($_POST['metro_id']) && is_array($_POST['metro_id'])) {

                $current_metros = [];

                $temp_metros = $this->Branch_metros_model->get_branch_metro($_POST['_hidden-id']);

                foreach ($temp_metros as $metro) {
                    $current_metros[(int)$metro['METRO_ID']] = 1;
                }

                foreach ($_POST['metro_id'] as $metro_id) {
                    if (!empty($current_metros[$metro_id]))
                        unset($current_metros[$metro_id]);
                    else
                        $this->Branch_metros_model->add_metro($_POST['_hidden-id'], $metro_id);
                }

                if (!empty($current_metros)) {
                    foreach ($current_metros as $current_metro => $dummy) {
                        $metros_to_delete[] = $current_metro;
                    }

                    $this->Branch_metros_model->delete_metros($_POST['_hidden-id'], $metros_to_delete);
                }
            } else {
                $this->Branch_metros_model->drop_branch_metro($_POST['_hidden-id']);
            }

            $fields = [
                'LOGO'                =>$logo_path,
                'PHONE1'              =>$_POST['phone1'],
                'PHONE2'              =>$_POST['phone2'],
                'PHONE3'              =>$_POST['phone3'],
                'LATITUDE'            =>$_POST['latitude'],
                'LONGITUDE'           =>$_POST['longitude'],
                'ADDRESS_INDEX'       =>!empty($_POST['actual_address_index']) ? $_POST['actual_address_index'] : NULL,
                'ADDRESS_REGION'      =>$_POST['actual_address_region'],
                'ADDRESS_CITY'        =>$_POST['actual_address_city'],
                'ADDRESS_STREET'      =>$_POST['actual_address_street'],
                'ADDRESS_HOUSE'       =>$_POST['actual_address_house'],
                'ADDRESS_BUILDING'    =>$_POST['actual_address_building'],
                'ADDRESS_OFFICE'      =>$_POST['actual_address_office'],
                'LEGAL_ADDRESS_INDEX'       =>!empty($_POST['legal_address_index']) ? $_POST['legal_address_index'] : NULL,
                'LEGAL_ADDRESS_REGION'      =>$_POST['legal_address_region'],
                'LEGAL_ADDRESS_CITY'        =>$_POST['legal_address_city'],
                'LEGAL_ADDRESS_STREET'      =>$_POST['legal_address_street'],
                'LEGAL_ADDRESS_HOUSE'       =>$_POST['legal_address_house'],
                'LEGAL_ADDRESS_BUILDING'    =>$_POST['legal_address_building'],
                'LEGAL_ADDRESS_OFFICE'      =>$_POST['legal_address_office'],
                'EMAIL'               =>$_POST['email'],
                'WEBSITE1'            =>$_POST['website1'],
                'WEBSITE2'            =>$_POST['website2'],
                'WEBSITE3'            =>$_POST['website3'],
                'OGRN'                    =>$_POST['ogrn'],
                'OKPO'                    =>$_POST['okpo'],
                'INN'                     =>$_POST['inn'],
                'KPP'                     =>$_POST['kpp'],
                'OKVED'                   =>$_POST['okved'],
                'OKTMO'                   =>$_POST['oktmo'],
                'ACCOUNT'                 =>$_POST['account'],
                'BANK'                    =>$_POST['bank'],
                'BIK'                     =>$_POST['bik'],
                'CHIEF_PHYSICIAN'         =>$_POST['chief_physician'],
                'CHIEF_ACCOUNTANT'        =>$_POST['chief_accountant'],
                'CEO'                     =>$_POST['ceo'],
                'MONDAY_WORK_FROM'        =>!empty($_POST['monday-work-from']) ? $_POST['monday-work-from'] : NULL,
                'MONDAY_WORK_TO'          =>!empty($_POST['monday-work-to']) ? $_POST['monday-work-to'] : NULL,
                'TUESDAY_WORK_FROM'       =>!empty($_POST['tuesday-work-from']) ? $_POST['tuesday-work-from'] : NULL,
                'TUESDAY_WORK_TO'         =>!empty($_POST['tuesday-work-to']) ? $_POST['tuesday-work-to'] : NULL,
                'WEDNESDAY_WORK_FROM'     =>!empty($_POST['wednesday-work-from']) ? $_POST['wednesday-work-from'] : NULL,
                'WEDNESDAY_WORK_TO'       =>!empty($_POST['wednesday-work-to']) ? $_POST['wednesday-work-to'] : NULL,
                'THURSDAY_WORK_FROM'      =>!empty($_POST['thursday-work-from']) ? $_POST['thursday-work-from'] : NULL,
                'THURSDAY_WORK_TO'        =>!empty($_POST['thursday-work-to']) ? $_POST['thursday-work-to'] : NULL,
                'FRIDAY_WORK_FROM'        =>!empty($_POST['friday-work-from']) ? $_POST['friday-work-from'] : NULL,
                'FRIDAY_WORK_TO'          =>!empty($_POST['friday-work-to']) ? $_POST['friday-work-to'] : NULL,
                'SATURDAY_WORK_FROM'      =>!empty($_POST['saturday-work-from']) ? $_POST['saturday-work-from'] : NULL,
                'SATURDAY_WORK_TO'        =>!empty($_POST['saturday-work-to']) ? $_POST['saturday-work-to'] : NULL,
                'SUNDAY_WORK_FROM'        =>!empty($_POST['sunday-work-from']) ? $_POST['sunday-work-from'] : NULL,
                'SUNDAY_WORK_TO'          =>!empty($_POST['sunday-work-to']) ? $_POST['sunday-work-to'] : NULL,
                'MONDAY_BREAK_FROM'       =>!empty($_POST['monday-break-from']) ? $_POST['monday-break-from'] : NULL,
                'MONDAY_BREAK_TO'         =>!empty($_POST['monday-break-to']) ? $_POST['monday-break-to'] : NULL,
                'TUESDAY_BREAK_FROM'      =>!empty($_POST['tuesday-break-from']) ? $_POST['tuesday-break-from'] : NULL,
                'TUESDAY_BREAK_TO'        =>!empty($_POST['tuesday-break-to']) ? $_POST['tuesday-break-to'] : NULL,
                'WEDNESDAY_BREAK_FROM'    =>!empty($_POST['wednesday-break-from']) ? $_POST['wednesday-break-from'] : NULL,
                'WEDNESDAY_BREAK_TO'      =>!empty($_POST['wednesday-break-to']) ? $_POST['wednesday-break-to'] : NULL,
                'THURSDAY_BREAK_FROM'     =>!empty($_POST['thursday-break-from']) ? $_POST['thursday-break-from'] : NULL,
                'THURSDAY_BREAK_TO'       =>!empty($_POST['thursday-break-to']) ? $_POST['thursday-break-to'] : NULL,
                'FRIDAY_BREAK_FROM'       =>!empty($_POST['friday-break-from']) ? $_POST['friday-break-from'] : NULL,
                'FRIDAY_BREAK_TO'         =>!empty($_POST['friday-break-to']) ? $_POST['friday-break-to'] : NULL,
                'SATURDAY_BREAK_FROM'     =>!empty($_POST['saturday-break-from']) ? $_POST['saturday-break-from'] : NULL,
                'SATURDAY_BREAK_TO'       =>!empty($_POST['saturday-break-to']) ? $_POST['saturday-break-to'] : NULL,
                'SUNDAY_BREAK_FROM'       =>!empty($_POST['sunday-break-from']) ? $_POST['sunday-break-from'] : NULL,
                'SUNDAY_BREAK_TO'         =>!empty($_POST['sunday-break-to']) ? $_POST['sunday-break-to'] : NULL,
                'NAME'                    =>$_POST['name']
            ];

            if (!empty($_POST['state']) && $_POST['state'] == "true")
                $fields['STATE'] = 1;
            else
                $fields['STATE'] = 0;
            //------------------------
            if (!empty($_POST['private']) && $_POST['private'] == "true")
                $fields['PRIVATE'] = 1;
            else
                $fields['PRIVATE'] = 0;
            //------------------------
            if (!empty($_POST['children']) && $_POST['children'] == "true")
                $fields['CHILDREN'] = 1;
            else
                $fields['CHILDREN'] = 0;
            //------------------------
            if (!empty($_POST['ambulance']) && $_POST['ambulance'] == "true")
                $fields['AMBULANCE'] = 1;
            else
                $fields['AMBULANCE'] = 0;
            //------------------------
            if (!empty($_POST['house']) && $_POST['house'] == "true")
                $fields['HOUSE'] = 1;
            else
                $fields['HOUSE'] = 0;
            //------------------------
            if (!empty($_POST['booking']) && $_POST['booking'] == "true")
                $fields['BOOKING'] = 1;
            else
                $fields['BOOKING'] = 0;
            //------------------------
            if (!empty($_POST['delivery']) && $_POST['delivery'] == "true")
                $fields['DELIVERY'] = 1;
            else
                $fields['DELIVERY'] = 0;
            //------------------------
            if (!empty($_POST['daynight']) && $_POST['daynight'] == "true")
                $fields['DAYNIGHT'] = 1;
            else
                $fields['DAYNIGHT'] = 0;
            //------------------------
            if (!empty($_POST['dms']) && $_POST['dms'] == "true")
                $fields['DMS'] = 1;
            else
                $fields['DMS'] = 0;
            //------------------------
            if (!empty($_POST['dlo']) && $_POST['dlo'] == "true")
                $fields['DLO'] = 1;
            else
                $fields['DLO'] = 0;
            //------------------------
            if (!empty($_POST['optics']) && $_POST['optics'] == "true")
                $fields['OPTICS'] = 1;
            else
                $fields['OPTICS'] = 0;
            //------------------------
            if (!empty($_POST['rpo']) && $_POST['rpo'] == "true")
                $fields['RPO'] = 1;
            else
                $fields['RPO'] = 0;
            //------------------------
            if (!empty($_POST['homeopathy']) && $_POST['homeopathy'] == "true")
                $fields['HOMEOPATHY'] = 1;
            else
                $fields['HOMEOPATHY'] = 0;
            //------------------------

            $update_id = $this->Branches_model->update($fields,$_POST['_hidden-id']);
            echo json_encode(['success'=>true,'msg'=>'Данные отделения изменены!']);
        }
        else
            echo json_encode(['error'=>'Введите данные отделения!']);
    }

    public function pay()
    {
        if(empty($this->user_id))
        {
            
              redirect('/profile/auth?back_url=/profile');
        }
           
        elseif($institution_exist = $this->db->where('USER_ID', $this->user_id)->limit(1)->get('ORGANIZATIONS')->row_array() != NULL
            or $specialist_exist = $this->Doctor_model->get_by_user_id($this->user_id) != NULL)
            {
                $data=$this->_mainMenuData(false);
                $pays=$this->Pays_model->get_all();
                $data['pays']=[];
                foreach ($pays as $key=>$pay)
                {
                    $data['pays'][$key]=$pay;
                    $data['pays'][$key]['prices']=$this->Pays_model->get_price_by_pay_id($pay['ID']);
                }
                $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
           $user=$this->User_model->get_by_id($this->user_id);
           if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
              $this->load->view("tmp/profile-templates/mainmenu",$data);
                $this->load->view("profile/pay",$data);
                $this->load->view("tmp/profile-templates/footer");
            }
            else
               {       $data=[];
               $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
                $user=$this->User_model->get_by_id($this->user_id);
                if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
                    $this->load->view("tmp/profile-templates/mainmenu",$data);
                $this->load->view("profile/pay",$data);
               $this->load->view("tmp/profile-templates/footer");
                   
               }
    }

    public function payFill()
    {
        $this->load->model('New_organisations_model');
        $institution_exist = $this->New_organisations_model->get_item(['o.USER_ID' => $this->user_id], true);
        $specialist_exist = $this->Doctor_model->get_by_user_id($this->user_id);

        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif(!empty($institution_exist))
        {
            $institution = $institution_exist;

            $contacts = [];
            if (!empty($institution['get_contacts'])) {
                $contacts = array_filter($institution['get_contacts']);
                if (!empty($contacts))
                    $contacts = array_filter($contacts['phones']);
            }

            $address = [];
            if (!empty($institution['get_addresses'])) {
                $address = array_filter($institution['get_addresses']);
                if (!empty($address))
                    $address = array_filter($address['legal']);
                    //print_r($address);
            }

            echo json_encode(['success'=>true,'name'=>$institution['ENTITY'],'inn'=>$institution['INN'],
                'kpp'=>$institution['KPP'],'phone' => (!empty($contacts) ? $contacts[0] : ""),
                'address' => (!empty($address) ? implode(" ",$address) : "")
            ]);
        }
        elseif($specialist_exist != NULL)
        {
            $specialist = $this->Doctor_model->get_by_user_id($this->user_id);
            $branch = $this->Branches_model->get_by_id($specialist['BRANCH_ID']);
            $institution = $this->Institution_model->get_by_id($branch['INSTITUTION_ID']);
            echo json_encode(['success'=>true,'name'=>$specialist['SECOND_NAME'].' '.$specialist['FIRST_NAME'].' '.$specialist['LAST_NAME'],'inn'=>$institution['INN'],
                'kpp'=>$institution['KPP'],'phone'=>$institution['PHONE'],
                'address'=>$institution['LEGAL_ADDRESS_CITY'].' '.$institution['LEGAL_ADDRESS_STREET'].' '.
                $institution['LEGAL_ADDRESS_HOUSE']]);
        }
        else
            echo json_encode(['error'=>'Вы не зарегистрированы как врач или отделение!']);
    }

    public function payAutoFill()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        else
        {
            $check_fin_id = $this->Checks_model->create(['USER_ID'=>$this->user_id]);
            echo json_encode(['success'=>true,'id'=>$check_fin_id,'date'=>date('d.m.Y')]);
        }
    }

    public function checkPayAsMSWordDocx()
    {
        require_once('Index.php');
        $fields = [
            '{NUM}'          =>$_POST['check_id_input'],
            '{DATE}'         =>$_POST['check_date_input'],
            '{NAME}'         =>$_POST['input_name_institution_pay'],
            '{INN}'          =>$_POST['input_inn_institution_pay'],
            '{KPP}'          =>$_POST['input_kpp_institution_pay'],
            '{ADDRESS}'      =>$_POST['input_address_institution_pay'],
            '{PHONE}'        =>$_POST['input_phone_institution_pay'],
            '{FAX}'          =>$_POST['input_fax_institution_pay'],
            '{SERVICE_NAME}' =>$_POST['name_cell'],
            '{MONTHS}'       =>$_POST['months_cell'],
            '{PRICE}'        =>$_POST['price_input'],
            '{PRICE_NDS}'    =>$_POST['price_input_nds'],
            '{COUNT}'        =>1
        ];
        if(Index::docxTemplate(FCPATH.'pay-template.docx',$fields,FCPATH.'pay-check.docx'))
            echo json_encode(['success'=>true]);
        else
            echo json_encode(['error'=>'Не удалось создать документ!']);
    }

    public function actions()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data=$this->_mainMenuData(false);
        $organisation_id = $this->db->where('USER_ID', $this->user_id)->limit(1)->get('ORGANIZATIONS')->row_array();

        $data['actions'] = [];

        if (!empty($organisation_id['ID'])) {
            $data['actions'] = $this->db->where('INSTITUTION_ID', $organisation_id['ID'])->get('ACTIONS')->result_array();
        }
        $user=$this->User_model->get_by_id($this->user_id);
        if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view("profile/actions",$data);
        $this->load->view("tmp/profile-templates/footer");
    }

    public function action($action_id = null)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        elseif($action_id)
        {
            $action = $this->db->query('SELECT * FROM ACTIONS WHERE ID = '.$action_id.' LIMIT 1')->row_array();
        }

        $data=$this->_mainMenuData(false);

        if (!empty($action_id) && empty($action))
            show_404();

        $data['action'] = !empty($action) ? $action : [];
        $data['metro'] = $this->db->query('SELECT * FROM METRO ORDER BY NAME')->result_array();
        $data['categories'] = $this->db->get('CATEGORY')->result_array();

        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view("profile/action",$data);
        $this->load->view("tmp/profile-templates/footer");
    }

    public function save_action()
    {
        if(!$this->user_id) {
            redirect('/profile/auth?back_url=/profile');
        }

        $user_organisation = $this->db->query('SELECT * FROM ORGANIZATIONS WHERE USER_ID = '.$this->user_id.' LIMIT 1')->row_array();

        $start_date = explode('/', $_POST['start_date']);
        $end_date = explode('/', $_POST['end_date']);

        if (!empty($_POST['name']))
            $field['NAME'] = $_POST['name'];
        if (!empty($_POST['public']) && $_POST['public'] == 'on')
            $field['PUBLIC'] = 1;
        if (!empty($_POST['start_date']))
            $field['START_DATE'] = implode('-', array_reverse($start_date));
        if (!empty($_POST['end_date']))
            $field['END_DATE'] = implode('-', array_reverse($end_date));
        if (!empty($_POST['metro_id']))
            $field['METRO_ID'] = $_POST['metro_id'];
        if (!empty($_POST['address']))
            $field['ADDRESS'] = $_POST['address'];
        if (!empty($_POST['price']))
            $field['PRICE'] = $_POST['price'];
        if (!empty($_POST['description']))
            $field['DESCRIPTION'] = $_POST['description'];
        if (!empty($_POST['text']))
            $field['TEXT'] = $_POST['text'];
        if (!empty($_POST['category_id']))
            $field['CATEGORY_ID'] = $_POST['category_id'];

        if (!empty($user_organisation['ID'])) {
            $field['INSTITUTION_ID'] = $user_organisation['ID'];

            if (empty($_POST['id'])) {
                $this->db->insert('ACTIONS', $field);
            } else {
                $this->db->where('ID', (int)$_POST['id']);
                $this->db->update('ACTIONS', $field);
            }
        }

        redirect('/profile/actions');
    }

    public function getUserMedCard($id)
    {
        $post = $this->db->where('ID', $id)->get('POSTS')->row_array();

        if (!empty($post) && $post['ATACH_MED_CARD'] == 1) {
            $destination = $this->db->where('ID_POST', $id)->get('POST_DESTINATIONS')->row_array();

            if ((!empty($post['ID_SENDER']) && $post['ID_SENDER'] == $this->user_id) || (!empty($destination['ID_DESTINATION']) && $destination['ID_DESTINATION'] == $this->user_id)) {

                $med_card = $this->db->where('USER_ID', $post['ID_SENDER'])->limit(1)->get('MED_CARDS')->row_array();


                $rows = [
                    ['Страховая медицинская организация',       (!empty($med_card['ORGANIZATION']) ? $med_card['ORGANIZATION'] : "" )],
                    ['Номер страхового полиса ОМС',             (!empty($med_card['OMS_NUMBER']) ? $med_card['OMS_NUMBER'] : "" )],
                    ['СНИЛС',                                   (!empty($med_card['SNILS']) ? $med_card['SNILS'] : "" )],
                    ['Код льготы',                              (!empty($med_card['EXEMPTION_CODE']) ? $med_card['EXEMPTION_CODE'] : "" )],
                    ['Фамилия',                                 (!empty($med_card['FAMILY']) ? $med_card['FAMILY'] : "" )],
                    ['Имя',                                     (!empty($med_card['NAME']) ? $med_card['NAME'] : "" )],
                    ['Отчество',                                (!empty($med_card['FATHER_NAME']) ? $med_card['FATHER_NAME'] : "" )],
                    ['Дата рождения',                           (!empty($med_card['BIRTH_DATE']) ? $med_card['BIRTH_DATE'] : "" )],
                    ['Адрес постоянного места жительства',      (!empty($med_card['LIFE_ADDR']) ? $med_card['LIFE_ADDR'] : "" )],
                    ['Адрес регистрации по месту пребывания',   (!empty($med_card['REG_ADDR']) ? $med_card['REG_ADDR'] : "" )],
                    ['Домашний телефон',                        (!empty($med_card['HOME_PHONE']) ? $med_card['HOME_PHONE'] : "" )],
                    ['Мобильный телефон',                       (!empty($med_card['MOBILE_PHONE']) ? $med_card['MOBILE_PHONE'] : "" )],
                    ['Документ, удостоверяющий право на льготное обеспечение (наименование, №, серия, дата, кем выдан)', (!empty($med_card['EXEMPTION_DOCUMENT']) ? $med_card['EXEMPTION_DOCUMENT'] : "" )],
                    ['Инвалидность',                            (!empty($med_card['DISABILITY']) ? $med_card['DISABILITY'] : "" )],
                    ['Характер работы',                         (!empty($med_card['WORK_CHAR']) ? $med_card['WORK_CHAR'] : "" )],

                    ['Профессия',                               (!empty($med_card['PROFESSION']) ? $med_card['PROFESSION'] : "" )],
                    ['Должность',                               (!empty($med_card['POSITION']) ? $med_card['POSITION'] : "" )],
                    ['Иждивенец',                               (!empty($med_card['DEPENDENT']) ? $med_card['DEPENDENT'] : "" )],
                    ['Группа крови',                            (!empty($med_card['BLOOD_TYPE']) ? $med_card['BLOOD_TYPE'] : "" )],
                    ['Лекарственная непереносимость',           (!empty($med_card['INTOLERANCE']) ? $med_card['INTOLERANCE'] : "" )],
                ];

                $csv = fopen($_SERVER['DOCUMENT_ROOT'].'/files/medcards/user'.$post['ID_SENDER'].'.csv', 'w');

                foreach ($rows as $row) {
                    fputcsv($csv, $row);
                }

                fclose($csv);

                if (ob_get_level()) {
                    ob_end_clean();
                }

                $file = $_SERVER['DOCUMENT_ROOT'].'/files/medcards/user'.$post['ID_SENDER'].'.csv';

                // заставляем браузер показать окно сохранения файла
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                // читаем файл и отправляем его пользователю
                readfile($file);
                exit();
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function organisation_content()
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data=$this->_mainMenuData(false);

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
           //  print_r($this->user_id);
        if (!empty($data)) {
            if (!empty($data['organisation'])) {
               //  print_r($this->user_id);
                $data['videos'] = $this->db->where('OBJECT', 1)->where('LOADER_ID', $data['organisation']['ID'])->get('VIDEOS')->result_array();
                $data['images'] = $this->db->where('OBJECT', 1)->where('LOADER_ID', $data['organisation']['ID'])->get('IMAGES')->result_array();
                $data['articles'] = $this->db->where('OBJECT', 1)->where('LOADER_ID', $data['organisation']['ID'])->get('ARTICLES')->result_array();
            } elseif (!empty($data['specialist'])) {
               // print_r($this->user_id);
                $data['videos'] = $this->db->where('OBJECT', 3)->where('LOADER_ID', $data['specialist']['ID'])->get('VIDEOS')->result_array();
                $data['images'] = $this->db->where('OBJECT', 3)->where('LOADER_ID', $data['specialist']['ID'])->get('IMAGES')->result_array();
                $data['articles'] = $this->db->where('OBJECT', 3)->where('LOADER_ID', $data['specialist']['ID'])->get('ARTICLES')->result_array();
            }

        } else {
            show_404();
        }
        $user=$this->User_model->get_by_id($this->user_id);
       if($user['TYPE']==2){
                
                $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread4($this->user_id);
            }
            elseif($user['TYPE']==3){
              
              
              $data['iduser']=$this->Orgnisations-> get_by_organisation_user_id($this->user_id);
            if(!empty($data['iduser'])){
               
              $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread3($this->user_id,$data['iduser'][0]['ID']);  
            }
            }  
        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view('profile/content', $data);
        /*грузим футер*/
        $this->load->view("tmp/profile-templates/footer");
    }

    public function add_image($id = null)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data=$this->_mainMenuData(false);

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        $data['image'] = [];
        $image = $this->db->limit(1)->where('ID', $id)->where('OBJECT', 1)->where('LOADER_ID', !empty($organisation_id) ? $organisation_id : $specialist_id )->get('IMAGES')->row_array();
        if (!empty($image))
            $data['image'] = $image;

        $data['cur_object'] = !empty($organisation_id) ? 1 : 3;

        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view('profile/add_photo');
        $this->load->view("tmp/profile-templates/footer");
    }

    public function save_image($id = null)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');
        $check = false;

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        if (!empty($_FILES['image']['tmp_name'])) {
            $config['upload_path']      = $_SERVER['DOCUMENT_ROOT'].'/images/licenses/';
            $config['allowed_types']    = 'gif|jpg|png';
            $config['max_size']         = 4000;
            $config['max_width']        = 15000;
            $config['max_height']       = 15000;
            $config['file_name']        = uniqid();

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('image')) {
                $data = $this->upload->data();
                $fields['VALUE'] = $config['file_name'].$data['file_ext'];
                $fields['OBJECT'] = !empty($organisation_id) ? 1 : 3;
                $fields['LOADER_ID'] = !empty($organisation_id) ? $organisation_id : $specialist_id;;
                $fields['PUBLIC'] = (!empty($_POST['public']) && ($_POST['public'] == 'on')) ? 1 : 0;
                if (!empty($_POST['object']) && $_POST['object'] == 3 && !empty($data['specialist']))
                    $fields['OBJECT'] = 3;

                $check = true;

                $config['image_library'] = 'gd2';
                $config['source_image'] = './images/licenses/'.$fields['VALUE'];
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width']         = 90;
                $config['height']       = 60;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();

                $this->db->insert('IMAGES', $fields);
            }
        }

        redirect('/profile/organisation_content');
    }

    public function delete_image($id)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        $this->db->where('OBJECT', (!empty($organisation_id) ? 1 : 3))->where('LOADER_ID', (!empty($organisation_id) ? $organisation_id : $specialist_id))->where('ID', $id)->delete('IMAGES');
        redirect('/profile/organisation_content');
    }

    public function add_video($id = null)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data=$this->_mainMenuData(false);

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        $data['video'] = [];
        $image = $this->db->limit(1)->where('ID', $id)->where('OBJECT', !empty($organisation_id) ? 1 : 3)->where('LOADER_ID', !empty($organisation_id) ? $organisation_id : $specialist_id)->get('VIDEOS')->row_array();
        if (!empty($image))
            $data['video'] = $image;

        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view('profile/add_video');
        $this->load->view("tmp/profile-templates/footer");
    }

    public function save_video($id = null)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
           
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }
            //print_r($_POST['video']);
            //exit();
        if (filter_var($_POST['video'], FILTER_VALIDATE_URL)) {
            $url = $_POST['video'];
            
            $result = parse_url($url);
            
            
            parse_str($result['query'], $get_params);
              
            if (!empty($result['path'])) {
                
                $fields = [
                    'OBJECT'        => !empty($organisation_id) ? 1 : 3,
                    'LOADER_ID'     => !empty($organisation_id) ? $organisation_id : $specialist_id,
                    'VALUE'         => "https://www.youtube.com/".$result['path'],
                    'PUBLIC'        => (!empty($_POST['public']) && ($_POST['public'] == 'on')) ? 1 : 0
                ];

                $this->db->insert('VIDEOS', $fields);
            }
        }

        redirect('/profile/organisation_content');
    }

    public function delete_video($id)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        $this->db->where('OBJECT', !empty($organisation_id) ? 1 : 3)->where('LOADER_ID', !empty($organisation_id) ? $organisation_id : $specialist_id)->where('ID', $id)->delete('VIDEOS');
        redirect('/profile/organisation_content');
    }

    public function add_publication($id = null)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data=$this->_mainMenuData(false);

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        $data['publication'] = [];
        $publication = $this->db->limit(1)->where('ID', $id)->where('OBJECT', !empty($organisation_id) ? 1 : 3)->where('LOADER_ID', !empty($organisation_id) ? $organisation_id : $specialist_id)->get('ARTICLES')->row_array();
        if (!empty($publication))
            $data['publication'] = $publication;

        $this->load->view("tmp/profile-templates/mainmenu",$data);
        $this->load->view('profile/add_publication');
        $this->load->view("tmp/profile-templates/footer");
    }

    public function save_publication($id = null)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        if (!empty($_POST['name'])) {
            $fields = [
                'OBJECT'        => !empty($organisation_id) ? 1 : 3,
                'NAME'          => $_POST['name'],
                'TEXT'          => $_POST['text'],
                'LOADER_ID'     => !empty($organisation_id) ? $organisation_id : $specialist_id,
                'PUBLIC'        => (!empty($_POST['public']) && ($_POST['public'] == 'on')) ? 1 : 0
            ];

            $this->db->insert('ARTICLES', $fields);
        }

        redirect('/profile/organisation_content');
    }

    public function delete_publication($id)
    {
        if(!$this->user_id)
            redirect('/profile/auth?back_url=/profile');

        $data['organisation'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('ORGANIZATIONS')->row_array();
        $data['specialist'] = $this->db->where('USER_ID', $this->user_id)->limit(1)->order_by('ID')->get('SPECIALISTS')->row_array();
        if (!empty($data)) {
            $organisation_id = $data['organisation']['ID'];
            $specialist_id = $data['specialist']['ID'];
        } else {
            show_404();
        }

        $this->db->where('OBJECT', !empty($organisation_id) ? 1 : 3)->where('LOADER_ID', !empty($organisation_id) ? $organisation_id : $specialist_id)->where('ID', $id)->delete('ARTICLES');
        redirect('/profile/organisation_content');
    }
}

