<?php

/**
 * Created by PhpStorm.
 * User: kulakov
 * Date: 21.11.16
 * Time: 18:01
 */
class Organisations extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->data['controller']   = $this->router->fetch_class();
        $this->data['method']       = $this->router->fetch_method();

        $this->user_id = $this->session->userdata('USER_ID');

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Carousel_model');
        $this->load->model('Action_model');
        $this->load->model('Banner_model');
        $this->load->model('News_model');
        $this->load->model('Institution_model');
        $this->load->model('New_organisations_model');
        $this->load->model('Branches_model');
        $this->load->model('Specialist_model');
        $this->load->model('Articles_model');
        $this->load->model('Geo_model');
        $this->load->model('Logos_model');
        $this->load->model('Post_model');
        $this->load->library('pagination');
        $this->load->model('Voting_model');

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $this->data['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $this->data['current_city'] = $cookie_city_id;
        } else {
            $this->data['current_city'] = 4400;
        }

        //Получаем две случайные акции для блока на главной
        $actions['actions'] = $this->Action_model->get_random_list(2, 2);

        foreach ($actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $actions['actions'][$key]['DESCRIPTION'] = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $actions['actions'][$key]['PRICE'] = number_format((float)$row['PRICE'], 2, ".", " ");
        }
          // print_r($actions);
        // Получаем акции для вывода на карту
        $map_actions['actions'] = $this->Action_model->get_random_map(4, 2);
            //  print_r($map_actions);
        foreach ($map_actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $map_actions['actions'][$key]['DESCRIPTION']    = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $map_actions['actions'][$key]['PRICE']              = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        $carousel = $this->Carousel_model->get_images(1);
        $actions_quantity        = $this->Action_model->count();
        $actions['quantity']     = $actions_quantity;
        $map_actions['quantity'] = $actions_quantity;
        $map_actions['green'] = false;
        $popular_institutions = $this->Institution_model->get_popular();
        $first_banners  = $this->Banner_model->get_banners(1);
        $second_banners = $this->Banner_model->get_banners(1);
        $news = $this->News_model->get_last();
        $map_actions['types'] = $this->Institution_model->get_types();

        // выбираем три рандомные категории
        $popular_themes_category = $this->Articles_model->get_random_themes();

        $popular_themes_category_ids = null;

        foreach ($popular_themes_category as $item) {
            $popular_themes_category_ids[] = $item['ID'];
        }

        $popular_themes = $this->Articles_model->get_articles_by_ids_array($popular_themes_category_ids);

        foreach ($popular_themes as $popular_theme) {
            $popular_themes_data['themes'][$popular_theme['CATEGORY_ID']][] = [
                'ID'        => $popular_theme['ID'],
                'NAME'      => $popular_theme['NAME']
            ];
        }

        $popular_themes_data['category'] = $popular_themes_category;

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);
        $cities['current_city'] = $this->data['current_city'];

        $logos = $this->Logos_model->get_logos();

        $voting = $this->Voting_model->get_vote();
        $voting['is_vote'] = $this->Voting_model->is_vote($voting['info']['ID'], $this->user_id);

        $this->data['templates'] = [
            'city_selector'     => $this->load->view('tmp/city_selector', $cities, true),
            'auth'              => ($this->user_id)
                ? $this->load->view('tmp/auth_entered', [
                    'data' => $this->User_model->get_by_id($this->user_id),
                    'msg_count' => $this->Post_model->get_by_user_id_in_count_unread($this->user_id)[0]['COUNT(*)']
                ], true)
                : $this->load->view('tmp/auth', null, true),
            'carousel'          => $this->load->view('tmp/carousel', ['images' => $carousel], true),
            'menu'              => $this->load->view('tmp/menu', ['controller' => $this->data['controller']], true),
            'search'            => $this->load->view('tmp/search', null, true),
            'ask_question'      => $this->load->view('tmp/ask_question', ['is_auth' => (!empty($this->user_id)) ? true : false], true),
//            'actions'           => $this->load->view('tmp/actions', null, true),
            'logos'             => $this->load->view('tmp/logos', ['logos' => $logos], true),
            'map_search'        => $this->load->view('tmp/map_search', $map_actions, true),
            'popular_institutions'  => $this->load->view('tmp/popular_institutions', ['institutions' => $popular_institutions], true),
            'first_banners'     => $this->load->view('tmp/banners', ['banners' => $first_banners], true),
            'second_banners'    => $this->load->view('tmp/banners', ['banners' => $second_banners], true),
            'popular_themes'    => $this->load->view('tmp/popular_themes', $popular_themes_data, true),
            'voiting'           => $this->load->view('tmp/voiting', $voting, true),
            'news'              => $this->load->view('tmp/news', ['news' => $news], true),
            'bottom_menu'       => $this->load->view('tmp/bottom_menu', null, true),
            'footer'            => $this->load->view('tmp/footer', ['user' => $this->user_id], true)
        ];
       // print_r($map_actions);
    }

    public function catalog()
    {    
        $per_page               = 25;
        $pages                  = 0;
        $this->data['category'] = 'Все организации';
        $this->data['type']     = 'Все типы';
        $for_the                = " организаций";
        $category_id            = null;
        $type_id                = null;
        $filter                 = [];

        if (!empty($_GET['per_page']))
            $per_page = (int)$_GET['per_page'];
        if (!empty($_GET['pages']))
            $pages = (((int)$_GET['pages'])-1)*$per_page;

        if (!empty($_GET['category_id']))
            $filter['o.CATEGORY_ID'] = (int)$_GET['category_id'];
        if (!empty($_GET['type_id']))
            $filter['o.TYPE_ID'] = (int)$_GET['type_id'];
        if (!empty($_GET['spec_id']))
            $filter['o.SPECIALIZATION_ID'] = (int)$_GET['spec_id'];

        /*
         * Если пользователь выбрал метро - для начала выбираем все идентификаторы организаций попавших под фильтр
         */

        $organisations_ids = [];

        if (!empty($_GET['m'])) {
            foreach ($_GET['m'] as $metro_id) {
                $metro_ids[] = (int)$metro_id;
            }

            $organisations_filtred_by_metro = $this->db->select('ORGANIZATION_ID')->group_by('ORGANIZATION_ID')->where_in('METRO_ID', $metro_ids)->get('ORGANIZATION_METRO')->result_array();

            if (!empty($organisations_filtred_by_metro)) {
                foreach ($organisations_filtred_by_metro as $organisations_filtred_by_metro_id) {
                    $organisations_ids[] = $organisations_filtred_by_metro_id['ORGANIZATION_ID'];
                }
            } else {
                $filter['o.ID'] = 0;
            }
        }

        if (!empty($_GET['state']))
            $filter['o_evidance.STATE'] = (!empty($_GET['state']) && $_GET['state'] == 'true') ? 1 : 0;
        if (!empty($_GET['private']))
            $filter['o_evidance.PRIVATE'] = (!empty($_GET['private']) && $_GET['private'] == 'true') ? 1 : 0;
        if (!empty($_GET['children']))
            $filter['o_evidance.CHILDREN'] = (!empty($_GET['children']) && $_GET['children'] == 'true') ? 1 : 0;
        if (!empty($_GET['ambulance']))
            $filter['o_evidance.AMBULANCE'] = (!empty($_GET['ambulance']) && $_GET['ambulance'] == 'true') ? 1 : 0;
        if (!empty($_GET['house']))
            $filter['o_evidance.HOUSE'] = (!empty($_GET['house']) && $_GET['house'] == 'true') ? 1 : 0;
        if (!empty($_GET['daynight']))
            $filter['o_evidance.DAYNIGHT'] = (!empty($_GET['daynight']) && $_GET['daynight'] == 'true') ? 1 : 0;
        if (!empty($_GET['booking']))
            $filter['o_evidance.BOOKING'] = (!empty($_GET['booking']) && $_GET['booking'] == 'true') ? 1 : 0;
        if (!empty($_GET['delivery']))
            $filter['o_evidance.DELIVERY'] = (!empty($_GET['delivery']) && $_GET['delivery'] == 'true') ? 1 : 0;
        if (!empty($_GET['dms']))
            $filter['o_evidance.DMS'] = (!empty($_GET['dms']) && $_GET['dms'] == 'true') ? 1 : 0;
        if (!empty($_GET['dlo']))
            $filter['o_evidance.DLO'] = (!empty($_GET['dlo']) && $_GET['dlo'] == 'true') ? 1 : 0;
        if (!empty($_GET['optics']))
            $filter['o_evidance.OPTICS'] = (!empty($_GET['optics']) && $_GET['optics'] == 'true') ? 1 : 0;
        if (!empty($_GET['rpo']))
            $filter['o_evidance.RPO'] = (!empty($_GET['rpo']) && $_GET['rpo'] == 'true') ? 1 : 0;
        if (!empty($_GET['homeopathy']))
            $filter['o_evidance.HOMEOPATHY'] = (!empty($_GET['homeopathy']) && $_GET['homeopathy'] == 'true') ? 1 : 0;


        if (!empty($_GET['category_id'])) {
            $category_id = $this->data['category_id'] = (int)$_GET['category_id'];
            $category_name = $this->db->query("SELECT ID, NAME FROM CATEGORY WHERE ID = $category_id")->row_array();
            if (!empty($category_name)) {
                $this->data['category'] = $category_name['NAME'];
                $this->data['category_id'] = $category_name['ID'];
            }
        }
        if (!empty($_GET['type_id'])) {
            $type_id = $this->data['type_id'] = (int)$_GET['type_id'];
            $type_name = $this->db->query("SELECT ID,NAME FROM INSTITUTION_TYPES WHERE ID = $type_id")->row_array();
            if (!empty($type_name)) {
                $this->data['type'] = $type_name["NAME"];
                $this->data['type_id'] = $type_name["ID"];
            }
        }

        $this->data['organisations'] = $this->New_organisations_model->get_list($filter, [], $per_page, $pages, false, $organisations_ids);
        $this->data['organisation_count'] = (int)$this->New_organisations_model->get_count($filter, []);

        $this->data['get_replacer'] = function ($params) {
            parse_str($_SERVER['QUERY_STRING'], $get_params);

            foreach ($params as $param => $value) {
                if (!empty($value)) {
                    $get_params[$param] = $value;
                } else {
                    if (isset($get_params[$param]))
                        unset($get_params[$param]);
                }
            }
            return ((!empty($get_params)) ? "?" : "" ).http_build_query($get_params);
        };

//        $data['url_with_params'] = $_SERVER['PATH_INFO']."?";
        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $this->data['organisation_count'];
        $config['per_page'] = $per_page;
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['next_link'] = FALSE;
        $config['prev_link'] = FALSE;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<span class="pages">Страница';
        $config['full_tag_close'] = '</span>';
//        $config['num_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
//        $config['num_tag_close'] = '</span>';
//        $config['prev_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
//        $config['prev_tag_close'] = '</span>';
        $config['cur_tag_open'] = '<a href="javascript:void(0)" class="active">';
        $config['cur_tag_close'] = '</a>';
//        $config['last_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
//        $config['last_tag_close'] = '</span>';
//        $config['next_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
//        $config['next_tag_close'] = '</span>';
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();

        $this->data['doctor_types']    = $this->db->query('SELECT * FROM DOCTOR_TYPES ORDER BY NAME')->result_array();
        $this->data['specializations'] = $this->db->query('SELECT * FROM SPECIALIZATIONS ORDER BY NAME')->result_array();

        $temp_metros = $this->db->query('SELECT `ID`,`NAME`,`CLASS`,`TOP`,`LEFT`,`BRANCH` FROM METRO')->result_array();

        $this->data['metro'] = [];

        foreach ($temp_metros as $temp_metro) {
            $this->data['metro'][$temp_metro['BRANCH']][] = $temp_metro;
        }

        $this->load->view('main/organisations_catalog', $this->data);
    }

    public function item($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['id'] = $id;

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        // Получить отделения
        $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $id]);

        if (empty($this->data['item']))
            show_404();

        $this->data['branches']     = $this->New_organisations_model->get_branches($id);
        $this->data['specialists']  = [];
        $this->data['articles']     = $this->db
            ->where('OBJECT', 1)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('ARTICLES')
            ->result_array();
        $this->data['photos']       = $this->db
            ->where('OBJECT', 1)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('IMAGES')
            ->result_array();
        $this->data['videos']       = $this->db
            ->where('OBJECT', 1)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('VIDEOS')
            ->result_array();
        $this->data['feedbacks']    = $this->db->where('OBJECT', 1)
            ->select('FEEDBACKS.*')
            ->select('USERS.FIRSTNAME')
            ->select('USERS.LASTNAME')
            ->select('USERS.MIDDLENAME')
            ->where('LOADER_ID', $id)
            ->join('USERS','USERS.ID = FEEDBACKS.USER_ID')
            ->limit(3)
            ->get('FEEDBACKS')
            ->result_array();



        if (!empty($this->data['videos'])) {
            $this->data['first_video'] = $this->data['videos'][0];
            unset($this->data['videos'][0]);
        } else {
            $this->data['first_video'] = [];
        }

        $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);

        $this->load->view('main/organisation', $this->data);
    }

    public function branches($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['id'] = $id;

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        // Получить отделения
        $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $id]);

        $this->data['branches']     = $this->New_organisations_model->get_branches($id);
        $this->data['specialists']  = [];
        $this->data['articles']     = [];
        $this->data['photos']       = [];
        $this->data['videos']       = [];
        $this->data['feedbacks']    = [];

        $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);

        $this->load->view('main/organisation_branches', $this->data);
    }

    public function specialists($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['id'] = $id;

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        // Получить отделения
        $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $id]);

        $this->data['branches']     = $this->New_organisations_model->get_branches($id);
        $this->data['specialists']  = [];
        $this->data['articles']     = [];
        $this->data['photos']       = [];
        $this->data['videos']       = [];
        $this->data['feedbacks']    = [];

        $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);

        $this->load->view('main/organisation_specialists', $this->data);
    }

    public function articles($id, $concrete = null)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['id'] = $id;

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        // Получить отделения
        $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $id]);

        $this->data['branches']     = $this->New_organisations_model->get_branches($id);
        $this->data['specialists']  = [];
        $this->data['articles']     = $this->db
            ->where('OBJECT', 1)
            ->where('LOADER_ID', $id)
            ->get('ARTICLES')
            ->result_array();
        $this->data['photos']       = [];
        $this->data['videos']       = [];
        $this->data['feedbacks']    = [];

        $data['concrete_article'] = [];

        $concrete_arctilce = $this->db
            ->where('OBJECT',1)
            ->where('LOADER_ID', $id)
            ->where('ID', $concrete)
            ->limit(1)
            ->get('ARTICLES')
            ->row_array();

        if (!empty($concrete_arctilce))
            $this->data['concrete_article'] = $concrete_arctilce;

        $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);

        $this->load->view('main/organisation_articles', $this->data);
    }

    public function photos($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['id'] = $id;

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        // Получить отделения
        $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $id]);

        $this->data['branches']     = $this->New_organisations_model->get_branches($id);
        $this->data['specialists']  = [];
        $this->data['articles']     = [];
        $this->data['photos']       = $this->db
            ->where('OBJECT', 1)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('IMAGES')
            ->result_array();
        $this->data['videos']       = [];
        $this->data['feedbacks']    = [];

        $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);

        $this->load->view('main/organisation_photos', $this->data);
    }

    public function videos($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['id'] = $id;

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        // Получить отделения
        $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $id]);

        $this->data['branches']     = $this->New_organisations_model->get_branches($id);
        $this->data['specialists']  = [];
        $this->data['articles']     = [];
        $this->data['photos']       = [];
        $this->data['videos']       = $this->db
            ->where('OBJECT', 1)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('VIDEOS')
            ->result_array();
        $this->data['feedbacks']    = [];
         // print_r($this->data['videos']);
          //exit();
        $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);

        $this->load->view('main/organisation_videos', $this->data);
    }

    public function feedbacks($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['id'] = $id;

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        // Получить отделения
        $this->data['item'] = $this->New_organisations_model->get_item(['o.ID' => $id]);

        $this->data['branches']     = $this->New_organisations_model->get_branches($id);
        $this->data['specialists']  = [];
        $this->data['articles']     = [];
        $this->data['photos']       = [];
        $this->data['videos']       = [];
        $this->data['feedbacks']    = $this->db->where('OBJECT', 1)
            ->select('FEEDBACKS.*')
            ->select('USERS.FIRSTNAME')
            ->select('USERS.LASTNAME')
            ->select('USERS.MIDDLENAME')
            ->where('LOADER_ID', $id)
            ->join('USERS','USERS.ID = FEEDBACKS.USER_ID')
            ->join('POSTS','POSTS.IDFEEDBACK = FEEDBACKS.ID')
            ->get('FEEDBACKS')
            ->result_array();
            
        $this->data['organisation_card'] = $this->load->view('tmp/organisation_card', $this->data, true);

        $this->load->view('main/organisation_feedbacks', $this->data);
    }
}