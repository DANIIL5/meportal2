<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->data['controller']   = $this->router->fetch_class();
        $this->data['method']       = $this->router->fetch_method();

        $this->user_id = $this->session->userdata('USER_ID');

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Carousel_model');
        $this->load->model('Action_model');
        $this->load->model('Banner_model');
        $this->load->model('News_model');
        $this->load->model('Institution_model');
        $this->load->model('Doctor_model');
        $this->load->model('Specialist_model');
        $this->load->model('Articles_model');
        $this->load->model('Geo_model');
        $this->load->model('Logos_model');
        $this->load->model('Post_model');
        $this->load->model('Voting_model');

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $this->data['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $this->data['current_city'] = $cookie_city_id;
        } else {
            $this->data['current_city'] = 4400;
        }

        //Получаем две случайные акции для блока на главной
        $actions['actions'] = $this->Action_model->get_random_list(2, 3);

        foreach ($actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $actions['actions'][$key]['DESCRIPTION'] = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $actions['actions'][$key]['PRICE'] = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        // Получаем акции для вывода на карту
        $map_actions['actions'] = $this->Action_model->get_random_map(4, 3);

        foreach ($map_actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $map_actions['actions'][$key]['DESCRIPTION']    = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $map_actions['actions'][$key]['PRICE']              = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        $carousel = $this->Carousel_model->get_images(1);
        $actions_quantity        = $this->Action_model->count();
        $actions['quantity']     = $actions_quantity;
        $map_actions['quantity'] = $actions_quantity;
        $map_actions['green'] = false;
        $popular_institutions = $this->Institution_model->get_popular();
        $popular_doctors = $this->Doctor_model->get_popular();
        $first_banners  = $this->Banner_model->get_banners(1);
        $second_banners = $this->Banner_model->get_banners(1);
        $news = $this->News_model->get_last();
        $map_actions['types'] = $this->Institution_model->get_types();

        // выбираем три рандомные категории
        $popular_themes_category = $this->Articles_model->get_random_themes();

        $popular_themes_category_ids = null;

        foreach ($popular_themes_category as $item) {
            $popular_themes_category_ids[] = $item['ID'];
        }

        $popular_themes = $this->Articles_model->get_articles_by_ids_array($popular_themes_category_ids);

        foreach ($popular_themes as $popular_theme) {
            $popular_themes_data['themes'][$popular_theme['CATEGORY_ID']][] = [
                'ID'        => $popular_theme['ID'],
                'NAME'      => $popular_theme['NAME']
            ];
        }

        $popular_themes_data['category'] = $popular_themes_category;

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);
        $cities['current_city'] = $this->data['current_city'];

        $logos = $this->Logos_model->get_logos();

        $voting = $this->Voting_model->get_vote();
        $voting['is_vote'] = $this->Voting_model->is_vote($voting['info']['ID'], $this->user_id);

        $this->data['templates'] = [
            'city_selector'     => $this->load->view('tmp/city_selector', $cities, true),
            'auth'              => ($this->user_id)
                ? $this->load->view('tmp/auth_entered', [
                    'data' => $this->User_model->get_by_id($this->user_id),
                    'msg_count' => $this->Post_model->get_by_user_id_in_count_unread($this->user_id)[0]['COUNT(*)']
                ], true)
                : $this->load->view('tmp/auth', null, true),
            'carousel'          => $this->load->view('tmp/carousel', ['images' => $carousel], true),
            'menu'              => $this->load->view('tmp/menu', ['controller' => $this->data['controller']], true),
            'search'            => $this->load->view('tmp/search', null, true),
            'ask_question'      => $this->load->view('tmp/ask_question', ['is_auth' => (!empty($this->user_id)) ? true : false], true),
//            'actions'           => $this->load->view('tmp/actions', null, true),
            'logos'             => $this->load->view('tmp/logos', ['logos' => $logos], true),
            'map_search'        => $this->load->view('tmp/map_search', $map_actions, true),
            'popular_institutions'  => $this->load->view('tmp/popular_institutions', ['institutions' => $popular_institutions], true),
            'popular_doctors'   => $this->load->view('tmp/popular_doctors', ['doctors' => $popular_doctors], true),
            'first_banners'     => $this->load->view('tmp/banners', ['banners' => $first_banners], true),
            'second_banners'    => $this->load->view('tmp/banners', ['banners' => $second_banners], true),
            'popular_themes'    => $this->load->view('tmp/popular_themes', $popular_themes_data, true),
            'voiting'           => $this->load->view('tmp/voiting', $voting, true),
            'news'              => $this->load->view('tmp/news', ['news' => $news], true),
            'bottom_menu'       => $this->load->view('tmp/bottom_menu', null, true),
            'footer'            => $this->load->view('tmp/footer', ['user' => $this->user_id], true)
        ];

        $this->data['isAuth'] = ($this->user_id) ? true : false;
    }

    public function index()
    {
        $this->data['specialisations'] = $this->db->order_by('NAME')->get('DOCTOR_TYPES')->result_array();

        $this->load->view('main/doctors', $this->data);
    }

    public function catalog()
    {
        $per_page = 10;
        $pages = 0;
        $filters = [];

        if (!empty($_GET['pages']))
            $pages = (int)$_GET['pages'];

        if (!empty($_GET['doctor_types']))
            $filters[] = "(SPECIALISTS.SPECIALIZATION_ID = '".$_GET['doctor_types']."')";

        $this->data['doctors'] = $this->Doctor_model->get_catalog($per_page, $pages, $filters);
          //print_r($this->data['doctors']);
        $this->data['doctors_copy'] = $this->data['doctors'];
        $catalog_quantity = $this->Doctor_model->get_catalog_quantity();

        $this->load->library('pagination');

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = 'Следующая';
        $config['prev_link'] = 'Предыдущая';
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<p style="margin-top: 10px;">';
        $config['full_tag_close'] = '</p>';
        $config['num_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['num_tag_close'] = '</span>';
        $config['prev_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['prev_tag_close'] = '</span>';
        $config['cur_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['cur_tag_close'] = '</span>';
        $config['last_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['last_tag_close'] = '</span>';
        $config['next_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['next_tag_close'] = '</span>';

        $this->pagination->initialize($config);

        $this->data['pagination'] = $this->pagination->create_links();
        
        $temp_metros = $this->db->query('SELECT `ID`,`NAME`,`CLASS`,`TOP`,`LEFT`,`BRANCH` FROM METRO')->result_array();

        $this->data['metro'] = [];

        // Запись
        $this->data['user_id'] = $this->user_id;
        $this->data['user_data'] = $this->User_model->get_by_id($this->user_id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        $this->data['tommorow_foramted_date'] = date('d-m-Y', time() + 3600 * 24);

        $this->data['times'] = range(strtotime("7:00"),strtotime("18:00"), 60*15);
        // конец Записи

        foreach ($temp_metros as $temp_metro) {
            $this->data['metro'][$temp_metro['BRANCH']][] = $temp_metro;
        }

        $this->data['metros'] = [];

        $this->data['doctor_types']    = $this->db->query('SELECT * FROM DOCTOR_TYPES ORDER BY NAME')->result_array();

        foreach ($this->data['doctors'] as $key => $doctor) {
            $this->data['doctors'][$key]['FULL_NAME'] = $doctor['SECOND_NAME'];
            if (!empty($doctor['FIRST_NAME']))
                $this->data['doctors'][$key]['FULL_NAME'] .= mb_substr(" ".$doctor['FIRST_NAME'], 0, 2, 'utf-8').".";
            if (!empty($doctor['SECOND_NAME']))
                $this->data['doctors'][$key]['FULL_NAME'] .= mb_substr(" ".$doctor['LAST_NAME'], 0, 2, 'utf-8').".";
        }

        $this->load->view('main/doctors_catalog', $this->data);
    }

   public function videos($id = 0)
    {
        $id = (int)$id;

        $this->data['id'] = $id;
        $this->data['work'] = [];
        $this->data['metro'] = [];
        $this->load->model('Branch_metros_model');
        
        $this->data['item'] = $this->Specialist_model->get_by_id($id);

//                var_dump($this->data['item']);exit();

        $this->data['work'] = [];
        if (!empty($this->data['item']['BRANCH_ID']))
            $this->data['work'] = $this->db->query('
                SELECT
                *
                FROM BRANCHES
                WHERE ID = '.$this->data['item']['BRANCH_ID'].'
                LIMIT 1')->row_array();

        $this->data['metros'] = [];
        if (!empty($this->data['work']['ID'])) {
            $this->data['metros'] = $this->db->query('
                SELECT
                METRO.NAME
                FROM BRANCH_METROS
                LEFT JOIN METRO ON METRO.ID = BRANCH_METROS.METRO_ID
                WHERE BRANCH_METROS.BRANCH_ID = '.$this->data['work']['ID'])->result_array();
        }

        if (empty($this->data['item']))
            show_404();

        $this->data['user_id'] = $this->user_id;
        $this->data['user_data'] = $this->User_model->get_by_id($this->user_id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        $this->data['tommorow_foramted_date'] = date('d-m-Y', time() + 3600 * 24);

        $this->data['item']['MONDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['MONDAY_WORK_FROM']));
        $this->data['item']['TUESDAY_WORK_FROM']      = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_FROM']));
        $this->data['item']['WEDNESDAY_WORK_FROM']    = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_FROM']));
        $this->data['item']['THURSDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_FROM']));
        $this->data['item']['FRIDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_FROM']));
        $this->data['item']['SATURDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_FROM']));
        $this->data['item']['SUNDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_FROM']));

        $this->data['item']['MONDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['MONDAY_WORK_TO']));
        $this->data['item']['TUESDAY_WORK_TO']        = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_TO']));
        $this->data['item']['WEDNESDAY_WORK_TO']      = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_TO']));
        $this->data['item']['THURSDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_TO']));
        $this->data['item']['FRIDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_TO']));
        $this->data['item']['SATURDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_TO']));
        $this->data['item']['SUNDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_TO']));

        $this->data['times'] = range(strtotime("7:00"),strtotime("18:00"), 60*15);

       $this->data['videos']       = $this->db
            ->where('OBJECT', 3)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('VIDEOS')
            ->result_array();
          
        //$this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();

        $this->load->view('main/doctor_video', $this->data);
    }
    
    public function photos($id = 0)
    {
        $id = (int)$id;

        $this->data['id'] = $id;
        $this->data['work'] = [];
        $this->data['metro'] = [];
        $this->load->model('Branch_metros_model');
        
        $this->data['item'] = $this->Specialist_model->get_by_id($id);

//                var_dump($this->data['item']);exit();

        $this->data['work'] = [];
        if (!empty($this->data['item']['BRANCH_ID']))
            $this->data['work'] = $this->db->query('
                SELECT
                *
                FROM BRANCHES
                WHERE ID = '.$this->data['item']['BRANCH_ID'].'
                LIMIT 1')->row_array();

        $this->data['metros'] = [];
        if (!empty($this->data['work']['ID'])) {
            $this->data['metros'] = $this->db->query('
                SELECT
                METRO.NAME
                FROM BRANCH_METROS
                LEFT JOIN METRO ON METRO.ID = BRANCH_METROS.METRO_ID
                WHERE BRANCH_METROS.BRANCH_ID = '.$this->data['work']['ID'])->result_array();
        }

        if (empty($this->data['item']))
            show_404();

        $this->data['user_id'] = $this->user_id;
        $this->data['user_data'] = $this->User_model->get_by_id($this->user_id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        $this->data['tommorow_foramted_date'] = date('d-m-Y', time() + 3600 * 24);

        $this->data['item']['MONDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['MONDAY_WORK_FROM']));
        $this->data['item']['TUESDAY_WORK_FROM']      = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_FROM']));
        $this->data['item']['WEDNESDAY_WORK_FROM']    = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_FROM']));
        $this->data['item']['THURSDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_FROM']));
        $this->data['item']['FRIDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_FROM']));
        $this->data['item']['SATURDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_FROM']));
        $this->data['item']['SUNDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_FROM']));

        $this->data['item']['MONDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['MONDAY_WORK_TO']));
        $this->data['item']['TUESDAY_WORK_TO']        = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_TO']));
        $this->data['item']['WEDNESDAY_WORK_TO']      = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_TO']));
        $this->data['item']['THURSDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_TO']));
        $this->data['item']['FRIDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_TO']));
        $this->data['item']['SATURDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_TO']));
        $this->data['item']['SUNDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_TO']));

        $this->data['times'] = range(strtotime("7:00"),strtotime("18:00"), 60*15);

       $this->data['photos']       = $this->db
            ->where('OBJECT', 3)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('IMAGES')
            ->result_array();
          
        //$this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();

        $this->load->view('main/doctor_photo', $this->data);
    }
    
    
    
    
    public function publications($id = 0)
    {
        $id = (int)$id;

        $this->data['id'] = $id;
        $this->data['work'] = [];
        $this->data['metro'] = [];
        $this->load->model('Branch_metros_model');
        
        $this->data['item'] = $this->Specialist_model->get_by_id($id);

//                var_dump($this->data['item']);exit();

        $this->data['work'] = [];
        if (!empty($this->data['item']['BRANCH_ID']))
            $this->data['work'] = $this->db->query('
                SELECT
                *
                FROM BRANCHES
                WHERE ID = '.$this->data['item']['BRANCH_ID'].'
                LIMIT 1')->row_array();

        $this->data['metros'] = [];
        if (!empty($this->data['work']['ID'])) {
            $this->data['metros'] = $this->db->query('
                SELECT
                METRO.NAME
                FROM BRANCH_METROS
                LEFT JOIN METRO ON METRO.ID = BRANCH_METROS.METRO_ID
                WHERE BRANCH_METROS.BRANCH_ID = '.$this->data['work']['ID'])->result_array();
        }

        if (empty($this->data['item']))
            show_404();

        $this->data['user_id'] = $this->user_id;
        $this->data['user_data'] = $this->User_model->get_by_id($this->user_id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        $this->data['tommorow_foramted_date'] = date('d-m-Y', time() + 3600 * 24);

        $this->data['item']['MONDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['MONDAY_WORK_FROM']));
        $this->data['item']['TUESDAY_WORK_FROM']      = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_FROM']));
        $this->data['item']['WEDNESDAY_WORK_FROM']    = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_FROM']));
        $this->data['item']['THURSDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_FROM']));
        $this->data['item']['FRIDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_FROM']));
        $this->data['item']['SATURDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_FROM']));
        $this->data['item']['SUNDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_FROM']));

        $this->data['item']['MONDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['MONDAY_WORK_TO']));
        $this->data['item']['TUESDAY_WORK_TO']        = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_TO']));
        $this->data['item']['WEDNESDAY_WORK_TO']      = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_TO']));
        $this->data['item']['THURSDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_TO']));
        $this->data['item']['FRIDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_TO']));
        $this->data['item']['SATURDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_TO']));
        $this->data['item']['SUNDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_TO']));

        $this->data['times'] = range(strtotime("7:00"),strtotime("18:00"), 60*15);

          $this->data['articles']     = $this->db
            ->where('OBJECT', 3)
            ->where('LOADER_ID', $id)
            ->where('PUBLIC', 1)
            ->get('ARTICLES')
            ->result_array();
          
        //$this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();

        $this->load->view('main/doctor_photo', $this->data);
    }
    
    
    
    
    
    
    
    
    public function item($id = 0)
    {
        $id = (int)$id;

        $this->data['id'] = $id;
        $this->data['work'] = [];
        $this->data['metro'] = [];
        $this->load->model('Branch_metros_model');
        
        $this->data['item'] = $this->Specialist_model->get_by_id($id);

//                var_dump($this->data['item']);exit();

        $this->data['work'] = [];
        if (!empty($this->data['item']['BRANCH_ID']))
            $this->data['work'] = $this->db->query('
                SELECT
                *
                FROM BRANCHES
                WHERE ID = '.$this->data['item']['BRANCH_ID'].'
                LIMIT 1')->row_array();

        $this->data['metros'] = [];
        if (!empty($this->data['work']['ID'])) {
            $this->data['metros'] = $this->db->query('
                SELECT
                METRO.NAME
                FROM BRANCH_METROS
                LEFT JOIN METRO ON METRO.ID = BRANCH_METROS.METRO_ID
                WHERE BRANCH_METROS.BRANCH_ID = '.$this->data['work']['ID'])->result_array();
        }

        if (empty($this->data['item']))
            show_404();

        $this->data['user_id'] = $this->user_id;
        $this->data['user_data'] = $this->User_model->get_by_id($this->user_id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        $this->data['tommorow_foramted_date'] = date('d-m-Y', time() + 3600 * 24);

        $this->data['item']['MONDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['MONDAY_WORK_FROM']));
        $this->data['item']['TUESDAY_WORK_FROM']      = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_FROM']));
        $this->data['item']['WEDNESDAY_WORK_FROM']    = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_FROM']));
        $this->data['item']['THURSDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_FROM']));
        $this->data['item']['FRIDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_FROM']));
        $this->data['item']['SATURDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_FROM']));
        $this->data['item']['SUNDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_FROM']));

        $this->data['item']['MONDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['MONDAY_WORK_TO']));
        $this->data['item']['TUESDAY_WORK_TO']        = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_TO']));
        $this->data['item']['WEDNESDAY_WORK_TO']      = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_TO']));
        $this->data['item']['THURSDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_TO']));
        $this->data['item']['FRIDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_TO']));
        $this->data['item']['SATURDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_TO']));
        $this->data['item']['SUNDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_TO']));

        $this->data['times'] = range(strtotime("7:00"),strtotime("18:00"), 60*15);

        //$this->data['videos'] = $this->db->query("SELECT * FROM VIDEOS WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();
        if (!empty($this->data['videos'])) {
            $this->data['first_video'] = $this->data['videos'][0];
            unset($this->data['videos'][0]);
        } else {
            $this->data['first_video'] = [];
        }

       // $this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();

        $this->load->view('main/doctor', $this->data);
    }
    
    
    
    
    
    
    
    
       public function opinions($id = 0,$iduser = 0)
    {
        $id = (int)$id;

        $this->data['id'] = $id;
        $this->data['work'] = [];
        $this->data['metro'] = [];
        $this->load->model('Branch_metros_model');
        
        $this->data['item'] = $this->Specialist_model->get_by_id($id);

//                var_dump($this->data['item']);exit();

        $this->data['work'] = [];
        if (!empty($this->data['item']['BRANCH_ID']))
            $this->data['work'] = $this->db->query('
                SELECT
                *
                FROM BRANCHES
                WHERE ID = '.$this->data['item']['BRANCH_ID'].'
                LIMIT 1')->row_array();

        $this->data['metros'] = [];
        if (!empty($this->data['work']['ID'])) {
            $this->data['metros'] = $this->db->query('
                SELECT
                METRO.NAME
                FROM BRANCH_METROS
                LEFT JOIN METRO ON METRO.ID = BRANCH_METROS.METRO_ID
                WHERE BRANCH_METROS.BRANCH_ID = '.$this->data['work']['ID'])->result_array();
        }

        if (empty($this->data['item']))
            show_404();

        $this->data['user_id'] = $this->user_id;
        $this->data['user_data'] = $this->User_model->get_by_id($this->user_id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        $this->data['tommorow_foramted_date'] = date('d-m-Y', time() + 3600 * 24);

        $this->data['item']['MONDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['MONDAY_WORK_FROM']));
        $this->data['item']['TUESDAY_WORK_FROM']      = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_FROM']));
        $this->data['item']['WEDNESDAY_WORK_FROM']    = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_FROM']));
        $this->data['item']['THURSDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_FROM']));
        $this->data['item']['FRIDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_FROM']));
        $this->data['item']['SATURDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_FROM']));
        $this->data['item']['SUNDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_FROM']));

        $this->data['item']['MONDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['MONDAY_WORK_TO']));
        $this->data['item']['TUESDAY_WORK_TO']        = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_TO']));
        $this->data['item']['WEDNESDAY_WORK_TO']      = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_TO']));
        $this->data['item']['THURSDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_TO']));
        $this->data['item']['FRIDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_TO']));
        $this->data['item']['SATURDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_TO']));
        $this->data['item']['SUNDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_TO']));

        $this->data['times'] = range(strtotime("7:00"),strtotime("18:00"), 60*15);

        //$this->data['videos'] = $this->db->query("SELECT * FROM VIDEOS WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();
        if (!empty($this->data['videos'])) {
            $this->data['first_video'] = $this->data['videos'][0];
            unset($this->data['videos'][0]);
        } else {
            $this->data['first_video'] = [];
        }

        //$this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();
        
        
        $this->data['feedbacks']    = $this->db->where('OBJECT', 3)
            ->select('FEEDBACKS.*')
            ->select('USERS.FIRSTNAME')
            ->select('USERS.LASTNAME')
            ->select('USERS.MIDDLENAME')
            ->where('LOADER_ID', $iduser)
            ->join('USERS','USERS.ID = FEEDBACKS.USER_ID')
            ->join('POSTS','POSTS.IDFEEDBACK = FEEDBACKS.ID')
            ->get('FEEDBACKS')
            ->result_array();
             $this->data['iduser'] = $iduser;
          
        $this->load->view('main/doctor_opinions', $this->data);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}