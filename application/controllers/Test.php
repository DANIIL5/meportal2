<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller
{
    public function fake_admin()
    {
        $data = [
            "ADMIN_ID"     => 1,
        ];
        $this->session->set_userdata($data);
    }

    public function image()
    {
//        print_r($_SERVER['DOCUMENT_ROOT']);exit();
        $config['image_library'] = 'GD2';
        $config['source_image'] = $_SERVER['DOCUMENT_ROOT'].'/images/advice-ava.png';
        $config['new_image'] = $_SERVER['DOCUMENT_ROOT'].'/images/thumb-advice-ava.png';
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']         = 75;
        $config['height']       = 50;

        $this->load->library('image_lib', $config);

        if ( ! $this->image_lib->resize())
        {
            echo $this->image_lib->display_errors();
//                    exit();
        }
    }

    function db()
    {
        ?><pre><?
        var_dump($this->db);
    }

    function info()
    {
        phpinfo();
    }

    public function new_import()
    {
        $start = microtime(true);
        // Настройки
        ini_set("memory_limit", "3000M");
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        mb_internal_encoding("UTF-8");
        ini_set('display_errors','Off');

        // выкачиваем РЛС базу и храним в памяти

        $sql = "SELECT
            NOMEN.ID NOMENID,
            FIRMS.FULLNAME FIRMNAME,
            TRADENAMES.NAME TRADENAME,
            TRADENAMES.INAME TRADEINAME,
            CLSDRUGFORMS.FULLNAME DRUGFORMFULLNAME,
            CLSDRUGFORMS.NAME DRUGFORMNAME,
            PREP.DFMASS,
            MASSUNITS.SHORTNAME DFMASSNAME,
            PREP.DFCONC,
            CONCENUNITS.SHORTNAME DFCONCNAME,
            PREP.DFACT,
            ACTUNITS.SHORTNAME DFACTNAME,
            PREP.DFSIZE,
            SIZEUNITS.SHORTNAME DFSIZENAME,
            DRUGFORMCHARS.FULLNAME DRUGFORMFULLCHAR,
            PREP.DRUGDOSE,
            PPACKDRUGPACK.FULLNAME PPACKFULLNAME,
            NOMEN.DRUGSINPPACK PPACKQUANTITY,
            NOMEN.PPACKMASS PPACKMASS,
            PPACKMASSUNITS.SHORTNAME PPACKMASSNAME,
            NOMEN.PPACKVOLUME PPACKVOLUME,
            PPACKCUBICUNITS.SHORTNAME PPACKVOLUMENAME,
            UPACKDRUGPACK.NAME UPACKNAME,
            NOMEN.PPACKINUPACK PPACKINUPACK,
            NOMEN.EANCODE,
            SPACKDRUGPACK.FULLNAME SPACKFULLNAME,
            NOMEN.UPACKINSPACK UPACKINSPACK
            FROM
            NOMEN NOMEN
            LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
            LEFT JOIN FIRMS ON FIRMS.ID = PREP.FIRMID
            LEFT JOIN TRADENAMES ON TRADENAMES.ID = PREP.TRADENAMEID
            LEFT JOIN LATINNAMES ON LATINNAMES.ID = PREP.LATINNAMEID
            LEFT JOIN CLSDRUGFORMS ON CLSDRUGFORMS.ID = PREP.DRUGFORMID
            LEFT JOIN MASSUNITS ON MASSUNITS.ID = PREP.DFMASSID
            LEFT JOIN CONCENUNITS ON CONCENUNITS.ID = PREP.DFCONCID
            LEFT JOIN ACTUNITS ON ACTUNITS.ID = PREP.DFACTID
            LEFT JOIN SIZEUNITS ON SIZEUNITS.ID = PREP.DFSIZEID
            LEFT JOIN DRUGFORMCHARS ON DRUGFORMCHARS.ID = PREP.DFCHARID
            LEFT JOIN DRUGPACK PPACKDRUGPACK ON PPACKDRUGPACK.ID = NOMEN.PPACKID
            LEFT JOIN MASSUNITS PPACKMASSUNITS ON PPACKMASSUNITS.ID = NOMEN.PPACKMASSUNID
            LEFT JOIN CUBICUNITS PPACKCUBICUNITS ON PPACKCUBICUNITS.ID = NOMEN.PPACKCUBUNID
            LEFT JOIN DRUGSET ON DRUGSET.ID = NOMEN.SETID
            LEFT JOIN DRUGPACK UPACKDRUGPACK ON UPACKDRUGPACK.ID = NOMEN.UPACKID
            LEFT JOIN CURRENCY ON CURRENCY.ID = NOMEN.CURRID
            LEFT JOIN DRUGSTORCOND ON DRUGSTORCOND.ID = NOMEN.CONDID
            LEFT JOIN DRUGLIFETIME ON DRUGLIFETIME.ID = NOMEN.LIFEID
            LEFT JOIN DRUGPACK SPACKDRUGPACK ON SPACKDRUGPACK.ID = NOMEN.SPACKID
            WHERE TRADENAMES.INAME IS NOT NULL";

        $rls_array = $this->db->query($sql)->result_array();

        // Сформировать массив с EAN-кодами, которые будут ссылаться на NOMENID

        foreach ($rls_array as $rls_row) {
            $name_index = mb_substr(strtoupper($rls_row['TRADEINAME']), 0, 3);

            $name = $rls_row['TRADEINAME']." ";
            $firm = $rls_row['FIRMNAME']." ";
            $dugformfullname = (!empty($rls_row['DRUGFORMNAME'])) ? $rls_row['DRUGFORMNAME']." " : "";
            $ppackfullname = (!empty($rls_row['PPACKFULLNAME'])) ? $rls_row['PPACKFULLNAME']." " : "";
            $dfmass = (!empty($rls_row['DFMASS']) && $rls_row['DFMASS'] > 0) ? (float)$rls_row['DFMASS'].$rls_row['DFMASSNAME']." " : "";
            $dfconc = (!empty($rls_row['DFCONC']) && $rls_row['DFCONC'] > 0) ? (float)$rls_row['DFCONC'].$rls_row['DFCONCNAME']." " : "";
            $dfact = (!empty($rls_row['DFACT']) && $rls_row['DFACT'] > 0) ? (float)$rls_row['DFACT'].$rls_row['DFACTNAME']." " : "";
            $dfsize = (!empty($rls_row['DFSIZE']) && $rls_row['DFSIZE'] > 0) ? (float)$rls_row['DFSIZE'].$rls_row['DFSIZENAME']." " : "";
            $quantity = (!empty($rls_row['PPACKQUANTITY']) && $rls_row['PPACKQUANTITY'] > 0) ? $rls_row['PPACKQUANTITY']." " : "";
            $ppackmass = (!empty($rls_row['PPACKMASS']) && $rls_row['PPACKMASS'] > 0) ? (float)$rls_row['PPACKMASS'].$rls_row['PPACKMASSNAME']." " : "";
            $ppackvolume = (!empty($rls_row['PPACKVOLUME']) && $rls_row['PPACKVOLUME'] > 0) ? (float)$rls_row['PPACKVOLUME'].$rls_row['PPACKVOLUMENAME']." " : "";
            $drugformfullchar = (!empty($rls_row['DRUGFORMFULLCHAR'])) ? $rls_row['DRUGFORMFULLCHAR'] : "";
            $value = $name.$quantity.$ppackmass.$dugformfullname.$dfmass.$dfconc.$dfact.$dfsize.$ppackvolume.$drugformfullchar.$firm;

            $value = mb_strtoupper(strip_tags(str_replace(array(',', '/', '.', '(', ')', '«', '»'), array(' ', ' ', ' ', ' ', ' ', '', ''), $value)));

            $rls_tree[$name_index][$rls_row['NOMENID']] = $value;

            if (!empty($rls_row['EANCODE']))
                $eans[mb_substr($rls_row['EANCODE'], 0, 12)] = $rls_row['NOMENID'];
        }

        unset($rls_array);

        //Разбираем товарные остатки аптеки в нашем формате
        $source_csv = fopen("/var/www/vhosts/mfp.rg3.su/ftp/1/13092016_1313011_3.csv","r");
        $i = 0;
        while (($csv_line = fgetcsv($source_csv)) !== FALSE) {
            $parsed_csv[] = $csv_line;
            $i ++;
            if ($i == 60000)
                break;
        }

        unset($source_csv);

        // переменные для проверки
        $iterations = 0;
        $good       = 0;
        $find_ean   = 0;
        $target     = 0;
        $find_lev   = 0;
        $bad        = 0;

        foreach ($parsed_csv as $item) {
            $iterations ++;

            if (!empty($eans[$item[6]])) {
                $test[] = "Лекарство: ".$item[1]." имеет NOMENID ".$eans[$item[6]];

                $temp_date = explode('.',$item[5]);
                $params = [
                    'BRANCH_ID'         => (int)$item[0],
                    'NOMEN_ID'          => (int)$eans[$item[6]],
                    'COUNT'             => (float)$item[3],
                    'PRICE'             => (float)preg_replace('/\p{Zs}/u','',$item[4]),
                    'EXPIRATION_DATE'   => $temp_date[2]."-".$temp_date[0]."-".$temp_date[1],
                    'DRUGNAME'          => $item[1],
                    'FIRMNAME'          => $item[2]
                ];
//                        $this->db->insert('TRADE_BALANCES',$params);
                $sql = $this->db->insert_string('TRADE_BALANCES', $params) . ' ON DUPLICATE KEY UPDATE COUNT = '.$item[3];
                $this->db->query($sql);
                $good++;
                $find_ean++;
            } else {
                $upper_line     = mb_strtoupper(str_replace('№', '', $item[1]." ".$item[2]));
                $upper_line     = strip_tags(str_replace(array(',', '/', '.', '(', ')'), array(' ', ' ', ' ', ' ', ' '), $upper_line));
                $temporary_key  = mb_substr($upper_line,0,3);

                $temp_date = explode('.',$item[5]);

                if (!empty($rls_tree[$temporary_key])) {
                    $levenstain_result = $this->lev_in_array($upper_line, $rls_tree[$temporary_key]);

                    if ($levenstain_result['rating'] > 0) {
                        //В гуд импорт
                        $find_lev++;

                        if ($levenstain_result['rating'] > 99)
                            $target++;

                        $insert_data = [
                            'BRANCH_ID'         => (int)$item[0],
                            'NOMEN_ID'          => (int)$levenstain_result['nomen_id'],
                            'COUNT'             => (float)$item[3],
                            'PRICE'             => (float)preg_replace('/\p{Zs}/u','',$item[4]),
                            'EXPIRATION_DATE'   => $temp_date[2]."-".$temp_date[0]."-".$temp_date[1],
                            'DRUGNAME'          => $item[1],
                            'FIRMNAME'          => $item[2]
                        ];

                        $sql = $this->db->insert_string('TRADE_BALANCES', $insert_data) . ' ON DUPLICATE KEY UPDATE COUNT = '.$item[3];
                        $this->db->query($sql);
                        $good++;
                    } else {
                        $insert_data = [
                            'BRANCH_ID'         => (int)$item[0],
                            'DRUGNAME'          => $item[1],
                            'FIRMNAME'          => $item[2],
                            'COUNT'             => (float)$item[3],
                            'PRICE'             => (float)preg_replace('/\p{Zs}/u','',$item[4]),
                            'EXPIRATION_DATE'   => $temp_date[2]."-".$temp_date[0]."-".$temp_date[1]
                        ];

                        $sql = $this->db->insert_string('BAD_IMPORT', $insert_data) . ' ON DUPLICATE KEY UPDATE COUNT = '.$item[3];
                        $this->db->query($sql);
                        $bad++;
                    }
                } else {
                    $insert_data = [
                        'BRANCH_ID'         => (int)$item[0],
                        'DRUGNAME'          => $item[1],
                        'FIRMNAME'          => $item[2],
                        'COUNT'             => (float)$item[3],
                        'PRICE'             => (float)preg_replace('/\p{Zs}/u','',$item[4]),
                        'EXPIRATION_DATE'   => $temp_date[2]."-".$temp_date[0]."-".$temp_date[1]
                    ];

                    $sql = $this->db->insert_string('BAD_IMPORT', $insert_data) . ' ON DUPLICATE KEY UPDATE COUNT = '.$item[3];
                    $this->db->query($sql);
                    $bad++;
                }
            }
        }

        ?><pre><?
//        print_r($rls_tree);

        echo "Количество строк из файла: ".$iterations."<br>";
        echo "Количество импортированных: ".$good."<br>";
        echo "Количество совпадений по ШКП: ".$find_ean."<br>";
        echo "Количество точных совпадений по алгоритму Левенштайна: ".$target."<br>";
        echo "Количество близких совпадений по алгоритму Левенштайна: ".$find_lev."<br>";
        echo "Количество не связанных товаров: ".$bad."<br>";
        echo "Время работы: ".(microtime(true) - $start)."секунд<br><br>";

        echo "Нагрузка оперативки: ".$this->convert(memory_get_usage(true))."<br>";
    }

    private function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }

    public function lev_in_array($string, $array)
    {
        $closest = null;
        $shortest = -1;
        $word = "";

        $first_word_string = explode(' ', $string)[0];

        foreach ($array as $key => $item) {
            $lev = levenshtein($string, $item);

            $first_word_item = explode(' ', $item)[0];

//            if ($first_word_string == $first_word_item)
//                $lev -= strlen($first_word_string);

            if ($lev == 0) {
                // это ближайшее слово (точное совпадение)
                $word = $item;
                $closest = $key;
                $shortest = 0;
                // выходим из цикла - мы нашли точное совпадение
                break;
            } elseif ($lev <= $shortest || $shortest < 0) {
                // если это расстояние меньше следующего наименьшего расстояния
                // ИЛИ если следующее самое короткое слово еще не было найдено
                $word = $item;
                $closest  = $key;
                $shortest = $lev;
            }
        }

//        if ($shortest == -1) {
//            return false;
//        } else {
//            return $closest;
//        }

//        echo "Вы ввели: $string<br>";
        if ($shortest == 0) {
//            echo "Найдено точное совпадение: $closest -> $word<br><br>";
            return [
                'nomen_id'   => $closest,
                'rating'    => 100.0
            ];
        } else {
            $length = strlen($string);
//            echo "Точных совпадений нет, может это: <br> $closest: $word?<br> У него индекс $shortest,<br> длина самой строки ".$length.",<br> рейтинг точности ". (39 - ($shortest/$length*100)) ."<br><br>";
            return [
                'nomen_id'   => $closest,
                'rating'    => (39 - ($shortest/$length*100))
            ];
        }
    }

    public function import_from_csv()
    {

    }
}