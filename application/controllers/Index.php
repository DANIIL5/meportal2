<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->data['controller']   = $this->router->fetch_class();
        $this->data['method']       = $this->router->fetch_method();

        $this->user_id = $this->session->userdata('USER_ID');

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Carousel_model');
        $this->load->model('Action_model');
        $this->load->model('Banner_model');
        $this->load->model('News_model');
        $this->load->model('Institution_model');
        $this->load->model('Articles_model');
        $this->load->model('Geo_model');
        $this->load->model('Logos_model');
        $this->load->model('Post_model');
        $this->load->model('Voting_model');
       // $this->load->model('Voting_model');

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $this->data['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $this->data['current_city'] = $cookie_city_id;
        } else {
            $this->data['current_city'] = 4400;
        }

        //Получаем две случайные акции для блока на главной
        $actions['actions'] = $this->Action_model->get_random_list(2,1);

        foreach ($actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $actions['actions'][$key]['DESCRIPTION'] = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $actions['actions'][$key]['PRICE'] = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        // Получаем акции для вывода на карту
        $map_actions['actions'] = $this->Action_model->get_random_map(4,1);

        foreach ($map_actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $map_actions['actions'][$key]['DESCRIPTION']    = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $map_actions['actions'][$key]['PRICE']              = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        $carousel = $this->Carousel_model->get_images(1);
        $actions_quantity        = $this->Action_model->count();
        $actions['quantity']     = $actions_quantity;

        $map_actions['quantity'] = $actions_quantity;
        $map_actions['green'] = false;
        $this->load->model('New_organisations_model');
        $popular_institutions = $this->New_organisations_model->get_list(['POPULAR' => 1], [], null);
        $first_banners  = $this->Banner_model->get_banners(1);
        $second_banners = $this->Banner_model->get_banners(1);
        $news = $this->News_model->get_last();
        $map_actions['types'] = $this->Institution_model->get_types();

        // выбираем три рандомные категории
        $popular_themes_category = $this->Articles_model->get_random_themes();

        $popular_themes_category_ids = null;

        foreach ($popular_themes_category as $item) {
            $popular_themes_category_ids[] = $item['ID'];
        }

        $popular_themes = $this->Articles_model->get_articles_by_ids_array($popular_themes_category_ids);

        foreach ($popular_themes as $popular_theme) {
            $popular_themes_data['themes'][$popular_theme['CATEGORY_ID']][] = [
                'ID'        => $popular_theme['ID'],
                'NAME'      => $popular_theme['NAME']
            ];
        }

        $popular_themes_data['category'] = $popular_themes_category;

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);
        $cities['current_city'] = $this->data['current_city'];

        $logos = $this->New_organisations_model->get_list(['POPULAR' => 1], []);

        $voting = $this->Voting_model->get_vote();
        $voting['is_vote'] = $this->Voting_model->is_vote($voting['info']['ID'], $this->user_id);

        $this->data['templates'] = [
            'city_selector'     => $this->load->view('tmp/city_selector', $cities, true),
            'auth'              => ($this->user_id)
                ? $this->load->view('tmp/auth_entered', [
                    'data' => $this->User_model->get_by_id($this->user_id),
                    'msg_count' => $this->Post_model->get_by_user_id_in_count_unread($this->user_id)[0]['COUNT(*)']
                ], true)
                : $this->load->view('tmp/auth', null, true),
            'carousel'          => $this->load->view('tmp/carousel', ['images' => $carousel], true),
            'menu'              => $this->load->view('tmp/menu', ['controller' => $this->data['controller']], true),
            'search'            => $this->load->view('tmp/search', null, true),
            'ask_question'      => $this->load->view('tmp/ask_question', ['is_auth' => (!empty($this->user_id)) ? true : false], true),
            'actions'           => $this->load->view('tmp/actions', $actions, true),
            'logos'             => $this->load->view('tmp/logos', ['logos' => $logos], true),
            'map_search'        => $this->load->view('tmp/map_search', $map_actions, true),
            'popular_institutions'  => $this->load->view('tmp/popular_institutions', ['institutions' => $popular_institutions], true),
            'first_banners'     => $this->load->view('tmp/banners', ['banners' => $first_banners], true),
            'second_banners'    => $this->load->view('tmp/banners', ['banners' => $second_banners], true),
            'popular_themes'    => $this->load->view('tmp/popular_themes', $popular_themes_data, true),
            'voiting'           => $this->load->view('tmp/voiting', $voting, true),
            'news'              => $this->load->view('tmp/news', ['news' => $news], true),
            'bottom_menu'       => $this->load->view('tmp/bottom_menu', null, true),
            'footer'            => $this->load->view('tmp/footer', ['user' => $this->user_id], true)
        ];

        $this->data['isAuth'] = ($this->user_id) ? true : false;
    }

    public function index()
    {
        $this->load->view('main/index',$this->data);
    }
    
//добавление картинки во временную папку
    public function addDocument($directory='images',$maxSize='20480')
    {   //настраиваем загрузку
        if($maxSize=='2048')
            $allowed_types='gif|jpg|png|jpeg';
        else
            $allowed_types='gif|jpg|jpeg|png|tif|tiff|pdf|doc|docx|rtf|txt|ppt|pptx|xls|xlsx|odf';
        $config['upload_path']   = FCPATH.'/'.$directory.'/';
        $config['allowed_types'] = $allowed_types;
        $config['max_size']      = $maxSize;
        $config['max_width']     = '0';
        $config['max_height']    = '0';
        $config['encrypt_name']  = true;
        //производим загрузку
        $this->load->library('upload', $config);
        //если успешно загрузилась
        if (!$this->upload->do_upload())
        {   
            $error = ['error' => $this->upload->display_errors()];
            echo json_encode(['error'=>$error]);
        }
        //если не загрузилась
        else
        {
             // print_r($_FILES);
            $data = ['upload_data' => $this->upload->data()];
            echo json_encode(['success'=>true,'msg'=>$data]);
        }
    }
    
    
     public function deletephoto()
    {  
        //print_r($_POST);
       
        $result=unlink($_SERVER['DOCUMENT_ROOT'].$_POST['path']);
        if($result){
         echo json_encode(['success'=>true]);   
        }
        else{
            echo json_encode(['fail'=>false]); 
        }
    }
    
    
    
    
    /*
     * Developed by psinetron
     * http://slybeaver.ru
     * https://github.com/psinetron
     * Params:
     * $filepath - path to docx file template
     * $templates - array of  pattern value
     * $output - path to output file
     * !Attention! You need the ZipArchive library or other Zip Class on PHP. ZipArchive library included on PHP >=5.2
     */
    public static function docxTemplate($filepath = false, $templates = false, $output = false)
    {
        if (($filepath===false) || ($templates===false) || ($output===false) || !file_exists($filepath))
            return false;
        if (file_exists($output))
            unlink($output);
        copy($filepath, $output);
        if (!file_exists($output))
            return false;
        $zip = new ZipArchive();
        if (!$zip->open($output))
            return false;
        $documentXml = $zip->getFromName('word/document.xml');
        $rekeys = array_keys($templates);
        for ($i = 0; $i < count($templates); $i++) {
            $reg = '';
            $reg .= substr($rekeys[$i], 0, 1);
            for ($i2 = 1; $i2 < strlen($rekeys[$i]); $i2++) {
                $reg .= '(<.*?>)*+' . substr($rekeys[$i], $i2, 1);
            }
            $reg = '#' . str_replace(array('#', '{', '[', ']', '}'), array('#', '{', '[', ']', '}'), $reg) . '#';
            $documentXml = preg_replace($reg, $templates[$rekeys[$i]], $documentXml);
        }
        $zip->deleteName('word/document.xml');
        $zip->addFromString('word/document.xml', $documentXml);
        $zip->close();
        return true;
    }
}