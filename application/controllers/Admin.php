<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function auth()
    {

    }

    public function test()
    {
        $this->checkAdmin();
    }

    public function checkAdmin()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/admin/auth');
    }
}