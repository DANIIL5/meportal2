<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->data['controller'] = $this->router->fetch_class();
        $this->data['method'] = $this->router->fetch_method();

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Question_model');

        $this->user_id = $this->session->userdata('USER_ID');
    }

    function change_city()
    {
        // Если пользователь авторизован, то сохраняем в базу, если нет, то в куки

        $city_id = (int)$_POST['city_id'];

        if (!empty($city_id)) {
            if (!empty($this->user_id)) {
                $this->User_model->update(['DEFAULT_CITY' => $city_id], $this->user_id);
            } else {
                $this->session->set_userdata(['DEFAULT_CITY' => $city_id]);
            }
        }
    }

    public function ask_question()
    {
        if (!empty($_POST['question']) && strlen($question = $_POST['question']) > 9) {
            $fields   = [
                'USER_ID'   => $this->user_id,
                'QUESTION'  => $question,
                'PAGE'      => $_SERVER['HTTP_REFERER']
            ];

            if ($this->Question_model->create($fields)) {
                $status = "success";
                $result = [];

                if (!empty($this->user_id)) {
                    $user=$this->User_model->get_by_id($this->user_id);
                    $fields = [
                        'ID_SENDER'     =>$this->user_id,
                        'MESSAGE'       =>$question,
                        'TIME'          =>date("Y-m-d H:i:s",time()),
                        'THEME'         =>'Вопрос на сайте',
                        'DELETED'       =>0,
                        'FINAL_DELETED' =>0
                    ];/*в зависимости от типа пользователя в письме он отображается как юзер, учреждение или врач*/
                    if($user['TYPE']==3)
                    {
                        $institutionThis = $this->db->where('USER_ID', $this->user_id)->get('ORGANIZATIONS')->row_array();
                        $fields['NAME_SENDER'] = $institutionThis['BRAND'];
                    }
                    elseif($user['TYPE']==2)
                    {
                        $this->load->model('Doctor_model');
                        $specialistThis=$this->Doctor_model->get_by_user_id($this->user_id);
                        $fields['NAME_SENDER']=$specialistThis['LAST_NAME'].' '.$specialistThis['FIRST_NAME'];
                    }
                    else
                        $fields['NAME_SENDER']=$user['LASTNAME'].' '.$user['FIRSTNAME'];
                    if(!empty($_POST['responded_post_id']))
                        $fields['ID_RESPONDED_POST']=$_POST['responded_post_id'];
                    $data['user']=$user;
                    $data['fields']=$fields;

                    $this->load->model('Post_model');
                    $this->load->model('Post_destinations_model');
                    $insert_post_id = $this->Post_model->create($data['fields']);

                    $fieldsDestination = [
                        'ID_POST'            =>$insert_post_id,
                        'DELETED'            =>0,
                        'FINAL_DELETED'      =>0,
                        'DESTINATION_READED' =>0,
                        'TO_ADMIN'           =>0,
                        'ID_DESTINATION'     => NULL
                    ];

                    $insert_destination_id = $this->Post_destinations_model->create($fieldsDestination);
                }

                $response = [
                    'status'    => $status,
                    'data'      => $result
                ];
            } else {
                $status = "error";
                $result = [
                    'error'     => 'Неизвестная ошибка'
                ];
            }
        } else {
            $status = "error";
            $result   = [
                'error'     => 'Вопрос не должен быть короче 10 символов'
            ];
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function send_review()
    {
        if (!empty($this->user_id)) {
            if (!empty($_POST['review']) && strlen($_POST['review']) > 4) {
                $fields   = [
                    'USER_ID'   => $this->user_id,
                    'REVIEW'    => mysqli_real_escape_string($this->db->conn_id, $_POST['review']),
                    'CREATED_AT'=> date('Y-m-d'),
                    'UPDATED_AT'=> date('Y-m-d'),
                    'MODERATED' => 0
                ];

                $this->db->insert('REVIEWS', $fields);

                $status = "success";
                $result = [];
            } else {
                $status = "error";
                $result = [
                    "error" => "Отзыв должен быть не короче 5 символов"
                ];
            }
        } else {
            $status = "error";
            $result = [
                "error" => "Вы должны быть авторизованы, чтобы отправлять комментарии"
                ];
        }

        $response = [
            'status'    => $status,
            'data'      => $result
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    /**
     * search() - новый поиск
     */

    public function search()
    {
    	if (!empty($_GET['query'])) {
    		$query = mysqli_real_escape_string($this->db->conn_id, $_GET['query']);
    		$result = $this->db->query("
    			(
    				SELECT 
    				NAME
    				FROM TRADENAMES 
    				WHERE INAME LIKE '$query%'
				)
    			UNION
    			(
    				SELECT
    				NAME
    				FROM INSTITUTIONS
    				WHERE NAME LIKE '%$query%'
				)
    			UNION
    			(
    				SELECT
    				NAME COLLATE utf8_general_ci
    				FROM ARTICLES
    				WHERE NAME LIKE '%$query%'
				)
    			UNION
    			(
    				SELECT
    				NAME
    				FROM ACTIONS
    				WHERE NAME LIKE '%$query%'
				)
    			ORDER BY NAME
    			LIMIT 5
    		")->result_array();

    		return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    	}
    }

    public function search_in_organisation()
    {
        $id     = !empty($_GET['id']) ? (int)$_GET['id'] : null;
        $query  = !empty($_GET['query']) ? mysqli_real_escape_string($this->db->conn_id, $_GET['query']) : null;

        if (!empty($query) && !empty($id)) {
            $result = $this->db->query("
    				SELECT 
    				CONCAT(coalesce(ADDRESS_REGION,''),' ',coalesce(ADDRESS_CITY,''),' ',coalesce(ADDRESS_STREET,''),' ',coalesce(ADDRESS_HOUSE,''),' ',coalesce(ADDRESS_BUILDING,''),' ',coalesce(ADDRESS_OFFICE,'')) NAME
    				FROM BRANCHES 
    				WHERE 
    				  (ADDRESS_REGION LIKE '%$query%'
    				  OR
    				  ADDRESS_CITY LIKE '%$query%'
    				  OR
    				  ADDRESS_STREET LIKE '%$query%'
    				  OR
    				  ADDRESS_HOUSE LIKE '%$query%'
    				  OR
    				  ADDRESS_BUILDING LIKE '%$query%'
    				  OR
    				  ADDRESS_OFFICE LIKE '%$query%')
    				AND INSTITUTION_ID = $id  
    			ORDER BY NAME
    			LIMIT 5
    		")->result_array();

            return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
        }
    }

    /**
     * old_search() - старый поиск
     */

    public function old_search()
    {
        $response = [];

        if (!empty($_GET['query'])) {
            $sphinx = new mysqli('127.0.0.1', null, null, null, 9306);
            $query = mysqli_real_escape_string($this->db->conn_id, $_GET['query']);
            $resource = $sphinx->query("SELECT * FROM main WHERE MATCH('$query') LIMIT 5");
            while ($row = $resource->fetch_assoc()) {
                $name = $row['tradeiname']." ";
                $firm = $row['firmname']." ";
                $dugformfullname = (!empty($row['drugformfullname'])) ? $row['drugformfullname']." " : "";
                $ppackfullname = (!empty($row['ppackfullname'])) ? $row['ppackfullname']." " : "";
                $dfmass = (!empty($row['dfmass']) && $row['dfmass'] > 0) ? (float)$row['dfmass']." " : "";
                $dfconc = (!empty($row['dfconc']) && $row['dfconc'] > 0) ? (float)$row['dfconc'].$row['dfconcfullname']." " : "";
                $dfact = (!empty($row['dfact']) && $row['dfact'] > 0) ? (float)$row['dfact'].$row['dfactfullname']." " : "";
                $dfsize = (!empty($row['dfsize']) && $row['dfsize'] > 0) ? (float)$row['dfsize'].$row['dfsizefullname']." " : "";
                $drugformfullchar = (!empty($row['drugformfullchar'])) ? $row['drugformfullchar'] : "";
                $value = $name.$dugformfullname.$ppackfullname.$dfmass.$dfconc.$dfact.$dfsize.$drugformfullchar.$firm;

                $response[] = [
                    'value'  => $value
                ];
            }
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function economical_buy()
    {
        $response   = null;
        $data       = [];
        $nomen_ids  = [];
        $joins      = [];
        $min_sum    = [];

        if (!empty($_GET['GOODS']) && is_array($_GET['GOODS'])) {
            foreach ($_GET['GOODS'] as $id => $count) {
                if (!empty($id) && !empty($count)) {
                    $id      = (int)$id;
                    $count   = (int)$count;

                    if (!empty($id) && !empty($count)) {
                        $nomen_ids[] = $id;
                        $joins[]     = "JOIN TRADE_BALANCES r$id ON r$id.BRANCH_ID = BRANCHES.ID AND r$id.NOMEN_ID = $id AND r$id.COUNT >= 1";
                        $min_sum[]   = "r$id.price * $count";
                    }
                }
            }

            if (!empty($nomen_ids)) {
                $join_condition     = implode(" ", $joins);
                $min_sum_selector   = implode(" + ", $min_sum);

                $query = "SELECT
                    BRANCHES.ID,
                    min($min_sum_selector) SUM
                    FROM BRANCHES
                    LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
                    $join_condition
                    GROUP BY id, name
                    ORDER BY sum
                    LIMIT 10";

                $branches = $this->db->query($query)->result_array();

                if (!empty($branches)) {
                    foreach ($branches as $branch) {
                        $full_prices[$branch['ID']] = $branch['SUM'];
                        $branch_ids[] = $branch['ID'];
                    }

                    $where_nomens   = "TRADE_BALANCES.NOMEN_ID IN (".implode(',', $nomen_ids).")";
                    $where_branches = "TRADE_BALANCES.BRANCH_ID IN (".implode(',', $branch_ids).")";
                    $where          = "WHERE ($where_nomens) AND ($where_branches)";

                    $query = "SELECT
                    NOMEN.ID NOMENID,
                    INSTITUTIONS.NAME INSTITUTION,
                    INSTITUTIONS.SHORTNAME INSTITUTION_SHORTNAME,
                    INSTITUTIONS.DESCRIPTION INSTITUTION_DESCRIPTON,
                    INSTITUTIONS.LOGO INSTITUTION_LOGO,
                    BRANCHES.ADDRESS INSTITUTION_ADDRESS,
                    METRO.NAME INSTITUTION_METRO,
                    BRANCHES.ID BRANCHID,
                    BRANCHES.PHONES INSTITUTION_PHONES,
                    BRANCHES.WEBSITES INSTITUTION_WEBSITES,
                    BRANCHES.LATITUDE INSTITUTION_LATITUDE,
                    BRANCHES.LONGITUDE INSTITUTION_LONGITUDE,
                    BRANCHES.SCHEDULE INSTITUTION_SCHEDULE,
                    FIRMS.FULLNAME FIRMNAME,
                    TRADENAMES.INAME TRADEINAME,
                    TRADENAMES.NAME TRADENAME,
                    CLSDRUGFORMS.FULLNAME DRUGFORMFULLNAME,
                    CLSDRUGFORMS.NAME DRUGFORMNAME,
                    PREP.DFMASS,
                    MASSUNITS.FULLNAME DFMASSFULLNAME,
                    MASSUNITS.SHORTNAME DFMASSNAME,
                    PREP.DFCONC,
                    CONCENUNITS.FULLNAME DFCONCFULLNAME,
                    CONCENUNITS.SHORTNAME DFCONCNAME,
                    PREP.DFACT,
                    ACTUNITS.FULLNAME DFACTFULLNAME,
                    ACTUNITS.SHORTNAME DFACTNAME,
                    PREP.DFSIZE,
                    SIZEUNITS.FULLNAME DFSIZEFULLNAME,
                    SIZEUNITS.SHORTNAME DFSIZENAME,
                    DRUGFORMCHARS.FULLNAME DRUGFORMFULLCHAR,
                    PREP.DRUGDOSE,
                    PPACKDRUGPACK.FULLNAME PPACKFULLNAME,
                    NOMEN.DRUGSINPPACK PPACKQUANTITY,
                    NOMEN.PPACKMASS PPACKMASS,
                    PPACKMASSUNITS.FULLNAME PPACKMASSFULLNAME,
                    NOMEN.PPACKVOLUME PPACKVOLUME,
                    PPACKCUBICUNITS.FULLNAME PPACKVOLUMEFULLNAME,
                    UPACKDRUGPACK.FULLNAME UPACKFULLNAME,
                    SPACKDRUGPACK.FULLNAME SPACKFULLNAME,
                    TRADE_BALANCES.COUNT COUNT,
                    TRADE_BALANCES.PRICE PRICE
                    FROM
                    TRADE_BALANCES
                    INNER JOIN NOMEN ON NOMEN.ID = TRADE_BALANCES.NOMEN_ID
                        LEFT JOIN BRANCHES ON BRANCHES.ID = TRADE_BALANCES.BRANCH_ID
                        LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
                        LEFT JOIN METRO ON METRO.ID = BRANCHES.METRO_ID
                        LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
                        LEFT JOIN FIRMS ON FIRMS.ID = PREP.FIRMID
                        LEFT JOIN TRADENAMES ON TRADENAMES.ID = PREP.TRADENAMEID
                        LEFT JOIN LATINNAMES ON LATINNAMES.ID = PREP.LATINNAMEID
                        LEFT JOIN CLSDRUGFORMS ON CLSDRUGFORMS.ID = PREP.DRUGFORMID
                        LEFT JOIN MASSUNITS ON MASSUNITS.ID = PREP.DFMASSID
                        LEFT JOIN CONCENUNITS ON CONCENUNITS.ID = PREP.DFCONCID
                        LEFT JOIN ACTUNITS ON ACTUNITS.ID = PREP.DFACTID
                        LEFT JOIN SIZEUNITS ON SIZEUNITS.ID = PREP.DFSIZEID
                        LEFT JOIN DRUGFORMCHARS ON DRUGFORMCHARS.ID = PREP.DFCHARID
                        LEFT JOIN DRUGPACK PPACKDRUGPACK ON PPACKDRUGPACK.ID = NOMEN.PPACKID
                        LEFT JOIN MASSUNITS PPACKMASSUNITS ON PPACKMASSUNITS.ID = NOMEN.PPACKMASSUNID
                        LEFT JOIN CUBICUNITS PPACKCUBICUNITS ON PPACKCUBICUNITS.ID = NOMEN.PPACKCUBUNID
                        LEFT JOIN DRUGSET ON DRUGSET.ID = NOMEN.SETID
                        LEFT JOIN DRUGPACK UPACKDRUGPACK ON UPACKDRUGPACK.ID = NOMEN.UPACKID
                        LEFT JOIN CURRENCY ON CURRENCY.ID = NOMEN.CURRID
                        LEFT JOIN DRUGSTORCOND ON DRUGSTORCOND.ID = NOMEN.CONDID
                        LEFT JOIN DRUGLIFETIME ON DRUGLIFETIME.ID = NOMEN.LIFEID
                        LEFT JOIN DRUGPACK SPACKDRUGPACK ON SPACKDRUGPACK.ID = NOMEN.SPACKID
                    $where
                    ORDER BY NOMENID,PRICE";

                    $goods = $this->db->query($query)->result_array();

                    foreach ($goods as $good) {
                        $data[$good['BRANCHID']]['DRUGSTORE'] = [
                            'NAME'          => $good['INSTITUTION_SHORTNAME'],
                            'ADDRESS'       => $good['INSTITUTION_ADDRESS'],
                            'FULLPRICE'     => $full_prices[$good['BRANCHID']],
                            'LATITUDE'      => $good['INSTITUTION_LATITUDE'],
                            'LONGITUDE'     => $good['INSTITUTION_LONGITUDE'],
                        ];

                        $name = $good['TRADENAME']." ";
                        $firm = $good['FIRMNAME']." ";
                        $dugformfullname    = (!empty($good['DRUGFORMFULLNAME'])) ? $good['DRUGFORMNAME']." " : "";
                        $ppackfullname      = (!empty($good['PPACKFULLNAME'])) ? $good['PPACKFULLNAME']." " : "";
                        $dfmass             = (!empty($good['DFMASS']) && $good['DFMASS'] > 0) ? (float)$good['DFMASS'].$good['DFMASSNAME']." " : "";
                        $dfconc             = (!empty($good['DFCONC']) && $good['DFCONC'] > 0) ? (float)$good['DFCONC'].$good['DFCONCNAME']." " : "";
                        $dfact              = (!empty($good['DFACT']) && $good['DFACT'] > 0) ? (float)$good['DFACT'].$good['DFACTNAME']." " : "";
                        $dfsize             = (!empty($good['DFSIZE']) && $good['DFSIZE'] > 0) ? (float)$good['DFSIZE'].$good['DFSIZENAME']." " : "";
                        $drugformfullchar   = (!empty($good['DRUGFORMFULLCHAR'])) ? $good['DRUGFORMCHAR'] : "";
                        $fullname           = rtrim($name/*.$dugformfullname.$ppackfullname*/.$dfmass.$dfconc.$dfact.$dfsize.$drugformfullchar/*.$firm*/);

                        if (empty($data[$good['BRANCHID']]['GOODS'][$good['NOMENID']]))
                            $data[$good['BRANCHID']]['GOODS'][$good['NOMENID']] = [
                                'NAME'          => $fullname,
                                'DRUGFORM'      => $dugformfullname,
                                'COUNT'         => $_GET['GOODS'][$good['NOMENID']],//$good['COUNT'],
                                'PRICE'         => number_format($good['PRICE'], 2, '.', ' ')
                            ];
                    }
                }
            }
        }

        $response = $this->load->view('/tmp/economical_buy_list', ['data' => $data], true);
        return $this->output
            ->set_content_type('text/html')
            ->set_output($response);
    }

    public function on_map()
    {
        $where = "";
        $where_array = [];
        $result = [];
        $response = [];

        if (!empty($_POST['query'])) {
            $sphinx = new mysqli('127.0.0.1', null, null, null, 9306);
            $query = mysqli_real_escape_string($this->db->conn_id, $_POST['query']);
            $resource = $sphinx->query("SELECT * FROM organisations WHERE MATCH('$query') LIMIT 25");
            while ($row = $resource->fetch_assoc()) {
                $ids[] = (int)$row['id'];

                if (!empty($ids))
                    $result = $this->db->where_in('ID', $ids)->get('ORGANIZATIONS')->result_array();
            }
        } elseif (!empty($_POST['id'])) {
            $_POST['type_id'] = (int)$_POST['id'];

            if (!empty($_POST['id'])) {
                $result = $this->db->where('TYPE_ID', $_POST['id'])->get('ORGANIZATIONS')->result_array();
            }
        } else {
            $result = $this->db->get('ORGANIZATIONS')->result_array();
        }

        //TODO перешлифовать на комплексную подгрузку

        foreach ($result as $item) {
            $phones = $this->db
                ->where('ORGANIZATION_ID', $item['ID'])
                ->where('TYPE_ID', 2)
                ->get('ORGANIZATION_CONTACTS')->result_array();

            $websites = $this->db
                ->where('ORGANIZATION_ID', $item['ID'])
                ->where('TYPE_ID', 3)
                ->get('ORGANIZATION_CONTACTS')->result_array();

            $addresses = $this->db
                ->where('ORGANIZATION_ID', $item['ID'])
                ->where('TYPE_ID', 2)
                ->get('ORGANIZATION_ADDRESSES')->row_array();

            $temp = [
                'NAME'      => $item['BRAND'],
                'LOGO'      => $item['LOGO'],
                'TYPE_ID'   => $item['TYPE_ID'],
                'LATITUDE'  => $item['LATITUDE'],
                'LONGITUDE' => $item['LONGITUDE'],
                'PHONE1'    => $phones[0]['NAME'],
                'PHONE2'    => $phones[1]['NAME'],
                'PHONE3'    => $phones[2]['NAME'],
                'WEBSITE1'  => $websites[0]['NAME'],
                'WEBSITE2'  => $websites[1]['NAME'],
                'WEBSITE3'  => $websites[2]['NAME'],
                'ADDRESS_REGION'    => $addresses['REGION'],
                'ADDRESS_CITY'      => $addresses['CITY'],
                'ADDRESS_STREET'    => $addresses['STREET'],
                'ADDRESS_HOUSE'     => $addresses['HOUSE'],
                'ADDRESS_BUILDING'  => $addresses['BUILDING'],
                'ADDRESS_OFFICE'    => $addresses['OFFICE']
            ];

            $response[] = $temp;
        }

//        $response = $this->db->query("
//            SELECT * FROM
//            (
//                SELECT
//                INSTITUTIONS.NAME,
//                INSTITUTIONS.LOGO,
//                INSTITUTIONS.TYPE_ID,
//                BRANCHES.PHONE1,
//                BRANCHES.PHONE2,
//                BRANCHES.PHONE3,
//                BRANCHES.WEBSITE1,
//                BRANCHES.WEBSITE2,
//                BRANCHES.WEBSITE3,
//                BRANCHES.LATITUDE,
//                BRANCHES.LONGITUDE,
//                BRANCHES.ADDRESS_REGION,
//                BRANCHES.ADDRESS_CITY,
//                BRANCHES.ADDRESS_STREET,
//                BRANCHES.ADDRESS_HOUSE,
//                BRANCHES.ADDRESS_BUILDING,
//                BRANCHES.ADDRESS_OFFICE
//                FROM BRANCHES
//                LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
//            UNION
//                SELECT
//                INSTITUTIONS.NAME,
//                INSTITUTIONS.LOGO,
//                INSTITUTIONS.TYPE_ID,
//                INSTITUTIONS.PHONE1,
//                INSTITUTIONS.PHONE2,
//                INSTITUTIONS.PHONE3,
//                INSTITUTIONS.WEBSITE1,
//                INSTITUTIONS.WEBSITE2,
//                INSTITUTIONS.WEBSITE3,
//                INSTITUTIONS.LATITUDE,
//                INSTITUTIONS.LONGITUDE,
//                INSTITUTIONS.ACTUAL_ADDRESS_REGION,
//                INSTITUTIONS.ACTUAL_ADDRESS_CITY,
//                INSTITUTIONS.ACTUAL_ADDRESS_STREET,
//                INSTITUTIONS.ACTUAL_ADDRESS_HOUSE,
//                INSTITUTIONS.ACTUAL_ADDRESS_BUILDING,
//                INSTITUTIONS.ACTUAL_ADDRESS_OFFICE
//                FROM INSTITUTIONS
//            ) TEMP
//            WHERE TEMP.LATITUDE IS NOT NULL AND TEMP.LONGITUDE IS NOT NULL
//            $where")->result_array();

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    /*
     * entry() - запись к врачу
     */

    public function entry()
    {
        if (!empty($this->user_id)) {
            if (!empty($_POST['entry_date']) && !empty($_POST['entry_time'])) {
                $data = [
                    'USER_ID'       => $this->user_id,
                    'NAME'          => $_POST['name'],
                    'PHONE'         => $_POST['phone'],
                    'DATE'          => $_POST['entry_date'],
                    'TIME'          => $_POST['entry_time'],
                    'COMMENT'       => $_POST['comment'],
                    'CREATED_AT'    => date('Y-m-d')
                ];

                $this->db->insert('ENTRIES_TO_THE_DOCTOR', $data);

                $user=$this->User_model->get_by_id($this->user_id);
                $fields = [
                    'ID_SENDER'     => $this->user_id,
                    'MESSAGE'       => 'Запись на прием к врачу. Имя: '.(!empty($_POST['name']) ? $_POST['name'] : "").
                    " Телефон: ".(!empty($_POST['phone']) ? $_POST['phone'] : "").
                    " Дата записи: ".(!empty($_POST['entry_date']) ? $_POST['entry_date'] : "").
                    " Время записи: ".(!empty($_POST['entry_time']) ? $_POST['entry_time'] : "").
                    " Комментарии: ".(!empty($_POST['comment']) ? $_POST['comment'] : ""),
                    'TIME'          =>date("Y-m-d H:i:s",time()),
                    'THEME'         =>'Запись на прием',
                    'DELETED'       =>0,
                    'FINAL_DELETED' =>0
                ];/*в зависимости от типа пользователя в письме он отображается как юзер, учреждение или врач*/
                if($user['TYPE']==3)
                {
                    $institutionThis=$this->Institution_model->get_by_user_id($this->user_id);
                    $fields['NAME_SENDER']=$institutionThis['NAME'];
                }
                elseif($user['TYPE']==2)
                {
                    $specialistThis=$this->Doctor_model->get_by_user_id($this->user_id);
                    $fields['NAME_SENDER']=$specialistThis['LAST_NAME'].' '.$specialistThis['FIRST_NAME'];
                }
                else
                    $fields['NAME_SENDER']=$user['LASTNAME'].' '.$user['FIRSTNAME'];
                if(!empty($_POST['responded_post_id']))
                    $fields['ID_RESPONDED_POST']=$_POST['responded_post_id'];
                $data['user']=$user;
                $data['fields']=$fields;

                $this->load->model('Post_model');
                $this->load->model('Post_destinations_model');
                $insert_post_id = $this->Post_model->create($data['fields']);

                $fieldsDestination = [
                    'ID_POST'            =>$insert_post_id,
                    'DELETED'            =>0,
                    'FINAL_DELETED'      =>0,
                    'DESTINATION_READED' =>0,
                    'TO_ADMIN'           =>1,
                    'ID_DESTINATION'     => NULL
                ];

                $insert_destination_id = $this->Post_destinations_model->create($fieldsDestination);

                $response = $response = [
                    'status'    => 'success'
                ];
            } else {
                $response = [
                    'status'    => 'error',
                    'error'     => 'Для записи к врачу, необходимо указать время и дату предполагаемого посещения.'
                ];
            }
        } else {
            $response = [
                'status'    => 'error',
                'error'     => 'Вы не авторизованы. Авторизуйтесь, чтобы записаться к врачу.'
            ];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function add_favorites()
    {
        $this->load->model('Liked_model');

        switch ($_POST['type']) {
            case "organisations" :
                $type_name = 'LIKED_INSTITUTIONS';
                $field_name = 'INSTITUTION_ID';
                $delete_func = function ($identifier) {
                    return $this->Liked_model->deleteInstitutionByObject($identifier, $this->user_id);
                };
                $insert_func = function ($object_id) use ($type_name, $field_name) {
                    $data = [
                        $field_name     => (int)$object_id,
                        'user_id'       => $this->user_id
                    ];
                    $this->db->insert($type_name, $data);
                };
                $check_object = function ($identifier) {
                    $object = $this->db->query("SELECT * FROM ORGANIZATIONS WHERE ID = $identifier")->result_array();
                    if (!empty($object))
                        return true;
                    else
                        return false;
                };
                break;
            case "specialists" :
                $type_name = 'LIKED_SPECIALISTS';
                $field_name = 'SPECIALIST_ID';
                $delete_func = function ($identifier) {
                    return $this->Liked_model->deleteSpecialistByObject($identifier, $this->user_id);
                };
                $insert_func = function ($object_id) use ($type_name, $field_name) {
                    $data = [
                        $field_name     => (int)$object_id,
                        'user_id'       => $this->user_id
                    ];
                    $this->db->insert($type_name, $data);
                };
                $check_object = function ($identifier) {
                    $object = $this->db->query("SELECT * FROM SPECIALISTS WHERE ID = $identifier")->result_array();
                    if (!empty($object))
                        return true;
                    else
                        return false;
                };
                break;
            case "medicines" :
                $type_name = 'LIKED_PREPS';
                $field_name = 'PREPS_ID';
                $delete_func = function ($identifier) {
                    return $this->Liked_model->deletePrepByObject($identifier, $this->user_id);
                };
                $insert_func = function ($object_id) use ($type_name, $field_name) {
                    $data = [
                        $field_name     => (int)$object_id,
                        'user_id'       => $this->user_id
                    ];
                    $this->db->insert($type_name, $data);
                };
                $check_object = function ($identifier) {
                    $object = $this->db->query("SELECT * FROM NOMEN WHERE ID = $identifier")->result_array();
                    if (!empty($object))
                        return true;
                    else
                        return false;
                };
                break;
            case "articles" :
                $type_name = 'LIKED_ARTICLES';
                $field_name = 'ARTICLES_ID';
                $delete_func = function ($identifier) {
                    return $this->Liked_model->deleteArticleByObject($identifier, $this->user_id);
                };
                $insert_func = function ($object_id) use ($type_name, $field_name) {
                    $data = [
                        $field_name     => (int)$object_id,
                        'user_id'       => $this->user_id
                    ];
                    $this->db->insert($type_name, $data);
                };
                $check_object = function ($identifier) {
                    $object = $this->db->query("SELECT * FROM ARTICLES WHERE ID = $identifier")->result_array();
                    if (!empty($object))
                        return true;
                    else
                        return false;
                };
                break;
            default :
                $type_name = null;
                $field_name = null;
        }

        $id = (!empty($_POST['id'])) ? (int)$_POST['id'] : null;

        if (!empty($this->user_id)) {
            if (!empty($type_name) && !empty($field_name)) {
                if (!empty($id)) {
                    if ($check_object($id)) {
                        $liked = $this->Liked_model->get_by_liked($this->user_id, $type_name, " AND $field_name = $id");

                        if (!empty($liked)) {
                            $delete_func($id);

                            $response = [
                                'status'        => 'success',
                                'message'       => 'Удалено из избранного',
                                'button_value'  => 'Добавить в избранное'
                            ];
                        } else {
                            $insert_func($id);

                            $response = [
                                'status'        => 'success',
                                'message'       => 'Добавлено в избранное',
                                'button_value'  => 'Удалить из избранного'
                            ];
                        }
                    } else {
                        $response = [
                            'status'    => 'error',
                            'error'     => 'Невозможно добавить объект в избранное. Данного объекта не существует'
                        ];
                    }
                } else {
                    $response = [
                        'status'    => 'error',
                        'error'     => 'Не указан идентификатор добавляемого объекта'
                    ];
                }
            } else {
                $response = [
                    'status'    => 'error',
                    'error'     => 'Не верно указан тип избранного.'
                ];
            }
        } else {
            $response = [
                'status'    => 'error',
                'error'     => 'Вы не авторизованы. Авторизуйтесь, чтобы добавлять и просматривать избранное'
            ];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function rate()
    {
        $errors = [];

        if (empty($_POST['object']))
            $errors[] = "Не выбран объект";
        if (empty($_POST['id']))
            $errors[] = "Не выбран объект";
        if (empty($_POST['date']))
            $errors[] = "Не указана дата";
        if (empty($_POST['place_rate']))
            $errors[] = "Оцените место приема";
        if (empty($_POST['price_rate']))
            $errors[] = "Оцените соотношение цена/качество";
        if (empty($_POST['competence_rate']))
            $errors[] = "Оцените компетенцию специалиста";
        if (empty($_POST['care_rate']))
            $errors[] = "Оцените внимание специалиста";
        if (empty($this->user_id))
            $errors[] = "Авторизуйтесь чтобы оставить отзыв";

        if (empty($errors)) {
            if (!preg_match("/([0-2]\d|3[01])-(0\d|1[012])-(\d{4})/", $_POST['date'])) {
                $errors[] = "Не верный формат даты";
            } else {
                $temp_date = explode('-',$_POST['date']);

                $day = (int)$temp_date[0];
                $month = (int)$temp_date[1];
                $year = (int)$temp_date[2];

                if ( ($day < 0 || $day > 31) || ($month < 1 || $month > 12) || ($year < 2016 || $year > (int)date('Y'))) {
                    $errors[] = "Не верный формат даты";
                } else {
                    $fields = [
                        'OBJECT'    => (int)$_POST['object'],
                        'LOADER_ID' => (int)$_POST['id'],
                        'USER_ID'   => (int)$this->user_id,
                        'DATE'      => implode('-', array_reverse(explode('-', $_POST['date']))),
                        'PLACE_RATE'=> (int)$_POST['place_rate'],
                        'PRICE_RATE'=> (int)$_POST['price_rate'],
                        'COMPETENCE_RATE'   => (int)$_POST['competence_rate'],
                        'CARE_RATE' => (int)$_POST['care_rate']
                    ];

                    $feedback_id = $this->db->insert('FEEDBACKS', $fields);
                    $feedbackid=$this->db->insert_id();
                }

                if ($feedback_id) {
                    if (!empty($this->user_id)) {
                        $user=$this->User_model->get_by_id($this->user_id);

                        $user = $this->db->limit(1)->where('ID', $this->user_id)->get('USERS')->row_array();
                        $name = implode(' ',array_filter([$user['LASTNAME'],$user['FIRSTNAME'],$user['MIDDLENAME']]));
                        if ($fields['OBJECT'] == 1) {
                            $object_type = 'об организации';
                            $organisation = $this->db->where('ID', $fields['LOADER_ID'])->limit(1)->get('ORGANIZATIONS')->row_array();
                            $object_name = '('.$organisation['ID'].')'.$organisation['ENTITY'].' '.$organisation['BRAND'];
                        } elseif ($fields['OBJECT'] == 3) {
                            $object_type = 'о специалисте';
                            $specialist = $this->db->where('ID', $fields['LOADER_ID'])->limit(1)->get('SPECIALISTS')->row_array();
                            $object_name = implode(' ', array_filter([$specialist['SECOND_NAME'],$specialist['FIRST_NAME'],$specialist['LAST_NAME']]));
                        }

                        $fields = [
                            'ID_SENDER'     =>$this->user_id,
                            'MESSAGE'       =>"Пользователь $name оставил отзыв $object_type $object_name
                                               Дата посещения: ".$_POST['date']."
                                               Оценки:
                                               - место приема : ".$_POST['place_rate']."
                                               - соотношение цена/качество : ".$_POST['price_rate']."
                                               - компетенция специалиста : ".$_POST['competence_rate']."
                                               - внимание специалиста : ".$_POST['care_rate']."",
                            'TIME'          =>date("Y-m-d H:i:s",time()),
                            'THEME'         =>'Отзыв, организация',
                            'DELETED'       =>0,
                            'IDFEEDBACK'       => $feedbackid,
                            'ID_COMPANY'       =>(int)$_POST['id'],
                            'FINAL_DELETED' =>0
                        ];/*в зависимости от типа пользователя в письме он отображается как юзер, учреждение или врач*/
                        if($user['TYPE']==3)
                        {
                            $institutionThis = $this->db->where('USER_ID', $this->user_id)->get('ORGANIZATIONS')->row_array();
                            $fields['NAME_SENDER'] = $institutionThis['BRAND'];
                        }
                        elseif($user['TYPE']==2)
                        {
                            $this->load->model('Doctor_model');
                            $specialistThis=$this->Doctor_model->get_by_user_id($this->user_id);
                            $fields['NAME_SENDER']=$specialistThis['LAST_NAME'].' '.$specialistThis['FIRST_NAME'];
                        }
                        else
                            $fields['NAME_SENDER']=$user['LASTNAME'].' '.$user['FIRSTNAME'];
                        if(!empty($_POST['responded_post_id']))
                            $fields['ID_RESPONDED_POST']=$_POST['responded_post_id'];
                        $data['user']=$user;
                        $data['fields']=$fields;

                        $this->load->model('Post_model');
                        $this->load->model('Post_destinations_model');
                        $insert_post_id = $this->Post_model->create($data['fields']);

                        $fieldsDestination = [
                            'ID_POST'            =>$insert_post_id,
                            'DELETED'            =>0,
                            'FINAL_DELETED'      =>0,
                            'DESTINATION_READED' =>0,
                            'TO_ADMIN'           =>0,
                            'ID_DESTINATION'     => NULL
                        ];

                        $insert_destination_id = $this->Post_destinations_model->create($fieldsDestination);
                    }
                }
            }
                 $this->load->library('email');
                 $date=date("Y-m-d H:i:s",time());
                 $fields = [
                            'ID_SENDER'     =>$this->user_id,
                            'MESSAGE'       =>"Пользователь $name оставил отзыв $object_type $object_name\nДата посещения: ".$_POST['date']."\nОценки:\n- место приема : ".$_POST['place_rate']."\n- соотношение цена/качество : ".$_POST['price_rate']."\n- компетенция специалиста : ".$_POST['competence_rate']."\n- внимание специалиста : ".$_POST['care_rate']."\n",
                            'TIME'          =>"$date\n",
                            'THEME'         =>'Отзыв, организация',
                            'DELETED'       =>0,
                            'FINAL_DELETED' =>0
                        ];
                 if(isset($_POST['controller'])){      
                 if($_POST['controller']=='organisations'){       
                  $this->load->model('New_organisations_model');      
                 $organisations = $this->New_organisations_model->get_item(['o.ID' => (int)$_POST['id']]);
                 
                         $user = $this->db->get_where("USERS", ['ID' => $organisations['USER_ID']], 1, 0)->row_array();
               // ECHO $organisations['USER_ID'];        
                $this->email->from('support@mfp.rg3.su', 'Поддержка пользователей МФП');
                $this->email->to($user['EMAIL']);
                $this->email->subject('Отзыв');
                $this->email->set_mailtype("text");                 
                $this->email->message($fields['MESSAGE'].$fields['TIME'].$fields['THEME']);
                $this->email->send();
                 }
                 }
                 else{
                      if(isset($_POST['iduser'])) {
                            
                          $this->data['user'] = $this->db->query("SELECT FIRSTNAME,LASTNAME,MIDDLENAME,EMAIL FROM USERS WHERE ID = ".$_POST['iduser']."")->result_array();
                             
                            
                            $this->email->from('support@mfp.rg3.su', 'Поддержка пользователей МФП');
                $this->email->to($this->data['user'][0]['EMAIL']);
                $this->email->subject('Отзыв');
                $this->email->set_mailtype("text");                 
                $this->email->message('Уважаемый(ая)'.' '.$this->data['user'][0]['LASTNAME'].' '.$this->data['user'][0]['FIRSTNAME'].' '.$this->data['user'][0]['MIDDLENAME'].' '.'у вас появился новый отзыв');
                $this->email->send();
                            
                      }
                   
                     
                 }     
                 
              
        }
          
        if (empty($errors)) {
            $response = [
                'status'    => 'success',
                'message'   => 'Отзыв отправлен на проверку модераторам. Как только отзыв проверят, он станет доступен другим пользователям'
            ];
        } else {
            $response = [
                'status'    => 'error',
                'errors'    => $errors
            ];
        }
          
         // print_r($user['EMAIL']);
    
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function booking()
    {
        $errors = [];

        $id         = (!empty($_POST['id'])) ? (int)$_POST['id'] : null;
        $quantity   = (!empty($_POST['quantity'])) ? (int)$_POST['quantity'] : null;
        $name       = (!empty($_POST['name'])) ? $_POST['name'] : null;
        $phone      = (!empty($_POST['phone'])) ? $_POST['phone'] : null;
        $email      = (!empty($_POST['email'])) ? $_POST['email'] : null;

        if (empty($id))
            $errors[] = 'Не указан товар для бронирования';
        if (empty($quantity))
            $errors[] = 'Не указано количество товара';
        if (empty($name))
            $errors[] = 'Не указано имя';
        if (empty($phone))
            $errors[] = 'Не указан телефон';
        if (empty($email))
            $errors[] = 'Не указана электронная почта';

        if (empty($errors)) {
            $drug = $this->db->get('TRADE_BALANCES', ['ID' => $id], 1, 0)->row_array();

            if (!empty($drug)) {
                if ($drug['COUNT'] >= $quantity) {
                    $reciever = $this->db->query("SELECT
                        u.*,
                        b.NAME ORGANISATION
                        FROM BRANCHES b
                        LEFT JOIN INSTITUTIONS i ON i.ID = b.INSTITUTION_ID
                        LEFT JOIN USERS u ON u.ID = i.USER_ID
                        WHERE b.ID = 14 
                        LIMIT 1")->row_array();

                    if (!empty($reciever)) {
                        $post_data = [
                            'MESSAGE'       => "$name желает забронировать ". $drug['DRUGNAME']. " в количестве $quantity. <br><br> Контактные данные: <br> телефон: $phone <br> e-mail: $email",
                            'THEME'         => "Бронирование лекарства ".$drug['DRUGNAME'],
                            'TIME'          => date('Y-m-d H:i:s'),
                            'NAME_SENDER'   => 'система бронирования МФП',
                            'DELETED'       => 0,
                            'FINAL_DELETED'  => 0
                        ];

                        $this->db->insert('POSTS', $post_data);

                        $post_destinations_data = [
                            'ID_DESTINATION'    => $reciever['ID'],
                            'DESTINATION_NAME'  => $reciever['ORGANISATION'],
                            'ID_POST'           => $this->db->insert_id(),
                            'DELETED'           => 0,
                            'DESTINATION_READED'=> 0,
                            'FINAL_DELETED'     => 0,
                            'TO_ADMIN'          => 0
                        ];

                        $this->db->insert('POST_DESTINATIONS', $post_destinations_data);
                    } else {
                        $errors[] = "Ошибка, мы не можем забронировать товар для этой аптеки";
                    }
                } else {
                    $errors[] = "Количество этого товара на складе меньше чем вы указали";
                }
            } else {
                $errors[] = "Указаного лекарства нет в базе";
            }
        }

        if (empty($errors)) {
            $response = [
                'status'    => 'success',
                'message'   => 'Забронировано'
            ];
        } else {
            $response = [
                'status'    => 'error',
                'errors'    => $errors
            ];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function forgot_password()
    {
        if (!empty($_POST['email'])) {
            $email = $_POST['email'];
            $user = $this->db->get_where("USERS", ['EMAIL' => $email], 1, 0)->row_array();

            if (!empty($user)) {
                $this->load->helper('mfp');
                $this->load->library('email');

                $new_pass = $this->generateRandomString(8);
                $password_hash = encodePassword($new_pass);

                $this->db->set('PASSWORD', $password_hash);
                $this->db->where('ID', $user['ID']);
                $this->db->update('USERS');

                $this->email->from('support@mfp.rg3.su', 'Поддержка пользователей МФП');
                $this->email->to($email);
                $this->email->subject('Восстановление пароля');
                $this->email->message("Ваш новый пароль: $new_pass");
                $this->email->send();
            } else {
                $errors[] = "Пользователя с такой почтой не зарегистрирован в системе";
            }
        } else {
            $errors[] = "Для восстановления пароля необходимо указать почту";
        }

        if (empty($errors)) {
            $response = [
                'status'    => 'success',
                'message'   => 'Новый пароль был выслан на почту'
            ];
        } else {
            $response = [
                'status'    => 'error',
                'errors'    => $errors
            ];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function inst_entry()
    {
        if (!empty($this->user_id)) {

            if (!empty($_POST['specialization']) && !empty($_POST['name']) && !empty($_POST['tel'])) {

                $user=$this->User_model->get_by_id($this->user_id);
                $fields = [
                    'ID_SENDER'     => $this->user_id,
                    'MESSAGE'       => 'Запись на прием к врачу. Имя: '.(!empty($_POST['name']) ? $_POST['name']."." : "").
                        " Телефон: ".(!empty($_POST['tel']) ? $_POST['tel']."." : "").
                        " Эл.почта: ".(!empty($_POST['email']) ? $_POST['email']."." : "").
                        " Желаемый специалист: ".(!empty($_POST['specialization']) ? $_POST['specialization']."." : "").
                        " Комментарии: ".(!empty($_POST['msg']) ? $_POST['msg']."." : ""),
                    'TIME'          =>date("Y-m-d H:i:s",time()),
                    'THEME'         =>'Запись на прием',
                    'DELETED'       =>0,
                    'FINAL_DELETED' =>0
                ];/*в зависимости от типа пользователя в письме он отображается как юзер, учреждение или врач*/
                if($user['TYPE']==3)
                {
                    $institutionThis=$this->db->limit(1)->where('USER_ID', $this->user_id)->get('ORGANIZATIONS')->row_array();
                    $fields['NAME_SENDER']=$institutionThis['BRAND'];
                }
                elseif($user['TYPE']==2)
                {
                    $specialistThis=$this->Doctor_model->get_by_user_id($this->user_id);
                    $fields['NAME_SENDER']=$specialistThis['LAST_NAME'].' '.$specialistThis['FIRST_NAME'];
                }
                else
                    $fields['NAME_SENDER']=$user['LASTNAME'].' '.$user['FIRSTNAME'];
                if(!empty($_POST['responded_post_id']))
                    $fields['ID_RESPONDED_POST']=$_POST['responded_post_id'];
                $data['user']=$user;
                $data['fields']=$fields;

                $this->load->model('Post_model');
                $this->load->model('Post_destinations_model');
                $insert_post_id = $this->Post_model->create($data['fields']);

                $fieldsDestination = [
                    'ID_POST'            =>$insert_post_id,
                    'DELETED'            =>0,
                    'FINAL_DELETED'      =>0,
                    'DESTINATION_READED' =>0,
                    'TO_ADMIN'           =>1,
                    'ID_DESTINATION'     => NULL
                ];

                $insert_destination_id = $this->Post_destinations_model->create($fieldsDestination);

                $response = $response = [
                    'status'    => 'success'
                ];
            } else {
                $response = [
                    'status'    => 'error',
                    'error'     => 'Все поля обязательны для заполнения'
                ];
            }
        } else {
            $response = [
                'status'    => 'error',
                'error'     => 'Вы не авторизованы. Авторизуйтесь, чтобы записаться на прием.'
            ];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function vote()
    {
        if (!empty($_POST['vote_id'])) {
            if (!empty($_POST['option_id'])) {
                if (!empty($this->user_id)) {
                    $answer = $this->db->where('USER_ID', $this->user_id)->where('VOTE_ID', $_POST['vote_id'])->get('VOTING_RESULT')->row_array();

                    if (empty($answer)) {
                        $fields = [
                            'USER_ID'   => $this->user_id,
                            'VOTE_ID'   => $_POST['vote_id'],
                            'OPTION_ID' => $_POST['option_id']
                        ];
                        $this->db->insert('VOTING_RESULT', $fields);
                    }

                    $results = $this->db
                        ->select('VOTING_RESULT.OPTION_ID')
                        ->select('VOTING_OPTION.NAME')
                        ->select('COUNT(OPTION_ID) COUNT')
                        ->where('VOTING_RESULT.VOTE_ID', $_POST['vote_id'])
                        ->group_by('VOTING_RESULT.OPTION_ID')
                        ->join('VOTING_OPTION','VOTING_OPTION.ID = VOTING_RESULT.OPTION_ID', 'left')
                        ->get('VOTING_RESULT')->result_array();

                    $count = $this->db->where('VOTE_ID', $_POST['vote_id'])->count_all_results('VOTING_RESULT');

                    $temp = [];

                    foreach ($results as $result) {
                        $temp[] = '<li>'.round($result['COUNT']/$count*100) .'% - '.$result['NAME'].'</li>';
                    }

                    $response = [
                        'status'    => 'success',
                        'html'      => '<p class="question">Благодарим за ответ!</p>
                                    <ul>
                                        '.(!empty($temp) ? implode('', $temp) : '<li>Нет ответов</li>').'
                                    </ul>'
                    ];
                } else {
                    $response = [
                        'status'    => 'error',
                        'error'     => 'Чтобы проголосовать, войдите в систему'
                    ];
                }
            } else {
                $response = [
                    'status'    => 'error',
                    'error'     => 'Чтобы проголосовать, выберите ответ'
                ];
            }
        } else {
            $response = [
                'status'    => 'error',
                'error'     => 'Неизвестное голосование'
            ];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function new_category()
    {
        if (!empty($_POST['category_name'])) {
            $this->db->insert('ARTICLES_CATEGORY', ['NAME' => $_POST['category_name']]);
        }

        $articles_type = $this->db->order_by('NAME')->get('ARTICLES_CATEGORY')->result_array();

        $html = '<select class="form-control" name="category_id">';

        foreach ($articles_type as $article) {
            $html .= '<option value="'.$article['ID'].'">'.$article['NAME'].'</option>';
        }
        $html .= '</select>';

        $response = [
            'status'   => 'error',
            'html'     => $html
        ];

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }
}