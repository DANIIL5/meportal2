<?php

/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 01.08.2016
 * Time: 13:06
 */
class Search extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->data['controller']   = $this->router->fetch_class();
        $this->data['method']       = $this->router->fetch_method();

        $this->user_id = $this->session->userdata('USER_ID');

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Carousel_model');
        $this->load->model('Action_model');
        $this->load->model('Banner_model');
        $this->load->model('News_model');
        $this->load->model('Institution_model');
        $this->load->model('Articles_model');
        $this->load->model('Geo_model');
        $this->load->model('Logos_model');
        $this->load->model('Post_model');
        $this->load->model('Voting_model');

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $this->data['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $this->data['current_city'] = $cookie_city_id;
        } else {
            $this->data['current_city'] = 4400;
        }

        //Получаем две случайные акции для блока на главной
        $actions['actions'] = $this->Action_model->get_random_list(2, 1);

        foreach ($actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $actions['actions'][$key]['DESCRIPTION'] = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $actions['actions'][$key]['PRICE'] = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        // Получаем акции для вывода на карту
        $map_actions['actions'] = $this->Action_model->get_random_map(4, 1);

        foreach ($map_actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $map_actions['actions'][$key]['DESCRIPTION']    = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $map_actions['actions'][$key]['PRICE']              = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        $carousel = $this->Carousel_model->get_images(1);
        $actions_quantity        = $this->Action_model->count();
        $actions['quantity']     = $actions_quantity;
        $map_actions['quantity'] = $actions_quantity;
        $map_actions['green'] = false;
        $popular_institutions = $this->Institution_model->get_popular();
        $first_banners  = $this->Banner_model->get_banners(1);
        $second_banners = $this->Banner_model->get_banners(1);
        $news = $this->News_model->get_last();
        $map_actions['types'] = $this->Institution_model->get_types();

        // выбираем три рандомные категории
        $popular_themes_category = $this->Articles_model->get_random_themes();

        $popular_themes_category_ids = null;

        foreach ($popular_themes_category as $item) {
            $popular_themes_category_ids[] = $item['ID'];
        }

        $popular_themes = $this->Articles_model->get_articles_by_ids_array($popular_themes_category_ids);

        foreach ($popular_themes as $popular_theme) {
            $popular_themes_data['themes'][$popular_theme['CATEGORY_ID']][] = [
                'ID'        => $popular_theme['ID'],
                'NAME'      => $popular_theme['NAME']
            ];
        }

        $popular_themes_data['category'] = $popular_themes_category;

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);
        $cities['current_city'] = $this->data['current_city'];

        $logos = $this->Logos_model->get_logos();

        $voting = $this->Voting_model->get_vote();
        $voting['is_vote'] = $this->Voting_model->is_vote($voting['info']['ID'], $this->user_id);

        $this->data['templates'] = [
            'city_selector'     => $this->load->view('tmp/city_selector', $cities, true),
            'auth'              => ($this->user_id)
                ? $this->load->view('tmp/auth_entered', [
                    'data' => $this->User_model->get_by_id($this->user_id),
                    'msg_count' => $this->Post_model->get_by_user_id_in_count_unread($this->user_id)[0]['COUNT(*)']
                ], true)
                : $this->load->view('tmp/auth', null, true),
            'carousel'          => $this->load->view('tmp/carousel', ['images' => $carousel], true),
            'menu'              => $this->load->view('tmp/menu', ['controller' => $this->data['controller']], true),
            'search'            => $this->load->view('tmp/search', null, true),
            'ask_question'      => $this->load->view('tmp/ask_question', ['is_auth' => (!empty($this->user_id)) ? true : false], true),
            'actions'           => $this->load->view('tmp/actions', $actions, true),
            'logos'             => $this->load->view('tmp/logos', ['logos' => $logos], true),
            'map_search'        => $this->load->view('tmp/map_search', $map_actions, true),
            'popular_institutions'  => $this->load->view('tmp/popular_institutions', ['institutions' => $popular_institutions], true),
            'first_banners'     => $this->load->view('tmp/banners', ['banners' => $first_banners], true),
            'second_banners'    => $this->load->view('tmp/banners', ['banners' => $second_banners], true),
            'popular_themes'    => $this->load->view('tmp/popular_themes', $popular_themes_data, true),
            'voiting'           => $this->load->view('tmp/voiting', $voting, true),
            'news'              => $this->load->view('tmp/news', ['news' => $news], true),
            'bottom_menu'       => $this->load->view('tmp/bottom_menu', null, true),
            'footer'            => $this->load->view('tmp/footer', ['user' => $this->user_id], true)
        ];

        $this->data['isAuth'] = ($this->user_id) ? true : false;
    }

    public function result()
    {
        $this->data['search_results']['medicines']      = [];
        $this->data['search_results']['drugstores']     = [];
        $this->data['search_results']['organisations']  = [];
        $this->data['search_results']['actions']        = [];
        $this->data['search_results']['articles']       = [];
        $this->data['search_results']['rss_news']       = [];
        $this->data['search_results']['calculators']    = [];
        $this->data['search_results']['ratings']        = [];

        $list_limit = 10;

        $this->data['search_results']['limit'] = $list_limit;

        if (!empty($_GET['query'])) {
            $query = mysqli_real_escape_string($this->db->conn_id, str_replace('®','', $_GET['query']));

            $this->data['search_results']['medicines']['list'] = $this->db->query("
                SELECT
                    SQL_CALC_FOUND_ROWS
                    NOMEN.ID,
    				TRADENAMES.NAME,
    				TRADENAMES.INAME
    				FROM NOMEN
    				LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
    				LEFT JOIN TRADENAMES ON TRADENAMES.ID = PREP.TRADENAMEID
    				WHERE INAME LIKE '%$query%'
                    ORDER BY NAME
                    LIMIT $list_limit
            ")->result_array();
            $count = $this->db->query("SELECT FOUND_ROWS() FOUND")->result_array();
            $this->data['search_results']['medicines']['count'] = $count[0]['FOUND'];

//            $this->data['search_results']['drugstores']['list'] = $this->db->query("
//                SELECT
//                    SQL_CALC_FOUND_ROWS
//                    ID,
//    				NAME
//    				FROM INSTITUTIONS
//    				WHERE
//    				TYPE_ID = 15
//    				AND
//    				INAME LIKE '%$query%' OR DESCRIPTION LIKE '%query%'
//                    ORDER BY NAME
//                    LIMIT $list_limit
//            ")->result_array();
            $count = $this->db->query("SELECT FOUND_ROWS() FOUND")->result_array();
            $this->data['search_results']['drugstores']['count'] = $count[0]['FOUND'];

            $this->data['search_results']['organisations']['list'] = $this->db->query("
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ID,
    				NAME
    				FROM INSTITUTIONS
    				WHERE
    				TYPE_ID != 15
    				AND
    				NAME LIKE '%$query%' OR DESCRIPTION LIKE '%query%'
                    ORDER BY NAME
                    LIMIT $list_limit
            ")->result_array();
            $count = $this->db->query("SELECT FOUND_ROWS() FOUND")->result_array();
            $this->data['search_results']['organisations']['count'] = $count[0]['FOUND'];

            $this->data['search_results']['actions']['list'] = $this->db->query("
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ID,
    				NAME,
    				DESCRIPTION
    				FROM ACTIONS
    				WHERE
    				NAME LIKE '%$query%' OR DESCRIPTION LIKE '%query%'
                    ORDER BY NAME
                    LIMIT $list_limit
            ")->result_array();
            $count = $this->db->query("SELECT FOUND_ROWS() FOUND")->result_array();
            $this->data['search_results']['actions']['count'] = $count[0]['FOUND'];

            $this->data['search_results']['articles']['list'] = $this->db->query("
                SELECT
                    SQL_CALC_FOUND_ROWS
                    ID,
    				NAME,
    				TEXT
    				FROM ARTICLES
    				WHERE
    				NAME LIKE '%$query%'
    				  OR TEXT LIKE '%query%'
                    ORDER BY NAME
                    LIMIT $list_limit
            ")->result_array();
            $count = $this->db->query("SELECT FOUND_ROWS() FOUND")->result_array();
            $this->data['search_results']['articles']['count'] = $count[0]['FOUND'];

            $like_query = trim(str_replace(['калькуляторы', 'калькулятор'], ['',''], $query));

            $this->data['search_results']['calculators']['list'] = $this->db
                ->from('PAGES')
                ->group_start()
                    ->like('NAME', $like_query)
                    ->or_like('TEXT', $like_query)
                ->group_end()
                ->where('TYPE_ID', 1)
                ->get()
                ->result_array();
            $this->data['search_results']['calculators']['count'] = $this->db
                ->from('PAGES')
                ->group_start()
                ->like('NAME', $like_query)
                ->or_like('TEXT', $like_query)
                ->group_end()
                ->where('TYPE_ID', 1)
                ->count_all_results();

            $like_query = trim(str_replace(['рейтинги', 'рейтинг'], ['',''], $query));

            $this->data['search_results']['ratings']['list'] = $this->db
                ->from('PAGES')
                ->group_start()
                ->like('NAME', $like_query)
                ->or_like('TEXT', $like_query)
                ->group_end()
                ->where('TYPE_ID', 2)
                ->get()
                ->result_array();

            $this->data['search_results']['ratings']['count'] = $this->db
                ->from('PAGES')
                ->group_start()
                ->like('NAME', $like_query)
                ->or_like('TEXT', $like_query)
                ->group_end()
                ->where('TYPE_ID', 2)
                ->count_all_results();


        //     $this->data['search_results']['rss_news']['list'] = $this->db->query("
        //         SELECT
        //             SQL_CALC_FOUND_ROWS
        //             ID,
    				// TITLE
    				// FROM RSS_NEWS
    				// WHERE
    				// TITLE LIKE '%$query%'
    				//   OR DESCRIPTION LIKE '%query%'
        //             ORDER BY NAME
        //             LIMIT $list_limit
        //     ")->result_array();
        //     $count = $this->db->query("SELECT FOUND_ROWS() FOUND")->result_array();
        //     $this->data['search_results']['rss_news']['count'] = $count[0]['FOUND'];
        }

//        print_r($this->data['search_results']);

        $this->load->view('main/search', $this->data);
    }
}