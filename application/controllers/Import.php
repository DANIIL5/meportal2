<?php

class Import extends CI_Controller
{
    public function rss_news()
    {
//        ini_set('error_reporting', E_ALL);
//        ini_set('display_errors', 1);
//        ini_set('display_startup_errors', 1);

        $white_list = array();
        $rss_words  = $this->db->query('SELECT WORD FROM RSS_WORDS')->result();
        foreach ($rss_words as $rss_word) {
            $white_list[] = $rss_word->WORD;
        }

        /*
         * Парсинг новостей от РИА новости
         */
//        $rss        = file_get_contents('http://ria.ru/export/rss2/science/index.xml');
//
//        if ($rss) {
//            $parser     = new SimpleXMLElement($rss, LIBXML_PARSEHUGE);
//
//            foreach ($parser->channel->item as $item) {
//
//                $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));
//
//                foreach ($white_list as $white_word) {
//                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s',$item->title), $white_word) !== false) {
//                        $params = [
//                            'TITLE'         => sprintf('%s',$item->title),
//                            'LINK'          => sprintf('%s',$item->link),
//                            'DESCRIPTION'   => $description,
//                            'DATE'          => date('Y-m-d H:i:s',strtotime(sprintf('%s',$item->pubDate))),
//                            'IMAGE'         => sprintf('%s','/temp/7.jpg')
//                        ];
//
//                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
//                        $this->db->query($sql);
//                        break;
//                    }
//                }
//            }
//        }


        /*
         * Парсинг новостей от ленты новости
         */
        $rss        = file_get_contents('http://lenta.ru/rss/articles');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = strip_tags(sprintf('%s',trim($item->description)));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', $item->enclosure['url'])
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }

        /*
         * Парсинг новостей от ленты.ру
         */
        $rss        = file_get_contents('http://lenta.ru/rss/');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = strip_tags(sprintf('%s',trim($item->description)));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', $item->enclosure['url'])
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }

        /*
         * Парсинг новостей от росздравнадзор
         */
        $rss        = file_get_contents('http://www.roszdravnadzor.ru/rss/');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', '/temp/7.jpg')
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }


        /*
         * Парсинг новостей от медикфорум
         */
        $rss        = file_get_contents('http://www.medikforum.ru/news/rss.xml');

        if ($rss) {
            try {
                $parser     = new SimpleXMLElement($rss);

                foreach ($parser->channel->item as $item) {

                    $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));

                    foreach ($white_list as $white_word) {
                        if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                            $params = [
                                'TITLE' => sprintf('%s', $item->title),
                                'LINK' => sprintf('%s', $item->link),
                                'DESCRIPTION' => $description,
                                'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                                'IMAGE' => sprintf('%s', $item->enclosure['url'])
                            ];

                            $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                            $this->db->query($sql);
                            break;
                        }
                    }
                }
            } catch (Exception $e) {

            }
        }


        /*
         * Парсинг новостей от  новости
         */
        $rss        = file_get_contents('http://www.mskagency.ru/rss/index.rss');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', '/temp/7.jpg')
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }

        /*
         * Парсинг новостей от  новости
         */

        $rss        = file_get_contents('http://www.rg.ru/xml/index.xml');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', $item->enclosure['url'])
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }

        /*
         * Парсинг новостей от  новости
         */

        $rss        = file_get_contents('http://izvestia.ru/xml/rss/all.xml');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', '/temp/7.jpg')
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }

        /*
        * Парсинг новостей от  новости
        */

        $rss        = file_get_contents('http://www.nkj.ru/rss/');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', $item->enclosure['url'])
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }

        /*
        * Парсинг новостей от  новости
        */

        $rss        = file_get_contents('http://elementy.ru/rss/news/medecine');

        if ($rss) {
            $parser     = new SimpleXMLElement($rss);

            foreach ($parser->channel->item as $item) {

                $description    = sprintf('%s',str_replace("\n","",strip_tags(trim($item->description))));

                foreach ($white_list as $white_word) {
                    if (stripos($description, $white_word) !== false || stripos(sprintf('%s', $item->title), $white_word) !== false) {
                        $params = [
                            'TITLE' => sprintf('%s', $item->title),
                            'LINK' => sprintf('%s', $item->link),
                            'DESCRIPTION' => $description,
                            'DATE' => date('Y-m-d H:i:s', strtotime(sprintf('%s', $item->pubDate))),
                            'IMAGE' => sprintf('%s', $item->enclosure['url'])
                        ];

                        $sql = $this->db->insert_string('RSS_NEWS',$params)." ON DUPLICATE KEY UPDATE DATE = DATE";
                        $this->db->query($sql);
                        break;
                    }
                }
            }
        }
    }

    public function trade_balances()
    {
        // Настройки
        ini_set("memory_limit", "2000M");
        set_time_limit(0);
        mb_internal_encoding("UTF-8");

        //Разбираем товарные остатки аптеки в нашем формате
        $source_csv = fopen("/opt/ftp/mfp1/01072016_1313011.csv","r");
        while (($csv_line = fgetcsv($source_csv)) !== FALSE) {
            $parsed_csv[] = $csv_line;
        }
        fclose($source_csv);

        //Загоняем базу РЛС к нам в оперативку
        $this->db->query('SET sql_mode = ""');

        $rls = $this->db->query('SELECT
          NOMEN.ID NOMEN_ID,
          PREP.ID PREP_ID,
          TRADENAMES.NAME NAME,
          TRADENAMES.INAME SEARCH_NAME,
          CLSDRUGFORMS.NAME DRUGDOSE_NAME,
          CLSDRUGFORMS.FULLNAME DRUGDOSE_FULL_NAME,
          FIRMNAMES.NAME FIRM_NAME,
          FIRMS.FULLNAME FIRM_FULL_NAME,
          DRUGPACK.NAME FIRST_PACK_SHORT_NAME,
          DRUGPACK.FULLNAME FIRST_PACK_FULL_NAME,
          NOMEN.DRUGSINPPACK FIRST_PACK_COUNT,
          PREP.DFMASS DF_MASS,
          MASSUNITS.SHORTNAME DF_MASS_SHORT_NAME,
          MASSUNITS.FULLNAME DF_MASS_FULL_NAME,
          NOMEN.PPACKVOLUME PACK_VOLUME,
          CUBICUNITS.SHORTNAME PACK_VOLUME_SHORT_NAME,
          CUBICUNITS.FULLNAME PACK_VOLUME_FULL_NAME,
          PREP.DFCONC DF_CONC,
          CONCENUNITS.SHORTNAME DF_CONC_SHORT_NAME,
          CONCENUNITS.FULLNAME DF_CONC_FULL_NAME,
          PREP.DFACT DF_ACT,
          ACTUNITS.SHORTNAME DF_ACT_SHORT_NAME,
          ACTUNITS.FULLNAME DF_ACT_FULL_NAME
        FROM NOMEN
        LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
        LEFT JOIN TRADENAMES ON PREP.TRADENAMEID = TRADENAMES.ID
        LEFT JOIN CLSDRUGFORMS ON PREP.DRUGFORMID = CLSDRUGFORMS.ID
        LEFT JOIN FIRMS ON PREP.FIRMID = FIRMS.ID
        LEFT JOIN FIRMNAMES ON FIRMS.NAMEID = FIRMNAMES.ID
        LEFT JOIN DRUGPACK  ON NOMEN.PPACKID = DRUGPACK.ID
        LEFT JOIN MASSUNITS ON PREP.DFMASSID = MASSUNITS.ID
        LEFT JOIN CUBICUNITS ON NOMEN.PPACKCUBUNID = CUBICUNITS.ID
        LEFT JOIN CONCENUNITS ON PREP.DFCONCID = CONCENUNITS.ID
        LEFT JOIN ACTUNITS ON PREP.DFACTID = ACTUNITS.ID
		WHERE TRADENAMES.NAME IS NOT NULL
        GROUP BY PREP_ID, NAME, FIRM_FULL_NAME, FIRST_PACK_SHORT_NAME, FIRST_PACK_COUNT, DF_MASS, DF_MASS_SHORT_NAME
        ORDER BY TRADENAMES.NAME')->result();

        foreach ($rls as $nomen) {
            $name_index = mb_substr(strtoupper($nomen->SEARCH_NAME),0,3);

            $rls_array[$name_index][(int)$nomen->NOMEN_ID] = $nomen->SEARCH_NAME.' '
                .((!empty($nomen->FIRST_PACK_COUNT) && (int)$nomen->FIRST_PACK_COUNT != 0) ? '№'.(int)$nomen->FIRST_PACK_COUNT.' ' : '')
                .$nomen->DRUGDOSE_NAME.' '
                .$nomen->DRUGDOSE_FULL_NAME.' '
                .((!empty($nomen->PACK_VOLUME)) ? (float)$nomen->PACK_VOLUME.$nomen->PACK_VOLUME_SHORT_NAME.' '.(float)$nomen->PACK_VOLUME.$nomen->PACK_VOLUME_FULL_NAME : '').' '
                .$nomen->FIRM_NAME .' '
                .$nomen->FIRM_FULL_NAME .' '
                .$nomen->FIRST_PACK_SHORT_NAME.' '
                .$nomen->FIRST_PACK_FULL_NAME.' '
                .((!empty($nomen->DF_MASS)) ? (float)$nomen->DF_MASS.$nomen->DF_MASS_SHORT_NAME.' '.(float)$nomen->DF_MASS.$nomen->DF_MASS_FULL_NAME : '').' ';
        }

        foreach ($parsed_csv as $parsed_line) {
            $variants = array();

            $upper_line = mb_strtoupper($parsed_line[7]);
            $keywords = explode(' ',preg_replace('/\s{2,}/', ' ',str_replace(array(',','/','.'), array(' ',' ',' '), $upper_line)));

            $temporary_key = mb_substr($upper_line,0,3);

            if(!empty($rls_array[$temporary_key])) {
                foreach ($rls_array[$temporary_key] as $rls_key => $rls_line) {
                    foreach ($keywords as $keyword) {
                        $rls_line = mb_strtoupper(strip_tags(str_replace(array(',', '/', '.', '(', ')'), array(' ', ' ', ' ', ' ', ' '), $rls_line)));

                        $template_with_spaces = '/\s' . $keyword . '\s/';
                        $template_with_start = '/^' . $keyword . '\s/';
                        $template_with_end = '/\s' . $keyword . '$/';

                        if (preg_match($template_with_spaces, $rls_line) != false
                            || preg_match($template_with_start, $rls_line) != false
                            || preg_match($template_with_end, $rls_line) != false
                        ) {
                            if (empty($variants[$rls_key]))
                                if ($keyword == $keywords[0] && !is_int($keyword)) {
                                    //                                        echo "За слово $keyword 4 бала".PHP_EOL;
                                    $variants[$rls_key] = 4;
                                } elseif (!is_int($keyword)) {
                                    //                                        echo "За слово $keyword 2 бала".PHP_EOL;
                                    $variants[$rls_key] = 2;
                                } else {
                                    //                                        echo "За слово $keyword 1 бал".PHP_EOL;
                                    $variants[$rls_key] = 1;
                                }
                            else
                                if ($keyword == $keywords[0] && !is_int($keyword)) {
                                    //                                        echo "За слово $keyword 4 бала".PHP_EOL;
                                    $variants[$rls_key] = 4;
                                } elseif (!is_int($keyword)) {
                                    //                                        echo "За слово $keyword 2 бала".PHP_EOL;
                                    $variants[$rls_key] += 2;
                                } else {
                                    //                                        echo "За слово $keyword 1 бал".PHP_EOL;
                                    $variants[$rls_key] += 1;
                                }
                        }
                    }

                    $firm_name = '/' . mb_strtoupper($parsed_line[8]) . '/';
                    if (preg_match($firm_name, $rls_line) != false)
                        if (empty($variants[$rls_key])) {
                            //                                echo "За фирму 5 балов".PHP_EOL;
                            $variants[$rls_key] = 5;
                        } else {
                            //                                echo "За фирму 5 балов".PHP_EOL;
                            $variants[$rls_key] += 5;
                        }
                }
            }

            $drugstore_id = $parsed_line[0];

            $max = 0;
            if (!empty($variants))
                $max = max($variants);

            if($max > 0) {
                $result_keys = array_keys($variants,$max);

                if (count($result_keys) == 1) {
                    foreach ($result_keys as $result_key) {
                        $prep_id = $result_key;
                    }

                    if ($drugstore_id) {
                        $temp_date = explode('.',$parsed_line[11]);
                        $params = [
                            'BRANCH_ID'         => $drugstore_id,
                            'NOMEN_ID'          => $prep_id,
                            'COUNT'             => $parsed_line[9],
                            'PRICE'             => (float)preg_replace('/\p{Zs}/u','',$parsed_line[10]),
                            'EXPIRATION_DATE'   => $temp_date[2]."-".$temp_date[0]."-".$temp_date[1],
                            'DRUGNAME'          => $parsed_line[7],
                            'FIRMNAME'          => $parsed_line[8]
                        ];
//                        $this->db->insert('TRADE_BALANCES',$params);
                        $sql = $this->db->insert_string('TRADE_BALANCES', $params) . ' ON DUPLICATE KEY UPDATE ID=ID';
                        $this->db->query($sql);
                    }
                } else {
                    $temp_date = explode('.',$parsed_line[11]);
                    $params = [
                        'BRANCH_ID'         => $drugstore_id,
                        'DRUGNAME'          => $parsed_line[7],
                        'FIRMNAME'          => $parsed_line[8],
                        'COUNT'             => $parsed_line[9],
                        'PRICE'             => (float)preg_replace('/\p{Zs}/u','',$parsed_line[10]),
                        'EXPIRATION_DATE'   => $temp_date[2]."-".$temp_date[0]."-".$temp_date[1]
                    ];
                    $this->db->insert('BAD_IMPORT',$params);
                }
                //
            } else {
                $temp_date = explode('.',$parsed_line[11]);
                $params = [
                    'BRANCH_ID'         => $drugstore_id,
                    'DRUGNAME'          => $parsed_line[7],
                    'FIRMNAME'          => $parsed_line[8],
                    'COUNT'             => $parsed_line[9],
                    'PRICE'             => (float)preg_replace('/\p{Zs}/u','',$parsed_line[10]),
                    'EXPIRATION_DATE'   => $temp_date[2]."-".$temp_date[0]."-".$temp_date[1]
                ];
                $this->db->insert('BAD_IMPORT',$params);
            }
        }
    }

    public function rls_xml_to_mysql()
    {
        $rls_path 	= "/var/www/vhosts/mfp.rg3.su/rls/";

        $this->db->save_queries = false;

        $reserved_rows = [
            'ID'            => 1,
            'NAME'          => 0,
            'FULLNAME'      => 0,
            'SHORTNAME'     => 0,
            'PARENTID'      => 1,
            'CODE'          => 0,
            'INFO'          => 0,
            'DRUGFORMID'    => 1,
            'LATNAME'       => 0,
            'LATFULLNAME'   => 0,
            'LATPARENTNAME' => 0,
            'LINKID'        => 1,
            'OKPDID'        => 1,
            'PREPID'        => 1,
            'NTFR_ID'       => 1,
            'OKDP'          => 1,
            'DESCRIPTION'   => 0,
            'PHG_ID'        => 1,
            'SYMBOLS'       => 0,
            'INAME'         => 0,
            'TEXT'          => 0,
            'LIFETIME'      => 2,
            'RUSNAME'       => 0,
            'STRONGGROUPID' => 1,
            'NARCOGROUPID'  => 1,
            'CHEMICALNAME'  => 0,
            'CHARACTERS'    => 0,
            'PHARMACOLOGY'  => 0,
            'INDICATIONS'   => 0,
            'USAGE'         => 0,
            'CONTRAINDICATIONS' => 0,
            'USELIMITATIONS'    => 0,
            'SIDEACTIONS'   => 0,
            'INTERACTIONS'  => 0,
            'USEMETHODANDDOSES' => 0,
            'OVERDOSE'      => 0,
            'PREGNANCYUSE'  => 0,
            'PRECAUTIONS'   => 0,
            'SPECIALGUIDELINES' => 0,
            'LITERATURE'    => 0,
            'LATNAME_PARENT'    => 0,
            'NAMEID'        => 1,
            'COUNTID'       => 1,
            'ADRMAIN'       => 0,
            'ADRRUSSIA'     => 0,
            'ADRUSSR'       => 0,
            'FPREPNAME'     => 0,
            'FIRMID'        => 1,
            'DESCID'        => 1,
            'COMPOSITION'   => 0,
            'DRUGFORMDESCR' => 0,
            'PHARMAACTIONS' => 0,
            'ACTONORG'      => 0,
            'COMPONENTSPROPERTIES'  => 0,
            'PHARMAKINETIC' => 0,
            'PHARMADYNAMIC' => 0,
            'CLINICALPHARMACOLOGY'  => 0,
            'DIRECTION'     => 0,
            'RECOMMENDATIONS'   => 0,
            'INSTRFORPAC'   => 0,
            'MANUFACTURER'  => 0 ,
            'COMMENT'       => 0,
            'APPLY'         => 0,
            'COMPLECTATION' => 0,
            'PRINCIPLE'     => 0,
            'MAINTECHCHARS' => 0,
            'SPECIFICATION' => 0,
            'SERVICE'       => 0,
            'OBSERVATION'   => 0,
            'COMPOSITION_DF'    => 0,
            'APTEKA_CONDITION'  => 0,
            'FORM'          => 0,
            'TRADENAMEID'   => 1,
            'LATINNAMEID'   => 1,
            'REGNUM'        => 0,
            'REGDATE'       => 3,
            'ENDDATE'       => 3,
            'STATUSID'      => 1,
            'DFMASS'        => 2,
            'DFMASSID'      => 1,
            'DFCONC'        => 2,
            'DFCONCID'      => 1,
            'DFACT'         => 2,
            'DFACTID'       => 1,
            'DFSIZE'        => 2,
            'DFSIZEID'      => 1,
            'DFCHARID'      => 1,
            'DRUGDOSE'      => 1,
            'NORECIPE'      => 0,
            'LISTTYPE'      => 0,
            'REGEND'        => 0,
            'REGCERTID'     => 1,
            'NTFRID'        => 1,
            'PPACKID'       => 1,
            'DRUGSINPPACK'  => 1,
            'PPACKMASS'     => 2,
            'PPACKMASSUNID' => 1,
            'PPACKVOLUME'   => 2,
            'PPACKCUBUNID'  => 1,
            'SETID'         => 1,
            'UPACKID'       => 1,
            'PPACKINUPACK'  => 1,
            'INANGRO'       => 0,
            'EANCODE'       => 0,
            'PRICEINRUB'    => 2,
            'PRICEORDER'    => 0,
            'PRICEINCURR'   => 2,
            'CURRID'        => 1,
            'CONDID'        => 1,
            'LIFEID'        => 1,
            'DRUGSTORCOND'  => 0,
            'DRUGLIFETIME'  => 0,
            'SPACKID'       => 1,
            'UPACKINSPACK'  => 1,
            'PRICEDATE'     => 0,
            'NOMENID'       => 1,
            'NUMBER'        => 1,
            'UNIQID'        => 1,
            'PHGRID'        => 1,
            'CERTID'        => 1,
            'MATTERID'      => 1,
            'PHACTID'       => 1,
            'LIMP_PHGR_ID'  => 1,
            'CLSATC_ID'     => 1,
            'MZ_PHGR_ID'    => 1,
            'ACTMATTERID'   => 1,
            'SYNON_ID'      => 1,
            'IIC_ID'        => 1,
            'IWID'          => 1,
            'DRUGID'        => 1,
            'PACKNX'        => 1,
            'SERNUM'        => 0,
            'ENDDATE'       => 3
        ];

        $files 	= scandir($rls_path);

        foreach ($files as $file) {
            if ($file != "." && $file != ".." && $file != "dtproperties.xml") {

                $fileinfo = explode(".", $file);
                $filename = $fileinfo[0];
                $fileext  = $fileinfo[1];

                $this->db->query("SET FOREIGN_KEY_CHECKS=0");
                $this->db->query("TRUNCATE $filename");
                $this->db->query("ALTER TABLE $filename ENGINE = InnoDB ROW_FORMAT = Dynamic;");
                $this->db->query("SET FOREIGN_KEY_CHECKS=1");

                $xmlURL = $rls_path.$filename.".".$fileext;

                $xml = simplexml_load_file($xmlURL);

                foreach ($xml->row as $row) {

                    $temp_array = [];

                    foreach ($row as $key => $value) {
                        if (isset($reserved_rows[$key])) {
                            if ($reserved_rows[$key] == 0) {
                                $value = (string)$value;
                            } elseif ($reserved_rows[$key] == 1) {
                                $value = (int)$value;
                            } elseif ($reserved_rows[$key] == 2) {
                                $value = (float)$value;
                            } elseif ($reserved_rows[$key] == 3) {
                                $value = (string)$value;
                                $value = date('Y-m-d H:i:s',strtotime($value));
                            }

                            $temp_array[$key] = $value;
                        }
                    }

                    if (!empty($temp_array['ID'])) {
                        $check = $this->db->query("SELECT * FROM $filename WHERE ID = ".$temp_array['ID'])->result_array();

                        if (empty($check)) {
                            $this->db->insert($filename, $temp_array);
                        }
                    }
                }
            }
        }
    }
}