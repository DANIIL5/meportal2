<?php

class Cp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit','1400M');
        $this->load->library('pagination');
        $this->load->helper('mfp');
        $this->load->model('Institution_model');
        $this->load->model('Branches_model');
        $this->load->model('Branch_metros_model');
        $this->load->model('Institution_metros_model');
        $this->load->model('Institution_departments_model');
        $this->load->model('Branch_departments_model');
        $this->load->model('Doctor_model');
        $this->load->model('Patients_model');
        $this->load->model('Post_files_model');
        $this->load->model('Post_model');
        $this->load->model('Address_book_model');
        $this->load->model('Post_destinations_model');
        $this->load->model('User_model');

        $message_count =  $this->Post_model->get_by_user_id_in_count_unread(null);
        $message_count2 =  $this->Post_model->get_by_user_id_in_count_unread2(null);
       $this->unreaded = $message_count[0]['COUNT(*)'];
        $this->unreaded2 = $message_count2[0]['COUNT(*)'];
        //print_r($this->unreaded2);

    }

    public function index()
    {   
   $pass = encodePassword('123456');
    //$pass='5cfa3e5cb9d89745e5fb52e5b4bc9fc053a750be39f34d489595076c0c9b6ffb';
    $password='123456';
     if(checkPassword($password, $pass)){
     //    echo 'true';
     }else{
         
        //  echo 'false';
     }
    // echo encodePassword('848451');     
          
      
        if ($this->session->userdata('ADMIN_ID'))
            redirect('/cp/articles');

        $data = array();

        if (isset($_POST['submit'])) {
        $result=$this->db->query("SELECT * FROM ADMINS WHERE NAME = \"".mysqli_real_escape_string($this->db->conn_id, $_POST['login'])."\"")->row_array();
       //print_r($result);
       echo checkPassword($_POST['password'], $result['PASSWORD']);
        //exit();
            if (!empty($_POST['login']) && !empty($_POST['password']) && $_POST['login'] == $result['NAME'] && checkPassword($_POST['password'], $result['PASSWORD'])) {
                $this->session->set_userdata(['ADMIN_ID' => 1,'role' =>$result['RIGHTS'],'idadmin'  => $result['ID']]);
                redirect('/cp/articles');
            } else {
                $data['error'] = "Не верно введен логин или пароль";
            }
        }

        $data['unreaded'] = $this->unreaded;
        $this->load->view('cp/auth', $data);
    }

    public function admins()
    {   
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $data['admins'] = $this->db->query("SELECT * FROM ADMINS")->result_array();

        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;
        $this->load->view('cp/admins', $data);
    }

    public function admins_add()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        if (isset($_POST['submit'])) {
            if (empty($_POST['name']))
                $errors[] = "Не указан логин администратора";
            if (empty($_POST['password']))
                $errors[] = "Поле \"Пароль\" не заполнено";
            elseif (strlen($password = $_POST['password']) < 6)
                $errors[] = "Длина пароля должна быть 6 и более символов";
            elseif (empty($_POST['confirmation']))
                $errors[] = "Пароль не подтвержден";
            elseif ($password !== $confirmation = $_POST['confirmation'])
                $errors[] = "Введенные пароли не совпадают";
            elseif (empty($_POST['email']))
                $errors[] = "поле email не может быть пустым";
                elseif (empty($_POST['fio']))
                $errors[] = "поле fio не может быть пустым";
            if (empty($errors)) {
                $rights = "";
                if (!empty($_POST['rights']) && is_array($_POST['rights']))
                    $rights = implode(',',$_POST['rights']);
                $password_hash = encodePassword($password);
                $fields = [
                    'NAME'          => $_POST['name'],
                    'PASSWORD'      => $password_hash,
                    'RIGHTS'        => $rights,
                    'EMAIL'        => $_POST['email'],
                    'FIO'        => $_POST['fio']
                ];

                if($this->db->query("SELECT * FROM ADMINS WHERE NAME = \"".mysqli_real_escape_string($this->db->conn_id, $_POST['name'])."\"")->row_array() == null) {
                    $this->db->insert('ADMINS',$fields);
                } else
                    $errors[] = "Пользователь с такой эл.почтой уже зарегистрирован";
            }
            
            redirect('/cp/admins');
        }

        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;
        $this->load->view('cp/admins_add', $data);
    }

    public function admins_edit($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $data = [];

        $id = (int)$id;
        //print_r($_POST);
        if (!empty($id)) {
            if (isset($_POST['submit'])) {
                if (empty($_POST['name']))
                    $errors[] = "Не указан логин администратора";
                if (!empty($_POST['password'])){
                        $errors[] = "Поле \"Пароль\" не заполнено";
                if (strlen($password = $_POST['password']) < 6)
                    $errors[] = "Длина пароля должна быть 6 и более символов";
                elseif (empty($_POST['confirmation']))
                    $errors[] = "Пароль не подтвержден";
                elseif ($password !== $confirmation = $_POST['confirmation'])
                    $errors[] = "Введенные пароли не совпадают";
                    
                }
                
                     elseif (empty($_POST['email']))
                $errors[] = "поле email не может быть пустым";
                elseif (empty($_POST['fio']))
                $errors[] = "поле fio не может быть пустым";
                if (empty($errors)) {
                    
                    $rights = "";
                    if (!empty($_POST['rights']) &&  is_array($_POST['rights']))
                        $rights = implode(',',$_POST['rights']);
                    $password_hash = encodePassword($password);
                    $fields = [
                        'NAME'          => $_POST['name'],
                        'PASSWORD'      => $password_hash,
                        'RIGHTS'        => $rights,
                        'EMAIL'        => $_POST['email'],
                        'FIO'        => $_POST['fio']
                    ];

                    $this->db->where('ID', $id);
                    $this->db->update('ADMINS',$fields);
                }
               // unset($_SESSION);
                //SetCookie("ci_session","");
                //setcookie('ci_session', '', 0, '/');
                redirect('/cp/');
            }

            $data['admin'] = $this->db->query("SELECT * FROM ADMINS WHERE ID = $id")->row_array();

            $rights = explode(',', $data['admin']['RIGHTS']);

            foreach ($rights as $right) {
                $temp_rights[$right] = 1;
            }

            $data['admin']['RIGHTS'] = $temp_rights;
        }

        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;
        $this->load->view('cp/admins_edit', $data);
    }
    public function exits(){
     unset($_SESSION);
                //SetCookie("ci_session","");
    setcookie('ci_session', '', 0, '/');   
        redirect('/cp/');
        
    }
    public function desktop()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $data['unreaded'] = $this->unreaded;
        $this->load->view('cp/index', $data);
    }

    public function articles()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
       
        if(isset($_SESSION['role'])){
          
           $role = explode(",",  $_SESSION['role']);
           //print_r($role);
       
        foreach ($role as $key=>$value){
            if($value=='content' || $value=='admins' ){
              $accses=true;  
            }
            //print_r($accses);
            //unset($_SESSION['role']);
        }
        }
        if(isset($accses) && $accses==true){
            
            
            
        
        $per_page = 25;
        $where = "";
        $where_array = array();
        $pages = 0;
        $offset = "";

        if (!empty($_GET['category_id'])) {

            $_GET['category_id'] = (int)$_GET['category_id'];

            if (!empty($_GET['category_id'])) {
                $where_array[] = "( ARTICLES.CATEGORY_ID = ".$_GET['category_id']." )";
            }
        }

        if (!empty($where_array))
            $where = "WHERE ".implode(' AND ', $where_array);

        if (!empty($_GET['pages']))
            $pages = (int)$_GET['pages'] * $per_page;


        $limit = "LIMIT $per_page";
        $offset = "OFFSET $pages";
        $query = "SELECT * FROM ARTICLES $where ORDER BY ID $limit $offset";

        $data['catalog']  = $this->db->query("SELECT * FROM ARTICLES $where ORDER BY ID $limit $offset")->result_array();
        $catalog_quantity = $this->db->query("SELECT COUNT(0) COUNT FROM ARTICLES $where")->row_array()['COUNT'];
        $data['filters']['categories'] = $this->db->query("SELECT * FROM ARTICLES_CATEGORY ORDER BY NAME")->result_array();
        //        $data['url_with_params'] = $_SERVER['PATH_INFO']."?";

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;
        $this->load->view('/cp/articles', $data);
        }
        else{
            echo 'вы не контент менеджер,администратор,через 5 секунд вы будете пернаправлены на главную страницу';
            header( 'Refresh: 5; url=/cp/specialists/' );
        }
    }

    public function new_organizations()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $this->load->model('New_organisations_model');

        $per_page   = 25;
        $pages      = 0;
        $filter     = [];

        if (!empty($_GET['pages']))
            $pages = (((int)$_GET['pages'])-1)*$per_page;

        $data['catalog'] = $this->New_organisations_model->get_list($filter, [], $per_page, $pages, true);
        $data['catalog_count'] = (int)$this->New_organisations_model->get_count($filter, [], true);

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $data['catalog_count'];
        $config['per_page'] = $per_page;
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['next_link'] = FALSE;
        $config['prev_link'] = FALSE;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<ul class="uk-pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="uk-active"><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $layout['content'] = $this->load->view('/admin/organisations_catalog', $data, true);
         
        $this->load->view('/admin/layout', $layout);
    }

    public function new_organizations_create($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $data = [
            'parent_id'     => $id,
            'types'         => $this->db->get('INSTITUTION_TYPES')->result_array(),
            'metro_list'    => $this->db->order_by('NAME','ASC')->get('METRO')->result_array(),
            'users'         => $this->db->query("SELECT * FROM USERS ORDER BY EMAIL")->result_array()
        ];

        $layout['content'] = $this->load->view('/admin/organisations_edit', $data, true);

        $this->load->view('/admin/layout', $layout);
    }

    public function new_organizations_edit($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $work_time_raw = $this->db->get_where('ORGANIZATION_WORKING_TIME', ['ORGANIZATION_ID' => $id], 7, 0)->result_array();

        $work_time = [];

        if (empty($id))
            show_404();

        foreach ($work_time_raw as $raw) {
            $work_time[$raw['DAY_ID']] = $raw;
        }

        $data = [
            'id'                => $id,
            'types'             => $this->db->get('INSTITUTION_TYPES')->result_array(),
            'organization'      => $this->db->get_where('ORGANIZATIONS', ['ID' => $id], 1, 0)->row_array(),
            'legal_address'     => $this->db->get_where('ORGANIZATION_ADDRESSES', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 1], 1, 0)->row_array(),
            'actual_address'    => $this->db->get_where('ORGANIZATION_ADDRESSES', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 2], 1, 0)->row_array(),
            'phone1'            => $this->db->get_where('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 2], 1, 0)->row_array(),
            'phone2'            => $this->db->get_where('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 2], 1, 1)->row_array(),
            'phone3'            => $this->db->get_where('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 2], 1, 2)->row_array(),
            'website1'          => $this->db->get_where('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 3], 1, 0)->row_array(),
            'website2'          => $this->db->get_where('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 3], 1, 1)->row_array(),
            'website3'          => $this->db->get_where('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $id, 'TYPE_ID' => 3], 1, 2)->row_array(),
            'work_time'         => $work_time,
            'metro'             => $this->db->get_where('ORGANIZATION_METRO', ['ORGANIZATION_ID' => $id])->result_array(),
            'metro_list'        => $this->db->order_by('NAME','ASC')->get('METRO')->result_array(),
            'evidance'          => $this->db->get_where('ORGANIZATION_EVIDANCE', ['ORGANIZATION_ID' => $id], 1, 0)->row_array()
        ];

        $data['specializations'] = $this->db->get('SPECIALIZATIONS')->result_array();
        $data['users'] = $this->db->query('SELECT * FROM USERS ORDER BY EMAIL')->result_array();
        $layout['content'] = $this->load->view('/admin/organisations_edit', $data, true);

        $this->load->view('/admin/layout', $layout);
    }

    public function new_organizations_save()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $test = $this->input->post('submit');
        
        if (isset($test)) {
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone1')];
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone2')];
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone3')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website1')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website2')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website3')];

            $input_address = $this->input->post('address');

            $addresses[] = [
                'type_id'   => 1,
                'index'     => $input_address['legal']['index'],
                'region'    => $input_address['legal']['region'],
                'city'      => $input_address['legal']['city'],
                'street'    => $input_address['legal']['street'],
                'house'     => $input_address['legal']['house'],
                'building'  => $input_address['legal']['building'],
                'office'    => $input_address['legal']['office']
            ];
            $addresses[] = [
                'type_id'   => 2,
                'index'     => $input_address['actual']['index'],
                'region'    => $input_address['actual']['region'],
                'city'      => $input_address['actual']['city'],
                'street'    => $input_address['actual']['street'],
                'house'     => $input_address['actual']['house'],
                'building'  => $input_address['actual']['building'],
                'office'    => $input_address['actual']['office']
            ];

            $input_working_times = $this->input->post('work');
            $input_breaking_times = $this->input->post('break');

            $working_times[] = [
                'day_id'        => 1,
                'work_from'     => $input_working_times[1]['from'],
                'work_to'       => $input_working_times[1]['to'],
                'break_from'    => $input_breaking_times[1]['from'],
                'break_to'      => $input_breaking_times[1]['to']
            ];
            $working_times[] = [
                'day_id'        => 2,
                'work_from'     => $input_working_times[2]['from'],
                'work_to'       => $input_working_times[2]['to'],
                'break_from'    => $input_breaking_times[2]['from'],
                'break_to'      => $input_breaking_times[2]['to']
            ];
            $working_times[] = [
                'day_id'        => 3,
                'work_from'     => $input_working_times[3]['from'],
                'work_to'       => $input_working_times[3]['to'],
                'break_from'    => $input_breaking_times[3]['from'],
                'break_to'      => $input_breaking_times[3]['to']
            ];
            $working_times[] = [
                'day_id'        => 4,
                'work_from'     => $input_working_times[4]['from'],
                'work_to'       => $input_working_times[4]['to'],
                'break_from'    => $input_breaking_times[4]['from'],
                'break_to'      => $input_breaking_times[4]['to']
            ];
            $working_times[] = [
                'day_id'        => 5,
                'work_from'     => $input_working_times[5]['from'],
                'work_to'       => $input_working_times[5]['to'],
                'break_from'    => $input_breaking_times[5]['from'],
                'break_to'      => $input_breaking_times[5]['to']
            ];
            $working_times[] = [
                'day_id'        => 6,
                'work_from'     => $input_working_times[6]['from'],
                'work_to'       => $input_working_times[6]['to'],
                'break_from'    => $input_breaking_times[6]['from'],
                'break_to'      => $input_breaking_times[6]['to']
            ];
            $working_times[] = [
                'day_id'        => 7,
                'work_from'     => $input_working_times[7]['from'],
                'work_to'       => $input_working_times[7]['to'],
                'break_from'    => $input_breaking_times[7]['from'],
                'break_to'      => $input_breaking_times[7]['to']
            ];

            $parent_id = $this->input->post('parent_id');

            $fields = [
                'main'  => [
                    'category_id'   => (int)$this->input->post('category_id'),
                    'type_id'       => (int)$this->input->post('type_id'),
                    'specialization_id' => (int)$this->input->post('specialization_id'),
                    'brand'         => $this->input->post('brand'),
                    'entity'        => $this->input->post('entity'),
                    'taxation_id'   => (int)$this->input->post('taxation_id'),
                    'description'   => $this->input->post('description'),
                    'license'       => $this->input->post('license'),
                    'ogrn'          => $this->input->post('ogrn'),
                    'inn'           => $this->input->post('inn'),
                    'okved'         => $this->input->post('okved'),
                    'okpo'          => $this->input->post('okpo'),
                    'kpp'           => $this->input->post('kpp'),
                    'oktmo'         => $this->input->post('oktmo'),
                    'checking_account'  => $this->input->post('checking_account'),
                    'corr_account'  => $this->input->post('corr_account'),
                    'bank'          => $this->input->post('bank'),
                    'bik'           => $this->input->post('bik'),
                    'administrator' => $this->input->post('administrator'),
                    'ceo'           => $this->input->post('ceo'),
                    'chief_accountant'  => $this->input->post('chief_accountant'),
                    'latitude'      => $this->input->post('latitude'),
                    'longitude'     => $this->input->post('longitude'),
                    'in_carousel'   => ($this->input->post('in_carousel') == 'on') ? 1 : 0,
                    'public'        => ($this->input->post('public') == 'on') ? 1 : 0,
                    'active'        => ($this->input->post('active') == 'on') ? 1 : 0,
                    'moderated'     => ($this->input->post('moderated') == 'on') ? 1 : 0,
                    'parent_id'     => (!empty($parent_id)) ? (int)$parent_id : null,
                    'popular'       => ($this->input->post('popular') == 'on') ? 1 : 0,
                ],
                'filter'        => [
                    'f_state'       => ($this->input->post('f_state') == 'on') ? 1 : 0,
                    'f_private'     => ($this->input->post('f_private') == 'on') ? 1 : 0,
                    'f_children'    => ($this->input->post('f_children') == 'on') ? 1 : 0,
                    'f_ambulance'   => ($this->input->post('f_ambulance') == 'on') ? 1 : 0,
                    'f_house'       => ($this->input->post('f_house') == 'on') ? 1 : 0,
                    'f_booking'     => ($this->input->post('f_booking') == 'on') ? 1 : 0,
                    'f_delivery'    => ($this->input->post('f_delivery') == 'on') ? 1 : 0,
                    'f_daynight'    => ($this->input->post('f_daynight') == 'on') ? 1 : 0,
                    'f_dms'         => ($this->input->post('f_dms') == 'on') ? 1 : 0,
                    'f_dlo'         => ($this->input->post('f_dlo') == 'on') ? 1 : 0,
                    'f_optics'      => ($this->input->post('f_optics') == 'on') ? 1 : 0,
                    'f_rpo'         => ($this->input->post('f_rpo') == 'on') ? 1 : 0,
                    'f_homeopathy'  => ($this->input->post('f_homeopathy') == 'on') ? 1 : 0,
                ],
                'contacts'      => $contacts,
                'addresses'     => $addresses,
                'working_times' => $working_times,
                'metros'        => $this->input->post('metros')
            ];

            $user_id = $this->input->post('user_id');
            if (!empty($user_id)) {
                $fields['main']['user_id'] = (int)$this->input->post('user_id');
            }

            $config['upload_path']          = '/images/organisations/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 512;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['file_name']            = md5(uniqid());

            $this->load->library('upload', $config);

            if (!empty($_FILES['logo']['tmp_name'])) {
                if ($this->upload->do_upload('logo')) {
                    $data = $this->upload->data();
                    $fields['main']['logo'] = $config['file_name'].$data['file_ext'];
                }

                $config['image_library'] = 'gd2';
                $config['source_image'] = './images/organisations/'.$fields['main']['logo'];
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width']         = 90;
                $config['height']       = 60;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();
            }

            $this->load->model('New_organisations_model');
            $this->New_organisations_model->create($fields);
            
            redirect('/cp/new_organizations');
        } else {
            show_404();
        }
    }

    public function new_organizations_update($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $button = $this->input->post('submit');


        if (isset($button)) {
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone1')];
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone2')];
            $contacts[] = ['type_id' => 2, 'value' => $this->input->post('phone3')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website1')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website2')];
            $contacts[] = ['type_id' => 3, 'value' => $this->input->post('website3')];

            $input_address = $this->input->post('address');

            $addresses[] = [
                'type_id'   => 1,
                'index'     => $input_address['legal']['index'],
                'region'    => $input_address['legal']['region'],
                'city'      => $input_address['legal']['city'],
                'street'    => $input_address['legal']['street'],
                'house'     => $input_address['legal']['house'],
                'building'  => $input_address['legal']['building'],
                'office'    => $input_address['legal']['office']
            ];
            $addresses[] = [
                'type_id'   => 2,
                'index'     => $input_address['actual']['index'],
                'region'    => $input_address['actual']['region'],
                'city'      => $input_address['actual']['city'],
                'street'    => $input_address['actual']['street'],
                'house'     => $input_address['actual']['house'],
                'building'  => $input_address['actual']['building'],
                'office'    => $input_address['actual']['office']
            ];

            $input_working_times = $this->input->post('work');
            $input_breaking_times = $this->input->post('break');

            $working_times[] = [
                'day_id'        => 1,
                'work_from'     => $input_working_times[1]['from'],
                'work_to'       => $input_working_times[1]['to'],
                'break_from'    => $input_breaking_times[1]['from'],
                'break_to'      => $input_breaking_times[1]['to']
            ];
            $working_times[] = [
                'day_id'        => 2,
                'work_from'     => $input_working_times[2]['from'],
                'work_to'       => $input_working_times[2]['to'],
                'break_from'    => $input_breaking_times[2]['from'],
                'break_to'      => $input_breaking_times[2]['to']
            ];
            $working_times[] = [
                'day_id'        => 3,
                'work_from'     => $input_working_times[3]['from'],
                'work_to'       => $input_working_times[3]['to'],
                'break_from'    => $input_breaking_times[3]['from'],
                'break_to'      => $input_breaking_times[3]['to']
            ];
            $working_times[] = [
                'day_id'        => 4,
                'work_from'     => $input_working_times[4]['from'],
                'work_to'       => $input_working_times[4]['to'],
                'break_from'    => $input_breaking_times[4]['from'],
                'break_to'      => $input_breaking_times[4]['to']
            ];
            $working_times[] = [
                'day_id'        => 5,
                'work_from'     => $input_working_times[5]['from'],
                'work_to'       => $input_working_times[5]['to'],
                'break_from'    => $input_breaking_times[5]['from'],
                'break_to'      => $input_breaking_times[5]['to']
            ];
            $working_times[] = [
                'day_id'        => 6,
                'work_from'     => $input_working_times[6]['from'],
                'work_to'       => $input_working_times[6]['to'],
                'break_from'    => $input_breaking_times[6]['from'],
                'break_to'      => $input_breaking_times[6]['to']
            ];
            $working_times[] = [
                'day_id'        => 7,
                'work_from'     => $input_working_times[7]['from'],
                'work_to'       => $input_working_times[7]['to'],
                'break_from'    => $input_breaking_times[7]['from'],
                'break_to'      => $input_breaking_times[7]['to']
            ];

            $fields = [
                'id'    => $id,
                'main'  => [
                    'category_id'   => $this->input->post('category_id'),
                    'type_id'       => $this->input->post('type_id'),
                    'specialization_id' => (int)$this->input->post('specialization_id'),
                    'brand'         => $this->input->post('brand'),
                    'entity'        => $this->input->post('entity'),
                    'taxation_id'   => $this->input->post('taxation_id'),
                    'description'   => $this->input->post('description'),
                    'license'       => $this->input->post('license'),
                    'ogrn'          => $this->input->post('ogrn'),
                    'inn'           => $this->input->post('inn'),
                    'okved'         => $this->input->post('okved'),
                    'okpo'          => $this->input->post('okpo'),
                    'kpp'           => $this->input->post('kpp'),
                    'oktmo'         => $this->input->post('oktmo'),
                    'checking_account'  => $this->input->post('checking_account'),
                    'corr_account'  => $this->input->post('corr_account'),
                    'bank'          => $this->input->post('bank'),
                    'bik'           => $this->input->post('bik'),
                    'administrator' => $this->input->post('administrator'),
                    'ceo'           => $this->input->post('ceo'),
                    'chief_accountant'  => $this->input->post('chief_accountant'),
                    'latitude'      => $this->input->post('latitude'),
                    'longitude'     => $this->input->post('longitude'),
                    'in_carousel'   => ($this->input->post('in_carousel') == 'on') ? 1 : 0,
                    'public'        => ($this->input->post('public') == 'on') ? 1 : 0,
                    'active'        => ($this->input->post('active') == 'on') ? 1 : 0,
                    'moderated'     => ($this->input->post('moderated') == 'on') ? 1 : 0,
                    'popular'       => ($this->input->post('popular') == 'on') ? 1 : 0,
                ],
                'filter'        => [
                    'f_state'       => ($this->input->post('f_state') == 'on') ? 1 : 0,
                    'f_private'     => ($this->input->post('f_private') == 'on') ? 1 : 0,
                    'f_children'    => ($this->input->post('f_children') == 'on') ? 1 : 0,
                    'f_ambulance'   => ($this->input->post('f_ambulance') == 'on') ? 1 : 0,
                    'f_house'       => ($this->input->post('f_house') == 'on') ? 1 : 0,
                    'f_booking'     => ($this->input->post('f_booking') == 'on') ? 1 : 0,
                    'f_delivery'    => ($this->input->post('f_delivery') == 'on') ? 1 : 0,
                    'f_daynight'    => ($this->input->post('f_daynight') == 'on') ? 1 : 0,
                    'f_dms'         => ($this->input->post('f_dms') == 'on') ? 1 : 0,
                    'f_dlo'         => ($this->input->post('f_dlo') == 'on') ? 1 : 0,
                    'f_optics'      => ($this->input->post('f_optics') == 'on') ? 1 : 0,
                    'f_rpo'         => ($this->input->post('f_rpo') == 'on') ? 1 : 0,
                    'f_homeopathy'  => ($this->input->post('f_homeopathy') == 'on') ? 1 : 0,
                ],
                'contacts'      => $contacts,
                'addresses'     => $addresses,
                'working_times' => $working_times,
                'metros'        => $this->input->post('metros')
            ];

            $user_id = $this->input->post('user_id');
            if (!empty($user_id)) {
                $fields['main']['user_id'] = (int)$this->input->post('user_id');
            }

            $config['upload_path']          = './images/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 500;
            $config['max_width']            = 3024;
            $config['max_height']           = 3024;
            $config['file_name']            = md5(uniqid());

            $this->load->library('upload', $config);

            if (!empty($_FILES['logo']['tmp_name'])) {       
                if ($this->upload->do_upload('logo')) {
                    $data = $this->upload->data();
                    $fields['main']['logo'] = $config['file_name'].$data['file_ext'];
                }                                     
                else{
                    //echo $this->upload->display_errors();
                    $error=true;
                  $this->load->library('session');
                  $this->session->set_userdata('error', 'ваше изображение превышает 3 мб');
                   //SetCookie("error","ваше изображение превышает 3 мб");
                  
                }
                $config['image_library'] = 'gd2';
                $config['source_image'] = './images/'.$fields['main']['logo'];
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width']         = 90;
                $config['height']       = 60;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();
            }
           
            $this->load->model('New_organisations_model');
            $this->New_organisations_model->update($fields,$error);
            if($error=true){
                 
              // $this->load->library('session');
               //$this->session->set_userdata('error', 'ваше изображение превышает 3 мб');
                header('location:' . $_SERVER['HTTP_REFERER']);

            }
            else{
              redirect('/cp/new_organizations');  
            }
            
        } else {
            show_404();
        }
    }

    public function new_organizations_delete($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $this->db->delete('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $id]);
        $this->db->delete('ORGANIZATION_ADDRESSES', ['ORGANIZATION_ID' => $id]);
        $this->db->delete('ORGANIZATION_DOCUMENTS', ['ORGANIZATION_ID' => $id]);
        $this->db->delete('ORGANIZATION_METRO', ['ORGANIZATION_ID' => $id]);
        $this->db->delete('ORGANIZATION_MULTIMEDIA', ['ORGANIZATION_ID' => $id]);
        $this->db->delete('ORGANIZATION_WORKING_TIME', ['ORGANIZATION_ID' => $id]);
        $this->db->delete('ORGANIZATIONS', ['ID' => $id]);

        redirect('/cp/new_organizations');
    }
    public function admins_delete($id)
    {
        if($id!=1){
            $this->db->delete('ADMINS', ['ID' => $id]);
            redirect('/cp/admins'); 
        }
        else{
             redirect('/cp/admins');
        }
       
        
    }    
    public function articles_add()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
        if (isset($_POST['submit']) && !empty($_POST['category_id']) && !empty($_POST['name']) && !empty($_POST['text'])) {
//            $_POST['text'] = mysqli_real_escape_string($this->db->conn_id, $_POST['text']);
            $this->db->query('SET FOREIGN_KEY_CHECKS=0');

//            $order   = array("\r\n", "\n", "\r", "rn", "rnrn", "<p>&nbsp;</p>");
//            $replace = '';
//            $text = str_replace($order, $replace, $_POST['text']);

            $insert_data = [
                'OBJECT'        => !empty($_GET['OBJECT']) ? (int)$_GET['OBJECT'] : NULL,
                'LOADER_ID'     => !empty($_GET['ID']) ? (int)$_GET['ID'] : NULL,
                'CATEGORY_ID'   => $_POST['category_id'],
                'NAME'          => $_POST['name'],
                'TEXT'          => $_POST['text'],
                'FIRST_HELP'    => (isset($_POST['first_help']) && $_POST['first_help'] == 'on') ? 1 : 0,
                'SITE_CATEGORY_ID'  => !empty($_POST['site_category_id']) ? (int)$_POST['site_category_id'] : NULL
            ];

            $this->db->insert('ARTICLES', $insert_data);

            $this->db->query('SET FOREIGN_KEY_CHECKS=1');
            redirect('/cp/articles');
        }

        if (!empty($_GET['OBJECT']) && !empty($_GET['ID'])) {

            $data['id'] = (int)$_GET['ID'];
            if ($_GET['OBJECT'] == 1) {
                $data['back_link_title']    = 'Редактирование организации';
                $data['back_link']          = 'organisations_edit';
                $data['item']               = $this->db->query("SELECT NAME,SHORTNAME FROM INSTITUTIONS WHERE ID = ".(int)$_GET['ID'])->row_array();
            } elseif ($_GET['OBJECT'] == 2) {
                $data['back_link_title']    = 'Редактирование отделения';
                $data['back_link']          = 'organisation_branches_edit';
                $data['item']               = $this->db->query("SELECT NAME,'' SHORTNAME FROM BRANCHES WHERE ID = ".(int)$_GET['ID'])->row_array();
            } elseif ($_GET['OBJECT'] == 3) {
                $data['back_link_title']    = 'Редактирование специалиста';
                $data['back_link']          = 'specialists_edit';
                $data['item']               = $this->db->query("SELECT SECOND_NAME NAME, FIRST_NAME SHORTNAME FROM SPECIALISTS WHERE ID = ".(int)$_GET['ID'])->row_array();
            }
        }

        $data['articles'] = $this->db->query('SELECT * FROM ARTICLES_CATEGORY')->result_array();
        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/articles_add', $data);
    }

    public function articles_edit($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            if (isset($_POST['submit']) && !empty($_POST['category_id']) && !empty($_POST['name']) && !empty($_POST['text'])) {
//                $_POST['text'] = mysqli_real_escape_string($this->db->conn_id, $_POST['text']);
                $insert_data = [
                    'OBJECT'        => !empty($_GET['OBJECT']) ? (int)$_GET['OBJECT'] : NULL,
                    'LOADER_ID'     => !empty($_GET['ID']) ? (int)$_GET['ID'] : NULL,
                    'CATEGORY_ID'   => $_POST['category_id'],
                    'NAME'          => $_POST['name'],
                    'TEXT'          => $_POST['text'],
                    'FIRST_HELP'    => (isset($_POST['first_help']) && $_POST['first_help'] == 'on') ? 1 : 0,
                    'SITE_CATEGORY_ID'  => !empty($_POST['site_category_id']) ? (int)$_POST['site_category_id'] : NULL
                ];
                $this->db->update('ARTICLES', $insert_data, ['ID' => $id]);
                redirect('/cp/articles');
            }
            $data['item'] = $this->db->query('SELECT * FROM ARTICLES WHERE ID = '.$id)->row_array();
            if (empty($data['item']))
                show_404();
        } else {
            show_404();
        }

        $data['articles'] = $this->db->query('SELECT * FROM ARTICLES_CATEGORY')->result_array();
        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/articles_edit', $data);
    }

    public function articles_delete($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $this->db->query('DELETE FROM ARTICLES WHERE ID = '.$id);
        }
        redirect('/cp/articles');
    }
    
    
    public function opinion_delete($id)
    {
        
      
        
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $idfeedback=$this->db->query('SELECT IDFEEDBACK FROM POSTS WHERE ID = '.$id);
        $result=$idfeedback->result();
        $feedback=$result[0]->IDFEEDBACK;
             //exit();
            $this->db->query('DELETE FROM FEEDBACKS WHERE ID = '.$feedback);
            $this->db->query('DELETE FROM POSTS WHERE ID = '.$id);
        }
        redirect('/cp/opinion');
    }
    
     public function message_delete($id)
    {
        
      
        
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $this->db->query('DELETE FROM POSTS WHERE ID = '.$id);
        }
        redirect('/cp/posts');
    }
    

    public function articles_category()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 25;

        $data['catalog']  = $this->db->query('SELECT * FROM ARTICLES_CATEGORY ORDER BY NAME')->result_array();
        $catalog_quantity = $this->db->query('SELECT COUNT(0) COUNT FROM ARTICLES_CATEGORY')->row_array()['COUNT'];
        //        $data['url_with_params'] = $_SERVER['PATH_INFO']."?";

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;
        $this->load->view('/cp/articles_category', $data);
    }

    public function articles_category_add()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
        if (isset($_POST['submit']) && !empty($_POST['category'])) {
            $this->db->query('INSERT INTO ARTICLES_CATEGORY(NAME) VALUES("'.$_POST['category'].'")');
            redirect('/cp/articles_category');
        }

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/articles_category_add', $data);
    }

    public function articles_category_delete($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $this->db->query('DELETE FROM ARTICLES_CATEGORY WHERE ID = '.$id);
        }
        redirect('/cp/articles_category');
    }

    public function articles_category_edit($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            if (isset($_POST['submit'])) {
                $this->db->query('UPDATE ARTICLES_CATEGORY SET NAME = "'.$_POST['category'].'" WHERE ID = '.$id);
                redirect('/cp/articles_category');
            }
            $data['item'] = $this->db->query('SELECT * FROM ARTICLES_CATEGORY WHERE ID = '.$id)->row_array();
            if (empty($data['item']))
                show_404();
        } else {
            show_404();
        }

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/articles_category_edit', $data);
    }

    public function pages($type)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
        if (empty($type))
            show_404();

        $data = [];

        switch ($type) {
            case 'calculator':
                $data['page_type'] = 'Калькуляторы';
                $data['type'] = 'calculator';
                $type_id = 1;
                break;
            case 'rating':
                $data['page_type'] = 'Рейтинги';
                $data['type'] = 'rating';
                $type_id = 2;
                break;
            case 'voting':
                $data['page_type'] = 'Голосования';
                $data['type'] = 'voting';
                $type_id = 3;
                break;
            default:
                show_404();
        }

        $data['catalog'] = $this->db->where('TYPE_ID', $type_id)->get('PAGES')->result_array();
        $data['type_id'] = $type_id;
        $layout['content'] = $this->load->view('/admin/pages', $data, true);;

        $this->load->view('/admin/layout', $layout);
    }

    
    public function page_delete($type,$id)
    {
                   
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;
          if($type=='calculator') {
            
            
        
        if (!empty($id)) {
        
        
            $this->db->query('DELETE FROM PAGES WHERE ID='.$id.' ');
        }
        redirect('/cp/pages/calculator');
        }
        else{
            if($type=='rating') {
             $this->db->query('DELETE FROM PAGES WHERE ID='.$id.' '); 
           redirect('/cp/pages/rating');  
        }
       
        }
    }
    
    
    
    
    
    
    public function page_edit($type = null, $id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (empty($type))
            show_404();

        $data = [];

        switch ($type) {
            case 'calculator':
                $name = 'Калькуляторы';
                $type_id = 1;
                break;
            case 'rating':
                $name = 'Рейтинги';
                $type_id = 2;
                break;
            case 'voting':
                $name = 'Голосования';
                $type_id = 3;
                break;
            default:
                show_404();
        }

        $page = $this->db->query("SELECT * FROM PAGES WHERE ID = $id LIMIT 1")->row_array();

        $data['page'] = [];

        if (!empty($page))
            $data['page'] = $page;

        $data['type_id'] = $type_id;
        $data['type_name'] = $name;

        $layout['content'] = $this->load->view('/admin/page_edit', $data, true);

        $this->load->view('/admin/layout', $layout);
    }

    public function page_save($id = null, $type_id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
        if (empty($id) && empty($type_id))
            show_404();

        if (!empty($id)) {
            $page = $this->db->limit(1)->where('ID', $id)->get('PAGES')->row_array();
            $type_id = $page['TYPE_ID'];
        } else {
            $type_id = $type_id;
        }

        $fields['TYPE_ID'] = $type_id;

        if (!empty($_POST['name']))
            $fields['NAME'] = $_POST['name'];
        if (!empty($_POST['text']))
            $fields['TEXT'] = $_POST['text'];

        if (!empty($id)) {
            $this->db->where('ID', $id);
            $this->db->update('PAGES', $fields);
        } else {
            $this->db->insert('PAGES', $fields);
        }

        switch($type_id) {
            case 1:
                $name = 'calculator'; break;
            case 2:
                $name = 'rating'; break;
            case 3:
                $name = 'voting'; break;
        }

        redirect("/cp/pages/$name");
    }

    public function vote()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');


        $data['catalog'] = $this->db->query('SELECT * FROM VOTING')->result_array();
        $layout['content'] = $this->load->view('/admin/votes', $data, true);

        $this->load->view('/admin/layout', $layout);
    }

    public function vote_edit($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $data= [];

        if (!empty($id)) {
            $data['item'] = $this->db->query("SELECT * FROM VOTING WHERE ID = $id LIMIT 1")->row_array();
            $data['options'] = $this->db->query("SELECT * FROM VOTING_OPTION WHERE VOTE_ID = $id")->result_array();
        }

        $layout['content'] = $this->load->view('/admin/vote_edit', $data, true);

        $this->load->view('/admin/layout', $layout);
    }

    public function vote_save($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $fields = [];

        if (!empty($_POST['start_date']))
            $fields['START_DATE'] = $_POST['start_date'];
        if (!empty($_POST['end_date']))
            $fields['END_DATE'] = $_POST['end_date'];
        if (!empty($_POST['question']))
            $fields['QUESTION'] = $_POST['question'];
        if (!empty($_POST['show_result']) && $_POST['show_result'] == 'on')
            $fields['SHOW_RESULT'] = 1;
        else
            $fields['SHOW_RESULT'] = 0;

        if (!empty($id)) {
            $this->db->where('ID', $id);
            $this->db->update('VOTING', $fields);

            $this->db->where('VOTE_ID', $id);
            $this->db->delete('VOTING_OPTION');

            foreach ($_POST['option'] as $option) {
                if (!empty($option)) {
                    $fields = [
                        'VOTE_ID'   => $id,
                        'NAME'      => $option
                    ];

                    $this->db->insert('VOTING_OPTION', $fields);
                }
            }
        } else {
            $this->db->insert('VOTING', $fields);
            $id = $this->db->insert_id();

            foreach ($_POST['option'] as $option) {
                if (!empty($option)) {
                    $fields = [
                        'VOTE_ID'   => $id,
                        'NAME'      => $option
                    ];

                    $this->db->insert('VOTING_OPTION', $fields);
                }
            }
        }

        redirect('/cp/vote');
    }

    public function specialists()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 25;
        $where = "";
        $where_array = array();
        $pages = 0;
        $offset = "";

        if (!empty($_GET['pages']))
                $pages = ((int)$_GET['pages']) * ($per_page);

        $limit = "LIMIT $per_page";
        $offset = "OFFSET $pages";

        if (!empty($_GET['institution_id'])) {

            $_GET['institution_id'] = (int)$_GET['institution_id'];

            if (!empty($_GET['institution_id'])) {
                $where_array[] = "( INSTITUTIONS.ID = ".$_GET['institution_id']." )";
            }
        }

        if (!empty($_GET['specialization_id'])) {

            $_GET['specialization_id'] = (int)$_GET['specialization_id'];

            if (!empty($_GET['specialization_id'])) {
                $where_array[] = "( DOCTOR_TYPES.ID = ".$_GET['specialization_id']." )";
            }
        }

        if (!empty($where_array))
            $where = "WHERE ".implode(' AND ', $where_array);

        $data['catalog'] = $this->db->query("SELECT
            SPECIALISTS.*,
            USERS.LASTNAME USER_LAST_NAME,
            USERS.FIRSTNAME USER_FIRST_NAME,
            USERS.MIDDLENAME USER_SECOND_NAME,
            USERS.EMAIL USER_EMAIL,
            DOCTOR_TYPES.ID SPECIALIZATION_ID,
            DOCTOR_TYPES.NAME SPECIALIZATION_NAME,
            INSTITUTIONS.ID ORGANISATION_ID,
            INSTITUTIONS.NAME ORGANISATION_NAME,
            INSTITUTIONS.SHORTNAME ORGANISATION_SHORT_NAME,
            BRANCHES.ID ORGANISATION_BRANCH_ID,
            BRANCHES.ADDRESS ORGANISATION_ADDRESS
            FROM SPECIALISTS
            LEFT JOIN DOCTOR_TYPES ON DOCTOR_TYPES.ID = SPECIALISTS.SPECIALIZATION_ID
            LEFT JOIN USERS ON USERS.ID = SPECIALISTS.USER_ID
            LEFT JOIN BRANCHES ON BRANCHES.ID = SPECIALISTS.BRANCH_ID
            LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
            $where
            $limit
            $offset")->result_array();
        $catalog_quantity = $this->db->query("SELECT
            COUNT(0) COUNT
            FROM SPECIALISTS
            LEFT JOIN DOCTOR_TYPES ON DOCTOR_TYPES.ID = SPECIALISTS.SPECIALIZATION_ID
            LEFT JOIN USERS ON USERS.ID = SPECIALISTS.USER_ID
            LEFT JOIN BRANCHES ON BRANCHES.ID = SPECIALISTS.BRANCH_ID
            LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
            $where")->row_array()['COUNT'];
        $data['filters']['institutions'] = $this->db->query('SELECT * FROM INSTITUTIONS ORDER BY NAME ')->result_array();
        $data['filters']['specialities'] = $this->db->query('SELECT * FROM DOCTOR_TYPES ORDER BY NAME')->result_array();

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;
        $this->load->view('/cp/specialists', $data);
    }

    public function specialists_add()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
        if (isset($_POST['submit'])) {
            $insert_data = array();
            // TODO прикрепление к пользователю
            if (!empty($_FILES['photo']) && is_uploaded_file($_FILES['photo']['tmp_name'])) {
                $name = $_FILES['photo']['name'];
                $filename_array = explode('.', $name);
                $extension = end($filename_array);
                $new_path = "/images/specialists/".uniqid().".".$extension;
                $path = $_SERVER['DOCUMENT_ROOT'].$new_path;
                move_uploaded_file($_FILES["photo"]["tmp_name"], $path);
                $insert_data['PHOTO'] = $new_path;
            }
            if (!empty($_POST['last_name']))
                $insert_data['LAST_NAME'] = $_POST['last_name'];
            if (!empty($_POST['first_name']))
                $insert_data['FIRST_NAME'] = $_POST['first_name'];
            if (!empty($_POST['second_name']))
                $insert_data['SECOND_NAME'] = $_POST['second_name'];
            if (!empty($_POST['description']))
                $insert_data['DESCRIPTION'] = $_POST['description'];
            if (!empty($_POST['branch_id']))
                $insert_data['BRANCH_ID'] = $_POST['branch_id'];
            else
                $insert_data['BRANCH_ID'] = null;
            if (!empty($_POST['position']))
                $insert_data['POSITION'] = $_POST['position'];
            if (!empty($_POST['specialization']))
                $insert_data['SPECIALIZATION_ID'] = $_POST['specialization'];
            else
                $insert_data['SPECIALIZATION_ID'] = null;
            if (!empty($_POST['specialization_experience']))
                $insert_data['SPECIALIZATION_EXPERIENCE'] = $_POST['specialization_experience'];
            if (!empty($_POST['total_experience']))
                $insert_data['TOTAL_EXPERIENCE'] = $_POST['total_experience'];
            if (!empty($_POST['work_phone']))
                $insert_data['WORK_PHONE'] = $_POST['work_phone'];
            if (!empty($_POST['mobile_phone']))
                $insert_data['MOBILE_PHONE'] = $_POST['mobile_phone'];
            if (!empty($_POST['monday_from']))
                $insert_data['MONDAY_WORK_FROM'] = $_POST['monday_from'];
            if (!empty($_POST['monday_to']))
                $insert_data['MONDAY_WORK_TO'] = $_POST['monday_to'];
            if (!empty($_POST['tuesday_from']))
                $insert_data['TUESDAY_WORK_FROM'] = $_POST['tuesday_from'];
            if (!empty($_POST['tuesday_to']))
                $insert_data['TUESDAY_WORK_TO'] = $_POST['tuesday_to'];
            if (!empty($_POST['wednesday_from']))
                $insert_data['WEDNESDAY_WORK_FROM'] = $_POST['wednesday_from'];
            if (!empty($_POST['wednesday_to']))
                $insert_data['WEDNESDAY_WORK_TO'] = $_POST['wednesday_to'];
            if (!empty($_POST['thursday_from']))
                $insert_data['THURSDAY_WORK_FROM'] = $_POST['thursday_from'];
            if (!empty($_POST['thursday_to']))
                $insert_data['THURSDAY_WORK_TO'] = $_POST['thursday_to'];
            if (!empty($_POST['friday_from']))
                $insert_data['FRIDAY_WORK_FROM'] = $_POST['friday_from'];
            if (!empty($_POST['friday_to']))
                $insert_data['FRIDAY_WORK_TO'] = $_POST['friday_to'];
            if (!empty($_POST['saturday_from']))
                $insert_data['SATURDAY_WORK_FROM'] = $_POST['saturday_from'];
            if (!empty($_POST['saturday_to']))
                $insert_data['SATURDAY_WORK_To'] = $_POST['saturday_to'];
            if (!empty($_POST['sunday_from']))
                $insert_data['SUNDAY_WORK_FROM'] = $_POST['sunday_from'];
            if (!empty($_POST['sunday_to']))
                $insert_data['SUNDAY_WORK_TO'] = $_POST['sunday_to'];
            if (!empty($_POST['university']))
                $insert_data['UNIVERSITY'] = $_POST['university'];
            if (!empty($_POST['faculty']))
                $insert_data['FACULTY'] = $_POST['faculty'];
            if (!empty($_POST['diploma_series_and_number']))
                $insert_data['DIPLOMA_SERIES_AND_NUMBER'] = $_POST['diploma_series_and_number'];
            if (!empty($_POST['graduation_date']))
                $insert_data['GRADUATION_DATE'] = $_POST['graduation_date'];
            if (!empty($_POST['intership_institution']))
                $insert_data['INTERNSHIP_INSTITUTION'] = $_POST['intership_institution'];
            if (!empty($_POST['intership_speciality']))
                $insert_data['INTERNSHIP_SPECIALITY'] = $_POST['intership_speciality'];
            if (!empty($_POST['intership_date_from']))
                $insert_data['INTERNSHIP_DATE_FROM'] = $_POST['intership_date_from'];
            if (!empty($_POST['intership_date_to']))
                $insert_data['INTERNSHIP_DATE_TO'] = $_POST['intership_date_to'];
            if (!empty($_POST['residency_institution']))
                $insert_data['RESIDENCY_INSTITUTION'] = $_POST['residency_institution'];
            if (!empty($_POST['residency_speciality']))
                $insert_data['RESIDENCY_SPECIALITY'] = $_POST['residency_speciality'];
            if (!empty($_POST['residency_date_from']))
                $insert_data['RESIDENCY_DATE_FROM'] = $_POST['residency_date_from'];
            if (!empty($_POST['residency_date_to']))
                $insert_data['RESIDENCY_DATE_TO'] = $_POST['residency_date_to'];
            if (!empty($_POST['professional_retraining_institution']))
                $insert_data['PROFESSIONAL_RETRAINING_INSTITUTION'] = $_POST['professional_retraining_institution'];
            if (!empty($_POST['professional_retraining_speciality']))
                $insert_data['PROFESSIONAL_RETRAINING_SPECIALITY'] = $_POST['professional_retraining_speciality'];
            if (!empty($_POST['professional_retraining_years']))
                $insert_data['PROFESSIONAL_RETRAINING_YEARS'] = $_POST['professional_retraining_years'];
            if (!empty($_POST['public']) && ($_POST['public'] == 'on'))
                $insert_data['PUBLIC']          = 1;
            else
                $insert_data['PUBLIC']          = 0;
            if (!empty($_POST['active']) && ($_POST['active'] == 'on'))
                $insert_data['ACTIVE']          = 1;
            else
                $insert_data['ACTIVE']          = 0;
            if (!empty($_POST['moderated']) && ($_POST['moderated'] == 'on'))
                $insert_data['MODERATED']       = 1;
            else
                $insert_data['MODERATED']       = 0;

            if(!empty($insert_data)) {
                $this->db->insert('SPECIALISTS',$insert_data);

                $inserted = $this->db->insert_id();

                if (!empty($inserted))
                    redirect('/cp/specialists');
            }
        }

        $data['specializations'] = $this->db->query('SELECT * FROM DOCTOR_TYPES ORDER BY NAME')->result_array();
        $data['organisations'] = $this->db->query('SELECT
            BRANCHES.ID,
            BRANCHES.ADDRESS,
            INSTITUTIONS.NAME
            FROM BRANCHES
            LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID')->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/specialists_add', $data);
    }

    public function specialists_edit($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        $data['id'] = $id;

        if (!empty($id)) {
            if (isset($_POST['submit'])) {
                $insert_data = array();
                // TODO прикрепление к пользователю
                if (!empty($_FILES['photo']) && is_uploaded_file($_FILES['photo']['tmp_name'])) {
                    $name = $_FILES['photo']['name'];
                    $filename_array = explode('.', $name);
                    $extension = end($filename_array);
                    $new_path = "/images/specialists/".uniqid().".".$extension;
                    $path = $_SERVER['DOCUMENT_ROOT'].$new_path;
                    move_uploaded_file($_FILES["photo"]["tmp_name"], $path);
                    $insert_data['PHOTO'] = $new_path;
                }
                if (!empty($_POST['last_name']))
                    $insert_data['LAST_NAME'] = $_POST['last_name'];
                if (!empty($_POST['first_name']))
                    $insert_data['FIRST_NAME'] = $_POST['first_name'];
                if (!empty($_POST['second_name']))
                    $insert_data['SECOND_NAME'] = $_POST['second_name'];
                if (!empty($_POST['description']))
                    $insert_data['DESCRIPTION'] = $_POST['description'];
                if (!empty($_POST['branch_id']))
                    $insert_data['BRANCH_ID'] = $_POST['branch_id'];
                else
                    $insert_data['BRANCH_ID'] = null;
                if (!empty($_POST['position']))
                    $insert_data['POSITION'] = $_POST['position'];
                if (!empty($_POST['specialization']))
                    $insert_data['SPECIALIZATION_ID'] = $_POST['specialization'];
                else
                    $insert_data['SPECIALIZATION_ID'] = null;
                if (!empty($_POST['specialization_experience']))
                    $insert_data['SPECIALIZATION_EXPERIENCE'] = $_POST['specialization_experience'];
                if (!empty($_POST['total_experience']))
                    $insert_data['TOTAL_EXPERIENCE'] = $_POST['total_experience'];
                if (!empty($_POST['work_phone']))
                    $insert_data['WORK_PHONE'] = $_POST['work_phone'];
                if (!empty($_POST['mobile_phone']))
                    $insert_data['MOBILE_PHONE'] = $_POST['mobile_phone'];
                if (!empty($_POST['monday_from']))
                    $insert_data['MONDAY_WORK_FROM'] = $_POST['monday_from'];
                if (!empty($_POST['monday_to']))
                    $insert_data['MONDAY_WORK_TO'] = $_POST['monday_to'];
                if (!empty($_POST['tuesday_from']))
                    $insert_data['TUESDAY_WORK_FROM'] = $_POST['tuesday_from'];
                if (!empty($_POST['tuesday_to']))
                    $insert_data['TUESDAY_WORK_TO'] = $_POST['tuesday_to'];
                if (!empty($_POST['wednesday_from']))
                    $insert_data['WEDNESDAY_WORK_FROM'] = $_POST['wednesday_from'];
                if (!empty($_POST['wednesday_to']))
                    $insert_data['WEDNESDAY_WORK_TO'] = $_POST['wednesday_to'];
                if (!empty($_POST['thursday_from']))
                    $insert_data['THURSDAY_WORK_FROM'] = $_POST['thursday_from'];
                if (!empty($_POST['thursday_to']))
                    $insert_data['THURSDAY_WORK_TO'] = $_POST['thursday_to'];
                if (!empty($_POST['friday_from']))
                    $insert_data['FRIDAY_WORK_FROM'] = $_POST['friday_from'];
                if (!empty($_POST['friday_to']))
                    $insert_data['FRIDAY_WORK_TO'] = $_POST['friday_to'];
                if (!empty($_POST['saturday_from']))
                    $insert_data['SATURDAY_WORK_FROM'] = $_POST['saturday_from'];
                if (!empty($_POST['saturday_to']))
                    $insert_data['SATURDAY_WORK_To'] = $_POST['saturday_to'];
                if (!empty($_POST['sunday_from']))
                    $insert_data['SUNDAY_WORK_FROM'] = $_POST['sunday_from'];
                if (!empty($_POST['sunday_to']))
                    $insert_data['SUNDAY_WORK_TO'] = $_POST['sunday_to'];
                if (!empty($_POST['university']))
                    $insert_data['UNIVERSITY'] = $_POST['university'];
                if (!empty($_POST['faculty']))
                    $insert_data['FACULTY'] = $_POST['faculty'];
                if (!empty($_POST['diploma_series_and_number']))
                    $insert_data['DIPLOMA_SERIES_AND_NUMBER'] = $_POST['diploma_series_and_number'];
                if (!empty($_POST['graduation_date']))
                    $insert_data['GRADUATION_DATE'] = $_POST['graduation_date'];
                if (!empty($_POST['intership_institution']))
                    $insert_data['INTERNSHIP__INSTITUTION'] = $_POST['intership_institution'];
                if (!empty($_POST['intership_speciality']))
                    $insert_data['INTERNSHIP__SPECIALITY'] = $_POST['intership_speciality'];
                if (!empty($_POST['intership_date_from']))
                    $insert_data['INTERNSHIP__DATE_FROM'] = $_POST['intership_date_from'];
                if (!empty($_POST['intership_date_to']))
                    $insert_data['INTERNSHIP__DATE_TO'] = $_POST['intership_date_to'];
                if (!empty($_POST['residency_institution']))
                    $insert_data['RESIDENCY_INSTITUTION'] = $_POST['residency_institution'];
                if (!empty($_POST['residency_speciality']))
                    $insert_data['RESIDENCY_SPECIALITY'] = $_POST['residency_speciality'];
                if (!empty($_POST['residency_date_from']))
                    $insert_data['RESIDENCY_DATE_FROM'] = $_POST['residency_date_from'];
                if (!empty($_POST['residency_date_to']))
                    $insert_data['RESIDENCY_DATE_TO'] = $_POST['residency_date_to'];
                if (!empty($_POST['professional_retraining_institution']))
                    $insert_data['PROFESSIONAL_RETRAINING_INSTITUTION'] = $_POST['professional_retraining_institution'];
                if (!empty($_POST['professional_retraining_speciality']))
                    $insert_data['PROFESSIONAL_RETRAINING_SPECIALITY'] = $_POST['professional_retraining_speciality'];
                if (!empty($_POST['professional_retraining_years']))
                    $insert_data['PROFESSIONAL_RETRAINING_YEARS'] = $_POST['professional_retraining_years'];
                if (!empty($_POST['public']) && ($_POST['public'] == 'on'))
                    $insert_data['PUBLIC']          = 1;
                else
                    $insert_data['PUBLIC']          = 0;
                if (!empty($_POST['active']) && ($_POST['active'] == 'on'))
                    $insert_data['ACTIVE']          = 1;
                else
                    $insert_data['ACTIVE']          = 0;
                if (!empty($_POST['moderated']) && ($_POST['moderated'] == 'on'))
                    $insert_data['MODERATED']       = 1;
                else
                    $insert_data['MODERATED']       = 0;

                if(!empty($insert_data)) {
                    $this->db->where('ID', $id);
                    $this->db->update('SPECIALISTS',$insert_data);
                    redirect('/cp/specialists');
                }
            }
            $data['item'] = $this->db->query('SELECT * FROM SPECIALISTS WHERE ID = '.$id)->row_array();
            if (empty($data['item']))
                show_404();
        } else {
            show_404();
        }

        $data['specializations'] = $this->db->query('SELECT * FROM DOCTOR_TYPES ORDER BY NAME')->result_array();
        $data['organisations'] = $this->db->query('SELECT
            BRANCHES.ID,
            BRANCHES.ADDRESS,
            INSTITUTIONS.NAME
            FROM BRANCHES
            LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID')->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/specialists_edit', $data);
    }

    public function specialists_delete($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $this->db->query('DELETE FROM SPECIALISTS WHERE ID = '.$id);
        }

        $data['unreaded'] = $this->unreaded;
        redirect('/cp/specialists');
    }

    public function organisations()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 25;
        $where = "";
        $where_array = array();
        $pages = 0;
        $offset = "";

        if (!empty($_GET['pages']))
            $pages = ((int)$_GET['pages']) * ($per_page);

        $limit = "LIMIT $per_page";
        $offset = "OFFSET $pages";

        if (!empty($_GET['id'])) {
            $where_id = (int)$_GET['id'];
            $where_array[] = "( INSTITUTIONS.ID = $where_id )";
        }
        if (!empty($_GET['status'])) {
            $status = (int)$_GET['status']-1;
            $where_array[] = "( INSTITUTIONS.PREMODERATE = $status )";
        }
        if (!empty($_GET['brand'])) {
            $brand = mysqli_real_escape_string($this->db->conn_id, $_GET['brand']);
            $where_array[] = "( INSTITUTIONS.NAME LIKE '%$brand%' )";
        }
        if (!empty($_GET['legal'])) {
            $legal = mysqli_real_escape_string($this->db->conn_id, $_GET['legal']);
            $where_array[] = "( INSTITUTIONS.SHORTNAME LIKE '%$legal%' )";
        }
        if (!empty($_GET['inn'])) {
            $inn = mysqli_real_escape_string($this->db->conn_id, $_GET['inn']);
            $where_array[] = "( INSTITUTIONS.INN LIKE '%$inn%' )";
        }
        if (!empty($_GET['category_id'])) {

            $_GET['category_id'] = (int)$_GET['category_id'];

            if (!empty($_GET['category_id'])) {
                $where_array[] = "( INSTITUTIONS.CATEGORY_ID = ".(int)$_GET['category_id']." )";
            }
        }

        if (!empty($_GET['type_id'])) {

            $_GET['type_id'] = (int)$_GET['type_id'];

            if (!empty($_GET['type_id'])) {
                $where_array[] = "( INSTITUTIONS.TYPE_ID = ".(int)$_GET['type_id']." )";
            }
        }

        if (!empty($_GET['last_change'])) {
            $last_change = explode(".", $_GET['last_change']);
            if (!empty($last_change[0]) && !empty($last_change[1]) && !empty($last_change[2])) {
                $change_date = $last_change[2]."-".$last_change[1]."-".$last_change[0];
                $change_date = mysqli_real_escape_string($this->db->conn_id, $change_date);
                $where_array[] = "( INSTITUTIONS.UPDATED_AT LIKE '%$change_date%' )";
            }
        }

        if (!empty($where_array))
            $where = "WHERE ".implode(' AND ', $where_array);

        $data['catalog'] = $this->db->query("SELECT
            CATEGORY.NAME CATEGORYNAME,
            INSTITUTION_TYPES.NAME TYPENAME,
            INSTITUTIONS.*
            FROM INSTITUTIONS
            LEFT JOIN CATEGORY ON CATEGORY.ID = INSTITUTIONS.CATEGORY_ID
            LEFT JOIN INSTITUTION_TYPES ON INSTITUTION_TYPES.ID = INSTITUTIONS.TYPE_ID
            $where
            ORDER BY INSTITUTIONS.NAME
            $limit
            $offset")->result_array();
        $catalog_quantity = $this->db->query("SELECT
            COUNT(0) COUNT
            FROM INSTITUTIONS
            LEFT JOIN CATEGORY ON CATEGORY.ID = INSTITUTIONS.CATEGORY_ID
            LEFT JOIN INSTITUTION_TYPES ON INSTITUTION_TYPES.ID = INSTITUTIONS.TYPE_ID
            $where
            ORDER BY INSTITUTIONS.NAME")->row_array()['COUNT'];

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['users']      = $this->db->query("SELECT * FROM USERS ORDER BY EMAIL")->result_array();
        $data['types']      = $this->db->query("SELECT * FROM INSTITUTION_TYPES ORDER BY ID")->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/organisations', $data);
    }

    public function organisations_add()
    {
        if (!$this->session->userdata('ADMIN_ID'))
        redirect('/cp/');

        if (isset($_POST['submit'])) {
            $insert_data = array();
            // TODO прикрепление к пользователю
            if (!empty($_FILES['logo']) && is_uploaded_file($_FILES['logo']['tmp_name'])) {
                $name = $_FILES['logo']['name'];
                $filename_array = explode('.', $name);
                $extension = end($filename_array);
                $new_name = uniqid();
                $new_path = "/images/organisations/" . $new_name . "." . $extension;
                $new_thumb_path = "/images/organisations/" . $new_name . "_thumb." . $extension;
                $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                move_uploaded_file($_FILES["logo"]["tmp_name"], $path);
                $insert_data['LOGO'] = $new_path;

                $config['image_library'] = 'gd2';
                $config['source_image'] = $path;
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width']         = 240;
                $config['height']       = 240;

                $this->load->library('image_lib', $config);

                if ( ! $this->image_lib->resize())
                {
                    echo $this->image_lib->display_errors();
//                    exit();
                }

                $insert_data['LOGO_THUMB'] = $new_thumb_path;
            }
            if (!empty($_POST['category_id']))
                $insert_data['CATEGORY_ID'] = $_POST['category_id'];
            else
                $insert_data['CATEGORY_ID'] = NULL;
            if (!empty($_POST['type_id']))
                $insert_data['TYPE_ID'] = $_POST['type_id'];
            else
                $insert_data['TYPE_ID'] = null;
            if (!empty($_POST['name']))
                $insert_data['NAME'] = $_POST['name'];
            if (!empty($_POST['shortname']))
                $insert_data['SHORTNAME'] = $_POST['shortname'];
            if (!empty($_POST['description']))
                $insert_data['DESCRIPTION'] = $_POST['description'];
            else
                $insert_data['DESCRIPTION'] = NULL;

            if (!empty($_POST['legal_address_index']))
                $insert_data['LEGAL_ADDRESS_INDEX'] = $_POST['legal_address_index'];
            else
                $insert_data['LEGAL_ADDRESS_INDEX'] = NULL;
            if (!empty($_POST['legal_address_region']))
                $insert_data['LEGAL_ADDRESS_REGION'] = $_POST['legal_address_region'];
            else
                $insert_data['LEGAL_ADDRESS_REGION'] = NULL;
            if (!empty($_POST['legal_address_city']))
                $insert_data['LEGAL_ADDRESS_CITY'] = $_POST['legal_address_city'];
            else
                $insert_data['LEGAL_ADDRESS_CITY'] = NULL;
            if (!empty($_POST['legal_address_street']))
                $insert_data['LEGAL_ADDRESS_STREET'] = $_POST['legal_address_street'];
            else
                $insert_data['LEGAL_ADDRESS_STREET'] = NULL;
            if (!empty($_POST['legal_address_house']))
                $insert_data['LEGAL_ADDRESS_HOUSE'] = $_POST['legal_address_house'];
            else
                $insert_data['LEGAL_ADDRESS_HOUSE'] = NULL;
            if (!empty($_POST['legal_address_building']))
                $insert_data['LEGAL_ADDRESS_BUILDING'] = $_POST['legal_address_building'];
            else
                $insert_data['LEGAL_ADDRESS_BUILDING'] = NULL;
            if (!empty($_POST['legal_address_office']))
                $insert_data['LEGAL_ADDRESS_OFFICE'] = $_POST['legal_address_office'];
            else
                $insert_data['LEGAL_ADDRESS_OFFICE'] = NULL;

            if (!empty($_POST['actual_address_index']))
                $insert_data['ACTUAL_ADDRESS_INDEX'] = $_POST['actual_address_index'];
            else
                $insert_data['ACTUAL_ADDRESS_INDEX'] = NULL;
            if (!empty($_POST['actual_address_region']))
                $insert_data['ACTUAL_ADDRESS_REGION'] = $_POST['actual_address_region'];
            else
                $insert_data['ACTUAL_ADDRESS_REGION'] = NULL;
            if (!empty($_POST['actual_address_city']))
                $insert_data['ACTUAL_ADDRESS_CITY'] = $_POST['actual_address_city'];
            else
                $insert_data['ACTUAL_ADDRESS_CITY'] = NULL;
            if (!empty($_POST['actual_address_street']))
                $insert_data['ACTUAL_ADDRESS_STREET'] = $_POST['actual_address_street'];
            else
                $insert_data['ACTUAL_ADDRESS_STREET'] = NULL;
            if (!empty($_POST['actual_address_house']))
                $insert_data['ACTUAL_ADDRESS_HOUSE'] = $_POST['actual_address_house'];
            else
                $insert_data['ACTUAL_ADDRESS_HOUSE'] = NULL;
            if (!empty($_POST['actual_address_building']))
                $insert_data['ACTUAL_ADDRESS_BUILDING'] = $_POST['actual_address_building'];
            else
                $insert_data['ACTUAL_ADDRESS_BUILDING'] = NULL;
            if (!empty($_POST['actual_address_office']))
                $insert_data['ACTUAL_ADDRESS_OFFICE'] = $_POST['actual_address_office'];
            else
                $insert_data['ACTUAL_ADDRESS_OFFICE'] = NULL;

            if (!empty($_POST['license']))
                $insert_data['LICENSE'] = $_POST['license'];
            else
                $insert_data['LICENSE'] = NULL;
            if (!empty($_POST['phone1']))
                $insert_data['PHONE1'] = $_POST['phone1'];
            else
                $insert_data['PHONE1'] = NULL;
            if (!empty($_POST['phone2']))
                $insert_data['PHONE2'] = $_POST['phone2'];
            else
                $insert_data['PHONE2'] = NULL;
            if (!empty($_POST['phone3']))
                $insert_data['PHONE3'] = $_POST['phone3'];
            else
                $insert_data['PHONE3'] = NULL;
            if (!empty($_POST['website1']))
                $insert_data['WEBSITE1'] = $_POST['website1'];
            else
                $insert_data['WEBSITE1'] = $_POST['website1'];
            if (!empty($_POST['website2']))
                $insert_data['WEBSITE2'] = $_POST['website2'];
            else
                $insert_data['WEBSITE2'] = $_POST['website2'];
            if (!empty($_POST['website3']))
                $insert_data['WEBSITE3'] = $_POST['website3'];
            else
                $insert_data['WEBSITE3'] = $_POST['website3'];

            $files = [];
            $insert_docs = [];

            foreach ($_FILES['docs'] as $attr => $doc) {
                foreach ($doc as $key => $value) {
                    $files[$key][$attr] = $value;
                }
            }

            if (!empty($files)) {
                foreach ($files as $doc) {
                    $name = $doc['name'];
                    $filename_array = explode('.', $name);
                    $extension = end($filename_array);
                    $new_path = "/images/licenses/" . uniqid() . "." . $extension;
                    $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                    move_uploaded_file($doc["tmp_name"], $path);
                    $insert_docs[] = $new_path;
                }
            }
            if (!empty($_POST['email']))
                $insert_data['EMAIL'] = $_POST['email'];
            else
                $insert_data['EMAIL'] = NULL;
            if (!empty($_POST['ogrn']))
                $insert_data['OGRN'] = $_POST['ogrn'];
            else
                $insert_data['OGRN'] = NULL;
            if (!empty($_POST['inn']))
                $insert_data['INN'] = $_POST['inn'];
            else
                $insert_data['INN'] = NULL;
            if (!empty($_POST['okved']))
                $insert_data['OKVED'] = $_POST['okved'];
            else
                $insert_data['OKVED'] = NULL;
            if (!empty($_POST['okpo']))
                $insert_data['OKPO'] = $_POST['okpo'];
            else
                $insert_data['OKPO'] = NULL;
            if (!empty($_POST['kpp']))
                $insert_data['KPP'] = $_POST['kpp'];
            else
                $insert_data['KPP'] = NULL;
            if (!empty($_POST['oktmo']))
                $insert_data['OKTMO'] = $_POST['oktmo'];
            else
                $insert_data['OKTMO'] = NULL;
            if (!empty($_POST['account']))
                $insert_data['ACCOUNT'] = $_POST['account'];
            else
                $insert_data['ACCOUNT'] = NULL;
            if (!empty($_POST['bank']))
                $insert_data['BANK'] = $_POST['bank'];
            else
                $insert_data['BANK'] = NULL;
            if (!empty($_POST['bik']))
                $insert_data['BIK'] = $_POST['bik'];
            else
                $insert_data['BIK'] = NULL;
            if (!empty($_POST['monday_from']))
                $insert_data['MONDAY_WORK_FROM'] = $_POST['monday_from'];
            if (!empty($_POST['monday_to']))
                $insert_data['MONDAY_WORK_TO'] = $_POST['monday_to'];
            if (!empty($_POST['tuesday_from']))
                $insert_data['TUESDAY_WORK_FROM'] = $_POST['tuesday_from'];
            if (!empty($_POST['tuesday_to']))
                $insert_data['TUESDAY_WORK_TO'] = $_POST['tuesday_to'];
            if (!empty($_POST['wednesday_from']))
                $insert_data['WEDNESDAY_WORK_FROM'] = $_POST['wednesday_from'];
            if (!empty($_POST['wednesday_to']))
                $insert_data['WEDNESDAY_WORK_TO'] = $_POST['wednesday_to'];
            if (!empty($_POST['thursday_from']))
                $insert_data['THURSDAY_WORK_FROM'] = $_POST['thursday_from'];
            if (!empty($_POST['thursday_to']))
                $insert_data['THURSDAY_WORK_TO'] = $_POST['thursday_to'];
            if (!empty($_POST['friday_from']))
                $insert_data['FRIDAY_WORK_FROM'] = $_POST['friday_from'];
            if (!empty($_POST['friday_to']))
                $insert_data['FRIDAY_WORK_TO'] = $_POST['friday_to'];
            if (!empty($_POST['saturday_from']))
                $insert_data['SATURDAY_WORK_FROM'] = $_POST['saturday_from'];
            if (!empty($_POST['saturday_to']))
                $insert_data['SATURDAY_WORK_To'] = $_POST['saturday_to'];
            if (!empty($_POST['sunday_from']))
                $insert_data['SUNDAY_WORK_FROM'] = $_POST['sunday_from'];
            if (!empty($_POST['sunday_to']))
                $insert_data['SUNDAY_WORK_TO'] = $_POST['sunday_to'];

            if (!empty($_POST['monday_break_from']))
                $insert_data['MONDAY_BREAK_FROM'] = $_POST['monday_break_from'];
            if (!empty($_POST['monday_break_to']))
                $insert_data['MONDAY_BREAK_TO'] = $_POST['monday_break_to'];
            if (!empty($_POST['tuesday_break_from']))
                $insert_data['TUESDAY_BREAK_FROM'] = $_POST['tuesday_break_from'];
            if (!empty($_POST['tuesday_break_to']))
                $insert_data['TUESDAY_BREAK_TO'] = $_POST['tuesday_break_to'];
            if (!empty($_POST['wednesday_break_from']))
                $insert_data['WEDNESDAY_BREAK_FROM'] = $_POST['wednesday_break_from'];
            if (!empty($_POST['wednesday_break_to']))
                $insert_data['WEDNESDAY_BREAK_TO'] = $_POST['wednesday_break_to'];
            if (!empty($_POST['thursday_break_from']))
                $insert_data['THURSDAY_BREAK_FROM'] = $_POST['thursday_break_from'];
            if (!empty($_POST['thursday_break_to']))
                $insert_data['THURSDAY_BREAK_TO'] = $_POST['thursday_break_to'];
            if (!empty($_POST['friday_break_from']))
                $insert_data['FRIDAY_BREAK_FROM'] = $_POST['friday_break_from'];
            if (!empty($_POST['friday_break_to']))
                $insert_data['FRIDAY_BREAK_TO'] = $_POST['friday_break_to'];
            if (!empty($_POST['saturday_break_from']))
                $insert_data['SATURDAY_BREAK_FROM'] = $_POST['saturday_break_from'];
            if (!empty($_POST['saturday_break_to']))
                $insert_data['SATURDAY_BREAK_To'] = $_POST['saturday_break_to'];
            if (!empty($_POST['sunday_break_from']))
                $insert_data['SUNDAY_BREAK_FROM'] = $_POST['sunday_break_from'];
            if (!empty($_POST['sunday_break_to']))
                $insert_data['SUNDAY_BREAK_TO'] = $_POST['sunday_break_to'];
            if (!empty($_POST['chief_physician']))
                $insert_data['CHIEF_PHYSICIAN'] = $_POST['chief_physician'];
            else
                $insert_data['CHIEF_PHYSICIAN'] = NULL;
            if (!empty($_POST['chief_accountant']))
                $insert_data['CHIEF_ACCOUNTANT'] = $_POST['chief_accountant'];
            else
                $insert_data['CHIEF_ACCOUNTANT'] = NULL;
            if (!empty($_POST['ceo']))
                $insert_data['CEO'] = $_POST['ceo'];
            else
                $insert_data['CEO'] = NULL;


            if (!empty($_POST['state']) && $_POST['state'] == "true")
                $insert_data['STATE'] = 1;
            else
                $insert_data['STATE'] = 0;
            //------------------------
            if (!empty($_POST['private']) && $_POST['private'] == "true")
                $insert_data['PRIVATE'] = 1;
            else
                $insert_data['PRIVATE'] = 0;
            //------------------------
            if (!empty($_POST['children']) && $_POST['children'] == "true")
                $insert_data['CHILDREN'] = 1;
            else
                $insert_data['CHILDREN'] = 0;
            //------------------------
            if (!empty($_POST['ambulance']) && $_POST['ambulance'] == "true")
                $insert_data['AMBULANCE'] = 1;
            else
                $insert_data['AMBULANCE'] = 0;
            //------------------------
            if (!empty($_POST['house']) && $_POST['house'] == "true")
                $insert_data['HOUSE'] = 1;
            else
                $insert_data['HOUSE'] = 0;
            //------------------------
            if (!empty($_POST['booking']) && $_POST['booking'] == "true")
                $insert_data['BOOKING'] = 1;
            else
                $insert_data['BOOKING'] = 0;
            //------------------------
            if (!empty($_POST['delivery']) && $_POST['delivery'] == "true")
                $insert_data['DELIVERY'] = 1;
            else
                $insert_data['DELIVERY'] = 0;
            //------------------------
            if (!empty($_POST['daynight']) && $_POST['daynight'] == "true")
                $insert_data['DAYNIGHT'] = 1;
            else
                $insert_data['DAYNIGHT'] = 0;
            //------------------------
            if (!empty($_POST['dms']) && $_POST['dms'] == "true")
                $insert_data['DMS'] = 1;
            else
                $insert_data['DMS'] = 0;
            //------------------------
            if (!empty($_POST['dlo']) && $_POST['dlo'] == "true")
                $insert_data['DLO'] = 1;
            else
                $insert_data['DLO'] = 0;
            //------------------------
            if (!empty($_POST['optics']) && $_POST['optics'] == "true")
                $insert_data['OPTICS'] = 1;
            else
                $insert_data['OPTICS'] = 0;
            //------------------------
            if (!empty($_POST['rpo']) && $_POST['rpo'] == "true")
                $insert_data['RPO'] = 1;
            else
                $insert_data['RPO'] = 0;
            //------------------------
            if (!empty($_POST['homeopathy']) && $_POST['homeopathy'] == "true")
                $insert_data['HOMEOPATHY'] = 1;
            else
                $insert_data['HOMEOPATHY'] = 0;
            //------------------------


            if (!empty($_POST['moderated']) && $_POST['moderated'] == "true")
                $insert_data['PREMODERATE'] = 1;
            if (!empty($_POST['in_logo_carousel']) && $_POST['in_logo_carousel'] == "true")
                $insert_data['IN_LOGO_CAROUSEL'] = 1;
            else
                $insert_data['IN_LOGO_CAROUSEL'] = 0;

            if (!empty($insert_data)) {
                $insert_data['UPDATED_AT'] = date('Y-m-d H:i:s');

                $this->db->insert('INSTITUTIONS', $insert_data);

                $inserted = $this->db->insert_id();

                if (!empty($_POST['metro_id']) && is_array($_POST['metro_id'])) {

                    $current_metros = [];
                    $temp_metros = [];

                    if (!empty($inserted))
                        $temp_metros = $this->Institution_metros_model->get_institution_metro($inserted);

                    foreach ($temp_metros as $metro) {
                        $current_metros[(int)$metro['METRO_ID']] = 1;
                    }

                    foreach ($_POST['metro_id'] as $metro_id) {
                        if (!empty($current_metros[$metro_id]))
                            unset($current_metros[$metro_id]);
                        else
                            $this->Institution_metros_model->add_metro($inserted, $metro_id);
                    }

                    if (!empty($current_metros)) {
                        foreach ($current_metros as $current_metro => $dummy) {
                            $metros_to_delete[] = $current_metro;
                        }

                        $this->Institution_metros_model->delete_metros($inserted, $metros_to_delete);
                    }
                } else {
                    $this->Institution_metros_model->drop_institution_metro($inserted);
                }


                // Здесь отделения - копия метро по сути
                
                if (!empty($_POST['department_id']) && is_array($_POST['department_id'])) {

                    $current_departmets = [];
                    $temp_departmets = [];

                    if (!empty($inserted))
                        $temp_departmets = $this->Institution_departments_model->get_institution_departments($inserted);

                    foreach ($temp_departmets as $departmet) {
                        $current_departmets[(int)$departmet['DEPARTMENT_ID']] = 1;
                    }

                    foreach ($_POST['department_id'] as $department_id) {
                        if (!empty($current_departmets[$department_id]))
                            unset($current_departmets[$department_id]);
                        else
                            $this->Institution_departments_model->add_department($inserted, $department_id);
                    }

                    if (!empty($current_departmets)) {
                        foreach ($current_departmets as $current_departmet => $dummy) {
                            $departments_to_delete[] = $current_departmet;
                        }

                        $this->Institution_departments_model->delete_departments($inserted, $departments_to_delete);
                    }
                } else {
                    $this->Institution_departments_model->drop_institution_departments($inserted);
                }

                if (!empty($inserted)) {
                    $institution_id = $this->db->insert_id();
                    foreach ($insert_docs as $insert_doc) {
                        $docs = [
                            'INSTITUTION_ID' => $institution_id,
                            'FILE'           => $insert_doc
                        ];
                        $this->db->insert('INSTITUTION_FILES', $docs);
                    }
                    redirect('/cp/organisations');
                }
            }
        }

        $data['departments'] = $this->db->query('SELECT * FROM MEDICAL_INSTITUTION_DEPARTMENT ORDER BY NAME')->result_array();
        $data['types'] = $this->db->query('SELECT * FROM INSTITUTION_TYPES ORDER BY NAME')->result_array();
        $data['metro'] = $this->db->query('SELECT * FROM METRO ORDER BY NAME')->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/organisations_add', $data);
    }

    public function organisations_edit($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        $data['id'] = $id;

        if (!empty($id)) {
            if (isset($_POST['submit'])) {
                $insert_data = array();
                // TODO прикрепление к пользователю
                if (!empty($_FILES['photo']) && is_uploaded_file($_FILES['photo']['tmp_name'])) {
                    $name = $_FILES['photo']['name'];
                    $filename_array = explode('.', $name);
                    $extension = end($filename_array);
                    $new_path = "/images/organisations/" . uniqid() . "." . $extension;
                    $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                    move_uploaded_file($_FILES["photo"]["tmp_name"], $path);
                    $insert_data['LOGO'] = $new_path;
                }
                if (!empty($_POST['category_id']))
                    $insert_data['CATEGORY_ID'] = $_POST['category_id'];
                else
                    $insert_data['CATEGORY_ID'] = NULL;
                if (!empty($_POST['type_id']))
                    $insert_data['TYPE_ID'] = $_POST['type_id'];
                else
                    $insert_data['TYPE_ID'] = NULL;
                if (!empty($_POST['name']))
                    $insert_data['NAME'] = $_POST['name'];
                if (!empty($_POST['shortname']))
                    $insert_data['SHORTNAME'] = $_POST['shortname'];
                if (!empty($_POST['description']))
                    $insert_data['DESCRIPTION'] = $_POST['description'];
                else
                    $insert_data['DESCRIPTION'] = NULL;

                if (!empty($_POST['legal_address_index']))
                    $insert_data['LEGAL_ADDRESS_INDEX'] = $_POST['legal_address_index'];
                else
                    $insert_data['LEGAL_ADDRESS_INDEX'] = NULL;
                if (!empty($_POST['legal_address_region']))
                    $insert_data['LEGAL_ADDRESS_REGION'] = $_POST['legal_address_region'];
                else
                    $insert_data['LEGAL_ADDRESS_REGION'] = NULL;
                if (!empty($_POST['legal_address_city']))
                    $insert_data['LEGAL_ADDRESS_CITY'] = $_POST['legal_address_city'];
                else
                    $insert_data['LEGAL_ADDRESS_CITY'] = NULL;
                if (!empty($_POST['legal_address_street']))
                    $insert_data['LEGAL_ADDRESS_STREET'] = $_POST['legal_address_street'];
                else
                    $insert_data['LEGAL_ADDRESS_STREET'] = NULL;
                if (!empty($_POST['legal_address_house']))
                    $insert_data['LEGAL_ADDRESS_HOUSE'] = $_POST['legal_address_house'];
                else
                    $insert_data['LEGAL_ADDRESS_HOUSE'] = NULL;
                if (!empty($_POST['legal_address_building']))
                    $insert_data['LEGAL_ADDRESS_BUILDING'] = $_POST['legal_address_building'];
                else
                    $insert_data['LEGAL_ADDRESS_BUILDING'] = NULL;
                if (!empty($_POST['legal_address_office']))
                    $insert_data['LEGAL_ADDRESS_OFFICE'] = $_POST['legal_address_office'];
                else
                    $insert_data['LEGAL_ADDRESS_OFFICE'] = NULL;

                if (!empty($_POST['actual_address_index']))
                    $insert_data['ACTUAL_ADDRESS_INDEX'] = $_POST['actual_address_index'];
                else
                    $insert_data['ACTUAL_ADDRESS_INDEX'] = NULL;
                if (!empty($_POST['actual_address_region']))
                    $insert_data['ACTUAL_ADDRESS_REGION'] = $_POST['actual_address_region'];
                else
                    $insert_data['ACTUAL_ADDRESS_REGION'] = NULL;
                if (!empty($_POST['actual_address_city']))
                    $insert_data['ACTUAL_ADDRESS_CITY'] = $_POST['actual_address_city'];
                else
                    $insert_data['ACTUAL_ADDRESS_CITY'] = NULL;
                if (!empty($_POST['actual_address_street']))
                    $insert_data['ACTUAL_ADDRESS_STREET'] = $_POST['actual_address_street'];
                else
                    $insert_data['ACTUAL_ADDRESS_STREET'] = NULL;
                if (!empty($_POST['actual_address_house']))
                    $insert_data['ACTUAL_ADDRESS_HOUSE'] = $_POST['actual_address_house'];
                else
                    $insert_data['ACTUAL_ADDRESS_HOUSE'] = NULL;
                if (!empty($_POST['actual_address_building']))
                    $insert_data['ACTUAL_ADDRESS_BUILDING'] = $_POST['actual_address_building'];
                else
                    $insert_data['ACTUAL_ADDRESS_BUILDING'] = NULL;
                if (!empty($_POST['actual_address_office']))
                    $insert_data['ACTUAL_ADDRESS_OFFICE'] = $_POST['actual_address_office'];
                else
                    $insert_data['ACTUAL_ADDRESS_OFFICE'] = NULL;

                $insert_data['UPDATED_AT'] = date('Y-m-d H:i:s');

                // Начало логики работы с метро

                if (!empty($_POST['metro_id']) && is_array($_POST['metro_id'])) {

                    $current_metros = [];

                    $temp_metros = $this->Institution_metros_model->get_institution_metro($id);

                    foreach ($temp_metros as $metro) {
                        $current_metros[(int)$metro['METRO_ID']] = 1;
                    }

                    foreach ($_POST['metro_id'] as $metro_id) {
                        if (!empty($current_metros[$metro_id]))
                            unset($current_metros[$metro_id]);
                        else
                            $this->Institution_metros_model->add_metro($id, $metro_id);
                    }

                    if (!empty($current_metros)) {
                        foreach ($current_metros as $current_metro => $dummy) {
                            $metros_to_delete[] = $current_metro;
                        }

                        $this->Institution_metros_model->delete_metros($id, $metros_to_delete);
                    }
                } else {
                    $this->Institution_metros_model->drop_institution_metro($id);
                }

                // Конец логики работы с метро

                // Внутренние отделения

                if (!empty($_POST['department_id']) && is_array($_POST['department_id'])) {

                    $current_departments = [];
                    $temp_departments = $this->Institution_departments_model->get_institution_departments($id);

                    foreach ($temp_departments as $department) {
                        $current_departments[(int)$department['DEPARTMENT_ID']] = 1;
                    }

                    foreach ($_POST['department_id'] as $department_id) {
                        if (!empty($current_departments[$department_id]))
                            unset($current_departments[$department_id]);
                        else
                            $this->Institution_departments_model->add_department($id, $department_id);
                    }

                    if (!empty($current_departments)) {
                        foreach ($current_departments as $current_department => $dummy) {
                            $departments_to_delete[] = $current_department;
                        }

                        $this->Institution_departments_model->delete_departments($id, $departments_to_delete);
                    }
                } else {
                    $this->Institution_departments_model->drop_institution_departments($id);
                }

                if (!empty($_POST['phone1']))
                    $insert_data['PHONE1'] = $_POST['phone1'];
                else
                    $insert_data['PHONE1'] = NULL;
                if (!empty($_POST['phone2']))
                    $insert_data['PHONE2'] = $_POST['phone2'];
                else
                    $insert_data['PHONE2'] = NULL;
                if (!empty($_POST['phone3']))
                    $insert_data['PHONE3'] = $_POST['phone3'];
                else
                    $insert_data['PHONE3'] = NULL;
                if (!empty($_POST['website1']))
                    $insert_data['WEBSITE1'] = $_POST['website1'];
                else
                    $insert_data['WEBSITE1'] = $_POST['website1'];
                if (!empty($_POST['website2']))
                    $insert_data['WEBSITE2'] = $_POST['website2'];
                else
                    $insert_data['WEBSITE2'] = $_POST['website2'];
                if (!empty($_POST['website3']))
                    $insert_data['WEBSITE3'] = $_POST['website3'];
                else
                    $insert_data['WEBSITE3'] = $_POST['website3'];


                $files = [];
                $insert_docs = [];

                foreach ($_FILES['docs'] as $attr => $doc) {
                    foreach ($doc as $key => $value) {
                        $files[$key][$attr] = $value;
                    }
                }

                if (!empty($files)) {
                    foreach ($files as $doc) {
                        if (!empty($doc['name'])) {
                            $name = $doc['name'];
                            $filename_array = explode('.', $name);
                            $extension = end($filename_array);
                            $new_path = "/images/licenses/" . uniqid() . "." . $extension;
                            $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                            move_uploaded_file($doc["tmp_name"], $path);
                            $insert_docs[] = $new_path;
                        }
                    }
                }

                if (!empty($_POST['latitude']))
                    $insert_data['LATITUDE'] = $_POST['latitude'];
                else
                    $insert_data['LATITUDE'] = NULL;
                if (!empty($_POST['longitude']))
                    $insert_data['LONGITUDE'] = $_POST['longitude'];
                else
                    $insert_data['LONGITUDE'] = NULL;
                if (!empty($_POST['email']))
                    $insert_data['EMAIL'] = $_POST['email'];
                else
                    $insert_data['EMAIL'] = NULL;
                if (!empty($_POST['ogrn']))
                    $insert_data['OGRN'] = $_POST['ogrn'];
                else
                    $insert_data['OGRN'] = NULL;
                if (!empty($_POST['inn']))
                    $insert_data['INN'] = $_POST['inn'];
                else
                    $insert_data['INN'] = NULL;
                if (!empty($_POST['okved']))
                    $insert_data['OKVED'] = $_POST['okved'];
                else
                    $insert_data['OKVED'] = NULL;
                if (!empty($_POST['okpo']))
                    $insert_data['OKPO'] = $_POST['okpo'];
                else
                    $insert_data['OKPO'] = NULL;
                if (!empty($_POST['kpp']))
                    $insert_data['KPP'] = $_POST['kpp'];
                else
                    $insert_data['KPP'] = NULL;
                if (!empty($_POST['oktmo']))
                    $insert_data['OKTMO'] = $_POST['oktmo'];
                else
                    $insert_data['OKTMO'] = NULL;
                if (!empty($_POST['account']))
                    $insert_data['ACCOUNT'] = $_POST['account'];
                else
                    $insert_data['ACCOUNT'] = NULL;
                if (!empty($_POST['bank']))
                    $insert_data['BANK'] = $_POST['bank'];
                else
                    $insert_data['BANK'] = NULL;
                if (!empty($_POST['bik']))
                    $insert_data['BIK'] = $_POST['bik'];
                else
                    $insert_data['BIK'] = NULL;
                if (!empty($_POST['monday_from']))
                    $insert_data['MONDAY_WORK_FROM'] = $_POST['monday_from'];
                if (!empty($_POST['monday_to']))
                    $insert_data['MONDAY_WORK_TO'] = $_POST['monday_to'];
                if (!empty($_POST['tuesday_from']))
                    $insert_data['TUESDAY_WORK_FROM'] = $_POST['tuesday_from'];
                if (!empty($_POST['tuesday_to']))
                    $insert_data['TUESDAY_WORK_TO'] = $_POST['tuesday_to'];
                if (!empty($_POST['wednesday_from']))
                    $insert_data['WEDNESDAY_WORK_FROM'] = $_POST['wednesday_from'];
                if (!empty($_POST['wednesday_to']))
                    $insert_data['WEDNESDAY_WORK_TO'] = $_POST['wednesday_to'];
                if (!empty($_POST['thursday_from']))
                    $insert_data['THURSDAY_WORK_FROM'] = $_POST['thursday_from'];
                if (!empty($_POST['thursday_to']))
                    $insert_data['THURSDAY_WORK_TO'] = $_POST['thursday_to'];
                if (!empty($_POST['friday_from']))
                    $insert_data['FRIDAY_WORK_FROM'] = $_POST['friday_from'];
                if (!empty($_POST['friday_to']))
                    $insert_data['FRIDAY_WORK_TO'] = $_POST['friday_to'];
                if (!empty($_POST['saturday_from']))
                    $insert_data['SATURDAY_WORK_FROM'] = $_POST['saturday_from'];
                if (!empty($_POST['saturday_to']))
                    $insert_data['SATURDAY_WORK_To'] = $_POST['saturday_to'];
                if (!empty($_POST['sunday_from']))
                    $insert_data['SUNDAY_WORK_FROM'] = $_POST['sunday_from'];
                if (!empty($_POST['sunday_to']))
                    $insert_data['SUNDAY_WORK_TO'] = $_POST['sunday_to'];

                if (!empty($_POST['monday_break_from']))
                    $insert_data['MONDAY_BREAK_FROM'] = $_POST['monday_break_from'];
                if (!empty($_POST['monday_break_to']))
                    $insert_data['MONDAY_BREAK_TO'] = $_POST['monday_break_to'];
                if (!empty($_POST['tuesday_break_from']))
                    $insert_data['TUESDAY_BREAK_FROM'] = $_POST['tuesday_break_from'];
                if (!empty($_POST['tuesday_break_to']))
                    $insert_data['TUESDAY_BREAK_TO'] = $_POST['tuesday_break_to'];
                if (!empty($_POST['wednesday_break_from']))
                    $insert_data['WEDNESDAY_BREAK_FROM'] = $_POST['wednesday_break_from'];
                if (!empty($_POST['wednesday_break_to']))
                    $insert_data['WEDNESDAY_BREAK_TO'] = $_POST['wednesday_break_to'];
                if (!empty($_POST['thursday_break_from']))
                    $insert_data['THURSDAY_BREAK_FROM'] = $_POST['thursday_break_from'];
                if (!empty($_POST['thursday_break_to']))
                    $insert_data['THURSDAY_BREAK_TO'] = $_POST['thursday_break_to'];
                if (!empty($_POST['friday_break_from']))
                    $insert_data['FRIDAY_BREAK_FROM'] = $_POST['friday_break_from'];
                if (!empty($_POST['friday_break_to']))
                    $insert_data['FRIDAY_BREAK_TO'] = $_POST['friday_break_to'];
                if (!empty($_POST['saturday_break_from']))
                    $insert_data['SATURDAY_BREAK_FROM'] = $_POST['saturday_break_from'];
                if (!empty($_POST['saturday_break_to']))
                    $insert_data['SATURDAY_BREAK_To'] = $_POST['saturday_break_to'];
                if (!empty($_POST['sunday_break_from']))
                    $insert_data['SUNDAY_BREAK_FROM'] = $_POST['sunday_break_from'];
                if (!empty($_POST['sunday_break_to']))
                    $insert_data['SUNDAY_BREAK_TO'] = $_POST['sunday_break_to'];
                if (!empty($_POST['chief_physician']))
                    $insert_data['CHIEF_PHYSICIAN'] = $_POST['chief_physician'];
                else
                    $insert_data['CHIEF_PHYSICIAN'] = NULL;
                if (!empty($_POST['chief_accountant']))
                    $insert_data['CHIEF_ACCOUNTANT'] = $_POST['chief_accountant'];
                else
                    $insert_data['CHIEF_ACCOUNTANT'] = NULL;
                if (!empty($_POST['ceo']))
                    $insert_data['CEO'] = $_POST['ceo'];
                else
                    $insert_data['CEO'] = NULL;


                if (!empty($_POST['state']) && $_POST['state'] == "true")
                    $insert_data['STATE'] = 1;
                else
                    $insert_data['STATE'] = 0;
                //------------------------
                if (!empty($_POST['private']) && $_POST['private'] == "true")
                    $insert_data['PRIVATE'] = 1;
                else
                    $insert_data['PRIVATE'] = 0;
                //------------------------
                if (!empty($_POST['children']) && $_POST['children'] == "true")
                    $insert_data['CHILDREN'] = 1;
                else
                    $insert_data['CHILDREN'] = 0;
                //------------------------
                if (!empty($_POST['ambulance']) && $_POST['ambulance'] == "true")
                    $insert_data['AMBULANCE'] = 1;
                else
                    $insert_data['AMBULANCE'] = 0;
                //------------------------
                if (!empty($_POST['house']) && $_POST['house'] == "true")
                    $insert_data['HOUSE'] = 1;
                else
                    $insert_data['HOUSE'] = 0;
                //------------------------
                if (!empty($_POST['booking']) && $_POST['booking'] == "true")
                    $insert_data['BOOKING'] = 1;
                else
                    $insert_data['BOOKING'] = 0;
                //------------------------
                if (!empty($_POST['delivery']) && $_POST['delivery'] == "true")
                    $insert_data['DELIVERY'] = 1;
                else
                    $insert_data['DELIVERY'] = 0;
                //------------------------
                if (!empty($_POST['daynight']) && $_POST['daynight'] == "true")
                    $insert_data['DAYNIGHT'] = 1;
                else
                    $insert_data['DAYNIGHT'] = 0;
                //------------------------
                if (!empty($_POST['dms']) && $_POST['dms'] == "true")
                    $insert_data['DMS'] = 1;
                else
                    $insert_data['DMS'] = 0;
                //------------------------
                if (!empty($_POST['dlo']) && $_POST['dlo'] == "true")
                    $insert_data['DLO'] = 1;
                else
                    $insert_data['DLO'] = 0;
                //------------------------
                if (!empty($_POST['optics']) && $_POST['optics'] == "true")
                    $insert_data['OPTICS'] = 1;
                else
                    $insert_data['OPTICS'] = 0;
                //------------------------
                if (!empty($_POST['rpo']) && $_POST['rpo'] == "true")
                    $insert_data['RPO'] = 1;
                else
                    $insert_data['RPO'] = 0;
                //------------------------
                if (!empty($_POST['homeopathy']) && $_POST['homeopathy'] == "true")
                    $insert_data['HOMEOPATHY'] = 1;
                else
                    $insert_data['HOMEOPATHY'] = 0;
                //------------------------


                if (!empty($_POST['moderated']) && $_POST['moderated'] == "true")
                    $insert_data['PREMODERATE'] = 1;
                else
                    $insert_data['PREMODERATE'] = 0;
                if (!empty($_POST['in_logo_carousel']) && $_POST['in_logo_carousel'] == "true")
                    $insert_data['IN_LOGO_CAROUSEL'] = 1;
                else
                    $insert_data['IN_LOGO_CAROUSEL'] = 0;

                if (!empty($insert_data)) {
                    $this->db->where('ID',$id);
                    $this->db->update('INSTITUTIONS', $insert_data);

                    foreach ($insert_docs as $insert_doc) {
                        $docs = [
                            'INSTITUTION_ID' => (int)$id,
                            'FILE'           => $insert_doc
                        ];
                        $this->db->insert('INSTITUTION_FILES', $docs);
                    }
                    redirect('/cp/organisations');
                }
            }

            $data['item'] = $this->db->query('SELECT * FROM INSTITUTIONS WHERE ID = '.$id)->row_array();
            if (empty($data['item'])) {
                show_404();
            } else {
                $temp_metros = $this->Institution_metros_model->get_institution_metro($id);

                foreach ($temp_metros as $metro) {
                    $data['current_metros'][(int)$metro['METRO_ID']] = 1;
                }

                $temp_departments = $this->Institution_departments_model->get_institution_departments($id);

                foreach ($temp_departments as $department) {
                    $data['current_departments'][(int)$department['DEPARTMENT_ID']] = 1;
                }
            }

        } else {
            show_404();
        }

        $data['departments'] = $this->db->query('SELECT * FROM MEDICAL_INSTITUTION_DEPARTMENT ORDER BY NAME')->result_array();
        $data['types'] = $this->db->query("SELECT * FROM INSTITUTION_TYPES ORDER BY NAME")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM BRANCHES WHERE INSTITUTION_ID = $id ORDER BY ADDRESS")->result_array();
        $data['docs'] = $this->db->query("SELECT * FROM INSTITUTION_FILES WHERE INSTITUTION_ID = $id")->result_array();
        $data['metro'] = $this->db->query('SELECT * FROM METRO ORDER BY NAME')->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/organisations_edit', $data);
    }

    public function delete_organisations_doc($id)
    {
        if (!empty($id)) {
            $this->db->delete('INSTITUTION_FILES', ['ID' => $id]);
            if (!empty($_GET['back_url'])) {
                redirect($_GET['back_url']);
            } else {
                redirect('cp/organisations/');
            }
        } else {
            if (!empty($_GET['back_url'])) {
                redirect($_GET['back_url']);
            } else {
                redirect('cp/organisations/');
            }
        }
    }

    public function organisations_delete($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $this->db->query('DELETE FROM INSTITUTIONS WHERE ID = '.$id);
        }
        redirect('/cp/organisations');
    }

    public function organisation_branches()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 25;
        $where = "";
        $where_array = array();
        $pages = 0;
        $offset = "";

        if (!empty($_GET['pages']))
            $pages = ((int)$_GET['pages']) * ($per_page);

        $limit = "LIMIT $per_page";
        $offset = "OFFSET $pages";

        if (!empty($_GET['id'])) {
            $where_id = (int)$_GET['id'];
            $where_array[] = "( BRANCHES.ID = $where_id )";
        }
        if (!empty($_GET['status'])) {
            $status = (int)$_GET['status']-1;
            $where_array[] = "( BRANCHES.PREMODERATE = $status )";
        }
        if (!empty($_GET['institution_id'])) {

            $_GET['institution_id'] = (int)$_GET['institution_id'];

            if (!empty($_GET['institution_id'])) {
                $where_array[] = "( BRANCHES.INSTITUTION_ID = ".$_GET['institution_id']." )";
            }
        }
        if (!empty($_GET['brand'])) {
            $brand = mysqli_real_escape_string($this->db->conn_id, $_GET['brand']);
            $where_array[] = "( INSTITUTIONS.NAME LIKE '%$brand%' )";
        }
        if (!empty($_GET['legal'])) {
            $legal = mysqli_real_escape_string($this->db->conn_id, $_GET['legal']);
            $where_array[] = "( INSTITUTIONS.SHORTNAME LIKE '%$legal%' )";
        }
        if (!empty($_GET['inn'])) {
            $inn = mysqli_real_escape_string($this->db->conn_id, $_GET['inn']);
            $where_array[] = "( INSTITUTIONS.INN LIKE '%$inn%' )";
        }
        if (!empty($_GET['category_id'])) {

            $_GET['category_id'] = (int)$_GET['category_id'];

            if (!empty($_GET['category_id'])) {
                $where_array[] = "( INSTITUTIONS.CATEGORY_ID = ".(int)$_GET['category_id']." )";
            }
        }

        if (!empty($_GET['type_id'])) {

            $_GET['type_id'] = (int)$_GET['type_id'];

            if (!empty($_GET['type_id'])) {
                $where_array[] = "( INSTITUTIONS.TYPE_ID = ".(int)$_GET['type_id']." )";
            }
        }

        if (!empty($_GET['last_change'])) {
            $last_change = explode(".", $_GET['last_change']);
            if (!empty($last_change[0]) && !empty($last_change[1]) && !empty($last_change[2])) {
                $change_date = $last_change[2]."-".$last_change[1]."-".$last_change[0];
                $change_date = mysqli_real_escape_string($this->db->conn_id, $change_date);
                $where_array[] = "( BRANCHES.UPDATED_AT LIKE '%$change_date%' )";
            }
        }

        if (!empty($where_array))
            $where = "WHERE ".implode(' AND ', $where_array);

        $data['catalog'] = $this->db->query("SELECT
            BRANCHES.*,
            INSTITUTIONS.NAME NAME,
            INSTITUTIONS.SHORTNAME,
            INSTITUTIONS.LOGO LOGO,
            METRO.NAME METRO,
            CATEGORY.NAME CATEGORY,
            INSTITUTION_TYPES.NAME TYPE
            FROM BRANCHES
            LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
            LEFT JOIN CATEGORY ON CATEGORY.ID = INSTITUTIONS.CATEGORY_ID
            LEFT JOIN INSTITUTION_TYPES ON INSTITUTION_TYPES.ID = INSTITUTIONS.TYPE_ID
            LEFT JOIN METRO ON METRO.ID = BRANCHES.METRO_ID
            $where")->result_array();
        $catalog_quantity = $this->db->query("SELECT
            COUNT(0) COUNT
            FROM BRANCHES
            LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
            LEFT JOIN CATEGORY ON CATEGORY.ID = INSTITUTIONS.CATEGORY_ID
            LEFT JOIN INSTITUTION_TYPES ON INSTITUTION_TYPES.ID = INSTITUTIONS.TYPE_ID
            LEFT JOIN METRO ON METRO.ID = BRANCHES.METRO_ID
            $where")->row_array()['COUNT'];

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['types'] = $this->db->query('SELECT * FROM INSTITUTION_TYPES ORDER BY NAME')->result_array();
        $data['brands'] = $this->db->query('SELECT * FROM INSTITUTIONS ORDER BY NAME')->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('cp/organisation_branches', $data);
    }

    public function organisation_branches_add()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        if (isset($_POST['submit'])) {
            $insert_data = array();

            if (!empty($_FILES['logo']) && is_uploaded_file($_FILES['logo']['tmp_name'])) {
                $name = $_FILES['logo']['name'];
                $filename_array = explode('.', $name);
                $extension = end($filename_array);
                $new_name = uniqid();
                $new_path = "/images/organisations/" . $new_name . "." . $extension;
                $new_thumb_path = "/images/organisations/" . $new_name . "_thumb." . $extension;
                $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                move_uploaded_file($_FILES["logo"]["tmp_name"], $path);
                $insert_data['LOGO'] = $new_path;

                $config['image_library'] = 'gd2';
                $config['source_image'] = $path;
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width']         = 240;
                $config['height']       = 240;

                $this->load->library('image_lib', $config);

                if ( ! $this->image_lib->resize())
                {
                    echo $this->image_lib->display_errors();
//                    exit();
                }

                $insert_data['LOGO_THUMB'] = $new_thumb_path;
            }
            if (!empty($_POST['category_id']))
                $insert_data['CATEGORY_ID'] = $_POST['category_id'];
            else
                $insert_data['CATEGORY_ID'] = NULL;
            if (!empty($_POST['type_id']))
                $insert_data['TYPE_ID'] = $_POST['type_id'];
            else
                $insert_data['TYPE_ID'] = null;
            if (!empty($_POST['name']))
                $insert_data['NAME'] = $_POST['name'];
            if (!empty($_POST['institution_id']))
                $insert_data['INSTITUTION_ID'] = $_POST['institution_id'];
            if (!empty($_POST['description']))
                $insert_data['DESCRIPTION'] = $_POST['description'];
            else
                $insert_data['DESCRIPTION'] = NULL;

            if (!empty($_POST['legal_address_index']))
                $insert_data['LEGAL_ADDRESS_INDEX'] = $_POST['legal_address_index'];
            else
                $insert_data['LEGAL_ADDRESS_INDEX'] = NULL;
            if (!empty($_POST['legal_address_region']))
                $insert_data['LEGAL_ADDRESS_REGION'] = $_POST['legal_address_region'];
            else
                $insert_data['LEGAL_ADDRESS_REGION'] = NULL;
            if (!empty($_POST['legal_address_city']))
                $insert_data['LEGAL_ADDRESS_CITY'] = $_POST['legal_address_city'];
            else
                $insert_data['LEGAL_ADDRESS_CITY'] = NULL;
            if (!empty($_POST['legal_address_street']))
                $insert_data['LEGAL_ADDRESS_STREET'] = $_POST['legal_address_street'];
            else
                $insert_data['LEGAL_ADDRESS_STREET'] = NULL;
            if (!empty($_POST['legal_address_house']))
                $insert_data['LEGAL_ADDRESS_HOUSE'] = $_POST['legal_address_house'];
            else
                $insert_data['LEGAL_ADDRESS_HOUSE'] = NULL;
            if (!empty($_POST['legal_address_building']))
                $insert_data['LEGAL_ADDRESS_BUILDING'] = $_POST['legal_address_building'];
            else
                $insert_data['LEGAL_ADDRESS_BUILDING'] = NULL;
            if (!empty($_POST['legal_address_office']))
                $insert_data['LEGAL_ADDRESS_OFFICE'] = $_POST['legal_address_office'];
            else
                $insert_data['LEGAL_ADDRESS_OFFICE'] = NULL;

            if (!empty($_POST['address_index']))
                $insert_data['ADDRESS_INDEX'] = $_POST['address_index'];
            else
                $insert_data['ADDRESS_INDEX'] = NULL;
            if (!empty($_POST['address_region']))
                $insert_data['ADDRESS_REGION'] = $_POST['address_region'];
            else
                $insert_data['ADDRESS_REGION'] = NULL;
            if (!empty($_POST['address_city']))
                $insert_data['ADDRESS_CITY'] = $_POST['address_city'];
            else
                $insert_data['ADDRESS_CITY'] = NULL;
            if (!empty($_POST['address_street']))
                $insert_data['ADDRESS_STREET'] = $_POST['address_street'];
            else
                $insert_data['ADDRESS_STREET'] = NULL;
            if (!empty($_POST['address_house']))
                $insert_data['ADDRESS_HOUSE'] = $_POST['address_house'];
            else
                $insert_data['ADDRESS_HOUSE'] = NULL;
            if (!empty($_POST['address_building']))
                $insert_data['ADDRESS_BUILDING'] = $_POST['address_building'];
            else
                $insert_data['ADDRESS_BUILDING'] = NULL;
            if (!empty($_POST['address_office']))
                $insert_data['ADDRESS_OFFICE'] = $_POST['address_office'];
            else
                $insert_data['ADDRESS_OFFICE'] = NULL;

            if (!empty($_POST['license']))
                $insert_data['LICENSE'] = $_POST['license'];
            else
                $insert_data['LICENSE'] = NULL;
            if (!empty($_POST['phone1']))
                $insert_data['PHONE1'] = $_POST['phone1'];
            if (!empty($_POST['phone2']))
                $insert_data['PHONE2'] = $_POST['phone2'];
            if (!empty($_POST['phone3']))
                $insert_data['PHONE3'] = $_POST['phone3'];
            if (!empty($_POST['website1']))
                $insert_data['WEBSITE1'] = $_POST['website1'];
            if (!empty($_POST['website2']))
                $insert_data['WEBSITE2'] = $_POST['website2'];
            if (!empty($_POST['website3']))
                $insert_data['WEBSITE3'] = $_POST['website3'];

            $files = [];
            $insert_docs = [];

            foreach ($_FILES['docs'] as $attr => $doc) {
                foreach ($doc as $key => $value) {
                    $files[$key][$attr] = $value;
                }
            }

            if (!empty($files)) {
                foreach ($files as $doc) {
                    if (!empty($doc['name'])) {
                        $name = $doc['name'];
                        $filename_array = explode('.', $name);
                        $extension = end($filename_array);
                        $new_path = "/images/licenses/" . uniqid() . "." . $extension;
                        $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                        move_uploaded_file($doc["tmp_name"], $path);
                        $insert_docs[] = $new_path;
                    }
                }
            }

            if (!empty($_POST['latitude']))
                $insert_data['LATITUDE'] = $_POST['latitude'];
            if (!empty($_POST['longitude']))
                $insert_data['LONGITUDE'] = $_POST['longitude'];
            if (!empty($_POST['email']))
                $insert_data['EMAIL'] = $_POST['email'];
            else
                $insert_data['EMAIL'] = NULL;
            if (!empty($_POST['ogrn']))
                $insert_data['OGRN'] = $_POST['ogrn'];
            else
                $insert_data['OGRN'] = NULL;
            if (!empty($_POST['inn']))
                $insert_data['INN'] = $_POST['inn'];
            else
                $insert_data['INN'] = NULL;
            if (!empty($_POST['okved']))
                $insert_data['OKVED'] = $_POST['okved'];
            else
                $insert_data['OKVED'] = NULL;
            if (!empty($_POST['okpo']))
                $insert_data['OKPO'] = $_POST['okpo'];
            else
                $insert_data['OKPO'] = NULL;
            if (!empty($_POST['kpp']))
                $insert_data['KPP'] = $_POST['kpp'];
            else
                $insert_data['KPP'] = NULL;
            if (!empty($_POST['oktmo']))
                $insert_data['OKTMO'] = $_POST['oktmo'];
            else
                $insert_data['OKTMO'] = NULL;
            if (!empty($_POST['account']))
                $insert_data['ACCOUNT'] = $_POST['account'];
            else
                $insert_data['ACCOUNT'] = NULL;
            if (!empty($_POST['bank']))
                $insert_data['BANK'] = $_POST['bank'];
            else
                $insert_data['BANK'] = NULL;
            if (!empty($_POST['bik']))
                $insert_data['BIK'] = $_POST['bik'];
            else
                $insert_data['BIK'] = NULL;
            if (!empty($_POST['monday_from']))
                $insert_data['MONDAY_WORK_FROM'] = $_POST['monday_from'];
            if (!empty($_POST['monday_to']))
                $insert_data['MONDAY_WORK_TO'] = $_POST['monday_to'];
            if (!empty($_POST['tuesday_from']))
                $insert_data['TUESDAY_WORK_FROM'] = $_POST['tuesday_from'];
            if (!empty($_POST['tuesday_to']))
                $insert_data['TUESDAY_WORK_TO'] = $_POST['tuesday_to'];
            if (!empty($_POST['wednesday_from']))
                $insert_data['WEDNESDAY_WORK_FROM'] = $_POST['wednesday_from'];
            if (!empty($_POST['wednesday_to']))
                $insert_data['WEDNESDAY_WORK_TO'] = $_POST['wednesday_to'];
            if (!empty($_POST['thursday_from']))
                $insert_data['THURSDAY_WORK_FROM'] = $_POST['thursday_from'];
            if (!empty($_POST['thursday_to']))
                $insert_data['THURSDAY_WORK_TO'] = $_POST['thursday_to'];
            if (!empty($_POST['friday_from']))
                $insert_data['FRIDAY_WORK_FROM'] = $_POST['friday_from'];
            if (!empty($_POST['friday_to']))
                $insert_data['FRIDAY_WORK_TO'] = $_POST['friday_to'];
            if (!empty($_POST['saturday_from']))
                $insert_data['SATURDAY_WORK_FROM'] = $_POST['saturday_from'];
            if (!empty($_POST['saturday_to']))
                $insert_data['SATURDAY_WORK_To'] = $_POST['saturday_to'];
            if (!empty($_POST['sunday_from']))
                $insert_data['SUNDAY_WORK_FROM'] = $_POST['sunday_from'];
            if (!empty($_POST['sunday_to']))
                $insert_data['SUNDAY_WORK_TO'] = $_POST['sunday_to'];

            if (!empty($_POST['monday_break_from']))
                $insert_data['MONDAY_BREAK_FROM'] = $_POST['monday_break_from'];
            if (!empty($_POST['monday_break_to']))
                $insert_data['MONDAY_BREAK_TO'] = $_POST['monday_break_to'];
            if (!empty($_POST['tuesday_break_from']))
                $insert_data['TUESDAY_BREAK_FROM'] = $_POST['tuesday_break_from'];
            if (!empty($_POST['tuesday_break_to']))
                $insert_data['TUESDAY_BREAK_TO'] = $_POST['tuesday_break_to'];
            if (!empty($_POST['wednesday_break_from']))
                $insert_data['WEDNESDAY_BREAK_FROM'] = $_POST['wednesday_break_from'];
            if (!empty($_POST['wednesday_break_to']))
                $insert_data['WEDNESDAY_BREAK_TO'] = $_POST['wednesday_break_to'];
            if (!empty($_POST['thursday_break_from']))
                $insert_data['THURSDAY_BREAK_FROM'] = $_POST['thursday_break_from'];
            if (!empty($_POST['thursday_break_to']))
                $insert_data['THURSDAY_BREAK_TO'] = $_POST['thursday_break_to'];
            if (!empty($_POST['friday_break_from']))
                $insert_data['FRIDAY_BREAK_FROM'] = $_POST['friday_break_from'];
            if (!empty($_POST['friday_break_to']))
                $insert_data['FRIDAY_BREAK_TO'] = $_POST['friday_break_to'];
            if (!empty($_POST['saturday_break_from']))
                $insert_data['SATURDAY_BREAK_FROM'] = $_POST['saturday_break_from'];
            if (!empty($_POST['saturday_break_to']))
                $insert_data['SATURDAY_BREAK_To'] = $_POST['saturday_break_to'];
            if (!empty($_POST['sunday_break_from']))
                $insert_data['SUNDAY_BREAK_FROM'] = $_POST['sunday_break_from'];
            if (!empty($_POST['sunday_break_to']))
                $insert_data['SUNDAY_BREAK_TO'] = $_POST['sunday_break_to'];
            if (!empty($_POST['chief_physician']))
                $insert_data['CHIEF_PHYSICIAN'] = $_POST['chief_physician'];
            else
                $insert_data['CHIEF_PHYSICIAN'] = NULL;
            if (!empty($_POST['chief_accountant']))
                $insert_data['CHIEF_ACCOUNTANT'] = $_POST['chief_accountant'];
            else
                $insert_data['CHIEF_ACCOUNTANT'] = NULL;
            if (!empty($_POST['ceo']))
                $insert_data['CEO'] = $_POST['ceo'];
            else
                $insert_data['CEO'] = NULL;



            if (!empty($_POST['state']) && $_POST['state'] == "true")
                $insert_data['STATE'] = 1;
            else
                $insert_data['STATE'] = 0;
            //------------------------
            if (!empty($_POST['private']) && $_POST['private'] == "true")
                $insert_data['PRIVATE'] = 1;
            else
                $insert_data['PRIVATE'] = 0;
            //------------------------
            if (!empty($_POST['children']) && $_POST['children'] == "true")
                $insert_data['CHILDREN'] = 1;
            else
                $insert_data['CHILDREN'] = 0;
            //------------------------
            if (!empty($_POST['ambulance']) && $_POST['ambulance'] == "true")
                $insert_data['AMBULANCE'] = 1;
            else
                $insert_data['AMBULANCE'] = 0;
            //------------------------
            if (!empty($_POST['house']) && $_POST['house'] == "true")
                $insert_data['HOUSE'] = 1;
            else
                $insert_data['HOUSE'] = 0;
            //------------------------
            if (!empty($_POST['booking']) && $_POST['booking'] == "true")
                $insert_data['BOOKING'] = 1;
            else
                $insert_data['BOOKING'] = 0;
            //------------------------
            if (!empty($_POST['delivery']) && $_POST['delivery'] == "true")
                $insert_data['DELIVERY'] = 1;
            else
                $insert_data['DELIVERY'] = 0;
            //------------------------
            if (!empty($_POST['daynight']) && $_POST['daynight'] == "true")
                $insert_data['DAYNIGHT'] = 1;
            else
                $insert_data['DAYNIGHT'] = 0;
            //------------------------
            if (!empty($_POST['dms']) && $_POST['dms'] == "true")
                $insert_data['DMS'] = 1;
            else
                $insert_data['DMS'] = 0;
            //------------------------
            if (!empty($_POST['dlo']) && $_POST['dlo'] == "true")
                $insert_data['DLO'] = 1;
            else
                $insert_data['DLO'] = 0;
            //------------------------
            if (!empty($_POST['optics']) && $_POST['optics'] == "true")
                $insert_data['OPTICS'] = 1;
            else
                $insert_data['OPTICS'] = 0;
            //------------------------
            if (!empty($_POST['rpo']) && $_POST['rpo'] == "true")
                $insert_data['RPO'] = 1;
            else
                $insert_data['RPO'] = 0;
            //------------------------
            if (!empty($_POST['homeopathy']) && $_POST['homeopathy'] == "true")
                $insert_data['HOMEOPATHY'] = 1;
            else
                $insert_data['HOMEOPATHY'] = 0;
            //------------------------




            if (!empty($_POST['moderated']) && $_POST['moderated'] == "true")
                $insert_data['PREMODERATE'] = 1;

            if (!empty($insert_data)) {
                $insert_data['UPDATED_AT'] = date('Y-m-d H:i:s');

                $this->db->insert('BRANCHES', $insert_data);

                $inserted = $this->db->insert_id();

                if (!empty($_POST['metro_id']) && is_array($_POST['metro_id'])) {

                    $current_metros = [];
                    $temp_metros = [];

                    if (!empty($inserted))
                        $temp_metros = $this->Branch_metros_model->get_branch_metro($inserted);

                    foreach ($temp_metros as $metro) {
                        $current_metros[(int)$metro['METRO_ID']] = 1;
                    }

                    foreach ($_POST['metro_id'] as $metro_id) {
                        if (!empty($current_metros[$metro_id]))
                            unset($current_metros[$metro_id]);
                        else
                            $this->Branch_metros_model->add_metro($inserted, $metro_id);
                    }

                    if (!empty($current_metros)) {
                        foreach ($current_metros as $current_metro => $dummy) {
                            $metros_to_delete[] = $current_metro;
                        }

                        $this->Branch_metros_model->delete_metros($inserted, $metros_to_delete);
                    }
                } else {
                    $this->Branch_metros_model->drop_branch_metro($inserted);
                }

                // внутренние отделения

                if (!empty($_POST['department_id']) && is_array($_POST['department_id'])) {

                    $current_departments = [];
                    $temp_departments = [];

                    if (!empty($inserted))
                        $temp_departments = $this->Branch_departments_model->get_branch_departments($inserted);

                    foreach ($temp_departments as $department) {
                        $current_departments[(int)$department['DEPARTMENT_ID']] = 1;
                    }

                    foreach ($_POST['department_id'] as $department_id) {
                        if (!empty($current_departments[$department_id]))
                            unset($current_departments[$department_id]);
                        else
                            $this->Branch_departments_model->add_department($inserted, $department_id);
                    }

                    if (!empty($current_departments)) {
                        foreach ($current_departments as $current_department => $dummy) {
                            $departments_to_delete[] = $current_department;
                        }

                        $this->Branch_departments_model->delete_departments($inserted, $departments_to_delete);
                    }
                } else {
                    $this->Branch_departments_model->drop_branch_departments($inserted);
                }

                if (!empty($inserted)) {
                    foreach ($insert_docs as $insert_doc) {
                        $docs = [
                            'BRANCH_ID' => $inserted,
                            'FILE'           => $insert_doc
                        ];
                        $this->db->insert('BRANCH_FILES', $docs);
                    }

                    redirect('/cp/organisation_branches');
                }
            }
        }

        $data['departments'] = $this->db->query('SELECT * FROM MEDICAL_INSTITUTION_DEPARTMENT ORDER BY NAME')->result_array();
        $data['types'] = $this->db->query("SELECT * FROM INSTITUTION_TYPES ORDER BY NAME")->result_array();
        $data['institutions'] = $this->db->query('SELECT * FROM INSTITUTIONS ORDER BY SHORTNAME')->result_array();
        $data['metro'] = $this->db->query('SELECT * FROM METRO ORDER BY NAME')->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('cp/organisation_branches_add', $data);
    }

    public function organisation_branches_edit($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        $data['id'] = $id;

        if (!empty($id)) {
            if (isset($_POST['submit'])) {
                $insert_data = array();

                if (!empty($_FILES['photo']) && is_uploaded_file($_FILES['photo']['tmp_name'])) {
                    $name = $_FILES['photo']['name'];
                    $filename_array = explode('.', $name);
                    $extension = end($filename_array);
                    $new_path = "/images/organisations/" . uniqid() . "." . $extension;
                    $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                    move_uploaded_file($_FILES["photo"]["tmp_name"], $path);
                    $insert_data['LOGO'] = $new_path;
                }
                if (!empty($_POST['category_id']))
                    $insert_data['CATEGORY_ID'] = $_POST['category_id'];
                else
                    $insert_data['CATEGORY_ID'] = NULL;
                if (!empty($_POST['type_id']))
                    $insert_data['TYPE_ID'] = $_POST['type_id'];
                else
                    $insert_data['TYPE_ID'] = NULL;
                if (!empty($_POST['name']))
                    $insert_data['NAME'] = $_POST['name'];
                if (!empty($_POST['institution_id']))
                    $insert_data['INSTITUTION_ID'] = $_POST['institution_id'];
                else
                    $insert_data['INSTITUTION_ID'] = "";
                if (!empty($_POST['description']))
                    $insert_data['DESCRIPTION'] = $_POST['description'];
                else
                    $insert_data['DESCRIPTION'] = NULL;

                if (!empty($_POST['legal_address_index']))
                    $insert_data['LEGAL_ADDRESS_INDEX'] = $_POST['legal_address_index'];
                else
                    $insert_data['LEGAL_ADDRESS_INDEX'] = NULL;
                if (!empty($_POST['legal_address_region']))
                    $insert_data['LEGAL_ADDRESS_REGION'] = $_POST['legal_address_region'];
                else
                    $insert_data['LEGAL_ADDRESS_REGION'] = NULL;
                if (!empty($_POST['legal_address_city']))
                    $insert_data['LEGAL_ADDRESS_CITY'] = $_POST['legal_address_city'];
                else
                    $insert_data['LEGAL_ADDRESS_CITY'] = NULL;
                if (!empty($_POST['legal_address_street']))
                    $insert_data['LEGAL_ADDRESS_STREET'] = $_POST['legal_address_street'];
                else
                    $insert_data['LEGAL_ADDRESS_STREET'] = NULL;
                if (!empty($_POST['legal_address_house']))
                    $insert_data['LEGAL_ADDRESS_HOUSE'] = $_POST['legal_address_house'];
                else
                    $insert_data['LEGAL_ADDRESS_HOUSE'] = NULL;
                if (!empty($_POST['legal_address_building']))
                    $insert_data['LEGAL_ADDRESS_BUILDING'] = $_POST['legal_address_building'];
                else
                    $insert_data['LEGAL_ADDRESS_BUILDING'] = NULL;
                if (!empty($_POST['legal_address_office']))
                    $insert_data['LEGAL_ADDRESS_OFFICE'] = $_POST['legal_address_office'];
                else
                    $insert_data['LEGAL_ADDRESS_OFFICE'] = NULL;

                if (!empty($_POST['address_index']))
                    $insert_data['ADDRESS_INDEX'] = $_POST['address_index'];
                else
                    $insert_data['ADDRESS_INDEX'] = NULL;

                if (!empty($_POST['address_region']))
                    $insert_data['ADDRESS_REGION'] = $_POST['address_region'];
                else
                    $insert_data['ADDRESS_REGION'] = NULL;

                if (!empty($_POST['address_city']))
                    $insert_data['ADDRESS_CITY'] = $_POST['address_city'];
                else
                    $insert_data['ADDRESS_CITY'] = NULL;

                if (!empty($_POST['address_street']))
                    $insert_data['ADDRESS_STREET'] = $_POST['address_street'];
                else
                    $insert_data['ADDRESS_STREET'] = NULL;

                if (!empty($_POST['address_house']))
                    $insert_data['ADDRESS_HOUSE'] = $_POST['address_house'];
                else
                    $insert_data['ADDRESS_HOUSE'] = NULL;

                if (!empty($_POST['address_building']))
                    $insert_data['ADDRESS_BUILDING'] = $_POST['address_building'];
                else
                    $insert_data['ADDRESS_BUILDING'] = NULL;

                if (!empty($_POST['address_office']))
                    $insert_data['ADDRESS_OFFICE'] = $_POST['address_office'];
                else
                    $insert_data['ADDRESS_OFFICE'] = NULL;

                if (!empty($_POST['metro_id']) && is_array($_POST['metro_id'])) {

                    $current_metros = [];

                    $temp_metros = $this->Branch_metros_model->get_branch_metro($id);

                    foreach ($temp_metros as $metro) {
                        $current_metros[(int)$metro['METRO_ID']] = 1;
                    }

                    foreach ($_POST['metro_id'] as $metro_id) {
                        if (!empty($current_metros[$metro_id]))
                            unset($current_metros[$metro_id]);
                        else
                            $this->Branch_metros_model->add_metro($id, $metro_id);
                    }

                    if (!empty($current_metros)) {
                        foreach ($current_metros as $current_metro => $dummy) {
                            $metros_to_delete[] = $current_metro;
                        }

                        $this->Branch_metros_model->delete_metros($id, $metros_to_delete);
                    }
                } else {
                    $this->Branch_metros_model->drop_branch_metro($id);
                }

                // Внутренние отделения

                if (!empty($_POST['department_id']) && is_array($_POST['department_id'])) {

                    $current_departments = [];
                    $temp_departments = $this->Branch_departments_model->get_branch_departments($id);

                    foreach ($temp_departments as $department) {
                        $current_departments[(int)$department['DEPARTMENT_ID']] = 1;
                    }

                    foreach ($_POST['department_id'] as $department_id) {
                        if (!empty($current_departments[$department_id]))
                            unset($current_departments[$department_id]);
                        else
                            $this->Branch_departments_model->add_department($id, $department_id);
                    }

                    if (!empty($current_departments)) {
                        foreach ($current_departments as $current_department => $dummy) {
                            $departments_to_delete[] = $current_department;
                        }

                        $this->Branch_departments_model->delete_departments($id, $departments_to_delete);
                    }
                } else {
                    $this->Branch_departments_model->drop_branch_departments($id);
                }

                if (!empty($_POST['license']))
                    $insert_data['LICENSE'] = $_POST['license'];
                else
                    $insert_data['LICENSE'] = NULL;
                if (!empty($_POST['phone1']))
                    $insert_data['PHONE1'] = $_POST['phone1'];
                else
                    $insert_data['PHONE1'] = NULL;
                if (!empty($_POST['phone2']))
                    $insert_data['PHONE2'] = $_POST['phone2'];
                else
                    $insert_data['PHONE2'] = NULL;
                if (!empty($_POST['phone3']))
                    $insert_data['PHONE3'] = $_POST['phone3'];
                else
                    $insert_data['PHONE3'] = NULL;
                if (!empty($_POST['website1']))
                    $insert_data['WEBSITE1'] = $_POST['website1'];
                if (!empty($_POST['website2']))
                    $insert_data['WEBSITE2'] = $_POST['website2'];
                if (!empty($_POST['website3']))
                    $insert_data['WEBSITE3'] = $_POST['website3'];

                $files = [];
                $insert_docs = [];

                foreach ($_FILES['docs'] as $attr => $doc) {
                    foreach ($doc as $key => $value) {
                        $files[$key][$attr] = $value;
                    }
                }

                if (!empty($files)) {
                    foreach ($files as $doc) {
                        $name = $doc['name'];
                        $filename_array = explode('.', $name);
                        $extension = end($filename_array);
                        $new_path = "/images/licenses/" . uniqid() . "." . $extension;
                        $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                        move_uploaded_file($doc["tmp_name"], $path);
                        $insert_docs[] = $new_path;
                    }
                }

                if (!empty($_POST['latitude']))
                    $insert_data['LATITUDE'] = $_POST['latitude'];
                else
                    $insert_data['LATITUDE'] = NULL;
                if (!empty($_POST['longitude']))
                    $insert_data['LONGITUDE'] = $_POST['longitude'];
                else
                    $insert_data['LONGITUDE'] = NULL;
                if (!empty($_POST['email']))
                    $insert_data['EMAIL'] = $_POST['email'];
                else
                    $insert_data['EMAIL'] = NULL;
                if (!empty($_POST['ogrn']))
                    $insert_data['OGRN'] = $_POST['ogrn'];
                else
                    $insert_data['OGRN'] = NULL;
                if (!empty($_POST['inn']))
                    $insert_data['INN'] = $_POST['inn'];
                else
                    $insert_data['INN'] = NULL;
                if (!empty($_POST['okved']))
                    $insert_data['OKVED'] = $_POST['okved'];
                else
                    $insert_data['OKVED'] = NULL;
                if (!empty($_POST['okpo']))
                    $insert_data['OKPO'] = $_POST['okpo'];
                else
                    $insert_data['OKPO'] = NULL;
                if (!empty($_POST['kpp']))
                    $insert_data['KPP'] = $_POST['kpp'];
                else
                    $insert_data['KPP'] = NULL;
                if (!empty($_POST['oktmo']))
                    $insert_data['OKTMO'] = $_POST['oktmo'];
                else
                    $insert_data['OKTMO'] = NULL;
                if (!empty($_POST['account']))
                    $insert_data['ACCOUNT'] = $_POST['account'];
                else
                    $insert_data['ACCOUNT'] = NULL;
                if (!empty($_POST['bank']))
                    $insert_data['BANK'] = $_POST['bank'];
                else
                    $insert_data['BANK'] = NULL;
                if (!empty($_POST['bik']))
                    $insert_data['BIK'] = $_POST['bik'];
                else
                    $insert_data['BIK'] = NULL;
                if (!empty($_POST['monday_from']))
                    $insert_data['MONDAY_WORK_FROM'] = $_POST['monday_from'];
                if (!empty($_POST['monday_to']))
                    $insert_data['MONDAY_WORK_TO'] = $_POST['monday_to'];
                if (!empty($_POST['tuesday_from']))
                    $insert_data['TUESDAY_WORK_FROM'] = $_POST['tuesday_from'];
                if (!empty($_POST['tuesday_to']))
                    $insert_data['TUESDAY_WORK_TO'] = $_POST['tuesday_to'];
                if (!empty($_POST['wednesday_from']))
                    $insert_data['WEDNESDAY_WORK_FROM'] = $_POST['wednesday_from'];
                if (!empty($_POST['wednesday_to']))
                    $insert_data['WEDNESDAY_WORK_TO'] = $_POST['wednesday_to'];
                if (!empty($_POST['thursday_from']))
                    $insert_data['THURSDAY_WORK_FROM'] = $_POST['thursday_from'];
                if (!empty($_POST['thursday_to']))
                    $insert_data['THURSDAY_WORK_TO'] = $_POST['thursday_to'];
                if (!empty($_POST['friday_from']))
                    $insert_data['FRIDAY_WORK_FROM'] = $_POST['friday_from'];
                if (!empty($_POST['friday_to']))
                    $insert_data['FRIDAY_WORK_TO'] = $_POST['friday_to'];
                if (!empty($_POST['saturday_from']))
                    $insert_data['SATURDAY_WORK_FROM'] = $_POST['saturday_from'];
                if (!empty($_POST['saturday_to']))
                    $insert_data['SATURDAY_WORK_To'] = $_POST['saturday_to'];
                if (!empty($_POST['sunday_from']))
                    $insert_data['SUNDAY_WORK_FROM'] = $_POST['sunday_from'];
                if (!empty($_POST['sunday_to']))
                    $insert_data['SUNDAY_WORK_TO'] = $_POST['sunday_to'];

                if (!empty($_POST['monday_break_from']))
                    $insert_data['MONDAY_BREAK_FROM'] = $_POST['monday_break_from'];
                if (!empty($_POST['monday_break_to']))
                    $insert_data['MONDAY_BREAK_TO'] = $_POST['monday_break_to'];
                if (!empty($_POST['tuesday_break_from']))
                    $insert_data['TUESDAY_BREAK_FROM'] = $_POST['tuesday_break_from'];
                if (!empty($_POST['tuesday_break_to']))
                    $insert_data['TUESDAY_BREAK_TO'] = $_POST['tuesday_break_to'];
                if (!empty($_POST['wednesday_break_from']))
                    $insert_data['WEDNESDAY_BREAK_FROM'] = $_POST['wednesday_break_from'];
                if (!empty($_POST['wednesday_break_to']))
                    $insert_data['WEDNESDAY_BREAK_TO'] = $_POST['wednesday_break_to'];
                if (!empty($_POST['thursday_break_from']))
                    $insert_data['THURSDAY_BREAK_FROM'] = $_POST['thursday_break_from'];
                if (!empty($_POST['thursday_break_to']))
                    $insert_data['THURSDAY_BREAK_TO'] = $_POST['thursday_break_to'];
                if (!empty($_POST['friday_break_from']))
                    $insert_data['FRIDAY_BREAK_FROM'] = $_POST['friday_break_from'];
                if (!empty($_POST['friday_break_to']))
                    $insert_data['FRIDAY_BREAK_TO'] = $_POST['friday_break_to'];
                if (!empty($_POST['saturday_break_from']))
                    $insert_data['SATURDAY_BREAK_FROM'] = $_POST['saturday_break_from'];
                if (!empty($_POST['saturday_break_to']))
                    $insert_data['SATURDAY_BREAK_To'] = $_POST['saturday_break_to'];
                if (!empty($_POST['sunday_break_from']))
                    $insert_data['SUNDAY_BREAK_FROM'] = $_POST['sunday_break_from'];
                if (!empty($_POST['sunday_break_to']))
                    $insert_data['SUNDAY_BREAK_TO'] = $_POST['sunday_break_to'];
                if (!empty($_POST['chief_physician']))
                    $insert_data['CHIEF_PHYSICIAN'] = $_POST['chief_physician'];
                else
                    $insert_data['CHIEF_PHYSICIAN'] = NULL;
                if (!empty($_POST['chief_accountant']))
                    $insert_data['CHIEF_ACCOUNTANT'] = $_POST['chief_accountant'];
                else
                    $insert_data['CHIEF_ACCOUNTANT'] = NULL;
                if (!empty($_POST['ceo']))
                    $insert_data['CEO'] = $_POST['ceo'];
                else
                    $insert_data['CEO'] = NULL;



                if (!empty($_POST['state']) && $_POST['state'] == "true")
                    $insert_data['STATE'] = 1;
                else
                    $insert_data['STATE'] = 0;
                //------------------------
                if (!empty($_POST['private']) && $_POST['private'] == "true")
                    $insert_data['PRIVATE'] = 1;
                else
                    $insert_data['PRIVATE'] = 0;
                //------------------------
                if (!empty($_POST['children']) && $_POST['children'] == "true")
                    $insert_data['CHILDREN'] = 1;
                else
                    $insert_data['CHILDREN'] = 0;
                //------------------------
                if (!empty($_POST['ambulance']) && $_POST['ambulance'] == "true")
                    $insert_data['AMBULANCE'] = 1;
                else
                    $insert_data['AMBULANCE'] = 0;
                //------------------------
                if (!empty($_POST['house']) && $_POST['house'] == "true")
                    $insert_data['HOUSE'] = 1;
                else
                    $insert_data['HOUSE'] = 0;
                //------------------------
                if (!empty($_POST['booking']) && $_POST['booking'] == "true")
                    $insert_data['BOOKING'] = 1;
                else
                    $insert_data['BOOKING'] = 0;
                //------------------------
                if (!empty($_POST['delivery']) && $_POST['delivery'] == "true")
                    $insert_data['DELIVERY'] = 1;
                else
                    $insert_data['DELIVERY'] = 0;
                //------------------------
                if (!empty($_POST['daynight']) && $_POST['daynight'] == "true")
                    $insert_data['DAYNIGHT'] = 1;
                else
                    $insert_data['DAYNIGHT'] = 0;
                //------------------------
                if (!empty($_POST['dms']) && $_POST['dms'] == "true")
                    $insert_data['DMS'] = 1;
                else
                    $insert_data['DMS'] = 0;
                //------------------------
                if (!empty($_POST['dlo']) && $_POST['dlo'] == "true")
                    $insert_data['DLO'] = 1;
                else
                    $insert_data['DLO'] = 0;
                //------------------------
                if (!empty($_POST['optics']) && $_POST['optics'] == "true")
                    $insert_data['OPTICS'] = 1;
                else
                    $insert_data['OPTICS'] = 0;
                //------------------------
                if (!empty($_POST['rpo']) && $_POST['rpo'] == "true")
                    $insert_data['RPO'] = 1;
                else
                    $insert_data['RPO'] = 0;
                //------------------------
                if (!empty($_POST['homeopathy']) && $_POST['homeopathy'] == "true")
                    $insert_data['HOMEOPATHY'] = 1;
                else
                    $insert_data['HOMEOPATHY'] = 0;
                //------------------------




                if (!empty($_POST['moderated']) && $_POST['moderated'] == "true")
                    $insert_data['PREMODERATE'] = 1;

                $insert_data['UPDATED_AT'] = date('Y-m-d H:i:s');

                if (!empty($insert_data)) {
                    $this->db->where('ID',$id);
                    $this->db->update('BRANCHES', $insert_data);

                    foreach ($insert_docs as $insert_doc) {
                        $docs = [
                            'BRANCH_ID' => (int)$id,
                            'FILE'           => $insert_doc
                        ];
                        $this->db->insert('BRANCH_FILES', $docs);
                    }

                    redirect('/cp/organisation_branches');
                }
            }

            $data['item'] = $this->db->query('SELECT * FROM BRANCHES WHERE ID = '.$id)->row_array();
            if (empty($data['item'])) {
                show_404();
            } else {
                $temp_metros = $this->Branch_metros_model->get_branch_metro($id);

                foreach ($temp_metros as $metro) {
                    $data['current_metros'][(int)$metro['METRO_ID']] = 1;
                }

                $temp_departments = $this->Branch_departments_model->get_branch_departments($id);

                foreach ($temp_departments as $department) {
                    $data['current_departments'][(int)$department['DEPARTMENT_ID']] = 1;
                }
            }
        } else {
            show_404();
        }

        $data['departments'] = $this->db->query('SELECT * FROM MEDICAL_INSTITUTION_DEPARTMENT ORDER BY NAME')->result_array();
        $data['types'] = $this->db->query("SELECT * FROM INSTITUTION_TYPES ORDER BY NAME")->result_array();
        $data['institutions'] = $this->db->query('SELECT * FROM INSTITUTIONS ORDER BY NAME')->result_array();
        $data['docs'] = $this->db->query("SELECT * FROM BRANCH_FILES WHERE BRANCH_ID = $id")->result_array();
        $data['metro'] = $this->db->query('SELECT * FROM METRO ORDER BY NAME')->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('cp/organisation_branches_edit', $data);
    }

    public function delete_branches_doc($id)
    {
        if (!empty($id)) {
            $this->db->delete('BRANCH_FILES', ['ID' => $id]);
            if (!empty($_GET['back_url'])) {
                redirect($_GET['back_url']);
            } else {
                redirect('cp/organisation_branches/');
            }
        } else {
            if (!empty($_GET['back_url'])) {
                redirect($_GET['back_url']);
            } else {
                redirect('cp/organisation_branches/');
            }
        }
    }

    public function organisation_branches_delete($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $this->db->query('DELETE FROM BRANCHES WHERE ID = '.$id);
        }
        redirect('/cp/organisation_branches');

    }


    // Прикрепление фотографий к организации
    public function organisation_add_photos($id = null)
    {
        $id = (int)$id;
        if (empty($id))
            show_404();

        $data['id'] = $id;
        $data['back_link_title']    = 'Редактирование организации';
        $data['back_link']          = 'organisations_edit';
        $data['object']             = "организации";

        if (isset($_POST['submit'])) {
            $files = [];
            $insert_docs = [];

            foreach ($_FILES['images'] as $attr => $doc) {
                foreach ($doc as $key => $value) {
                    $files[$key][$attr] = $value;
                }
            }

            if (!empty($files)) {
                foreach ($files as $doc) {
                    $name = $doc['name'];
                    $filename_array = explode('.', $name);
                    $extension = end($filename_array);
                    $new_path = "/images/licenses/" . uniqid() . "." . $extension;
                    $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                    move_uploaded_file($doc["tmp_name"], $path);
                    $insert_docs[] = $new_path;
                }
            }

            foreach ($insert_docs as $insert_doc) {
                $docs = [
                    'OBJECT'        => 1,
                    'LOADER_ID'     => $id,
                    'VALUE'         => $insert_doc
                ];
                $this->db->insert('IMAGES', $docs);
            }
        }

        $data['images'] = $this->db->query("SELECT ID, VALUE FROM IMAGES WHERE OBJECT = 1 AND LOADER_ID = $id")->result_array();
        $data['item'] = $this->db->query("SELECT NAME, SHORTNAME FROM INSTITUTIONS WHERE ID = ".$id)->row_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/images', $data);
    }

    // Прикрепление видео к организациям
    public function organisation_add_videos($id = null)
    {
        $id = (int)$id;
        if (empty($id))
            show_404();

        $data['id'] = $id;
        $data['back_link_title']    = 'Редактирование организации';
        $data['back_link']          = 'organisations_edit';
        $data['object']             = "организации";

        if (isset($_POST['submit'])) {
            if (filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
                $url = $_POST['url'];
                $result = parse_url($url);
                parse_str($result['query'], $get_params);

                if (!empty($get_params['v'])) {
                    $docs = [
                        'OBJECT'        => 1,
                        'LOADER_ID'     => $id,
                        'VALUE'         => "https://www.youtube.com/embed/".$get_params['v']
                    ];
                    $this->db->insert('VIDEOS', $docs);
                }
            }
        }

        $data['item'] = $this->db->query("SELECT NAME,SHORTNAME FROM INSTITUTIONS WHERE ID = ".$id)->row_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/videos', $data);
    }

    // Прикрепление фотографий к отделениям
    public function branches_add_photos($id = null)
    {
        $id = (int)$id;
        if (empty($id))
            show_404();

        $data['id'] = $id;
        $data['back_link_title']    = 'Редактирование отделения';
        $data['back_link']          = 'organisation_branches_edit';
        $data['object']             = "отделения";

        if (isset($_POST['submit'])) {
            $files = [];
            $insert_docs = [];

            foreach ($_FILES['images'] as $attr => $doc) {
                foreach ($doc as $key => $value) {
                    $files[$key][$attr] = $value;
                }
            }

            if (!empty($files)) {
                foreach ($files as $doc) {
                    $name = $doc['name'];
                    $filename_array = explode('.', $name);
                    $extension = end($filename_array);
                    $new_path = "/images/licenses/" . uniqid() . "." . $extension;
                    $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                    move_uploaded_file($doc["tmp_name"], $path);
                    $insert_docs[] = $new_path;
                }
            }

            foreach ($insert_docs as $insert_doc) {
                $docs = [
                    'OBJECT'        => 2,
                    'LOADER_ID'     => $id,
                    'VALUE'         => $insert_doc
                ];
                $this->db->insert('IMAGES', $docs);
            }
        }

        $data['images'] = $this->db->query("SELECT ID, VALUE FROM IMAGES WHERE OBJECT = 2 AND LOADER_ID = $id")->result_array();
        $data['item'] = $this->db->query("SELECT NAME, '' SHORTNAME FROM BRANCHES WHERE ID = ".$id)->row_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/images', $data);
    }

    // Прикрепление видео к отделениям
    public function branches_add_videos($id = null)
    {
        $id = (int)$id;
        if (empty($id))
            show_404();

        $data['id']                 = $id;
        $data['back_link_title']    = 'Редактирование отделения';
        $data['back_link']          = 'organisation_branches_edit';
        $data['object']             = "отделения";

        if (isset($_POST['submit'])) {
            if (filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
                $url = $_POST['url'];
                $result = parse_url($url);
                parse_str($result['query'], $get_params);

                if (!empty($get_params['v'])) {
                    $docs = [
                        'OBJECT'        => 2,
                        'LOADER_ID'     => $id,
                        'VALUE'         => "https://www.youtube.com/embed/".$get_params['v']
                    ];
                    $this->db->insert('VIDEOS', $docs);
                }

            }
        }

        $data['item'] = $this->db->query("SELECT NAME, '' SHORTNAME FROM BRANCHES WHERE ID = ".$id)->row_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/videos', $data);
    }

    // Прикрепление фотографий к специалистам
    public function specialists_add_photos($id = null)
    {
        $id = (int)$id;
        if (empty($id))
            show_404();

        $data['id'] = $id;
        $data['back_link_title']    = 'Редактирование специалиста';
        $data['back_link']          = 'pecialists_edit';
        $data['object']             = "специалиста";
        
        if (isset($_POST['submit'])) {
            $files = [];
            $insert_docs = [];

            foreach ($_FILES['images'] as $attr => $doc) {
                foreach ($doc as $key => $value) {
                    $files[$key][$attr] = $value;
                }
            }

            if (!empty($files)) {
                foreach ($files as $doc) {
                    $name = $doc['name'];
                    $filename_array = explode('.', $name);
                    $extension = end($filename_array);
                    $new_path = "/images/licenses/" . uniqid() . "." . $extension;
                    $path = $_SERVER['DOCUMENT_ROOT'] . $new_path;
                    move_uploaded_file($doc["tmp_name"], $path);
                    $insert_docs[] = $new_path;
                }
            }

            foreach ($insert_docs as $insert_doc) {
                $docs = [
                    'OBJECT'        => 3,
                    'LOADER_ID'     => $id,
                    'VALUE'         => $insert_doc
                ];
                $this->db->insert('IMAGES', $docs);
            }
        }

        $data['images'] = $this->db->query("SELECT ID, VALUE FROM IMAGES WHERE OBJECT = 3 AND LOADER_ID = $id")->result_array();
        $data['item'] = $this->db->query("SELECT SECOND_NAME NAME, FIRST_NAME SHORTNAME FROM SPECIALISTS WHERE ID = ".$id)->row_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/images', $data);
    }

    // Прикрепление видео к специалистам
    public function specialists_add_videos($id = null)
    {
        $id = (int)$id;
        if (empty($id))
            show_404();

        $data['id']                 = $id;
        $data['back_link_title']    = 'Редактирование специалиста';
        $data['back_link']          = 'specialists_edit';
        $data['object']             = "Специалиста";

        if (isset($_POST['submit'])) {
            if (filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
                $url = $_POST['url'];
                $result = parse_url($url);
                parse_str($result['query'], $get_params);

                if (!empty($get_params['v'])) {
                    $docs = [
                        'OBJECT'        => 3,
                        'LOADER_ID'     => $id,
                        'VALUE'         => "https://www.youtube.com/embed/".$get_params['v']
                    ];
                    $this->db->insert('VIDEOS', $docs);
                }

            }
        }

        $data['item'] = $this->db->query("SELECT SECOND_NAME NAME,FIRST_NAME SHORTNAME FROM SPECIALISTS WHERE ID = ".$id)->row_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/videos', $data);
    }

    public function rss_news()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 25;
        $where = "";
        $where_array = array();
        $pages = 0;
        $offset = "";

        if (!empty($_GET['pages']))
                $pages = ((int)$_GET['pages']) * ($per_page);

        $limit = "LIMIT $per_page";
        $offset = "OFFSET $pages";

        $data['catalog'] = $this->db->query("SELECT
            *
            FROM RSS_NEWS
            $where
            ORDER BY `DATE` DESC
            $limit
            $offset")->result_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('/cp/rss_news', $data);
    }

    public function rss_news_view($id)
    {
        $data['item'] = $this->db->query("SELECT * FROM RSS_NEWS WHERE ID = ".$id)->row_array();

        $data['unreaded'] = $this->unreaded;
        $this->load->view('cp/rss_news_view', $data);
    }

    public function rss_news_switch($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $item = $this->db->query("SELECT * FROM RSS_NEWS WHERE ID = ".$id)->row_array();
            if ($item['ACTIVE'] == 1) {
                $this->db->query('UPDATE RSS_NEWS SET ACTIVE = 0 WHERE ID = '.$id.' LIMIT 1');
            } else {
                $this->db->query('UPDATE RSS_NEWS SET ACTIVE = 1 WHERE ID = '.$id.' LIMIT 1');
            }
        }
        redirect('/cp/rss_news');
    }

    public function actions($id)
    { 
        
       
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 20;
        $pages = 0;

        if (!empty($_GET['pages']))
            $pages = ((int)$_GET['pages']) * ($per_page);

        if (!empty($_GET['organization_id']))
            $this->db->where('INSTITUTION_ID', (int)$_GET['organization_id']);

        $data['catalog'] = $this->db->get('ACTIONS', $pages, $per_page)->result_array();
        $catalog_quantity = $this->db->count_all_results('ACTIONS');

        $data['organisations'] = $this->db->order_by('BRAND', 'ASC')->get('ORGANIZATIONS')->result_array();

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;

        $this->load->view('cp/actions', $data);
    }

    public function actions_add()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        if (isset($_POST['submit'])) {
            if (!empty($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                $name = $_FILES['image']['name'];
                $filename_array = explode('.', $name);
                $extension = end($filename_array);
                $new_path = "/images/actions/".uniqid().".".$extension;
                $path = $_SERVER['DOCUMENT_ROOT'].$new_path;
                move_uploaded_file($_FILES["image"]["tmp_name"], $path);
                $insert_data['THUMB'] = $new_path;
            }

            $start_date = implode('-', array_reverse(explode('/', $_POST['start_date'])));
            $end_date = implode('-', array_reverse(explode('/', $_POST['end_date'])));

            if (!empty($_POST['name']))
                $insert_data['NAME']            = $_POST['name'];
            if (!empty($_POST['category_id']))
                $insert_data['CATEGORY_ID']     = $_POST['category_id'];
            else
                $insert_data['CATEGORY_ID']     = NULL;
            if (!empty($_POST['price']))
                $insert_data['PRICE']           = $_POST['price'];
            if (!empty($_POST['description']))
                $insert_data['DESCRIPTION']     = $_POST['description'];
            if (!empty($_POST['text']))
                $insert_data['TEXT']            = $_POST['text'];
            if (!empty($_POST['discount']))
                $insert_data['DISCOUNT']        = $_POST['discount'];
            if (!empty($_POST['start_date']))
                $insert_data['START_DATE']      = $start_date;
            if (!empty($_POST['end_date']))
                $insert_data['END_DATE']        = $end_date;
            if (!empty($_POST['address']))
                $insert_data['ADDRESS']         = $_POST['address'];
            if (!empty($_POST['metro_id']))
                $insert_data['METRO_ID']        = $_POST['metro_id'];
            if (!empty($_POST['public']) && ($_POST['public'] == 'on'))
                $insert_data['PUBLIC']          = 1;
            else
                $insert_data['PUBLIC']          = 0;
            if (!empty($_POST['active']) && ($_POST['active'] == 'on'))
                $insert_data['ACTIVE']          = 1;
            else
                $insert_data['ACTIVE']          = 0;
            if (!empty($_POST['moderated']) && ($_POST['moderated'] == 'on'))
                $insert_data['MODERATED']       = 1;
            else
                $insert_data['MODERATED']       = 0;
            if (!empty($_POST['map']) && ($_POST['map'] == 'on'))
                $insert_data['MAP']             = 1;
            else
                $insert_data['MAP']             = 0;
            if (!empty($_POST['list']) && ($_POST['list'] == 'on'))
                $insert_data['LIST']       = 1;
            else
                $insert_data['LIST']       = 0;

            $this->db->insert('ACTIONS', $insert_data);

            redirect('/cp/actions');
        }

        $data['metro'] = $this->db->query('SELECT * FROM METRO ORDER BY NAME')->result_array();
        $data['categories'] = $this->db->get('CATEGORY')->result_array();

        $data['unreaded'] = $this->unreaded;

        $this->load->view('cp/actions_add', $data);
    }

    public function actions_edit($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (empty($id))
            show_404();

        if (isset($_POST['submit'])) {

            if (!empty($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                $name = $_FILES['image']['name'];
                $filename_array = explode('.', $name);
                $extension = end($filename_array);
                $new_path = "/images/actions/".uniqid().".".$extension;
                $path = $_SERVER['DOCUMENT_ROOT'].$new_path;
                move_uploaded_file($_FILES["image"]["tmp_name"], $path);
                $insert_data['THUMB'] = $new_path;
            }

            $start_date = implode('-', array_reverse(explode('/', $_POST['start_date'])));
            $end_date = implode('-', array_reverse(explode('/', $_POST['end_date'])));

            if (!empty($_POST['name']))
                $insert_data['NAME']            = $_POST['name'];
            if (!empty($_POST['category_id']))
                $insert_data['CATEGORY_ID']     = $_POST['category_id'];
            else
                $insert_data['CATEGORY_ID']     = NULL;
            if (!empty($_POST['price']))
                $insert_data['PRICE']           = $_POST['price'];
            if (!empty($_POST['description']))
                $insert_data['DESCRIPTION']     = $_POST['description'];
            if (!empty($_POST['text']))
                $insert_data['TEXT']            = $_POST['text'];
            if (!empty($_POST['discount']))
                $insert_data['DISCOUNT']        = $_POST['discount'];
            if (!empty($_POST['start_date']))
                $insert_data['START_DATE']      = $start_date;
            if (!empty($_POST['end_date']))
                $insert_data['END_DATE']        = $end_date;
            if (!empty($_POST['address']))
                $insert_data['ADDRESS']         = $_POST['address'];
            if (!empty($_POST['metro_id']))
                $insert_data['METRO_ID']        = $_POST['metro_id'];
            if (!empty($_POST['public']) && ($_POST['public'] == 'on'))
                $insert_data['PUBLIC']          = 1;
            else
                $insert_data['PUBLIC']          = 0;
            if (!empty($_POST['active']) && ($_POST['active'] == 'on'))
                $insert_data['ACTIVE']          = 1;
            else
                $insert_data['ACTIVE']          = 0;
            if (!empty($_POST['moderated']) && ($_POST['moderated'] == 'on'))
                $insert_data['MODERATED']       = 1;
            else
                $insert_data['MODERATED']       = 0;
            if (!empty($_POST['map']) && ($_POST['map'] == 'on'))
                $insert_data['MAP']             = 1;
            else
                $insert_data['MAP']             = 0;
            if (!empty($_POST['list']) && ($_POST['list'] == 'on'))
                $insert_data['LIST']       = 1;
            else
                $insert_data['LIST']       = 0;

            $this->db->where('ID', $id);
            $this->db->update('ACTIONS', $insert_data);

            redirect('/cp/actions');
        }

        $data['item'] = $this->db->get_where('ACTIONS', ['ID' => $id], 0, 1)->row_array();

        $data['metro'] = $this->db->query('SELECT * FROM METRO ORDER BY NAME')->result_array();
        $data['categories'] = $this->db->get('CATEGORY')->result_array();

        $data['unreaded'] = $this->unreaded;

        $this->load->view('cp/actions_edit', $data);
    }

    public function actions_delete($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $this->db->delete('ACTIONS', array('ID' => $id));
        }
        redirect('/cp/actions');
    }

    public function header_carousel($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $data['catalog'] = $this->db->query('SELECT * FROM HEADER_CAROUSEL WHERE CATEGORY_ID = '.$id)->result_array();

            $data['unreaded'] = $this->unreaded;
            $this->load->view('cp/header_carousel_category', $data);
        } else {
            $data['catalog'] = $this->db->query('SELECT * FROM CATEGORY')->result_array();

            $data['unreaded'] = $this->unreaded;
            $this->load->view('cp/header_carousel', $data);
        }
    }

    public function banners($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $data['catalog'] = $this->db->query('SELECT * FROM BANNERS WHERE CATEGORY_ID = '.$id)->result_array();

            $data['unreaded'] = $this->unreaded;
            $this->load->view('cp/banners_category', $data);
        } else {
            $data['catalog'] = $this->db->query('SELECT * FROM CATEGORY')->result_array();

            $data['unreaded'] = $this->unreaded;
            $this->load->view('cp/banners', $data);
        }
    }

    public function rls_reference()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $data['unreaded'] = $this->unreaded;
        $this->load->view("cp/rls", $data);
    }

    public function mfp_reference()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $data['unreaded'] = $this->unreaded;
        $this->load->view("cp/mfp", $data);
    }

//    public function imported()
//    {
//        if (!$this->session->userdata('ADMIN_ID'))
//            redirect('/cp/');
//
//        $data['unreaded'] = $this->unreaded;
//        $this->load->view("cp/imported", $data);
//    }

//    public function not_imported()
//    {
//        if (!$this->session->userdata('ADMIN_ID'))
//            redirect('/cp/');
//
//        $data['unreaded'] = $this->unreaded;
//        $this->load->view("cp/not_imported", $data);
//    }

    public function logout()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $this->session->set_userdata(['ADMIN_ID' => null]);
        redirect('/cp');
    }

    public function ajax_get_organisation_fields()
    {
        $result = [];

        if ($this->session->userdata('ADMIN_ID')) {
            $id = 0;

            if (!empty($_POST['id']))
                $id = (int)$_POST['id'];

            if (!empty($id))
                $result = $this->db->query("SELECT * FROM INSTITUTIONS WHERE ID = $id")->row_array();
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function posts($category='all')
    {
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
        else
        {    /*иначе загружаем страницу с его письмами*/
            $data=$this->_postsData($category,false);
             $data['unreaded2'] = $this->unreaded2;
            $data['unreaded'] = $this->unreaded;
            $this->load->view("cp/posts", $data);
        }
    }
    public function opinion($id='')
    {
        //print_r($id);
        /*Если человек не авторизован, то переадресовывем его на страницу авторизации*/
        if(!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');
        else
        {    /*иначе загружаем страницу с его письмами*/
            $data=$this->_postsData('all',false);
             // print_r($data);
           // $data['unreaded2'] = $this->unreaded2;
            $this->load->model('New_organisations_model');
            $data['organisations'] = $this->New_organisations_model->get_list($filter=[], [], $per_page=[], $pages=[], false, $organisations_ids=[]);
            
            //print_r( $data);
           // if(isset($_GET['id'])){
                //$id=$_GET['id'];
               // print_r($id);
                
           // }
            $this->load->view("cp/opnion", $data);
        } 
    }

    public function post_read($id)
    {
        $id = (int)$id;

        if (!empty($id)) {
            $post = $this->db->query("SELECT 
                p.ID,
                p.ID_SENDER SENDER_ID,
                p.ID_RESPONDED_POST RESPONDED_ID,
                p.MESSAGE,
                p.`TIME` `DATE`,
                p.NAME_SENDER SENDER_NAME,
                p.DELETED,
                p.THEME,
                p.FINAL_DELETED,
                p.LIKED,
                p.RESEND_SPECIALIST_ID,
                pd.ID DESTINATION_ID,
                pd.ID_DESTINATION RECIEVER_ID,
                pd.DESTINATION_NAME RECIEVER,
                pd.DELETED DESTINATION_DELETED,
                pd.ID_POST POST_ID,
                pd.DESTINATION_READED READED,
                pd.LIKED DESTINATION_LIKED,
                pd.FINAL_DELETED DESTINATION_FINAL_DELETED,
                pd.TO_ADMIN
                FROM POSTS p
                LEFT JOIN POST_DESTINATIONS pd ON pd.ID_POST = p.ID
                WHERE p.ID = $id AND (pd.ID_DESTINATION IS NULL OR p.ID_SENDER IS NULL)
                LIMIT 1")->row_array();

            $get_user_type = function ($user_id,$id) {
                $user_data = $this->db->query("SELECT TYPE, EMAIL, FIRSTNAME, LASTNAME, MIDDLENAME FROM USERS WHERE ID = $user_id LIMIT 1")->row_array();
                $data["EMAIL"] = $user_data["EMAIL"];

                switch ($user_data['TYPE']) {
                    case 2 :
                   // print_r($id);
                        $temp = $this->db->query("SELECT LAST_NAME, FIRST_NAME, SECOND_NAME FROM SPECIALISTS WHERE USER_ID = $user_id LIMIT 1")->row_array();
                        $data["NAME"] = "Врач: ".$temp['LAST_NAME']." ".$temp['FIRST_NAME']." ".$temp['SECOND_NAME'];
                        $temp = $this->db->query("SELECT FILE FROM POST_FILES WHERE POST_ID =$id ")->result_array();
                        // print_r($temp);
                         
                        foreach($temp as $value){
                            $data["FILES"][] =$value['FILE'];
                            
                        };
                         
                        break;
                    case 3:
                        $temp = $this->db->query("SELECT BRAND SHORTNAME, ENTITY NAME FROM ORGANIZATIONS WHERE USER_ID = $user_id LIMIT 1")->row_array();
                        $data["NAME"] = "Организация: ".$temp['SHORTNAME']." (".$temp['NAME'].")"; 
                        $temp = $this->db->query("SELECT FILE FROM POST_FILES WHERE POST_ID = $id")->row_array();
                        if(!empty($temp)){
                           foreach($temp as $value){
                            $data["FILES"][] =$value['FILE'];
                            
                        }; 
                        }
                        
                        break;
                    default: 
                        $data["NAME"] = "Пользователь: ".$user_data['LASTNAME']." ".$user_data['FIRSTNAME']." ".$user_data['MIDDLENAME'];
                        break;
                }

                return $data;
            };

            if (!empty($post)) {
                $data['post'] = $post;
                if (empty($post['SENDER_ID'])) {
                    $data['info'] = $get_user_type($post['RECIEVER_ID'],$id);
                    $data['type'] = 'sent';
                } else if (empty($post['RECIEVER_ID'])) {
                    $data['info'] = $get_user_type($post['SENDER_ID'],$id);
                    $data['type'] = 'recieved';
                }

            } else {
                $data['errors'][] = 'Невозможно прочесть это письмо';
            }

            $data['unreaded'] = $this->unreaded;
            $this->load->view('cp/read_post', $data);
        } else {
            show_404();
        }
    }
    
    public function send_post()
    {
        $data = [];
        $data['unreaded'] = $this->unreaded;
        $this->load->view("/cp/post", $data);
    }

    public function resendPost($post_id=false)
    {
        if(!$this->session->userdata('ADMIN_ID'))
            redirect('/profile/auth?back_url=/profile');
        elseif($post_id)
        {
            $post=$this->Post_model->get_by_id($post_id);
            $data=$this->_addressBookData();
            $data['post_theme']=$post['THEME'];
            $data['post_message']=$post['MESSAGE'];
            $data['destination_email']='';
            $data['unreaded'] = $this->unreaded;
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/post",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function respondPost($post_id=false,$destination_id=false)
    {
        if(!$this->session->userdata('ADMIN_ID'))
            redirect('/profile/auth?back_url=/profile');
        elseif($post_id and $destination_id)
        {
            $post=$this->Post_model->get_by_id($post_id);
            $data=$this->_addressBookData();
            $data['post_theme']=$post['THEME'];
            $data['post_message']=$post['MESSAGE'];
            if($destination_id=='admin')
                $data['destination_email']='Администратор';
            else
            {
                $destination_user=$this->User_model->get_by_id($destination_id);
                if($destination_user['TYPE']==3)
                {
                    $institutionThis=$this->Institution_model->get_by_user_id($destination_id);
                    $data['destination_email']=$institutionThis['EMAIL'];
                }
                else
                    $data['destination_email']=$destination_user['EMAIL'];
            }
            $data['responded_post_id']=$post_id;
            $data['unreaded'] = $this->unreaded;
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/post",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function resend_specialist($specialist_id=false)
    {
        if(!$this->session->userdata('ADMIN_ID'))
            redirect('/profile/auth?back_url=/profile');
        elseif($specialist_id and $specialist_exist=$this->Doctor_model->get_by_id($specialist_id) != NULL)
        {
            $data=$this->_addressBookData();
            $data['specialist']=$this->Doctor_model->get_by_id($specialist_id);
            $data['unreaded'] = $this->unreaded;
            $this->load->view("tmp/profile-templates/mainmenu",$data);
            $this->load->view("profile/resend_specialist",$data);
            $this->load->view("tmp/profile-templates/footer");
        }
        else
            show_404();
    }

    public function sendPost()
    {
        if (!empty($_POST['destinations'][0]))
            $email_destination = $_POST['destinations'][0];

        if(!$this->session->userdata('ADMIN_ID'))
            redirect('/cp?back_url=/cp/posts');
        elseif(!empty($email_destination))
        {
            if($destination_exist=$this->User_model->get_by_email($email_destination) != NULL
                or $institution_exist=$this->Institution_model->get_by_email($email_destination) != NULL)
            {
                $data=$this->_postSenderData();

                if($insert_post_id = $this->Post_model->create($data['fields']))
                {
                    echo json_encode(['success'=>'true','msg'=>'Письмо успешно отправлено!']);
                    $this->_addressDestinationPost($insert_post_id,$email_destination,$data);
                }
                else
                    echo json_encode(['error'=>'Не удалось отправить письмо!']);
            }
            else
                echo json_encode(['error'=>'Нет получателя с email '.$email_destination.'!']);
        }
        else
            echo json_encode(['error'=>'Введите email получателя!']);
    }

    private function _postSenderData()
    {
        $user=$this->User_model->get_by_id(null);
        $fields = [
            'ID_SENDER'     =>null,
            'MESSAGE'       =>$_POST['message'],
            'TIME'          =>date("Y-m-d H:i:s"),
            'THEME'         =>$_POST['theme'],
            'DELETED'       =>0,
            'FINAL_DELETED' =>0
        ];/*в зависимости от типа пользователя в письме он отображается как юзер, учреждение или врач*/
        if($user['TYPE']==3)
        {
            $institutionThis=$this->Institution_model->get_by_user_id(null);
            $fields['NAME_SENDER']=$institutionThis['NAME'];
        }
        elseif($user['TYPE']==2)
        {
            $specialistThis=$this->Doctor_model->get_by_user_id(null);
            $fields['NAME_SENDER']=$specialistThis['LAST_NAME'].' '.$specialistThis['FIRST_NAME'];
        }
        elseif ($user['TYPE']==1)
        {
            $fields['NAME_SENDER']=$user['LASTNAME'].' '.$user['FIRSTNAME'];
        }
        else
        {
            $fields['NAME_SENDER']='Администратор';
        }
        if(!empty($_POST['responded_post_id']))
            $fields['ID_RESPONDED_POST']=$_POST['responded_post_id'];
        $data['user']=$user;
        $data['fields']=$fields;
        return $data;
    }

    private function _postsData($category,$search)
    {    /*число непрочтённых писем*/
         $data['unreaded']=$this->Post_model->get_by_user_id_in_count_unread(null);
     
        /*данные для адресной книги*/
        $data['addressUsers']=$this->Address_book_model->get_by_user_id_users(null);
        $data['addressSpecialists']=$this->Address_book_model->get_by_user_id_specialists(null);
        $data['addressInstitutions']=$this->Address_book_model->get_by_user_id_institutions(null);
        $data['unreaded'] = $this->unreaded;
        $data['unreaded2'] = $this->unreaded2;
        /*подгружаем кусок страницы с данными для адресной книги*/
        $data['addressBook'] = $this->load->view("tmp/profile-templates/address_book", $data, true);
        $data['category']=$category;
        if($category=='out')
            $data['posts']=$this->Post_model->get_by_user_id_out(null,$search);
        if($category=='in')
            $data['posts']=$this->Post_model->get_by_user_id_in(null,$search);
        if($category=='all')
            $data['posts']=$this->Post_model->get_by_user_id_all(null,$search);
        if($category=='delete')
            $data['posts']=$this->Post_model->get_by_user_id_delete(null,$search);
        if($category=='liked')
            $data['posts']=$this->Post_model->get_by_user_id_liked(null,$search);
        $posts=[];
        foreach ($data['posts'] as $key=>$post)
            $posts[$key]=$this->_formatPost($post);
        $data['posts']=$posts;
        return $data;
    }

    private function _formatPost($post)
    {
        $post=$this->_formatTime($post);
        /*файлы, приложенные к письму*/
        $post['FILES']=$this->Post_files_model->get_by_post_id($post['ID']);
        if($post['TO_ADMIN'] == 1)
        {/*если получатель - администратор, то выводятся его данные*/
            $post['ID_DESTINATION']='admin';
            $post['DESTINATION_NAME']='Администратор';
            if($post['ID_DESTINATION']!=null)
                $post['DESTINATION_READED']=1;
        }/*если получатель - это мы, то выводятся данные отправителя*/
        elseif($post['ID_DESTINATION']==null)
        {
            $post['ID_DESTINATION']=$post['ID_SENDER'];
            $post['DESTINATION_NAME']=$post['NAME_SENDER'];
        }
        else
            $post['DESTINATION_READED']=1;
        $have_resp=$post['ID_RESPONDED_POST'];
        $resps=[];
        $respsI=0;
        while(!empty($have_resp))
        {    /*формирование цепочки писем, ответом на которые является данное письмо*/
            $resps[$respsI]=$this->Post_model->get_by_id($have_resp);
            $resps[$respsI]=$this->_formatTime($resps[$respsI]);
            $have_resp=$resps[$respsI]['ID_RESPONDED_POST'];
            $respsI=$respsI+1;
        }
        $post['RESPONDEDS']=$resps;
        return $post;
    }

    private function _formatTime($post)
    {    /*в базе дата и время хранятся в виде - 0000-00-00 00:00 - а на сайте - 00/00/0000 00:00*/
        $dateTimeToArray=explode(' ',$post['TIME']);
        $dateToArray=explode('-',$dateTimeToArray[0]);
        $post['DATE']=$dateToArray[2].'/'.$dateToArray[1].'/'.$dateToArray[0];
        $post['TIME']=substr($dateTimeToArray[1],0,-3);
        return $post;
    }

    private function _addressDestinationPost($insert_post_id,$email_destination,$data)
    {    /*вбиваем получателя*/
        $fieldsDestination = [
            'ID_POST'            =>$insert_post_id,
            'DELETED'            =>0,
            'FINAL_DELETED'      =>0,
            'DESTINATION_READED' =>0,
            'TO_ADMIN'           =>0
        ];
        if($institution_exist=$this->Institution_model->get_by_email($email_destination) != NULL)
        {
            $institutionDestination=$this->Institution_model->get_by_email($email_destination);
            $userDestination=$this->User_model->get_by_id($institutionDestination['USER_ID']);
            $fieldsDestination['ID_DESTINATION']=$userDestination['ID'];
            $fieldsDestination['DESTINATION_NAME']=$institutionDestination['NAME'];
        }
        elseif($specialist_exist=$this->Doctor_model->get_by_email($email_destination) != NULL)
        {
            $specialistDestination=$this->Doctor_model->get_by_email($email_destination);
            $userDestination=$this->User_model->get_by_id($specialistDestination['USER_ID']);
            $fieldsDestination['ID_DESTINATION']=$userDestination['ID'];
            $fieldsDestination['DESTINATION_NAME']=$specialistDestination['LAST_NAME'].' '.$specialistDestination['FIRST_NAME'];
        }
        else
        {
            $userDestination=$this->User_model->get_by_email($email_destination);
            $fieldsDestination['ID_DESTINATION']=$userDestination['ID'];
            $fieldsDestination['DESTINATION_NAME']=$userDestination['LASTNAME'].' '.$userDestination['FIRSTNAME'];
        }
        if($insert_destination_id = $this->Post_destinations_model->create($fieldsDestination))
        {
            $this->_imagesPost($insert_post_id);
            if($userDestination['TYPE']==1)
                $this->_createAddressUser($userDestination,$data['user']);
            elseif($userDestination['TYPE']==2)
            {
                $this->_createAddressSpecialist($userDestination,$data['user']);
                $this->_addSpecialistPatient($userDestination,$data['user']);
            }
            elseif($userDestination['TYPE']==3)
            {
                $this->_createAddressInstitution($userDestination,$data['user']);
                $this->_addInstitutionPatient($userDestination,$data['user']);
            }
            if($data['user']['TYPE']==1)
                $this->_createAddressUser($data['user'],$userDestination);
            elseif($data['user']['TYPE']==2)
                $this->_createAddressSpecialist($data['user'],$userDestination);
            elseif($data['user']['TYPE']==3)
                $this->_createAddressInstitution($data['user'],$userDestination);
        }
    }

    private function _imagesPost($insert_post_id)
    {
        if(!empty($_POST['images']))
            foreach ($_POST['images'] as $imagePath)
                if(is_file(FCPATH.$imagePath))
                {    /*если файл есть - из временной папки он перекидывается в постоянную*/
                    $fileName=strrchr($imagePath,'/');
                    rename(FCPATH.$imagePath, FCPATH.'/images'.$fileName);
                    $imageFields = [
                        'POST_ID' =>$insert_post_id,
                        'FILE'    =>"/images".$fileName
                    ];/*файлы добавляются в БД*/
                    $insert_image_id = $this->Post_files_model->create($imageFields);
                }
    }

    private function _createAddressUser($destination,$user)
    {
        if($this->Address_book_model->get_user_by_user_id($destination['ID'],$user['ID'])==NULL)
        {
            $fieldsAddress = [
                'ID_USER_ADDRESS' =>$destination['ID'],
                'USER_ID'         =>$user['ID'],
                'NAME_USER'       =>$destination['LASTNAME'].' '.$destination['FIRSTNAME']
            ];
            $insert_address_id=$this->Address_book_model->create_user($fieldsAddress);
        }
    }

    private function _createAddressSpecialist($destination,$user)
    {
        $specialist=$this->Doctor_model->get_by_user_id($destination['ID']);
        if($this->Address_book_model->get_specialist_by_user_id($specialist['ID'],$user['ID'])==NULL)
        {
            $fieldsAddress = [
                'ID_SPECIALIST'  =>$specialist['ID'],
                'USER_ID'        =>$user['ID'],
                'NAME_SPECIALIST'=>$specialist['LAST_NAME'].' '.$specialist['FIRST_NAME']
            ];
            $insert_address_id=$this->Address_book_model->create_specialist($fieldsAddress);
        }
    }

    private function _addSpecialistPatient($destination,$user)
    {
        $specialist=$this->Doctor_model->get_by_user_id($destination['ID']);
        $branch=$this->Branches_model->get_by_id($specialist['BRANCH_ID']);
        if($this->Patients_model->get_by_specialist_id_user_id($specialist['ID'],$user['ID'])==NULL)
        {
            $fieldsPatient = [
                'INSTITUTION_ID'  =>$branch['INSTITUTION_ID'],
                'SPECIALIST_ID'   =>$specialist['ID'],
                'USER_ID'         =>$user['ID'],
                'USER_NAME'       =>$user['LASTNAME'].' '.$user['FIRSTNAME'],
                'USER_EMAIL'      =>$user['EMAIL'],
                'USER_PHONE'      =>$user['PHONE'],
                'RECEPTION_DATE'  =>date('Y-m-d'),
                'USER_CITY'       =>'Не указан'
            ];
            $insert_patient_id=$this->Patients_model->create($fieldsPatient);
        }
    }

    public function imported()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 25;
        $where = "";
        $where_array = array();
        $pages = 0;

        if (!empty($_GET['pages']))
            $pages = (((int)$_GET['pages'])-1)*$per_page;


        $limit = $per_page;
        $offset = $pages;

        $this->data['catalog'] = $this->db->query("SELECT
                   TRADE_BALANCES.ID ID,
                   TRADE_BALANCES.DRUGNAME IMPORTED_NAME,
                   NOMEN.ID NOMENID,      
                   NOMEN.DRUGSINPPACK,    
                   NOMEN.PPACKMASS,       
                   NOMEN.PPACKVOLUME,      
                   NOMEN.PPACKINUPACK,
                   PREP.ID PREPID,       
                   TRADENAMES.NAME,      
                   TRADENAMES.INAME,     
                   LATINNAMES.NAME LNAME,
                   CLSDRUGFORMS.FULLNAME DRUGFORMNAME,  
                   CLSDRUGFORMS.NAME DRUGFORMSHORTNAME, 
                   PREP.DFMASS,        
                   MASSUNITS.FULLNAME DFMASSNAME,       
                   MASSUNITS.SHORTNAME DFMASSSHORTNAME, 
                   PREP.DFCONC,          
                   CONCENUNITS.FULLNAME DFCONCNAME,      
                   CONCENUNITS.SHORTNAME DFCONCSHORTNAME,
                   PREP.DFACT,           
                   ACTUNITS.FULLNAME DFACTNAME,        
                   ACTUNITS.SHORTNAME DFACTSHORTNAME,     
                   PREP.DFSIZE,           
                   SIZEUNITS.FULLNAME DFSIZENAME,        
                   SIZEUNITS.SHORTNAME DFSIZESHORTNAME,  
                   PREP.DRUGDOSE,        
                   FIRMS.ID FIRMID,                     
                   FIRMS.FULLNAME FIRMFULLNAME,         
                   FIRMNAMES.NAME FIRMNAME,             
                   #COUNTRIES.NAME COUNTRY,              
                   NOMENFIRMS.ID NOMENFIRMID,            
                   NOMENFIRMS.FULLNAME NOMENFIRMFULLNAME,
                   NOMENFIRMNAMES.NAME NOMENFIRMNAME,   
                   DRUGFORMCHARS.FULLNAME DRUGFORMCHARFULLNAME,
                   DRUGFORMCHARS.SHORTNAME DRUGFORMCHARNAME       
              FROM TRADE_BALANCES
         LEFT JOIN NOMEN ON NOMEN.ID = TRADE_BALANCES.NOMEN_ID
         LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
         LEFT JOIN FIRMS ON FIRMS.ID = PREP.FIRMID
         LEFT JOIN FIRMNAMES ON FIRMNAMES.ID = FIRMS.NAMEID
         LEFT JOIN TRADENAMES ON TRADENAMES.ID = PREP.TRADENAMEID
         LEFT JOIN LATINNAMES ON LATINNAMES.ID = PREP.LATINNAMEID
         LEFT JOIN CLSDRUGFORMS ON CLSDRUGFORMS.ID = PREP.DRUGFORMID
         LEFT JOIN MASSUNITS ON MASSUNITS.ID = PREP.DFMASSID
         LEFT JOIN CONCENUNITS ON CONCENUNITS.ID = PREP.DFCONCID
         LEFT JOIN ACTUNITS ON ACTUNITS.ID = PREP.DFACTID
         LEFT JOIN SIZEUNITS ON SIZEUNITS.ID = PREP.DFSIZEID
         LEFT JOIN DRUGFORMCHARS ON DRUGFORMCHARS.ID = PREP.DFCHARID
         LEFT JOIN FIRMS NOMENFIRMS ON NOMENFIRMS.ID = NOMEN.FIRMID
         LEFT JOIN FIRMNAMES NOMENFIRMNAMES ON NOMENFIRMNAMES.ID = NOMENFIRMS.NAMEID
                   LIMIT $limit
                   OFFSET $offset")->result_array();
        
        $catalog_quantity = $this->db->query('SELECT COUNT(0) COUNT FROM TRADE_BALANCES')->row_array();

        $this->load->library('pagination');
        $config['total_rows'] = $catalog_quantity['COUNT'];
        $config['per_page'] = $per_page;
        $config['first_link'] = "Первая";
        $config['last_link'] = "Последняя";
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 3;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<span class="pages">';
        $config['full_tag_close'] = '</span>';
        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';

        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();

        $this->data['unreaded'] = $this->unreaded;

        $this->load->view('/cp/imported', $this->data);
    }

    public function not_imported()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $per_page = 25;
        $where = "";
        $where_array = array();
        $pages = 0;

        if (!empty($_GET['pages']))
            $pages = ((int)$_GET['pages']) * ($per_page);

        $limit = $per_page;
        $offset = $pages;

        $this->data['catalog'] = $this->db->query("SELECT
                           BAD_IMPORT.ID ID,
                           BAD_IMPORT.DRUGNAME IMPORTED_NAME
                      FROM BAD_IMPORT
                           LIMIT $limit
                           OFFSET $offset")->result_array();

        $catalog_quantity = $this->db->query('SELECT COUNT(0) COUNT FROM BAD_IMPORT')->row_array();

        $this->load->library('pagination');
        $config['total_rows'] = $catalog_quantity['COUNT'];
        $config['per_page'] = $per_page;
        $config['first_link'] = "Первая";
        $config['last_link'] = "Последняя";
        $config['next_link'] = false;
        $config['prev_link'] = false;
        $config['num_links'] = 3;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<span class="pages">';
        $config['full_tag_close'] = '</span>';
        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';

        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();

        $this->data['unreaded'] = $this->unreaded;

        $this->load->view('/cp/not_imported', $this->data);
    }

    public function imported_deatach($id = null)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $id = (int)$id;

        if (!empty($id)) {
            $row = $this->db->where('ID', $id)->get('TRADE_BALANCES')->row_array();
            $fields = [
                'BRANCH_ID'     => $row['BRANCH_ID'],
                'DRUGNAME'      => $row['DRUGNAME'],
                'COUNT'         => $row['COUNT'],
                'PRICE'         => $row['PRICE'],
                'EXPIRATION_DATE'   => $row['EXPIRATION_DATE'],
                'FIRMNAME'      => $row['FIRMNAME']
            ];

            $this->db->delete('TRADE_BALANCES', ['ID' => $id]);
            $this->db->insert('BAD_IMPORT', $fields);

            if (!empty($_GET['back_url']))
                redirect($_GET['back_url']);
            else
                redirect('/cp/imported');
        } else {
            show_404();
        }
    }

    public function imported_atach($id)
    {
        if (!$this->session->userdata('ADMIN_ID'))
            redirect('/cp/');

        $this->data['item'] = $this->db->where('ID', $id)->get('BAD_IMPORT')->row_array();

        if (empty($this->data['item']))
            show_404();

        if (isset($_POST['submit'])) {
            if (!empty($_POST['id'])) {
                $fields = [
                    'NOMEN_ID'      => $_POST['id'],
                    'BRANCH_ID'     => $this->data['item']['BRANCH_ID'],
                    'DRUGNAME'      => $this->data['item']['DRUGNAME'],
                    'COUNT'         => $this->data['item']['COUNT'],
                    'PRICE'         => $this->data['item']['PRICE'],
                    'EXPIRATION_DATE'   => $this->data['item']['EXPIRATION_DATE'],
                    'FIRMNAME'      => $this->data['item']['FIRMNAME']
                ];

                $this->db->query('SET FOREIGN_KEY_CHECKS = 0');
                $this->db->delete('BAD_IMPORT', ['ID' => $id]);
                $this->db->insert('TRADE_BALANCES', $fields);
                $this->db->query('SET FOREIGN_KEY_CHECKS = 1');

                if (!empty($_GET['back_url']))
                    redirect($_GET['back_url']);
                else
                    redirect('/cp/not_imported');
            } else {
                $this->data['errors'] = ['Не выбрано лекарство для связки'];
            }
        }

        $this->data['unreaded'] = $this->unreaded;

        $this->load->view('/cp/imported_atach', $this->data);
    }

    public function find_by_name()
    {
        if (!$this->session->userdata('ADMIN_ID'))
            show_404();

        $result = [];
        $tradename_ids = [];

        if (!empty($_POST['query'])) {
            $find = $this->db
                ->like('NAME', $_POST['query'])
                ->or_like('INAME', $_POST['query'])
                ->get('TRADENAMES')
                ->result_array();

            if (!empty($find))
                $tradename_ids = array_column($find, 'ID');

            if (!empty($tradename_ids)) {
                $where_in = 'WHERE PREP.TRADENAMEID IN ('.implode(',', $tradename_ids).')';

                $items = $this->db->query("SELECT
                       NOMEN.ID NOMENID,      # идентификатор в номенклатуре
                       #PPACK.NAME PPACKSHORTNAME,                 # название в начальной упаковке 
                       #PPACK.FULLNAME PPACKNAME,                  # название в начальной упаковке 
                       NOMEN.DRUGSINPPACK,    # количество в начальной упаковке
                       NOMEN.PPACKMASS,       # масса упаковки
                       PPACKMASS.FULLNAME PPACKMASSNAME,          # название массы упаковки
                       PPACKMASS.SHORTNAME PPACKMASSSHORTNAME,    # короткое название массы упаковки
                       NOMEN.PPACKVOLUME,     # объем упаковки
                       PPACKVOLUME.FULLNAME PPACKVOLUMENAME,      # название объема упаковки
                       PPACKVOLUME.SHORTNAME PPACKVOLUMESHORTNAME,# короткое название объема упаковки
                       DRUGSET.FULLNAME DRUGSETNAME,              # название комплекта
                       DRUGSET.SHORTNAME DRUGSETSHORTNAME,        # короткое название комплекта
                       NOMEN.PPACKINUPACK,    # количество первичных упаковок во вторичной
                       #UPACK.NAME UPACKNAME,  # название вторичной упаковки
                       NOMEN.INANGRO,         # признак АНГРО
                       NOMEN.EANCODE,         # ШКП (штрихкод производителя)
                       NOMEN.DRUGSTORCOND NOMENDRUGSTORCOND,      # условия хранения из общей таблицы
                       #DRUGSTORCOND.TEXT DRUGSTORCOND,            # условия хранения
                       #NOMEN.DRUGLIFETIME NOMENDRUGLIFETIME,      # срок годности из общей таблицы
                       #DRUGLIFETIME.TEXT DRUGLIFETIME,            # срок годности
                       #NOMEN.UPACKINSPACK,    # количество вторичных упаковок в третьичной
                       #SPACK.NAME SPACKNAME,  # название третьичной упаковки
                       PREP.ID PREPID,        # идентификатор препарата
                       TRADENAMES.NAME,       # название препарата
                       TRADENAMES.INAME,      # название препарата (без спецсимволов)
                       LATINNAMES.NAME LNAME, # название препарата (латинское)
                       CLSDRUGFORMS.FULLNAME DRUGFORMNAME,    # название лекарственной формы
                       CLSDRUGFORMS.NAME DRUGFORMSHORTNAME,   # короткое название лекарственной формы
                       PREP.DFMASS,           # масса лекарственной формы
                       MASSUNITS.FULLNAME DFMASSNAME,         # название единицы измерения
                       MASSUNITS.SHORTNAME DFMASSSHORTNAME,   # короткое название единицы измерения
                       PREP.DFCONC,           # концентрация лекарственной формы
                       CONCENUNITS.FULLNAME DFCONCNAME,       # название единицы измерения
                       CONCENUNITS.SHORTNAME DFCONCSHORTNAME, # короткое название единицы измерения
                       PREP.DFACT,            # количество единиц действия
                       ACTUNITS.FULLNAME DFACTNAME,           # название единицы измерения
                       ACTUNITS.SHORTNAME DFACTSHORTNAME,     # короткое название единицы измерения
                       PREP.DFSIZE,           # размеры лекарственной формы
                       SIZEUNITS.FULLNAME DFSIZENAME,         # название единицы измерения
                       SIZEUNITS.SHORTNAME DFSIZESHORTNAME,   # короткое название единицы измерения
                       PREP.DRUGDOSE,         # количество доз в упаковке 
                       #PREP.NORECIPE,         # признак безрецептурного отпуска 
                       FIRMS.ID FIRMID,                       # идентификатор фирмы
                       FIRMS.FULLNAME FIRMFULLNAME,           # полное название фирмы (может отсутствовать)
                       FIRMNAMES.NAME FIRMNAME,               # название фирмы
                       #COUNTRIES.NAME COUNTRY,                # название страны фирмы
                       NOMENFIRMS.ID NOMENFIRMID,             # идентификатор фирмы
                       NOMENFIRMS.FULLNAME NOMENFIRMFULLNAME, # полное название фирмы (может отсутствовать)
                       NOMENFIRMNAMES.NAME NOMENFIRMNAME,     # название фирмы
                       NOMENCOUNTRIES.NAME NOMENCOUNTRY,      # название страны фирмы
                       #FIRMS.ADRMAIN,         # адрес основного офиса
                       #FIRMS.ADRRUSSIA,       # адрес в россии
                       #FIRMS.ADRUSSR,         # адрес в странах ближнего зарубежья
                       DRUGFORMCHARS.FULLNAME DRUGFORMCHARFULLNAME,
                       DRUGFORMCHARS.SHORTNAME DRUGFORMCHARNAME,
                       #DESCTEXTES.COMPOSITION,                # состав и форма выпуска
                       #DESCTEXTES.DRUGFORMDESCR,              # описание лекарственной формы
                       #DESCTEXTES.PHARMAACTIONS,              # фармакологическое действия
                       #DESCTEXTES.PHARMAKINETIC,              # фармакинетика
                       #DESCTEXTES.PHARMADYNAMIC,              # фармадинамика
                       #DESCTEXTES.INDICATIONS,                # показания
                       #DESCTEXTES.CONTRAINDICATIONS,          # противопоказания
                       #DESCTEXTES.PREGNANCYUSE,               # применение при беременности и кормлении грудью
                       #DESCTEXTES.SIDEACTIONS,                # побочные действия
                       #DESCTEXTES.INTERACTIONS,               # взаимодействие
                       #DESCTEXTES.OVERDOSE,                   # передозировка
                       #DESCTEXTES.SPECIALGUIDELINES,          # особые указания
                       IDENT_WIND_STR.IWID IMAGE              # изображение
                  FROM NOMEN
             LEFT JOIN PREP ON PREP.ID = NOMEN.PREPID
             LEFT JOIN FIRMS ON FIRMS.ID = PREP.FIRMID
             LEFT JOIN FIRMNAMES ON FIRMNAMES.ID = FIRMS.NAMEID
             #LEFT JOIN COUNTRIES ON COUNTRIES.ID = FIRMS.COUNTID
             LEFT JOIN TRADENAMES ON TRADENAMES.ID = PREP.TRADENAMEID
             LEFT JOIN LATINNAMES ON LATINNAMES.ID = PREP.LATINNAMEID
             LEFT JOIN CLSDRUGFORMS ON CLSDRUGFORMS.ID = PREP.DRUGFORMID
             LEFT JOIN MASSUNITS ON MASSUNITS.ID = PREP.DFMASSID
             LEFT JOIN CONCENUNITS ON CONCENUNITS.ID = PREP.DFCONCID
             LEFT JOIN ACTUNITS ON ACTUNITS.ID = PREP.DFACTID
             LEFT JOIN SIZEUNITS ON SIZEUNITS.ID = PREP.DFSIZEID
             LEFT JOIN DRUGFORMCHARS ON DRUGFORMCHARS.ID = PREP.DFCHARID
             LEFT JOIN FIRMS NOMENFIRMS ON NOMENFIRMS.ID = NOMEN.FIRMID
             LEFT JOIN FIRMNAMES NOMENFIRMNAMES ON NOMENFIRMNAMES.ID = NOMENFIRMS.NAMEID
             LEFT JOIN COUNTRIES NOMENCOUNTRIES ON NOMENCOUNTRIES.ID = NOMENFIRMS.COUNTID
             LEFT JOIN DRUGPACK PPACK ON PPACK.ID = NOMEN.PPACKID
             #LEFT JOIN DRUGPACK UPACK ON UPACK.ID = NOMEN.UPACKID
             #LEFT JOIN DRUGPACK SPACK ON SPACK.ID = NOMEN.SPACKID
             LEFT JOIN MASSUNITS PPACKMASS ON PPACKMASS.ID = NOMEN.PPACKMASSUNID
             LEFT JOIN CUBICUNITS PPACKVOLUME ON PPACKVOLUME.ID = NOMEN.PPACKCUBUNID
             LEFT JOIN DRUGSET ON DRUGSET.ID = NOMEN.SETID
             #LEFT JOIN DRUGSTORCOND ON DRUGSTORCOND.ID = NOMEN.CONDID
             #LEFT JOIN DRUGLIFETIME ON DRUGLIFETIME.ID = NOMEN.LIFEID
             LEFT JOIN DESCRIPTIONS ON DESCRIPTIONS.FIRMID = FIRMS.ID AND DESCRIPTIONS.FPREPNAME = TRADENAMES.INAME
             #LEFT JOIN DESCTEXTES ON DESCTEXTES.DESCID = DESCRIPTIONS.ID 
             LEFT JOIN IDENT_WIND_STR ON IDENT_WIND_STR.NOMENID = NOMEN.ID
                       $where_in
                       LIMIT 10")->result_array();

                foreach ($items as $item) {
                    $result[] = [
                        'ID'    => $item['NOMENID'],
                        'VALUE' => implode(' ', array_filter([
                            $item['INAME'],
                            $item['DRUGFORMSHORTNAME'],
                            (float)$item['DFMASS'], $item['DFMASSSHORTNAME'],
                            (float)$item['DFCONC'], $item['DFCONCSHORTNAME'],
                            (float)$item['DFACT'], $item['DFACTSHORTNAME'],
                            (float)$item['DFSIZE'], $item['DFSIZESHORTNAME'],
                            (float)$item['PPACKVOLUME'], $item['PPACKVOLUMESHORTNAME'],
                            (float)$item['PPACKMASS'], $item['PPACKMASSNAME'],
                            (!empty($item['DRUGSINPPACK']) ? $item['DRUGSINPPACK'].'шт' : '' ),
                            (float)$item['DRUGDOSE']
                        ]))
                    ];
                }
            }
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}