<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institutions extends CI_Controller
{
    private $data;
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->data['controller']   = $this->router->fetch_class();
        $this->data['method']       = $this->router->fetch_method();

        $this->user_id = $this->session->userdata('USER_ID');

        $this->load->helper('url');
        $this->load->model('User_model');
        $this->load->model('Carousel_model');
        $this->load->model('Action_model');
        $this->load->model('Banner_model');
        $this->load->model('News_model');
        $this->load->model('Institution_model');
        $this->load->model('Branches_model');
        $this->load->model('Specialist_model');
        $this->load->model('Articles_model');
        $this->load->model('Geo_model');
        $this->load->model('Logos_model');
        $this->load->model('Post_model');
        $this->load->model('Voting_model');

        $database_city_id = $this->User_model->get_by_id($this->user_id)['DEFAULT_CITY'];
        $cookie_city_id = $this->session->userdata('DEFAULT_CITY');

        if (!empty($database_city_id)) {
            $this->data['current_city'] = $database_city_id;
        } elseif (!empty($cookie_city_id)) {
            $this->data['current_city'] = $cookie_city_id;
        } else {
            $this->data['current_city'] = 4400;
        }

        //Получаем две случайные акции для блока на главной
        $actions['actions'] = $this->Action_model->get_random_list(2, 2);

        foreach ($actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $actions['actions'][$key]['DESCRIPTION'] = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $actions['actions'][$key]['PRICE'] = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        // Получаем акции для вывода на карту
        $map_actions['actions'] = $this->Action_model->get_random_map(4, 2);

        foreach ($map_actions['actions'] as $key => $row) {
            // Если описание длинное, то обрезаем его до 47 символов и добавляем троеточие
            if (strlen($row['DESCRIPTION']) > 50)
                $map_actions['actions'][$key]['DESCRIPTION']    = mb_substr($row['DESCRIPTION'], 0, 47)."...";

            // Приводим к удобному виду цену
            $map_actions['actions'][$key]['PRICE']              = number_format((float)$row['PRICE'], 2, ".", " ");
        }

        $carousel = $this->Carousel_model->get_images(1);
        $actions_quantity        = $this->Action_model->count();
        $actions['quantity']     = $actions_quantity;
        $map_actions['quantity'] = $actions_quantity;
        $map_actions['green'] = false;
        $this->load->model('New_organisations_model');
        $popular_institutions = $this->New_organisations_model->get_list(['POPULAR' => 1], [], null);
        $first_banners  = $this->Banner_model->get_banners(1);
        $second_banners = $this->Banner_model->get_banners(1);
        $news = $this->News_model->get_last();
        $map_actions['types'] = $this->Institution_model->get_types();

        // выбираем три рандомные категории
        $popular_themes_category = $this->Articles_model->get_random_themes();

        $popular_themes_category_ids = null;

        foreach ($popular_themes_category as $item) {
            $popular_themes_category_ids[] = $item['ID'];
        }

        $popular_themes = $this->Articles_model->get_articles_by_ids_array($popular_themes_category_ids);

        foreach ($popular_themes as $popular_theme) {
            $popular_themes_data['themes'][$popular_theme['CATEGORY_ID']][] = [
                'ID'        => $popular_theme['ID'],
                'NAME'      => $popular_theme['NAME']
            ];
        }

        $popular_themes_data['category'] = $popular_themes_category;

        $cities['cities'] = $this->Geo_model->get_cities(null, null, null, 3159);
        $cities['current_city'] = $this->data['current_city'];

        $logos = $this->Logos_model->get_logos();

        $voting = $this->Voting_model->get_vote();
        $voting['is_vote'] = $this->Voting_model->is_vote($voting['info']['ID'], $this->user_id);

        $this->data['templates'] = [
            'city_selector'     => $this->load->view('tmp/city_selector', $cities, true),
            'auth'              => ($this->user_id)
                ? $this->load->view('tmp/auth_entered', [
                    'data' => $this->User_model->get_by_id($this->user_id),
                    'msg_count' => $this->Post_model->get_by_user_id_in_count_unread($this->user_id)[0]['COUNT(*)']
                ], true)
                : $this->load->view('tmp/auth', null, true),
            'carousel'          => $this->load->view('tmp/carousel', ['images' => $carousel], true),
            'menu'              => $this->load->view('tmp/menu', ['controller' => $this->data['controller']], true),
            'search'            => $this->load->view('tmp/search', null, true),
            'ask_question'      => $this->load->view('tmp/ask_question', ['is_auth' => (!empty($this->user_id)) ? true : false], true),
//            'actions'           => $this->load->view('tmp/actions', null, true),
            'logos'             => $this->load->view('tmp/logos', ['logos' => $logos], true),
            'map_search'        => $this->load->view('tmp/map_search', $map_actions, true),
            'popular_institutions'  => $this->load->view('tmp/popular_institutions', ['institutions' => $popular_institutions], true),
            'first_banners'     => $this->load->view('tmp/banners', ['banners' => $first_banners], true),
            'second_banners'    => $this->load->view('tmp/banners', ['banners' => $second_banners], true),
            'popular_themes'    => $this->load->view('tmp/popular_themes', $popular_themes_data, true),
            'voiting'           => $this->load->view('tmp/voiting', $voting, true),
            'news'              => $this->load->view('tmp/news', ['news' => $news], true),
            'bottom_menu'       => $this->load->view('tmp/bottom_menu', null, true),
            'footer'            => $this->load->view('tmp/footer', ['user' => $this->user_id], true)
        ];
        //TODO вынести отовсюду хлебные крошки в отдельный модуль
    }

    /*
     * Страница типов медицинских учреждений
     * Обычное представление
     */

    public function index()
    {
        $this->data['specializations'] = $this->db->order_by('NAME')->get('DOCTOR_TYPES')->result_array();

        $this->load->view('main/institutions', $this->data);
    }

    public function catalog()
    {
        $per_page = 10;
        $pages = 0;
        $this->data['category'] = 'Все организации';
        $this->data['type'] = 'Все типы';
        $category_id = null;
        $type_id = null;
        $for_the = " организаций";

        $filters = [];

        if (!empty($_GET['category_id'])) {
            $category_id = (int)$_GET['category_id'];
            $category_name = $this->db->query("SELECT ID, NAME FROM CATEGORY WHERE ID = $category_id")->row_array();
            if (!empty($category_name)) {
                $this->data['category'] = $category_name['NAME'];
                $this->data['category_id'] = $category_name['ID'];
            }
        }
        if (!empty($_GET['type_id'])) {
            $type_id = (int)$_GET['type_id'];
            $type_name = $this->db->query("SELECT ID,NAME FROM INSTITUTION_TYPES WHERE ID = $type_id")->row_array();
            if (!empty($type_name)) {
                $this->data['type'] = $type_name["NAME"];
                $this->data['type_id'] = $type_name["ID"];
            }
        }
        if (!empty($_GET['pages']))
            $pages = (int)$_GET['pages'];

        if (!empty($_GET['state']) && $_GET['state'] == "true")
            $filters[] = "(i.STATE = 1)";
        //------------------------
        if (!empty($_GET['private']) && $_GET['private'] == "true")
            $filters[] = "(i.PRIVATE = 1)";
        //------------------------
        if (!empty($_GET['children']) && $_GET['children'] == "true")
            $filters[] = "(i.CHILDREN = 1)";
        //------------------------
        if (!empty($_GET['ambulance']) && $_GET['ambulance'] == "true")
            $filters[] = "(i.AMBULANCE = 1)";
        //------------------------
        if (!empty($_GET['house']) && $_GET['house'] == "true")
            $filters[] = "(i.HOUSE = 1)";
        //------------------------
        if (!empty($_GET['booking']) && $_GET['booking'] == "true")
            $filters[] = "(i.BOOKING = 1)";
        //------------------------
        if (!empty($_GET['delivery']) && $_GET['delivery'] == "true")
            $filters[] = "(i.DELIVERY = 1)";
        //------------------------
        if (!empty($_GET['daynight']) && $_GET['daynight'] == "true")
            $filters[] = "(i.DAYNIGHT = 1)";
        //------------------------
        if (!empty($_GET['dms']) && $_GET['dms'] == "true")
            $filters[] = "(i.DMS = 1)";
        //------------------------
        if (!empty($_GET['dlo']) && $_GET['dlo'] == "true")
            $filters[] = "(i.DLO = 1)";
        //------------------------
        if (!empty($_GET['optics']) && $_GET['optics'] == "true")
            $filters[] = "(i.OPTICS = 1)";
        //------------------------
        if (!empty($_GET['rpo']) && $_GET['rpo'] == "true")
            $filters[] = "(i.RPO = 1)";
        //------------------------
        if (!empty($_GET['homeopathy']) && $_GET['homeopathy'] == "true")
        $filters[] = "(i.HOMEOPATHY = 1)";
        //------------------------

        if (!empty($_GET['specializations']))
            $spec = (int)$_GET['specializations'];
        else
            $spec = null;


//        print_r($filters);

        $this->data['catalog'] = $this->Institution_model->new_get_catalog($category_id, $type_id, $per_page, $pages, $filters, $spec);
        $catalog_quantity = $this->Institution_model->get_catalog_quantity($category_id, $type_id);

        $this->load->library('pagination');

//        $data['url_with_params'] = $_SERVER['PATH_INFO']."?";

        $config['base_url'] = $_SERVER['REDIRECT_URL'];
        $config['total_rows'] = $catalog_quantity;
        $config['per_page'] = $per_page;
        $config['first_link'] = 'Первая';
        $config['last_link'] = 'Последняя';
        $config['next_link'] = 'Следующая';
        $config['prev_link'] = 'Предыдущая';
        $config['num_links'] = 2;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = "pages";
        $config['full_tag_open'] = '<p style="margin-top: 10px;">';
        $config['full_tag_close'] = '</p>';
        $config['num_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['num_tag_close'] = '</span>';
        $config['prev_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['prev_tag_close'] = '</span>';
        $config['cur_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['cur_tag_close'] = '</span>';
        $config['last_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['last_tag_close'] = '</span>';
        $config['next_tag_open'] = '<span style="padding: 0 4px 0 4px;">';
        $config['next_tag_close'] = '</span>';


        $this->pagination->initialize($config);

        $this->data['pagination'] = $this->pagination->create_links();

        $temp_metros = $this->db->query('SELECT `ID`,`NAME`,`CLASS`,`TOP`,`LEFT`,`BRANCH` FROM METRO')->result_array();

        $this->data['metro'] = [];

        foreach ($temp_metros as $temp_metro) {
            $this->data['metro'][$temp_metro['BRANCH']][] = $temp_metro;
        }

        $this->data['doctor_types']    = $this->db->query('SELECT * FROM DOCTOR_TYPES ORDER BY NAME')->result_array();
        $this->data['specializations'] = $this->db->query('SELECT * FROM SPECIALIZATIONS ORDER BY NAME')->result_array();

        $this->load->view('main/institutions_catalog', $this->data);
    }

    public function item($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();


        $this->data['id'] = $id;
        $this->data['item'] = $this->Institution_model->new_get_item($id);
        $this->data['branches'] = $this->Branches_model->get_by_institution_id($id);
        $this->data['specialists'] = $this->Specialist_model->get_by_institution_id($id);
        $this->data['articles'] = $this->Articles_model->get_articles_for(1, $id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        if (empty($this->data['item']))
            show_404();

        $this->data['videos'] = $this->db->query("SELECT * FROM VIDEOS WHERE OBJECT = 1 AND LOADER_ID = $id")->result_array();
        if (!empty($this->data['videos'])) {
            $this->data['first_video'] = $this->data['videos'][0];
            unset($this->data['videos'][0]);
        } else {
            $this->data['first_video'] = [];
        }

        $this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 1 AND LOADER_ID = $id")->result_array();

        $this->load->view('main/institution', $this->data);
    }

    public function specialists_list($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['item'] = $this->Institution_model->new_get_item($id);
        $this->data['specialists'] = $this->Specialist_model->get_by_institution_id($id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        if (empty($this->data['item']))
            show_404();

        $this->load->view('main/institution_specialists', $this->data);
    }

    public function branches_list($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['item'] = $this->Institution_model->new_get_item($id);
        $this->data['branches'] = $this->Branches_model->get_by_institution_id($id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        if (empty($this->data['item']))
            show_404();

        $this->load->view('main/institution_branches', $this->data);
    }

    public function publications_list($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['item'] = $this->Institution_model->new_get_item($id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        if (empty($this->data['item']))
            show_404();

        $this->data['articles'] = $this->Articles_model->get_articles_for(1, $id);

        $this->load->view('main/institution_publications', $this->data);
    }

    public function photos_list($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['item'] = $this->Institution_model->new_get_item($id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        if (empty($this->data['item']))
            show_404();

        $this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 1 AND LOADER_ID = $id")->result_array();

        $this->load->view('main/institution', $this->data);
    }

    public function videos_list($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['item'] = $this->Institution_model->new_get_item($id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        if (empty($this->data['item']))
            show_404();

        $this->data['videos'] = $this->db->query("SELECT * FROM VIDEOS WHERE OBJECT = 1 AND LOADER_ID = $id")->result_array();
        if (!empty($this->data['videos'])) {
            $this->data['first_video'] = $this->data['videos'][0];
            unset($this->data['videos'][0]);
        } else {
            $this->data['first_video'] = [];
        }

        $this->load->view('main/institution', $this->data);
    }

    public function branches($id)
    {
        $id = (int)$id;

        if (empty($id))
            show_404();

        $this->data['item'] = $this->Branches_model->new_get_item($id);
        $this->data['specialists'] = $this->Specialist_model->get_by_branch_id($id);

        $months = [
            1   => 'Январь',
            2   => 'Февраль',
            3   => 'Март',
            4   => 'Апрель',
            5   => 'Май',
            6   => 'Июнь',
            7   => 'Июль',
            8   => 'Август',
            9   => 'Сентябрь',
            10  => 'Октябрь',
            11  => 'Ноябрь',
            12  => 'Декабрь'
        ];

        $this->data['month'] = $months[(int)date('m')];
        $monday = strtotime("last Monday");
        $this->data['week']['mon'] = date("d", $monday);
        $this->data['week']['tue'] = date("d", $monday + 86400);
        $this->data['week']['wed'] = date("d", $monday + 2*86400);
        $this->data['week']['thu'] = date("d", $monday + 3*86400);
        $this->data['week']['fri'] = date("d", $monday + 4*86400);
        $this->data['week']['sat'] = date("d", $monday + 5*86400);
        $this->data['week']['sun'] = date("d", $monday + 6*86400);

        if (empty($this->data['item']))
            show_404();

//        print_r($this->data['item']);

        $this->data['item']['MONDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['MONDAY_WORK_FROM']));
        $this->data['item']['TUESDAY_WORK_FROM']      = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_FROM']));
        $this->data['item']['WEDNESDAY_WORK_FROM']    = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_FROM']));
        $this->data['item']['THURSDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_FROM']));
        $this->data['item']['FRIDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_FROM']));
        $this->data['item']['SATURDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_FROM']));
        $this->data['item']['SUNDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_FROM']));

        $this->data['item']['MONDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['MONDAY_WORK_TO']));
        $this->data['item']['TUESDAY_WORK_TO']        = date("H:i", strtotime($this->data['item']['TUESDAY_WORK_TO']));
        $this->data['item']['WEDNESDAY_WORK_TO']      = date("H:i", strtotime($this->data['item']['WEDNESDAY_WORK_TO']));
        $this->data['item']['THURSDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['THURSDAY_WORK_TO']));
        $this->data['item']['FRIDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_TO']));
        $this->data['item']['SATURDAY_WORK_TO']       = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_TO']));
        $this->data['item']['SUNDAY_WORK_TO']         = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_TO']));

        $this->data['item']['MONDAY_BREAK_FROM']      = date("H:i", strtotime($this->data['item']['MONDAY_BREAK_FROM']));
        $this->data['item']['TUESDAY_BREAK_FROM']     = date("H:i", strtotime($this->data['item']['TUESDAY_BREAK_FROM']));
        $this->data['item']['WEDNESDAY_BREAK_FROM']   = date("H:i", strtotime($this->data['item']['WEDNESDAY_BREAK_FROM']));
        $this->data['item']['THURSDAY_BREAK_FROM']    = date("H:i", strtotime($this->data['item']['THURSDAY_BREAK_FROM']));
        $this->data['item']['FRIDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['FRIDAY_WORK_FROM']));
        $this->data['item']['SATURDAY_WORK_FROM']     = date("H:i", strtotime($this->data['item']['SATURDAY_WORK_FROM']));
        $this->data['item']['SUNDAY_WORK_FROM']       = date("H:i", strtotime($this->data['item']['SUNDAY_WORK_FROM']));

        $this->data['item']['MONDAY_BREAK_TO']        = date("H:i", strtotime($this->data['item']['MONDAY_BREAK_TO']));
        $this->data['item']['TUESDAY_BREAK_TO']       = date("H:i", strtotime($this->data['item']['TUESDAY_BREAK_TO']));
        $this->data['item']['WEDNESDAY_BREAK_TO']     = date("H:i", strtotime($this->data['item']['WEDNESDAY_BREAK_TO']));
        $this->data['item']['THURSDAY_BREAK_TO']      = date("H:i", strtotime($this->data['item']['THURSDAY_BREAK_TO']));
        $this->data['item']['FRIDAY_BREAK_TO']        = date("H:i", strtotime($this->data['item']['FRIDAY_BREAK_TO']));
        $this->data['item']['SATURDAY_BREAK_TO']      = date("H:i", strtotime($this->data['item']['SATURDAY_BREAK_TO']));
        $this->data['item']['SUNDAY_BREAK_TO']        = date("H:i", strtotime($this->data['item']['SUNDAY_BREAK_TO']));

        $this->data['videos'] = $this->db->query("SELECT * FROM VIDEOS WHERE OBJECT = 2 AND LOADER_ID = $id")->result_array();
        if (!empty($this->data['videos'])) {
            $this->data['first_video'] = $this->data['videos'][0];
            unset($this->data['videos'][0]);
        } else {
            $this->data['first_video'] = [];
        }

        $this->data['photos'] = $this->db->query("SELECT * FROM IMAGES WHERE OBJECT = 2 AND LOADER_ID = $id")->result_array();
        $this->data['articles'] = $this->db->query("SELECT * FROM ARTICLES WHERE OBJECT = 2 AND LOADER_ID = $id")->result_array();

        $this->load->view('main/branches', $this->data);
    }
}