<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Медицинские учреждения</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Мама и ребенок</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех медучреждениях вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>учреждений</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 867 220</b><br>клиник</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Медучреждения</a></li>
                <li><a href="#">Больницы</a></li>
                <li>Университетская клиническая больница № 1 Первого МГМУ им. И. М. Сеченова</li>
            </ul>
        </div>

        <div class="container maron">
            <div class="search-card pharmacy-card hospital-card">
                <div class="two-cols">
                    <div class="left-col">
                        <div class="description">
                            <div class="image"><img src="/temp/15.jpg" alt=""></div>
                            <div class="params">
                                <h1><span class="transform">Университетская клиническая больница № 1 Первого МГМУ им. И. М. Сеченова</span></h1>
                                <a href="#" class="btn order in-development">Записаться на прием</a>
                            </div>
                        </div>

                        <div class="search-in-card search_form">
                            <form action="#">
                                <var class="title georgia transform">поиск</var>
                                <span class="for_input"><input type="text" class="text" placeholder="Поиск в этой больнице"></span>
                                <input type="submit" class="submit btn" value="Найти" title="Найти">
                            </form>
                        </div>

                        <div class="description-bottom clearfix">
                            <div class="description-left size12">
                                <h2>Описание</h2>
                                <p>Университетская клиническая больница №1 объединяет ряд клиник Девичьего поля хирургического и терапевтического профилей, открытых при медицинском факультете Московского университета в середине 19 века. Таким образом, история подразделений нашей больницы насчитывает более 160 лет, что само по себе является уникальным явлением. В настоящее время, сохраняя лучшие традиции отечественной университетской медицины, больница непрерывно совершенствует лечебный процесс, благодаря научному поиску, осуществляемому на клинических кафедрах.</p>
                            </div>
                            <div class="description-middle">
                                <h2>Контакты</h2>
                                <table class="table-card pharmacy-card size14">
                                    <tr>
                                        <td>Адрес:</td>
                                        <td>г. Москва, Хамовники район, ул. Большая Пироговская, д. 6, стр. 1 </td>
                                    </tr>
                                    <tr>
                                        <td>Метро:</td>
                                        <td><span class="metro">Спортивная</span></td>
                                    </tr>
                                    <tr>
                                        <td>Телефон:</td>
                                        <td>459 132-98-98<br/> 495 123-32-23</td>
                                    </tr>
                                    <tr>
                                        <td>Сайт:</td>
                                        <td><a href="#">UCH01MUMU.narod.ru</a></td>
                                    </tr>
                                    <tr>
                                        <td>График работы:</td>
                                        <td>9.00 - 23.00 пн-пт<br/> 9.00 - 18.00 сб-вс </td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                    </div>


                    <div class="right-col">
                        <h2>Больница</h2>

                        <ul class="list-instruction">
                            <li><a href="#">описание</a></li>
                            <li><a href="#">услуги</a></li>
                            <li><a href="#">специализации</a></li>
                            <li><a href="#">персонал</a></li>
                            <li><a href="#">отделения</a></li>
                            <li><a href="#">отзывы</a></li>
                            <li><a href="#">публикации</a></li>
                            <li><a href="#">новости</a></li>
                            <li><a href="#">записаться на прием</a></li>
                        </ul>

                        <div class="write-us">
                            <h2>Свяжитесь с нами</h2>
                            <form action="#">
                                <textarea cols="50" rows="4" placeholder="Наш формацевт ответит вам в течение 10 минут"></textarea>
                                <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                            </form>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>
            </div>

        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <div class="popular popular-agency">
            <div class="container">
                <h2>Популярные медучреждения</h2>

                <div class="item">
                    <span class="ava"><img src="/temp/12.jpg" alt=""></span>
                    <p><a href="#">Университетская клиническая больница №1 Первого <br> МГМУ им. И.И. Сеченова</a></p>
                    Хаовинский район <br> ул. Большая Бронная
                </div>

                <div class="item">
                    <span class="ava"><img src="/temp/13.jpg" alt=""></span>
                    <p><a href="#">Городская клиническая больница № 1 им. Н. И. Пирогова</a></p>
                    Хаовинский район <br> ул. Большая Бронная
                </div>

                <div class="item">
                    <span class="ava"><img src="/temp/14.jpg" alt=""></span>
                    <p><a href="#">ГБУЗ МО «Центральная клиническая психиатрическая больница»</a></p>
                    Хаовинский район <br> ул. Большая Бронная
                </div>

                <div class="item">
                    <span class="ava"><img src="/temp/12.jpg" alt=""></span>
                    <p><a href="#">Университетская клиническая больница №1 Первого <br> МГМУ им. И.И. Сеченова</a></p>
                    Хаовинский район <br> ул. Большая Бронная
                </div>

            </div>
        </div>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>


        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>