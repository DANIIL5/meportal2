<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Медицинские учреждения</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="/css/metro_style.css" />
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>


<!-- метро для фильтра -->

<div id="metro_wrapper" style="width: 100vw; height: 100vh; background-color: rgba(0, 0, 0, 0.1); position: fixed; top: 0; left: 0; z-index: 9999999999; display: none;">
    <div class="test" style="background: #fff; width: 1250px; height: 80vh; margin: 10vh auto; border-top: 3px solid #c44848; border-bottom: 3px solid #fe5401; background: linear-gradient(to bottom, #c44848 0%, #fe5401 100%); overflow-y: scroll;">
        <div class="popSubwayBlock clearfix" style="height: 100vh; margin: 0 auto;">

            <div class="subwayPopUpMap_metro_header clearfix">
                <div class="right" style="margin: 27px 40px 0 0;"><input style="background: #666; padding: 1px; width:177px; cursor:pointer" name="" onClick="$('#metro_wrapper').toggle(); metro_wrapper(); get_selected_metro();" type="submit" value="Выбрать" class="btn" style="width:170px;"></div>
                <div class="subwayPopUpMap_mission">
                    Выберите расположение: по карте метро или по карте районов Москвы
                </div>
            </div>
            <div class="subwayPopUpMap_metro_block">
                <div class="subwayPopUpMap_filter_top clearfix">
                    <a href="#" class="unlink"><img src="/images/filter3.png">
			<span>
				Выбрать станции внутри <br>произвольной области <br><i>(просто выделите область мышью)</i>
			</span>
                    </a>
                    <a href="#" id="vnutrikolcevie"><i></i>
			<span>
				Выбрать станции <br>внутри кольцевой ветки
			</span>
                    </a>
                    <a href="#" id="kolcevaya"><i></i>
			<span>
				Выбрать станции <br>кольцевой ветки
			</span>
                    </a>
                    <a href="#" id="reset_all" onclick="$('.subwayPopUpMap_checkbox input').prop('checked',false); $('.subwayPopUpMap_checkbox .checked').removeClass('checked');
			$(this).removeClass('checked');"><i></i>
                        <span>Очистить выбор</span>
                    </a>
                </div>

                <div class="subwayPopUpMap_filter_right">
                    <div class="subwayPopUpMap_filter_map">
                        <a href="#" id="vnutrikolcevie"><i>Центр</i></a>
                        <a href="#" id="sad_zapad"><i>САД-<br>ТТК <br>(запад)</i></a>
                        <a href="#" id="sad_sever"><i>САД-ТТК (север)</i></a>
                        <a href="#" id="sad_vostok"><i>САД-<br>ТТК <br>(восток)</i></a>
                        <a href="#" id="sad_yug"><i>САД-ТТК (юг)</i></a>
                        <a href="#" id="mkad_zapad"><i>ТТК-МКАД (запад)</i></a>
                        <a href="#" id="mkad_sever"><i>ТТК-МКАД (север)</i></a>
                        <a href="#" id="mkad_vostok"><i>ТТК-МКАД (восток)</i></a>
                        <a href="#" id="mkad_yug"><i>ТТК-МКАД (юг)</i></a>
                        <a href="#" id="new_moscow"><i>Новая Москва</i></a>
                    </div>
                    <div class="subwayPopUpMap_filter_names">
                        <a href="#" id="vnutrikolcevie"><i>Центр</i></a>
                        <a href="#" id="sad_zapad"><i>САД-ТТК (запад)</i></a>
                        <a href="#" id="sad_sever"><i>САД-ТТК (север)</i></a>
                        <a href="#" id="sad_vostok"><i>САД-ТТК (восток)</i></a>
                        <a href="#" id="sad_yug"><i>САД-ТТК (юг)</i></a>
                        <a href="#" id="mkad_zapad"><i>ТТК-МКАД (запад)</i></a>
                        <a href="#" id="mkad_sever"><i>ТТК-МКАД (восток)</i></a>
                        <a href="#" id="mkad_vostok"><i>ТТК-МКАД (восток)</i></a>
                        <a href="#" id="mkad_yug"><i>ТТК-МКАД (юг)</i></a>
                        <a href="#" id="new_moscow"><i>Новая Москва</i></a>
                    </div>
                </div>

                <div class="subwayPopUpMap_filter">
                    <a href="#" id="sokolnicheskaya_right">↓</a>
                    <a href="#" id="sokolnicheskaya_left">↑</a>
                    <a href="#" id="zamoskvoreckaya_left">↓</a>
                    <a href="#" id="zamoskvoreckaya_right">↑</a>
                    <a href="#" id="arbatsko-pokrovskaya_left">↓</a>
                    <a href="#" id="arbatsko-pokrovskaya_right">↓</a>
                    <a href="#" id="filevskaya_left">↓</a>
                    <a href="#" id="kalujsko-rijskaya_top">↓</a>
                    <a href="#" id="kalujsko-rijskaya_bottom">↑</a>
                    <a href="#" id="tagansko-krasnopresnenskaya_left">↓</a>
                    <a href="#" id="tagansko-krasnopresnenskaya_right">↑</a>
                    <a href="#" id="kalininskaya_left">↓</a>
                    <a href="#" id="kalininskaya_right">↓</a>
                    <a href="#" id="serpuxovsko-timiryazevskaya_top">↓</a>
                    <a href="#" id="serpuxovsko-timiryazevskaya_bottom">↑</a>
                    <a href="#" id="lyublinskaya_top">↓</a>
                    <a href="#" id="lyublinskaya_bottom">↑</a>
                    <a href="#" id="kaxovskaya_bottom">↑</a>
                    <a href="#" id="butovskaya_bottom">↓</a>
                </div>

                <div class="subwayPopUpMap_metro_images">
                    <div class="subwayPopUpMap_metro_stores">
                        <form action="" id="metro_map_form">
                            <? foreach ($metro as $line => $stations) { ?>
                                <div class="<?=$line?>">
                                    <? foreach ($stations as $station) { ?>
                                        <label class="subwayPopUpMap_checkbox <?=$station['CLASS']?>" style="top: <?=$station['TOP']?>px; left: <?=$station['LEFT']?>px;">
                                            <input type="checkbox" value="<?=$station['ID']?>" form="filter" data-name="<?=$station['NAME']?>" name="m[]">
                                            <i class="ui-selectee"></i>
                                            <span><?=$station['NAME']?><?=$station['ID']?></span>
                                        </label>
                                    <? } ?>
                                </div>
                            <? } ?>
                            <img src="/images/metro_bright.png" alt="" id="metro_bright">
                            <div class="text-center"><br><input style="background: #666; padding: 1px; width:177px; cursor:pointer" type="button" value="Выбрать" class="btn" style="width:170px;" onclick="$('#metro_wrapper').toggle(); metro_wrapper(); get_selected_metro();"><br></div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end метро -->




<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Медицинские учреждения</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех медучреждениях вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>учреждений</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 867 220</b><br>клиник</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="/institutions/catalog<? if (!empty($category_id)) { ?>?category_id=<?=$category_id?><? } ?>"><?=$category?></a></li>
                <li><a href="/institutions/catalog<? if (!empty($type_id)) { ?>?type_id=<?=$type_id?><? } ?>"><?=$type?></a></li>
            </ul>

            <div class="two-cols search-result search-result-list hospital">
                <div class="left-col">

                    <div class="top clearfix">
                        <h1><?=$category?></h1>
                    </div>

                    <table class="results-table">
                        <tr>
                            <th class="name">Организация</th>
                            <th class="distance">Удаленность</th>
                            <th class="on-map">На карте</th>
                        </tr>
                    </table>

                    <? //Если первый элемент, то сразу присваиваем класс best, который раскроет его для просмотра
                    if (empty($catalog)) { ?>
                        <br>
                        По вашему запросу не найдено ни одного объекта <?
                    } else {
                        reset($catalog);
                        $first_key = key($catalog);

                        foreach ($catalog as $key => $item) { ?>
                            <div class="no-active<?=($key == $first_key) ? " best" : ""?> b-row_rMenu">
                                <table class="results-table">
                                    <tr>
                                        <td class="name"><a href="#" class="dotted"><?=$item['NAME']?></a></td>
                                        <td class="distance">~ 2 час</td>
                                        <td class="on-map"><a href="#" class="link">На карте</a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="list-detail">
                                            <div class="two-cols">
                                                <div class="left-col">
                                                    <div class="image"><img src="<?=$item["LOGO"]?>" alt=""></div>
                                                    <? if ($item['CATEGORY_ID'] != 4 ) { ?>
                                                        <a class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-zap" href="#">записаться на прием</a>
                                                    <? } ?>
                                                </div>
                                                <div class="right-col">
                                                    <table class="contacts">
                                                        <tr>
                                                            <td><span class="label">Адрес:</span></td>
                                                            <td colspan="3"><?=(!empty($item['ACTUAL_ADDRESS_REGION']) ? $item['ACTUAL_ADDRESS_REGION'].", " : "" ).(!empty($item['ACTUAL_ADDRESS_CITY']) ? $item['ACTUAL_ADDRESS_CITY'].", " : "" ).(!empty($item['ACTUAL_ADDRESS_STREET']) ? $item['ACTUAL_ADDRESS_STREET'].", " : "" ).(!empty($item['ACTUAL_ADDRESS_HOUSE']) ? $item['ACTUAL_ADDRESS_HOUSE'].", " : "" ).(!empty($item['ACTUAL_ADDRESS_BUILDING']) ? $item['ACTUAL_ADDRESS_BUILDING'].", " : "" ).(!empty($item['ACTUAL_ADDRESS_OFFICE']) ? $item['ACTUAL_ADDRESS_OFFICE'] : "" )?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="label">Метро:</span></td>
                                                            <td><span class="metro"><?=(!empty($item['METROS'])) ? $item['METROS'] : ""?></span></td>
                                                            <td><span class="label">Сайт:</span></td>
                                                            <td><?
                                                                if (!empty($item['WEBSITE1'])) { ?>
                                                                    <a href="<?$item['WEBSITE1']?>"><?=$item['WEBSITE1']?></a><br><?
                                                                }
                                                                if (!empty($item['WEBSITE2'])) { ?>
                                                                    <a href="<?$item['WEBSITE2']?>"><?=$item['WEBSITE2']?></a><br><?
                                                                }
                                                                if (!empty($item['WEBSITE3'])) { ?>
                                                                    <a href="<?$item['WEBSITE3']?>"><?=$item['WEBSITE3']?></a><br><?
                                                                }
                                                            ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="label">Телефон:</span></td>
                                                            <td><?=(!empty($item['PHONE1']) ? $item['PHONE1']."<br/>" : "").(!empty($item['PHONE2']) ? $item['PHONE2']."<br/>" : "").(!empty($item['PHONE3']) ? $item['PHONE3']."<br/>" : "")?></td>
<!--                                                            <td><span class="label">График работы:</span></td>-->
<!--                                                            <td>9.00 - 23.00 пн-пт<br/>-->
<!--                                                                9.00 - 18.00 сб-вс </td>-->
                                                        </tr>
                                                    </table>

                                                    <div class="description-text">
                                                        <?=$item['DESCRIPTION']?>
                                                    </div>

                                                    <ul class="list-instruction">
                                                        <li><a href="/institutions/item/<?=$item['ID']?>">Описание</a></li>
                                                        <li><a href="/institutions/specialists_list/<?=$item['ID']?>">Специалисты</a></li>
                                                        <li><a href="/medicines/catalog?institution_id=<?=$item['ID']?>">Каталог</a></li>
                                                        <li><a href="/institutions/branches_list/<?=$item['ID']?>">Отделения</a></li>
                                                        <li><a href="/institutions/publications_list/<?=$item['ID']?>">Публикации</a></li>
<!--                                                        <li><a href="/institutions/actions_list/--><?//=$item['ID']?><!--">Акции</a></li>-->
                                                        <li><a href="/institutions/photos_list/<?=$item['ID']?>">Фото</a></li>
                                                        <li><a href="/institutions/videos_list/<?=$item['ID']?>">Видео</a></li>
                                                        <li><a href="/institutions/feedbacks_list/<?=$item['ID']?>">Отзывы</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <? }
                    } ?>



                    <?=$pagination?>
                </div>

                <div class="right-col">
                    <? if (!empty($category_id) && $category_id == 2) {?>
                    <h2>Уточните поиск по медицинским учреждениям</h2>
                    <div class="search-block-red search-place">
                        <form id="filter" action="">
                            <input type="hidden" name="category_id" value="<?=!empty($_GET['category_id']) ? (int)$_GET['category_id'] : ""?>">
                            <input type="hidden" name="type_id" value="<?=!empty($_GET['type_id']) ? (int)$_GET['type_id'] : ""?>">
                            <p class="title inlineBlock red">Местоположение</p>
                            <a href="#" class="reset red">Сбросить все</a>
                            <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>
                            <p><i class="icon place"></i><a href="#" class="dotted" onclick="event.preventDefault(); $('#metro_wrapper').toggle(); metro_wrapper();">Выбрать метро</a></p>
                            <ul id="metro_list" style="max-height: 90px; overflow-y: auto;">

                            </ul>

                            <hr/>

<!--                            <ul class="list-two">-->
<!--                                <li><input type="checkbox" class="checkbox" id="input_1"/><label for="input_1">Государственные</label></li>-->
<!--                                <li><input type="checkbox" class="checkbox" id="input_2"/><label for="input_2">Частные</label></li>-->
<!--                            </ul>-->

                            <hr/>

                            <select name="specializations">
                                <option>Cпециализация</option>
                                <? foreach ($specializations as $specialization) { ?>
                                    <option value="<?=$specialization['ID']?>"><?=$specialization['NAME']?></option>
                                <? } ?>
                            </select>

                            <hr/>

                            <select name="doctor_types">
                                <option>Врач</option>
                                <? foreach ($doctor_types as $doctor_type) { ?>
                                    <option value="<?=$doctor_type['ID']?>"><?=$doctor_type['NAME']?></option>
                                <? } ?>
                            </select>

                            <hr/>

                            <select>
                                <option>Анализы</option>
                                <option>Анализы 1</option>
                                <option>Анализы 2</option>
                                <option>Анализы 3</option>
                                <option>Анализы 4</option>
                            </select>

                            <hr/>

                            <select>
                                <option>Диагностика</option>
                                <option>Диагностика 1</option>
                                <option>Диагностика 2</option>
                                <option>Диагностика 3</option>
                                <option>Диагностика 4</option>
                            </select>

                            <hr/>


                            <ul class="list-two">
                                <li><input type="checkbox" class="checkbox" id="input_1" value="true" name="state"/><label for="input_1">Государственная</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_2" value="true" name="private"/><label for="input_2">Частная</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_3" value="true" name="children"/><label for="input_3">Детское отделение</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_4" value="true" name="ambulance"/><label for="input_4">Скорая</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_5" value="true" name="house"/><label for="input_5">Выезд на дом</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_6" value="true" name="daynight"/><label for="input_6">Круглосуточно</label></li>
                            </ul>

                            <input type="submit" class="submit btn filter_button" value="Фильтровать">
                        </form>
                    </div>
                    <? } elseif (!empty($category_id) && $category_id == 4) { ?>
                        <h2>Уточните поиск по аптекам</h2>
                        <div class="search-block-red search-place">
                            <form action="">
                                <p class="title inlineBlock red">Местоположение</p>
                                <a href="#" class="reset red">Сбросить все</a>
                                <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>
                                <p><i class="icon metro"></i><a href="#" class="dotted">Указать станцию метро</a></p>

                                <hr/>
                                <select>
                                    <option>Все аптечные сети</option>
                                    <option>Все аптечные сети 1</option>
                                    <option>Все аптечные сети 2</option>
                                    <option>Все аптечные сети 3</option>
                                    <option>Все аптечные сети 4</option>
                                </select>
                                <hr/>

                                <ul class="list-two">
                                    <li><input type="checkbox" class="checkbox" id="input_1" value="true" name="booking"/><label for="input_1">Бронирование</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_2" value="true" name="delivery"/><label for="input_2">С доставкой</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_3" value="true" name="daynight"/><label for="input_3">Круглосуточно</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_4" value="true" name="dms"/><label for="input_4">ДМС</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_5" value="true" name="dlo"/><label for="input_5">ДЛО</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_6" value="true" name="optics"/><label for="input_6">Отдел оптики</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_7" value="true" name="rpo"/><label for="input_7">Рецептурно-производственный отдел</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_6" value="true" name="homeopathy"/><label for="input_6">Отдел гомеопатии</label></li>
                                </ul>

                                <input type="submit" class="submit btn filter_button" value="Фильтровать">
                            </form>
                        </div>
                    <? } ?>

                    <div class="advice">
                        Задайте <a href="#" class="red link submit js-pUp__openeer" data-pup-id="pUp-answer">вопрос </a> по медицинским учреждениям
                        <div class="popup" style="display: none;">
                            <form action="#">
                                <textarea cols="50" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно."></textarea>
                                <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>


                </div>

            </div>


        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['popular_institutions']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>

        <?=$templates['first_banners']?>

        <?=$templates['news']?>
    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>




<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_modal ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->
            <div class="pUps__item js-pUp_m__item" id="pUp-zap">
                <div class="pUps__item__title">
                    Записаться
                </div>
                <div class="pUps__item__info-txt pUps__item__info-txt_grey">
                    Сообщение будет отправлено администратору
                    учреждения/врачу. После этого с вами должны
                    связаться по телефону или эл. почте.
                </div>

                <form>
                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Как Вас зовут?</div>
                        <div class="b-input b-input_icon b-input_icon_name">
                            <input class="" name="name" placeholder="Как Вас зовут?" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Номер телефона</div>
                        <div class="b-input b-input_icon b-input_icon_tel">
                            <input class="js-tel" name="tel" placeholder="Номер телефона" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l b-input_icon_input">
                        <div class="b-input-box__label">Эл. почта</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="email" placeholder="Эл. почта" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Сообщение</div>
                        <div class="b-input b-input_icon b-input_tArea b-input_icon_msg">
                            <textarea name="msg" placeholder="Сообщение" type="text"></textarea>
                        </div>
                    </div>
                    <input type="submit" class="submit b-btn b-btn_uc pUps__item__button pUps__item__button_finish" value="Отправить сообщение" title="Отправить сообщение">
                </form>
            </div>

            <div class="pUps__item js-pUp_m__item" id="pUp-answer">
                <div class="pUps__item__title">
                    Вопрос
                </div>

                <form>
                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Как Вас зовут?</div>
                        <div class="b-input b-input_icon b-input_icon_name">
                            <input class="" name="name" placeholder="Как Вас зовут?" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Эл. почта</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="email" placeholder="Эл. почта" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Сообщение</div>
                        <div class="b-input b-input_icon b-input_tArea b-input_icon_msg">
                            <textarea name="msg" placeholder="Сообщение" type="text"></textarea>
                        </div>
                    </div>
                    <input type="submit" class="submit b-btn b-btn_uc pUps__item__button pUps__item__button_finish" value="Задать" title="Задать">
                </form>
            </div>

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/metro_script.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>
<script>
    function metro_wrapper() {
        isVisible = $( "#metro_wrapper" ).is( ":visible" );
        console.log(isVisible);
        if (isVisible) {
            $('body').css('overflow-y','hidden');
        } else {
            $('body').css('overflow-y','auto');
        }
    }
</script>

</body>
</html>
