<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Главная</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header" class="main_page">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <!-- Подключение выпадающего списка с городами -->
                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <a href="/profile/registration"><div class="btn">Портал для профессионалов</div></a>
            </div>

            <div class="slogan">
                <h1>Портал для профессионалов</h1>
                <span class="text">Мы постарались собрать всю информацию о медицине и фармакалогии в одном месте.</span>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <div class="three_cols type-e" style="margin-bottom: 50px;">
                <h1 class="h1" style="padding-bottom: 10px;"><?=$article['NAME']?></h1>
                <div class="b-btn-ico" style="margin-bottom: 20px;">
                    <span class="b-btn-ico__ico"><img src="/images/rating-star_def.png" alt=""></span>
                    <input id="add_to_favorites" attr-id="<?=$article['ID']?>" attr-type="articles" value="<?=($in_fav) ? "Удалить из избранного" : "Добавить в избранное"?>" title="<?=($in_fav) ? "Удалить из избранного" : "Добавить в избранное"?>" class="submit b-btn b-btn_uc g-width100 b-btn__counter-gray-ico b-btn-ico__input" type="submit">
                </div>
                <p><?=$article['TEXT']?></p>

                <div style="content: '.'; visibility: hidden; display: block; height: 0; clear: both;"></div>
            </div>

        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['first_banners']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes'];?>

                <?=$templates['voiting'];?>
            </div>
        </div>

        <?=$templates['second_banners'];?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>
