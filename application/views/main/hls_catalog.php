<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - ЗОЖ</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header" class="white-text">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Здоровый образ жизни</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о здоровом образе жизни</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>1 560</b><br/>фитнес-центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867</b><br/>спорт центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 320</b><br>салонов красоты и спа</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Здоровый образ жизни</a></li>
                <li><a href="#">Активный спорт</a></li>
                <li>Футбол</li>
            </ul>
            <div class="two-cols search-result search-green">
                <div class="left-col">

                    <div class="top clearfix">
                        <h1>Футбол</h1>
                        <div class="buttons see-also">
                            <span class="title size12">Так же найдено</span>
                            <span class="see-also-link"><a href="#" class="transform">справочники</a> (12)</span>
                            <span class="see-also-link"><a href="#" class="transform">Врачи</a> (2)</span>
                        </div>
                    </div>

                    <div class="doctors-list">
                        <div class="item">

                            <div class="top">
                                <a href="#" class="dotted">Футбол для детей ( от 0 до 4 лет)</a>
                                <div class="right-block">
                                    <span class="distance">~ 25 мин</span>
                                    <a href="#" class="on-map">На карте</a>
                                </div>

                            </div>

                            <div class="left-block">
                                <span class="ava"><img src="/temp/19.jpg" alt=""></span>
                            </div>

                            <div class="right-block">

                                <table class="contacts">
                                    <tr>
                                        <td><span class="label">Адрес:</span></td>
                                        <td colspan="3">г. Москва, Хамовники район, ул. Большая Пироговская, д. 6, стр. 1</td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Метро:</span></td>
                                        <td colspan="3"><span class="metro">Спортивная</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Телефон:</span></td>
                                        <td>459 132-98-98 <br>
                                            495 123-32-23</td>
                                        <td><span class="label">График работы:</span></td>
                                        <td>9.00 - 23.00 пн-пт<br/>
                                            9.00 - 18.00 сб-вс </td>
                                    </tr>
                                </table>

                                <div class="about">
                                    У Динамо другая философия, я не хочу обсуждать эту тему. Их первый и последний громкий трансфер – переход Андрея Шевченко в Милан… А Ярмоленко? Это один из самых сильных футболистов в Украине. Но по-человечески я понимаю его желание уйти. Как понимал и желание Косты.
                                </div>

                            </div>

                        </div>

                        <div class="item">

                            <div class="top">
                                <a href="#" class="dotted">Футбол для детей ( от 0 до 4 лет)</a>
                                <div class="right-block">
                                    <span class="distance">~ 25 мин</span>
                                    <a href="#" class="on-map">На карте</a>
                                </div>

                            </div>

                            <div class="left-block">
                                <span class="ava"><img src="/temp/20.jpg" alt=""></span>
                            </div>

                            <div class="right-block">

                                <table class="contacts">
                                    <tr>
                                        <td><span class="label">Адрес:</span></td>
                                        <td colspan="3">г. Москва, Хамовники район, ул. Большая Пироговская, д. 6, стр. 1</td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Метро:</span></td>
                                        <td colspan="3"><span class="metro">Спортивная</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Телефон:</span></td>
                                        <td>459 132-98-98 <br>
                                            495 123-32-23</td>
                                        <td><span class="label">График работы:</span></td>
                                        <td>9.00 - 23.00 пн-пт<br/>
                                            9.00 - 18.00 сб-вс </td>
                                    </tr>
                                </table>

                                <div class="about">
                                    У Динамо другая философия, я не хочу обсуждать эту тему. Их первый и последний громкий трансфер – переход Андрея Шевченко в Милан… А Ярмоленко? Это один из самых сильных футболистов в Украине. Но по-человечески я понимаю его желание уйти. Как понимал и желание Косты.
                                </div>

                            </div>

                        </div>

                        <div class="item">

                            <div class="top">
                                <a href="#" class="dotted">Футбол для детей ( от 0 до 4 лет)</a>
                                <div class="right-block">
                                    <span class="distance">~ 25 мин</span>
                                    <a href="#" class="on-map">На карте</a>
                                </div>

                            </div>

                            <div class="left-block">
                                <span class="ava"><img src="/temp/19.jpg" alt=""></span>
                            </div>

                            <div class="right-block">

                                <table class="contacts">
                                    <tr>
                                        <td><span class="label">Адрес:</span></td>
                                        <td colspan="3">г. Москва, Хамовники район, ул. Большая Пироговская, д. 6, стр. 1</td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Метро:</span></td>
                                        <td colspan="3"><span class="metro">Спортивная</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Телефон:</span></td>
                                        <td>459 132-98-98 <br>
                                            495 123-32-23</td>
                                        <td><span class="label">График работы:</span></td>
                                        <td>9.00 - 23.00 пн-пт<br/>
                                            9.00 - 18.00 сб-вс </td>
                                    </tr>
                                </table>

                                <div class="about">
                                    У Динамо другая философия, я не хочу обсуждать эту тему. Их первый и последний громкий трансфер – переход Андрея Шевченко в Милан… А Ярмоленко? Это один из самых сильных футболистов в Украине. Но по-человечески я понимаю его желание уйти. Как понимал и желание Косты.
                                </div>

                            </div>

                        </div>

                        <div class="item">

                            <div class="top">
                                <a href="#" class="dotted">Футбол для детей ( от 0 до 4 лет)</a>
                                <div class="right-block">
                                    <span class="distance">~ 25 мин</span>
                                    <a href="#" class="on-map">На карте</a>
                                </div>

                            </div>

                            <div class="left-block">
                                <span class="ava"><img src="/temp/20.jpg" alt=""></span>
                            </div>

                            <div class="right-block">

                                <table class="contacts">
                                    <tr>
                                        <td><span class="label">Адрес:</span></td>
                                        <td colspan="3">г. Москва, Хамовники район, ул. Большая Пироговская, д. 6, стр. 1</td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Метро:</span></td>
                                        <td colspan="3"><span class="metro">Спортивная</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Телефон:</span></td>
                                        <td>459 132-98-98 <br>
                                            495 123-32-23</td>
                                        <td><span class="label">График работы:</span></td>
                                        <td>9.00 - 23.00 пн-пт<br/>
                                            9.00 - 18.00 сб-вс </td>
                                    </tr>
                                </table>

                                <div class="about">
                                    У Динамо другая философия, я не хочу обсуждать эту тему. Их первый и последний громкий трансфер – переход Андрея Шевченко в Милан… А Ярмоленко? Это один из самых сильных футболистов в Украине. Но по-человечески я понимаю его желание уйти. Как понимал и желание Косты.
                                </div>

                            </div>

                        </div>

                        <div class="item">

                            <div class="top">
                                <a href="#" class="dotted">Футбол для детей ( от 0 до 4 лет)</a>
                                <div class="right-block">
                                    <span class="distance">~ 25 мин</span>
                                    <a href="#" class="on-map">На карте</a>
                                </div>

                            </div>

                            <div class="left-block">
                                <span class="ava"><img src="/temp/19.jpg" alt=""></span>
                            </div>

                            <div class="right-block">

                                <table class="contacts">
                                    <tr>
                                        <td><span class="label">Адрес:</span></td>
                                        <td colspan="3">г. Москва, Хамовники район, ул. Большая Пироговская, д. 6, стр. 1</td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Метро:</span></td>
                                        <td colspan="3"><span class="metro">Спортивная</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label">Телефон:</span></td>
                                        <td>459 132-98-98 <br>
                                            495 123-32-23</td>
                                        <td><span class="label">График работы:</span></td>
                                        <td>9.00 - 23.00 пн-пт<br/>
                                            9.00 - 18.00 сб-вс </td>
                                    </tr>
                                </table>

                                <div class="about">
                                    У Динамо другая философия, я не хочу обсуждать эту тему. Их первый и последний громкий трансфер – переход Андрея Шевченко в Милан… А Ярмоленко? Это один из самых сильных футболистов в Украине. Но по-человечески я понимаю его желание уйти. Как понимал и желание Косты.
                                </div>

                            </div>

                        </div>

                    </div>


                </div>

                <div class="right-col">
                    <h2>Уточнить поиск</h2>

                    <div class="search-block-red search-block-green search-place">
                        <form action="#">
                            <p class="title inlineBlock red">Местоположение</p>
                            <a href="#" class="reset red">Сбросить все</a>
                            <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>
                            <p><i class="icon metro"></i><a href="#" class="dotted">Указать станцию метро</a></p>

                            <hr/>

                            <ul class="list-two">
                                <li><input type="checkbox" class="checkbox" id="input_1"/><label for="input_1">Фитнес</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_2"/><label for="input_2">Питание</label></li>
                                <li><input type="checkbox" class="checkbox" checked="checked" id="input_3"/><label for="input_3">Спортивные секции</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_4"/><label for="input_4">Товары для здоровья</label></li>
                            </ul>
                        </form>
                    </div>

                    <div class="advice green-block">
                        Получите <a href="#" class="red link">совет</a> по данному виду спорта
                        <div class="popup">
                            <form action="#">
                                <textarea cols="50" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно."></textarea>
                                <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>

                    <div class="links-world">
                        <span class="line"><a href="#" style="font-size: 45px;">Фитнесс</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 38px;">Спорт секции</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 34px;">Профессиональный спорт</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 28px;">Тренеры</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 24px;">Питание</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 21px;">Салоны красоты</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 19px;">Спа</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 18px;">Товары для здоровья и красоты</a><sup>1 234</sup></span>
                        <span class="line"><a href="#" style="font-size: 14px;">Интернет-магазины</a><sup>1 234</sup></span>
                    </div>

                </div>

            </div>


        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <div class="grey-block">
            <?=$templates['bottom_menu']?>

            <?=$templates['news']?>
        </div>


    </div>



    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>