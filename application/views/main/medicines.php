<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Результат поиска</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Все аптеки города</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех аптеках вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>аптек</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 433 562</b><br/>препаратов</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Товары</a></li>
                <li><a href="#">Поиск по запросу: "<?=(!empty($_GET['query'])) ? strip_tags($_GET['query']) : "Все товары"?>"</a></li>
            </ul>
        <?=(!empty($branch) ? $branch : "")?>
        <? if (!empty($organisation_card)) { ?>
        <div class="two-cols two-cols_newMed search-result search-result-list hospital">
            <div class="left-col">
                <div class="b-clinic_new">
                    <?=$organisation_card?>

                    <!--  блок с описанием  -->
                    <? if (!empty($organisation['DESCRIPTION'])) { ?>
                        <div class="b-clinic_new__descriptions">
                            <h3 class="g-NEW-title g-NEW-title_not-UpCase g-NEW-title_bord">Описание</h3>
                            <p class="g-NEW-inner">
                                <?=$organisation['DESCRIPTION']?>
                            </p>
                        </div>
                    <? } ?>
                    <!-- // блок с описанием  -->

                    <!-- блок с поиском -->
                    <div class="search-in-card search_form">
                        <div class="wrapper">
                            <div id="organisation_search" class="forma">
                                <form action="/medicines/catalog" method="get" enctype="multipart/form-data">
                                    <var class="title georgia transform">поиск</var>
                                    <span class="for_input"><input id="organisation_search_query" attr-id="<?=$item['ID']?>" type="text" class="text" autocomplete="off" placeholder="например, аптека на Невском" name="query"></span>
                                    <input type="hidden" name="id" value="1">
                                    <input type="submit" class="submit btn" value="Найти" title="Найти">
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- // блок с поиском-->
                </div>
            </div>
            <div class="right-col">
                <ul class="list-instruction">
                    <li><a href="/organisations/item/<?=$item['ID']?>">Описание</a></li>
                    <li><a href="/organisations/specialists/<?=$item['ID']?>">Специалисты</a></li>
                    <li><a href="/medicines/catalog?institution_id=<?=$item['ID']?>">Каталог</a></li>
                    <? if (empty($item['PARENT_ID'])) {?><li><a href="/organisations/branches/<?=$item['ID']?>">Отделения</a></li><? } ?>
                    <? if (!empty($item['PARENT_ID'])) {?><li><a href="/organisations/item/<?=$item['PARENT_ID']?>">Главное отделение</a></li><? } ?>
                    <li><a href="/organisations/articles/<?=$item['ID']?>">Публикации</a></li>
                    <!--                        <li><a href="/institutions/actions_list/--><?//=$item['ID']?><!--">Акции</a></li>-->
                    <li><a href="/organisations/photos/<?=$item['ID']?>">Фото</a></li>
                    <li><a href="/organisations/videos/<?=$item['ID']?>">Видео</a></li>
                    <li><a href="/organisations/feedbacks/<?=$item['ID']?>">Отзывы</a></li>
                </ul>
            </div>
        </div>
        <? } ?>
        </div>

        <div class="container">
            <div class="two-cols search-result">
                <div class="left-col">
                    <div class="top clearfix">
                        <h1><?=(!empty($_GET['query'])) ? strip_tags($_GET['query']) : "Все товары"?></h1>
                        <div class="buttons">
                            <a href="/economical_buy" class="btn"><i class="icon percent"></i>Рассчитать экономную покупку</a>
<!--                            <a href="#" class="btn"><i class="icon other"></i>Подобрать аналоги</a>-->
                        </div>
                    </div>

                    <table class="results-table">
                        <tr>
                            <th class="name"><span class="title">Название <span class="sort sortDown"></span></span></th>
                            <th class="producer"><span class="title">Производитель <span class="sort sortUp"></span></span></th>
                            <th class="forma"><span class="title">Форма <br/>выпуска <span class="sort sortDown"></span></span></th>
                            <th class="value"><span class="title">Количество <span class="sort sortDown"></span></span></th>
                            <th class="price"><span class="title">Цена <span class="sort sortDown"></span></span></th>
                            <th class="pharmacy"><span class="title">Организация <span class="sort sortDown"></span></span></th>
                            <th class="on-map">На карте</th>
                            <th class="on-map">Бронь</th>
                        </tr>
                        <? foreach ($goods as $good) { ?>
                            <tr>
                                <td class="name"><a href="/medicines/item/<?=$good['NOMEN_ID']?>"><?=$good['DRUGNAME']?></a></td>
                                <td class="producer"><span class="shadow"><a href="#"><?=$good['FIRMNAME']?></a></span></td>
                                <td class="forma">10 мгр</td>
                                <td class="value"><?=$good['COUNT']?> шт</td>
                                <td class="price"><?=$good['PRICE']?> Р</td>
                                <td class="pharmacy">&laquo;<a href="#"><?=$good['BRAND']?></a>&raquo;<br><?=implode(',', array_filter([$good['REGION'],$good['CITY'],$good['STREET'],$good['HOUSE'],$good['BUILDING'],$good['OFFICE']]))?></td>
                                <td class="on-map"><span class="link" onclick="show_on_map('<?=$good['BRANCH_ID']?>','<?=$good['LATITUDE']?>','<?=$good['LONGITUDE']?>','<?=$good['LOGO']?>','<?=$good['BRAND']?>');">На карте</span></td>
                                <td class=""><a class="booking" href="" class="red js-pUp__openeer" data-pup-id="pUp-bron"  attr-id="<?=$good['ID']?>"><span class="link">Бронь</span></a></td>
                            </tr>
                            <tr>
                                <td colspan="7" class="map-line">
                                    <div id="id<?=$good['BRANCH_ID']?>" class="map-cell" style="height: 400px;">

                                    </div>
                                </td>
                            </tr>
                        <? } ?>
                    </table>

                    <div class="pagination">
                        <p>Мы показываем по <a href="/medicines/catalog<?=$get_replacer(['per_page' => 25])?>" <?=((!empty($_GET['per_page']) && $_GET['per_page'] == 25) ? "class=\"active\"" : "")?>>25</a> <a href="/medicines/catalog<?=$get_replacer(['per_page' => 50])?>" <?=((!empty($_GET['per_page']) && $_GET['per_page'] == 50) ? "class=\"active\"" : "")?>>50</a> <a href="/medicines/catalog<?=$get_replacer(['per_page' => 100])?>" <?=((!empty($_GET['per_page']) && $_GET['per_page'] == 100) ? "class=\"active\"" : "")?>>100</a> результатов на странице
                            <?=$pagination?>
                    </div>

<!--                    <div class="instruction">-->
<!--                        <i class="icon guide"></i>-->
<!--                        <div class="middle">-->
<!--                            <h2>Инструкция <small>Фарацетрон 20 мг №20</small></h2>-->
<!--                            <p class="instruction-text">Интерпретация всех изложенных ниже наблюдений предполагает, что еще до начала измерений упаривание известно. Притяжение, как бы это ни казалось симбиотичным, растворимо испускает интермедиат. Бюретка, как того требуют</p>-->
<!--                        </div>-->
<!--                        <a class="full-info" href="#">Полностью</a>-->
<!--                    </div>-->

                    <? if ((!empty($item['CATEGORY_ID']) && $item['CATEGORY_ID'] == 4) || empty($item['CATEGORY_ID'])) {?>
                    <div class="special-offer analog-offer">
                        <h3>АНАЛОГИ ЛЕКАРСТВ</h3>
                        <p class="note">Совпадает код АТС, состав действующих веществ и форма выпуска</p>
                        <hr/>
                        <div class="list">
                            <div class="special-offer-good">
                                <img class="preview" src="/temp/offer-1.png" alt=""/>
                                <span class="sale">50 % <var>скидка</var></span>
                                <a class="title" href="#">Никоретте</a>
                                <p>2 100 - 4 500 Р</p>
                            </div>

                            <div class="special-offer-good">
                                <img class="preview" src="/temp/offer-2.jpg" alt=""/>
                                <span class="sale">50 % <var>скидка</var></span>
                                <a class="title" href="#">Coldrex</a>
                                <p>2 100 - 4 500 Р</p>
                            </div>

                            <div class="special-offer-good">
                                <img class="preview" src="/temp/offer-1.png" alt=""/>
                                <a class="title" href="#">Никоретте</a>
                                <p>2 100 - 4 500 Р</p>
                            </div>

                            <div class="special-offer-good">
                                <img class="preview" src="/temp/offer-2.jpg" alt=""/>
                                <a class="title" href="#">Coldrex</a>
                                <p>2 100 - 4 500 Р</p>
                            </div>

                            <div class="special-offer-good">
                                <img class="preview" src="/temp/offer-1.png" alt=""/>
                                <span class="sale">50 % <var>скидка</var></span>
                                <a class="title" href="#">Никоретте</a>
                                <p>2 100 - 4 500 Р</p>
                            </div>

                            <div class="special-offer-good">
                                <img class="preview" src="/temp/offer-2.jpg" alt=""/>
                                <span class="sale">50 % <var>скидка</var></span>
                                <a class="title" href="#">Coldrex</a>
                                <p>2 100 - 4 500 Р</p>
                            </div>

                        </div>
                    </div>
                    <? } ?>

                </div>

                <div class="right-col">
                    <h2>Уточнить поиск</h2>
                    <? if (!$all) { ?>
                    <div class="search-block-red search-place">
                        <form action="">
                            <p class="title inlineBlock red">Местоположение</p>
                            <a href="#" class="reset red">Сбросить все</a>
                            <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>
                            <p><i class="icon metro"></i><a href="#" class="dotted">Указать станцию метро</a></p>

                            <hr/>
                            <select>
                                <option>Все компании</option>
                                <? foreach ($organisations as $organisation) { ?>
                                    <option value="<?=$organisation['ID']?>"><?=$organisation['BRAND']?></option>
                                <? } ?>
                            </select>
                            <hr/>

                            <ul class="list-two">
                                <li><input type="checkbox" class="checkbox" id="input_1"/><label for="input_1">Бронирование</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_2"/><label for="input_2">С доставкой</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_3"/><label for="input_3">Круглосуточно</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_4"/><label for="input_4">ДМС</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_5"/><label for="input_5">ДЛО</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_6"/><label for="input_6">Отдел оптики</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_7"/><label for="input_7">Рецептурно-производственный отдел</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_6"/><label for="input_6">Отдел гомеопатии</label></li>
                            </ul>

                            <input type="submit" class="submit btn filter_button" value="Фильтровать">
                        </form>
                    </div>
                    <? } ?>

                    <div class="search-block-red speciality">
                        <form action="#">
                            <p class="title inlineBlock red">Особенности препарата</p>
                            <select>
                                <option>Любая форма выпуска</option>
                                <option>Любая форма выпуска 1</option>
                                <option>Любая форма выпуска 2</option>
                                <option>Любая форма выпуска 3</option>
                                <option>Любая форма выпуска 4</option>
                            </select>
                            <hr/>
                            <select>
                                <option>Все производители</option>
                                <option>Все производители 1</option>
                                <option>Все производители 2</option>
                                <option>Все производители 3</option>
                                <option>Все производители 4</option>
                            </select>
                            <hr/>
                            <p class="label-text"><span class="label">Срок годности</span><input type="text" class="text" placeholder="Не менее (год)"/></p>
                            <hr/>
                            <p class="label-text"><span class="label">Количество в аптеке</span><input type="text" class="text" placeholder=""/></p>
                            <hr/>
                            <p class="label-text"><span class="label">Минимальные цены в аптеках</span><input type="text" class="text" placeholder="Минимум"/></p>
                            <hr/>
                            <select>
                                <option>Аналоги</option>
                                <option>Аналоги 1</option>
                                <option>Аналоги 2</option>
                                <option>Аналоги 3</option>
                                <option>Аналоги 4</option>
                            </select>
                            <input type="submit" class="submit btn filter_button" value="Фильтровать">
                        </form>
                    </div>

                    <div class="advice">
                        Получите <a href="#" class="red link">совет</a> по данному препарату
                        <div class="popup" style="display: none;">
                            <form action="#">
                                <textarea cols="50" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно."></textarea>
                                <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>


                </div>

                <div class="clear"></div>

            </div>
        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>



<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_modal popup-position" style="margin-left: -187px;">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->
            <div class="pUps__item js-pUp_m__item" id="pUp-bron">
                <div class="pUps__item__title">
                    Забронировать товар <br>
                    <span id="good_name"></span>
                </div>
                <div class="pUps__item__info-txt pUps__item__info-txt_grey">
                    Укажите количество, имя и способ связи с вами
                </div>

                <form id="booking_form">
                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Как Вас зовут?</div>
                        <div class="b-input b-input_icon b-input_icon_name">
                            <input class="" name="name" placeholder="Как Вас зовут?" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Номер телефона</div>
                        <div class="b-input b-input_icon b-input_icon_tel">
                            <input class="" name="phone" placeholder="Номер телефона" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l b-input_icon_input">
                        <div class="b-input-box__label">Электронная почта</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="email" placeholder="Эл. почта" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l b-input_icon_input">
                        <div class="b-input-box__label">Количество товара</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="quantity" placeholder="Количество" type="text"></input>
                        </div>
                    </div>
                    <input id="good_id" class="" name="id" value="" type="hidden"></input>
                    <input type="submit" class="submit b-btn b-btn_uc pUps__item__button pUps__item__button_finish booking_submit" value="Забронировать" title="Забронировать">
                </form>
            </div>

            <!-- success -->

            <div class="pUps__item pUps__item_comments b-comments js-pUp_m__item" id="pUp-success">
                <div class="b-comments__info">
                    Товар забронирован!
                </div>
            </div>

            <!-- END success -->

            <div class="pUps__item pUps__item_comments b-comments js-pUp_m__item" id="pUp-comment">
                <div class="b-comments__title">
                    ОТЗЫВ
                </div>
                <div class="b-comments__info">
                    Сообщение будет отправлено администратору сайта для формирования рейтинга специалистов.
                </div>
                <div class="b-comments__fields-box">
                    <form class="b-comments-f">
                        <input type="hidden" name="object" value="1">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <ul class="b-rating">
                            <li class="b-rating__row">
                                <span class="b-rating__label">Дата визита</span>
                                <div class="b-input-date_ico js-date_box">
                                    <input class="b-rating__date js-date-i" id="b-rating__date" name="date" type="text"></p>
                                </div>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Место приема</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="place_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Цена/качество</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="price_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Компетенция специалиста</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="competence_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Внимание специалиста</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="care_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                        </ul>
                </div>
                <input type="submit" class="submit b-btn b-btn_uc g-width100 send_rating" data-pup-id="pUp-comment" value="Оставить отзыв" title="Оставить отзыв">
                </form>
            </div>

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->



<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>
