<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Результат поиска</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Все аптеки города</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех аптеках вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>аптек</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 433 562</b><br/>препаратов</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="/database/medicines">Товары</a></li>
                <li><?=$nomen['NAME']?></li>
            </ul>

            <div class="search-card">
                <div class="two-cols">
                    <div class="left-col">
                        <div class="description">
                            <div class="image">
                                <? if (empty($nomen['IMAGE'])) { ?>
                                    <img src="/temp/10.jpg" alt="">
                                <? } else { ?>
                                    <img src="/images/rls/<?=(int)$nomen['IMAGE']?>.gif" alt="">
                                <? } ?>
                            </div>
                            <div class="params">
                                <h1>
                                    <span class="transform"><?=$nomen['NAME']?></span> <!-- 20 мг №20 -->
                                    <em><?=$nomen['LNAME']?></em>
                                </h1>
                                <table class="table-card">
                                    <? if (!empty($nomen['NOMENFIRMNAME'])) { ?>
                                    <tr>
                                        <td>Производитель:</td>
                                        <td><a href="#"><?=$nomen['NOMENFIRMNAME']?></a></td>
                                    </tr>
                                    <? } ?>
                                    <? if (!empty($nomen['NOMENCOUNTRY'])) { ?>
                                    <tr>
                                        <td>Страна:</td>
                                        <td><?=$nomen['NOMENCOUNTRY']?></td>
                                    </tr>
                                    <? } ?>
<!--                                    <tr>-->
<!--                                        <td class = "table-head">Форма выпуска:</td>-->
<!--                                        <td>-->
<!--                                            10 мг, 30 шт <br/>-->
<!--                                            <a class="red size12 dotted other-link" href="#">Все формы</a>-->
<!--                                            <div class="other">-->
<!--                                                <ul>-->
<!--                                                    <li>Таблетки</li>-->
<!--                                                    <li>Смесь</li>-->
<!--                                                    <li>Ампулы</li>-->
<!--                                                </ul>-->
<!--                                            </div>-->
<!--                                        </td>-->
<!--                                    </tr>-->
                                    <? if (!empty($prices['MIN']) && !empty($prices['MAX'])) { ?>
                                    <tr>
                                        <td class = "table-head">Цены:</td>
                                        <td>
                                            от <?=$prices['MIN']?> Р до <?=$prices['MAX']?> Р
                                        </td>
                                    </tr>
                                    <? } ?>
                                    <? if (!empty($drugstores)) { ?>
                                    <tr>
                                        <td class = "table-head">Наличие:</td>
                                        <td>
                                            <?=$drugstores?> аптеки <br/>
                                            <a class = "red size12" href="/medicines/catalog?id=<?=$nomen['NOMENID']?>">Показать список</a>
                                        </td>
                                    </tr>
                                    <? } else { ?>
                                    <tr>
                                        <td class = "table-head">Наличие:</td>
                                        <td>
                                            Нет в наличии
                                        </td>
                                    </tr>
                                    <? } ?>
                                </table>
                                <div class="b-btn-ico">
                                    <span class="b-btn-ico__ico"><img src="/images/rating-star_def.png" alt=""></span>
                                    <input id="add_to_favorites" attr-id="<?=$nomen['NOMENID']?>" attr-type="medicines" <? if ($in_fav) { ?>value="Удалить из избранного" title="Удалить из избранного" <? } else { ?>value="Добавить в избранное" title="Добавить в избранное" <? } ?> class="submit b-btn b-btn_uc g-width100 b-btn__counter-gray-ico b-btn-ico__input" type="submit">
                                </div>
                            </div>
                        </div>

                        <? if (!empty($nomen['DRUGFORMNAME'])) {?>
                        <h2 id="DRUGFORM">Форма выпуска</h2>
                        <p><?=trim($nomen['DRUGFORMNAME'])?></p>
                        <hr/>
                        <? } ?>

                        <? if (!empty($nomen['COMPOSITION'])) {?>
                        <h2 id="COMPOSITION">Состав</h2>
                        <p><?=trim($nomen['COMPOSITION'])?></p>
                        <hr/>
                        <? } ?>


                        <? if (!empty($nomen['PPACKNAME'])) {?>
                        <h2 id="PPACK">Упаковка</h2>
                        <p><?=trim($nomen['PPACKNAME'])?></p>
                        <hr/>
                        <? } ?>

                        <? if (!empty($nomen['PHARMAACTIONS'])) {?>
                        <h2 id="PHARMAACTIONS">Фармакологическое действие</h2>
                        <p><?=trim($nomen['PHARMAACTIONS'])?></p>
                        <hr/>
                        <? } ?>

                        <? if (!empty($nomen['PHARMAKINETIC'])) {?>
                        <h2 id="PHARMAKINETIC">Фармакокинетика</h2>
                        <p><?=trim($nomen['PHARMAKINETIC'])?></p>
                        <hr/>
                        <? } ?>

                        <? if (!empty($nomen['PHARMADYNAMIC'])) {?>
                        <h2 id="PHARMADYNAMIC">Фармакодинамика</h2>
                        <p><?=trim($nomen['PHARMADYNAMIC'])?></p>
                        <hr/>
                        <? } ?>

                        <? if (!empty($nomen['INDICATIONS'])) {?>
                        <h2 id="INDICATIONS">Показания</h2>
                        <p><?=trim($nomen['INDICATIONS'])?></p>
                        <hr/>
                        <? } ?>

                        <? if (!empty($nomen['CONTRAINDICATIONS'])) {?>
                        <h2 id="CONTRAINDICATIONS">Противопоказания</h2>
                        <p><?=trim($nomen['CONTRAINDICATIONS'])?></p>
                        <hr/>
                        <? } ?>

                        <? if (!empty($nomen['USEMETHODANDDOSES'])) {?>
                        <h2 id="USEMETHODANDDOSES">Способ применения и дозы</h2>
                        <p><?=trim($nomen['USEMETHODANDDOSES'])?></p>
                        <hr/>
                        <? } ?>
                    </div>


                    <div class="right-col">
                        <h2>Инструкция</h2>

                        <ul class="list-instruction">
                            <? if (!empty($nomen['DRUGFORMNAME'])) { ?><li><a href="#DRUGFORM">Форма выпуска</a></li><? } ?>
                            <? if (!empty($nomen['COMPOSITION'])) { ?><li><a href="#COMPOSITION">Состав</a></li><? } ?>
                            <? if (!empty($nomen['PPACKNAME'])) { ?><li><a href="#PPACK">Упаковка</a></li><? } ?>
                            <? if (!empty($nomen['PHARMAACTIONS'])) { ?><li><a href="#PHARMAACTIONS">Фарм.Действие</a></li><? } ?>
                            <? if (!empty($nomen['PHARMAKINETIC'])) { ?><li><a href="#PHARMAKINETIC">Фармакокинетика</a></li><? } ?>
                            <? if (!empty($nomen['PHARMADYNAMIC'])) { ?><li><a href="#PHARMADYNAMIC">Фармакодинамика</a></li><? } ?>
                            <? if (!empty($nomen['INDICATIONS'])) { ?><li><a href="#INDICATIONS">Показания</a></li><? } ?>
                            <? if (!empty($nomen['CONTRAINDICATIONS'])) { ?><li><a href="#CONTRAINDICATIONS">Противопоказания</a></li><? } ?>
                            <? if (!empty($nomen['USEMETHODANDDOSES'])) { ?><li><a href="#USEMETHODANDDOSES">Способ применения и дозы</a></li><? } ?>
                        </ul>

                        <div class="advice">
                            Получите <a href="#" class="red link">совет</a> по данному препарату
                            <div class="popup">
                                <form action="#">
                                    <textarea cols="50" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно."></textarea>
                                    <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                                </form>
                                <span class="arrow"></span>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>


            <div class="special-offer analog-offer container analog-offer-8">
                <h3>АНАЛОГИ ЛЕКАРСТВ</h3>
                <p class="note">Совпадает код АТС, состав действующих веществ и форма выпуска</p>
                <hr/>
                <div class="list">
                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-1.png" alt=""/>
                        <span class="sale">50 % <var>скидка</var></span>
                        <a class="title" href="#">Никоретте</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>

                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-2.jpg" alt=""/>
                        <span class="sale">50 % <var>скидка</var></span>
                        <a class="title" href="#">Coldrex</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>

                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-1.png" alt=""/>
                        <a class="title" href="#">Никоретте</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>

                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-2.jpg" alt=""/>
                        <a class="title" href="#">Coldrex</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>

                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-1.png" alt=""/>
                        <span class="sale">50 % <var>скидка</var></span>
                        <a class="title" href="#">Никоретте</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>

                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-2.jpg" alt=""/>
                        <span class="sale">50 % <var>скидка</var></span>
                        <a class="title" href="#">Coldrex</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>
                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-1.png" alt=""/>
                        <span class="sale">50 % <var>скидка</var></span>
                        <a class="title" href="#">Никоретте</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>

                    <div class="special-offer-good">
                        <img class="preview" src="/temp/offer-2.jpg" alt=""/>
                        <span class="sale">50 % <var>скидка</var></span>
                        <a class="title" href="#">Coldrex</a>
                        <p>2 100 - 4 500 Р</p>
                    </div>

                </div>
            </div>

        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>