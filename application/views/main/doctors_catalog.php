<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Врачи</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="/css/metro_style.css" />
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>



<!-- метро для фильтра -->

<div id="metro_wrapper" style="width: 100vw; height: 100vh; background-color: rgba(0, 0, 0, 0.1); position: fixed; top: 0; left: 0; z-index: 9999999999; display: none;">
    <div class="test" style="background: #fff; width: 1250px; height: 80vh; margin: 10vh auto; border-top: 3px solid #c44848; border-bottom: 3px solid #fe5401; background: linear-gradient(to bottom, #c44848 0%, #fe5401 100%); overflow-y: scroll;">
        <div class="popSubwayBlock clearfix" style="height: 100vh; margin: 0 auto;">

            <div class="subwayPopUpMap_metro_header clearfix">
                <div class="right" style="margin: 27px 40px 0 0;"><input style="background: #666; padding: 1px; width:177px; cursor:pointer" name="" onClick="$('#metro_wrapper').toggle(); metro_wrapper(); get_selected_metro();" type="submit" value="Выбрать" class="btn" style="width:170px;"></div>
                <div class="subwayPopUpMap_mission">
                    Выберите расположение: по карте метро или по карте районов Москвы
                </div>
            </div>
            <div class="subwayPopUpMap_metro_block">
                <div class="subwayPopUpMap_filter_top clearfix">
                    <a href="#" class="unlink"><img src="/images/filter3.png">
			<span>
				Выбрать станции внутри <br>произвольной области <br><i>(просто выделите область мышью)</i>
			</span>
                    </a>
                    <a href="#" id="vnutrikolcevie"><i></i>
			<span>
				Выбрать станции <br>внутри кольцевой ветки
			</span>
                    </a>
                    <a href="#" id="kolcevaya"><i></i>
			<span>
				Выбрать станции <br>кольцевой ветки
			</span>
                    </a>
                    <a href="#" id="reset_all" onclick="$('.subwayPopUpMap_checkbox input').prop('checked',false); $('.subwayPopUpMap_checkbox .checked').removeClass('checked');
			$(this).removeClass('checked');"><i></i>
                        <span>Очистить выбор</span>
                    </a>
                </div>

                <div class="subwayPopUpMap_filter_right">
                    <div class="subwayPopUpMap_filter_map">
                        <a href="#" id="vnutrikolcevie"><i>Центр</i></a>
                        <a href="#" id="sad_zapad"><i>САД-<br>ТТК <br>(запад)</i></a>
                        <a href="#" id="sad_sever"><i>САД-ТТК (север)</i></a>
                        <a href="#" id="sad_vostok"><i>САД-<br>ТТК <br>(восток)</i></a>
                        <a href="#" id="sad_yug"><i>САД-ТТК (юг)</i></a>
                        <a href="#" id="mkad_zapad"><i>ТТК-МКАД (запад)</i></a>
                        <a href="#" id="mkad_sever"><i>ТТК-МКАД (север)</i></a>
                        <a href="#" id="mkad_vostok"><i>ТТК-МКАД (восток)</i></a>
                        <a href="#" id="mkad_yug"><i>ТТК-МКАД (юг)</i></a>
                        <a href="#" id="new_moscow"><i>Новая Москва</i></a>
                    </div>
                    <div class="subwayPopUpMap_filter_names">
                        <a href="#" id="vnutrikolcevie"><i>Центр</i></a>
                        <a href="#" id="sad_zapad"><i>САД-ТТК (запад)</i></a>
                        <a href="#" id="sad_sever"><i>САД-ТТК (север)</i></a>
                        <a href="#" id="sad_vostok"><i>САД-ТТК (восток)</i></a>
                        <a href="#" id="sad_yug"><i>САД-ТТК (юг)</i></a>
                        <a href="#" id="mkad_zapad"><i>ТТК-МКАД (запад)</i></a>
                        <a href="#" id="mkad_sever"><i>ТТК-МКАД (восток)</i></a>
                        <a href="#" id="mkad_vostok"><i>ТТК-МКАД (восток)</i></a>
                        <a href="#" id="mkad_yug"><i>ТТК-МКАД (юг)</i></a>
                        <a href="#" id="new_moscow"><i>Новая Москва</i></a>
                    </div>
                </div>

                <div class="subwayPopUpMap_filter">
                    <a href="#" id="sokolnicheskaya_right">↓</a>
                    <a href="#" id="sokolnicheskaya_left">↑</a>
                    <a href="#" id="zamoskvoreckaya_left">↓</a>
                    <a href="#" id="zamoskvoreckaya_right">↑</a>
                    <a href="#" id="arbatsko-pokrovskaya_left">↓</a>
                    <a href="#" id="arbatsko-pokrovskaya_right">↓</a>
                    <a href="#" id="filevskaya_left">↓</a>
                    <a href="#" id="kalujsko-rijskaya_top">↓</a>
                    <a href="#" id="kalujsko-rijskaya_bottom">↑</a>
                    <a href="#" id="tagansko-krasnopresnenskaya_left">↓</a>
                    <a href="#" id="tagansko-krasnopresnenskaya_right">↑</a>
                    <a href="#" id="kalininskaya_left">↓</a>
                    <a href="#" id="kalininskaya_right">↓</a>
                    <a href="#" id="serpuxovsko-timiryazevskaya_top">↓</a>
                    <a href="#" id="serpuxovsko-timiryazevskaya_bottom">↑</a>
                    <a href="#" id="lyublinskaya_top">↓</a>
                    <a href="#" id="lyublinskaya_bottom">↑</a>
                    <a href="#" id="kaxovskaya_bottom">↑</a>
                    <a href="#" id="butovskaya_bottom">↓</a>
                </div>

                <div class="subwayPopUpMap_metro_images">
                    <div class="subwayPopUpMap_metro_stores">
                        <form action="" id="metro_map_form">
                            <? foreach ($metro as $line => $stations) { ?>
                                <div class="<?=$line?>">
                                    <? foreach ($stations as $station) { ?>
                                        <label class="subwayPopUpMap_checkbox <?=$station['CLASS']?>" style="top: <?=$station['TOP']?>px; left: <?=$station['LEFT']?>px;">
                                            <input type="checkbox" value="<?=$station['ID']?>" form="filter" data-name="<?=$station['NAME']?>" name="m[]">
                                            <i class="ui-selectee"></i>
                                            <span><?=$station['NAME']?><?=$station['ID']?></span>
                                        </label>
                                    <? } ?>
                                </div>
                            <? } ?>
                            <img src="/images/metro_bright.png" alt="" id="metro_bright">
                            <div class="text-center"><br><input style="background: #666; padding: 1px; width:177px; cursor:pointer" type="button" value="Выбрать" class="btn" style="width:170px;" onclick="$('#metro_wrapper').toggle(); metro_wrapper(); get_selected_metro();"><br></div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end метро -->





<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Врачи</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех врачах вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>специалистов</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <div class="two-cols search-result">
                <div class="left-col">

                    <div class="top clearfix">
                        <h1>Специалисты</h1>
                        <div class="buttons see-also">
                            <span class="title size12">Так же найдено</span>
                            <span class="see-also-link"><a href="#" class="transform">справочники</a> (12)</span>
                            <span class="see-also-link"><a href="#" class="transform">Врачи</a> (2)</span>
                        </div>
                    </div>

                    <div class="doctors-list">
                        <? foreach ($doctors as $doctor) { ?>
                            <div class="item">
                                <div class="left-block">
                                    <span class="ava"><img src="<?=$doctor["PHOTO"]?>" alt=""></span>
                                    <p class="name"><a href="/doctors/item/<?=$doctor["ID"]?>"><?=$doctor["FULL_NAME"]?></a></p>
                                    <p class="title"><?=$doctor['SPECIALIZATION_NAME']?></p>
<!--                                    <p class="price">2 500 Р</p>-->
<!--                                    <span class="price-note">Стоимость приема</span>-->
                                    <a href="#" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_1_<?=$doctor['ID']?>">Запись на прием</a>
                                </div>

                                <div class="right-block">
                                    <div class="about">
                                        <?=$doctor["DESCRIPTION"]?>
                                    </div>

                                    <table class="contacts">
                                        <tr>
                                            <td><span class="label">Место работы:</span></td>
                                            <td colspan="3"><?=(!empty($doctor["INSTITUTION"])) ? "<a href=\"/institutions/item/".$doctor["INSTITUTION_ID"]."\">".$doctor["INSTITUTION"]."</a>" : $doctor["BRANCH"]?></td>
                                        </tr>
                                        <? if (!empty($doctor["INSTITUTION"])) { ?>
                                        <tr>
                                            <td><span class="label">Адрес:</span></td>
                                            <td colspan="3"><?=$doctor["INSTITUTION_ADDRESS"]?></td>
                                        </tr>
                                        <tr>
                                            <td><span class="label">Метро:</span></td>
                                            <td colspan="3"><span class="metro"><?=$doctor["INSTITUTION_METRO"]?></span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="label">Телефон:</span></td>
                                            <td><?=nl2br($doctor["INSTITUTION_PHONES"])?></td>
                                            <td><span class="label">График работы:</span></td>
                                            <td><?=nl2br($doctor["INSTITUTION_SCHEDULE"])?></td>
                                        </tr>
                                        <? } ?>
                                    </table>

                                    <ul class="links transform size10">
                                        <li><a href="/doctors/item/<?=$doctor["ID"]?>">График</a></li>
<!--                                        <li><a href="#">Рейтинг</a></li>-->
                                        <li><a href="/doctors/opinions/<?=$doctor["ID"]?>/<?=$doctor["USER_ID"]?>">Отзывы</a></li>
<!--                                        <li><a href="#">Публикации</a></li>-->
<!--                                        <li><a href="#">Награды</a></li>-->
                                    </ul>
                                </div>

                            </div>
                        <? } ?>
                        <?=$pagination?>
                    </div>


                </div>

                <div class="right-col">
                    <h2>Уточнить поиск по врачам</h2>
                    <div class="search-block-red search-place">
                        <form id="filter" action="" method="get">
                            <p class="title inlineBlock red">Местоположение</p>
                            <a href="#" class="reset red">Сбросить все</a>
                            <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>
                            <p><i class="icon place"></i><a href="#" class="dotted" onclick="event.preventDefault(); $('#metro_wrapper').toggle(); metro_wrapper();">Выбрать метро</a></p>
                            <ul id="metro_list" style="max-height: 90px; overflow-y: auto;">

                            </ul>

                            <hr/>

<!--                            <select>-->
<!--                                <option>Cпециализация</option>-->
<!--                                <option>Cпециализация 1</option>-->
<!--                                <option>Cпециализация 2</option>-->
<!--                                <option>Cпециализация 3</option>-->
<!--                                <option>Cпециализация 4</option>-->
<!--                            </select>-->
<!---->
<!--                            <hr/>-->

                            <select name="doctor_types">
                                <option>Врач</option>
                                <? foreach ($doctor_types as $doctor_type) { ?>
                                    <option value="<?=$doctor_type['ID']?>"><?=$doctor_type['NAME']?></option>
                                <? } ?>
                            </select>

                            <hr/>

                            <select>
                                <option>Анализы</option>
                                <option>Анализы 1</option>
                                <option>Анализы 2</option>
                                <option>Анализы 3</option>
                                <option>Анализы 4</option>
                            </select>

                            <hr/>

                            <select>
                                <option>Диагностика</option>
                                <option>Диагностика 1</option>
                                <option>Диагностика 2</option>
                                <option>Диагностика 3</option>
                                <option>Диагностика 4</option>
                            </select>

                            <hr/>


                            <ul class="list-two">
                                <li><input type="checkbox" class="checkbox" checked="checked" id="input_3"/><label for="input_3">Круглосуточно</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_5"/><label for="input_5">Скорая помощь</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_4"/><label for="input_4">Детский врач</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_4"/><label for="input_4">Выезд на день</label></li>
                            </ul>

                            <input type="submit" class="submit btn filter_button" value="Фильтровать">
                        </form>
                    </div>

                    <div class="advice">
                        Задайте <a href="#" class="red link js-pUp__openeer" data-pup-id="pUp-answer">вопрос</a> по специалистам
                        <div class="popup" style="display: none;">
                            <form action="#">
                                <textarea cols="50" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно."></textarea>
                                <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>

                </div>

            </div>


        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['first_banners']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>

        <?=$templates['news']?>
    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>




<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_modal ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->
            <div class="pUps__item js-pUp_m__item" id="pUp-zap">
                <div class="pUps__item__title">
                    Записаться
                </div>
                <div class="pUps__item__info-txt pUps__item__info-txt_grey">
                    Сообщение будет отправлено администратору
                    учреждения/врачу. После этого с вами должны
                    связаться по телефону или эл. почте.
                </div>

                <form>
                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Как Вас зовут?</div>
                        <div class="b-input b-input_icon b-input_icon_name">
                            <input class="" name="name" placeholder="Как Вас зовут?" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Номер телефона</div>
                        <div class="b-input b-input_icon b-input_icon_tel">
                            <input class="js-tel" name="tel" placeholder="Номер телефона" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l b-input_icon_input">
                        <div class="b-input-box__label">Эл. почта</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="email" placeholder="Эл. почта" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Сообщение</div>
                        <div class="b-input b-input_icon b-input_tArea b-input_icon_msg">
                            <textarea name="msg" placeholder="Сообщение" type="text"></textarea>
                        </div>
                    </div>
                    <input type="submit" class="submit b-btn b-btn_uc pUps__item__button pUps__item__button_finish" value="Отправить сообщение" title="Отправить сообщение">
                </form>
            </div>

            <div class="pUps__item js-pUp_m__item" id="pUp-answer">
                <div class="pUps__item__title">
                    Вопрос
                </div>

                <form>
                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Как Вас зовут?</div>
                        <div class="b-input b-input_icon b-input_icon_name">
                            <input class="" name="name" placeholder="Как Вас зовут?" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Эл. почта</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="email" placeholder="Эл. почта" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Сообщение</div>
                        <div class="b-input b-input_icon b-input_tArea b-input_icon_msg">
                            <textarea name="msg" placeholder="Сообщение" type="text"></textarea>
                        </div>
                    </div>
                    <input type="submit" class="submit b-btn b-btn_uc pUps__item__button pUps__item__button_finish" value="Задать" title="Задать">
                </form>
            </div>


            <div class="pUps__item js-pUp_m__item" id="pUp-bron">
                <div class="pUps__item__title">
                    Забронировать товар
                </div>
                <div class="pUps__item__info-txt pUps__item__info-txt_grey">
                    Укажите количество, имя и способ связи с вами
                </div>

                <form>
                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Как Вас зовут?</div>
                        <div class="b-input b-input_icon b-input_icon_name">
                            <input class="" name="name" placeholder="Как Вас зовут?" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Номер телефона</div>
                        <div class="b-input b-input_icon b-input_icon_tel">
                            <input class="js-tel" name="phone" placeholder="Номер телефона" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l b-input_icon_input">
                        <div class="b-input-box__label">Количество товара</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="quantity" placeholder="Эл. почта" type="text"></input>
                        </div>
                    </div>
                    <input type="submit" class="submit b-btn b-btn_uc pUps__item__button pUps__item__button_finish" value="Забронировать" title="Забронировать">
                </form>
            </div>



            <? foreach ($doctors_copy as $item) {?>

            <!-- 1-й шаг ПОПАП - записи к врачу - ВЫБОР ДАТЫ-->
            <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_1_<?=$item['ID']?>">
                <h2 class="h2 pUps__name">Запись к специалисту</h2>
                <div class="b-profile">
                    <div class="b-profile__short-inf b-info-box">
                        <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                        <span class="b-info-box__type"><?=isset($item['LINKED_SPECIALIZATION']) ?: "" ?></span>
                        <div class="b-info-box__foto">
                            <? if (!empty($item['PHOTO'])) { ?>
                                <img src="<?=$item['PHOTO']?>">
                            <? } ?>
                        </div>
                        <? if (!empty($work)) {?>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Клиника: </span>
                                </div>
                                <div class="b-fields-row__content">
                                    <a class="g-link" href="#"><?=$work['NAME']?></a>
                                </div>
                            </div>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Адрес: </span>
                                </div>
                                <div class="b-fields-row__content">
                                    <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                </div>
                            </div>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Метро: </span>
                                </div>
                                <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                </div>
                            </div>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Телефон: </span>
                                </div>
                                <div class="b-fields-row__content">
                                    <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                </div>
                            </div>
                        <? } else { ?>
                        <div class="b-info-box__fields-row b-fields-row">
                            <div class="b-fields-row__label">
                                <span class="g-label">Специалист не указал место работы</span>
                            </div>
                            <? } ?>
                        </div>
                        <div class="b-profile__all-inf g-v_a-top">
                            <div class="b-steps">
                                <div class="b-steps__select-time">
                                    <span class="b-steps__select-time__title">Выберите дату и время посещения специалиста</span>
                                    <div class="b-steps__select-time__box">
                                        <span class="b-steps__select-time_ttl">Дата</span>
                                        <div class="b-steps__select-time__select js-select-date_week"></div>
                                    </div>
                                    <div id="entry_time_choose" class="b-steps__select-time__list">
                                        <ul class="b-time js-time-get">
                                            <? foreach ($times as $time) { ?>
                                                <li class="b-time__i">
                                                    <span class="b-time__i-i"><?=date("H:i", $time)?></span>
                                                </li>
                                            <? }?>
                                        </ul>
                                    </div>
                                    <form id="js-time-get" class="g-dnone">
                                        <input type="text" name="date" value="0">
                                        <input type="text" name="time" value="0">
                                    </form>
                                </div>
                            </div>
                            <div class="b-nav-profile">'
                                <? if (!empty($user_id)) { ?>
                                    <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_3_<?=$item['ID']?>" value="Записаться на прием" title="Записаться на приём">
                                <? } else {?>
                                    <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_2_<?=$item['ID']?>" value="Записаться на прием" title="Записаться на приём">
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- 2-й шаг ПОПАП - записи к врачу - АВТОРИЗАЦИЯ -->
                <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_2_<?=$item['ID']?>">
                    <h2 class="h2 pUps__name">Запись к специалисту</h2>
                    <div class="b-profile">
                        <div class="b-profile__short-inf b-info-box">
                            <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                            <span class="b-info-box__type"><?=$item['LINKED_SPECIALIZATION']?></span>
                            <div class="b-info-box__foto">
                                <? if (!empty($item['PHOTO'])) { ?>
                                    <img src="<?=$item['PHOTO']?>">
                                <? } ?>
                            </div>
                            <? if (!empty($work)) {?>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Клиника: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                        <a class="g-link" href="#"><?=$work['NAME']?></a>
                                    </div>
                                </div>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Адрес: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                        <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                    </div>
                                </div>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Метро: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                    </div>
                                </div>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Телефон: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                        <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                    </div>
                                </div>
                            <? } else { ?>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Специалист не указал место работы</span>
                                </div>
                                <? } ?>
                            </div>
                            <div class="b-profile__all-inf">
                                <div class="b-steps">
                                    <div class="b-steps__not-auth">
                                        Для записи на прием к специалисту необходимо <a class="g-link" href="/profile/auth?back_url=<?=$_SERVER['REQUEST_URI']?>"><br>войти</a> или <a class="g-link" href="/profile/registration?back_url=<?=$_SERVER['HTTP_REFERER']?>">зарегистрироваться</a>
                                    </div>
                                </div>
                                <div class="b-nav-profile">
                                    <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_3_<?=$item['ID']?>" value="< Назад" title="Назад">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 3-й шаг ПОПАП - записи к врачу - ПОДТВЕРЖДЕНИЕ ИНФОРМАЦИИ  -->
                    <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_3_<?=$item['ID']?>">
                        <h2 class="h2 pUps__name">Запись к специалисту</h2>
                        <div class="b-profile">
                            <div class="b-profile__short-inf b-info-box">
                                <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                                <span class="b-info-box__type"><?=$item['LINKED_SPECIALIZATION']?></span>
                                <div class="b-info-box__foto">
                                    <? if (!empty($item['PHOTO'])) { ?>
                                        <img src="<?=$item['PHOTO']?>">
                                    <? } ?>
                                </div>
                                <? if (!empty($work)) {?>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Клиника: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                            <a class="g-link" href="#"><?=$work['NAME']?></a>
                                        </div>
                                    </div>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Адрес: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                            <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                        </div>
                                    </div>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Метро: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                        </div>
                                    </div>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Телефон: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                            <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                        </div>
                                    </div>
                                <? } else { ?>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Специалист не указал место работы</span>
                                    </div>
                                    <? } ?>
                                </div>
                                <div class="b-profile__all-inf g-v_a-top">
                                    <div class="b-steps">
                                        <div class="b-steps__your-data b-form-your-data">
                                            <span class="b-form-your-data__title">Пожалуйста, уточните Ваши контактные данные:</span>
                                            <form id="entry_form">
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Имя:
																			</span>
                                                    <input type="text" name="name" readonly placeholder="Иван Петров" value="<?=(!empty($user_data['LASTNAME']) ? $user_data['LASTNAME']." " : "").(!empty($user_data['FIRSTNAME']) ? $user_data['FIRSTNAME']." " : "").(!empty($user_data['MIDDLENAME']) ? $user_data['MIDDLENAME']." " : "")?>" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Телефон:
																			</span>
                                                    <input type="text" name="phone" readonly placeholder="+7" value="<?=$user_data['PHONE']?>" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Дата посещения специалиста:
																			</span>
                                                    <input id="entry_date" type="text" name="entry_date" readonly  placeholder="7 июля 2016" value="<?=$tommorow_foramted_date?>" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Время посещения:
																			</span>
                                                    <input id="entry_time" type="text" name="entry_time" readonly placeholder="9:15" value="9:00" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Ваше примечание:
																			</span>
																			<textarea type="text" name="comment" placeholder="9:15" class="b-form-your-data__field-row-input">
																			</textarea>
                                                </div>
                                                <input id="send_entry_form" type="submit" class="submit b-btn b-btn_uc b-form-your-data__submit js-pUp__openeer" data-pup-id="pUp-goDoc_4_<?=$item['ID']?>" value="Подтвердить и отправить" title="Подтвердить и отправить">
                                            </form>
                                        </div>
                                    </div>
                                    <div class="b-nav-profile">
                                        <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_1_<?=$item['ID']?>" value="< Назад" title="Назад">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 3-й шаг ПОПАП - записи к врачу - БЛАГОДАРНОСТЬ  -->
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_4_<?=$item['ID']?>">
                            <h2 class="h2 pUps__name">Запись к специалисту</h2>
                            <div class="b-profile">
                                <div class="b-profile__short-inf b-info-box">
                                    <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                                    <span class="b-info-box__type"><?=$item['LINKED_SPECIALIZATION']?></span>
                                    <div class="b-info-box__foto">
                                        <? if (!empty($item['PHOTO'])) { ?>
                                            <img src="<?=$item['PHOTO']?>">
                                        <? } ?>
                                    </div>
                                    <? if (!empty($work)) {?>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Клиника: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                                <a class="g-link" href="#"><?=$work['NAME']?></a>
                                            </div>
                                        </div>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Адрес: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                                <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                            </div>
                                        </div>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Метро: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                            </div>
                                        </div>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Телефон: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                                <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                            </div>
                                        </div>
                                    <? } else { ?>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Специалист не указал место работы</span>
                                        </div>
                                        <? } ?>
                                    </div>
                                    <div class="b-profile__all-inf">
                                        <div class="b-steps">
                                            <div class="b-steps__thx">
                                                <p class="b-steps__thx-txt">СПАСИБО!</p>
                                                <p>Специалист свяжется с Вами по вопросу записи на приём</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            <? } ?>

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/metro_script.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>
<script>
    function metro_wrapper() {
        isVisible = $( "#metro_wrapper" ).is( ":visible" );
        console.log(isVisible);
        if (isVisible) {
            $('body').css('overflow-y','hidden');
        } else {
            $('body').css('overflow-y','auto');
        }
    }
</script>

</body>
</html>