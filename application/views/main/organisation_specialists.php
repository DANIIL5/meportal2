<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Медицинские учреждения</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/jquery-ui.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script ></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Медицинские учреждения</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех медучреждениях вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>учреждений</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 867 220</b><br>клиник</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>

        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="/organisations/catalog?type_id=<?=$item["CATEGORY_ID"]?>"><?=$item["CATEGORY"]?></a></li>
                <? if (!empty($item["TYPE"])) {?><li><a href="/organisations/catalog?type_id=<?=$item["TYPE_ID"]?>"><?=$item["TYPE"]?></a></li><? } ?>
                <li><?=$item["BRAND"]?></li>
            </ul>

            <div class="two-cols two-cols_newMed search-result search-result-list hospital">
                <div class="left-col">
                    <div class="b-clinic_new">
                        <?=$organisation_card?>

                        <!--  блок с описанием  -->
                        <? if (!empty($item['DESCRIPTION'])) { ?>
                        <div class="b-clinic_new__descriptions">
                            <h3 class="g-NEW-title g-NEW-title_not-UpCase g-NEW-title_bord">Описание</h3>
                            <p class="g-NEW-inner">
                                <?=$item['DESCRIPTION']?>
                            </p>
                        </div>
                        <? } ?>
                        <!-- // блок с описанием  -->

                        <!-- блок с поиском -->
                        <div class="search-in-card search_form">
                            <div class="wrapper">
                                <div id="organisation_search" class="forma">
                                    <form action="/medicines/catalog" method="get" enctype="multipart/form-data">
                                        <var class="title georgia transform">поиск</var>
                                        <span class="for_input"><input id="organisation_search_query" attr-id="<?=$item['ID']?>" type="text" class="text" autocomplete="off" placeholder="например, аптека на Невском" name="query"></span>
                                        <input type="hidden" name="id" value="1">
                                        <input type="submit" class="submit btn" value="Найти" title="Найти">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- // блок с поиском-->

                        <!--  блок с таблицей_спецов  -->
                        <? if (!empty($specialists)) { ?>
                        <div class="b-clinic_new__table b-clinic_new__table_spec">
                            <h3 class="g-NEW-title g-NEW-title_not-UpCase">Специалисты</h3>
                            <table class="b-NEW-table">
                                <thead>
                                <tr>
                                    <td>
                                        Специальность
                                    </td>
                                    <td>
                                        Фото
                                    </td>
                                    <td>
                                        ФИО
                                    </td>
                                    <td>
                                        Адрес
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($specialists as $specialist) { ?>
                                    <tr>
                                        <td>
                                            <?=$specialist['LINKED_SPECIALIZATION']?>
                                        </td>
                                        <td>
                                            <img class="b-clinic_new__ava" src="<?=$specialist['PHOTO']?>" alt="photo">
                                        </td>
                                        <td>
                                            <a href="/doctors/item/<?=$specialist['ID']?>">
                                                <?=(!empty($specialist['SECOND_NAME']) ? $specialist['SECOND_NAME']." " : "").(!empty($specialist['FIRST_NAME']) ? $specialist['FIRST_NAME']." " : "").(!empty($specialist['LAST_NAME']) ? $specialist['LAST_NAME']." " : "")?>
                                            </a>
                                        </td>
                                        <td>
                                            <?=(!empty($specialist['ADDRESS_REGION']) ? $specialist['ADDRESS_REGION'].", " : "" ).(!empty($specialist['ADDRESS_CITY']) ? $specialist['ADDRESS_CITY'].", " : "" ).(!empty($specialist['ADDRESS_STREET']) ? $specialist['ADDRESS_STREET'].", " : "" ).(!empty($specialist['ADDRESS_HOUSE']) ? $specialist['ADDRESS_HOUSE'].", " : "" ).(!empty($specialist['ADDRESS_BUILDING']) ? $specialist['ADDRESS_BUILDING'].", " : "" ).(!empty($specialist['ADDRESS_OFFICE']) ? $specialist['ADDRESS_OFFICE'] : "" )?>
                                        </td>
                                        <td>
                                            <input class="submit b-btn b-btn_uc b-btn_countour g-width100 js-pUp__openeer" data-pup-id="pUp-goDoc_1" value="Записаться на прием" title="Записаться на прием" type="submit">
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
<!--                            <ul class="b-pagin">-->
<!--                                <li class="___active">1</li>-->
<!--                                <li>2</li>-->
<!--                                <li>3</li>-->
<!--                                <li>4</li>-->
<!--                                <li>5</li>-->
<!--                            </ul>-->
<!--                            <a class="g-link b-abs-view-all transform" href="/doctors/catalog">Все специалисты</a>-->
                        </div>
                        <? } ?>
                        <!--  // блок с таблицей_спецов  -->
                    </div>
                </div>

                <div class="right-col">
                    <ul class="list-instruction">
                        <li><a href="/organisations/item/<?=$item['ID']?>">Описание</a></li>
                        <li><a href="/organisations/specialists/<?=$item['ID']?>">Специалисты</a></li>
                        <li><a href="/organisations/branches/<?=$item['ID']?>">Отделения</a></li>
                        <li><a href="/organisations/articles/<?=$item['ID']?>">Публикации</a></li>
<!--                        <li><a href="/institutions/actions_list/--><?//=$item['ID']?><!--">Акции</a></li>-->
                        <li><a href="/organisations/photos/<?=$item['ID']?>">Фото</a></li>
                        <li><a href="/organisations/videos/<?=$item['ID']?>">Видео</a></li>
                        <li><a href="/organisations/feedbacks/<?=$item['ID']?>">Отзывы</a></li>
                    </ul>
                </div>

            </div>

        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['popular_institutions']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>

        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>


<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments" style="margin-left: -185px !important;">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <div class="pUps__item pUps__item_comments b-comments js-pUp_m__item" id="pUp-comment">
                <div class="b-comments__title">
                    ОТЗЫВ
                </div>
                <div class="b-comments__info">
                    Сообщение будет отправлено администратору сайта для формирования рейтинга специалистов.
                </div>
                <div class="b-comments__fields-box">
                    <form class="b-comments-f">
                        <input type="hidden" name="object" value="1">
                        <input type="hidden" name="id" value="<?=$id?>">
                        <ul class="b-rating">
                            <li class="b-rating__row">
                                <span class="b-rating__label">Дата визита</span>
                                <div class="b-input-date_ico js-date_box">
                                    <input class="b-rating__date js-date-i" id="b-rating__date" name="date" type="text"></p>
                                </div>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Место приема</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="place_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Цена/качество</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="price_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Компетенция специалиста</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="competence_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                            <li class="b-rating__row">
                                <span class="b-rating__label">Внимание специалиста</span>
                                <ul class="b-rating__star j-rating-star">
                                    <li class="g-dnone">
                                        <input class="g-dnone b-rating__input" type="text" name="care_rate" val="0">
                                    </li>
                                    <li class="b-rating__star-i">1</li>
                                    <li class="b-rating__star-i">2</li>
                                    <li class="b-rating__star-i">3</li>
                                    <li class="b-rating__star-i">4</li>
                                    <li class="b-rating__star-i">5</li>
                                </ul>
                            </li>
                        </ul>
                </div>
                <input type="submit" class="submit b-btn b-btn_uc g-width100 send_rating" data-pup-id="pUp-comment" value="Оставить отзыв" title="Оставить отзыв">
                </form>
            </div>
            
            <!-- END list popups -->
        </div>
    </div>
</div>
<!-- END - popUps -->

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>