<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Акции</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <a href="/profile/registration"><div class="btn">Портал для профессионалов</div></a>
            </div>

            <div class="slogan slider-head">
                <h1>Все аптеки города</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех аптеках вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>аптек</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 433 562</b><br/>препаратов</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">
        <div class="themes actions-list">
            <div class="container">
                <h2>Акции</h2>
                <div class="list actions">
                    <? foreach ($actions as $action) { ?>
                        <div class="item">
                            <div class="top">
                                <img src="<?=$action['THUMB']?>" alt="">
                                <p class="title" style="background: rgba(60,60,60,0.6);padding: 5px;box-sizing: border-box;"><strong><a href="/actions/item/<?=$action['ID']?>"><?=$action['NAME']?></a></strong></p>
                                <? if (!empty($action['DISCOUNT'])) { ?><span class="sale"><?=$action['DISCOUNT']?> <var><?=(!empty($action['DISCOUNT'])) ? "скидка" : ""?></var></span><? } ?>
                            </div>
                            <div class="bottom">
                                <p class="price"><?=$action['PRICE']?> <span class="value">Р</span></p>
                                <?=$action['DESCRIPTION']?>
                            </div>
                        </div>
                    <? } ?>
                </div>

                <?=$templates['voiting']?>
            </div>
        </div>


        <?=$templates['second_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>