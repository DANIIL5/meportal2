<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - ЗОЖ</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header" class="white-text">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Здоровый образ жизни</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о здоровом образе жизни</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>1 560</b><br/>фитнес-центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867</b><br/>спорт центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 320</b><br>салонов красоты и спа</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container green-icons">

            <div class="three_cols">
                <div class="left_col apteca_list institution_list">
                    <ul>
                        <li>
                            <a href="#"><h3>ФИТНЕС</h3></a>
                            <i class="icon list"></i>
                            <span>Отсортированный список мед. <br/>учреждений вашего города</span>
                        </li>
                        <li>
                            <a href="#"><h3>ПРОФЕССИОНАЛЬНЫЙ СПОРТ</h3></a>
                            <i class="icon list"></i>
                            <span>Отсортированный список мед. <br/>учреждений вашего города</span>
                        </li>
                        <li>
                            <a href="#"><h3>СПОРТСЕКЦИИ</h3></a>
                            <i class="icon paper"></i>
                            <span>Рейтинг частных клиник</span>
                        </li>
                        <li>
                            <a href="#"><h3>САЛОНЫ КРАСОТЫ и сПА</h3></a>
                            <i class="icon paper"></i>
                            <span>Рейтинг частных клиник</span>
                        </li>
                        <li>
                            <a href="#"><h3>ТРЕНЕРЫ</h3></a>
                            <i class="icon guide"></i>
                            <span>Бюджетные заведения</span>
                        </li>
                        <li>
                            <a href="#"><h3>ТОВАРЫ для здоровья</h3></a>
                            <i class="icon guide"></i>
                            <span>Бюджетные заведения</span>
                        </li>
                        <li>
                            <a href="#"><h3>ПИТАНИЕ</h3></a>
                            <i class="icon producer"></i>
                            <span>Дополнительная информация при обращении в медучреждение</span>
                        </li>
                        <li>
                            <a href="#"><h3>ИНТЕРНЕТ-МАГАЗИНЫ</h3></a>
                            <i class="icon producer"></i>
                            <span>Дополнительная информация при обращении в медучреждение</span>
                        </li>
                    </ul>
                </div>


                <?=$templates['ask_question']?>


        </div>

        <div class="popular-green">
            <div class="container_row">
                <h2>Популярные направления</h2>
                <ul class="list">
                    <li>
                        <span class="image"><img src="/images/popular-icon-1.jpg" alt=""></span>
                        <a href="#">Теннис</a>
                    </li>
                    <li>
                        <span class="image"><img src="/images/popular-icon-2.jpg" alt=""></span>
                        <a href="#">Футбол</a>
                    </li>
                    <li>
                        <span class="image"><img src="/images/popular-icon-3.jpg" alt=""></span>
                        <a href="#">Баскетбол</a>
                    </li>
                    <li>
                        <span class="image"><img src="/images/popular-icon-4.jpg" alt=""></span>
                        <a href="#">Гольф</a>
                    </li>
                    <li>
                        <span class="image"><img src="/images/popular-icon-1.jpg" alt=""></span>
                        <a href="#">Теннис</a>
                    </li>
                    <li>
                        <span class="image"><img src="/images/popular-icon-2.jpg" alt=""></span>
                        <a href="#">Футбол</a>
                    </li>
                    <li>
                        <span class="image"><img src="/images/popular-icon-3.jpg" alt=""></span>
                        <a href="#">Баскетбол</a>
                    </li>
                    <li>
                        <span class="image"><img src="/images/popular-icon-4.jpg" alt=""></span>
                        <a href="#">Гольф</a>
                    </li>
                </ul>
            </div>
        </div>

            <?=$templates['logos']?>

            <?=$templates['map_search']?>

            <?=$templates['first_banners']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>

        <div class="grey-block">

            <?=$templates['first_banners']?>

            <?=$templates['news']?>

        </div>


    </div>



    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

    <!--  popUps  -->
    <div class="pUps js-pUp_m">
        <div class="pUps__bg js-pUp_m__bg"></div>
        <div class="pUps__w pUps__w_comments ">
            <span class="pUps__x js-pUp_m__x"></span>
            <div class="pUps__reg js-pUp_m__reg g-v_a-top">
                <!-- list popups of ID -->

                <!-- END list popups -->
            </div>
        </div>
    </div>

    <div class="b-ALERT js-alert">
        <div class="b-ALERT__ov"></div>
        <div class="b-ALERT__box">
            <div class="b-ALERT__x"></div>
            <div class="b-ALERT__inner">
                Содержимое алерта
            </div>
        </div>
    </div>
    <!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
    <script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>