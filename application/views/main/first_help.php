<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Первая помощь</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="/http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="#"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 300-32-32</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted">Регистратура</div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Первая помощь</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех медучреждениях вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>учреждений</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 867 220</b><br>клиник</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Аптеки</a></li>
                <li>Ригла</li>
            </ul>

            <div class="two-cols search-result search-result-list hospital">
                <div class="left-col">
                    <div class="b-first-help">
                        <div class="b-first-help__i">
                            <div class="b-INFO-reg">
                                <h2>Первая помощь в городе Москва</h2>
                                <ul class="b-INFO-list">
                                    <li class="b-INFO-list__i b-INFO-list__i_image">
                                        <img src="/images/img_ambula.png" alt="">
                                    </li>
                                    <li class="b-INFO-list__i b-INFO-list__i_title">
                                        <span>Телефоны первой помощи</span>
                                    </li>
                                    <li class="b-INFO-list__i b-INFO-list__i_info b-info-tel">
											<span class="b-info-tel__ttl">
												Первый телефон первой помощи
											</span>
                                        <span class="b-info-tel__tel">8 (3822) 75-38-17</span>
                                    </li>
                                    <li class="b-INFO-list__i b-INFO-list__i_info b-INFO-list__i_info_grey">
											<span class="b-info-tel__ttl">
												Второй телефон первой помощи
											</span>
                                        <span class="b-info-tel__tel">8 (3822) 75-38-17</span>
                                    </li>
                                    <li class="b-INFO-list__i b-INFO-list__i_info">
											<span class="b-info-tel__ttl">
												Третий телефон первой помощи
											</span>
                                        <span class="b-info-tel__tel">8 (3822) 75-38-17</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="b-first-help__i">
                            <h2>Неотложные состояния</h2>
                            <div class="theme theme_FH">
                                <div class="columns">
                                    <? if (empty($list)) { ?>
                                        Нет статей
                                    <? } else {
                                        foreach ($list as $category => $articles) { ?>
                                            <div class="column">
                                                <h4 class="title"><?=$category?></h4>
                                                <ul class="list">
                                                    <? foreach ($articles as $article) { ?>
                                                        <li><a href="/articles/item/<?=$article['ID']?>"><?=$article['NAME']?></a></li>
                                                    <? } ?>
                                                </ul>
                                            </div>
                                        <? }
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <div class="b-first-help__i">

                            <div class="top clearfix">
                                <h2 class="top__ttl">Места экстренной помощи</h2>
<!--                                <div class="buttons see-also">-->
<!--                                    <span class="title size12">Так же найдено</span>-->
<!--                                    <span class="see-also-link"><a href="#" class="transform">справочники</a> (12)</span>-->
<!--                                    <span class="see-also-link"><a href="#" class="transform">Врачи</a> (2)</span>-->
<!--                                </div>-->
                            </div>
                           
                            <div class="top clearfix">
                                <h1>Мед. Учреждения</h1>
                            </div>
                             
                            <div class="b-doctor-list">
                            <? if (!empty($branches)): ?>
                                <? foreach ($branches as $id => $branch) { ?>
                                    <div class="no-active">
                                        <table class="results-table">
                                            <tr>
                                                <td class="name"><a href="/institutions/branches/<?=$id?>" class="dotted"><?=$branch['NAME']?></a></td>
                                                <td class="distance">~ 1 час</td>
                                                <td class="on-map"><a href="#" class="link">На карте</a></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="list-detail">
                                                    <div class="top clearfix">
                                                        <h1>Врачи</h1>
                                                    </div>
                                                    <div class="doctors-list">
                                                        <? if (empty($branch['DOCTORS'])) {?>
                                                            Нет списка специалистов данного учреждения
                                                        <? } else {
                                                                foreach ($branch['DOCTORS'] as $doctor) { ?>
                                                            <div class="item">
                                                                <div class="left-block">
                                                                    <span class="ava"><img src="/temp/16.jpg" alt=""></span>
                                                                    <p class="name"><a href="#">Иванов В. И.</a></p>
                                                                    <p class="title">Терапевт</p>
                                                                    <p class="price">2 500 Р</p>
                                                                    <span class="price-note">Стоимость приема</span>
                                                                    <a href="#" class="btn transform">Записаться на прием</a>
                                                                </div>

                                                                <div class="right-block">
                                                                    <div class="about">
                                                                        Инфе́кция&nbsp;— опасность заражения живых организмов микроорганизмами (бактериями, грибами, простейшими), а также вирусами, прионами, риккетсиями, микоплазмами, протеями, вирбрионами, паразитами, насекомыми и членистоногими(очень редко). Термин означает различные виды взаимодействия чужеродных микроорганизмов.
                                                                    </div>

                                                                    <table class="contacts">
                                                                        <tbody><tr>
                                                                            <td><span class="label">Адрес:</span></td>
                                                                            <td colspan="3">г. Москва, Хамовники район, ул. Большая Пироговская, д. 6, стр. 1</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span class="label">Метро:</span></td>
                                                                            <td colspan="3"><span class="metro">Спортивная</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span class="label">Телефон:</span></td>
                                                                            <td>459 132-98-98 <br>
                                                                                495 123-32-23</td>
                                                                            <td><span class="label">График работы:</span></td>
                                                                            <td>9.00 - 23.00 пн-пт<br>
                                                                                9.00 - 18.00 сб-вс </td>
                                                                        </tr>
                                                                        </tbody></table>

                                                                    <ul class="links transform size10">
                                                                        <li><a href="#">График</a></li>
                                                                        <li><a href="#">Рейтинг</a></li>
                                                                        <li><a href="#">Отзывы</a></li>
                                                                        <li><a href="#">Публикации</a></li>
                                                                        <li><a href="#">Награды</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        <? }
                                                        } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                <? } ?>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right-col">
                    <h2>Уточнить поиск</h2>
                    <div class="search-block-red search-place">
                        <form action="" method="get">
                            <div class="b-filter-search-i">
                                <p class="title inlineBlock red">Найти информацию о состоянии:</p>
                                <select name="category_id">
                                    <option value="">Выберите</option>
                                    <? foreach ($categories as $category) { ?>
                                        <option value="<?=$category['ID']?>"><?=$category['NAME']?></option>
                                    <? } ?>
                                </select>
                                <input type="submit" class="submit btn btn_right-filter js-search-btn" value="Применить" title="Применить">
                                <a href="/first_help/catalog" class="reset red reset_not-abs">Сбросить</a>
                            </div>
                            <hr/>

                            <p class="title inlineBlock red">Найти учреждение, врача, услугу</p>

                            <div class="b-filter-search-i">
                                <p class="title inlineBlock red">Местоположение</p>
                                <a href="/first_help/catalog" class="reset red">Сбросить все</a>
                                <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>
                                <p><i class="icon metro"></i><a href="#" class="dotted">Указать станцию метро</a></p>


                                <p class="title inlineBlock red">Специалист</p>
                                <select>
                                    <option value="">Выберите</option>
                                    <? foreach ($doctor_types as $doctor_type) { ?>
                                        <option value="<?=$doctor_type['ID']?>"><?=$doctor_type['NAME']?></option>
                                    <? } ?>
                                </select>

                                <p class="title inlineBlock red">Услуга</p>
                                <select>
                                    <option>Обследование</option>
                                    <option>Обследование 1</option>
                                    <option>Обследование 2</option>
                                    <option>Обследование 3</option>
                                    <option>Обследование 4</option>
                                </select>

                                <p class="title inlineBlock red">Анализы</p>
                                <select>
                                    <option>Анализы</option>
                                    <option>Анализы 1</option>
                                    <option>Анализы 2</option>
                                    <option>Анализы 3</option>
                                    <option>Анализы 4</option>
                                </select>
                            </div>

                            <hr/>

                            <div class="b-filter-search-i">
                                <ul class="list-two">
                                    <li><input type="checkbox" class="checkbox" checked="checked" id="input_3"/><label for="input_3">Круглосуточно</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_5"/><label for="input_5">Скорая помощь</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_5"/><label for="input_5">Прием на дому</label></li>
                                    <li><input type="checkbox" class="checkbox" id="input_4"/><label for="input_4">Детское отделение</label></li>
                                </ul>
                                <input type="submit" class="submit btn btn_right-filter js-search-btn" value="Применить" title="Применить">
                                <a href="/first_help/catalog" class="reset red reset_not-abs">Сбросить</a>
                            </div>
                        </form>
                    </div>

                    <h2>Частые проблемы</h2>
                    <ul class="b-list-popular-link">
                        <li><a class="g-link b-btn_uc" href="#">Зубная боль</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Головная боль</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Боль в спине</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Боль в ногах</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Поясница</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Плечо</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Глаза</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Шея</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Ухо</a></li>
                        <li><a class="g-link b-btn_uc" href="#">Зубная боль</a></li>
                    </ul>
                </div>

            </div>


        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['first_banners']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
        </div>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>
