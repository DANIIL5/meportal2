<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Результат поиска</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Все аптеки города</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех аптеках вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>аптек</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 433 562</b><br/>препаратов</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Препараты</a></li>
                <li><a href="#">Лечение заболеваний ЖКТ и печени</a></li>
                <li><a href="#">Препараты для желудочно-кишечного тракта</a></li>
                <li>Фармацитрон</li>
            </ul>

            <div class="two-cols search-result-list">
                <div class="left-col">
                    <h1>Рассчет экономной покупки</h1>
                    <p class="size12 note">Выбранные лекарства</p>

                    <div id="eco_buy_list">
                        <div class="product-selected">
                            <h2>Фармацитрон</h2>
                            <div class="value">
                                <span class="minus arrow">&lt;</span>
                                <input type="text" class="text" value="1" size="5"/>
                                <span class="plus arrow">&gt;</span>
                            </div>
                            <a class="delete" href="#">Удалить</a>
                        </div>

                        <div class="product-selected">
                            <h2>Кларитромицин ретард -OBL</h2>
                            <div class="value">
                                <span class="minus arrow">&lt;</span>
                                <input type="text" class="text" value="1" size="5"/>
                                <span class="plus arrow">&gt;</span>
                            </div>
                            <a class="delete" href="#">Удалить</a>
                        </div>

                        <div class="product-selected" id="new_eco_buy_element">
                            <input type="text" placeholder="Начните вводить название, а затем выберите лекарство из списка">
                            <div class="value">
                                <span class="minus arrow">&lt;</span>
                                <input type="text" class="text" value="1" size="5"/>
                                <span class="plus arrow">&gt;</span>
                            </div>
                            <a class="delete" href="#">Удалить</a>
                        </div>

                        <div class="product-selected">
                            <h2>Курантил №25</h2>
                            <div class="value">
                                <span class="minus arrow">&lt;</span>
                                <input type="text" class="text" value="1" size="5"/>
                                <span class="plus arrow">&gt;</span>
                            </div>
                            <a class="delete" href="#">Удалить</a>
                        </div>
                    </div>

                    <div class="tables economical">
                    <!-- Тут будет вставка лекарств-->
                        <table class="results-table">
                            <tr>
                                <th class="name">Аптека</th>
                                <th class="distance">Удаленность</th>
                                <th class="price">Цена</th>
                                <th class="value">Количество</th>
                                <th class="on-map">На карте</th>
                            </tr>
                        </table>
                    </div>
                    <div>Добавьте лекарство, чтобы рассчитать экономную покупку</div>
                    <button id="add_to_economical" type="button" class="submit btn" style="float: right; font-size: 14px; margin-top: 0px; border: none; margin-bottom: 40px; cursor: pointer;" onclick="add_to_economical();">Добавить лекарство</button>
                </div>


                <div class="right-col">
                    <h2>УТОЧНИТЬ ПОИСК</h2>

                    <div class="search-block-red search-place">
                        <form action="#">
                            <p class="title inlineBlock red">Местоположение</p>
                            <a href="#" class="reset red">Сбросить все</a>
                            <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>
                            <p><i class="icon metro"></i><a href="#" class="dotted">Указать станцию метро</a></p>

                            <hr/>
                            <select>
                                <option>Все аптечные сети</option>
                                <option>Все аптечные сети 1</option>
                                <option>Все аптечные сети 2</option>
                                <option>Все аптечные сети 3</option>
                                <option>Все аптечные сети 4</option>
                            </select>
                            <hr/>

                            <ul class="list-two">
                                <li><input type="checkbox" class="checkbox" id="input_1"/><label for="input_1">С доставкой</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_2"/><label for="input_2">ДЛО</label></li>
                                <li><input type="checkbox" class="checkbox" checked="checked" id="input_3"/><label for="input_3">Бронирование</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_4"/><label for="input_4">ДМС</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_5"/><label for="input_5">Круглосуточно</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_6"/><label for="input_6">Рецептурно-производственный отдел</label></li>
                            </ul>
                        </form>
                    </div>


                    <div class="advice">
                        Получите <a href="#" class="red link">совет</a> по данному препарату
                        <div class="popup closed">
                            <form action="#">
                                <textarea cols="50" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно."></textarea>
                                <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>
                </div>




                <div class="clear"></div>
            </div>
        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="pUps js-pUp_m pUps_ALERT">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>