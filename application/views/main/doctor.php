<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Медицинские учреждения</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/jquery-ui.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Медицинские учреждения</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех медучреждениях вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>учреждений</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 867 220</b><br>клиник</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>

        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="#">Специалисты</a></li>
                <li><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></li>
            </ul>

            <div class="two-cols two-cols_newMed search-result search-result-list hospital">
                <div class="left-col">
                    <div class="b-clinic_new">
                        <div class="b-clinic_new__foto-or-logo">
                            <? if (!empty($item['PHOTO'])) { ?>
                                <img src="<?=$item['PHOTO']?>" alt="">
                            <? } else { ?>
                                <div style="width: 200px; height: 200px; background: lightgray; margin-bottom: 10px;"></div>
                            <? } ?>
                            <ul class="b-rating">
                                <li class="b-rating__row b-rating__row_img">
                                    <span class="b-rating__label">Рейтинг</span>
                                    <ul class="b-rating__star j-rating-star">
                                        <li class="g-dnone __active">
                                            <input class="g-dnone b-rating__input" name="field-1" val="0" type="text">
                                        </li>
                                        <li class="b-rating__star-i __active">1</li>
                                        <li class="b-rating__star-i">2</li>
                                        <li class="b-rating__star-i">3</li>
                                        <li class="b-rating__star-i">4</li>
                                        <li class="b-rating__star-i">5</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <!--  блок с инфо  -->
                        <div class="b-clinic_new__info">
                            <div class="b-info-clinic b-info-clinic_right-panel">
                                <div class="b-info-clinic__main">
                                    <h2 class="h2"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></h2>
                                    <span class="b-jur"><?=$item['LINKED_SPECIALIZATION']?></span>
                                    <table class="b-info-table">
                                        <tbody>
                                        <? if (!empty($item['ADDRESS_REGION']) || !empty($item['ADDRESS_CITY']) || !empty($item['ADDRESS_STREET']) || !empty($item['ADDRESS_HOUSE']) || !empty($item['ADDRESS_BUILDING']) || !empty($item['ADDRESS_OFFICE'])) { ?>
                                            <tr>
                                                <td> Адрес: </td>
                                                <td><?=(!empty($item['ADDRESS_REGION'])  ? $item['ADDRESS_REGION'].", " : "" ).(!empty($item['ADDRESS_CITY']) ? $item['ADDRESS_CITY'].", " : "" ).(!empty($item['ADDRESS_STREET']) ? $item['ADDRESS_STREET'].", " : "" ).(!empty($item['ADDRESS_HOUSE']) ? $item['ADDRESS_HOUSE'].", " : "" ).(!empty($item['ADDRESS_BUILDING']) ? $item['ADDRESS_BUILDING'].", " : "" ).(!empty($item['ADDRESS_OFFICE']) ? $item['ADDRESS_OFFICE'] : "" )?>  <a class="g-link go-map" href="">Показать на карте</a></td>
                                            </tr>
                                        <? } ?>
                                        <? if (!empty($metros)) { ?>
                                            <tr>
                                                <td> Метро: </td>
                                                <td><span class="metro">
                                                        <? foreach ($metros as $metro) { ?>
                                                            <?=$metro['NAME'].","?>
                                                        <? }?>
                                                    </span></td>
                                            </tr>
                                        <? } ?>
                                        <? if (!empty($item['WORK_PHONE']) || !empty($item['MOBILE_PHONE'])) {?>
                                            <tr>
                                                <td> Телефон: </td>
                                                <td><?=(!empty($item['WORK_PHONE']) ? $item['WORK_PHONE']."<br>" : "").(!empty($item['MOBILE_PHONE']) ? $item['MOBILE_PHONE']."<br>" : "")?></td>
                                            </tr>
                                        <? } ?>
                                        <? if (!empty($item['WEBSITES'])) { ?>
                                            <tr>
                                                <td> Веб-сайт: </td>
                                                <td><a class="g-link" href="<?=$item['WEBSITES']?>"><?=$item['WEBSITES']?></a></td>
                                            </tr>
                                        <? } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="b-info-clinics__RP">
<!--                                    <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_1" value="Записаться на прием" title="Найти">-->

                                    <a href="/profile/post?email=<?=$item['WORK_EMAIL']?>" class="submit b-btn b-btn_uc g-width100"  style="width:195px; margin-bottom: 20px;" title="Написать" type="submit">Написать</a>
                                    <input type="submit" class="submit b-btn b-btn_uc g-width100 js-pUp__openeer"  style="width:195px;" data-pup-id="pUp-goDoc_1"  value="Записаться на прием" title="Записаться на прием" type="submit">
                                    <input class="submit b-btn b-btn_uc g-width100 js-pUp__openeer" style="width:195px;" value="Оставить отзыв" data-pup-id="pUp-comment" title="Оставить отзыв" class="submit b-btn b-btn_uc g-width100" type="submit">
                                    <div class="b-btn-ico">
                                        <span class="b-btn-ico__ico"><img src="/images/rating-star_def.png" alt=""></span>
                                        <input id="add_to_favorites" attr-id="<?=$item['ID']?>" attr-type="specialists" value="Добавить в избранное" title="Добавить в избранное" class="submit b-btn b-btn_uc g-width100 b-btn__counter-gray-ico b-btn-ico__input" type="submit">
                                    </div>
                                </div>
                            </div>
                            <div class="work-time">
                                <h3>График, <small><?=$month?></small></h3>
                            </div>
                            <ul class="b-work-time">
                                <li class="b-work-time__i b-work-time-cell">
                                    <div class="b-work-time-cell__i">
                                        <?=$week['mon']?>, <span class="g-label">Пн</span><br>
                                        <? if (!empty($item['MONDAY_WORK_FROM']) && !empty($item['MONDAY_WORK_TO'])) {?><?=$item['MONDAY_WORK_FROM']?> - <?=$item['MONDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                                    </div>
                                    <? if (!empty($item['MONDAY_BREAK_FROM']) && !empty($item['MONDAY_BREAK_TO'])) {?>
                                        <div class="b-work-time-cell__i">
                                            <span class="g-label">Перерыв</span><br>
                                            <?=$item['MONDAY_BREAK_FROM']?> - <?=$item['MONDAY_BREAK_TO']?>
                                        </div>
                                    <? } ?>
                                </li>
                                <li class="b-work-time__i b-work-time-cell">
                                    <div class="b-work-time-cell__i">
                                        <?=$week['tue']?>, <span class="g-label">Вт</span><br>
                                        <? if (!empty($item['TUESDAY_WORK_FROM']) && !empty($item['TUESDAY_WORK_TO'])) {?><?=$item['TUESDAY_WORK_FROM']?> - <?=$item['TUESDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                                    </div>
                                    <? if (!empty($item['TUESDAY_BREAK_FROM']) && !empty($item['TUESDAY_BREAK_TO'])) {?>
                                        <div class="b-work-time-cell__i">
                                            <span class="g-label">Перерыв</span><br>
                                            <?=$item['TUESDAY_BREAK_FROM']?> - <?=$item['TUESDAY_BREAK_TO']?>
                                        </div>
                                    <? } ?>
                                </li>
                                <li class="b-work-time__i b-work-time-cell">
                                    <div class="b-work-time-cell__i">
                                        <?=$week['wed']?>, <span class="g-label">Ср</span><br>
                                        <? if (!empty($item['WEDNESDAY_WORK_FROM']) && !empty($item['WEDNESDAY_WORK_TO'])) {?><?=$item['WEDNESDAY_WORK_FROM']?> - <?=$item['WEDNESDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                                    </div>
                                    <? if (!empty($item['WEDNESDAY_BREAK_FROM']) && !empty($item['WEDNESDAY_BREAK_TO'])) {?>
                                        <div class="b-work-time-cell__i">
                                            <span class="g-label">Перерыв</span><br>
                                            <?=$item['WEDNESDAY_BREAK_FROM']?> - <?=$item['WEDNESDAY_BREAK_TO']?>
                                        </div>
                                    <? } ?>
                                </li>
                                <li class="b-work-time__i b-work-time-cell">
                                    <div class="b-work-time-cell__i">
                                        <?=$week['thu']?>, <span class="g-label">Чт</span><br>
                                        <? if (!empty($item['THURSDAY_WORK_FROM']) && !empty($item['THURSDAY_WORK_TO'])) {?><?=$item['THURSDAY_WORK_FROM']?> - <?=$item['THURSDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                                    </div>
                                    <? if (!empty($item['THURSDAY_BREAK_FROM']) && !empty($item['THURSDAY_BREAK_TO'])) {?>
                                        <div class="b-work-time-cell__i">
                                            <span class="g-label">Перерыв</span><br>
                                            <?=$item['THURSDAY_BREAK_FROM']?> - <?=$item['THURSDAY_BREAK_TO']?>
                                        </div>
                                    <? } ?>
                                </li>
                                <li class="b-work-time__i b-work-time-cell">
                                    <div class="b-work-time-cell__i">
                                        <?=$week['fri']?>, <span class="g-label">Пт</span><br>
                                        <? if (!empty($item['FRIDAY_WORK_FROM']) && !empty($item['FRIDAY_WORK_TO'])) {?><?=$item['FRIDAY_WORK_FROM']?> - <?=$item['FRIDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                                    </div>
                                    <? if (!empty($item['FRIDAY_BREAK_FROM']) && !empty($item['FRIDAY_BREAK_TO'])) {?>
                                        <div class="b-work-time-cell__i">
                                            <span class="g-label">Перерыв</span><br>
                                            <?=$item['FRIDAY_BREAK_FROM']?> - <?=$item['FRIDAY_BREAK_TO']?>
                                        </div>
                                    <? } ?>
                                </li>
                                <li class="b-work-time__i b-work-time-cell">
                                    <div class="b-work-time-cell__i">
                                        <?=$week['sat']?>, <span class="g-label">Сб</span><br>
                                        <? if (!empty($item['SATURDAY_WORK_FROM']) && !empty($item['SATURDAY_WORK_TO'])) {?><?=$item['SATURDAY_WORK_FROM']?> - <?=$item['SATURDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                                    </div>
                                    <? if (!empty($item['SATURDAY_BREAK_FROM']) && !empty($item['SATURDAY_BREAK_TO'])) {?>
                                        <div class="b-work-time-cell__i">
                                            <span class="g-label">Перерыв</span><br>
                                            <?=$item['SATURDAY_BREAK_FROM']?> - <?=$item['SATURDAY_BREAK_TO']?>
                                        </div>
                                    <? } ?>
                                </li>
                                <li class="b-work-time__i b-work-time-cell">
                                    <div class="b-work-time-cell__i">
                                        <?=$week['sun']?>, <span class="g-label">Вс</span><br>
                                        <? if (!empty($item['SUNDAY_WORK_FROM']) && !empty($item['SUNDAY_WORK_TO'])) {?><?=$item['SUNDAY_WORK_FROM']?> - <?=$item['SUNDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                                    </div>
                                    <? if (!empty($item['SUNDAY_BREAK_FROM']) && !empty($item['SUNDAY_BREAK_TO'])) {?>
                                        <div class="b-work-time-cell__i">
                                            <span class="g-label">Перерыв</span><br>
                                            <?=$item['SUNDAY_BREAK_FROM']?> - <?=$item['SUNDAY_BREAK_TO']?>
                                        </div>
                                    <? } ?>
                                </li>
                            </ul>
                        </div>
                        <!-- // блок с инфо  -->

                        <!--  блок с описанием  -->
                        <? if (!empty($item['DESCRIPTION'])) { ?>
                        <div class="b-clinic_new__descriptions">
                            <h3 class="g-NEW-title g-NEW-title_not-UpCase g-NEW-title_bord">Описание</h3>
                            <p class="g-NEW-inner">
                                <?=$item['DESCRIPTION']?>
                            </p>
                        </div>
                        <? } ?>
                        <!-- // блок с описанием  -->

                        <!-- блок с поиском -->
                        <div class="search-in-card search_form">
                            <div class="wrapper">
                                <div id="organisation_search" class="forma">
                                    <form action="/medicines/catalog" method="get" enctype="multipart/form-data">
                                        <var class="title georgia transform">поиск</var>
                                        <span class="for_input"><input id="organisation_search_query" attr-id="1" type="text" class="text" placeholder="например, аптека на Невском" name="query"></span>
                                        <input type="hidden" name="id" value="1">
                                        <input type="submit" class="submit btn" value="Найти" title="Найти">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- // блок с поиском-->

                        <!--  блок с публикациями  -->
                        <? if (!empty($articles)) { ?>
                        <div class="b-clinic_new__publication">
                            <h3 class="g-NEW-title g-NEW-title_not-UpCase g-NEW-title_bord">Публикации</h3>
                            <div class="g-NEW-inner">
                                <ul class="b-publication">
                                    <? foreach ($articles as $article) { ?>
                                        <li class="b-publication__i">
                                            <h4 class="g-NEW-title g-NEW-title_bord"><?=$article['NAME']?></h4>
                                            <div class="b-publication__text">
                                                <? if (strlen($article['TEXT'] > 297)) { ?>
                                                    <?=substr($article['TEXT'], 0, 300)."..."?>
                                                <? } else { ?>
                                                    <?=$article['TEXT']?>
                                                <? } ?>
                                            </div>
                                            <!--                                            <span class="b-publication__date">12 июля</span>-->
                                            <div class="b-publication__read-More"><a class="g-link" href="/articles/item/<?=$article['ID']?>">Читать полностью</a></div>
                                        </li>
                                    <? } ?>
                                </ul>
                            </div>
                            <div class="b-hr_new-clinic b-hr_new-clinic_link b-clinic_new__publication-hr">
                                <hr>
                                <a class="g-link b-hr_new-clinic_link__href transform" href="/articles/catalog">Все публикации</a>
                            </div>
                        </div>
                        <? } ?>
                        <!--  // блок с публикациями  -->

                        <!-- БЛОК - СТРАНИЦА С ФОТО  -->
                        <? if (!empty($photos)) { ?>
                            <div class="b-clinic_new__all-photo">
                                <h2 class="h2">фото</h2>
                                <div class="b-prew-photo">
                                    <? foreach ($photos as $photo) { ?>
                                        <a class="js-fb-bgalery b-prew-photo__i" rel="clinic_photo" href="<?=$photo['VALUE']?>" style="text-align:center;"><img src="/images/licenses/<?=$photo['VALUE']?>" alt=""></a>
                                    <? }?>
                                </div>
                            </div>
                        <? } ?>
                        <!-- БЛОК - СТРАНИЦА С ФОТО  -->

                        <!-- БЛОК - СТРАНИЦА С ВИДЕО  -->
                        <? if (!empty($first_video)) { ?>
                        <div class="b-clinic_new__all-video">
                            <h2 class="h3">видео</h2>
                            <div class="b-videoPlayer">
                                <div id="b-YT_video"></div>
                                <!-- <span class="b-videoPlayer__play js-viewPlay"></span> -->
                                <!-- <img class="b-videoPlayer__prev-img" src="temp/video-prev.jpg"> -->
                            </div>

<!--                            <div class="b-clinic_new__descriptions">-->
<!--                                <h3 class="g-NEW-title g-NEW-title_not-UpCase g-NEW-title_bord">Мишка-продавец - новая одержимость</h3>-->
<!--                                <p class="g-NEW-inner">-->
<!--                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit-->
<!--                                </p>-->
<!--                            </div>-->

                            <? if (!empty($first_video) && !empty($videos)) { ?>
                            <div class="b-prew-video">
                                <h3 class="g-NEW-title g-NEW-title_not-UpCase g-NEW-title_bord">Все видео</h3>
                                <div class="b-prew-video__list">
                                    <ul class="b-video__list">
                                        <? foreach ($videos as $video) { ?>
                                            <li class="b-video__list__i">
                                                <iframe width="274" height="148" src="<?=$video['VALUE']?>" frameborder="0" allowfullscreen></iframe>
                                            </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </div>
                            <? } ?>
                        </div>
                        <? } ?>
                        <!-- БЛОК - СТРАНИЦА С видео  -->
                    </div>
                </div>

                <div class="right-col">
                    <ul class="list-instruction"> 
                        <li><a href="#" >описание</a></li>
                        <li><a href="#">прайс-лист</a></li>
                        <li><a href="#">акции</a></li>
                         <li><a href="/doctors/publications/<?=$item['ID']?>">Публикации</a></li>
                        <li><a href="/doctors/photos/<?=$item['ID']?>">Фото</a></li>
                        <li><a href="/doctors/videos/<?=$item['ID']?>">Видео</a></li>
                         <li><a href="/doctors/opinions/<?=$item['ID']?>/<?=$item['USER_ID']?>">Отзывы</a></li>
                    </ul>
                </div>

            </div>

        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['popular_institutions']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>

        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg">
            <!-- list popups of ID -->

                <div class="pUps__item pUps__item_comments b-comments js-pUp_m__item" id="pUp-comment">
                    <div class="b-comments__title">
                        ОТЗЫВ
                    </div>
                    <div class="b-comments__info">
                        Сообщение будет отправлено администратору сайта для формирования рейтинга специалистов.
                    </div>
                    <div class="b-comments__fields-box">
                        <form class="b-comments-f">
                            <input type="hidden" name="object" value="3">
                            <input type="hidden" name="id" value="<?=$item['USER_ID']?>">
                            <ul class="b-rating">
                                <li class="b-rating__row">
                                    <span class="b-rating__label">Дата визита</span>
                                    <div class="b-input-date_ico js-date_box">
                                        <input class="b-rating__date js-date-i" id="b-rating__date" name="date" type="text"></p>
                                    </div>
                                </li>
                                <li class="b-rating__row">
                                    <span class="b-rating__label">Место приема</span>
                                    <ul class="b-rating__star j-rating-star">
                                        <li class="g-dnone">
                                            <input class="g-dnone b-rating__input" type="text" name="place_rate" val="0">
                                        </li>
                                        <li class="b-rating__star-i">1</li>
                                        <li class="b-rating__star-i">2</li>
                                        <li class="b-rating__star-i">3</li>
                                        <li class="b-rating__star-i">4</li>
                                        <li class="b-rating__star-i">5</li>
                                    </ul>
                                </li>
                                <li class="b-rating__row">
                                    <span class="b-rating__label">Цена/качество</span>
                                    <ul class="b-rating__star j-rating-star">
                                        <li class="g-dnone">
                                            <input class="g-dnone b-rating__input" type="text" name="price_rate" val="0">
                                        </li>
                                        <li class="b-rating__star-i">1</li>
                                        <li class="b-rating__star-i">2</li>
                                        <li class="b-rating__star-i">3</li>
                                        <li class="b-rating__star-i">4</li>
                                        <li class="b-rating__star-i">5</li>
                                    </ul>
                                </li>
                                <li class="b-rating__row">
                                    <span class="b-rating__label">Компетенция специалиста</span>
                                    <ul class="b-rating__star j-rating-star">
                                        <li class="g-dnone">
                                            <input class="g-dnone b-rating__input" type="text" name="competence_rate" val="0">
                                        </li>
                                        <li class="b-rating__star-i">1</li>
                                        <li class="b-rating__star-i">2</li>
                                        <li class="b-rating__star-i">3</li>
                                        <li class="b-rating__star-i">4</li>
                                        <li class="b-rating__star-i">5</li>
                                    </ul>
                                </li>
                                <li class="b-rating__row">
                                    <span class="b-rating__label">Внимание специалиста</span>
                                    <ul class="b-rating__star j-rating-star">
                                        <li class="g-dnone">
                                            <input class="g-dnone b-rating__input" type="text" name="care_rate" val="0">
                                        </li>
                                        <li class="b-rating__star-i">1</li>
                                        <li class="b-rating__star-i">2</li>
                                        <li class="b-rating__star-i">3</li>
                                        <li class="b-rating__star-i">4</li>
                                        <li class="b-rating__star-i">5</li>
                                    </ul>
                                </li>
                            </ul>
                    </div>
                    <input type="submit" class="submit b-btn b-btn_uc g-width100 send_rating" data-pup-id="pUp-comment" value="Оставить отзыв" title="Оставить отзыв">
                    </form>
                </div>

            <!-- 1-й шаг ПОПАП - записи к врачу - ВЫБОР ДАТЫ-->
            <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_1">
                <h2 class="h2 pUps__name">Запись к специалисту</h2>
                <div class="b-profile">
                    <div class="b-profile__short-inf b-info-box">
                        <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                        <span class="b-info-box__type"><?=$item['LINKED_SPECIALIZATION']?></span>
                        <div class="b-info-box__foto">
                            <? if (!empty($item['PHOTO'])) { ?>
                                <img src="<?=$item['PHOTO']?>">
                            <? } ?>
                        </div>
                        <? if (!empty($work)) {?>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Клиника: </span>
                                </div>
                                <div class="b-fields-row__content">
                                    <a class="g-link" href="#"><?=$work['NAME']?></a>
                                </div>
                            </div>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Адрес: </span>
                                </div>
                                <div class="b-fields-row__content">
                                    <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                </div>
                            </div>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Метро: </span>
                                </div>
                                <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                </div>
                            </div>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Телефон: </span>
                                </div>
                                <div class="b-fields-row__content">
                                    <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                </div>
                            </div>
                        <? } else { ?>
                        <div class="b-info-box__fields-row b-fields-row">
                            <div class="b-fields-row__label">
                                <span class="g-label">Специалист не указал место работы</span>
                            </div>
                            <? } ?>
                        </div>
                        <div class="b-profile__all-inf g-v_a-top">
                            <div class="b-steps">
                                <div class="b-steps__select-time">
                                    <span class="b-steps__select-time__title">Выберите дату и время посещения специалиста</span>
                                    <div class="b-steps__select-time__box">
                                        <span class="b-steps__select-time_ttl">Дата</span>
                                        <div class="b-steps__select-time__select js-select-date_week"></div>
                                    </div>
                                    <div id="entry_time_choose" class="b-steps__select-time__list">
                                        <ul class="b-time js-time-get">
                                            <? foreach ($times as $time) { ?>
                                                <li class="b-time__i">
                                                    <span class="b-time__i-i"><?=date("H:i", $time)?></span>
                                                </li>
                                            <? }?>
                                        </ul>
                                    </div>
                                    <form id="js-time-get" class="g-dnone">
                                        <input type="text" name="date" value="0">
                                        <input type="text" name="time" value="0">
                                    </form>
                                </div>
                            </div>
                            <div class="b-nav-profile">'
                                <? if (!empty($user_id)) { ?>
                                    <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_3" value="Записаться на прием" title="Записаться на приём">
                                <? } else {?>
                                    <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_2" value="Записаться на прием" title="Записаться на приём">
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 2-й шаг ПОПАП - записи к врачу - АВТОРИЗАЦИЯ -->
                <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_2">
                    <h2 class="h2 pUps__name">Запись к специалисту</h2>
                    <div class="b-profile">
                        <div class="b-profile__short-inf b-info-box">
                            <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                            <span class="b-info-box__type"><?=$item['LINKED_SPECIALIZATION']?></span>
                            <div class="b-info-box__foto">
                                <? if (!empty($item['PHOTO'])) { ?>
                                    <img src="<?=$item['PHOTO']?>">
                                <? } ?>
                            </div>
                            <? if (!empty($work)) {?>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Клиника: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                        <a class="g-link" href="#"><?=$work['NAME']?></a>
                                    </div>
                                </div>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Адрес: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                        <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                    </div>
                                </div>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Метро: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                    </div>
                                </div>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Телефон: </span>
                                    </div>
                                    <div class="b-fields-row__content">
                                        <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                    </div>
                                </div>
                            <? } else { ?>
                            <div class="b-info-box__fields-row b-fields-row">
                                <div class="b-fields-row__label">
                                    <span class="g-label">Специалист не указал место работы</span>
                                </div>
                                <? } ?>
                            </div>
                            <div class="b-profile__all-inf">
                                <div class="b-steps">
                                    <div class="b-steps__not-auth">
                                        Для записи на прием к специалисту необходимо <a class="g-link" href="/profile/auth?back_url=<?=$_SERVER['REQUEST_URI']?>"><br>войти</a> или <a class="g-link" href="/profile/registration?back_url=<?=$_SERVER['HTTP_REFERER']?>">зарегистрироваться</a>
                                    </div>
                                </div>
                                <div class="b-nav-profile">
                                    <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_3" value="< Назад" title="Назад">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 3-й шаг ПОПАП - записи к врачу - ПОДТВЕРЖДЕНИЕ ИНФОРМАЦИИ  -->
                    <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_3">
                        <h2 class="h2 pUps__name">Запись к специалисту</h2>
                        <div class="b-profile">
                            <div class="b-profile__short-inf b-info-box">
                                <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                                <span class="b-info-box__type"><?=$item['LINKED_SPECIALIZATION']?></span>
                                <div class="b-info-box__foto">
                                    <? if (!empty($item['PHOTO'])) { ?>
                                        <img src="<?=$item['PHOTO']?>">
                                    <? } ?>
                                </div>
                                <? if (!empty($work)) {?>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Клиника: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                            <a class="g-link" href="#"><?=$work['NAME']?></a>
                                        </div>
                                    </div>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Адрес: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                            <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                        </div>
                                    </div>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Метро: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                        </div>
                                    </div>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Телефон: </span>
                                        </div>
                                        <div class="b-fields-row__content">
                                            <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                        </div>
                                    </div>
                                <? } else { ?>
                                <div class="b-info-box__fields-row b-fields-row">
                                    <div class="b-fields-row__label">
                                        <span class="g-label">Специалист не указал место работы</span>
                                    </div>
                                    <? } ?>
                                </div>
                                <div class="b-profile__all-inf g-v_a-top">
                                    <div class="b-steps">
                                        <div class="b-steps__your-data b-form-your-data">
                                            <span class="b-form-your-data__title">Пожалуйста, уточните Ваши контактные данные:</span>
                                            <form id="entry_form">
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Имя:
																			</span>
                                                    <input type="text" name="name" readonly placeholder="Иван Петров" value="<?=(!empty($user_data['LASTNAME']) ? $user_data['LASTNAME']." " : "").(!empty($user_data['FIRSTNAME']) ? $user_data['FIRSTNAME']." " : "").(!empty($user_data['MIDDLENAME']) ? $user_data['MIDDLENAME']." " : "")?>" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Телефон:
																			</span>
                                                    <input type="text" name="phone" readonly placeholder="+7" value="<?=$user_data['PHONE']?>" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Дата посещения специалиста:
																			</span>
                                                    <input id="entry_date" type="text" name="entry_date" readonly  placeholder="7 июля 2016" value="<?=$tommorow_foramted_date?>" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Время посещения:
																			</span>
                                                    <input id="entry_time" type="text" name="entry_time" readonly placeholder="9:15" value="9:00" class="b-form-your-data__field-row-input">
                                                    </span>
                                                </div>
                                                <div class="b-form-your-data__field-row b-form-your-data__field-row_2col">
																			<span class="b-form-your-data__field-row-lable">Ваше примечание:
																			</span>
                                                    <textarea type="text" name="comment" placeholder="9:15" class="b-form-your-data__field-row-input">
																			</textarea>
                                                </div>
                                                <input id="send_entry_form" type="submit" class="submit b-btn b-btn_uc b-form-your-data__submit js-pUp__openeer" data-pup-id="pUp-goDoc_4" value="Подтвердить и отправить" title="Подтвердить и отправить">
                                            </form>
                                        </div>
                                    </div>
                                    <div class="b-nav-profile">
                                        <input type="submit" class="submit b-btn b-btn_uc js-pUp__openeer" data-pup-id="pUp-goDoc_1" value="< Назад" title="Назад">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 3-й шаг ПОПАП - записи к врачу - БЛАГОДАРНОСТЬ  -->
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-goDoc_4">
                            <h2 class="h2 pUps__name">Запись к специалисту</h2>
                            <div class="b-profile">
                                <div class="b-profile__short-inf b-info-box">
                                    <span class="b-info-box__name"><?=(!empty($item['SECOND_NAME']) ? $item['SECOND_NAME']." " : "").(!empty($item['FIRST_NAME']) ? $item['FIRST_NAME']." " : "").(!empty($item['LAST_NAME']) ? $item['LAST_NAME']." " : "")?></span>
                                    <span class="b-info-box__type"><?=$item['LINKED_SPECIALIZATION']?></span>
                                    <div class="b-info-box__foto">
                                        <? if (!empty($item['PHOTO'])) { ?>
                                            <img src="<?=$item['PHOTO']?>">
                                        <? } ?>
                                    </div>
                                    <? if (!empty($work)) {?>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Клиника: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                                <a class="g-link" href="#"><?=$work['NAME']?></a>
                                            </div>
                                        </div>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Адрес: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                                <span><?=(!empty($work['ADDRESS_REGION']) ? $work['ADDRESS_REGION']."," : "").(!empty($work['ADDRESS_CITY']) ? $work['ADDRESS_CITY']."," : "").(!empty($work['ADDRESS_STREET']) ? $work['ADDRESS_STREET']."," : "").(!empty($work['ADDRESS_HOUSE']) ? $work['ADDRESS_HOUSE']."," : "").(!empty($work['ADDRESS_BUILDING']) ? $work['ADDRESS_BUILDING']."," : "").(!empty($work['ADDRESS_OFFICE']) ? $work['ADDRESS_OFFICE']."," : "")?></span>
                                            </div>
                                        </div>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Метро: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                <span class="b-fields-row__metro">
                                    <? foreach ($metros as $metro) { ?>
                                        <?=$metro['NAME'].","?>
                                    <? } ?>
                                </span>
                                            </div>
                                        </div>
                                        <div class="b-info-box__fields-row b-fields-row">
                                            <div class="b-fields-row__label">
                                                <span class="g-label">Телефон: </span>
                                            </div>
                                            <div class="b-fields-row__content">
                                                <span><?=(!empty($work['PHONE1']) ? $work['PHONE1']." " : "").(!empty($work['PHONE2']) ? $work['PHONE2']." " : "").(!empty($work['PHONE3']) ? $work['PHONE3']." " : "")?></span>
                                            </div>
                                        </div>
                                    <? } else { ?>
                                    <div class="b-info-box__fields-row b-fields-row">
                                        <div class="b-fields-row__label">
                                            <span class="g-label">Специалист не указал место работы</span>
                                        </div>
                                        <? } ?>
                                    </div>
                                    <div class="b-profile__all-inf">
                                        <div class="b-steps">
                                            <div class="b-steps__thx">
                                                <p class="b-steps__thx-txt">СПАСИБО!</p>
                                                <p>Специалист свяжется с Вами по вопросу записи на приём</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- END list popups -->
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>