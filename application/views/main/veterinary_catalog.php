<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Медицинские учреждения</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Ветеренария</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех ветучреждениях вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>учреждений</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 867 220</b><br>клиник</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="/veterinary/catalog">Ветеринария</a></li>
                <li><a href=""><?=$type?></a></li>
            </ul>

            <div class="two-cols search-result search-result-list hospital">
                <div class="left-col">

                    <div class="top clearfix">
                        <h1><?=$type?></h1>
                    </div>

                    <table class="results-table">
                        <tr>
                            <th class="name">Медучреждение</th>
                            <th class="distance">Удаленность</th>
                            <th class="on-map">На карте</th>
                        </tr>
                    </table>
                    <? //Если первый элемент, то сразу присваиваем класс best, который раскроет его для просмотра
                    if (empty($catalog)) { ?>
                        <br>
                        По вашему запросу не найдено ни одного объекта
                    <? } else {
                        reset($catalog);
                        $first_key = key($catalog);

                        foreach ($catalog as $key => $element) { ?>
                            <div class="no-active<?=($key == $first_key) ? " best" : ""?>">
                                <table class="results-table">
                                    <tr>
                                        <td class="name"><a href="#" class="dotted"><?=$element['NAME']?></a></td>
                                        <td class="distance">~ 2 час</td>
                                        <td class="on-map"><a href="#" class="link">На карте</a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="list-detail">
                                            <div class="two-cols">
                                                <div class="left-col">
                                                    <div class="image"><img src="<?=$element["LOGO"]?>" alt=""></div>
                                                    <a class="btn in-development" href="#">записаться на прием</a>
                                                    <ul class="list-instruction">
                                                        <li><a href="/institutions/item/<?=$element['ID']?>">описание</a></li>
                                                        <li><a href="#">услуги</a></li>
                                                        <!--                                                    <li><a href="#">специализации</a></li>-->
                                                        <li><a href="#">персонал</a></li>
                                                        <li><a href="#">отделения</a></li>
                                                        <li><a href="#">отзывы</a></li>
                                                    </ul>
                                                </div>
                                                <div class="right-col">
                                                    <table class="contacts">
                                                        <tr>
                                                            <td><span class="label">Адрес:</span></td>
                                                            <td colspan="3"><?=$element['ADDRESS']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="label">Метро:</span></td>
                                                            <td><span class="metro"><?=$element['METRO']?></span></td>
                                                            <td><span class="label">Сайт:</span></td>
                                                            <td><a href="#"><?=$element['WEBSITES']?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="label">Телефон:</span></td>
                                                            <td><?=nl2br($element['PHONES'])?></td>
                                                            <td><span class="label">График работы:</span></td>
                                                            <td><?=nl2br($element['SCHEDULE'])?></td>
                                                        </tr>
                                                    </table>

                                                    <div class="description-text">
                                                        <?=nl2br($element['DESCRIPTION'])?>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <? }
                    } ?>
                    <?=$pagination?>
                </div>

                <div class="right-col">
                    <h2>Уточнить поиск</h2>
                    <div class="search-block-red search-place">
                        <form action="#">
                            <p class="title inlineBlock red">Местоположение</p>
                            <a href="#" class="reset red">Сбросить все</a>
                            <p><i class="icon place"></i><a href="#" class="dotted">Выбрать местоположение</a></p>

                            <hr/>

                            <ul class="list-two">
                                <li><input type="checkbox" class="checkbox" id="input_1"/><label for="input_1">Государственные</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_2"/><label for="input_2">Частные</label></li>
                            </ul>

                            <hr/>

                            <select>
                                <option>Cпециализация</option>
                                <option>Cпециализация 1</option>
                                <option>Cпециализация 2</option>
                                <option>Cпециализация 3</option>
                                <option>Cпециализация 4</option>
                            </select>

                            <hr/>

                            <select>
                                <option>Врач</option>
                                <option>Врач 1</option>
                                <option>Врач 2</option>
                                <option>Врач 3</option>
                                <option>Врач 4</option>
                            </select>

                            <hr/>

                            <select>
                                <option>Анализы</option>
                                <option>Анализы 1</option>
                                <option>Анализы 2</option>
                                <option>Анализы 3</option>
                                <option>Анализы 4</option>
                            </select>

                            <hr/>

                            <select>
                                <option>Диагностика</option>
                                <option>Диагностика 1</option>
                                <option>Диагностика 2</option>
                                <option>Диагностика 3</option>
                                <option>Диагностика 4</option>
                            </select>

                            <hr/>


                            <ul class="list-two">
                                <li><input type="checkbox" class="checkbox" checked="checked" id="input_3"/><label for="input_3">Круглосуточно</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_5"/><label for="input_5">Скорая помощь</label></li>
                                <li><input type="checkbox" class="checkbox" id="input_4"/><label for="input_4">Детское отделение</label></li>
                            </ul>
                        </form>
                    </div>

                    <div class="advice">
                        Получите <a href="#" class="red link">совет</a> по данному препарату
                        <div class="popup">
                            <form action="#">
                                <textarea cols="50" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно."></textarea>
                                <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>


                </div>

            </div>


        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['popular_institutions']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>


        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>
