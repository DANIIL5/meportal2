<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Медицинские учреждения</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Реабилитация</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех медучреждениях вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>учреждений</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>центров</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_klin.png" alt="">
                        <span><b>1 867 220</b><br>клиник</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">

            <div class="three_cols">
                <div class="left_col apteca_list institution_list">
                    <ul>
                        <li>
                            <a href="/rehabilitation/catalog"><h3>Все медицинские учреждения</h3></a>
                            <i class="icon list"></i>
                            <span>Отсортированный список мед. <br/>учреждений вашего города</span>
                        </li>
                        <li>
                            <a href="/rehabilitation/catalog?type_id=1"> <h3>ДИСПАНСЕРЫ</h3></a>
                            <i class="icon list"></i>
                            <span>Отсортированный список мед. <br/>учреждений вашего города</span>
                        </li>
                        <li>
                            <a href="/rehabilitation/catalog?type_id=2"><h3>БОЛЬНИЦЫ</h3></a>
                            <i class="icon paper"></i>
                            <span>Рейтинг частных клиник</span>
                        </li>
                        <li>
                            <a href="/rehabilitation/catalog?type_id=3"><h3>РОД. ДОМА</h3></a>
                            <i class="icon paper"></i>
                            <span>Рейтинг частных клиник</span>
                        </li>
                        <li>
                            <a href="/rehabilitation/catalog?type_id=4"><h3>ПОЛИКЛИНИКИ</h3></a>
                            <i class="icon guide"></i>
                            <span>Бюджетные заведения</span>
                        </li>
                        <li>
                            <a href="/rehabilitation/catalog?type_id=5"><h3>ЛАБОРАТОРИИ</h3></a>
                            <i class="icon guide"></i>
                            <span>Бюджетные заведения</span>
                        </li>
                        <li>
                            <a href="/rehabilitation/catalog?type_id=6"><h3>ТРАВМПУНКТЫ</h3></a>
                            <i class="icon producer"></i>
                            <span>Дополнительная информация при обращении в медучреждение</span>
                        </li>
                    </ul>
                    <div class="order2doctor">
                        <a href="#" class="btn in-development">Записаться на прием</a>
                        <div class="popup">
                            <form action="#">
                                <p class="title transform size12"><b>Найти специалиста</b></p>
                                <input class="text" type="text">
                                <input type="submit" class="submit btn" value="Найти" title="Найти">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>
                </div>


                <?=$templates['ask_question']?>
            </div>


        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['popular_institutions']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?>

                <?=$templates['voiting']?>
            </div>
        </div>


        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>