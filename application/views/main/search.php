<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Результаты поиска</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header" class="main_page">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 300-32-32</a>

                <!-- Подключение выпадающего списка с городами -->
                <?=$templates['city_selector']?>

                <div class="registry dotted">Регистратура</div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan">
                <h1>Портал для профессионалов</h1>
                <span class="text">Мы постарались собрать всю информацию о медицине и фармакалогии в одном месте.</span>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <div class="b-width-right-box">
                <div class="b-width-right-box__main">
                    <h2 class="b-width-right-box__main-ttl">Результаты поиска:</h2>
                    <div class="b-search-result">
                        <!-- блок, если ничего не найдено -->
                        <!-- 	<div class="b-search-result__row b-search-result__no-result">
                                Совпадений не найдено
                            </div> -->
                        <? if (empty($search_results['medicines']['list']) 
                                && empty($search_results['drugstores']['list']) 
                                && empty($search_results['organisations']['list']) 
                                && empty($search_results['actions']['list'])
                                && empty($search_results['articles']['list'])
                                && empty($search_results['rss_news']['list'])
                                && empty($search_results['calculators']['list'])
                                && empty($search_results['ratings']['list'])) {?>
                            <div class="b-search-result__row b-search-result__no-result">
                                Совпадений не найдено
                            </div>
                        <?} else {?>
                            <? if (!empty($search_results['medicines']['list']) && $search_results['medicines']['count'] > 0) {?>
                                <div class="b-search-result__row b-search-item">
                                    <div class="b-search-item__category b-search-result-category">
                                        <div class="b-search-result-category__title">
                                            <h3 class="b-search-result-category__title-name">Товары</h3>
                                            <span class="b-search-result-category__title-count">(<?=$search_results['medicines']['count']?>)</span>
                                        </div>
                                    </div>
                                    <div class="b-search-item__list">
                                        <ul class="b-short-info-list">
                                        <? foreach ($search_results['medicines']['list'] as $array) { ?>
                                            <li>
                                                <a class="b-short-info-list__name" href="/medicines/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a>
                                                <span class="b-short-info-list__bcrumbs"><a href="/medicines/catalog">Товары</a> > <a href="/medicines/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a> > <a href="/medicines/catalog?id=<?=$array['ID']?>">Где купить</a></span>
                                            </li>
                                        <? } ?>
                                            <? if ($search_results['medicines']['count'] > $search_results['limit']) {?>
                                            <li class="b-short-info-list__view-all">
                                                <form action="/medicines/catalog" method="get">
                                                    <input type="hidden" name="query" value="<?=$_GET['query']?>">
                                                    <input class="submit b-btn b-btn_countour" value="Показать все (<?=$search_results['medicines']['count']?>)" title="Показать все (<?=$search_results['medicines']['count']?>)" type="submit">
                                                </form>
                                            </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            <? } ?>

                            <? if (!empty($search_results['drugstores']['list']) && $search_results['drugstores']['count'] > 0) {?>
                                <div class="b-search-result__row b-search-item">
                                    <div class="b-search-item__category b-search-result-category">
                                        <div class="b-search-result-category__title">
                                            <h3 class="b-search-result-category__title-name">Аптеки</h3>
                                            <span class="b-search-result-category__title-count">(<?=$search_results['drugstores']['count']?>)</span>
                                        </div>
                                    </div>
                                    <div class="b-search-item__list">
                                        <ul class="b-short-info-list">
                                        <? foreach ($search_results['drugstores']['list'] as $array) { ?>
                                            <li>
                                                <a class="b-short-info-list__name" href="/drugstores/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a>
                                                <span class="b-short-info-list__bcrumbs"><a href="/drugstores/catalog">Аптеки</a> > <a href="/drugstores/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a></span>
                                                <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                            </li>
                                        <? } ?>
                                            <? if ($search_results['drugstores']['count'] > $search_results['limit']) {?>
                                            <li class="b-short-info-list__view-all">
                                                <form action="/drugstores/catalog" method="get">
                                                    <input type="hidden" name="query" value="<?=$_GET['query']?>">
                                                    <input class="submit b-btn b-btn_countour" value="Показать все (<?=$search_results['drugstores']['count']?>)" title="Показать все (<?=$search_results['drugstores']['count']?>)" type="submit">
                                                </form>
                                            </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            <? } ?>

                            <? if (!empty($search_results['organisations']['list']) && $search_results['organisations']['count'] > 0) {?>
                                <div class="b-search-result__row b-search-item">
                                    <div class="b-search-item__category b-search-result-category">
                                        <div class="b-search-result-category__title">
                                            <h3 class="b-search-result-category__title-name">Организации</h3>
                                            <span class="b-search-result-category__title-count">(<?=$search_results['organisations']['count']?>)</span>
                                        </div>
                                    </div>
                                    <div class="b-search-item__list">
                                        <ul class="b-short-info-list">
                                        <? foreach ($search_results['organisations']['list'] as $array) { ?>
                                            <li>
                                                <a class="b-short-info-list__name" href="/institutions/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a>
                                                <span class="b-short-info-list__bcrumbs"><a href="/institutions/catalog">Организации</a> > <a href="/institutions/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a></span>
                                                <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                            </li>
                                        <? } ?>
                                            <? if ($search_results['organisations']['count'] > $search_results['limit']) {?>
                                            <li class="b-short-info-list__view-all">
                                                <form action="/institutions/catalog" method="get">
                                                    <input type="hidden" name="query" value="<?=$_GET['query']?>">
                                                    <input class="submit b-btn b-btn_countour" value="Показать все (<?=$search_results['organisations']['count']?>)" title="Показать все (<?=$search_results['organisations']['count']?>)" type="submit">
                                                </form>
                                            </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            <? } ?>

                            <? if (!empty($search_results['articles']['list']) && $search_results['articles']['count'] > 0) {?>
                                <div class="b-search-result__row b-search-item">
                                    <div class="b-search-item__category b-search-result-category">
                                        <div class="b-search-result-category__title">
                                            <h3 class="b-search-result-category__title-name">Статьи</h3>
                                            <span class="b-search-result-category__title-count">(<?=$search_results['articles']['count']?>)</span>
                                        </div>
                                    </div>
                                    <div class="b-search-item__list">
                                        <ul class="b-short-info-list">
                                        <? foreach ($search_results['articles']['list'] as $array) { ?>
                                            <li>
                                                <a class="b-short-info-list__name" href="/articles/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a>
                                                <span class="b-short-info-list__bcrumbs"><a href="/articles/catalog">Статьи</a> > <a href="/articles/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a></span>
                                                <p class="b-short-info-list__info"><?=mb_substr(strip_tags($array['TEXT']),0,130,'UTF-8')."...";?></p>
                                            </li>
                                        <? } ?>
                                            <? if ($search_results['articles']['count'] > $search_results['limit']) {?>
                                            <li class="b-short-info-list__view-all">
                                                <form action="/articles/catalog" method="get">
                                                    <input type="hidden" name="query" value="<?=$_GET['query']?>">
                                                    <input class="submit b-btn b-btn_countour" value="Показать все (<?=$search_results['articles']['count']?>)" title="Показать все (<?=$search_results['articles']['count']?>)" type="submit">
                                                </form>
                                            </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            <? } ?>

                            <? if (!empty($search_results['actions']['list']) && $search_results['actions']['count'] > 0) {?>
                                <div class="b-search-result__row b-search-item">
                                    <div class="b-search-item__category b-search-result-category">
                                        <div class="b-search-result-category__title">
                                            <h3 class="b-search-result-category__title-name">Акции</h3>
                                            <span class="b-search-result-category__title-count">(<?=$search_results['actions']['count']?>)</span>
                                        </div>
                                    </div>
                                    <div class="b-search-item__list">
                                        <ul class="b-short-info-list">
                                        <? foreach ($search_results['actions']['list'] as $array) { ?>
                                            <li>
                                                <a class="b-short-info-list__name" href="/actions/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a>
                                                <span class="b-short-info-list__bcrumbs"><a href="/actions/catalog">Акции</a> > <a href="/actions/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a></span>
                                                <p class="b-short-info-list__info"><?=mb_substr($array['DESCRIPTION'], 0, 247).'...'?></p>
                                            </li>
                                        <? } ?>
                                            <? if ($search_results['actions']['count'] > $search_results['limit']) {?>
                                            <li class="b-short-info-list__view-all">
                                                <form action="/actions/catalog" method="get">
                                                    <input type="hidden" name="query" value="<?=$_GET['query']?>">
                                                    <input class="submit b-btn b-btn_countour" value="Показать все (<?=$search_results['actions']['count']?>)" title="Показать все (<?=$search_results['actions']['count']?>)" type="submit">
                                                </form>
                                            </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            <? } ?>

                            <? if (!empty($search_results['calculators']['list']) && $search_results['calculators']['count'] > 0) {?>
                                <div class="b-search-result__row b-search-item">
                                    <div class="b-search-item__category b-search-result-category">
                                        <div class="b-search-result-category__title">
                                            <h3 class="b-search-result-category__title-name">Калькуляторы</h3>
                                            <span class="b-search-result-category__title-count">(<?=$search_results['calculators']['count']?>)</span>
                                        </div>
                                    </div>
                                    <div class="b-search-item__list">
                                        <ul class="b-short-info-list">
                                            <? foreach ($search_results['calculators']['list'] as $array) { ?>
                                                <li>
                                                    <a class="b-short-info-list__name" href="/calculators/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a>
                                                    <span class="b-short-info-list__bcrumbs"><a href="/calculators/catalog">Калькуляторы</a> > <a href="/calculators/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a></span>
                                                    <p class="b-short-info-list__info"><?=strip_tags(mb_substr($array['TEXT'], 0, 247)).'...'?></p>
                                                </li>
                                            <? } ?>
                                            <? if ($search_results['calculators']['count'] > $search_results['limit']) {?>
                                                <li class="b-short-info-list__view-all">
                                                    <form action="/calculators/catalog" method="get">
                                                        <input type="hidden" name="query" value="<?=$_GET['query']?>">
                                                        <input class="submit b-btn b-btn_countour" value="Показать все (<?=$search_results['calculators']['count']?>)" title="Показать все (<?=$search_results['calculators']['count']?>)" type="submit">
                                                    </form>
                                                </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            <? } ?>

                        <? if (!empty($search_results['ratings']['list']) && $search_results['ratings']['count'] > 0) {?>
                            <div class="b-search-result__row b-search-item">
                                <div class="b-search-item__category b-search-result-category">
                                    <div class="b-search-result-category__title">
                                        <h3 class="b-search-result-category__title-name">Рейтинги</h3>
                                        <span class="b-search-result-category__title-count">(<?=$search_results['ratings']['count']?>)</span>
                                    </div>
                                </div>
                                <div class="b-search-item__list">
                                    <ul class="b-short-info-list">
                                        <? foreach ($search_results['ratings']['list'] as $array) { ?>
                                            <li>
                                                <a class="b-short-info-list__name" href="/ratings/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a>
                                                <span class="b-short-info-list__bcrumbs"><a href="/ratings/catalog">Рейтинги</a> > <a href="/ratings/item/<?=(int)$array['ID']?>"><?=$array['NAME']?></a></span>
                                                <p class="b-short-info-list__info"><?=strip_tags(mb_substr($array['TEXT'], 0, 247)).'...'?></p>
                                            </li>
                                        <? } ?>
                                        <? if ($search_results['ratings']['count'] > $search_results['limit']) {?>
                                            <li class="b-short-info-list__view-all">
                                                <form action="/ratings/catalog" method="get">
                                                    <input type="hidden" name="query" value="<?=$_GET['query']?>">
                                                    <input class="submit b-btn b-btn_countour" value="Показать все (<?=$search_results['ratings']['count']?>)" title="Показать все (<?=$search_results['ratings']['count']?>)" type="submit">
                                                </form>
                                            </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </div>
                        <? } ?>
                        <!-- <div class="b-search-result__row b-search-item">
                            <div class="b-search-item__category b-search-result-category">
                                <div class="b-search-result-category__title">
                                    <h3 class="b-search-result-category__title-name">Медучреждения</h3>
                                    <span class="b-search-result-category__title-count">(3)</span>
                                </div>
                            </div>
                            <div class="b-search-item__list">
                                <ul class="b-short-info-list">
                                    <li>
                                        <a class="b-short-info-list__name" href="#">АС «Здоровье»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">Аптека «Сердце и легкие» Москомспорта г. Москвы и МО</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">Аптека «Сердце и легкие» Москомспорта г. Москвы и МО</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">Аптечный пункт «Не боли моя печень»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">АС «Здоровье»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">АС «Здоровье»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li class="b-short-info-list__view-all">
                                        <input class="submit b-btn b-btn_countour" value="Все 64 аптеки" title="Все 64 аптеки" type="submit">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="b-search-result__row b-search-item">
                            <div class="b-search-item__category b-search-result-category">
                                <div class="b-search-result-category__title">
                                    <h3 class="b-search-result-category__title-name">Аптеки</h3>
                                    <span class="b-search-result-category__title-count">(64)</span>
                                </div>
                            </div>
                            <div class="b-search-item__list">
                                <ul class="b-short-info-list">
                                    <li>
                                        <a class="b-short-info-list__name" href="#">АС «Здоровье»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">Аптека «Сердце и легкие» Москомспорта г. Москвы и МО</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">Аптека «Сердце и легкие» Москомспорта г. Москвы и МО</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">Аптечный пункт «Не боли моя печень»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">АС «Здоровье»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li>
                                        <a class="b-short-info-list__name" href="#">АС «Здоровье»</a>
                                        <span class="b-short-info-list__bcrumbs"><a href="#">Аптеки</a> > <a href="#">АС «Здоровье»</a></span>
                                        <p class="b-short-info-list__info">Ощущение мира амбивалентно рефлектирует из ряда вон выходящий конфликт при любом агрегатном состоянии среды взаимодействия.</p>
                                    </li>
                                    <li class="b-short-info-list__view-all">
                                        <input class="submit b-btn b-btn_countour" value="Все 64 аптеки" title="Все 64 аптеки" type="submit">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- <? } ?> -->
                    </div> 
                </div>
                <div class="b-width-right-box__right">
                    <?=$templates['actions']?>
                </div>
            </div>
        </div>
    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>