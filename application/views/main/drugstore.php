<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Аптека</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <a href="/profile/registration"><div class="btn">Портал для профессионалов</div></a>
            </div>

            <div class="slogan slider-head">
                <h1>Все аптеки города</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех аптеках вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_apt.png" alt=""/>
                        <span><b>200</b><br/>аптек</span>
                    </div>
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 433 562</b><br/>препаратов</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="/drugstores/catalog">Аптеки</a></li>
                <li><a href=""><?=$item["NAME"]?></a></li>
            </ul>

            <div class="search-card pharmacy-card">
                <div class="two-cols">
                    <div class="left-col">
                        <div class="description">
                            <div class="image"><img src="<?=$item["LOGO"]?>" alt=""></div>
                            <div class="params">
                                <h1><span class="transform"><?=$item["NAME"]?></span></h1>
                                <table class="table-card pharmacy-card">
                                    <tr>
                                        <td>Адрес:</td>
                                        <td><?=$item["ADDRESS"]?><br/><a class="red size12 dotted" href="#">На карте</a></td>
                                    </tr>
                                    <tr>
                                        <td>Телефон:</td>
                                        <td><?=nl2br($item["PHONES"])?></td>
                                    </tr>
                                    <tr>
                                        <td>График работы:</td>
                                        <td><?=nl2br($item["SCHEDULE"])?></td>
                                    </tr>
                                    <tr>
                                        <td>Ближайшее метро:</td>
                                        <td><?=nl2br($item["METRO"])?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>


                    <div class="right-col">
                        <h2>Аптека</h2>

                        <ul class="list-instruction">
                            <li><a href="#">Описание</a></li>
                            <li><a href="#">Прайс-лист</a></li>
                            <li><a href="#">Адреса всех аптек</a></li>
                            <li><a href="#">Акции</a></li>
                        </ul>
                    </div>

                    <div class="clear"></div>

                    <div class="search-in-card search_form">
                        <div class="wrapper">
                            <div id="organisation_search" class="forma">
                                <form action="/medicines/catalog" method="get" enctype="multipart/form-data">
                                    <var class="title georgia transform">поиск</var>
                                    <span class="for_input"><input id="organisation_search_query" attr-id="<?=$branchid?>" type="text" class="text" placeholder="например, аптека на Невском" name="query"></span>
                                    <input type="hidden" name="id" value="<?=$branchid?>">
                                    <input type="submit" class="submit btn" value="Найти" title="Найти">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="description-bottom clearfix">
                        <div class="description-left size12">
                            <h2>Описание</h2>
                            <?=nl2br($item["DESCRIPTION"])?>
                        </div>
                        <div class="description-middle size12">
                            <h2>Еще аптеки</h2>
                            <ul class="pharmacy-list">
                                <?
                                if (!empty($another_drugstores)) {
                                    $count = count($another_drugstores);
                                    for ($i = 0; $i < $count && $i < 3; $i++) {?>
                                    <li><a href><?=nl2br($another_drugstores[$i]["ADDRESS"])?></a><br/>
                                        <?=nl2br($another_drugstores[$i]["PHONES"])?>
                                    </li>
                                <? }
                                }?>
                            </ul>
                            <p><a href="#" class="dotted red list-all">Все аптеки одним списком</a></p>
                        </div>

                        <div class="description-right">
                            <div class="write-us">
                                <h2>Свяжитесь с нами</h2>
                                <form action="#">
                                    <textarea cols="50" rows="4" placeholder="Наш формацевт ответит вам в течение 10 минут"></textarea>
                                    <input type="submit" class="submit btn" value="Отправить" title="Отправить">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="pharmacy-table">
                        <table class="results-table">
                            <tr>
                                <th class="name">Адрес</th>
                                <th class="distance">Удаленность</th>
                                <th class="phone">Телефон</th>
                                <th class="on-map">На карте</th>
                            </tr>
                            <? foreach ($another_drugstores as $another_drugstore) { ?>
                                <tr>
                                <td class="name"><a href="/drugstores/item/<?=$another_drugstore["ID"]?>"><?=$another_drugstore["ADDRESS"]?></td>
                                <td class="distance">~ 15 мин</td>
                                <td class="phone"><?=$another_drugstore["PHONES"]?></td>
                                <td class="on-map"><a href class="link">На карте</a></td>
                            </tr>
                            <? } ?>
                        </table>
                    </div>

<!--                    <div class="special-offer analog-offer container analog-offer-8">-->
<!--                        <h3>скидки</h3>-->
<!--                        <p class="note">Совпадает код АТС, состав действующих веществ и форма выпуска</p>-->
<!--                        <hr/>-->
<!--                        <div class="list">-->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-1.png" alt=""/>-->
<!--                                <span class="sale">50 % <var>скидка</var></span>-->
<!--                                <a class="title" href="#">Никоретте</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!---->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-2.jpg" alt=""/>-->
<!--                                <span class="sale">50 % <var>скидка</var></span>-->
<!--                                <a class="title" href="#">Coldrex</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!---->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-1.png" alt=""/>-->
<!--                                <a class="title" href="#">Никоретте</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!---->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-2.jpg" alt=""/>-->
<!--                                <a class="title" href="#">Coldrex</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!---->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-1.png" alt=""/>-->
<!--                                <span class="sale">50 % <var>скидка</var></span>-->
<!--                                <a class="title" href="#">Никоретте</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!---->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-2.jpg" alt=""/>-->
<!--                                <span class="sale">50 % <var>скидка</var></span>-->
<!--                                <a class="title" href="#">Coldrex</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-1.png" alt=""/>-->
<!--                                <span class="sale">50 % <var>скидка</var></span>-->
<!--                                <a class="title" href="#">Никоретте</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!---->
<!--                            <div class="special-offer-good">-->
<!--                                <img class="preview" src="/temp/offer-2.jpg" alt=""/>-->
<!--                                <span class="sale">50 % <var>скидка</var></span>-->
<!--                                <a class="title" href="#">Coldrex</a>-->
<!--                                <p>2 100 - 4 500 Р</p>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>




        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['first_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>

<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_comments ">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

</body>
</html>