<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города - Врачи</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted"><a href="/profile/registration">Регистратура</a></div>

                <div class="btn">Портал для профессионалов</div>
            </div>

            <div class="slogan slider-head">
                <h1>Врачи</h1>
                <span class="text">МЫ ПОСТАРАЛИСЬ СОБРАТЬ подробную информацию о всех врачах вашего города</span>
                <div class="header-icons">
                    <div class="head-icon">
                        <img src="/images/slider_head_drug.png" alt=""/>
                        <span><b>1 867 220</b><br/>специалистов</span>
                    </div>
                </div>
            </div>

            <?=$templates['auth']?>
        </div>
        <?=$templates['carousel']?>
    </header>

    <?=$templates['menu']?>

    <?=$templates['search']?>

    <div id="content">

        <div class="container">

            <div class="three_cols doctors">
                <div class="left_col">

                    <div class="first-aid">
                        <h3><a href="/first_help/catalog/?category_id=19">Первая помощь</a></h3>
                        <p>срочное выполнение лечебно-профилактических мероприятий, меры экстренной помощи раненым или больным людям.</p>
                        <a href="#" class="btn_white">Получить информацию</a>
                    </div>

                    <div class="order2doctor">
                        <a href="#"  class="submit b-btn b-btn_uc js-pUp__openeer btn" data-pup-id="pUp-zap">Записаться на прием</a>
                        <div class="popup">
                            <form action="#">
                                <p class="title transform size12"><b>Найти специалиста</b></p>
                                <input class="text" type="text">
                                <input type="submit" class="submit btn" value="Найти" title="Найти">
                            </form>
                            <span class="arrow"></span>
                        </div>
                    </div>
                </div>


                <div class="center_col apteca_list">
                    <ul class="list-with-icons height-3">
                        <li>
                            <a href="/doctors/catalog"><h3>Все специалисты</h3></a>
                            <i class="icon doctor-catalog"></i>
                            <span>Рейтинг частных клиник</span>
                        </li>
                        <li>
                            <a href="/doctors/catalog?pediatrician=1"><h3>Детские врачи</h3></a>
                            <i class="icon doctor-baby"></i>
                            <span>Бюджетные заведения</span>
                        </li>
                        <li>
                            <a href="/doctors/catalog?home=1"><h3>Выезд на дом</h3></a>
                            <i class="icon doctor-home"></i>
                            <span>Дополнительная информация при обращении в медучреждение</span>
                        </li>
                    </ul>

                </div>

                <?=$templates['ask_question']?>
            </div>


        </div>

        <?=$templates['logos']?>

        <?=$templates['map_search']?>

        <?=$templates['popular_doctors']?>

        <?=$templates['first_banners']?>

        <div class="themes">
            <div class="container">
                <?=$templates['popular_themes']?> 

                <?=$templates['voiting']?>
            </div>
        </div>


        <?=$templates['second_banners']?>

        <?=$templates['news']?>

    </div>

    <footer id="footer">
        <?=$templates['bottom_menu']?>

        <?=$templates['footer']?>
    </footer>
</div>



<!--  popUps  -->
<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg"></div>
    <div class="pUps__w pUps__w_modal " style="margin-left: -188px !important;">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg g-v_a-top">
            <!-- list popups of ID -->
            <div class="pUps__item js-pUp_m__item" id="pUp-zap">
                <div class="pUps__item__title">
                    Записаться
                </div>
                <div class="pUps__item__info-txt pUps__item__info-txt_grey">
                    Сообщение будет отправлено администратору
                    учреждения/врачу. После этого с вами должны
                    связаться по телефону или эл. почте.
                </div>

                <form id="inst_entry " onsubmit="inst_entry(event, this)">
                    <select name="specialization" id="">
                        <? foreach ($specialisations as $specialisation) { ?>
                        <option value="<?=$specialisation['ID']?>"><?=$specialisation['NAME']?></option>
                        <? } ?>
                    </select>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Как Вас зовут?</div>
                        <div class="b-input b-input_icon b-input_icon_name">
                            <input class="" name="name" placeholder="Как Вас зовут?" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Номер телефона</div>
                        <div class="b-input b-input_icon b-input_icon_tel">
                            <input class="js-tel" name="tel" placeholder="Номер телефона" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l b-input_icon_input">
                        <div class="b-input-box__label">Эл. почта</div>
                        <div class="b-input b-input_icon b-input_icon_email">
                            <input class="" name="email" placeholder="Эл. почта" type="text"></input>
                        </div>
                    </div>

                    <div class="b-input-box b-input-box_not-l">
                        <div class="b-input-box__label">Сообщение</div>
                        <div class="b-input b-input_icon b-input_tArea b-input_icon_msg">
                            <textarea name="msg" placeholder="Сообщение" type="text"></textarea>
                        </div>
                    </div>
                    <input type="submit" class="submit b-btn b-btn_uc pUps__item__button pUps__item__button_finish" value="Отправить сообщение" title="Отправить сообщение">
                </form>
            </div>

            <!-- END list popups -->
        </div>
    </div>
</div>

<div class="b-ALERT js-alert">
    <div class="b-ALERT__ov"></div>
    <div class="b-ALERT__box">
        <div class="b-ALERT__x"></div>
        <div class="b-ALERT__inner">
            Содержимое алерта
        </div>
    </div>
</div>
<!-- END - popUps -->

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script type="text/javascript" src="/js/YAmap.js"></script>
<script type="text/javascript" src="/js/js-ALERT.js"></script>

<script>
    $(function(){
        $(document).on("submit","#metro_map_form",function(e){
            var values = [];
            $("#metro_list").html("");
            $(this).find('input:checked').each(function(index, element) {
                values.push($(this).val());
                var id_metro = $(this).val();
                var name_metro = $(this).attr("data-name");
                $("#metro_list").append('<p style="margin-bottom:0px;" id="metrolist_'+id_metro+'"> '+name_metro+' [<a class="del_metroList" data-id="'+id_metro+'" href="#">x</a>]<input type="hidden" value="'+id_metro+'" name="metro_list[]" /></p>');
            });
            $('#metro_ids').val(JSON.stringify(values));
            $('#metro_ids').trigger('change');
            $('form.filter input').trigger('change');
            $.magnificPopup.close();
            show_count();
            //$('#metro_ids').parents('form').submit();
            return false;
        });

        function show_count()
        {
            $(".show_count").html("");
            $(".show_count").html("<img src='/upload/ajax-loader.gif' />");
            $.ajax({
                type: "POST",
                url: "/catalog/show_count.php",
                data: $("#catalog_filter").serialize(), // serializes the form's elements.
                success: function(data)
                {
                    console.log(data);
                    $(".show_count").html(data);
                },
                error: function()
                {
                    $(".show_count").html("");
                }
            });

            return false;
        }
    });
</script>

</body>
</html>