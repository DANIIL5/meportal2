<!--  END - MAIN MENU -->
<div class="content js-pUp__openeer" data-pup-id="pUp-save-thx"> 
    <span class="title title-box">ФОТОГРАФИИ</span>
    <div class="box box_company">
        <div class="clear"></div>
        <!--  BLOCK - ADD FILES -->
        <div class="box b-add-f">
            <p class="paragraph paragraph_prew">Постарайтесь, чтобы размер картинки не превышал <span class="b-add-f__limit">2 Мб</span> и картинка была в одном из следующих форматов - jpg, png, gif.</p>
            <div class="b-add-f-reg js-add-files">
            	<ul class="b-add-f-list js-add-list"><?php
                    foreach($files as $file)
                    {
                		?><li class="b-add-f-list__i js-add-li" data-img-path="<?=$file['FILE']?>">
                            <img src="<?=$file['FILE']?>"/>
                            <input type="hidden" class="_hidden-id" value="<?=$file['FILE']?>"/>
                            <span class="b-add-f-list__i-delete jd-add-f_delete" data-pup-id="pUp-del-document"></span>
                        </li><?php
                    }
            	    ?><li class="b-add-f-list__i b-add-f-list__i_add-new js-add-f"></li>
                </ul>
            </div>
        </div>
        <!--  END - BLOCK - ADD FILES -->
        <div class="clear"></div>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->