<!-- END - MAIN MENU -->
<div class="content js-pUp__openeer">
    <div class="box box_favorite_main">
        <div class="box-fields-row box-fields-row_inner">
            <div class="b-tab-content_statistic">
                <ul class="b-tab-content_st">
                    <li class="b-tab-content_st__i">
                        <span class="b-tab-content_st__i-count"><?=$count_liked_institutions[0]['COUNT']?></span>
                        <div class="b-tab-content_st__i-desc">медучреждений</div>
                    </li>
                    <li class="b-tab-content_st__i">
                        <span class="b-tab-content_st__i-count"><?=$count_liked_specialists[0]['COUNT']?></span>
                        <div class="b-tab-content_st__i-desc">врачей</div>
                    </li>
                    <li class="b-tab-content_st__i">
                        <span class="b-tab-content_st__i-count"><?=$count_liked_preps[0]['COUNT']?></span>
                        <div class="b-tab-content_st__i-desc">препаратов</div>
                    </li>
                    <li class="b-tab-content_st__i">
                        <span class="b-tab-content_st__i-count"><?=$count_liked_articles[0]['COUNT']?></span>
                        <div class="b-tab-content_st__i-desc">статей</div>
                    </li>
                </ul>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text box-fields_search">
                    <form action="/profile/liked_search" method="POST">
                        <input type="text" placeholder="Искать среди избранного" class="box-fields__input box-fields__input_search" name="search"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="box-fields-row box-fields-row_out box-fields-row_out_gray">
            <div class="box-fields-row_inner">
                <div class="title-row">
                    <div class="title title-box">Медицинские учреждения</div>
                    <input value="ПОКАЗАТЬ ВСЕ" class="btn-lk btn-lk_inner btn-lk_inner_contour" type="submit" onclick="window.location.pathname='/profile/liked_institutions'"/>
                </div>
                <div class="b-row-tab">
                    <ul class="b-tab-content"><?php
                        foreach($liked_institutions as $liked)
                        {
                            ?><li class="b-tab-content__i institution" data-target-id="<?=$liked['ID']?>">
                            <div class="b-tab-content__i-avatar">
                                <div class="b-tab-content__i-avatar_cover"><?php
                                    if(!empty($liked['LOGO']))
                                    {
                                        ?><a href="/organisations/item/<?=$liked['ID']?>"><img src="/images/organisations/<?=$liked['LOGO']?>" style="margin-top:10%;width:100%;"/></a><?php
                                    }
                                    else
                                    {
                                        ?><a href="/organisations/item/<?=$liked['ID']?>"><img src="/images/bg/def-ava.png" style="margin-top:10%;width:30%;"/></a><?php
                                    }
                                ?></div>
                                <input type="hidden" class="_hidden-id" value="<?=$liked['ID']?>"/>
                                <span class="b-tab-content__i-delete" onclick="pUp_m.eGetId(this,'Удалить из избранного?')" data-pup-id="pUp-del-liked-institution"></span>
                            </div>
                            <div class="b-tab-content__i-name"><?=$liked['BRAND']?></div>
                            <div class="b-tab-content__i-about"><?=$liked['ENTITY']?></div>
                            <div class="b-tab-content__i-about"><?=$liked['TYPE']?></div>
                            </li><?php
                        }?><li class="b-tab-content__i b-tab-content__i_add-new" onclick="window.open('/organisations/catalog','_blank');">
                            <div class="b-tab-content__i-avatar">
                            </div>
                            <div class="b-tab-content__i-name">Добавить</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="box-fields-row box-fields-row_out">
            <div class="box-fields-row_inner">
                <div class="title-row">
                    <div class="title title-box">врачи</div>
                    <input value="ПОКАЗАТЬ ВСЕ" class="btn-lk btn-lk_inner btn-lk_inner_contour" type="submit" onclick="window.location.pathname='/profile/liked_specialists'"/>
                </div>
                <div class="b-row-tab">
                    <ul class="b-tab-content"><?php
                        foreach($liked_specialists as $liked)
                        {
                            ?><li class="b-tab-content__i specialist" data-target-id="<?=$liked['ID']?>">
                                <div class="b-tab-content__i-avatar">
                                    <div class="b-tab-content__i-avatar_cover"><?php
                                        if(!empty($liked['PHOTO']))
                                        {
                                            ?><a href="/doctors/item/<?=$liked['ID']?>"><img src="<?=$liked['PHOTO']?>" style="margin-top:10%;width:100%;"/></a><?php
                                        }
                                        else
                                        {
                                            ?><a href="/doctors/item/<?=$liked['ID']?>"><img src="/images/bg/def-ava.png" style="margin-top:10%;width:30%;"/></a><?php
                                        }
                                    ?></div>
                                    <input type="hidden" class="_hidden-id" value="<?=$liked['ID']?>"/>
                                    <span class="b-tab-content__i-delete" onclick="pUp_m.eGetId(this,'Удалить из избранного?')" data-pup-id="pUp-del-liked-specialist"></span>
                                </div>
                                <div class="b-tab-content__i-name" onclick="window.location.pathname='/doctors/item/<?=$liked['ID']?>'"><?=$liked['SECOND_NAME']?> <?=mb_substr($liked['FIRST_NAME'],0,1)?>.<?=mb_substr($liked['LAST_NAME'],0,1)?>.</div>
                                <div class="b-tab-content__i-about" onclick="window.location.pathname='/doctors/doctors/<?=$liked['ID']?>'"><?=$liked['SPECIALIZATION']?></div>
                            <div class="b-tab-content__i-about" onclick="window.location.pathname='/doctors/doctors/<?=$liked['ID']?>'"><?=$liked['WORK']?></div>
                            </li><?php
                        }?><li class="b-tab-content__i b-tab-content__i_add-new" onclick="window.open('/doctors/catalog','_blank');">
                             <div class="b-tab-content__i-avatar">
                            </div>
                            <div class="b-tab-content__i-name">Добавить</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="box-fields-row box-fields-row_out box-fields-row_out_gray">

            <div class="title-row">
                <div class="title title-box">препараты</div>
                <input value="ПОКАЗАТЬ ВСЕ" class="btn-lk btn-lk_inner btn-lk_inner_contour" type="submit" onclick="window.location.pathname='/profile/liked_preps'"/>
            </div>
            <div class="b-row-tab">
                <ul class="b-tab-content"><?php
                    foreach($liked_preps as $liked)
                    {
                        ?><li class="b-tab-content__i prep" data-target-id="<?=$liked['ID']?>">
                             <div class="b-tab-content__i-avatar">
                                <div class="b-tab-content__i-avatar_cover"><?php
                                    if(!empty($liked['IMAGE']))
                                    {
                                        ?><a href="/medicines/item/<?=$liked['NOMENID']?>"><img src="/images/rls/<?=(int)$liked['IMAGE']?>.gif"/><?php
                                    }
                                    else
                                    {
                                        ?><a href="/medicines/item/<?=$liked['NOMENID']?>"><img src="/temp/10.jpg" style="margin-top:10%;max-width:100%; max-height: 100%;"/></a><?php
                                    }
                                ?></div>
                                <input type="hidden" class="_hidden-id" value="<?=$liked['ID']?>"/>
                                <span class="b-tab-content__i-delete" onclick="pUp_m.eGetId(this,'Удалить из избранного?')" data-pup-id="pUp-del-liked-prep"></span>
                            </div>
                            <div class="b-tab-content__i-name"><?=$liked['NAME']?></div>
                            <div class="b-tab-content__i-about"><?=(!empty($liked['NOMENFIRMNAME']) ? $liked['NOMENFIRMNAME'] : $liked['FIRMNAME']) ?></div>
                            <div class="b-tab-content__i-about"><?=(!empty($liked['DRUGFORMNAME']) ? $liked['DRUGFORMNAME'] : "") ?></div>
                            <div class="b-tab-content__i-about"><?=(!empty($liked['PPACKNAME']) ? $liked['PPACKNAME'] : "") ?></div>
                         </li><?php
                    }?><li class="b-tab-content__i b-tab-content__i_add-new" onclick="window.open('/medicines/catalog','_blank');">
                        <div class="b-tab-content__i-avatar">
                        </div>
                        <div class="b-tab-content__i-name">Добавить</div>
                     </li>
                </ul>
            </div>
        </div>
        <div class="box-fields-row box-fields-row_out">
            <div class="box-fields-row_inner">
                <div class="title-row">
                    <div class="title title-box">Статьи</div>
                    <input value="ПОКАЗАТЬ ВСЕ" class="btn-lk btn-lk_inner btn-lk_inner_contour" type="submit" onclick="window.location.pathname='/profile/liked_articles'"/>
                </div>

                <div class="b-row-tab">
                    <ul class="b-tab-content b-tab-content_weight">
                        <?php foreach($liked_articles as $liked)
                        {
                            ?><li class="b-tab-content__i article" data-target-id="<?=$liked['ID']?>">
                                <div class="b-tab-content__i-avatar" onclick="window.open('/articles/item/<?=$liked['ID']?>');">
                                    <div class="b-tab-content__i-avatar_cover"><?php
                                        if(!empty($liked['ARTICLES_IMAGE']))
                                        {
                                            ?><a href="/medicines/item/<?=$liked['ID']?>"><img src="<?=$liked['ARTICLES_IMAGE']?>"/></a><?php
                                        }
                                        else
                                        {
                                            ?><a href="/medicines/item/<?=$liked['ID']?>"><img src="/images/bg/def-ava.png" style="margin-top:10%;width:30%;"/></a><?php
                                        }
                                    ?></div>
                                    <input type="hidden" class="_hidden-id" value="<?=$liked['ID']?>"/>
                                    <span class="b-tab-content__i-delete" onclick="pUp_m.eGetId(this,'Удалить из избранного?')" data-pup-id="pUp-del-liked-article"></span>
                                </div>
                                <div class="b-tab-content__i-name"><?=$liked['NAME']?></div>
<!--                                <div class="b-tab-content__i-about">--><?//=$liked['TEXT']?><!--</div>-->
                             </li><?php
                        }?><li class="b-tab-content__i b-tab-content__i_add-new" onclick="window.open('/articles/catalog','_blank');">
                             <div class="b-tab-content__i-avatar">
                            </div>
                            <div class="b-tab-content__i-name">Добавить</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->