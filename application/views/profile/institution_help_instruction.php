<!--  END - MAIN MENU -->
<div class="content box-branches js-b-tgl" style="max-width: 1000px;">

	<? if ($user_type == 2 || $user_type == 3) { ?>
		<span class="title title-box institution-help-instruction">Уважаемый Пользователь!</span>

		<span class="title title-box institution-help-instruction">Благодарим за использование сервисов Медико-Фармацевтического портала!</span>

		<span class="title title-box institution-help-instruction">Как использовать Личный кабинет?</span>

		<span class="title title-box institution-help-instruction">Личный кабинет Пользователя предназначен для управления профилем организации или специалиста, в том числе его публичной частью на портале.</span>



		<span class="title title-box institution-help-instruction">ПЕРСОНАЛЬНЫЕ ДАННЫЕ Пользователя предназначены для идентификации Пользователя в системе и не транслируются третьим лицам. Заполняя персональные данные в Личном кабинете профиля организации или специалиста, Пользователь получает статус официального Администратора организации или специалиста на портале, а так же обеспечивает исполнение всех правил использования сервисов Медико-Фармацевтического портала.</span>

		<span class="title title-box institution-help-instruction">Портрет в разделе ПЕРСОНАЛЬНЫЕ ДАННЫЕ не является публичным и не доступен Посетителям портала.</span>



		<span class="title title-box institution-help-instruction">Раздел ОБ УЧРЕЖДЕНИИ предназначен для заполнения профиля организации или специалиста. Рекомендуем обязательно загрузить фотографию или логотип организации, так как это повышает эффективность взаимодействия Посетителей портала с организацией или специалистом. Качественное заполнение данных в разделе ОБ УЧРЕЖДЕНИИ обеспечивает вывод информации об организации или специалисте для потенциальных клиентов. За достоверность и актуальность данных об учреждении или специалисте отвечает Администратор личного кабинета. Рекомендуем своевременно обновлять информацию в случае ее изменения.</span>



		<span class="title title-box institution-help-instruction">Раздел СООБЩЕНИЯ предназначен для получения информации от администрации Портала, переписки с организациями и специалистами, клиентами и Посетителями портала.</span>



		<span class="title title-box institution-help-instruction">Раздел КОНТЕНТ, предназначен для размещения фото, видео и публикаций организации или специалиста. Администратор личного кабинета самостоятельно принимает решение по изменению статуса контента на «публичный», доступный Посетителям портала, в публичной части портала, в карточке организации или специалиста. Размещение достаточного количества фото, видео материалов и публикаций повышает интерес к организации или специалисту со стороны Посетителей портала и является дополнительной рекламой товаров или услуг.</span>



		<span class="title title-box institution-help-instruction">Раздел ОТДЕЛЕНИЯ предназначен для заведения профиля отделения организации. В данном случае один Администратор может управлять профилями нескольких отделений (филиалов) организации или иного объединения.</span>



		<span class="title title-box institution-help-instruction">Раздел СПЕЦИАЛИСТЫ предназначен для заведения специалистов организации, с целью информирования Посетителей профиля организации о специалистах организации и записи на прием.</span>



		<span class="title title-box institution-help-instruction">Раздел КЛИЕНТЫ содержит информацию о пользователях портала, которые проявили интерес к организации или специалисту, товарам или услугам и упрощает связь с клиентом.</span>



		<span class="title title-box institution-help-instruction">Раздел АКЦИИ предназначен для заведения специальных предложений организации или специалиста для Посетителей портала.</span>



		<span class="title title-box institution-help-instruction">Раздел ОТЗЫВЫ предназначен для работы с отзывами Посетителей портала об организации или специалисте. Администратор личного кабинета самостоятельно принимает решение о присвоении статуса «публичный» для отзыва.</span>



		<span class="title title-box institution-help-instruction">Раздел ОПЛАТА СЕРВИСА предназначен для управления сервисами портала.</span>

		<span class="title title-box institution-help-instruction">В данном разделе можно ознакомиться с условиями предоставления услуг портала, и сформировать счет на их оплату. Реквизиты организации, заполненные в профиле организации или специалиста можно загрузить в счет или заполнить реквизиты в счете самостоятельно.</span>



		<span class="title title-box institution-help-instruction">Где можно изучить действующие правила использования сервисов Медико-Фармацевтического портала?</span>



		<span class="title title-box institution-help-instruction">Действующие правила использования сервисов Медико-Фармацевтического портала описаны в Пользовательском соглашении, Договоре оферте, Правилах размещения информации на портале и Правилах заведения торговых наименований на портале и размещены на сайте:</span>

		<span class="title title-box institution-help-instruction">www.zdorovie.online/info/about</span>

		<span class="title title-box institution-help-instruction">www.medfarmportal.ru/info/about</span>

		<span class="title title-box institution-help-instruction">www.медфармпортал.рф/info/about</span>



		<span class="title title-box institution-help-instruction">При регистрации на Медико-Фармацевтическом портале Пользователь знакомится с Пользовательским соглашением и принимает его условия. Оплата счета за услуги портала означает принятие условий Договора оферты.</span>



		<span class="title title-box institution-help-instruction">Какие требования к изображениям, логотипу организации?</span>

		<span class="title title-box institution-help-instruction">Основные форматы изображений: gif, jpg, png</span>
		<span class="title title-box institution-help-instruction">Максимальный размер изображения 512кб</span>
		<span class="title title-box institution-help-instruction">Максимальная ширина изображения: 1024</span>
		<span class="title title-box institution-help-instruction">Максимальная высота изображения: 768</span>
		<span class="title title-box institution-help-instruction">Желательное соотношение сторон 4:3</span>



		<span class="title title-box institution-help-instruction">Как загрузить прайс-лист организации на портал?</span>



		<span class="title title-box institution-help-instruction">Для загрузки прайс-листа организации или специалиста необходимо:</span>

		<span class="title title-box institution-help-instruction">Зарегистрироваться на портале как врач, специалист или мед. учреждение, организация.</span>

		<span class="title title-box institution-help-instruction">Отправить запрос на размещение прайс-листа, а разделе СООБЩЕНИЯ в меню НАПИСАТЬ АДМИНИСТРАТОРУ вверху справа.</span>

		<span class="title title-box institution-help-instruction">Администратор портала отправит информацию c адресом FTP и инструкцией по загрузке файлов прайс-листа.</span>

		<span class="title title-box institution-help-instruction">Следуя инструкции, настроить загрузку файлов с прайс-листом на FTP.</span>



		<span class="title title-box institution-help-instruction">Можно воспользоваться помощью системного администратора портала, написав об этом администратору портала.</span>





		<span class="title title-box institution-help-instruction">Как можно связаться со специалистами Медико-Фармацевтического портала?</span>



		<span class="title title-box institution-help-instruction">Со специалистами Медико-Фармацевтического портала можно связаться любым удобным способом:</span>

		<span class="title title-box institution-help-instruction">В Личном кабинете в разделе СООБЩЕНИЯ выбрать меню НАПИСАТЬ АДМИНИСТРАТОРУ в верхнем правом углу Личного кабинета.</span>

		<span class="title title-box institution-help-instruction">Отправить почтовую корреспонденцию: 115035, г. Москва, ул. Пятницкая, дом 6/1 стр . 8</span>

		<span class="title title-box institution-help-instruction">Позвонить на горячую линию: 8 800 555-31-91</span>

		<span class="title title-box institution-help-instruction">Написать на эл. Почту:</span>

		<span class="title title-box institution-help-instruction">по техническим вопросам help@medfarmportal.ru</span>

		<span class="title title-box institution-help-instruction">реклама, сотрудничество market@medfarmportal.ru</span>
	<? } elseif ($user_type == 1) { ?>
		<span class="title title-box institution-help-instruction">Уважаемый Пользователь!</span>

		<span class="title title-box institution-help-instruction">Благодарим за использование сервисов Медико-Фармацевтического портала!</span>

		<span class="title title-box institution-help-instruction">Как использовать Личный кабинет?</span>

		<span class="title title-box institution-help-instruction">Личный кабинет Пользователя предназначен для удобства решения вопросов связанных с хранением полезной информации с портала, а так же общения со специалистами и организациями.</span>



		<span class="title title-box institution-help-instruction">ПЕРСОНАЛЬНЫЕ ДАННЫЕ Пользователя предназначены для идентификации Пользователя в системе и не транслируются третьим лицам.</span>



		<span class="title title-box institution-help-instruction">Раздел СООБЩЕНИЯ предназначен для получения информации от администрации Портала, переписки с организациями и специалистами.</span>



		<span class="title title-box institution-help-instruction">Раздел ИЗБРАННОЕ, предназначен для хранения информации об организациях, специалистах, товарах, публикациях, отмеченных пользователем как избранное.</span>

		<span class="title title-box institution-help-instruction">В любой момент можно убрать статус избранное с организации, специалиста, товара, публикации, если она перестала быть актуальной.</span>



		<span class="title title-box institution-help-instruction">Раздел МЕДКАРТА предназначен для заполнения актуальной информации о состоянии здоровья и будет полезен при оперативном обращении в лечебно-профилактическое учреждение или к специалисту. Пользователь оставляет на свое усмотрение заполнение данного раздела. Содержание МЕДКАРТЫ можно отправить специалисту или в лечебно- профилактическое учреждение используя раздел СООБЩЕНИЯ отметив галочкой меню ПРИКРЕПИТЬ МЕДКАРТУ К ПИСЬМУ внизу отправляемого сообщения.</span>



		<span class="title title-box institution-help-instruction">Раздел ЦЕЛИ предназначен для управления личными целями. Пользователь оставляет на свое усмотрение заполнение данного раздела.</span>



		<span class="title title-box institution-help-instruction">Раздел ГРАФИК предназначен для визуализации графиков решения целей и задач, связанных со здоровьем и не только. В раздел ГРАФИК попадают все цели, заведенные Пользователем в разделе ЦЕЛИ.</span>



		<span class="title title-box institution-help-instruction">Где можно изучить действующие правила использования сервисов Медико-Фармацевтического портала?</span>



		<span class="title title-box institution-help-instruction">Действующие правила использования сервисов Медико-Фармацевтического портала описаны в Пользовательском соглашении и размещены на сайте:</span>

		<span class="title title-box institution-help-instruction">www.zdorovie.online/info/about</span>

		<span class="title title-box institution-help-instruction">www.medfarmportal.ru/info/about</span>

		<span class="title title-box institution-help-instruction">www.медфармпортал.рф/info/about</span>

		<span class="title title-box institution-help-instruction">При регистрации на Медико-Фармацевтическом портале Пользователь знакомится с Пользовательским соглашением и принимает его условия.</span>



		<span class="title title-box institution-help-instruction">Как можно связаться со специалистами Медико-Фармацевтического портала?</span>



		<span class="title title-box institution-help-instruction">Со специалистами Медико-Фармацевтического портала можно связаться любым удобным способом:</span>

		<span class="title title-box institution-help-instruction">В Личном кабинете в разделе СООБЩЕНИЯ выбрать меню НАПИСАТЬ АДМИНИСТРАТОРУ в верхнем правом углу Личного кабинета.</span>

		<span class="title title-box institution-help-instruction">Отправить почтовую корреспонденцию: 115035, г. Москва, ул. Пятницкая, дом 6/1 стр . 8</span>

		<span class="title title-box institution-help-instruction">Позвонить на горячую линию: 8 800 555-31-91</span>

		<span class="title title-box institution-help-instruction">Написать на эл. Почту:</span>

		<span class="title title-box institution-help-instruction">по техническим вопросам help@medfarmportal.ru</span>

		<span class="title title-box institution-help-instruction">реклама, сотрудничество market@medfarmportal.ru</span>
	<? } ?>
</div>
<!--  RIGHT - PANEL -->
<div class="right-panel">
	<div class="b-RP-buttons">
		<a link="#" class="btn-lk btn-lk_RP btn-lk_RP_pref">Настройки</a>
		<a href="/profile/institutionHelpInstruction" class="btn-lk btn-lk_RP btn-lk_RP_help">Помощь</a>
		<a href="/profile/post/admin" class="btn-lk btn-lk_RP btn-lk_RP_help" style="height:50px;">Написать администратору</a>
	</div>
	<div class="title title_DEF b-RP-search-adr">Адресная книга<span class="b-RP-search-adr__p"></span></div>
	<div class="box-fields input_text box-fields_search b-RP-search">
		<input onkeyup="return mfp.searchAddress(this)" placeholder="Искать адресата" class="box-fields__input box-fields__input_search" type="text"/>
	</div>
	<div class="b-RP-adress-list"><?=$addressBook?></div>
</div>
<!--  END RIGHT - PANEL -->