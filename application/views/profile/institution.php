<!--  END - MAIN MENU -->
<div class="content">
    <span class="title title-box">ОБ УЧРЕЖДЕНИИ</span>
    <div class="box box_company">
        <form action="/profile/changeInstitution" onsubmit="return mfp.ajaxLoader(this)">
            <div class="box box_avar js-avatar" style="margin-top: 40px;margin-bottom: 40px;">
                <div class="title title-box">Логотип</div>
                <div class="box_avar__image js-avatar__img js-avatar_init <?php if(!empty($institution['LOGO'])) echo 'notEmpty'?>">
                    <img src="<?=$institution['LOGO']?>"/>
                    <input type="hidden" class="_path-input" value="<?=$institution['LOGO']?>"/>
                    <span class="b-add-f-list__i-delete" data-pup-id="pUp-del-user-ava" onclick="pUp_m.eGetId(this,'Удалить текущий логотип?')" <?php if(empty($institution['LOGO'])) echo 'style="display:none;"'?>></span>
                </div>
                <div class="box_avar__inf">
                    <div class="box_avar__inf-alert">Постарайтесь, чтобы размер картинки не превышал 2 Мб и картинка была в одном из следующих форматов - jpg, png, gif.</div>
                    <input type="submit" value="ВЫБРАТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour js-avatar_init" onclick="return false">
                </div>
                <input type="file" name="ava" class="js-avatar__form-i dnone" accept="image/*"/>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридическое название</span>
                    <input type="text" value="<?=str_replace('"','\'',$institution['NAME'])?>" class="box-fields__input" name="shortname"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text b-filter-btn__list-i specialist">
                    <span class="box-fields__label">Тип учреждения</span>
                    <select name="type-id" class="b-filter-btn__select js-select specialist" data-smart-positioning="false">
                        <option value="Без типа">Без типа</option><?php
                        foreach($types as $type)
                        {
                            ?><option value="<?=$type['ID']?>" <?php if($institution['TYPE_ID']==$type['ID']) echo 'selected' ?>><?=$type['NAME']?></option><?php
                        }
                    ?></select>
                </div>
                <div class="box-fields input_text b-filter-btn__list-i specialist" style="margin-left: 315px;">
                    <span class="box-fields__label">Категория</span>
                    <select name="category-id" class="b-filter-btn__select js-select specialist right" data-smart-positioning="false">
                        <option value="Выберите...">Выберите...</option>
                        <option value="2" <?php if($institution['CATEGORY_ID']==2) echo 'selected' ?>>Мед. учреждения</option>
                        <option value="4" <?php if($institution['CATEGORY_ID']==4) echo 'selected' ?>>Аптеки</option>
                        <option value="5" <?php if($institution['CATEGORY_ID']==5) echo 'selected' ?>>Красота и здоровье</option>
                        <option value="7" <?php if($institution['CATEGORY_ID']==7) echo 'selected' ?>>Мама и ребенок</option>
                        <option value="8" <?php if($institution['CATEGORY_ID']==8) echo 'selected' ?>>Реабилитация</option>
                        <option value="9" <?php if($institution['CATEGORY_ID']==9) echo 'selected' ?>>Ветеринария</option>
                    </select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Бренд</span>
                    <input type="text" value="<?=str_replace('"','\'',$institution['SHORTNAME'])?>" class="box-fields__input" name="name"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Адрес эл. почты</span>
                    <input type="text" value="<?=$institution['EMAIL']?>" class="box-fields__input" name="email"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Лицензия №</span>
                    <input type="text" value="<?=$institution['LICENSE']?>" class="box-fields__input" name="license"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Описание</span>
                    <textarea class="box-fields__input box-fields__text-area" name="description"><?=$institution['DESCRIPTION']?></textarea>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: индекс</span>
                    <input type="text" value="<?=$institution['LEGAL_ADDRESS_INDEX']?>" class="box-fields__input" name="legal_address_index"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: индекс</span>
                    <input type="text" value="<?=$institution['ACTUAL_ADDRESS_INDEX']?>" class="box-fields__input" name="actual_address_index"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: область/регион</span>
                    <input type="text" value="<?=$institution['LEGAL_ADDRESS_REGION']?>" class="box-fields__input" name="legal_address_region"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: область/регион</span>
                    <input type="text" value="<?=$institution['ACTUAL_ADDRESS_REGION']?>" class="box-fields__input" name="actual_address_region"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: город/населенный пункт</span>
                    <input type="text" value="<?=$institution['LEGAL_ADDRESS_CITY']?>" class="box-fields__input" name="legal_address_city"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: город/населенный пункт</span>
                    <input type="text" value="<?=$institution['ACTUAL_ADDRESS_CITY']?>" class="box-fields__input" name="actual_address_city"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: улица/переулок</span>
                    <input type="text" value="<?=$institution['LEGAL_ADDRESS_STREET']?>" class="box-fields__input" name="legal_address_street"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: улица/переулок</span>
                    <input type="text" value="<?=$institution['ACTUAL_ADDRESS_STREET']?>" class="box-fields__input" name="actual_address_street"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: дом/здание</span>
                    <input type="text" value="<?=$institution['LEGAL_ADDRESS_HOUSE']?>" class="box-fields__input" name="legal_address_house"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: дом/здание</span>
                    <input type="text" value="<?=$institution['ACTUAL_ADDRESS_HOUSE']?>" class="box-fields__input" name="actual_address_house"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: корпус/строение</span>
                    <input type="text" value="<?=$institution['LEGAL_ADDRESS_BUILDING']?>" class="box-fields__input" name="legal_address_building"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: корпус/строение</span>
                    <input type="text" value="<?=$institution['ACTUAL_ADDRESS_BUILDING']?>" class="box-fields__input" name="actual_address_building"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: квартира/офис</span>
                    <input type="text" value="<?=$institution['LEGAL_ADDRESS_OFFICE']?>" class="box-fields__input" name="legal_address_office"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: квартира/офис</span>
                    <input type="text" value="<?=$institution['ACTUAL_ADDRESS_OFFICE']?>" class="box-fields__input" name="actual_address_office"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text b-filter-btn__list-i specialist">
                    <span class="box-fields__label">Метро</span>
                    <select name="metro_id[]" multiple size="4" class="b-filter-btn__select  specialist" data-smart-positioning="false">
                        <option value="Выберите..." >Выберите...</option><?php
                        foreach($metros as $metro)
                        {
                            ?><option value="<?=$metro['ID']?>" <?=(!empty($current_metros) && !empty($current_metros[$metro['ID']])) ? "selected" : ""?>><?=$metro['NAME']?></option><?php
                        }
                        ?></select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Широта</span>
                    <input type="text" value="<?=!empty($institution['LATITUDE']) ? $institution['LATITUDE'] : "55.73519720952004" ?>" type="text" name="latitude" class="box-fields__input js-map js-map-lat"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Долгота</span>
                    <input type="text" value="<?=!empty($institution['LONGITUDE']) ? $institution['LONGITUDE'] : "37.637177346771104" ?>" type="text" name="longitude" class="box-fields__input js-map js-map-lon"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">На карте:</span>
                    <div class="box-fields__map-reg js-map js-map-box" id="b-map"></div>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон</span>
                    <input type="text" value="<?=$institution['PHONE']?>" class="box-fields__input js-tel" name="phone"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОГРН</span>
                    <input type="text" value="<?=$institution['OGRN']?>" class="box-fields__input" name="ogrn"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ИНН</span>
                    <input type="text" value="<?=$institution['INN']?>" class="box-fields__input" name="inn"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">КПП</span>
                    <input type="text" value="<?=$institution['KPP']?>" class="box-fields__input" name="kpp"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКВЭД</span>
                    <input type="text" value="<?=$institution['OKVED']?>" class="box-fields__input" name="okved"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКПО</span>
                    <input type="text" value="<?=$institution['OKPO']?>" class="box-fields__input" name="okpo"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Расчетный счет</span>
                    <input type="text" value="<?=$institution['ACCOUNT']?>" class="box-fields__input" name="account"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Банк</span>
                    <input type="text" value="<?=str_replace('"','\'',$institution['BANK'])?>" class="box-fields__input" name="bank"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">БИК</span>
                    <input type="text" value="<?=$institution['BIK']?>" class="box-fields__input" name="bik"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКТМО</span>
                    <input type="text" value="<?=$institution['OKTMO']?>" class="box-fields__input" name="oktmo"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Главный бухгалтер</span>
                    <input type="text" value="<?=$institution['CHIEF_ACCOUNTANT']?>" class="box-fields__input" name="chief_accountant"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Главный врач / заведующий / администратор</span>
                    <input type="text" value="<?=$institution['CHIEF_PHYSICIAN']?>" class="box-fields__input" name="chief_physician"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Генеральный директор</span>
                    <input type="text" value="<?=$institution['CEO']?>" class="box-fields__input" name="ceo"/>
                </div>
            </div>
            <div class="clear"></div>

            <span class="title title-box title-box_inner">Контактные данные</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 1</span>
                    <input type="text" value="<?=$institution['PHONE1']?>" class="box-fields__input phone_mask" name="phone1"/>
                </div>

                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 1</span>
                    <input type="text" value="<?=$institution['WEBSITE1']?>" class="box-fields__input" name="website1"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 2</span>
                    <input type="text" value="<?=$institution['PHONE2']?>" class="box-fields__input phone_mask" name="phone2"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 2</span>
                    <input type="text" value="<?=$institution['WEBSITE2']?>" class="box-fields__input" name="website2"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 3</span>
                    <input type="text" value="<?=$institution['PHONE3']?>" class="box-fields__input phone_mask" name="phone3"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 3</span>
                    <input type="text" value="<?=$institution['WEBSITE3']?>" class="box-fields__input" name="website3"/>
                </div>
            </div>

            <span class="title title-box title-box_inner">Рабочее время</span>
            <div class="box-branches_add">
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПН)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['MONDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="monday-work-from"/>
                            <input type="text" value="<?=$institution['MONDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="monday-work-to"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['TUESDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="tuesday-work-from"/>
                            <input type="text" value="<?=$institution['TUESDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="tuesday-work-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СР)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['WEDNESDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="wednesday-work-from">
                            <input type="text" value="<?=$institution['WEDNESDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="wednesday-work-to">
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ЧТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['THURSDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="thursday-work-from"/>
                            <input type="text" value="<?=$institution['THURSDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="thursday-work-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['FRIDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="friday-work-from"/>
                            <input type="text" value="<?=$institution['FRIDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="friday-work-to"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СБ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['SATURDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="saturday-work-from"/>
                            <input type="text" value="<?=$institution['SATURDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="saturday-work-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВС)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['SUNDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="sunday-work-from"/>
                            <input type="text" value="<?=$institution['SUNDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="sunday-work-to"/>
                        </div>
                    </div>
                </div>
            </div>

            <span class="title title-box title-box_inner">Перерыв</span>
            <div class="box-branches_add">
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ПН)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['MONDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="monday-break-from"/>
                            <input type="text" value="<?=$institution['MONDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="monday-break-to"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ВТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['TUESDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="tuesday-break-from"/>
                            <input type="text" value="<?=$institution['TUESDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="tuesday-break-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (СР)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['WEDNESDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="wednesday-break-from">
                            <input type="text" value="<?=$institution['WEDNESDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="wednesday-break-to">
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ЧТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['THURSDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="thursday-break-from"/>
                            <input type="text" value="<?=$institution['THURSDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="thursday-break-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ПТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['FRIDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="friday-break-from"/>
                            <input type="text" value="<?=$institution['FRIDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="friday-break-to"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (СБ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['SATURDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="saturday-break-from"/>
                            <input type="text" value="<?=$institution['SATURDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="saturday-break-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ВС)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$institution['SUNDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="sunday-break-from"/>
                            <input type="text" value="<?=$institution['SUNDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="sunday-break-to"/>
                        </div>
                    </div>
                </div>
            </div>

            <!--  BLOCK - ADD FILES -->
            <div class="box b-add-f">
                <span class="title title-box">Сканы лицензий и файлы</span>
                <p class="paragraph paragraph_prew">Постарайтесь, чтобы размер файла не превышал <span class="b-add-f__limit">20 Мб</span> и файл был в одном из следующих форматов - jpg, png, gif, tif, pdf, docx, doc, rtf, txt, ppt, pptx, xls, xlsx, odf.</p>
                <div class="b-add-f-reg js-add-files">
                	<ul class="b-add-f-list js-add-list"><?php
                        foreach($files as $file)
                        {
                    		?><li class="b-add-f-list__i js-add-li" data-img-path="<?=$file['FILE']?>">
                                <img src="<?=$file['FILE']?>"/>
                                <input type="hidden" class="_hidden-id" value="<?=$file['FILE']?>"/>
                                <span class="b-add-f-list__i-delete jd-add-f_delete" data-pup-id="pUp-del-document"></span>
                            </li><?php
                        }
                	    ?><li class="b-add-f-list__i b-add-f-list__i_add-new js-add-f"></li>
                    </ul>
                </div>
            </div>
            <!--  END - BLOCK - ADD FILES -->

            <div>
                <ul class="list-two">
                    <li><input type="checkbox" class="checkbox" id="input_1" value="true" name="state"/><label for="input_1">Государственная</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_2" value="true" name="private"/><label for="input_2">Частная</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_3" value="true" name="children"/><label for="input_3">Детское отделение</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_4" value="true" name="ambulance"/><label for="input_4">Скорая</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_5" value="true" name="house"/><label for="input_5">Выезд на дом</label></li>
                </ul>

                <br>

                <ul class="list-two">
                    <li><input type="checkbox" class="checkbox" id="input_1" value="true" name="booking"/><label for="input_1">Бронирование</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_2" value="true" name="delivery"/><label for="input_2">С доставкой</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_3" value="true" name="daynight"/><label for="input_3">Круглосуточно</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_4" value="true" name="dms"/><label for="input_4">ДМС</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_5" value="true" name="dlo"/><label for="input_5">ДЛО</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_6" value="true" name="optics"/><label for="input_6">Отдел оптики</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_7" value="true" name="rpo"/><label for="input_7">Рецептурно-производственный отдел</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_6" value="true" name="homeopathy"/><label for="input_6">Отдел гомеопатии</label></li>
                </ul>
            </div>

            <div class="clear"></div>
            <input type="submit" value="Сохранить изменения" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
        </form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->