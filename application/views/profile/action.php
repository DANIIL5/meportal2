<!--  END - MAIN MENU -->
<div class="content"> 
    <div class="box box_med-card">
        <form action="/profile/save_action" method="post">
            <? if (!empty($action)) {?><input type="hidden" name="id" value="<?=$action['ID']?>"><? } ?>
            <span class="title title-box">Акция</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Название</span>
                    <input type="text" value="<?=(!empty($action['NAME']) ? $action['NAME'] : "")?>" class="box-fields__input" name="name"/>
                </div>
                <div class="box-fields input_text">
                    <label class="b-filter-btn__select js-select" for="">Акция опубликована<input type="checkbox" <?=(!empty($action['PUBLIC']) ? "checked" : "")?> value="on" class="box-fields__input" name="public"/></label>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Категории размещения</span>
                    <select name="category_id" class="b-filter-btn__select js-select specialist" name="metro_id" data-smart-positioning="false">
                        <option value="">Все категории</option>
                        <? foreach ($categories as $category) { ?>
                            <option value="<?=$category['ID']?>" <? if (!empty($action['CATEGORY_ID']) && $action['CATEGORY_ID'] == $category['ID']) { ?>selected<? } ?>><?=$category['NAME']?></option>
                        <? } ?>
                    </select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text js-date_box">
                    <span class="box-fields__label">Дата начала акции</span>
                    <input type="text" name="start_date" class="box-fields__input box-fields__input_date js-date-i future" value="<?=(!empty($action['START_DATE'])) ? date('d.m.Y', strtotime($action['START_DATE'])) : "" ?>"/>
                    <span class="box-fields__date-init js-date-e"></span>
                </div>
                <div class="box-fields input_text js-date_box">
                    <span class="box-fields__label">Дата окончания акции</span>
                    <input type="text" name="start" class="box-fields__input box-fields__input_date js-date-i future" value="<?=(!empty($action['END_DATE'])) ? date('d.m.Y', strtotime($action['END_DATE'])) : "" ?>" name="end_date"/>
                    <span class="box-fields__date-init js-date-e"></span>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Адрес акции</span>
                    <input type="text" value="<?=(!empty($action['ADDRESS']) ? $action['ADDRESS'] : "")?>" class="box-fields__input" name="address"/>
                </div>
                 <div class="box-fields input_text">
                     <span class="box-fields__label">Метро</span>
                     <select name="metro_id" class="b-filter-btn__select js-select specialist" name="metro_id" data-smart-positioning="false">
                         <option value="">Не выбрано</option>
                         <? foreach ($metro as $station) { ?>
                             <option value="<?=$station['ID']?>" <? if (!empty($action['METRO_ID']) && $action['METRO_ID'] == $station['ID']) { ?>selected<? } ?>><?=$station['NAME']?></option>
                         <? } ?>
                     </select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Изображение</span>
                    <input type="file" value="" class="box-fields__input" name="image"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Цена</span>
                    <input type="text" value="<?=(!empty($action['PRICE']) ? $action['PRICE'] : "")?>" class="box-fields__input" name="price"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Описание</span>
                    <textarea class="box-fields__input box-fields__text-area" name="description"><?=(!empty($action['DESCRIPTION']) ? $action['DESCRIPTION'] : "")?></textarea>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Текст</span>
                    <textarea class="box-fields__input box-fields__text-area" name="text"><?=(!empty($action['TEXT']) ? $action['TEXT'] : "")?></textarea>
                </div>
            </div>
            <!--  END - BLOCK - ADD FILES -->
            <div class="clear"></div>
            <input type="submit" value="Сохранить изменения" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
        </form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->