<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/register.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header" class="short">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>


                <div class="registry dotted">Регистратура</div>

                <div class="btn">Портал для профессионалов</div>
            </div>


        </div>
        <div class="slider_top flexslider">
            <div class="slide"><img class="slider_img" src="/temp/slide-2.jpg" alt=""></div>
            <div class="slide"><img class="slider_img" src="/temp/slide-1.jpg" alt=""></div>
            <div class="slide"><img class="slider_img" src="/temp/slide-2.jpg" alt=""></div>
        </div>
    </header>



    <div id="content">

        <div class="container">


            <div class="registration">
                <div class="heading">
                    <h1>Регистрация</h1>
                    <span>Выберите подходящую для Вас категорию</span>
                </div>
                <div class="content">
                    <ul class="choose_type">
                        <li>
                            <img src="/images/chat-pic-2.png" alt="">
                            <center>
                                <a href="/profile/registration/user" class="submit btn_white">Частный пользователь</a>
                            </center>
                        </li>
                        <li>
                            <img src="/images/chat-pic-1.png" alt="">
                            <center>
                                <a href="/profile/registration/doctor" class="submit btn_white">Врач, специалист</a>
                            </center>
                        </li>
                        <li>
                            <img src="/images/chat-pic-3.png" alt="">
                            <center>
                                <a href="/profile/registration/company" class="submit btn_white">Мед.учреждение, компания</a>
                            </center>
                        </li>
                    </ul>
                </div>
            </div>



            <div class="three_cols">




            </div>

        </div>

    </div>

    <footer id="footer">
        <div class="top">
            <div class="container menu">
                <ul class="list">
                    <li><a href="#">Мед. учреждения</a></li>
                    <li><a href="#">Врачи</a></li>
                    <li class="selected"><a href="#">Аптеки</a></li>
                    <li><a href="#">Зож</a></li>
                    <li><a href="#">Красота</a></li>
                    <li><a href="#">Справочники</a></li>
                    <li><a href="#">Мама и ребенок</a></li>
                    <li><a href="#">Реабилитация</a></li>
                    <li><a href="#">Ветеренария</a></li>
                </ul>
            </div>
        </div>

        <div class="bottom">
            <div class="container">
                <div class="left_col">
                    <div class="logo_footer"><a href="#"><img src="/images/logo-footer.png" alt="Медико Фармацевтический портал"/></a></div>
                    <ul class="social">
                        <li class="fb"><a href="#">fb</a></li>
                        <li class="tw"><a href="#">twitter</a></li>
                        <li class="ball"><a href="#">fb</a></li>
                        <li class="video"><a href="#">video</a></li>
                        <li class="google"><a href="#">google</a></li>
                    </ul>
                </div>


                <div class="center_col">
                    <a class="tel" href="tel:88003003232"><span>8 800</span> 555-31-91</a>
                    <p class="copy">Распространение материалов только с согласия редакции. Все права защищены. &copy; 2015 <br/>Эл. почта для связи — <a href="mailto:hello@helpmed.com">hello@helpmed.com</a>.</p>
                </div>

                <div class="right_col">
                    <ul class="submenu">
                        <li><a href="#">Контактная информация</a></li>
                        <li><a href="#">О портале</a></li>
                        <li><a href="#">Обратная связь</a></li>
                    </ul>
                    <div class="subscribe">
                        <span class="georgia title">Подписка</span>
                        <div class="forma">
                            <form action="#">
                                <input type="text" class="text" placeholder="hello@helpmed.com"/>
                                <span class="button"><input type="submit" class="submit" value="Подписаться"></span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>




<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/script.js"></script>

</body>
</html>