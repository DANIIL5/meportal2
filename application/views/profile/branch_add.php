<!--  END - MAIN MENU -->
<div class="content">
    <span class="title title-box">ОБ УЧРЕЖДЕНИИ</span>
    <div class="box box_company">
        <form action="/profile/organisation_save" method="post">
            <div class="box box_avar js-avatar" style="margin-top: 40px;margin-bottom: 40px;">
                <div class="title title-box">Логотип</div>
                <div class="box_avar__image js-avatar__img js-avatar_init">
                    <img src=""/>
                    <input type="hidden" class="_path-input" value=""/>
                    <span class="b-add-f-list__i-delete" data-pup-id="pUp-del-user-ava" onclick="pUp_m.eGetId(this,'Удалить текущий логотип?')" ></span>
                </div>
                <div class="box_avar__inf">
                    <div class="box_avar__inf-alert">Постарайтесь, чтобы размер картинки не превышал 2 Мб и картинка была в одном из следующих форматов - jpg, png, gif.</div>
                    <input type="submit" value="ВЫБРАТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour js-avatar_init" onclick="return false">
                </div>
                <input type="file" name="ava" class="js-avatar__form-i dnone" accept="image/*"/>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридическое название</span>
                    <input type="text" value="" class="box-fields__input" name="entity"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text b-filter-btn__list-i specialist">
                    <span class="box-fields__label">Тип учреждения</span>
                    <select name="type_id" class="b-filter-btn__select js-select specialist" data-smart-positioning="false">
                        <option value="Без типа">Без типа</option><?php
                        foreach($types as $type)
                        {
                            ?><option value="<?=$type['ID']?>" ><?=$type['NAME']?></option><?php
                        }
                    ?></select>
                </div>
                <div class="box-fields input_text b-filter-btn__list-i specialist" style="margin-left: 315px;">
                    <span class="box-fields__label">Категория</span>
                    <select name="category_id" class="b-filter-btn__select js-select specialist right" data-smart-positioning="false">
                        <option value="Выберите...">Выберите...</option>
                        <option value="2">Мед. учреждения</option>
                        <option value="4">Аптеки</option>
                        <option value="5">Красота и здоровье</option>
                        <option value="7">Мама и ребенок</option>
                        <option value="8">Реабилитация</option>
                        <option value="9">Ветеринария</option>
                    </select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Бренд</span>
                    <input type="text" value="" class="box-fields__input" name="brand"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Лицензия №</span>
                    <input type="text" value="" class="box-fields__input" name="license"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Описание</span>
                    <textarea class="box-fields__input box-fields__text-area" name="description"></textarea>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: индекс</span>
                    <input type="text" value="" class="box-fields__input" name="address[legal][index]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: индекс</span>
                    <input type="text" value="" class="box-fields__input" name="address[actual][index]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: область/регион</span>
                    <input type="text" value="" class="box-fields__input" name="address[legal][region]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: область/регион</span>
                    <input type="text" value="" class="box-fields__input" name="address[actual][region]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: город/населенный пункт</span>
                    <input type="text" value="" class="box-fields__input" name="address[legal][city]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: город/населенный пункт</span>
                    <input type="text" value="" class="box-fields__input" name="address[actual][city]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: улица/переулок</span>
                    <input type="text" value="" class="box-fields__input" name="address[legal][street]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: улица/переулок</span>
                    <input type="text" value="" class="box-fields__input" name="address[actual][street]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: дом/здание</span>
                    <input type="text" value="" class="box-fields__input" name="address[legal][house]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: дом/здание</span>
                    <input type="text" value="" class="box-fields__input" name="address[actual][house]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: корпус/строение</span>
                    <input type="text" value="" class="box-fields__input" name="address[legal][building]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: корпус/строение</span>
                    <input type="text" value="" class="box-fields__input" name="address[actual][building]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: квартира/офис</span>
                    <input type="text" value="" class="box-fields__input" name="address[legal][office]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: квартира/офис</span>
                    <input type="text" value="" class="box-fields__input" name="address[actual][office]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text b-filter-btn__list-i specialist">
                    <span class="box-fields__label">Метро</span>
                    <select name="metro_id[]" multiple size="4" class="b-filter-btn__select  specialist" data-smart-positioning="false">
                        <option value="Выберите..." >Выберите...</option><?php
                        foreach($metros as $metro)
                        {
                            ?><option value="<?=$metro['ID']?>"><?=$metro['NAME']?></option><?php
                        }
                        ?></select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Широта</span>
                    <input type="text" value="" type="text" name="latitude" class="box-fields__input js-map js-map-lat"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Долгота</span>
                    <input type="text" value="" type="text" name="longitude" class="box-fields__input js-map js-map-lon"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">На карте:</span>
                    <div class="box-fields__map-reg js-map js-map-box" id="b-map"></div>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОГРН</span>
                    <input type="text" value="" class="box-fields__input" name="ogrn"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКТМО</span>
                    <input type="text" value="" class="box-fields__input" name="oktmo"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ИНН</span>
                    <input type="text" value="" class="box-fields__input" name="inn"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">КПП</span>
                    <input type="text" value="" class="box-fields__input" name="kpp"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКВЭД</span>
                    <input type="text" value="" class="box-fields__input" name="okved"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКПО</span>
                    <input type="text" value="" class="box-fields__input" name="okpo"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Расчетный счет</span>
                    <input type="text" value="" class="box-fields__input" name="checking_account"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Банк</span>
                    <input type="text" value="" class="box-fields__input" name="bank"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">БИК</span>
                    <input type="text" value="" class="box-fields__input" name="bik"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Главный врач / заведующий / администратор</span>
                    <input type="text" value="" class="box-fields__input" name="administrator"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Главный бухгалтер</span>
                    <input type="text" value="" class="box-fields__input" name="chief_accountant"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Генеральный директор</span>
                    <input type="text" value="" class="box-fields__input" name="ceo"/>
                </div>
            </div>
            <div class="clear"></div>

            <span class="title title-box title-box_inner">Контактные данные</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 1</span>
                    <input type="text" value="" class="box-fields__input phone_mask" name="phone1"/>
                </div>

                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 1</span>
                    <input type="text" value="" class="box-fields__input" name="website1"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 2</span>
                    <input type="text" value="" class="box-fields__input phone_mask" name="phone2"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 2</span>
                    <input type="text" value="" class="box-fields__input" name="website2"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 3</span>
                    <input type="text" value="" class="box-fields__input phone_mask" name="phone3"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 3</span>
                    <input type="text" value="" class="box-fields__input" name="website3"/>
                </div>
            </div>

            <span class="title title-box title-box_inner">Рабочее время</span>
            <div class="box-branches_add">
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПН)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[1][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[1][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[2][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[2][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СР)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[3][from]">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[3][to]">
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ЧТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[4][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[4][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[5][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[5][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СБ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[6][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[6][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВС)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[7][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="work[7][to]"/>
                        </div>
                    </div>
                </div>
            </div>

            <span class="title title-box title-box_inner">Перерыв</span>
            <div class="box-branches_add">
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ПН)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[1][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[1][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ВТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[2][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[2][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (СР)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[3][from]">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[3][to]">
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ЧТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[4][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[4][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ПТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[5][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[5][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (СБ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[6][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[6][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ВС)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[7][from]"/>
                            <input type="text" value="" class="box-fields__input js-time-input" name="break[7][to]"/>
                        </div>
                    </div>
                </div>
            </div>

            <!--  BLOCK - ADD FILES -->
            <div class="box b-add-f">
                <span class="title title-box">Сканы лицензий и файлы</span>
                <p class="paragraph paragraph_prew">Постарайтесь, чтобы размер файла не превышал <span class="b-add-f__limit">20 Мб</span> и файл был в одном из следующих форматов - jpg, png, gif, tif, pdf, docx, doc, rtf, txt, ppt, pptx, xls, xlsx, odf.</p>
                <div class="b-add-f-reg js-add-files">
                	<ul class="b-add-f-list js-add-list"><?php
                        foreach($files as $file)
                        {
                    		?><li class="b-add-f-list__i js-add-li" data-img-path="<?=$file['FILE']?>">
                                <img src="<?=$file['FILE']?>"/>
                                <input type="hidden" class="_hidden-id" value="<?=$file['FILE']?>"/>
                                <span class="b-add-f-list__i-delete jd-add-f_delete" data-pup-id="pUp-del-document"></span>
                            </li><?php
                        }
                	    ?><li class="b-add-f-list__i b-add-f-list__i_add-new js-add-f"></li>
                    </ul>
                </div>
            </div>
            <!--  END - BLOCK - ADD FILES -->

            <div>
                <ul class="list-two">
                    <li><input type="checkbox" class="checkbox" id="input_1" value="on" name="f_state"/><label for="input_1">Государственная</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_2" value="on" name="f_private"/><label for="input_2">Частная</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_3" value="on" name="f_children"/><label for="input_3">Детское отделение</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_4" value="on" name="f_ambulance"/><label for="input_4">Скорая</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_5" value="on" name="f_house"/><label for="input_5">Выезд на дом</label></li>
                </ul>

                <br>

                <ul class="list-two">
                    <li><input type="checkbox" class="checkbox" id="input_6" value="on" name="f_booking"/><label for="input_6">Бронирование</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_7" value="on" name="f_delivery"/><label for="input_7">С доставкой</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_8" value="on" name="f_daynight"/><label for="input_8">Круглосуточно</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_9" value="on" name="f_dms"/><label for="input_9">ДМС</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_10" value="on" name="f_dlo"/><label for="input_10">ДЛО</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_11" value="on" name="f_optics"/><label for="input_11">Отдел оптики</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_12" value="on" name=f_rpo"/><label for="input_12">Рецептурно-производственный отдел</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_13" value="on" name=f_homeopathy"/><label for="input_13">Отдел гомеопатии</label></li>
                </ul>
            </div>

            <input type="hidden" name="new" value="1">
            <div class="clear"></div>
            <input type="submit" value="Сохранить изменения" name="submit" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
        </form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->