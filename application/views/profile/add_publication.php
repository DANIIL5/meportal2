<!--  END - MAIN MENU -->
<div class="content box-branches js-b-tgl">
        <span class="title title-box title-box_inner">Контент: добавление публикации</span>

        <div>
            <form action="/profile/save_publication<?=(!empty($publication['ID'])) ? '/'.$publication['ID'] : ""?>" method="post" enctype="multipart/form-data">
                <input type="text" name="name" placeholder="Название публикации" value="<?=(!empty($publication['NAME'])) ? htmlentities($publication['NAME']) : ""?>"><br><br>
                <textarea name="text" id="" cols="30" rows="10" placeholder="Текст публикации"><?=(!empty($publication['TEXT'])) ? htmlentities($publication['TEXT']) : ""?></textarea><br><br>
                <lable>Публиковать?<input type="checkbox" name="public" value="on" <?=(!empty($publication['PUBLIC'])) ? 'checked' : ""?>></lable>
                <input type="submit" name="save" value="Сохранить">
            </form>
        </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->