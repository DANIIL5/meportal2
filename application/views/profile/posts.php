<!--  END - MAIN MENU -->
<div class="content"> 
	<div class="box box_mail">
		<div class="b-filter-btn">
			<ul class="b-filter-btn__list">
				<li class="b-filter-btn__list-i">
					<input value="НАПИСАТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour" type="submit" onclick="window.location.pathname='/profile/post'"/>
				</li>
				<li class="b-filter-btn__list-i">
					<input value="ОБНОВИТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour" type="submit" onclick="location.reload()"/>
				</li>
				<li class="b-filter-btn__list-i">
					<select name="filter-mail" class="b-filter-btn__select js-select" onchange="mfp.filterMail(this)" data-smart-positioning="false">
					  <option <?php if($category=='all') echo 'selected'?> value="all">Все письма</option>
					  <option <?php if($category=='in') echo 'selected'?> value="in">Входящие</option>
					  <option <?php if($category=='out') echo 'selected'?> value="out">Исходящие</option>
					  <option <?php if($category=='liked') echo 'selected'?> value="liked">Важные</option>
					  <option <?php if($category=='delete') echo 'selected'?> value="delete">Корзина</option>
					</select>
				</li>
				<li class="b-filter-btn__list-i">   
					<div class="box-fields__input_search_cover">
						<form action="/profile/ajaxSearchPosts/<?=$category?>" method="GET">
							<input placeholder="Искать среди сообщений" class="box-fields__input box-fields__input_search post-search" type="text" name="get-search" <?php if(!empty($getSearch)) echo 'value="'.$getSearch.'"' ?> />
						</form>
					</div>
				</li>
			</ul>
		</div>
		<div class="box-msg">
			<div class="b-msg"><?php
				if(empty($posts))
				{
					?><div>По данному запросу нет писем.</div><?php
				}
				else
				{
					foreach ($posts as $post)
					{
						?><div class="b-msg__row js-mail <?php if($post['DESTINATION_READED']!=1) echo 'b-msg__row_new-msg' ?>" data-target-id="<?=$post['ID']?>">
							<div class="b-msg__row_inner b-msg__row_inner_prew js-mail__opener">
								<div class="b-msg__td b-msg__td_fav">
									<input class="js-checkbox" type="checkbox">
								</div>
								<div class="b-msg__td b-msg__td_title"><?=$post['DESTINATION_NAME']?></div>
								<div class="b-msg__td b-msg__td_content"><span class="b-msg__td_content-title"><?=$post['THEME']?></span></div>
								<div class="b-msg__td b-msg__td_date"><?=$post['TIME']?></div>
								<div class="b-msg__td b-msg__td_date"><?=$post['DATE']?></div>
							</div>
							<div class="b-msg__row_inner b-msg__row_inner_content js-mail__content">
								<span class="b-msg__row_inner_content__close js-mail__close"></span>
								<div class="b-msg__row_inner_content_main-info">
									<div class="b-msg__td b-msg__td_fav">
										<input class="js-checkbox" type="checkbox">
									</div>
									<div class="b-msg__td b-msg__td_title"><?=$post['DESTINATION_NAME']?></div>
									<div class="b-msg__td b-msg__td_content">
										<ul class="b-msg-operation">
											<li class="b-msg-operation__i b-msg-operation__i_rep" onclick="window.location.pathname='/profile/resendPost/<?=$post['ID']?>'">Переслать</li>
											<li class="b-msg-operation__i b-msg-operation__i_rew" onclick="window.location.pathname='/profile/respondPost/<?=$post['ID']?>/<?=$post['ID_DESTINATION']?>'">Ответить</li><?php
											if($category != 'liked' and $category != 'delete')
											{
												?><li class="b-msg-operation__i b-msg-operation__i_del" onclick="return mfp.targetPostLikedDelete(this,'/profile/deleteLikedPost',false)" style="display:none;">Из важных</li>
												<li class="b-msg-operation__i b-msg-operation__i_rep" onclick="return mfp.targetPostLiked(this,'/profile/likedPost')">В важные</li><?php
											}
											elseif($category != 'delete')
											{
												?><li class="b-msg-operation__i b-msg-operation__i_del" onclick="return mfp.targetPostLikedDelete(this,'/profile/deleteLikedPost',true)">Из важных</li><?php
											}
											?><input type="hidden" class="_hidden-id" value="<?=$post['ID']?>"/><?php
											if($category=='delete')
											{
												?><li class="b-msg-operation__i b-msg-operation__i_del js-pUp__openeer" onclick="pUp_m.eGetId(this,'Вы действительно хотите безвозвратно удалить это письмо?')" data-pup-id="pUp-finaldel-post">Удалить</li>
												<li class="b-msg-operation__i b-msg-operation__i_del" onclick="return mfp.postRestore(this,'/profile/restorePost')">Восстановить</li><?php
											}
											else
											{
												?><li class="b-msg-operation__i b-msg-operation__i_del js-pUp__openeer" onclick="pUp_m.eGetId(this,'Вы действительно хотите отправить это письмо в корзину?')" data-pup-id="pUp-del-post">Удалить</li><?php   
											}
										?></ul>
									</div>
									<div class="b-msg__td b-msg__td_date"><span class="b-msg__td_date-time"><?=date('H:i:s',strtotime($post['TIME']))?></span>&nbsp;<?=date('d.m.Y',strtotime($post['TIME']))?></div>
								</div>
								<div class="b-msg__row_inner_content_info">
									<span class="title title-msg"><?=$post['THEME']?></span>
									<p class="paragraph"><?=$post['MESSAGE']?></p>
									<? if ($post['ATACH_MED_CARD'] == 1) {?><p class="paragraph">Пользователь прикрепил <a href="<?="https://mfp.rg3.su/profile/getUserMedCard/".$post['ID']?>">мед.карту</a></p><? } ?>
									<ul class="b-msg-img-list"><?php
										foreach($post['FILES'] as $file)
										{
											?><li class="b-msg-img-list__i">
													<div  class="b-msg-img-list__img">
														<img src="<?=$file['FILE']?>"/>
													</div>
													<span  class="b-msg-img-list__title"><?=strrchr($file['FILE'],'/')?></span>
													<span  class="b-msg-img-list__size">612 Kb</span>
												</li><?php
										}
									?></ul>
								</div><?php
								foreach ($post['RESPONDEDS'] as $respond)
								{
									?><div class="b-msg__row_inner_content_reward">
										<ul class="b-reward-adr">
											<li class="b-reward-adr__i b-reward-adr__i_autor"><?=$respond['NAME_SENDER']?> писал:</li>
											<li class="b-reward-adr__i b-reward-adr__i_date">
												<span><?=$respond['TIME']?></span>&nbsp;<?=$respond['DATE']?></li>
										</ul>
										<div class="b-reward-content">
											<span class="title-msg b-reward-content__title"><?=$respond['THEME']?></span>
											<p class="paragraph"><?=$respond['MESSAGE']?></p>
										</div>
									</div><?php
								}
							?></div>
						</div><?php
					}
				}
			?></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!--  RIGHT - PANEL -->
<div class="right-panel">
	<div class="b-RP-buttons">
<!--		<a link="#" class="btn-lk btn-lk_RP btn-lk_RP_pref">Настройки</a>-->
		<a href="/profile/institutionHelpInstruction" class="btn-lk btn-lk_RP btn-lk_RP_help">Помощь</a>
		<a href="/profile/post/admin" class="btn-lk btn-lk_RP btn-lk_RP_help" style="height:50px;">Написать администратору</a>
	</div>
	<div class="title title_DEF b-RP-search-adr">Адресная книга<!--<span class="b-RP-search-adr__p"></span>--></div>
	<div class="box-fields input_text box-fields_search b-RP-search">
		<input onkeyup="return mfp.searchAddress(this)" placeholder="Искать адресата" class="box-fields__input box-fields__input_search" type="text"/>
	</div>
	<div class="b-RP-adress-list"><?=$addressBook?></div>
</div>
<!--  END RIGHT - PANEL -->