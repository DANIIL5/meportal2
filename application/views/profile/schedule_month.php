<div class="b-calend-reg js-calend-reg" data-type-calend="2">
    <ul class="b-calend" id="js-slider-bx">
        <li class="b-calend__stage" data-monthi="1">
            <ul class="b-calend__days"><?php
                $weekI=0;
                $FirstWeekDayI=date("w", mktime(0, 0, 0, date("m"), 1, date("Y")));
                for ($i=0; $i<42; $i++)
                {
                    ?><li class="b-calend__days_i" data-weeki="<?=$weekI?>" data-dayi="<?=$i?>">
                        <div class="b-calend__date <?php if(($weekI+1)==date("w") and ($i+2-$FirstWeekDayI)==date("d")) echo 'b-calend__date_now' ?>">
                            <span class="b-calend__date_numb"><?=date("d/m", mktime(0, 0, 0, date("m"), $i+2-$FirstWeekDayI, date("Y")))?></span>
                            <span class="b-calend__date_dw" style="display:inline-block;"><?=$weekLetters[$weekI]?></span>
                        </div><?php
                        foreach ($targetsCur as $target)
                        {
                            if($target['DATE']>=date("d/m/Y", mktime(0, 0, 0, date("m"), $i+2-$FirstWeekDayI, date("Y")))
                               and $target['DATE']<=date("d/m/Y", mktime(23, 59, 59, date("m"), $i+2-$FirstWeekDayI, date("Y"))))
                                {
                                    ?><div class="b-calend__event js-clndr-b">
                                        <span class="b-calend__event_prew js-clndr-e"><?=$target['NAME']?></span>
                                        <input type="hidden" class="target-name" value="<?=$target['NAME']?>"/>
                                        <input type="hidden" class="target-time" value="<?=$target['TIME']?>"/>
                                        <input type="hidden" class="target-date" value="<?=$target['DATE']?>"/>
                                        <input type="hidden" class="target-text" value="<?=$target['TEXT']?>"/>
                                    </div><?php
                                }
                        }
                    ?></li><?php
                    if($weekI==6)
                        $weekI=0;
                    else
                        $weekI=$weekI+1;
                }
            ?></ul>                            
        </li>
        <li class="b-calend__stage" data-monthi="2">
            <ul class="b-calend__days"><?php
                $weekI=0;
                $FirstWeekDayI=date("w", mktime(0, 0, 0, date("m")+1, 1, date("Y")));
                for ($i=0; $i<42; $i++)
                {
                    ?><li class="b-calend__days_i" data-weeki="<?=$weekI?>" data-dayi="<?=$i?>">
                        <div class="b-calend__date">
                            <span class="b-calend__date_numb"><?=date("d/m", mktime(0, 0, 0, date("m")+1, $i+2-$FirstWeekDayI, date("Y")))?></span>
                            <span class="b-calend__date_dw" style="display:inline-block;"><?=$weekLetters[$weekI]?></span>
                        </div><?php
                        foreach ($targetsNext as $target)
                        {
                            if($target['DATE']>=date("d/m/Y", mktime(0, 0, 0, date("m")+1, $i+2-$FirstWeekDayI, date("Y")))
                               and $target['DATE']<=date("d/m/Y", mktime(0, 0, 0, date("m")+1, $i+2-$FirstWeekDayI, date("Y"))))
                                {
                                    ?><div class="b-calend__event js-clndr-b">
                                        <span class="b-calend__event_prew js-clndr-e"><?=$target['NAME']?></span>
                                        <input type="hidden" class="target-name" value="<?=$target['NAME']?>"/>
                                        <input type="hidden" class="target-time" value="<?=$target['TIME']?>"/>
                                        <input type="hidden" class="target-date" value="<?=$target['DATE']?>"/>
                                        <input type="hidden" class="target-text" value="<?=$target['TEXT']?>"/>
                                    </div><?php
                                }
                        }
                    ?></li><?php
                    if($weekI==6)
                        $weekI=0;
                    else
                        $weekI=$weekI+1;
                }
            ?></ul>                            
        </li>
    </ul>