<!--  END - MAIN MENU -->
<div class="content"> 
    <div class="box box-aim">
        <input value="Новая цель" class="btn-lk btn-lk_aim" type="button" onclick="window.location.pathname='/profile/target'"/>
        <div class="box-msg">
            <div class="b-msg"><?php
                if(empty($targets))
                {
                    ?><div>Вы пока не выбрали цель.</div><?php
                }
                else
                {
                    foreach ($targets as $target)
                    {
                        ?><div class="b-msg__row js-mail" data-target-id="<?=$target['ID']?>">
                            <div class="b-msg__row_inner b-msg__row_inner_prew js-mail__opener">
                                <div class="b-msg__td b-msg__td_fav">
                                    <input class="js-checkbox _liked" type="checkbox"/>
                                </div>
                                <div class="b-msg__td b-msg__td_title"><?=$target['NAME']?></div>
                                <div class="b-msg__td b-msg__td_content"><span class="b-msg__td_content-title"><?=$target['TEXT']?></span></div>
                                <div class="b-msg__td b-msg__td_date"><?=(!empty($target['START']) ? date('d/m/Y', strtotime($target['START'])) : "")?> <b><?=(!empty($target['START']) ? date('H:i', strtotime($target['START'])) : "")?></b></div>
                                <div class="b-msg__td b-msg__td_date"><?=(!empty($target['TIME']) ? date('d/m/Y', strtotime($target['TIME'])) : "")?> <b><?=(!empty($target['TIME']) ? date('H:i', strtotime($target['TIME'])) : "")?></b></div>
                            </div>
                            <div class="b-msg__row_inner b-msg__row_inner_content js-mail__content">
                                <span class="b-msg__row_inner_content__close js-mail__close"></span>
                                <div class="b-msg__row_inner_content_main-info">
                                    <div class="b-msg__td b-msg__td_fav">
                                        <input class="js-checkbox" type="checkbox">
                                    </div>
                                    <div class="b-msg__td b-msg__td_title"><?=$target['NAME']?></div>
                                    <div class="b-msg__td b-msg__td_content">
                                        <ul class="b-msg-operation">
                                            <li class="b-msg-operation__i b-msg-operation__i_rep" onclick="window.location.pathname='/profile/editTarget/<?=$target['ID']?>'">Редактировать</li><?php
                                            if($liked!='liked')
                                            {
                                                ?><li class="b-msg-operation__i b-msg-operation__i_del" onclick="return mfp.targetPostLikedDelete(this,'/profile/deleteLikedTarget',false)" style="display:none;">Удалить из избранного</li>
                                                <li class="b-msg-operation__i b-msg-operation__i_rep" onclick="return mfp.targetPostLiked(this,'/profile/likedTarget')">Добавить в избранное</li><?php
                                            }
                                            else
                                            {
                                                ?><li class="b-msg-operation__i b-msg-operation__i_del" onclick="return mfp.targetPostLikedDelete(this,'/profile/deleteLikedTarget',true)">Удалить из избранного</li><?php
                                            }
                                            ?><input type="hidden" class="_hidden-id" value="<?=$target['ID']?>"/>
                                            <li class="b-msg-operation__i b-msg-operation__i_del js-pUp__openeer" onclick="pUp_m.eGetId(this,'Вы действительно хотите безвозвратно удалить эту цель?')" data-pup-id="pUp-del-target">Удалить</li>
                                        </ul>
                                    </div>
                                    <div class="b-msg__td b-msg__td_date"> <?=(!empty($target['START']) ? date('d/m/Y', strtotime($target['START'])) : "")?><b><?=(!empty($target['START']) ? date('H:i', strtotime($target['START'])) : "")?></b></div>
                                    <div class="b-msg__td b-msg__td_date">&nbsp;<?=(!empty($target['TIME']) ? date('d/m/Y', strtotime($target['TIME'])) : "")?><b><?=(!empty($target['START']) ? date('H:i', strtotime($target['TIME'])) : "")?></b></div>
                                </div>
                                <div class="b-msg__row_inner_content_info">
                                    <span class="title title-msg" style="width:870px"><?=$target['NAME']?></span>
                                    <p class="paragraph"><?=$target['TEXT']?></p>
                                </div>
                            </div>
                        </div><?php
                    }
                }
            ?></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!--  FOOTER -->