<!--  END - MAIN MENU -->
<div class="content box-branches js-b-tgl">
	<div class="box-branches_add">
    	<form action="/profile/updateBranch" onsubmit="return mfp.ajaxLoader(this)" enctype="multipart/form-data">
	        <span class="title title-box title-box_inner">Редактировать отделение</span>

	        <div class="box-fields-row box-fields-row_cell-2">
	            <div class="box-fields input_text">
	                <span class="box-fields__label">Название</span>
	                <input type="text" value="<?=str_replace('"','\'',$branch['NAME'])?>" class="box-fields__input" name="name"/>
	            </div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Адрес эл. почты</span>
					<input type="text" value="<?=$branch['EMAIL']?>" class="box-fields__input" name="email"/>
				</div>
	        </div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Юридический адрес: индекс</span>
					<input type="text" value="<?=$branch['LEGAL_ADDRESS_INDEX']?>" class="box-fields__input" name="legal_address_index"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Фактический адрес: индекс</span>
					<input type="text" value="<?=$branch['ADDRESS_INDEX']?>" class="box-fields__input" name="actual_address_index"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Юридический адрес: область/регион</span>
					<input type="text" value="<?=$branch['LEGAL_ADDRESS_REGION']?>" class="box-fields__input" name="legal_address_region"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Фактический адрес: область/регион</span>
					<input type="text" value="<?=$branch['ADDRESS_REGION']?>" class="box-fields__input" name="actual_address_region"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Юридический адрес: город/населенный пункт</span>
					<input type="text" value="<?=$branch['LEGAL_ADDRESS_CITY']?>" class="box-fields__input" name="legal_address_city"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Фактический адрес: город/населенный пункт</span>
					<input type="text" value="<?=$branch['ADDRESS_CITY']?>" class="box-fields__input" name="actual_address_city"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Юридический адрес: улица/переулок</span>
					<input type="text" value="<?=$branch['LEGAL_ADDRESS_STREET']?>" class="box-fields__input" name="legal_address_street"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Фактический адрес: улица/переулок</span>
					<input type="text" value="<?=$branch['ADDRESS_STREET']?>" class="box-fields__input" name="actual_address_street"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Юридический адрес: дом/здание</span>
					<input type="text" value="<?=$branch['LEGAL_ADDRESS_HOUSE']?>" class="box-fields__input" name="legal_address_house"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Фактический адрес: дом/здание</span>
					<input type="text" value="<?=$branch['ADDRESS_HOUSE']?>" class="box-fields__input" name="actual_address_house"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Юридический адрес: корпус/строение</span>
					<input type="text" value="<?=$branch['LEGAL_ADDRESS_BUILDING']?>" class="box-fields__input" name="legal_address_building"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Фактический адрес: корпус/строение</span>
					<input type="text" value="<?=$branch['ADDRESS_BUILDING']?>" class="box-fields__input" name="actual_address_building"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Юридический адрес: квартира/офис</span>
					<input type="text" value="<?=$branch['LEGAL_ADDRESS_OFFICE']?>" class="box-fields__input" name="legal_address_office"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Фактический адрес: квартира/офис</span>
					<input type="text" value="<?=$branch['ADDRESS_OFFICE']?>" class="box-fields__input" name="actual_address_office"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Телефон 1</span>
					<input type="text" value="<?=$branch['PHONE1']?>" class="box-fields__input" name="phone1"/>
				</div>

				<div class="box-fields input_text">
					<span class="box-fields__label">Веб-сайт 1</span>
					<input type="text" value="<?=$branch['WEBSITE1']?>" class="box-fields__input" name="website1"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Телефон 2</span>
					<input type="text" value="<?=$branch['PHONE2']?>" class="box-fields__input" name="phone2"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Веб-сайт 2</span>
					<input type="text" value="<?=$branch['WEBSITE2']?>" class="box-fields__input" name="website2"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Телефон 3</span>
					<input type="text" value="<?=$branch['PHONE3']?>" class="box-fields__input" name="phone3"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Веб-сайт 3</span>
					<input type="text" value="<?=$branch['WEBSITE3']?>" class="box-fields__input" name="website3"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text b-filter-btn__list-i specialist">
					<span class="box-fields__label">Метро</span>
					<select name="metro_id[]" multiple size="4" class="b-filter-btn__select  specialist" data-smart-positioning="false">
						<option value="Выберите..." >Выберите...</option><?php
						foreach($metros as $metro)
						{
							?><option value="<?=$metro['ID']?>" <?=(!empty($current_metros) && !empty($current_metros[$metro['ID']])) ? "selected" : ""?>><?=$metro['NAME']?></option><?php
						}
						?></select>
				</div>
			</div>
	        <div class="box-fields-row box-fields-row_cell-2 dnone">
	            <div class="box-fields input_text">
	                <span class="box-fields__label">Широта</span>
	                <input type="text" value="<?=$branch['LATITUDE']?>" type="text" name="latitude" class="box-fields__input js-map js-map-lat"/>
	            </div>
	            <div class="box-fields input_text">
	                <span class="box-fields__label">Долгота</span>
	                <input type="text" value="<?=$branch['LONGITUDE']?>" type="text" name="longitude" class="box-fields__input js-map js-map-lon"/>
	            </div>
	        </div>
	        <div class="box-fields-row">
	            <div class="box-fields input_text">
	                <span class="box-fields__label">На карте:</span>
	                <div class="box-fields__map-reg js-map js-map-box" id="b-map"></div>
	            </div>
	        </div>

			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">ОГРН</span>
					<input type="text" value="<?=$branch['OGRN']?>" class="box-fields__input" name="ogrn"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">ИНН</span>
					<input type="text" value="<?=$branch['INN']?>" class="box-fields__input" name="inn"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">КПП</span>
					<input type="text" value="<?=$branch['KPP']?>" class="box-fields__input" name="kpp"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">ОКВЭД</span>
					<input type="text" value="<?=$branch['OKVED']?>" class="box-fields__input" name="okved"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">ОКПО</span>
					<input type="text" value="<?=$branch['OKPO']?>" class="box-fields__input" name="okpo"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Расчетный счет</span>
					<input type="text" value="<?=$branch['ACCOUNT']?>" class="box-fields__input" name="account"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Банк</span>
					<input type="text" value="<?=str_replace('"','\'',$branch['BANK'])?>" class="box-fields__input" name="bank"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">БИК</span>
					<input type="text" value="<?=$branch['BIK']?>" class="box-fields__input" name="bik"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">ОКТМО</span>
					<input type="text" value="<?=$branch['OKTMO']?>" class="box-fields__input" name="oktmo"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Главный бухгалтер</span>
					<input type="text" value="<?=$branch['CHIEF_ACCOUNTANT']?>" class="box-fields__input" name="chief_accountant"/>
				</div>
				<div class="box-fields input_text">
					<span class="box-fields__label">Главный врач / заведующий / администратор</span>
					<input type="text" value="<?=$branch['CHIEF_PHYSICIAN']?>" class="box-fields__input" name="chief_physician"/>
				</div>
			</div>
			<div class="box-fields-row box-fields-row_cell-2">
				<div class="box-fields input_text">
					<span class="box-fields__label">Генеральный директор</span>
					<input type="text" value="<?=$branch['CEO']?>" class="box-fields__input" name="ceo"/>
				</div>
			</div>

			<span class="title title-box title-box_inner">Рабочее время</span>
			<div class="box-branches_add">
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Рабочее время (ПН)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['MONDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="monday-work-from"/>
							<input type="text" value="<?=$branch['MONDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="monday-work-to"/>
						</div>
					</div>
					<div class="box-fields input_text">
						<span class="box-fields__label">Рабочее время (ВТ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['TUESDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="tuesday-work-from"/>
							<input type="text" value="<?=$branch['TUESDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="tuesday-work-to"/>
						</div>
					</div>
				</div>
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Рабочее время (СР)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['WEDNESDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="wednesday-work-from">
							<input type="text" value="<?=$branch['WEDNESDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="wednesday-work-to">
						</div>
					</div>
					<div class="box-fields input_text">
						<span class="box-fields__label">Рабочее время (ЧТ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['THURSDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="thursday-work-from"/>
							<input type="text" value="<?=$branch['THURSDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="thursday-work-to"/>
						</div>
					</div>
				</div>
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Рабочее время (ПТ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['FRIDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="friday-work-from"/>
							<input type="text" value="<?=$branch['FRIDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="friday-work-to"/>
						</div>
					</div>
					<div class="box-fields input_text">
						<span class="box-fields__label">Рабочее время (СБ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['SATURDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="saturday-work-from"/>
							<input type="text" value="<?=$branch['SATURDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="saturday-work-to"/>
						</div>
					</div>
				</div>
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Рабочее время (ВС)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['SUNDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="sunday-work-from"/>
							<input type="text" value="<?=$branch['SUNDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="sunday-work-to"/>
						</div>
					</div>
				</div>
			</div>



			<span class="title title-box title-box_inner">Перерыв</span>
			<div class="box-branches_add">
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Время перерыва (ПН)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['MONDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="monday-break-from"/>
							<input type="text" value="<?=$branch['MONDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="monday-break-to"/>
						</div>
					</div>
					<div class="box-fields input_text">
						<span class="box-fields__label">Время перерыва (ВТ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['TUESDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="tuesday-break-from"/>
							<input type="text" value="<?=$branch['TUESDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="tuesday-break-to"/>
						</div>
					</div>
				</div>
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Время перерыва (СР)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['WEDNESDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="wednesday-break-from">
							<input type="text" value="<?=$branch['WEDNESDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="wednesday-break-to">
						</div>
					</div>
					<div class="box-fields input_text">
						<span class="box-fields__label">Время перерыва (ЧТ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['THURSDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="thursday-break-from"/>
							<input type="text" value="<?=$branch['THURSDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="thursday-break-to"/>
						</div>
					</div>
				</div>
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Время перерыва (ПТ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['FRIDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="friday-break-from"/>
							<input type="text" value="<?=$branch['FRIDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="friday-break-to"/>
						</div>
					</div>
					<div class="box-fields input_text">
						<span class="box-fields__label">Время перерыва (СБ)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['SATURDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="saturday-break-from"/>
							<input type="text" value="<?=$branch['SATURDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="saturday-break-to"/>
						</div>
					</div>
				</div>
				<div class="box-fields-row box-fields-row_cell-2">
					<div class="box-fields input_text">
						<span class="box-fields__label">Время перерыва (ВС)</span>
						<div class="box-fields__input_cell-2">
							<input type="text" value="<?=$branch['SUNDAY_BREAK_FROM']?>" class="box-fields__input js-time-input" name="sunday-break-from"/>
							<input type="text" value="<?=$branch['SUNDAY_BREAK_TO']?>" class="box-fields__input js-time-input" name="sunday-break-to"/>
						</div>
					</div>
				</div>
			</div>

			<ul class="list-two">
				<li><input type="checkbox" class="checkbox" id="input_1" value="true" name="state"/><label for="input_1">Государственная</label></li>
				<li><input type="checkbox" class="checkbox" id="input_2" value="true" name="private"/><label for="input_2">Частная</label></li>
				<li><input type="checkbox" class="checkbox" id="input_3" value="true" name="children"/><label for="input_3">Детское отделение</label></li>
				<li><input type="checkbox" class="checkbox" id="input_4" value="true" name="ambulance"/><label for="input_4">Скорая</label></li>
				<li><input type="checkbox" class="checkbox" id="input_5" value="true" name="house"/><label for="input_5">Выезд на дом</label></li>
			</ul>

			<br>

			<ul class="list-two">
				<li><input type="checkbox" class="checkbox" id="input_1" value="true" name="booking"/><label for="input_1">Бронирование</label></li>
				<li><input type="checkbox" class="checkbox" id="input_2" value="true" name="delivery"/><label for="input_2">С доставкой</label></li>
				<li><input type="checkbox" class="checkbox" id="input_3" value="true" name="daynight"/><label for="input_3">Круглосуточно</label></li>
				<li><input type="checkbox" class="checkbox" id="input_4" value="true" name="dms"/><label for="input_4">ДМС</label></li>
				<li><input type="checkbox" class="checkbox" id="input_5" value="true" name="dlo"/><label for="input_5">ДЛО</label></li>
				<li><input type="checkbox" class="checkbox" id="input_6" value="true" name="optics"/><label for="input_6">Отдел оптики</label></li>
				<li><input type="checkbox" class="checkbox" id="input_7" value="true" name="rpo"/><label for="input_7">Рецептурно-производственный отдел</label></li>
				<li><input type="checkbox" class="checkbox" id="input_6" value="true" name="homeopathy"/><label for="input_6">Отдел гомеопатии</label></li>
			</ul>

	        <div class="clear"></div>
	        <input type="hidden" class="_hidden-id" value="<?=$branch['ID']?>" name="_hidden-id"/>
	        <input type="submit" value="Редактировать отделение" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
		</form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->