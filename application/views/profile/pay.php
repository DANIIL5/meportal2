        <!--  END - MAIN MENU -->
        <div class="content js-pUp__openeer">
            <div class="b-table-price">
                <div class="b-table-price__period">
                    <div class="title title-box">Прайс-лист</div>
                    <span>действителен с 1 августа 2016 года</span>
                </div>
                <div class="b-table-price__info">
                    МЕДИКО-ФАРМАЦЕВТИЧЕСКИЙ ПОРТАЛ — комплексное решение персональных и коммерческих задач
                </div>
                <div class="b-table-price__alert">
                    Создайте информационно-рекламную площадку Вашей компании на МЕДИКО-ФАРМАЦЕВТИЧЕСКОМ ПОРТАЛЕ и получите новых клиентов и партнеров!
                </div>
                <div class="b-table-price__tbody b-table b-table_pay">
                    <table>
                        <thead>
                            <tr>
                                <td>Услуга</td>
                                <td>Техническое описание</td>
                                <td class="b-table_pay__hasInner" colspan="2">
                                  <div class="b-table_pay__inner">
                                    <table>
                                      <tbody>
                                          <tr>
                                            <td> Период</td>
                                            <td>Стоимость</td>
                                            <td colspan="3">
                                              <span class="b-table__major-i" style="color:#c44848;">Специальное предложение</span>
                                              для новых компаний<br>действует до 31 августа 2016г
                                            </td>
                                          </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody><?php
                        foreach($pays as $pay)
                            {
                              ?><tr class="_service_pay_tr">
                                  <td class="_name_cell"><?=$pay['NAME']?></td>
                                  <td><?=$pay['DESCRIPTION']?></td>
                                  <td class="b-table_pay__hasInner">
                                    <div class="b-table_pay__inner">
                                      <table>
                                        <tbody><?php
                                            foreach($pay['prices'] as $price)
                                            {
                                                ?><tr class="_service_pay_price_tr">
                                                  <td class="_months_cell"><?=$price['MONTHS']?></td>
                                                  <td class="_price_cell"><?=$price['PRICE']?> руб.</td>
                                                  <td><span class="b-table__major-i-dib _special_price_cell"><?=$price['SPEC_PRICE']?></span>руб.</td>
                                                  <td>
                                                    <a class="def-link" data-pup-id="pUp-view-ORDER" onclick="pUp_m.eGetId(this,0)" data-pup-id="pUp-view-ORDER">ПОДКЛЮЧИТЬ</a>
                                                  </td>
                                                </tr><?php
                                            }
                                        ?></tbody>
                                      </table>
                                    </div>
                                  </td>
                                  <td>
                                    <div class="b-table_pay__b-faq js-t-pay-faq">
                                      <span class="b-table_pay__faq js-t-pay-faq__e">?</span>
                                      <div class="b-table_pay__faq-i js-t-pay-faq__i"><?=$pay['HELP']?></div>
                                    </div>
                                  </td> 
                              </tr><?php
                            }
                        ?></tbody>
                        <tfoot>
                          <tr>
                            <td colspan="4">
                              <ul class="b-table_pay__footer-contact">
                                <li>
                                  Индивидуальный подход и возможность размещения специальных проектов
                                </li>
                                <li>
                                  8-800-555-31-91 
                                  <span class="g-color_grey">&nbsp;Иванова Мария</span>
                                </li>
                                <li>
                                  Эл. почта для связи — 
                                  <a class="def-link" href="mailto:hello@helpmed.com">hello@helpmed.com</a>
                                </li>
                              </ul>
                            </td>
                          </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- <div class="b-table">
                  <table>
                    <tbody>
                      <tr></tr>
                    </tbody>
                  </table>
                </div> -->
            </div>
            <!-- <a href="#" class="btn-lk js-pUp__openeer" data-pup-id="pUp-save-thx"> Сохранить</a> -->
        </div>
        <!--  FOOTER -->