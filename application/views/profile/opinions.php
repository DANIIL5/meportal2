<!--  END - MAIN MENU -->
<div class="content"> 
    <div class="box box_new-msg" style="width: 760px;"><?php
        if($destination_email == 'Администратор')
        {
            ?><form action="/profile/sendPostToAdmin" onsubmit="return mfp.ajaxLoader(this)" width="760px"><?php
        }
        else
        {
            ?><form action="/profile/sendPost" onsubmit="return mfp.ajaxLoader(this)" width="760px"><?php
        }
            ?><span class="title title-box">новое соообщение</span>
              <div class="box-fields-row">
                <div class="b-input-add-contacts">
                     <div class="box-fields input_text box-fields_add-cont">
                            <span class="box-fields__label">Кому</span>
                            <input type="text" class="box-fields__input" name="destinations[]" value="<?=(!empty($destination_email) ? $destination_email : (!empty($_GET['email']) ? $_GET['email'] : "") )?>"/>
                            <span class="box-fields_add-cont__ico">+</span>
                        </div>
                     </div>
                </div>
             <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Тема</span>
                    <input type="text" class="box-fields__input" name="theme" value="<?=$post_theme?>"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Сообщение</span>
                    <textarea class="box-fields__input box-fields__input_new-target" name="message"><?=$post_message?></textarea>
                </div>
            </div>
        
            <div class="clear"></div>
            <!--  BLOCK - ADD FILES -->
      
            <!--  END - BLOCK - ADD FILES -->
            <div class="clear"></div><?php
            if(!empty($responded_post_id))
            {
                ?><input type="hidden" name="responded_post_id" value="<?=$responded_post_id?>"/><?php
            }
            ?>
          
            <input type="hidden"  name="idsender" value="<?=$idsender?>"/>
                <input type="submit" value="Отправить" class="btn-lk btn-lk_send js-pUp__openeer" data-pup-id="pUp-save-thx"/>
                
        </form>
    </div>
</div>
<!--  RIGHT - PANEL -->
<div class="right-panel">
    <div class="b-RP-buttons">
<!--        <a link="#" class="btn-lk btn-lk_RP btn-lk_RP_pref">Настройки</a>-->
           <a href="/profile/institutionHelpInstruction" class="btn-lk btn-lk_RP btn-lk_RP_help">Помощь</a>
        <a href="/profile/post/admin" class="btn-lk btn-lk_RP btn-lk_RP_help" style="height:50px;">Написать администратору</a>
    </div>
    <div class="title title_DEF b-RP-search-adr">Адресная книга<!-- <span class="b-RP-search-adr__p"></span> --></div>
    <div class="box-fields input_text box-fields_search b-RP-search">
        <input onkeyup="return mfp.searchAddress(this)" placeholder="Искать адресата" class="box-fields__input box-fields__input_search" type="text"/>
    </div>
    <div class="b-RP-adress-list"><?=$addressBook?></div>
</div>
<!--  END RIGHT - PANEL -->
<!--  FOOTER -->