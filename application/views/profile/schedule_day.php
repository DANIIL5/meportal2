<div class="b-calend-reg js-calend-reg" data-type-calend="1">
    <ul class="b-calend" id="js-slider-bx">
        <li class="b-calend__stage">
            <ul class="b-calend__one-days"><?php
                foreach($hours as $key=>$hourCur)
                {
                    ?><li class="b-calend__hours-row">
                        <span class="b-calend__hours-row_h"><?=$hourCur?></span><?php
                        foreach($targetsCur as $target)
                        {
                            if ($target['TIME']>=$hourCur and $target['TIME']<$hours[$key+1])
                            {
                                ?><div class="b-calend__event js-clndr-b">
                                    <span class="b-calend__event_prew js-clndr-e"><?=$target['NAME']?></span>
                                    <input type="hidden" class="target-name" value="<?=$target['NAME']?>"/>
                                    <input type="hidden" class="target-time" value="<?=$target['TIME']?>"/>
                                    <input type="hidden" class="target-date" value="<?=$target['DATE']?>"/>
                                    <input type="hidden" class="target-text" value="<?=$target['TEXT']?>"/>
                                </div><?php
                            }
                        }
                    ?></li><?php
                    if($key==23)break;
                }
            ?></ul>
        </li>
        <li class="b-calend__stage">
            <ul class="b-calend__one-days"><?php
                foreach($hours as $key=>$hourCur)
                {
                    ?><li class="b-calend__hours-row">
                        <span class="b-calend__hours-row_h"><?=$hourCur?></span><?php
                        foreach($targetsNext as $target)
                        {
                            if ($target['TIME']>=$hourCur and $target['TIME']<$hours[$key+1])
                            {
                                ?><div class="b-calend__event js-clndr-b">
                                    <span class="b-calend__event_prew js-clndr-e"><?=$target['NAME']?></span>
                                    <input type="hidden" class="target-name" value="<?=$target['NAME']?>"/>
                                    <input type="hidden" class="target-time" value="<?=$target['TIME']?>"/>
                                    <input type="hidden" class="target-date" value="<?=$target['DATE']?>"/>
                                    <input type="hidden" class="target-text" value="<?=$target['TEXT']?>"/>
                                </div><?php
                            }
                        }
                    ?></li><?php
                    if($key==23)break;
                }
            ?></ul>
        </li>
    </ul>