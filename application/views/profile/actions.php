<!--  END - MAIN MENU -->
<div class="content box-branches js-b-tgl"><?php
    if(!empty($actions))
    {
        ?><span class="title title-box title-box_inner">Акции</span>
        <div class="b-toogle-row js-b-tgl-r js-b-tgl-r_opener-vis"><?php
            foreach($actions as $action)
            {
                ?><div class="b-toogle-row__tr js-b-tgl-r__i b-branches b-toogle-row__tr_2col">
                    <span class="b-toogle-row__tr_close js-b-tgl-r__x"></span>
                    <div class="b-toogle-row__tr_opener js-b-tgl-r__o">
                        <div class="b-toogle-row__td" width="64%">
                            <a href="#" class="main-link main-link_dot" onclick="window.location.href = '/profile/action/<?=$action['ID']?>'" style="overflow:hidden;text-overflow:ellipsis;max-height: 33px;"><?=$action['NAME']?></a>
                        </div>
                        <div class="b-toogle-row__td" width="35%">
                            <ul class="b-branches__nav">
                                <li class="b-branches__nav-i b-branches__nav-i_ico">
<!--                                    <img src="/images/bg/map_ico.png"/>-->
                                </li>
                                <li class="b-branches__nav-i b-branches__nav-i_edit" onclick="window.location.href = '/profile/action/<?=$action['ID']?>';"><a class="main-link main-link_dot" href="/profile/action/<?=$action['ID']?>">Редактировать</a></li>
                                <input type="hidden" class="_hidden-id" value="<?=$action['ID']?>"/>
                                <li class="b-branches__nav-i b-branches__nav-i_del" onclick="pUp_m.eGetId(this,'Вы действительно хотите удалить это отделение?')" data-pup-id="pUp-del-branch"><a class="main-link main-link_dot">Удалить</a></li>
                            </ul>
                        </div>
                    </div>
                </div><?php
            }
        ?></div><?php
    }
    else
    {
        ?><span class="title title-box title-box_inner">Список отделений пока пуст</span><?php
    }
    ?>
	<input type="submit" value="Добавить акцию" onclick="window.location.href = '/profile/action/'" class="btn-lk btn-lk_save js-pUp__openeer"/>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->