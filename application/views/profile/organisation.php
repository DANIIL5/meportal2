<!--  END - MAIN MENU -->
<div class="content">
    <span class="title title-box">ОБ УЧРЕЖДЕНИИ</span>
    <div class="box box_company">
        <form action="/profile/organisation_save" method="post">
            <div class="box box_avar js-avatar" style="margin-top: 40px;margin-bottom: 40px;">
                <div class="title title-box">Логотип</div>
                <div class="box_avar__image js-avatar__img js-avatar_init <?php if(!empty($organisation['LOGO'])) echo 'notEmpty'?>">
                    <img src="<?=$organisation['LOGO']?>"/>
                    <input type="hidden" class="_path-input" value="<?=$organisation['LOGO']?>"/>
                    <span class="b-add-f-list__i-delete" data-pup-id="pUp-del-user-ava" onclick="pUp_m.eGetId(this,'Удалить текущий логотип?')" <?php if(empty($organisation['LOGO'])) echo 'style="display:none;"'?>></span>
                </div>
                <div class="box_avar__inf">
                    <div class="box_avar__inf-alert">Постарайтесь, чтобы размер картинки не превышал 2 Мб и картинка была в одном из следующих форматов - jpg, png, gif.</div>
                    <input type="submit" value="ВЫБРАТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour js-avatar_init" onclick="return false">
                </div>
                <input type="file" name="ava" class="js-avatar__form-i dnone" accept="image/*"/>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридическое название</span>
                    <input type="text" value="<?=str_replace('"','\'',$organisation['ENTITY'])?>" class="box-fields__input" name="entity"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text b-filter-btn__list-i specialist">
                    <span class="box-fields__label">Тип учреждения</span>
                    <select name="type_id" class="b-filter-btn__select js-select specialist" data-smart-positioning="false">
                        <option value="Без типа">Без типа</option><?php
                        foreach($types as $type)
                        {
                            ?><option value="<?=$type['ID']?>" <?php if($organisation['TYPE_ID']==$type['ID']) echo 'selected' ?>><?=$type['NAME']?></option><?php
                        }
                    ?></select>
                </div>
                <div class="box-fields input_text b-filter-btn__list-i specialist" style="margin-left: 315px;">
                    <span class="box-fields__label">Категория</span>
                    <select name="category_id" class="b-filter-btn__select js-select specialist right" data-smart-positioning="false">
                        <option value="Выберите...">Выберите...</option>
                        <option value="2" <?php if($organisation['CATEGORY_ID']==2) echo 'selected' ?>>Мед. учреждения</option>
                        <option value="4" <?php if($organisation['CATEGORY_ID']==4) echo 'selected' ?>>Аптеки</option>
                        <option value="5" <?php if($organisation['CATEGORY_ID']==5) echo 'selected' ?>>Красота и здоровье</option>
                        <option value="7" <?php if($organisation['CATEGORY_ID']==7) echo 'selected' ?>>Мама и ребенок</option>
                        <option value="8" <?php if($organisation['CATEGORY_ID']==8) echo 'selected' ?>>Реабилитация</option>
                        <option value="9" <?php if($organisation['CATEGORY_ID']==9) echo 'selected' ?>>Ветеринария</option>
                    </select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Бренд</span>
                    <input type="text" value="<?=str_replace('"','\'',$organisation['BRAND'])?>" class="box-fields__input" name="brand"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Лицензия №</span>
                    <input type="text" value="<?=$organisation['LICENSE']?>" class="box-fields__input" name="license"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Описание</span>
                    <textarea class="box-fields__input box-fields__text-area" name="description"><?=$organisation['DESCRIPTION']?></textarea>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: индекс</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['legal']['INDEX']) ? $organisation['get_addresses']['legal']['INDEX'] : ""?>" class="box-fields__input" name="address[legal][index]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: индекс</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['actual']['INDEX']) ? $organisation['get_addresses']['actual']['INDEX'] : ""?>" class="box-fields__input" name="address[actual][index]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: область/регион</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['legal']['REGION']) ? $organisation['get_addresses']['legal']['REGION'] : ""?>" class="box-fields__input" name="address[legal][region]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: область/регион</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['actual']['REGION']) ? $organisation['get_addresses']['actual']['REGION'] : ""?>" class="box-fields__input" name="address[actual][region]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: город/населенный пункт</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['legal']['CITY']) ? $organisation['get_addresses']['legal']['CITY'] : ""?>" class="box-fields__input" name="address[legal][city]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: город/населенный пункт</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['actual']['CITY']) ? $organisation['get_addresses']['actual']['CITY'] : ""?>" class="box-fields__input" name="address[actual][city]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: улица/переулок</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['legal']['STREET']) ? $organisation['get_addresses']['legal']['STREET'] : ""?>" class="box-fields__input" name="address[legal][street]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: улица/переулок</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['actual']['STREET']) ? $organisation['get_addresses']['actual']['STREET'] : ""?>" class="box-fields__input" name="address[actual][street]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: дом/здание</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['legal']['HOUSE']) ? $organisation['get_addresses']['legal']['HOUSE'] : ""?>" class="box-fields__input" name="address[legal][house]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: дом/здание</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['actual']['HOUSE']) ? $organisation['get_addresses']['actual']['HOUSE'] : ""?>" class="box-fields__input" name="address[actual][house]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: корпус/строение</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['legal']['BUILDING']) ? $organisation['get_addresses']['legal']['BUILDING'] : ""?>" class="box-fields__input" name="address[legal][building]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: корпус/строение</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['actual']['BUILDING']) ? $organisation['get_addresses']['actual']['BUILDING'] : ""?>" class="box-fields__input" name="address[actual][building]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Юридический адрес: квартира/офис</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['legal']['OFFICE']) ? $organisation['get_addresses']['legal']['OFFICE'] : ""?>" class="box-fields__input" name="address[legal][office]"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фактический адрес: квартира/офис</span>
                    <input type="text" value="<?=!empty($organisation['get_addresses']['actual']['OFFICE']) ? $organisation['get_addresses']['actual']['OFFICE'] : ""?>" class="box-fields__input" name="address[actual][office]"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text b-filter-btn__list-i specialist">
                    <span class="box-fields__label">Метро</span>
                    <select name="metro_id[]" multiple size="4" class="b-filter-btn__select  specialist" data-smart-positioning="false">
                        <option value="Выберите..." >Выберите...</option><?php
                        foreach($metros as $metro)
                        {
                            ?><option value="<?=$metro['ID']?>" <?=(!empty($current_metros) && !empty($current_metros[$metro['ID']])) ? "selected" : ""?>><?=$metro['NAME']?></option><?php
                        }
                        ?></select>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Широта</span>
                    <input type="text" value="<?=(!empty($organisation['LATITUDE']) && (float)$organisation['LATITUDE'] != 0) ? (float)$organisation['LATITUDE'] : "55.73519720952004" ?>" type="text" name="latitude" class="box-fields__input js-map js-map-lat"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Долгота</span>
                    <input type="text" value="<?=(!empty($organisation['LONGITUDE']) && (float)$organisation['LONGITUDE'] != 0) ? (float)$organisation['LONGITUDE'] : "37.637177346771104" ?>" type="text" name="longitude" class="box-fields__input js-map js-map-lon"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">На карте:</span>
                    <div class="box-fields__map-reg js-map js-map-box" id="b-map"></div>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОГРН</span>
                    <input type="text" value="<?=$organisation['OGRN']?>" class="box-fields__input" name="ogrn"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКТМО</span>
                    <input type="text" value="<?=$organisation['OKTMO']?>" class="box-fields__input" name="oktmo"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ИНН</span>
                    <input type="text" value="<?=$organisation['INN']?>" class="box-fields__input" name="inn"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">КПП</span>
                    <input type="text" value="<?=$organisation['KPP']?>" class="box-fields__input" name="kpp"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКВЭД</span>
                    <input type="text" value="<?=$organisation['OKVED']?>" class="box-fields__input" name="okved"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">ОКПО</span>
                    <input type="text" value="<?=$organisation['OKPO']?>" class="box-fields__input" name="okpo"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Расчетный счет</span>
                    <input type="text" value="<?=$organisation['CHECKING_ACCOUNT']?>" class="box-fields__input" name="checking_account"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Банк</span>
                    <input type="text" value="<?=str_replace('"','\'',$organisation['BANK'])?>" class="box-fields__input" name="bank"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">БИК</span>
                    <input type="text" value="<?=$organisation['BIK']?>" class="box-fields__input" name="bik"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Главный врач / заведующий / администратор</span>
                    <input type="text" value="<?=$organisation['ADMINISTRATOR']?>" class="box-fields__input" name="administrator"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Главный бухгалтер</span>
                    <input type="text" value="<?=$organisation['CHIEF_ACCOUNTANT']?>" class="box-fields__input" name="chief_accountant"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Генеральный директор</span>
                    <input type="text" value="<?=$organisation['CEO']?>" class="box-fields__input" name="ceo"/>
                </div>
            </div>
            <div class="clear"></div>

            <span class="title title-box title-box_inner">Контактные данные</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 1</span>
                    <input type="text" value="<?=$organisation['get_contacts']['phones'][0]?>" class="box-fields__input phone_mask" name="phone1"/>
                </div>

                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 1</span>
                    <input type="text" value="<?=$organisation['get_contacts']['websites'][0]?>" class="box-fields__input" name="website1"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 2</span>
                    <input type="text" value="<?=$organisation['get_contacts']['phones'][1]?>" class="box-fields__input phone_mask" name="phone2"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 2</span>
                    <input type="text" value="<?=$organisation['get_contacts']['websites'][1]?>" class="box-fields__input" name="website2"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Телефон 3</span>
                    <input type="text" value="<?=$organisation['get_contacts']['phones'][2]?>" class="box-fields__input phone_mask" name="phone3"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Веб-сайт 3</span>
                    <input type="text" value="<?=$organisation['get_contacts']['websites'][2]?>" class="box-fields__input" name="website3"/>
                </div>
            </div>

            <span class="title title-box title-box_inner">Рабочее время</span>
            <div class="box-branches_add">
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПН)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][1]['WORK_FROM']?>" class="box-fields__input js-time-input" name="work[1][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][1]['WORK_TO']?>" class="box-fields__input js-time-input" name="work[1][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][2]['WORK_FROM']?>" class="box-fields__input js-time-input" name="work[2][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][2]['WORK_TO']?>" class="box-fields__input js-time-input" name="work[2][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СР)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][3]['WORK_FROM']?>" class="box-fields__input js-time-input" name="work[3][from]">
                            <input type="text" value="<?=$organisation['get_working_time'][3]['WORK_TO']?>" class="box-fields__input js-time-input" name="work[3][to]">
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ЧТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][4]['WORK_FROM']?>" class="box-fields__input js-time-input" name="work[4][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][4]['WORK_TO']?>" class="box-fields__input js-time-input" name="work[4][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][5]['WORK_FROM']?>" class="box-fields__input js-time-input" name="work[5][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][5]['WORK_TO']?>" class="box-fields__input js-time-input" name="work[5][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СБ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][6]['WORK_FROM']?>" class="box-fields__input js-time-input" name="work[6][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][6]['WORK_TO']?>" class="box-fields__input js-time-input" name="work[6][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВС)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][7]['WORK_FROM']?>" class="box-fields__input js-time-input" name="work[7][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][7]['WORK_TO']?>" class="box-fields__input js-time-input" name="work[7][to]"/>
                        </div>
                    </div>
                </div>
            </div>

            <span class="title title-box title-box_inner">Перерыв</span>
            <div class="box-branches_add">
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ПН)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][1]['BREAK_FROM']?>" class="box-fields__input js-time-input" name="break[1][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][1]['BREAK_TO']?>" class="box-fields__input js-time-input" name="break[1][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ВТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][2]['BREAK_FROM']?>" class="box-fields__input js-time-input" name="break[2][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][2]['BREAK_TO']?>" class="box-fields__input js-time-input" name="break[2][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (СР)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][3]['BREAK_FROM']?>" class="box-fields__input js-time-input" name="break[3][from]">
                            <input type="text" value="<?=$organisation['get_working_time'][3]['BREAK_TO']?>" class="box-fields__input js-time-input" name="break[3][to]">
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ЧТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][4]['BREAK_FROM']?>" class="box-fields__input js-time-input" name="break[4][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][4]['BREAK_TO']?>" class="box-fields__input js-time-input" name="break[4][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ПТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][5]['BREAK_FROM']?>" class="box-fields__input js-time-input" name="break[5][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][5]['BREAK_TO']?>" class="box-fields__input js-time-input" name="break[5][to]"/>
                        </div>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (СБ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][6]['BREAK_FROM']?>" class="box-fields__input js-time-input" name="break[6][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][6]['BREAK_TO']?>" class="box-fields__input js-time-input" name="break[6][to]"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Время перерыва (ВС)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$organisation['get_working_time'][7]['BREAK_FROM']?>" class="box-fields__input js-time-input" name="break[7][from]"/>
                            <input type="text" value="<?=$organisation['get_working_time'][7]['BREAK_TO']?>" class="box-fields__input js-time-input" name="break[7][to]"/>
                        </div>
                    </div>
                </div>
            </div>

            <!--  BLOCK - ADD FILES -->
            <div class="box b-add-f">
                <span class="title title-box">Сканы лицензий и файлы</span>
                <p class="paragraph paragraph_prew">Постарайтесь, чтобы размер файла не превышал <span class="b-add-f__limit">20 Мб</span> и файл был в одном из следующих форматов - jpg, png, gif, tif, pdf, docx, doc, rtf, txt, ppt, pptx, xls, xlsx, odf.</p>
                <div class="b-add-f-reg js-add-files">
                	<ul class="b-add-f-list js-add-list"><?php
                        foreach($files as $file)
                        {
                    		?><li class="b-add-f-list__i js-add-li" data-img-path="<?=$file['FILE']?>">
                                <img src="<?=$file['FILE']?>"/>
                                <input type="hidden" class="_hidden-id" value="<?=$file['FILE']?>"/>
                                <span class="b-add-f-list__i-delete jd-add-f_delete" data-pup-id="pUp-del-document"></span>
                            </li><?php
                        }
                	    ?><li class="b-add-f-list__i b-add-f-list__i_add-new js-add-f"></li>
                    </ul>
                </div>
            </div>
            <!--  END - BLOCK - ADD FILES -->

            <div>
                <ul class="list-two">
                    <li><input type="checkbox" class="checkbox" id="input_1" value="on" <?=(!empty($organisation["F_STATE"])) ? "checked" : ""?> name="f_state"/><label for="input_1">Государственная</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_2" value="on" <?=(!empty($organisation["F_PRIVATE"])) ? "checked" : ""?> name="f_private"/><label for="input_2">Частная</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_3" value="on" <?=(!empty($organisation["F_CHILDREN"])) ? "checked" : ""?> name="f_children"/><label for="input_3">Детское отделение</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_4" value="on" <?=(!empty($organisation["F_AMBULANCE"])) ? "checked" : ""?> name="f_ambulance"/><label for="input_4">Скорая</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_5" value="on" <?=(!empty($organisation["F_HOUSE"])) ? "checked" : ""?> name="f_house"/><label for="input_5">Выезд на дом</label></li>
                </ul>

                <br>

                <ul class="list-two">
                    <li><input type="checkbox" class="checkbox" id="input_1" value="on" <?=(!empty($organisation["F_BOOKING"])) ? "checked" : ""?> name="f_booking"/><label for="input_1">Бронирование</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_2" value="on" <?=(!empty($organisation["F_DELIVERY"])) ? "checked" : ""?> name="f_delivery"/><label for="input_2">С доставкой</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_3" value="on" <?=(!empty($organisation["F_DAYNIGHT"])) ? "checked" : ""?> name="f_daynight"/><label for="input_3">Круглосуточно</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_4" value="on" <?=(!empty($organisation["F_DMS"])) ? "checked" : ""?> name="f_dms"/><label for="input_4">ДМС</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_5" value="on" <?=(!empty($organisation["F_DLO"])) ? "checked" : ""?> name="f_dlo"/><label for="input_5">ДЛО</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_6" value="on" <?=(!empty($organisation["F_OPTICS"])) ? "checked" : ""?> name="f_optics"/><label for="input_6">Отдел оптики</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_7" value="on" <?=(!empty($organisation["F_RPO"])) ? "checked" : ""?> name="f_rpo"/><label for="input_7">Рецептурно-производственный отдел</label></li>
                    <li><input type="checkbox" class="checkbox" id="input_6" value="on" <?=(!empty($organisation["F_HOMEOPATHY"])) ? "checked" : ""?> name="f_homeopathy"/><label for="input_6">Отдел гомеопатии</label></li>
                </ul>
            </div>

            <? if (!empty($branch_id)) { ?>
                <input type="hidden" name="branch_id" value="<?=$branch_id?>">
            <? } ?>
            <div class="clear"></div>
            <input type="submit" value="Сохранить изменения" name="submit" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
        </form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->