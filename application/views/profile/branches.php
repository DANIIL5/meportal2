<!--  END - MAIN MENU -->
<div class="content box-branches js-b-tgl"><?php
    if(!empty($branches))
    {
        ?><span class="title title-box title-box_inner">Отделения</span>
        <div class="b-toogle-row js-b-tgl-r js-b-tgl-r_opener-vis"><?php
            foreach($branches as $branch)
            {
                ?><div class="b-toogle-row__tr js-b-tgl-r__i b-branches b-toogle-row__tr_2col" data-target-id="<?=$branch['ID']?>">
                    <span class="b-toogle-row__tr_close js-b-tgl-r__x"></span>
                    <div class="b-toogle-row__tr_opener js-b-tgl-r__o">
                        <div class="b-toogle-row__td" width="64%">
                            <a href="#" class="main-link main-link_dot b-branches__text js-b-tgl-r__o" style="overflow:hidden;text-overflow:ellipsis;max-height: 33px;"><?=$branch['BRAND']?> <?=!empty($branch['ENTITY']) ? "<<".$branch['ENTITY'].">>" : ""?></a>
                        </div>
                        <div class="b-toogle-row__td" width="35%">
                            <ul class="b-branches__nav">
                                <li class="b-branches__nav-i b-branches__nav-i_ico">
<!--                                    <img src="/images/bg/map_ico.png"/>-->
                                </li>
                                <li class="b-branches__nav-i b-branches__nav-i_edit" onclick="window.location.href = '/profile/organisation/<?=$branch['ID']?>';"><a class="main-link main-link_dot" href="/profile/organisation/<?=$branch['ID']?>">Редактировать</a></li>
                                <input type="hidden" class="_hidden-id" value="<?=$branch['ID']?>"/>
                                <li class="b-branches__nav-i b-branches__nav-i_del" onclick="pUp_m.eGetId(this,'Вы действительно хотите удалить это отделение?')" data-pup-id="pUp-del-branch"><a class="main-link main-link_dot">Удалить</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="b-toogle-row__tr_content js-b-tgl-r__c">
                        <ul class="b-branches__info">
							<? if (!empty($branch['get_addresses']['actual'])) { ?>
                            <li class="b-branches__info-i" style="width:540px;overflow:hidden;text-overflow:ellipsis;height:20px;">
                                <div class="b-branches__info-i-title">Адрес:</div>
                                <div class="b-branches__info-i-txt">
									<?=implode(', ', $branch['get_addresses']['actual'])?>
                                </div>
                            </li>
							<? } ?>
							<? if (!empty($branch['get_metros'])) { ?>
                            <li class="b-branches__info-i" style="width:200px;overflow:hidden;text-overflow:ellipsis;height:25px;">
                                <div class="b-branches__info-i-title">Метро:</div>
                                <div class="b-branches__info-i-txt b-branches__info-i-txt_metro">
									<?=implode(', ', array_column($branch['get_metros'], 'NAME'))?>
								</div>
                            </li>
							<? } ?>
							<? if (!empty($branch['get_contacts']['websites'])) { ?>
								<li class="b-branches__info-i marat-height" style="width:285px;margin-right:75px;">
                                <div class="b-branches__info-i-title">Cайт:</div>
                                <div class="b-branches__info-i-txt">
									<? foreach ($branch['get_contacts']['websites'] as $website) { ?>
                                    <a style="height: 18px;overflow: hidden;text-overflow: ellipsis;" href="<?=$website?>" class="main-link b-branches__text"><?=$website?></a><br>
									<? } ?>
                                </div>
                            </li>
							<? } ?>
							<? if (!empty($branch['get_contacts']['phones'])) { ?>
                            <li class="b-branches__info-i marat-height">
                                <div class="b-branches__info-i-title">Телефон:</div>
                                <div class="b-branches__info-i-txt">
                                    <?=implode(', ', $branch['get_contacts']['phones'])?>
                                </div>
                            </li>
							<? } ?>
							<? if (!empty($branch['get_working_time'])) { ?>
							<li class="b-branches__info-i marat-height">
                                <div class="b-branches__info-i-title">График работы:</div>
                                <div class="b-branches__info-i-txt"><?php
									foreach ($branch['get_working_time'] as $key => $times) { ?>
										<?=$days[$key-1]?>: работа с<?=$times['WORK_FROM']?> до<?=$times['WORK_TO']?>, перерыв с<?=$times['BREAK_FROM']?> до<?=$times['BREAK_TO']?><br>
									<? } ?>
								</div>
                            </li>
							<? } ?>
                        </ul>
                    </div>
                </div><?php
            }
        ?></div><?php
    }
    else
    {
        ?><span class="title title-box title-box_inner">Список отделений пока пуст</span><?php
    }
    ?>
	<input type="submit" value="Добавить отделение" onclick="window.location.href = '/profile/branch_add/'" class="btn-lk btn-lk_save js-pUp__openeer"/>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->