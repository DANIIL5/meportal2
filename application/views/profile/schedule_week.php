<div class="b-calend-reg js-calend-reg" data-type-calend="1">
    <ul class="b-calend" id="js-slider-bx">
        <li class="b-calend__stage">
            <ul class="b-calend__weeks">
                <li class="b-calend__hours-row _dw"><?php
                    foreach ($weekLetters as $key => $weekLetter)
                    {
                        ?><div class="b-calend__day <?php if(($key+1)==date("w")) echo '_dw_now' ?>"><?=$weekLetter?>,
                            <span class="_dw__m"><?=date("d/m",mktime(0, 0, 0, date("m"), date("d")+($key+1)-date("w"), date("Y")))?></span></span>
                        </div><?php
                    }
                ?></li><?php
                foreach($hours as $key=>$hourCur)
                {
                    ?><li class="b-calend__hours-row">
                        <span class="b-calend__hours-row_h"><?=$hourCur?></span><?php
                        for ($i=0; $i<7; $i++)
                        {
                            ?><div class="b-calend__day"><?php
                                foreach ($targetsCur as $target)
                                {
                                    if($target['DATE']>=date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+($i+1)-date("w"), date("Y")))
                                       and $target['DATE']<=date("d/m/Y", mktime(23, 59, 59, date("m"), date("d")+($i+1)-date("w"), date("Y")))
                                       and $target['TIME']>=$hourCur and $target['TIME']<$hours[$key+1])
                                        {
                                            ?><div class="b-calend__event js-clndr-b">
                                                <span class="b-calend__event_prew js-clndr-e"><?=$target['NAME']?></span>
                                                <input type="hidden" class="target-name" value="<?=$target['NAME']?>"/>
                                                <input type="hidden" class="target-time" value="<?=$target['TIME']?>"/>
                                                <input type="hidden" class="target-date" value="<?=$target['DATE']?>"/>
                                                <input type="hidden" class="target-text" value="<?=$target['TEXT']?>"/>
                                            </div><?php
                                        }
                                }
                            ?></div><?php
                        }
                    ?></li><?php
                    if($key==23)break;
                }
            ?></ul>
        </li>
        <li class="b-calend__stage">
            <ul class="b-calend__weeks">
                <li class="b-calend__hours-row _dw"><?php
                    foreach ($weekLetters as $key => $weekLetter)
                    {
                        ?><div class="b-calend__day"><?=$weekLetter?>,
                            <span class="_dw__m"><?=date("d/m", mktime(0, 0, 0, date("m"), date("d")+($key+8)-date("w"), date("Y")))?></span>
                        </div><?php
                    }
                ?></li><?php
                foreach($hours as $key=>$hourCur)
                {
                    ?><li class="b-calend__hours-row">
                        <span class="b-calend__hours-row_h"><?=$hourCur?></span><?php
                        for ($i=0; $i<7; $i++)
                        {
                            ?><div class="b-calend__day"><?php
                                foreach ($targetsNext as $target)
                                {
                                    if($target['DATE']>=date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+($i+8)-date("w"), date("Y")))
                                       and $target['DATE']<=date("d/m/Y", mktime(23, 59, 59, date("m"), date("d")+($i+8)-date("w"), date("Y")))
                                       and $target['TIME']>=$hourCur and $target['TIME']<$hours[$key+1])
                                        {
                                            ?><div class="b-calend__event js-clndr-b">
                                                <span class="b-calend__event_prew js-clndr-e"><?=$target['NAME']?></span>
                                                <input type="hidden" class="target-name" value="<?=$target['NAME']?>"/>
                                                <input type="hidden" class="target-time" value="<?=$target['TIME']?>"/>
                                                <input type="hidden" class="target-date" value="<?=$target['DATE']?>"/>
                                                <input type="hidden" class="target-text" value="<?=$target['TEXT']?>"/>
                                            </div><?php
                                        }
                                }
                            ?></div><?php
                        }
                    ?></li><?php
                    if($key==23)break;
                }
            ?></ul>
        </li>
    </ul>