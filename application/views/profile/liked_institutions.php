<!--  END - MAIN MENU -->
<div class="content js-pUp__openeer">
    <div class="box box_favorite_doctor">
        <div class="box-fields-row box-fields-row_cell-2">
            <div class="box-fields input_text">
                <div class="title title-box">Медицинские учреждения</div>
            </div>
            <div class="box-fields input_text">
                <div class="title-stat">
                    <ul class="title-stat-list">
                        <li class="title-stat-list__i">
                            <a href="/profile/liked_specialists">Врачи</a>
                            <span class="title-stat-list__i-count"><?=$count_liked_specialists[0]['COUNT']?></span>
                        </li>
                        <li class="title-stat-list__i">
                            <a href="/profile/liked_preps">Препараты</a>
                            <span class="title-stat-list__i-count"><?=$count_liked_preps[0]['COUNT']?></span>
                        </li>
                        <li class="title-stat-list__i">
                            <a href="/profile/liked_articles">Статьи</a>
                            <span class="title-stat-list__i-count"><?=$count_liked_articles[0]['COUNT']?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="box-fields-row">
            <div class="box-fields input_text box-fields_search">
                <form action="/profile/liked_institutions_search" method="POST">
                    <input type="text" value="" placeholder="Искать среди избранного" class="box-fields__input box-fields__input_search" name="search">
                </form>
            </div>
        </div>
        <div class="box-fields-row box-fields-row_inner">
            <div class="b-row-tab">
                <ul class="b-tab-nav">
                    <li class="b-tab-nav__i <?php if($_SERVER['PHP_SELF']=='/index.php/profile/liked_institutions') echo 'b-tab-nav__i_active' ?>"><a href="/profile/liked_institutions">Все</a></li>
                    <?php foreach($count_liked_institutions_type_id as $key=>$liked)
                    {
                        ?><li class="b-tab-nav__i <?php if($_SERVER['PHP_SELF']=='/index.php/profile/liked_institutions_filter/'.$liked['INSTITUTION_TYPE_ID']) echo 'b-tab-nav__i_active' ?>"><a href="/profile/liked_institutions_filter/<?=$liked['INSTITUTION_TYPE_ID']?>"><?=$count_liked_institutions_type[$key]['INSTITUTION_TYPE']?></a>
                            <span class="b-tab-nav__i-count">(<?=$liked['COUNT']?>)</span>
                        </li>
                    <?php
                    }
                    ?></ul>
                <div class="clear"></div>
                <ul class="b-tab-content">
                    <?php foreach($liked_institutions as $liked)
                    {
                        ?><li class="b-tab-content__i institution" data-target-id="<?=$liked['ID']?>">
                            <div class="b-tab-content__i-avatar">
                                <div class="b-tab-content__i-avatar_cover">
                                    <img src="<?=$liked['INSTITUTION_LOGO']?>" \>
                                </div>
                                <input type="hidden" class="_hidden-id" value="<?=$liked['ID']?>"/>
                                <span class="b-tab-content__i-delete" onclick="pUp_m.eGetId(this,'Удалить из избранного?')" data-pup-id="pUp-del-liked-institution"></span>
                            </div>
                            <div class="b-tab-content__i-name"><?=$liked['INSTITUTION_NAME']?></div>
                            <div class="b-tab-content__i-about"><?=$liked['INSTITUTION_TYPE']?></div>
                        </li><?php
                    }?><li class="b-tab-content__i b-tab-content__i_add-new">
                            <div class="b-tab-content__i-avatar">
                            </div>
                            <div class="b-tab-content__i-name">Добавить</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->