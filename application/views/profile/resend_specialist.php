<!--  END - MAIN MENU -->
<div class="content"> 
    <div class="box box_new-msg">
        <form action="/profile/resendSpecialist" onsubmit="return mfp.ajaxResendLoader(this)">
             <span class="title title-box">новое соообщение</span>
              <div class="box-fields-row">
                <div class="b-input-add-contacts">
                     <div class="box-fields input_text box-fields_add-cont">
                            <span class="box-fields__label">Кому</span>
                            <input type="text" class="box-fields__input" name="destinations[]"/>
                            <span class="box-fields_add-cont__ico">+</span>
                        </div>
                     </div>
                </div>
             <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Тема</span>
                    <input type="text" class="box-fields__input" name="theme"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Сообщение</span>
                    <div class="box-fields__input box-fields__input_new-target box-fields__message-reg js-msg-paste-Input">
                        <div class="box-fields__message-reg-i js-msg-paste-Input__text" contenteditable="true">Уважаемый, обратите внимание на нашего нового специалиста.
                        </div>
                        <div class="box-fields__message-attach" contenteditable="false">
                            <span class="box-fields__label box-fields__message-attach-title">К сообщению прикреплена контактная информация специалиста:</span>
                            <div class="box-fields__message-attach-item">
                                <div class="b-attach-contact">
                                    <div class="b-attach-contact__f b-attach-contact__f_ava">
                                        <div class="b-attach-contact__f_ava-img">
                                            <img src="<?=$specialist['PHOTO']?>"/>
                                        </div>
                                    </div>
                                    <div class="b-attach-contact__f b-attach-contact__f_info">
                                        <input type="hidden" name="resend_specialist_id" value="<?=$specialist['ID']?>"/>
                                        <span class="b-attach-contact__f-name"><?=$specialist['LAST_NAME'].' '.$specialist['FIRST_NAME'].' '.$specialist['SECOND_NAME']?></span>
                                        <span class="b-attach-contact__f-desc"><?=$specialist['POSITION']?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <textarea class="g-visNot g-heightNot js-msg-paste-Input__input" name="message"></textarea>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <!--  BLOCK - ADD FILES -->
            <div class="box b-add-f">
                <span class="title title-box">Документы и файлы</span>
                <p class="paragraph paragraph_prew">Постарайтесь, чтобы размер файла не превышал <span class="b-add-f__limit">20 Мб</span> и файл был в одном из следующих форматов - jpg, png, gif, tif, pdf, docx, doc, rtf, txt, ppt, pptx, xls, xlsx, odf.</p>
                <div class="b-add-f-reg js-add-files">
                	<ul class="b-add-f-list js-add-list">
                		<li class="b-add-f-list__i b-add-f-list__i_add-new js-add-f"></li>
                	</ul>
                </div>
            </div>
            <!--  END - BLOCK - ADD FILES -->
            <div class="clear"></div>
            <input type="submit" value="Отправить" class="btn-lk btn-lk_send js-pUp__openeer" data-pup-id="pUp-save-thx"/>
        </form>
    </div>
</div>
<!--  RIGHT - PANEL -->
<div class="right-panel">
    <div class="b-RP-buttons">
        <a link="#" class="btn-lk btn-lk_RP btn-lk_RP_pref">Настройки</a>
        <a href="/profile/institutionHelpInstruction" class="btn-lk btn-lk_RP btn-lk_RP_help">Помощь</a>
        <a href="/profile/post/admin" class="btn-lk btn-lk_RP btn-lk_RP_help" style="height:50px;">Написать администратору</a>
    </div>
    <div class="title title_DEF b-RP-search-adr">Адресная книга<span class="b-RP-search-adr__p"></span></div>
    <div class="box-fields input_text box-fields_search b-RP-search">
        <input onkeyup="return mfp.searchAddress(this)" placeholder="Искать адресата" class="box-fields__input box-fields__input_search" type="text"/>
    </div>
    <div class="b-RP-adress-list"><?=$addressBook?></div>
</div>
<!--  END RIGHT - PANEL -->
<!--  FOOTER -->