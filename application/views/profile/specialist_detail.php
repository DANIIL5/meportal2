<!--  END - MAIN MENU -->
<div class="content b-reviews">
    <div class="box-fields-row_inner">
        <div class="box box_personal-card">
            <div class="b-info b-info-short">
                <ul class="b-info-short-fields">
                    <li class="b-info-short-fields__i">
                        <div class="b-info-short-fields__avatar"><?php
                            if(!empty($specialist['PHOTO']))
                            {
                                ?><img src="<?=$specialist['PHOTO']?>"/><?php
                            }
                            else
                            {
                                ?><img src="/images/bg/def-ava.png" style="margin-top:35%;width:30%;"/><?php
                            }
                        ?></div>
                        <span class="b-info-short-fields__fio"><?=$specialist['LAST_NAME'].' '.mb_substr($specialist['FIRST_NAME'],0,1).'. '.mb_substr($specialist['SECOND_NAME'],0,1).'.'?></span>
                    </li>
                    <li class="b-info-short-fields__i">
                        <div class="b-info-short-fields__s"><?php
                            if(!empty($specialist['BRANCH']))
                            {
                                ?><span class="b-info-short-fields__s-ttl">Клиника</span> 
                                <span class="b-info-short-fields__s-i">«<a class="def-link" href="#"><?=$specialist['BRANCH']?></a>»</span><?php
                            }
                        ?></div>
                    </li>
                    <li class="b-info-short-fields__i">
                        <div class="b-info-short-fields__s"><?php
                            if(!empty($specialist['ADDRESS_AND_INDEX']))
                            {
                                ?><span class="b-info-short-fields__s-ttl">Адрес</span> 
                                <span class="b-info-short-fields__s-i"><?=$specialist['ADDRESS_AND_INDEX']?></span><?php
                            }
                        ?></div>
                    </li>
                    <li class="b-info-short-fields__i">
                        <div class="b-info-short-fields__s"><?php
                            if(!empty($specialist['WORK_PHONE']))
                            {
                                ?><span class="b-info-short-fields__s-ttl">Телефон</span> 
                                <span class="b-info-short-fields__s-i"><?=$specialist['WORK_PHONE']?></span><?php
                            }
                        ?></div>
                    </li>
                    <li class="b-info-short-fields__i">
                        <div class="b-info-short-fields__s"><?php
                            if(!empty($specialist['EMAIL']))
                            {
                                ?><span class="b-info-short-fields__s-ttl">Эл. почта</span> 
                                <span class="b-info-short-fields__s-i">
                                    <a class="def-link" href="mailto:<?=$specialist['EMAIL']?>"><?=$specialist['EMAIL']?></a>
                                </span><?php
                            }
                        ?></div>
                    </li>
                </ul>
            </div>
            <div class="b-info b-info-all">
                <div class="b-info-all__row detail-spec">
                    <span class="b-info-all__row-i b-info-all__row-i_fio"><?=$specialist['LAST_NAME'].' '.$specialist['FIRST_NAME'].' '.$specialist['SECOND_NAME']?></span>
                    <span class="b-info-all__row-i b-info-all__row-i_desc"><?=$specialist['POSITION']?></span>
                    <div class="b-info-all__row-i b-info-all__row-i_form-elem">
                        <input value="УДАЛИТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour_lite" type="submit"/>
                        <input type="submit" value="ПЕРЕСЛАТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour" onclick="window.location.pathname='/profile/resend_specialist/<?=$specialist['ID']?>';"/>
                    </div>
                </div>
                <div class="b-info-all__row">
                    <p class="def-p"><?=$specialist['DESCRIPTION']?></p>
                </div>
                <div class="b-info-all__row">
                     <div class="b-info-all__row-i b-graph-timing">
                        <span class="b-graph-timing__title">График</span>
                        <ul class="b-graph-timing-list"><?php
                            foreach ($weekDays as $key => $weekDay)
                            {
                                ?><li class="b-graph-timing-list__i">
                                    <span class="b-graph-timing-list__wd"><?=$weekLetters[$key]?>,
                                        <span class="b-graph-timing-list__wdt"><?=$weekDay?></span>
                                    </span><?php
                                    if(!empty($timeFrom[$key]) and !empty($timeTo[$key]))
                                    {
                                        ?><span class="b-graph-timing-list__ival"><?=$timeFrom[$key]?> — <?=$timeTo[$key]?></span><?php
                                    }
                                    else
                                    {
                                        ?><span class="b-graph-timing-list__ival b-graph-timing-list__ival_not">Выходной</span><?php
                                    }
                                ?></li><?php
                            }
                        ?></ul>
                     </div>
                </div>   
                <div class="clear"></div>
                <input type="button" value="Записаться на прием" class="btn-lk btn-lk_save detail-spec">
                <div class="b-info-all__row b-info-all__row_reviews">
                    <div class="b-reviews-list"><?php
                        if(!empty($comments))
                        {
                            ?><span class="title title_DEF b-reviews-list__title">Отзывы пользователей</span>
                            <ul class="reviews-list js-reviews-ldr __off"><?php
                                foreach ($comments as $comment)
                                {
                                    ?><li class="reviews-list__i">
                                        <div class="reviews-list__i-ava">
                                            <div class="reviews-list__i-ava-img <?php if(empty($comment['USER_PHOTO'])) echo '__def' ?>">
                                                <img src="<?=$comment['USER_PHOTO']?>"/>
                                            </div>
                                            <div class="reviews-list__i-ava-nickname"><?=$comment['USER_NAME']?></div>
                                        </div>
                                        <div class="reviews-list__i-txt js-reviews">
                                            <span class="reviews-list__i-date"><?=$comment['DATE']?></span>
                                            <div class="reviews-list__i-txt_p-cover js-reviews__P-cover">
                                                <div class="reviews-list__i-txt_p js-reviews__P"><?=$comment['MESSAGE']?></div>
                                            </div>
                                            <span class="reviews-list__i-view-more js-reviews__VM">Показать целиком</span>
                                        </div>
                                    </li><?php
                                }
                            ?></ul><?php
                        }
                        else
                        {
                            ?><span class="title title_DEF b-reviews-list__title">Отзывов пока нет</span><?php
                        }
                    ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->