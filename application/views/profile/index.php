<!--  END - MAIN MENU -->
<div class="content"> 
    <div class="box box_personal-data">
        <form action="/profile/changeProfile" onsubmit="return mfp.ajaxLoader(this)">
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields box-fields_column">
                    <div class="box_personal-data__fio">
                        <div class="title title-box title-box_id">
                            Персональные данные
                            <span class="title-box__id"><?=$user['ID']?></span>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Фамилия</span>
                            <input type="text" value="<?=$user['LASTNAME']?>" class="box-fields__input" name="lastname"/>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Имя</span>
                            <input type="text" value="<?=$user['FIRSTNAME']?>" class="box-fields__input" name="firstname"/>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Отчество</span>
                            <input type="text" value="<?=$user['MIDDLENAME']?>" class="box-fields__input" name="middlename"/>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Электронная почта</span>
                            <input type="text" value="<?=$user['EMAIL']?>" class="box-fields__input" name="email"/>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Номер телефона</span>
                            <input type="text" value="<?=$user['PHONE']?>" class="box-fields__input js-tel" name="phone"/>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="box_secure">
                        <span class="title title-box title-box_inner">Безопасность</span>
                        <p class="paragraph paragraph_prew">Если вы хотите поменять свой пароль, просто введите новый и сохраните его.</p>
                        <div class="box_secure__reg js-change-pass">
                            <input type="password" name="password" class="box-fields__input box-fields__input_change-pass js-change-pass__input"/>
                            <span class="box_secure__change-pass-loop js-change-pass__loop"></span>
                            <input type="submit" value="ИЗМЕНИТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour js-change-pass__submit js-pUp__openeer" data-pup-id="pUp-save-thx" onclick="return mfp.changeProfilePassword(this)"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields  box-fields_column">
                    <div class="box box_avar js-avatar">
                        <div class="title title-box">Портрет</div>
                        <div class="box_avar__image js-avatar__img js-avatar_init <?php if(!empty($user['PHOTO'])) echo 'notEmpty'?>">
                            <img src="<?=$user['PHOTO']?>"/>
                            <input type="hidden" class="_path-input" value="<?=$user['PHOTO']?>"/>
                            <span class="b-add-f-list__i-delete" data-pup-id="pUp-del-user-ava" onclick="pUp_m.eGetId(this,'Удалить текущий портрет?')" <?php if(empty($user['PHOTO'])) echo 'style="display:none;"'?>></span>
                        </div>
                        <div class="box_avar__inf">
                            <div class="box_avar__inf-alert">Постарайтесь, чтобы размер картинки не превышал 2 Мб и картинка была в одном из следующих форматов - jpg, png, gif.</div>
                            <input type="submit" value="ВЫБРАТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour js-avatar_init" onclick="return false">
                        </div>
                        <input type="file" name="ava" class="js-avatar__form-i dnone" accept="image/*"/>
                    </div>
                    <div class="clear"></div>
                    <!--  BLOCK - SOCIO -->
                    <div class="box box_socio js-socio">
                        <span class="title title-box title-box_inner">Социальные сети</span>
                        <p class="paragraph paragraph_prew" style="width:480px">Вы можете указать свои профили в различных социальных сетях, чтобы упростить связь с врачем или мед. учреждением.</p>
                        <ul class="box_socio-list"></ul><?php
                            foreach ($user_nets as $user_net)
                            {
                                ?><li class="box_socio-list__i" data-target-id="<?=$user_net['SOC_NET_ID']?>">
                                    <a target="_blank" href="<?=$user_net['SOC_PAGE']?>">
                                        <img src="<?=$user_net['ICON']?>"/>
                                    </a>
                                    <input type="hidden" class="_hidden-id" value="<?=$user_net['SOC_NET_ID']?>"/>
                                    <span class="b-add-f-list__i-delete" data-pup-id="pUp-del-soc-net" onclick="pUp_m.eGetId(this,'Удалить ссылку на профиль?')"></span>
                                </li><?php
                            }
                            ?><li class="box_socio-list__i box_socio-list__i_add-new js-socio-opener"></li>
                        </ul><?php
                        if(!empty($soc_nets))
                        {
                            ?><div class="box_socio__popup js-socio-pUp">
                                <span class="box_socio__popup-title title title-box">СОЦИАЛЬНЫЕ СЕТИ</span>
                                <div class="box_socio__popup-list">
                                    <ul class="box_socio__popup-list-i"><?php
                                        foreach($soc_nets as $net)
                                        {
                                            $rep=0;
                                            foreach ($user_nets as $user_net)
                                            {
                                                if($user_net['SOC_NET_ID']==$net['ID'])
                                                {
                                                    $rep=1;
                                                    break(1);
                                                }
                                            }
                                            if($rep==0)
                                            {
                                                ?><li class="<?=$net['CSS_CLASS']?>">
                                                    <a href="<?=$net['AUTH_LINK']?>"></a>
                                                </li><?php
                                            }
                                            else
                                            {
                                                ?><li class="<?=$net['CSS_CLASS']?> disable" data-id="<?=$net['ID']?>"></li><?php
                                            }
                                        }
                                    ?></ul>
                                </div>
                            </div><?php
                        }
                    ?></div>
                    <!--  END - BLOCK - ADD FILES -->
                </div>
            </div>
            <div class="clear"></div>
            <div class="box-btn-lk-save">
                <input type="submit" value="Сохранить изменения" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
                <span class="box-btn-lk-save__alert">Постарайтесь не забыть свой новый пароль.</span>
            </div>
        </form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->