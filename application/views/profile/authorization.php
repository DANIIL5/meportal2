<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Все аптеки города</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/register.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/css/hot_css.css" type="text/css" media="screen">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body>

<div id="wrap">
    <header id="header" class="short">
        <div class="container">
            <div class="top">
                <div id="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>

                <a class="phone" href="tel:88003003232"><span>8 800</span> 555-31-91</a>

                <?=$templates['city_selector']?>

                <div class="registry dotted">Регистратура</div>

                <div class="btn">Портал для профессионалов</div>
            </div>


        </div>
        <div class="slider_top flexslider">
            <div class="slide"><img class="slider_img" src="/temp/slide-2.jpg" alt=""></div>
            <div class="slide"><img class="slider_img" src="/temp/slide-1.jpg" alt=""></div>
            <div class="slide"><img class="slider_img" src="/temp/slide-2.jpg" alt=""></div>
        </div>
    </header>



    <div id="content">

        <div class="container">


            <div class="registration">
                <div class="heading">
                    <h1>Авторизация</h1>
                    <span>Нет аккаунта? <a href="/profile/registration">Зарегистрируйтесь</a></span>
                    <?=$templates['errors']?>
                </div>
                <div class="content">

                    <form class="register specialist" action="" method="post" enctype="multipart/form-data">
                        <center>
                            <input class="text" type="text" name="email" size="128" value="<?=$forms['registration']['email']?>" placeholder="Эл.почта">
                        </center>
                        <center>
                            <input class="text" type="password" name="password" size="48" placeholder="Пароль">
                        </center>
                        <center>
                            <button type="submit" class="btn" style="margin:20px 0px;" name="auth">Авторизация</button>
                        </center>
                        <center>
                            <span style="font-size: 11px;">Забыли пароль? <a href="#" class="js-pUp__openeer" data-pup-id="pUp-rem-pass">Восстановите его!</a></span>
                        </center>
                    </form>

                </div>
            </div>

            <div class="three_cols">
            </div>

        </div>

    </div>

    <footer id="footer">
        <div class="top">
            <div class="container menu">
                <ul class="list">
                    <li><a href="#">Мед. учреждения</a></li>
                    <li><a href="#">Врачи</a></li>
                    <li><a href="#">Аптеки</a></li>
                    <li><a href="#">Зож</a></li>
                    <li><a href="#">Красота</a></li>
                    <li><a href="#">Справочники</a></li>
                    <li><a href="#">Мама и ребенок</a></li>
                    <li><a href="#">Реабилитация</a></li>
                    <li><a href="#">Ветеренария</a></li>
                </ul>
            </div>
        </div>

        <div class="bottom">
            <div class="container">
                <div class="left_col">
                    <div class="logo_footer"><a href="#"><img src="/images/logo-footer.png" alt="Медико Фармацевтический портал"/></a></div>
                    <ul class="social">
                        <li class="fb"><a href="#">fb</a></li>
                        <li class="tw"><a href="#">twitter</a></li>
                        <li class="ball"><a href="#">fb</a></li>
                        <li class="video"><a href="#">video</a></li>
                        <li class="google"><a href="#">google</a></li>
                    </ul>
                </div>


                <div class="center_col">
                    <a class="tel" href="tel:88003003232"><span>8 800</span> 555-31-91</a>
                    <p class="copy">Распространение материалов только с согласия редакции. Все права защищены. &copy; 2015 <br/>Эл. почта для связи — <a href="mailto:hello@helpmed.com">hello@helpmed.com</a>.</p>
                </div>

                <div class="right_col">
                    <ul class="submenu">
                        <li><a href="#">Контактная информация</a></li>
                        <li><a href="#">О портале</a></li>
                        <li><a href="#">Обратная связь</a></li>
                    </ul>
                    <div class="subscribe">
                        <span class="georgia title">Подписка</span>
                        <div class="forma">
                            <form action="#">
                                <input type="text" class="text" placeholder="hello@helpmed.com"/>
                                <span class="button"><input type="submit" class="submit" value="Подписаться"></span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>



<div class="pUps js-pUp_m">
    <div class="pUps__bg js-pUp_m__bg js-pUp_m__bg_not-x"></div>
    <div class="pUps__w pUps__w_nX pUps__w_auth">
        <span class="pUps__x js-pUp_m__x"></span>
        <div class="pUps__reg js-pUp_m__reg">
            <!-- list popups of ID -->
            <div class="pUps__item pUps__item_rem-pass js-pUp_m__item js-valid" id="pUp-rem-pass">
                <div class="pUps__item_title-txt">
                    <span class="pUps__item-title">Восстановление пароля</span>
                </div>

                <form id="send_password_form" action="">
                    <input value="" placeholder="Эл.почта" class="box-fields__input js-valid-i" name="email" type="text">
                </form>
                <div class="pUps__item_err-txt js-valid-err"></div>
                <input id="send_password" value="Отправить" class="pUps__btn js-valid-e" type="submit">
            </div>

            <div class="pUps__item pUps__item_rem-pass js-pUp_m__item js-valid" id="pUp-sent-info-mail">
                <div class="pUps__item_title-txt">
                    <span class="pUps__item-title">Восстановление пароля</span>
                </div>

                <p style="line-height: 24px; padding-bottom: 20px;">Новый пароль был выслан на почту.</p>
            </div>
            <!-- END list popups -->
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/usr/main.js"></script>
<script type="text/javascript" src="/js/script.js"></script>

<!--  SCRIPTS inline  -->
<script type="text/javascript">
    $(document).ready(function(){
        $(".pUps__bg").on("click", function(){
            pUp_m.eHidePup('pUp-auth');
        });
        $("#send_password").on('click', function(e) {
            e.preventDefault();

            $.post('/ajax/forgot_password', $('#send_password_form').serialize(), function (data) {
                if (data.status == 'success') {
                    pUp_m.eHidePup('pUp-rem-pass');
                    pUp_m.eShowPup('pUp-sent-info-mail');
                } else if(data.status == 'error') {
                    var alert_message = "";
                    for (i in data.errors) {
                        alert_message += data.errors[i] + "\r\n";
                    }
                    alert(alert_message);
                }
            }, 'json');
        });
    })
</script>
</body>
</html>
