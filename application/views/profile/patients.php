<!--  END - MAIN MENU -->
<div class="content box-patients js-sort"><?php
    if(!empty($patients))
    {
        ?><div class="box-w50">
            <span class="title title-box box-w50__1">Список ВАШИХ пациентов</span>
            <ul class="b-filter-sort box-w50__2 js-sort-param">
                <li class="b-filter-sort__i b-filter-sort__i_label">Сортировать по: </li>
                <li class="b-filter-sort__i b-filter-sort__i_param js-sort-table__param-p">
                    <a class="b-filter-sort__i-i js-sort-table__param" href="#" id="js-sort-table__fio">ФАМИЛИИ</a>
                </li>
                <li class="b-filter-sort__i b-filter-sort__i_param js-sort-table__param-p">
                    <a class="b-filter-sort__i-i js-sort-table__param" id="js-sort-table__bd" href="#">Дате обращения</a>
                </li>
            </ul>
        </div>
        <div class="b-table">
            <table class="w6 js-sort-table" id="js-sort-table">
                <thead>
                    <tr>
                        <th class="js-sort-table__fio">ФИО</th>
                        <th>e-mail</th>
                        <th>Телефон</th>
                        <th>Город</th>
                        <th class="js-sort-table__bd">Дата обращения</th>
                        <th>ЛС</th>
                    </tr>
                </thead>
                <tbody><?php
                    foreach($patients as $patient)
                    {
                        ?><tr>
                            <td>
                                <a href=""><?=$patient['USER_NAME']?></a>
                            </td>
                            <td>
                                <a href="mailto:<?=$patient['USER_EMAIL']?>"><?=$patient['USER_EMAIL']?></a>
                            </td>
                            <td><?=$patient['USER_PHONE']?></td>
                            <td><?=$patient['USER_CITY']?></td>
                            <td><?=$patient['RECEPTION_DATE']?></td>
                            <td class="b-table__E"><a href="/profile/post/<?=$patient['USER_ID']?>">М</a></td>
                        </tr><?php
                    }
                ?></tbody>
            </table>
        </div><?php
    }
    else
    {
        ?><div class="box-w50">
            <span class="title title-box box-w50__1">Список ВАШИХ пациентов пока пуст</span>
        </div><?php
    }
?></div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->