<!--  END - MAIN MENU -->
<div class="content"> 
    <div class="box box_med-card">
        <form action="/profile/changeMedCard" onsubmit="return mfp.ajaxLoader(this)">
            <span class="title title-box">Медицинская карта</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Страховая медицинская организация</span>
                    <input type="text" value="<?=$med_card['ORGANIZATION']?>" class="box-fields__input" name="organization"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Номер страхового полиса ОМС</span>
                    <input type="text" value="<?=$med_card['OMS_NUMBER']?>" class="box-fields__input" name="oms-number"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">СНИЛС</span>
                    <input type="text" value="<?=$med_card['SNILS']?>" class="box-fields__input" name="snils"/>
                </div>
                 <div class="box-fields input_text">
                    <span class="box-fields__label">Код льготы</span>
                    <input type="text" value="<?=$med_card['EXEMPTION_CODE']?>" class="box-fields__input" name="exemption-code"/>
                </div>
            </div>
            <span class="title title-box title-box_inner">Личные данные</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фамилия</span>
                    <input type="text" value="<?= (!empty($med_card['FAMILY']) ? $med_card['FAMILY'] : (!empty($user_data['LASTNAME']) ? $user_data['LASTNAME'] : "" ))?>" class="box-fields__input" name="family"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Имя</span>
                    <input type="text" value="<?=(!empty($med_card['NAME']) ? $med_card['NAME'] : (!empty($user_data['FIRSTNAME']) ? $user_data['FIRSTNAME'] : "" ))?>" class="box-fields__input" name="name"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Отчество</span>
                    <input type="text" value="<?=(!empty($med_card['FATHER_NAME']) ? $med_card['FATHER_NAME'] : (!empty($user_data['MIDDLENAME']) ? $user_data['MIDDLENAME'] : "" ))?>" class="box-fields__input" name="father-name"/>
                </div>
                 <div class="box-fields input_text js-date_box">
                     <span class="box-fields__label">Дата рождения</span>
                    <input type="text" value="<?=$med_card['BIRTH_DATE']?>" class="box-fields__input box-fields__input_date js-date-i" name="birth-date"/>
                    <span class="box-fields__date-init js-date-e"></span>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Адрес постоянного места жительства</span>
                    <textarea class="box-fields__input box-fields__text-area" name="life-addr"><?=$med_card['LIFE_ADDR']?></textarea>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Адрес регистрации по месту пребывания</span>
                    <textarea class="box-fields__input box-fields__text-area" name="reg-addr"><?=$med_card['REG_ADDR']?></textarea>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Домашний телефон</span>
                    <input type="text" value="<?=$med_card['HOME_PHONE']?>" class="box-fields__input js-tel" name="home-phone"/>
                </div>
                 <div class="box-fields input_text">
                    <span class="box-fields__label">Мобильный телефон</span>
                    <input type="text" value="<?=(!empty($med_card['MOBILE_PHONE']) ? $med_card['MOBILE_PHONE'] : (!empty($user_data['PHONE']) ? $user_data['PHONE'] : "" ))?>" class="box-fields__input js-tel" name="mobile-phone"/>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Документ, удостоверяющий право на льготное обеспечение (наименование, №, серия, дата, кем выдан)</span>
                    <input value="<?=$med_card['EXEMPTION_DOCUMENT']?>" class="box-fields__input" type="text" name="exemption-document"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Инвалидность</span>
                    <input type="text" value="<?=$med_card['DISABILITY']?>" class="box-fields__input" name="disability"/>
                </div>
                 <div class="box-fields input_text">
                    <span class="box-fields__label">Характер работы</span>
                    <input type="text" value="<?=$med_card['WORK_CHAR']?>" class="box-fields__input" name="work-char"/>
                </div>
            </div>

<!--            Начало дополнения -->

            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Профессия</span>
                    <input type="text" value="<?=$med_card['PROFESSION']?>" class="box-fields__input" name="profession"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Должность</span>
                    <input type="text" value="<?=$med_card['POSITION']?>" class="box-fields__input" name="position"/>
                </div>
            </div>

            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Иждивенец</span>
                    <input type="text" value="<?=$med_card['DEPENDENT']?>" class="box-fields__input" name="dependent"/>
                </div>
            </div>

            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Группа крови</span>
                    <input type="text" value="<?=$med_card['BLOOD_TYPE']?>" class="box-fields__input" name="blood_type"/>
                </div>
            </div>

            <div class="box-fields-row">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Лекарственная непереносимость</span>
                    <textarea class="box-fields__input" name="intolerance" id="" style="width: 100%;" rows="10"><?=$med_card['INTOLERANCE']?></textarea>
                </div>
            </div>

            <div class="clear"></div>
            <!--  BLOCK - ADD FILES -->
            <div class="box b-add-f">
                <span class="title title-box">Документы и файлы</span>
                <p class="paragraph paragraph_prew">Постарайтесь, чтобы размер файла не превышал <span class="b-add-f__limit">20 Мб</span> и файл был в одном из следующих форматов - jpg, png, gif, tif, pdf, docx, doc, rtf, txt, ppt, pptx, xls, xlsx, odf.</p>
                <div class="b-add-f-reg js-add-files">
                	<ul class="b-add-f-list js-add-list"><?php
                        foreach($files as $file)
                        {
                            ?><li class="b-add-f-list__i js-add-li" data-img-path="<?=$file['FILE']?>">
                                <img src="<?=$file['FILE']?>"/>
                                <input type="hidden" class="_hidden-id" value="<?=$file['FILE']?>"/>
                                <span class="b-add-f-list__i-delete jd-add-f_delete" data-pup-id="pUp-del-document"></span>
                            </li><?php
                        }
                        ?><li class="b-add-f-list__i b-add-f-list__i_add-new js-add-f"></li>
                	</ul>
                </div>
            </div>
            <!--  END - BLOCK - ADD FILES -->
            <div class="clear"></div>
            <input type="submit" value="Сохранить изменения" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
        </form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->