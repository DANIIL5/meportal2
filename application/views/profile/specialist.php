<!-- END - MAIN MENU -->
<div class="content"> 
    <span class="title title-box">ЛИЧНЫЕ ДАННЫЕ</span>
    <div class="box box_private-data">
        <form action="/profile/changeSpecialist" onsubmit="return mfp.ajaxLoader(this)">
            <div class="box box_avar js-avatar" style="margin-top: 40px;margin-bottom: 40px;">
                <div class="title title-box">Фотография</div>
                <div class="box_avar__image js-avatar__img js-avatar_init <?php if(!empty($specialist['PHOTO'])) echo 'notEmpty'?>">
                    <img src="<?=$specialist['PHOTO']?>"/>
                    <input type="hidden" class="_path-input" value="<?=$specialist['PHOTO']?>"/>
                    <span class="b-add-f-list__i-delete" data-pup-id="pUp-del-user-ava" onclick="pUp_m.eGetId(this,'Удалить текущее фото?')" <?php if(empty($specialist['PHOTO'])) echo 'style="display:none;"'?>></span>
                </div>
                <div class="box_avar__inf">
                    <div class="box_avar__inf-alert">Постарайтесь, чтобы размер картинки не превышал 2 Мб и картинка была в одном из следующих форматов - jpg, png, gif.</div>
                    <input type="submit" value="ВЫБРАТЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour js-avatar_init" onclick="return false">
                </div>
                <input type="file" name="ava" class="js-avatar__form-i dnone" accept="image/*"/>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Фамилия</span>
                    <input type="text" value="<?=$specialist['SECOND_NAME']?>" class="box-fields__input" name="second-name"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Имя</span>
                    <input type="text" value="<?=$specialist['FIRST_NAME']?>" class="box-fields__input" name="first-name"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Отчество</span>
                    <input type="text" value="<?=$specialist['LAST_NAME']?>" class="box-fields__input" name="last-name"/>
                </div>
            </div>
            <span class="title title-box title-box_inner">Место работы</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields box-fields_group">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Должность</span>
                        <input type="text" value="<?=$specialist['POSITION']?>" class="box-fields__input" name="position"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Описание</span>
                        <textarea class="box-fields__input box-fields__text-area" name="description"><?=$specialist['DESCRIPTION']?></textarea>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочий телефон</span>
                        <input type="text" value="<?=$specialist['WORK_PHONE']?>" class="box-fields__input js-tel" name="work-phone"/>
                    </div>
                    <div class="box-fields input_text b-filter-btn__list-i specialist">
                        <span class="box-fields__label">Место работы</span>
                        <select name="branch-id" class="b-filter-btn__select js-select specialist" data-smart-positioning="false">
                            <option data-branchname="Выберите...">Выберите...</option><?php
                            foreach($branches as $branch)
                            {
                                ?><option value="<?=$branch['ID']?>" <?php if($specialist['BRANCH_ID']==$branch['ID']) echo 'selected' ?>>
                                <?=$branch['BRAND'].", "
                            .$branch['ENTITY']
                            . (!empty($branch['get_addresses']['actual']) ? " (".implode(', ', $branch['get_addresses']['actual']).")" : "" ) ?></option><?php
                            }
                        ?></select>
                    </div>
                </div>
                <div class="box-fields box-fields_group">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Специальность</span>
                        <input type="text" value="<?=$specialist['SPECIALIZATION']?>" class="box-fields__input" name="specialization"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Специализация</span>
                        <select name="specialization_id" class="b-filter-btn__select js-select specialist" data-smart-positioning="false">
                            <option data-branchname="Выберите...">Выберите...</option><?php
                            foreach($specialisations as $specialisation)
                            {
                                ?><option value="<?=$specialisation['ID']?>" <?php if($specialist['SPECIALIZATION_ID']==$specialisation['ID']) echo 'selected' ?>><?=$specialisation['NAME']?></option><?php
                            }
                            ?></select>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Стаж по специальности</span>
                        <input type="text" value="<?=$specialist['SPECIALIZATION_EXPERIENCE']?>" class="box-fields__input" name="specialization-experience"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Общий стаж</span>
                        <input type="text" value="<?=$specialist['TOTAL_EXPERIENCE']?>" class="box-fields__input" name="total-experience"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Мобильный телефон</span>
                        <input type="text" value="<?=$specialist['MOBILE_PHONE']?>" class="box-fields__input js-tel" name="mobile-phone"/>
                    </div>
                </div>
            </div>
            <span class="title title-box title-box_inner">Адрес</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Индекс</span>
                    <input type="text" value="<?=$specialist['ADDRESS_INDEX']?>" class="box-fields__input" name="address_index"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Область / Регион</span>
                    <input type="text" value="<?=$specialist['ADDRESS_REGION']?>" class="box-fields__input" name="address_region"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Населенный пункт</span>
                    <input type="text" value="<?=$specialist['ADDRESS_CITY']?>" class="box-fields__input" name="address_city"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Улица</span>
                    <input type="text" value="<?=$specialist['ADDRESS_STREET']?>" class="box-fields__input" name="address_street"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Дом</span>
                    <input type="text" value="<?=$specialist['ADDRESS_HOUSE']?>" class="box-fields__input" name="address_house"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Строение</span>
                    <input type="text" value="<?=$specialist['ADDRESS_BUILDING']?>" class="box-fields__input" name="address_building"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Квартира</span>
                    <input type="text" value="<?=$specialist['ADDRESS_OFFICE']?>" class="box-fields__input" name="address_office"/>
                </div>
            </div>
            <span class="title title-box title-box_inner">Рабочее время</span>
            <div class="box-branches_add">
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПН)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$specialist['timeFrom']['MONDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="monday-work-from"/>
                            <input type="text" value="<?=$specialist['timeTo']['MONDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="monday-work-to"/>
                        </div>
                    </div>
                     <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$specialist['timeFrom']['TUESDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="tuesday-work-from"/>
                            <input type="text" value="<?=$specialist['timeTo']['TUESDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="tuesday-work-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СР)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$specialist['timeFrom']['WEDNESDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="wednesday-work-from">
                            <input type="text" value="<?=$specialist['timeTo']['WEDNESDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="wednesday-work-to">
                        </div>
                    </div>
                     <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ЧТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$specialist['timeFrom']['THURSDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="thursday-work-from"/>
                            <input type="text" value="<?=$specialist['timeTo']['THURSDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="thursday-work-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ПТ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$specialist['timeFrom']['FRIDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="friday-work-from"/>
                            <input type="text" value="<?=$specialist['timeTo']['FRIDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="friday-work-to"/>
                        </div>
                    </div>
                     <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (СБ)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$specialist['timeFrom']['SATURDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="saturday-work-from"/>
                            <input type="text" value="<?=$specialist['timeTo']['SATURDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="saturday-work-to"/>
                        </div>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Рабочее время (ВС)</span>
                        <div class="box-fields__input_cell-2">
                            <input type="text" value="<?=$specialist['timeFrom']['SUNDAY_WORK_FROM']?>" class="box-fields__input js-time-input" name="sunday-work-from"/>
                            <input type="text" value="<?=$specialist['timeTo']['SUNDAY_WORK_TO']?>" class="box-fields__input js-time-input" name="sunday-work-to"/>
                        </div>
                    </div>
                </div>
            </div>
            <span class="title title-box title-box_inner">ОБРАЗОВАНИЕ</span>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">ВУЗ</span>
                    <input type="text" value="<?=$specialist['UNIVERSITY']?>" class="box-fields__input" name="university"/>
                </div>
                <div class="box-fields input_text">
                    <span class="box-fields__label">Серия и номер диплома</span>
                    <input type="text" value="<?=$specialist['DIPLOMA_SERIES_AND_NUMBER']?>" class="box-fields__input" name="diploma-series-and-number"/>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <span class="box-fields__label">Факультет</span>
                    <input type="text" value="<?=$specialist['FACULTY']?>" class="box-fields__input" name="faculty"/>
                </div>
                <div class="box-fields input_text js-date_box">
                    <span class="box-fields__label">Дата окончания</span>
                    scr
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields box-fields_group">
                    <span class="title title-group">Интернатура</span>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Заведение</span>
                        <input type="text" value="<?=$specialist['INTERNSHIP_INSTITUTION']?>" class="box-fields__input" name="internship-institution"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Специальность</span>
                        <input type="text" value="<?=$specialist['INTERNSHIP_SPECIALITY']?>" class="box-fields__input" name="internship-speciality"/>
                    </div>
                    <div class="box-fields input_text box-fields_period">
                        <span class="box-fields__label">Период</span>
                        <div class="box-fields_period-reg">
                            <div class="box-fields_period-reg_cover js-date_box"> 
                                <span class="box-fields__label-p">с</span>
                                <input type="text" value="<?=$specialist['INTERNSHIP_DATE_FROM']?>" class="box-fields__input box-fields__input_start js-date-i" name="internship-date-from"/>
                                <span class="box-fields__date-init js-date-e"></span>
                            </div>
                            <div class="box-fields_period-reg_cover js-date_box"> 
                                <span class="box-fields__label-p box-fields__label-p">по</span>
                                <input type="text" value="<?=$specialist['INTERNSHIP_DATE_TO']?>" class="box-fields__input box-fields__input_finish js-date-i" name="internship-date-to"/>
                                <span class="box-fields__date-init js-date-e">Дата рождения</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-fields box-fields_group">
                    <span class="title title-group">Ординатура</span>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Заведение</span>
                        <input type="text" value="<?=$specialist['RESIDENCY_INSTITUTION']?>" class="box-fields__input" name="residency-institution"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Специальность</span>
                        <input type="text" value="<?=$specialist['RESIDENCY_SPECIALITY']?>" class="box-fields__input" name="residency-speciality"/>
                    </div>
                    <div class="box-fields input_text box-fields_period">
                        <span class="box-fields__label">Период</span>
                        <div class="box-fields_period-reg">
                            <div class="box-fields_period-reg_cover js-date_box">  
                                <span class="box-fields__label-p">с</span>
                                <input type="text" value="<?=$specialist['RESIDENCY_DATE_FROM']?>" class="box-fields__input box-fields__input_start js-date-i" name="residency-date-from"/>
                                <span class="box-fields__date-init js-date-e"></span>
                            </div>
                            <div class="box-fields_period-reg_cover js-date_box"> 
                                <span class="box-fields__label-p">по</span>
                                <input type="text" value="<?=$specialist['RESIDENCY_DATE_TO']?>" class="box-fields__input box-fields__input_finish js-date-i" name="residency-date-to"/>
                                <span class="box-fields__date-init js-date-e"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_cell-3-date">
                <div class="box-fields-group">
                    <span class="title title-group">профессиональная переподготовка</span>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Заведение</span>
                        <input type="text" value="<?=$specialist['PROFESSIONAL_RETRAINING_INSTITUTION']?>" class="box-fields__input" name="professional-retraining-institution"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Специальность</span>
                        <input type="text" value="<?=$specialist['PROFESSIONAL_RETRAINING_SPECIALITY']?>" class="box-fields__input" name="professional-retraining-speciality"/>
                    </div>
                    <div class="box-fields input_text input_select">
                        <span class="box-fields__label">Число лет</span>
                        <select class="box-fields__input b-def-select js-def-select" name="professional-retraining-years">
                            <option <?php if($specialist['PROFESSIONAL_RETRAINING_YEARS']==1) echo 'selected'?>>1</option>
                            <option <?php if($specialist['PROFESSIONAL_RETRAINING_YEARS']==2) echo 'selected'?>>2</option>
                            <option <?php if($specialist['PROFESSIONAL_RETRAINING_YEARS']==3) echo 'selected'?>>3</option>
                        </select>
                    </div>
                </div>
            </div><?php
                foreach ($refreshers as $refresher)
                {
                    ?><span class="title title-box title-box_inner">ПОВЫШЕНИЯ КВАЛИФИКАЦИИ</span>
                    <div class="box-fields-row box-fields-row_cell-2">
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Учреждения</span>
                            <input type="text" value="<?=$refresher['INSTITUTION']?>" class="box-fields__input" name="refresher-training-institution[]"/>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Срок</span>
                            <input type="text" value="<?=$refresher['PERIOD']?>" class="box-fields__input" name="refresher-training-period[]"/>
                        </div>
                    </div>
                    <div class="box-fields-row box-fields-row_cell-2">
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Специальность</span>
                            <input type="text" value="<?=$refresher['SPECIALITY']?>" class="box-fields__input" name="refresher-training-speciality[]"/>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Количество часов</span>
                            <input type="text" value="<?=$refresher['HOURS']?>" class="box-fields__input" name="refresher-training-hours[]"/>
                        </div>
                    </div>
                    <div class="box-fields-row box-fields-row_cell-2">
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Номер документа</span>
                            <input type="text" value="<?=$refresher['DOCUMENT_NUMBER']?>" class="box-fields__input" name="refresher-training-document-number[]"/>
                        </div>                   
                        <div class="box-fields input_text js-date_box">
                            <span class="box-fields__label">Дата окончания</span>
                            <input type="text" value="<?=$refresher['GRADUATION_DATE']?>" class="box-fields__input box-fields__input_date js-date-i" name="refresher-training-graduation-date[]"/>
                            <span class="box-fields__date-init js-date-e"></span>
                        </div>
                    </div>
                    <input type="hidden" name="refresher-id[]" value="<?=$refresher['ID']?>"/><?php
                }
            ?><div class="refresher">
                <span class="title title-box title-box_inner">ПОВЫШЕНИЕ КВАЛИФИКАЦИИ</span>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Учреждения</span>
                        <input type="text" class="box-fields__input" name="refresher-training-institution[]"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Срок</span>
                        <input type="text" class="box-fields__input" name="refresher-training-period[]"/>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Специальность</span>
                        <input type="text" class="box-fields__input" name="refresher-training-speciality[]"/>
                    </div>
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Количество часов</span>
                        <input type="text" class="box-fields__input" name="refresher-training-hours[]"/>
                    </div>
                </div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Номер документа</span>
                        <input type="text" class="box-fields__input" name="refresher-training-document-number[]"/>
                    </div>                   
                    <div class="box-fields input_text js-date_box">
                        <span class="box-fields__label">Дата окончания</span>
                        <input type="text" class="box-fields__input box-fields__input_date js-date-i" name="refresher-training-graduation-date[]"/>
                        <span class="box-fields__date-init js-date-e"></span>
                    </div>
                </div>
                <input type="hidden" name="refresher-id[]" value="e"/>
            </div>
            <input type="submit" value="Добавить еще" class="btn-lk btn-lk_inner" onclick="return mfp.addRefresher(this)"/>
            <div class="clear"></div>
            <!--  BLOCK - ADD FILES -->
            <div class="box b-add-f">
                <span class="title title-box">Документы и файлы</span>
                <p class="paragraph paragraph_prew">Постарайтесь, чтобы размер файла не превышал <span class="b-add-f__limit">20 Мб</span> и файл был в одном из следующих форматов - jpg, png, gif, tif, pdf, docx, doc, rtf, txt, ppt, pptx, xls, xlsx, odf.</p>
                <div class="b-add-f-reg js-add-files">
                	<ul class="b-add-f-list js-add-list"><?php
                        foreach($files as $file)
                        {
                            ?><li class="b-add-f-list__i js-add-li" data-img-path="<?=$file['FILE']?>">
                                <img src="<?=$file['FILE']?>"/>
                                <input type="hidden" class="_hidden-id" value="<?=$file['FILE']?>"/>
                                <span class="b-add-f-list__i-delete jd-add-f_delete" data-pup-id="pUp-del-document"></span>
                            </li><?php
                        }
                        ?><li class="b-add-f-list__i b-add-f-list__i_add-new js-add-f"></li>
                	</ul>
                </div>
            </div>
            <!--  END - BLOCK - ADD FILES -->
            <div class="clear"></div>
            <input type="submit" value="Сохранить изменения" class="btn-lk btn-lk_save js-pUp__openeer" data-pup-id="pUp-save-thx"/>
        </form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->