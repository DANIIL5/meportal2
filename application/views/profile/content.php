<!--  END - MAIN MENU -->
<div class="content box-branches js-b-tgl">
        <span class="title title-box title-box_inner">Контент</span>

    <input type="submit" value="Добавить фото" onclick="window.location.href = '/profile/add_image/'" class="btn-lk btn-lk_save js-pUp__openeer"/>
    <input type="submit" value="Добавить видео" onclick="window.location.href = '/profile/add_video/'" class="btn-lk btn-lk_save js-pUp__openeer"/>
    <input type="submit" value="Добавить публикацию" onclick="window.location.href = '/profile/add_publication/'" class="btn-lk btn-lk_save js-pUp__openeer"/>

    <? if (!empty($videos)) { ?>
    <div style="margin-top: 20px;">
        <h1>Видео</h1>
        <? foreach ($videos as $video) { ?>
        <div style="width: 30%; margin-right: 3%; float: left; margin-top: 10px; position: relative;">
            <iframe style="width: 100%; height: 100%;" src="<?=$video['VALUE']?>" frameborder="0" allowfullscreen></iframe>
<!--            <img src="" alt="" style="width: 100%; height: auto;">-->
            <p style="position: absolute; bottom: 0px; margin-bottom: 2px; padding: 5px; width: 100%; background: rgba(60, 60, 60, 0.7);">
                <a style="color: white;" href="/profile/delete_video/<?=$video['ID']?>">
                    Удалить
                </a>
                &nbsp | &nbsp
                <a style="color: white;" href="/profile/add_video/<?=$video['ID']?>">
                    Редактировать
                </a>
            </p>
        </div>
        <? } ?>

        <div style="clear: both"></div>
    </div>
    <? } ?>

    <? if (!empty($images)) { ?>
        <div style="margin-top: 20px;">
            <h1>Фото</h1>
            <? foreach ($images as $image) { ?>
                <div style="width: 30%; margin-right: 3%; float: left; margin-top: 10px; position:relative;">
                    <img src="/images/licenses/<?=$image['VALUE']?>" alt="" style="width: 100%; height: auto;">
                    <p style="position: absolute; bottom: 0px; margin-bottom: 2px; padding: 5px; width: 100%; background: rgba(60, 60, 60, 0.7);">
                        <a style="color: white;" href="/profile/delete_image/<?=$image['ID']?>">
                            Удалить
                        </a>
                        &nbsp | &nbsp
                        <a style="color: white;" href="/profile/add_image/<?=$image['ID']?>">
                            Редактировать
                        </a>
                    </p>
                </div>
            <? } ?>

            <div style="clear: both"></div>
        </div>
    <? } ?>

    <? if (!empty($articles)) { ?>
        <div style="margin-top: 20px;">
            <h1>Публикации</h1>
            <? foreach ($articles as $article) { ?>
                <div style="width: 30%; margin-right: 3%; margin-top: 10px;">
                    <h3><?=$article['NAME']?></h3>
                    <p><?=mb_substr($article['TEXT'], 0, 100).'...'?></p>
                    <p style="font-style: italic">
                        <a href="/profile/delete_publication/<?=$article['ID']?>">Удалить публикацию</a><br>
                        <a href="/profile/add_publication/<?=$article['ID']?>">Редактировать публикацию</a>
                    </p>
                </div>
            <? } ?>
        </div>
    <? } ?>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->