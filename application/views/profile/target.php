<!--  END - MAIN MENU -->
    <div class="content"><?php
        if($target['ID']!=-1)
        {
            ?><form action="/profile/updateTarget" onsubmit="return mfp.ajaxLoader(this)"><?php
        }
        else
        {
            ?><form action="/profile/sendTarget" onsubmit="return mfp.ajaxLoader(this)"><?php
        }
            ?><div class="box box_new-target">
                <div class="title title-box">Новая цель</div>
                <div class="box-fields-row box-fields-row_cell-2">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Название</span>
                        <input type="text" name="name" class="box-fields__input" value="<?=$target['NAME']?>"/>
                    </div>
                </div>
                    <div class="box-fields-row box-fields-row_cell-2">
                        <div class="box-fields input_text js-date_box">
                            <span class="box-fields__label">Дата начала</span>
                            <input type="text" name="start" class="box-fields__input box-fields__input_date js-date-i future" value="<?=(!empty($target['START'])) ? date('d:m:Y', strtotime($target['START'])) : $target['DATE']?>"/>
                            <span class="box-fields__date-init js-date-e"></span>
                        </div>
                        <div class="box-fields input_text js-date_box">
                            <span class="box-fields__label">Дата окончания</span>
                            <input type="text" name="time" class="box-fields__input box-fields__input_date js-date-i future" value="<?=(!empty($target['TIME'])) ? date('d:m:Y', strtotime($target['TIME'])) : $target['DATE']?>"/>
                            <span class="box-fields__date-init js-date-e"></span>
                        </div>
                    </div>
                    <div class="box-fields-row box-fields-row_cell-2">
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Время начала</span>
                            <input type="text" name="time_from" class="box-fields__input time" placeholder="09:00" value="<?=(!empty($target['START'])) ? date('H:i', strtotime($target['START'])) : "12:00"?>"/>
                        </div>
                        <div class="box-fields input_text">
                            <span class="box-fields__label">Время окончания</span>
                            <input type="text" name="time_to" class="box-fields__input time" placeholder="09:00" value="<?=(!empty($target['TIME'])) ? date('H:i', strtotime($target['TIME'])) : "12:00"?>"/>
                        </div>
                    </div>
                <div class="box-fields-row">
                    <div class="box-fields input_text">
                        <span class="box-fields__label">Сообщение</span>
                        <textarea class="box-fields__input box-fields__input_new-target" name="text"><?=$target['TEXT']?></textarea>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <input type="hidden" class="_hidden-id" value="<?=$target['ID']?>" name="_hidden-id"/><?php
            if($target['ID']!=-1)
            {
                ?><input type="submit" value="Изменить" class="btn-lk btn-lk_create js-pUp__openeer" data-pup-id="pUp-save-thx"/><?php
            }
            else
            {
                ?><input type="submit" value="Создать" class="btn-lk btn-lk_create js-pUp__openeer" data-pup-id="pUp-save-thx"/><?php
            }
        ?></form>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->