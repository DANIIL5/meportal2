    <!--  END - MAIN MENU -->
    <div class="content js-pUp__openeer">
        <div class="box box_favorite_doctor">
            <div class="box-fields-row box-fields-row_cell-2">
                <div class="box-fields input_text">
                    <div class="title title-box">Врачи</div>
                </div>
            </div>
            <div class="box-fields-row">
                <div class="box-fields input_text box-fields_search">
                    <form action="/profile/institution_specialists_search" method="POST">
                        <input type="text" value="" placeholder="Искать среди врачей" class="box-fields__input box-fields__input_search" name="search"/>
                    </form>
                </div>
            </div>
            <div class="box-fields-row box-fields-row_inner">
                <div class="b-row-tab">
                    <ul class="b-tab-nav">
                        <li class="b-tab-nav__i <?php if($_SERVER['PHP_SELF']=='/index.php/profile/liked_specialists') echo 'b-tab-nav__i_active' ?>"><a href="/profile/institution_specialists">Все</a></li><?php
                        foreach($count_institution_specialists_specialization_id as $key=>$liked)
                        {
                            ?><li class="b-tab-nav__i <?php if($_SERVER['PHP_SELF']=='/index.php/profile/institution_specialists_filter/'.$liked['SPECIALIST_SPECIALIZATION_ID']) echo 'b-tab-nav__i_active' ?>"><a href="/profile/institution_specialists_filter/<?=$liked['SPECIALIST_SPECIALIZATION_ID']?>"><?=$count_institution_specialists_specialization[$key]['SPECIALIST_SPECIALIZATION']?></a>
                                <span class="b-tab-nav__i-count">(<?=$liked['COUNT']?>)</span>
                            </li><?php
                        }
                    ?></ul>
                    <div class="clear"></div>
                    <ul class="b-tab-content"><?php
                        foreach($institution_specialists as $liked)
                        {
                            ?><li class="b-tab-content__i specialist" data-target-id="<?=$liked['ID']?>">
                                <div class="b-tab-content__i-avatar">
                                    <div class="b-tab-content__i-avatar_cover"><?php
                                        if(!empty($liked['SPECIALIST_PHOTO']))
                                        {
                                            ?><img src="<?=$liked['SPECIALIST_PHOTO']?>"/><?php
                                        }
                                        else
                                        {
                                            ?><img src="/images/bg/def-ava.png" style="margin-top:35%;width:30%;"/><?php
                                        }
                                    ?></div>
                                    <input type="hidden" class="_hidden-id" value="<?=$liked['ID']?>"/>
                                    <span class="b-tab-content__i-delete" onclick="pUp_m.eGetId(this,'Удалить врача?')" data-pup-id="pUp-del-institution-specialist"></span>
                                </div>
                                <div class="b-tab-content__i-name" onclick="window.location.pathname='/profile/specialistDetail/<?=$liked['SPECIALIST_ID']?>'"><?=$liked['SPECIALIST_LAST_NAME']?> <?=mb_substr($liked['SPECIALIST_FIRST_NAME'],0,1)?>. <?=mb_substr($liked['SPECIALIST_SECOND_NAME'],0,1)?>.</div>
                                <div class="b-tab-content__i-about" onclick="window.location.pathname='/profile/specialistDetail/<?=$liked['SPECIALIST_ID']?>'"><?=$liked['SPECIALIST_SPECIALIZATION']?></div>
                            </li><?php
                        }?><li class="b-tab-content__i b-tab-content__i_add-new">
                            <div class="b-tab-content__i-avatar">
                            </div>
                            <div class="b-tab-content__i-name">Добавить</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- //= template/right-panel.html -->
    <!--  FOOTER -->