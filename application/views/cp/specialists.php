
<?php
$this->view('cp/header', ['count' => $unreaded,'count2' => $unreaded2]);
?>

<div class="container">
    <h2 class="sub-header mb_40">Специалисты</h2>

    <a href="/cp/specialists_add" class="btn btn-success">Добавить</a>

</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="table-responsive mt_20 doct">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th style="width: 45%;">ФИО</th>
                        <th>Место работы</th>
                        <th>Специальность</th>
                        <th>Последнее изменение</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($catalog as $item) { ?>
                        <tr id="usrrs_1">
                            <td>
                                <? if (!empty($item['PHOTO'])) { ?>
                                    <img src="<?=$item['PHOTO']?>" alt="" style="max-width: 60px; max-height: 60px;">
                                <? } else { ?>
                                <div style="width: 60px; height: 60px; background: #a1a1a1;">
                                    <? } ?>
                            </td>
                            <td><?=((!empty($item['SECOND_NAME'])) ? $item['SECOND_NAME']." " : "").((!empty($item['FIRST_NAME'])) ? $item['FIRST_NAME']." " : "").((!empty($item['LAST_NAME'])) ? $item['LAST_NAME']." " : "")?></td>
                            <td><?=(!empty($item['BRANCH_ID'])) ? $item['ORGANISATION_NAME'] : $item['BRANCH'] ?></td>
                            <td><?=(!empty($item['SPECIALIZATION_ID'])) ? $item['SPECIALIZATION_NAME'] : $item['SPECIALIZATION'] ?></td>
                            <td><?=(!empty($item['UPDATED_AT'])) ? date('d/m/Y H:i:s',strtotime($item['UPDATED_AT'])) : "" ?></td>
                            <td>
                                <span class="icon_event cat_dell" attr-id="<?=$item['ID']?>"><span class="icon_event_title">Удалить</span></span>
                                <a href="/cp/specialists_edit/<?=$item['ID']?>" class="icon_event cat_remove"><span class="icon_event_title">Изменить</span></a>
                            </td>
                        </tr>
                    <? }?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xs-4 col-md-4">
            <div class="mt_20">
                <form action="" class="fillter_pub">
                    <h4>Фильтр</h4>

                    <div class="item mt_20">
                        <h4>Место работы</h4>
                        <select class="form-control" name="institution_id">
                            <option value="">Не выбрано</option>
                            <? foreach ($filters['institutions'] as $institution) { ?>
                                <option value="<?=$institution['ID']?>" <?=(!empty($_GET['institution_id']) && $_GET['institution_id'] == $institution['ID']) ? "selected" : "" ?>><?=$institution['NAME']?></option>
                            <? } ?>
                        </select>
                    </div>
                    <div class="item">
                        <h4>Специализация	</h4>
                        <select class="form-control" name="specialization_id">
                            <option value="">Не выбрано</option>
                            <? foreach ($filters['specialities'] as $speciality) { ?>
                                <option value="<?=$speciality['ID']?>" <?=(!empty($_GET['specialization_id']) && $_GET['specialization_id'] == $speciality['ID']) ? "selected" : "" ?>><?=$speciality['NAME']?></option>
                            <? } ?>
                        </select>
                    </div>
<!--                    <div class="item">-->
<!--                        <h4>Рабочие время	</h4>-->
<!--                        <select class="form-control">-->
<!--                            <option value="">Понедельник</option>-->
<!--                            <option value="">Вторник</option>-->
<!--                            <option value="">Среда</option>-->
<!--                            <option value="">Четверг</option>-->
<!--                            <option value="">Пятница</option>-->
<!--                            <option value="">Суббота</option>-->
<!--                            <option value="">Воскресенье</option>-->
<!--                        </select>-->
<!--                        <table class="mt_20">-->
<!--                            <tbody>-->
<!--                            <tr>-->
<!--                                <td><input class="form-control datepicker_from" placeholder="С"></td>-->
<!--                                <td><input class="form-control datepicker_to" placeholder="До"></td>-->
<!--                            </tr>-->
<!--                            </tbody>-->
<!--                        </table>-->
<!--                    </div>-->
                    <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                </form>
            </div>
        </div>
    </div>

    <?=$pagination?>

</div>

<!-- Для удаления -->
<div id="dialog-confirm" title="Удаление доктора" style="display: none;">
    <p>
        Вы действительно хотите удалить данного доктора?
    </p>
</div>
<div id="dialog-message" title="Доктор удален" style="display: none;">
    <p>
        Доктор удален
    </p>
</div>
<!-- end Для удаления -->

<script src="/admin/js/js_pages/doctors.js"></script>

<?php
$this->view('cp/footer');
?>	