<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <div class="container">
        <h2 class="sub-header mb_40">Написать сообщение</h2>

        <form id="post_form" action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table">
                <tr>
                    <td>Кому</td>
                    <td>
                        <input type="text" placeholder="Заголовок публикации" class="form-control" name="destinations[]">
                    </td>
                </tr>
                <tr>
                    <td>Тема</td>
                    <td>
                        <input type="text" placeholder="Заголовок публикации" class="form-control" name="theme">
                    </td>
                </tr>
                <tr>
                    <td>Сообщение</td>
                    <td>
                    <textarea class="col-md-12" name="message" rows="10" placeholder="Сообщение"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button id="send_post" type="submit" class="btn btn-success" name="submit">Отправить сообщение</button></td>
                </tr>
            </table>
        </form>
    </div>

    <script>
        $('#send_post').on('click', function (e) {
            e.preventDefault();

            $.post('/cp/sendPost', $('#post_form').serialize(), function (data) {
                if (data.status !=  undefined && data.status == 'success') {
                    window.location.href = "/cp/posts/out";
                } else {
                    console.log(data);
                    alert(data.error);
                }
            }, 'json');
        });
    </script>

<?php
$this->view('cp/footer');
?>