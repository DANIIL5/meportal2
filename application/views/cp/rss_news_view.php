
<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
<div class="container">
    <h2 class="sub-header mb_40">Добавление администратора</h2>
</div>
<div class="container">
    <form action="" method="post" enctype="multipart/form-data">
        <table class="table_rightLeft mt_20 mb_40">
            <tbody>
            <tr>
                <td>Заголовок</td>
                <td colspan="2" width="450">
                    <span><?=(!empty($item['TITLE'])) ? $item['TITLE'] : ""?></span>
                </td>
            </tr>
            <tr>
                <td>Изображение</td>
                <td colspan="2">
                    <span>
                        <? if (!empty($item['IMAGE'])) { ?>
                            <img src="<?=$item['IMAGE']?>" alt="">
                        <? } ?>
                    </span>
                </td>
            </tr>
            <tr>
                <td>Описание</td>
                <td colspan="2">
                    <span><?=(!empty($item['DESCRIPTION'])) ? $item['DESCRIPTION'] : ""?></span>
                </td>
            </tr>
            <tr>
                <td>Ссылка</td>
                <td colspan="2">
                    <span><?=(!empty($item['DATE'])) ? $item['DATE'] : ""?></span>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>


<?php
$this->view('cp/footer');
?>