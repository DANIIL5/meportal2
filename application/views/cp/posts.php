<?php
$this->view('cp/header', ['count' => $unreaded,'count2' => $unreaded2]);
?>

    <div class="container">
        <h2 class="sub-header mb_40">Сообщения</h2>

        <a href="/cp/send_post" class="btn btn-success">Написать сообщение</a>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="b-tab">
                    <div class="b-tab__i">
                        <span class="b-tab__it active"><a href="/cp/posts">Все</a></span>
                        <span class="b-tab__it"><a href="/cp/posts/in">Входящие</a></span>
                        <span class="b-tab__it"><a href="/cp/posts/out">Отправленные</a></span>
                        <span class="b-tab__it"><a href="/cp/posts/liked">Важные</a></span>
                        <span class="b-tab__it"><a href="/cp/posts/delete">Удаленные</a></span>
                       
                    </div>
                    <ul class="list-group mt_20 categories">
                        <? foreach ($posts as $item) { ?>
                        <? preg_match("/Отзыв/", $item['THEME'], $match) ?>
                        <? if(empty($match)): ?>
                        
                            <li id="cat_1" class="list-group-item">
                                <span class="icon_event cat_dell" attr-id=<?=$item['ID']?>><span class="icon_event_title">Удалить</span></span>
                                <a href="/cp/post_read/<?=$item['ID']?>"><span class="icon_event cat_remove" attr-id="1"><span class="icon_event_title">Изменить</span></span></a>
                                <span class="car_title"><?=$item['NAME_SENDER']?>: <?=$item['THEME']?></span>
                            </li>
                        <? endif; ?>
                        <? } ?>
                       
                    </ul>
                </div>
            </div>

            <div class="col-xs-4 col-md-4">
                <div class="mt_20">
                    <form action="" class="fillter_pub">
                        <h4>Категория</h4>
                        <select class="form-control input-lg" name="category_id">
                            <option value=""    >Не выбрано</option>
                            <? foreach ($filters['categories'] as $category) { ?>
                                <option value="<?=$category['ID']?>" <?=(!empty($_GET['category_id']) && $_GET['category_id'] == $category['ID']) ? "selected" : "" ?> ><?=$category['NAME']?></option>
                            <? } ?>
                        </select>
                        <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                    </form>
                </div>
            </div>
        </div>

<!--        --><?//=$pagination?>

    </div>

    <!-- Для удаления -->
    <div id="dialog-confirm" title="Удаление сообщения" style="display: none;">
        <p>
            Вы действительно хотите удалить данное сообщение?
        </p>
    </div>
    <div id="dialog-message" title="категория удалена" style="display: none;">
        <p>
            Категория удалена
        </p>
    </div>
    <!-- end Для удаления -->

    <!-- для извменения -->
    <div id="dialog-confirm_2" title="Удаление категории" style="display: none;">
        <p>
            Вы действительно хотите изменить данную категорию?
        </p>
    </div>
    <div id="dialog-message_2" title="Изменения категории" style="display: none;">
        <p>
            Категория успешно изменена
        </p>
    </div>
    <!-- end для извменения -->


<!--    <script src="/admin/js/js_pages/publication.js"></script>-->
 <script src="/admin/js/js_pages/message.js"></script>
<?php
$this->view('cp/footer');
?>