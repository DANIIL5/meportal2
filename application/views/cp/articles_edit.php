<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>

    <div class="container">
        <h2 class="sub-header mb_40">Добавить публикацию</h2>

        <form action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table">
                <tr>
                    <td>Заголовок</td>
                    <td><input type="text" placeholder="Заголовок публикации" class="form-control" name="name" value="<?=(!empty($item['NAME'])) ? $item['NAME'] : ""?>"></td>
                </tr>
                <tr>
                    <td>Категория</td>
                    <td>
                        <table class="col-12">
                            <tr>
                                <td>
                                    <select id="category" class="form-control" name="category_id">
                                        <? foreach ($articles as $article) {?>
                                            <option value="<?=$article['ID']?>" <?=($item['CATEGORY_ID'] == $article['ID']) ? "selected" : ""?>><?=$article['NAME']?></option>
                                        <? } ?>
                                    </select>
                                    <input id="category_input" style="display: none;" type="input" name="category_name">
                                </td>
                                <td>
                                    <a id="category_link" href="/cp/articles_category_add" onclick="new_category(event)">Добавить новую категорию</a>
                                    <button id="category_button" style="display: none;" type="button" name="category_save">cохранить</button>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>Категория на сайте</td>
                    <td>
                        <table class="col-12">
                            <tr>
                                <td>
                                    <select class="form-control" name="site_category_id">
                                        <option value="">Все категории</option>
                                        <option value="1" <?=($item['SITE_CATEGORY_ID'] == 1) ? "selected" : ""?>>Главная</option>
                                        <option value="2" <?=($item['SITE_CATEGORY_ID'] == 2) ? "selected" : ""?>>Мед учреждения</option>
                                        <option value="3" <?=($item['SITE_CATEGORY_ID'] == 3) ? "selected" : ""?>>Врачи</option>
                                        <option value="12" <?=($item['SITE_CATEGORY_ID'] == 12) ? "selected" : ""?>>Первая помощь</option>
                                        <option value="4" <?=($item['SITE_CATEGORY_ID'] == 4) ? "selected" : ""?>>Аптеки</option>
                                        <option value="5" <?=($item['SITE_CATEGORY_ID'] == 5) ? "selected" : ""?>>Здоровье и красота</option>
                                        <option value="7" <?=($item['SITE_CATEGORY_ID'] == 7) ? "selected" : ""?>>Мама и ребенок</option>
                                        <option value="9" <?=($item['SITE_CATEGORY_ID'] == 9) ? "selected" : ""?>>Ветеренария</option>
                                        <option value="8" <?=($item['SITE_CATEGORY_ID'] == 8) ? "selected" : ""?>>Реабилитация</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                    <textarea id="tinymce" name="text">
                        <?=(!empty($item['TEXT'])) ? $item['TEXT'] : ""?>
                    </textarea>
                    </td>
                </tr>
<!--                <tr>-->
<!--                    <td>Первая помощь?</td>-->
<!--                    <td><input type="checkbox" name="first_help" value="on" --><?//=(!empty($item['FIRST_HELP']) && $item['FIRST_HELP'] == 1) ? "checked" : ""?><!--></td>-->
<!--                </tr>-->
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-success" name="submit">Добавить публикацию</button></td>
                </tr>
            </table>
        </form>
    </div>

    <script>
        tinymce.init({
            selector: '#tinymce',
            height: 500,
            apply_source_formatting : true,
            forced_root_block: false,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>

<?php
$this->view('cp/footer');
?>