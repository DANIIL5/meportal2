<?php
$this->view('cp/header', ['count' => $unreaded,'count2' => $unreaded2]);
?>

<div class="container">
    <h2 class="sub-header mb_40">Категории</h2>

    <a href="/cp/articles_category_add" class="btn btn-success">Добавить новую категорию</a>

   <div class="b-tab">
                <div class="b-tab__i">
                   
                    <span class="b-tab__it"><a href="/cp/articles">Публикации</a></a> </span>
                    <span class="b-tab__it active">Категории</span>
                </div>
    </div>            

    <div class="add_categ">
        <input type="text" placeholder="Введите название категории" class="form-control">
        <button type="button" class="btn btn-success add_new_categ">Добавить</button>
    </div>

    <ul class="list-group mt_20 categories">
        <? foreach ($catalog as $item) { ?>
            <li id="cat_1" class="list-group-item">
                <span class="icon_event cat_dell" attr-id="<?=$item['ID']?>"><span class="icon_event_title">Удалить</span></span>
                <a href="/cp/articles_category_edit/<?=$item['ID']?>"><span class="icon_event cat_remove"><span class="icon_event_title">Изменить</span></span></a>
                <span class="car_title"><?=$item['NAME']?></span>
            </li>
        <? } ?>
    </ul>

    <?=$pagination?>
</div>

<!-- Для удаления -->
<div id="dialog-confirm" title="Удаление категории" style="display: none;">
    <p>
        Вы действительно хотите удалить данного категорию? <span style="color: red">Если вы удалите категорию, все статьи относящиеся к этой категории будут удалены</span>
    </p>
</div>
<div id="dialog-message" title="категория удалена" style="display: none;">
    <p>
        Категория удалена
    </p>
</div>
<!-- end Для удаления -->

<!-- для извменения -->
<div id="dialog-confirm_2" title="Удаление категории" style="display: none;">
    <p>
        Вы действительно хотите изменить данную категорию?
    </p>
</div>
<div id="dialog-message_2" title="Изменения категории" style="display: none;">
    <p>
        Категория успешно изменена
    </p>
</div>
<!-- end для извменения -->

<!-- для Добавления -->
<div id="dialog-confirm_3" title="Добавления категории" style="display: none;">
    <p>
        Вы действительно хотите добавить новую категорию?
    </p>
</div>
<div id="dialog-message_3" title="Добавления категории" style="display: none;">
    <p>
        Категория успешно добавлена
    </p>
</div>
<!-- end для Добавления -->

<script src="/admin/js/js_pages/categories.js"></script>

<?php
$this->view('cp/footer');
?>