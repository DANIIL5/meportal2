<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
<div class="container">
    <h2 class="sub-header mb_40">собранные RSS новости</h2>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 mb_40">
            <div class="b-tab">
                <div class="b-tab__i">
                    <span class="b-tab__it active">Новости </span>
                    <span class="b-tab__it"><a href="/cp/rss_words">Ядро для поиска</a></span>
                </div>
                <div class="table-responsive mt_20 doct table-bord">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Заголовок</th>
                            <th>Дата</th>
                            <th>Активность</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($catalog as $item) { ?>
                            <tr>
                                <td>
                                    <? if (!empty($item['IMAGE'])) { ?>
                                        <img src="<?=$item['IMAGE']?>" alt="" style="max-width: 60px; max-height: 60px;">
                                    <? } else { ?>
                                        <div style="width: 60px; height: 60px; background: #a1a1a1;">
                                    <? } ?>
                                </td>
                                <td><?=$item['TITLE']?></td>
                                <td><?=$item['DATE']?></td>
                                <td><?=$item['ACTIVE']?></td>
                                <td>
                                    <span class="icon_event cat_dell" attr-id="<?=$item['ID']?>"><span class="icon_event_title">Отключить</span></span>
                                    <a href="/cp/rss_news_view/<?=$item['ID']?>" class="icon_event cat_remove"><span class="icon_event_title">Просмотреть</span></a>
                                </td>
                            </tr>
                        <? }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-md-4">
            <div class="mt_20">
                <form action="" method="get" enctype="multipart/form-data" class="fillter_pub">
                    <h4>Фильтр</h4>

                    <div class="item mt_20">
                        <h4>Фильтр 1</h4>
                        <select class="form-control" name="category_id">
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                </form>
            </div>
        </div>
    </div>

    <?=$pagination?>
</div>

<!-- Для удаления -->
<div id="dialog-confirm" title="Переключение активности новости" style="display: none;">
    <p>
        Вы действительно хотите переключить активность данной новости?
    </p>
</div>
<div id="dialog-message" title="Доктор удален" style="display: none;">
    <p>
        Активность новости переключена
    </p>
</div>

<script src="/admin/js/js_pages/rss_news.js"></script>
<!-- end Для удаления -->

<?php
$this->view('cp/footer');
?>	