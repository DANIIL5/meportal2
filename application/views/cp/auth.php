<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
        }
        #errors {
            width: 300px;
            position: absolute;
            text-align: center;
            top: 80px;
            color: red;
            left: 50%;
            margin-left:-150px;
        }
    </style>
</head>
<body>
    <div id="errors">
        <?=!empty($error) ? $error : ""?>
    </div>

    <form action="" method="post">
        <div style="width: 300px; height: 150px; margin: 180px auto; border: 2px #434 solid; border-radius: 5px; padding: 30px;">
            <h1 style="width: 100%; margin: 0 0 10px 0; text-align: center; font-family: Arial; font-size: 22px;">Вход в панель</h1>
            <input type="text" style="width: 100%; height: 26px; margin-bottom: 10px; font-family: Arial; font-size: 18px;" placeholder="Логин" name="login">
            <input type="password" style="width: 100%; height: 26px; margin-bottom: 10px; font-family: Arial; font-size: 18px;" placeholder="Пароль" name="password">
            <input type="submit" style="width: 100px; height: 26px; font-size: 18px; margin-left: 100px; border-radius: 5px;" value="Войти" name="submit">
        </div>
    </form>
</body>
</html>