
<?php
$this->view('cp/header', ['count' => $unreaded,'count2' => $unreaded2]);
?>
<div class="container">
    <h2 class="sub-header mb_40">Администраторы</h2>
    <a href="/cp/admins_add" class="btn btn-success">Добавить</a>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 mb_40">
            <div class="b-tab">
                <div class="b-tab__i">
                    <span class="b-tab__it active">Администраторы </span>
                </div>
                <div class="table-responsive mt_20 doct table-bord">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th style="width: 30%">Логин</th>
                            <th style="width: 58%">Доступы</th>
                            <th style="width: 12%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($admins as $admin) { ?>
                            <tr>
                                <td><?=$admin['NAME']?></td>
                                <td><?=$admin['RIGHTS']?></td>
                                <td>
                                <?if($admin['ID']!=1):?>
                                    <span class="icon_event cat_dell" attr-id="<?=$admin['ID']?>"><span class="icon_event_title">Удалить</span></span>
                                   <?endif;?>
                                    <a href="/cp/admins_edit/<?=$admin['ID']?>" class="icon_event cat_remove"><span class="icon_event_title">Редактировать</span></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-md-4">
            <div class="mt_20">
                <form action="" class="fillter_pub">
                    <h4>Фильтр</h4>

                    <div class="item mt_20">
                        <h4>Фильтр 1</h4>
                        <select class="form-control" name="institution_id">
                            <option value="">Не выбрано</option>
                        </select>
                    </div>
                    <div class="item">
                        <h4>Фильтр 2    </h4>
                        <select class="form-control" name="specialization_id">
                            <option value="">Не выбрано</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Для удаления -->
<div id="dialog-confirm" title="Удаление доктора" style="display: none;">
    <p>
        Вы действительно хотите удалить данного администратора?
    </p>
</div>
<div id="dialog-message" title="Доктор удален" style="display: none;">
    <p>
        Доктор удален
    </p>
</div>

<script src="/admin/js/js_pages/admins.js"></script>
<!-- end Для удаления -->

<?php
$this->view('cp/footer');
?>