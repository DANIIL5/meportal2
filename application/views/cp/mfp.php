<?php
$this->view('cp/header', ['count' => $unreaded]);
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="table-responsive mt_20 doct">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Производитель</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr id="id10036067">
                            <td>1000 трав бальзам д/волос питат. 500мл</td>
                            <td>Первое решение ООО</td>
                        </tr>
                        <tr id="id7771">
                            <td>48 крем, 50г д/рук</td>
                            <td>Lek</td>
                        </tr>
                        <tr id="id115487">
                            <td>5 дней паста противомозольная,10г</td>
                            <td>С-Петербургская ФФ/ ГаленоФарм</td>
                        </tr>
                        <tr id="id10014232">
                            <td>911 Ногтимицин крем 30 мл</td>
                            <td>Твинс Тэк ЗАО/ Стрелец ЗАО</td>
                        </tr>
                        <tr id="id132402">
                            <td>L-Карнитин 20% ра-р 50мл</td>
                            <td>Vitaline</td>
                        </tr>
                        <tr id="id5425">
                            <td>Oral-B зубн.нить, вощен. ментол</td>
                            <td>Oral B Lab.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xs-4 col-md-4">
            <div class="mt_20">
                <form action="" class="fillter_pub">
                    <h4>Фильтр</h4>

                    <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div style="position: absolute; bottom: 0px; width: 100%; height: 240px; background: #eee; box-shadow: 0 0 2px 2px #888;">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Название</th>
            <th>Производитель</th>
            <th>Аптека</th>
            <th>Количество</th>
            <th>Цена</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1000 трав бальзам д/волос питат. 500мл</td>
            <td>Первое решение ООО</td>
            <td>Сердечко</td>
            <td>5</td>
            <td>140 р.</td>
            <td>Отвязать</td>
        </tr>
        <tr>
            <td>1000 трав бальзам д/волос питат. 500мл</td>
            <td>Первое решение ООО</td>
            <td>Ригла</td>
            <td>3</td>
            <td>146 р.</td>
            <td>Отвязать</td>
        </tr>
</div>

<script>

</script>

<?php
$this->view('cp/footer');
?>
