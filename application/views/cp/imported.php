<?php
$this->view('cp/header', ['count' => $unreaded]);
?>

    <div class="container">
        <h2 class="sub-header mb_40">Импортированные лекарства</h2>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-16 col-sm-16 col-md-16">
                <div class="b-tab">
                    <ul class="list-group mt_20 categories">
                        <li id="cat_1" class="list-group-item">
                            <span class="icon_event cat_dell"><span class="icon_event_title">Отвязать</span></span>
                            <span class="car_title">Исходное название | </span>
                            <span class="car_title">Сохраненное название</span>
                        </li>
                        <? foreach ($catalog as $item) { ?>
                            <li id="cat_1" class="list-group-item">
                                <a href="/cp/imported_deatach/<?=$item['ID']?>?back_url=<?=urlencode($_SERVER['REQUEST_URI'])?>"><span class="icon_event cat_dell"><span class="icon_event_title">Отвязать</span></span></a>
                                <span class="car_title"><?=$item['IMPORTED_NAME']?> | </span>
                                <span class="car_title"><?=implode(' ', array_filter(
                                        [$item['NAME'],
                                        $item['DRUGFORMSHORTNAME'],
                                        (float)$item['DFMASS'], $item['DFMASSSHORTNAME'],
                                        (float)$item['DFCONC'], $item['DFCONCSHORTNAME'],
                                        (float)$item['DFACT'], $item['DFACTSHORTNAME'],
                                        (float)$item['DFSIZE'], $item['DFSIZENAME'],
                                        $item['FIRMNAME']
                                        ]))?></span>
                            </li>
                        <? } ?>
                    </ul>
                </div>
            </div>
        </div>

        <?=$pagination?>

    </div>

<?php
$this->view('cp/footer');
?>