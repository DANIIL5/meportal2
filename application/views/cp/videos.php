<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>

    <div class="container">
        <h3 class="sub-header mb_40"><a href="/cp/<?=$back_link?>/<?=$id?>"><?=$back_link_title?></a> | <a href="/cp/branches_add_photos/<?=$id?>">Добавить фото</a> | <a href="/cp/articles_add?OBJECT=2&ID=<?=$id?>">Добавить статью</a></h3>
    </div>

    <div class="container">
        <h2 class="sub-header mb_40">Добавить видео для <?=$object?> <?=(!empty($item)) ? $item['NAME']." (".$item['SHORTNAME'].")" : ""?></h2>
        <?
        parse_str($_SERVER['QUERY_STRING'], $test);
        ?>
        <form action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table"> 
                <tr>
                    <td width="100">Ссылка на видео на ютубе</td>
                    <td><input type="text" placeholder="https://www.youtube.com/watch?v=00000000000" class="form-control" name="url"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-success" name="submit">Добавить видео</button></td>
                </tr>
            </table>
        </form>
    </div>

<?php
$this->view('cp/footer');
?>