<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>

<? if (!empty($_GET['OBJECT']) && !empty($_GET['ID'])) { ?>
    <div class="container">
        <h3 class="sub-header mb_40"><a href="/cp/<?=$back_link?>/<?=$id?>"><?=$back_link_title?></a> | <a href="/cp/branches_add_videos/<?=$id?>">Добавить видео</a> | <a href="/cp/branches_add_photos/<?=$id?>">Добавить фото</a></h3>
    </div>
<? } ?>

    <div class="container">
        <h2 class="sub-header mb_40">Редактировать акцию</h2>

        <form action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table">
                <tr>
                    <td>Название</td>
                    <td><input type="text" placeholder="Название" class="form-control" name="name" value="<?=(!empty($item['NAME'])) ? $item['NAME'] : ""?>"></td>
                </tr>

                <tr>
                    <td>Категория</td>
                    <td>
                        <select name="category_id" id="">
                            <option value="0">Все</option>
                            <? foreach ($categories as $category) {?>
                                <option value="<?=$category['ID']?>" <? if (!empty($item['CATEGORY_ID']) && $item['CATEGORY_ID'] == $category['ID']) { ?>selected<? } ?> ><?=$category['NAME']?></option>
                            <? } ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Изображение</td>
                    <td>
                        <? if (!empty($item['THUMB'])) {?>
                            <img src="<?=$item['THUMB']?>" style="max-height: 200px; max-width: 200px;" alt="<?=(!empty($item['NAME'])) ? $item['NAME'] : ""?>">
                        <? } ?>
                        
                    	<span class="btn btn-default btn-file">
							Выбрать файл
							<input type="file" name="image" accept="image/jpeg,image/png,image/gif">
						</span>
                        <span class="res_file"></span>
                    </td>
                </tr>
                <tr>
                    <td>Дата начала</td>
                    <td><input type="text" class="form-control masked_date" name="start_date" value="<?=(!empty($item['START_DATE'])) ? date('d/m/Y', strtotime($item['START_DATE'])) : ""?>"></td>
                </tr>
                <tr>
                    <td>Дата окончания</td>
                    <td><input type="text" class="form-control masked_date" name="end_date" value="<?=(!empty($item['END_DATE'])) ? date('d/m/Y', strtotime($item['END_DATE'])) : ""?>"></td>
                </tr>
                <tr>
                    <td>Адрес</td>
                    <td><input type="text" placeholder="Адрес" class="form-control" name="address" value="<?=(!empty($item['ADDRESS'])) ? $item['ADDRESS'] : ""?>"></td>
                </tr>
                <tr>
                    <td>Метро</td>
                    <td>
                        <select name="metro_id" id="">
                            <option value="">Метро не выбрано</option>
                            <? foreach ($metro as $station) { ?>
                                <option value="<?=$station['ID']?>" <? if (!empty($item['METRO_ID']) && $item['METRO_ID'] == $station['ID']) { ?> selected <? } ?>><?=$station['NAME']?></option>
                            <? } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Цена</td>
                    <td><input type="text" placeholder="Цена" class="form-control" name="price" value="<?=(!empty($item['PRICE'])) ? $item['PRICE'] : ""?>"></td>
                </tr>
                <tr>
                    <td>Описание</td>
                    <td>
                        <textarea class="form-control" name="description" id="" cols="30" rows="3" placeholder="Описание"><?=(!empty($item['DESCRIPTION'])) ? $item['DESCRIPTION'] : ""?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Текст</td>
                    <td>
                        <textarea id="tinymce" class="form-control" name="text" id="" cols="30" rows="10" placeholder="Текст"><?=(!empty($item['TEXT'])) ? $item['TEXT'] : ""?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Скидка</td>
                    <td><input type="text" placeholder="Скидка" class="form-control" name="discount" value="<?=(!empty($item['DISCOUNT'])) ? $item['DISCOUNT'] : ""?>"></td>
                </tr>
                <tr>
                    <td>Разрешения</td>
                    <td>
                        <label for=""><input type="checkbox" name="public" <? if (!empty($item['PUBLIC'])) {?> checked <? } ?> value="on">Публиковать?</label>
                        <label for=""><input type="checkbox" name="active" <? if (!empty($item['ACTIVE'])) {?> checked <? } ?> value="on">Активно?</label>
                        <label for=""><input type="checkbox" name="moderated" <? if (!empty($item['MODERATED'])) {?> checked <? } ?> value="on">Проверено модератором?</label>
                        <label for=""><input type="checkbox" name="map" <? if (!empty($item['MAP'])) {?> checked <? } ?> value="on">Выводить акцию на карте?</label>
                        <label for=""><input type="checkbox" name="list" <? if (!empty($item['LIST'])) {?> checked <? } ?> value="on">Выводить акцию в общем списке</label>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-success" name="submit">Добавить акцию</button></td>
                </tr>
            </table>
        </form>
    </div>

    <script>
        $('.masked_date').mask('99/99/9999');

        tinymce.init({
            selector: '#tinymce',
            height: 500,
            apply_source_formatting : true,
            forced_root_block: false,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>

<?php
$this->view('cp/footer');
?>