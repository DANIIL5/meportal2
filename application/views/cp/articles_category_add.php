<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>

    <div class="container">
        <h2 class="sub-header mb_40">Добавить категорию</h2>

        <form action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table">
                <tr>
                    <td>Название</td>
                    <td><input type="text" placeholder="Заголовок публикации" class="form-control" name="category" value=""></td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-success" name="submit" value="добавить">Добавить категорию</button></td>
                </tr>
            </table>
        </form>
    </div>

<?php
$this->view('cp/footer');
?>