<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>

<? if (!empty($_GET['OBJECT']) && !empty($_GET['ID'])) { ?>
    <div class="container">
        <h3 class="sub-header mb_40"><a href="/cp/<?=$back_link?>/<?=$id?>"><?=$back_link_title?></a> | <a href="/cp/branches_add_videos/<?=$id?>">Добавить видео</a> | <a href="/cp/branches_add_photos/<?=$id?>">Добавить фото</a></h3>
    </div>
<? } ?>

    <div class="container">
        <h3 class="sub-header mb_40">Привязка <?=implode(' ', array_filter([$item['FIRMNAME'], $item['DRUGNAME']]))?> к объекту РЛС</h3>

        <ul>
        <? if (!empty($errors)) {
            foreach ($errors as $error) { ?>
                <li><?=$error?></li>
            <? }
        }?>
        </ul>

        <form id="form" action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table">
                <tr>
                    <td class="col-md-2">Поиск по названию</td>
                    <td><input type="text" placeholder="Название" class="form-control" name="query" value="" id="autocomplete"></td>
                </tr>
                <input id="id" name="id" type="hidden" value="">
                <tr>
                    <td class="col-md-2" >Привязано к:</td>
                    <td><i id="choosed" style="font-size: 18px;">Объект не выбран</i></td>
                </tr>
                <tr id="save" style="display: none;">
                    <td class="col-md-2"></td>
                    <td><button type="submit" class="btn btn-success" name="submit">Сохранить</button></td>
                </tr>
            </table>
        </form>
    </div>

    <script>
        var options = {

            url: function(query) {
                return "/cp/find_by_name";
            },

            getValue: function(element) {
                return element.VALUE;
            },

            ajaxSettings: {
                dataType: "json",
                method: "POST",
                data: {}
            },

            preparePostData: function(data) {
                data.query = $("#autocomplete").val();
                return data;
            },

            requestDelay: 500,

            list: {
                onSelectItemEvent: function() {
                    var selectedItemValue = $("#autocomplete").getSelectedItemData().VALUE;
                    var selectedItemId = $("#autocomplete").getSelectedItemData().ID;

                    $("#choosed").html(selectedItemValue);
                    $("#id").val(selectedItemId);
                    $("#save").show();
                },
            }
        };

        $("#autocomplete").easyAutocomplete(options);

//        function find()
//        {
//            var form = $('#form').serialize();
//            $.post('/cp/find_by_name', form, function (data) {
//                console.log(data)
//            });
//        }
    </script>

<?php
$this->view('cp/footer');
?>