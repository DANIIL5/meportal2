<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
<div class="container">
    <h2 class="sub-header mb_40">Добавление организации</h2>
</div>
<div class="container">
    <form action="" method="post" enctype="multipart/form-data">
        <table class="table_rightLeft mt_20 mb_40">
            <tbody>
            <tr>
                <td>Логотип</td>
                <td>
                    	<span class="btn btn-default btn-file">
							Выбрать файл
							<input type="file" name="logo" accept="image/jpeg,image/png,image/gif">
						</span>
                    <span class="res_file"></span>
                </td>
            </tr>

            <tr>
                <td>Владелец</td>
                <td>
                    <select name="user_id" class="form-control" id="">
                        <option value="">Без владельца</option>
                        <? foreach ($users as $user) { ?>
                            <option value="<?= $user['ID'] ?>"><?= $user['EMAIL'] ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Категория</td>
                <td>
                    <select name="category_id" class="form-control" id="">
                        <option value="2">Медицинские учреждения</option>
                        <option value="4">Аптеки</option>
                        <option value="5">Красота и здоровье</option>
                        <option value="7">Мама и ребенок</option>
                        <option value="8">Реабилитация</option>
                        <option value="9">Ветеринария</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Тип</td>
                <td>
                    <select name="type_id" id="" class="form-control">
                        <option value="">Без типа</option>
                        <? foreach ($types as $type) { ?>
                            <option value="<?=$type['ID']?>"><?=$type['NAME']?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Внутренние отделения</td>
                <td>
                    <select multiple name="department_id[]" id="" class="form-control">
                        <option value="">Не выбрано</option>
                        <? foreach ($departments as $department) { ?>
                            <option value="<?=$department['ID']?>"><?=$department['NAME']?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Бренд</td>
                <td colspan="2" width="450">
                    <input type="text" name="name" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридическое название</td>
                <td colspan="2">
                    <input type="text" name="shortname" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Описание</td>
                <td colspan="2" width="450">
                    <textarea name="description" id="" cols="30" rows="20" class="form-control"></textarea>
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: индекс</td>
                <td colspan="2">
                    <input type="text" name="legal_address_index" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: область/регион</td>
                <td colspan="2">
                    <input type="text" name="legal_address_region" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: город/населенный пункт</td>
                <td colspan="2">
                    <input type="text" name="legal_address_city" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: улица/переулок</td>
                <td colspan="2">
                    <input type="text" name="legal_address_street" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: дом/здание</td>
                <td colspan="2">
                    <input type="text" name="legal_address_house" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: корпус/строение</td>
                <td colspan="2">
                    <input type="text" name="legal_address_building" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: квартира/офис</td>
                <td colspan="2">
                    <input type="text" name="legal_address_office" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: индекс</td>
                <td colspan="2">
                    <input type="text" name="actual_address_index" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: область/регион</td>
                <td colspan="2">
                    <input type="text" name="actual_address_region" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: город/населенный пункт</td>
                <td colspan="2">
                    <input type="text" name="actual_address_city" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: улица/переулок</td>
                <td colspan="2">
                    <input type="text" name="actual_address_street" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: дом/здание</td>
                <td colspan="2">
                    <input type="text" name="actual_address_house" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: корпус/строение</td>
                <td colspan="2">
                    <input type="text" name="actual_address_building" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: квартира/офис</td>
                <td colspan="2">
                    <input type="text" name="actual_address_office" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Метро</td>
                <td>
                    <select multiple name="metro_id[]" id="" class="form-control">
                        <option value="">Не выбрано</option>
                        <? foreach ($metro as $value) { ?>
                            <option value="<?=$value['ID']?>"><?=$value['NAME']?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Лицензия №</td>
                <td colspan="2">
                    <input type="text" name="license" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Сканы лицензий и файлы</td>
                <td>
                    <div class="b-img-prew">
                    </div>

                    	<span class="btn btn-default btn-file">
							Выбрать файл
							<input type="file" name="docs[]" multiple accept="image/jpeg,image/png,image/gif">
						</span>
                    <span class="res_file"></span>
                </td>
            </tr>
            <tr>
                <td>Телефон 1</td>
                <td>
                    <input type="text" name="phone1" class="check_in phone_inp form-control" data-input="phone" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Телефон 2</td>
                <td>
                    <input type="text" name="phone2" class="phone_inp form-control" data-input="phone" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Телефон 3</td>
                <td>
                    <input type="text" name="phone3" class="phone_inp form-control" data-input="phone" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Веб-сайт 1</td>
                <td>
                    <input type="text" name="website1" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Веб-сайт 2</td>
                <td>
                    <input type="text" name="website2" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Веб-сайт 3</td>
                <td>
                    <input type="text" name="website3" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Почта</td>
                <td>
                    <input type="text" name="email" class="check_in form-control" data-input="mail" data-regex="mail" placeholder="Эл. адрес">
                </td>
            </tr>
            <tr class="g-dnone" style="display: table-row;">
                <td>Широта</td>
                <!-- при сохранении - координаты нужно сохранять в виде числа т.к. в value  - оно хранится в строке -->
                <td>
                    <input type="text" name="latitude" class="form-control decimal_inp js-map js-map-lat">
                </td>
            </tr>
            <tr class="g-dnone" style="display: table-row;">
                <td>Долгота</td>
                <!-- при сохранении - координаты нужно сохранять в виде числа т.к. в value  - оно хранится в строке -->
                <td>
                    <input type="text" name="longitude" class="form-control decimal_inp js-map js-map-lon">
                </td>
            </tr>
            <tr class="b-map js-map js-map-row">
                <td>На карте</td>
                <td colspan="3">
                    <div class="b-map__prew js-map js-map-box" id="b-map"></div>
                </td>
            </tr>
            <tr>
                <td>ОГРН</td>
                <td>
                    <input type="text" data-regex="number" name="ogrn" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ИНН</td>
                <td>
                    <input type="text" data-regex="number" name="inn" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ОКВЭД</td>
                <td>
                    <input type="text" name="okved" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ОКПО</td>
                <td>
                    <input type="text" data-regex="number" name="okpo" class="form-control">
                </td>
            </tr>
            <tr>
                <td>КПП</td>
                <td>
                    <input type="text" data-regex="number" name="kpp" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ОКТМО</td>
                <td>
                    <input type="text" data-regex="number" name="oktmo" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Расчетный счет</td>
                <td>
                    <input type="text" data-regex="number" name="account" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Банк</td>
                <td>
                    <input type="text" name="bank" class="form-control">
                </td>
            </tr>
            <tr>
                <td>БИК</td>
                <td>
                    <input type="text" data-regex="number" name="bik" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Главный врач / заведующий / администратор</td>
                <td>
                    <input type="text" name="chief_physician" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Главный бухгалтер</td>
                <td>
                    <input type="text" name="chief_accountant" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Генеральный директор</td>
                <td>
                    <input type="text" name="ceo" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Рабочее время (ПН)</td>
                <td><input type="text" placeholder="с" name="monday_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="monday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ПН)</td>
                <td><input type="text" placeholder="с" name="monday_break_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="monday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (ВТ)</td>
                <td><input type="text" placeholder="с" name="tuesday_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="tuesday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ВТ)</td>
                <td><input type="text" placeholder="с" name="tuesday_break_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="tuesday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (СР)</td>
                <td><input type="text" placeholder="с" name="wednesday_from" class="form-control js-date-time "></td>
                <td><input type="text" placeholder="до" name="wednesday_to" class="form-control js-date-time "></td>

                <td>Перерыв (СР)</td>
                <td><input type="text" placeholder="с" name="wednesday_break_from" class="form-control js-date-time "></td>
                <td><input type="text" placeholder="до" name="wednesday_break_to" class="form-control js-date-time "></td>
            </tr>
            <tr>
                <td>Рабочее время (ЧТ)</td>
                <td><input type="text" placeholder="с" name="thursday_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="thursday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ЧТ)</td>
                <td><input type="text" placeholder="с" name="thursday_break_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="thursday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (ПТ)</td>
                <td><input type="text" placeholder="с" name="friday_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="friday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ПТ)</td>
                <td><input type="text" placeholder="с" name="friday_break_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="friday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (СБ)</td>
                <td><input type="text" placeholder="с" name="saturday_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="saturday_to" class="form-control js-date-time"></td>

                <td>Перерыв (СБ)</td>
                <td><input type="text" placeholder="с" name="saturday_break_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="saturday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (ВС)</td>
                <td><input type="text" placeholder="с" name="sunday_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="sunday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ВС)</td>
                <td><input type="text" placeholder="с" name="sunday_break_from" class="form-control js-date-time"></td>
                <td><input type="text" placeholder="до" name="sunday_break_to" class="form-control js-date-time"></td>
            </tr>



            <tr>
                <td></td>
                <td>
                    <input type="checkbox" class="checkbox" id="input_1" value="true" name="state"/><label for="input_1">Государственная</label>
                    <input type="checkbox" class="checkbox" id="input_2" value="true" name="private"/><label for="input_2">Частная</label>
                    <input type="checkbox" class="checkbox" id="input_3" value="true" name="children"/><label for="input_3">Детское отделение</label>
                    <input type="checkbox" class="checkbox" id="input_4" value="true" name="ambulance"/><label for="input_4">Скорая</label>
                    <input type="checkbox" class="checkbox" id="input_5" value="true" name="house"/><label for="input_5">Выезд на дом</label>

                    <br>

                    <input type="checkbox" class="checkbox" id="input_1" value="true" name="booking"/><label for="input_1">Бронирование</label>
                    <input type="checkbox" class="checkbox" id="input_2" value="true" name="delivery"/><label for="input_2">С доставкой</label>
                    <input type="checkbox" class="checkbox" id="input_3" value="true" name="daynight"/><label for="input_3">Круглосуточно</label>
                    <input type="checkbox" class="checkbox" id="input_4" value="true" name="dms"/><label for="input_4">ДМС</label>
                    <input type="checkbox" class="checkbox" id="input_5" value="true" name="dlo"/><label for="input_5">ДЛО</label>
                    <input type="checkbox" class="checkbox" id="input_6" value="true" name="optics"/><label for="input_6">Отдел оптики</label>
                    <input type="checkbox" class="checkbox" id="input_7" value="true" name="rpo"/><label for="input_7">Рецептурно-производственный отдел</label>
                    <input type="checkbox" class="checkbox" id="input_6" value="true" name="homeopathy"/><label for="input_6">Отдел гомеопатии</label>

                </td>
            </tr>



            <tr>
                <td>Логотипы</td>
                <td>
                    <input id="logo_carousel" type="checkbox" name="in_logo_carousel" value="true"><label
                        for="logo_carousel">Показывать в строке с логотипами на сайте</label>
                </td>
            </tr>
            <tr>
                <td>Модерация</td>
                <td>
                    <input id="moderated" type="checkbox" name="moderated" value="true" checked><label
                        for="moderated">Проверено и допущено к размещению на портале</label>
                </td>
            </tr>

            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td><button type="submit" class="btn btn-success" name="submit">Добавить</button></td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>


<?php
$this->view('cp/footer');
?>	