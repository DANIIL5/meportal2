<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <div class="container">
        <h2 class="sub-header mb_40">Карусель (шапка сайта)</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 mb_40">
                <div class="b-tab">
                    <div class="table-responsive mt_20 doct table-bord">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Категория</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($catalog as $item) { ?>
                                <tr>
                                    <td><img src="<?=$item['PATH']?>" alt="" style="max-width: 100px; max-height: 100px "></td>
                                    <td><?=$item['TITLE']?></td>
                                    <td>
                                        <a href="/cp/header_carousel_edit/<?=$item['ID']?>" class="icon_event cat_remove"><span class="icon_event_title">Редактировать</span></a>
                                    </td>
                                </tr>
                            <? }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-md-4">
                <div class="mt_20">
                    <form action="" method="get" enctype="multipart/form-data" class="fillter_pub">
                        <h4>Фильтр</h4>

                        <div class="item mt_20">
                            <h4>Фильтр 1</h4>
                            <select class="form-control" name="category_id">
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
$this->view('cp/footer');
?>