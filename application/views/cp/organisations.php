<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
<div class="container">
    <h2 class="sub-header mb_40">Организации</h2>
    <a href="/cp/organisations_add" class="btn btn-success">Добавить новую организацию</a>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 mb_40">
            <div class="b-tab">
                <div class="b-tab__i">
                    <span class="b-tab__it active">Организации </span>
                    <span class="b-tab__it"><a href="/cp/organisation_branches">Отделения</a></span>
                </div>
                <div class="table-responsive mt_20 doct table-bord">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th></th>
                            <th>Бренд</th>
                            <th>Юр.лицо</th>
                            <th>Категория</th>
                            <th>Тип</th>
                            <th>Последнее изменение</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($catalog as $item) { ?>
                            <tr>
                                <td><?=$item['ID']?></td>
                                <td>
                                    <? if (!empty($item['LOGO'])) { ?>
                                        <img src="<?=$item['LOGO']?>" alt="" style="max-width: 60px; max-height: 60px;">
                                    <? } else { ?>
                                        <div style="width: 60px; height: 60px; background: #a1a1a1;">
                                    <? } ?>
                                </td>
                                <td><?=$item['NAME']?></td>
                                <td><?=$item['SHORTNAME']?></td>
                                <td><?=$item['CATEGORYNAME']?></td>
                                <td><?=$item['TYPENAME']?></td>
                                <td><?=$item['UPDATED_AT']?></td>
                                <td>
                                    <span class="icon_event cat_dell" attr-id="<?=$item['ID']?>"><span class="icon_event_title">Удалить</span></span>
                                    <a href="/cp/organisations_edit/<?=$item['ID']?>" class="icon_event cat_remove"><span class="icon_event_title">Редактировать</span></a>
                                </td>
                            </tr>
                        <? }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-md-4">
            <div class="mt_20">
                <form action="" method="get" enctype="multipart/form-data" class="fillter_pub">
                    <h4>Фильтр</h4>

                    <div class="item">
                        <h4>ID</h4>
                        <input type="text" class="form-control" name="id" placeholder="123" value="<?=(!empty($_GET['id'])) ? $_GET['id'] : "" ?>">
                    </div>
                    <div class="item mt_20">
                        <h4>Статус</h4>
                        <select class="form-control" name="status">
                            <option value="">Не выбрано</option>
                            <option value="0" <?=(!empty($_GET['status']) && $_GET['status'] == 1) ? "selected" : ""?>>Непроверенные организации </option>
                            <option value="1" <?=(!empty($_GET['status']) && $_GET['status'] == 2) ? "selected" : ""?>>Проверенные</option>
                            <option value="2" <?=(!empty($_GET['status']) && $_GET['status'] == 3) ? "selected" : ""?>>Заблокированные</option>
                        </select>
                    </div>
                    <div class="item">
                        <h4>Бренд</h4>
                        <input type="text" class="form-control" name="brand" placeholder="СЕРДЕЧКО" value="<?=(!empty($_GET['brand'])) ? $_GET['brand'] : "" ?>">
                    </div>
                    <div class="item">
                        <h4>Юр.лицо</h4>
                        <input type="text" class="form-control" name="legal" placeholder="ООО КОНСАЛТИНГ+" value="<?=(!empty($_GET['legal'])) ? $_GET['legal'] : "" ?>">
                    </div>
                    <div class="item">
                        <h4>ИНН</h4>
                        <input type="text" class="form-control" name="inn" placeholder="000000000000" value="<?=(!empty($_GET['inn'])) ? $_GET['inn'] : "" ?>">
                    </div>
                    <div class="item mt_20">
                        <h4>Категория</h4>
                        <select class="form-control" name="category_id">
                            <option value="">Не выбрано</option>
                            <option value="2" <?=(!empty($_GET['category_id']) && $_GET['category_id'] == 2) ? "selected" : ""?>>Мед.учреждения </option>
                            <option value="4" <?=(!empty($_GET['category_id']) && $_GET['category_id'] == 4) ? "selected" : ""?>>Аптеки</option>
                            <option value="5" <?=(!empty($_GET['category_id']) && $_GET['category_id'] == 5) ? "selected" : ""?>>Красота и здоровье</option>
                            <option value="7" <?=(!empty($_GET['category_id']) && $_GET['category_id'] == 7) ? "selected" : ""?>>Мама и ребенок</option>
                            <option value="8" <?=(!empty($_GET['category_id']) && $_GET['category_id'] == 8) ? "selected" : ""?>>Реабилитация</option>
                            <option value="9" <?=(!empty($_GET['category_id']) && $_GET['category_id'] == 9) ? "selected" : ""?>>Ветеринария</option>
                        </select>
                    </div>
                    <div class="item">
                        <h4>Тип </h4>
                        <select class="form-control" name="type_id">
                            <option value="">Не выбрано</option>
                            <? foreach ($types as $type) { ?>
                                <option value="<?=$type['ID']?>" <?=(!empty($_GET['type_id']) && $_GET['type_id'] == $type['ID']) ? "selected" : ""?>><?=$type['NAME']?></option>
                            <? } ?>
                        </select>
                    </div>
                    <div class="item">
                        <h4>Последнее изменение</h4>
                        <input type="text" class="form-control" name="last_change" placeholder="31.12.2016" value="<?=(!empty($_GET['last_change'])) ? $_GET['last_change'] : "" ?>">
                    </div>
                    <div class="item">
                        <br>
                        <p><a href="/cp/organisations">Сбросить фильтр</a></p>
                    </div>
                    <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                </form>
            </div>
        </div>
    </div>

    <?=$pagination?>
</div>

<!-- Для удаления -->
<div id="dialog-confirm" title="Удаление доктора" style="display: none;">
    <p>
        Вы действительно хотите удалить данную организацию?
    </p>
</div>
<div id="dialog-message" title="Доктор удален" style="display: none;">
    <p>
        Доктор удален
    </p>
</div>

<script src="/admin/js/js_pages/organisations.js"></script>
<!-- end Для удаления -->

<?php
$this->view('cp/footer');
?>	