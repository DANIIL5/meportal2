<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<div class="container">
    <h2 class="sub-header mb_40">Добавление отделения</h2>
</div>
<div class="container mb_40">
    <form action="" method="post" enctype="multipart/form-data">
        <table class="table_rightLeft mt_20 mb_40">
            <tr>
                <td>Юр. лицо </td>
                <td>
                    <select id="linked_organisation" name="institution_id" id="" class="form-control">
                        <? foreach ($institutions as $institution) {?>
                            <option value="<?=$institution['ID']?>" <?=(!empty($item['INSTITUTION_ID']) && $item['INSTITUTION_ID'] == $institution['ID']) ? "selected" : ""?>><?=(!empty($institution['SHORTNAME']) ? $institution['SHORTNAME']." " : "")?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Логотип</td>
                <td>
                    	<span class="btn btn-default btn-file">
							Выбрать файл
							<input type="file" name="logo" accept="image/jpeg,image/png,image/gif">
						</span>
                    <span class="res_file"></span>
                </td>
            </tr>
            <tr>
                <td>Категория</td>
                <td>
                    <select id="category" name="category_id" class="form-control" id="">
                        <option value="2">Медицинские учреждения</option>
                        <option value="4">Аптеки</option>
                        <option value="5">Красота и здоровье</option>
                        <option value="7">Мама и ребенок</option>
                        <option value="8">Реабилитация</option>
                        <option value="9">Ветеринария</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Тип</td>
                <td>
                    <select id="type" name="type_id" id="" class="form-control">
                        <option value="">Без типа</option>
                        <? foreach ($types as $type) { ?>
                            <option value="<?=$type['ID']?>"><?=$type['NAME']?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Внутренние отделения</td>
                <td>
                    <select multiple name="department_id[]" id="" class="form-control">
                        <option value="">Не выбрано</option>
                        <? foreach ($departments as $department) { ?>
                            <option value="<?=$department['ID']?>"><?=$department['NAME']?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Бренд</td>
                <td colspan="2" width="450">
                    <input id="brand" type="text" name="name" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Описание</td>
                <td colspan="2" width="450">
                    <textarea id="description" name="description" id="" cols="30" rows="20" class="form-control"></textarea>
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: индекс</td>
                <td colspan="2">
                    <input id="legal_index" type="text" name="legal_address_index" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: область/регион</td>
                <td colspan="2">
                    <input id="legal_region" type="text" name="legal_address_region" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: город/населенный пункт</td>
                <td colspan="2">
                    <input id="legal_city" type="text" name="legal_address_city" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: улица/переулок</td>
                <td colspan="2">
                    <input id="legal_street" type="text" name="legal_address_street" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: дом/здание</td>
                <td colspan="2">
                    <input id="legal_house" type="text" name="legal_address_house" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: корпус/строение</td>
                <td colspan="2">
                    <input id="legal_building" type="text" name="legal_address_building" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Юридический адрес: квартира/офис</td>
                <td colspan="2">
                    <input id="legal_office" type="text" name="legal_address_office" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: Индекс</td>
                <td>
                    <input id="actual_index" type="text" name="address_index" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: область/регион</td>
                <td>
                    <input id="actual_region" type="text" name="address_region" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: город/населенный пункт</td>
                <td>
                    <input id="actual_city" type="text" name="address_city" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: улица/переулок</td>
                <td>
                    <input id="actual_street" type="text" name="address_street" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: дом/здание</td>
                <td>
                    <input id="actual_house" type="text" name="address_house" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: корпус/строение</td>
                <td>
                    <input id="actual_building" type="text" name="address_building" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Фактический адрес: квартира/офис</td>
                <td>
                    <input id="actual_office" type="text" name="address_office" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Метро</td>
                <td>
                    <select multiple name="metro_id[]" id="" class="form-control">
                        <option value="">Не выбрано</option>
                        <? foreach ($metro as $value) { ?>
                            <option value="<?=$value['ID']?>"><?=$value['NAME']?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Лицензия №</td>
                <td colspan="2">
                    <input id="license" type="text" name="license" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Сканы лицензий и файлы</td>
                <td>
                    <div class="b-img-prew">
                    </div>

                    	<span class="btn btn-default btn-file">
							Выбрать файл
							<input type="file" name="docs[]" multiple accept="image/jpeg,image/png,image/gif">
						</span>
                    <span class="res_file"></span>
                </td>
            </tr>
<!--            <tr>-->
<!--                <td>Телефоны</td>-->
<!--                <td colspan="2" width="450">-->
<!--                    <textarea name="phones" id="" cols="30" rows="5" class="form-control"></textarea>-->
<!--                </td>-->
<!--            </tr>-->
            <tr>
                <td>Телефон 1</td>
                <td>
                    <input id="phone1" type="text" name="phone1" class="check_in phone_inp form-control" data-input="phone" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Телефон 2</td>
                <td>
                    <input id="phone2" type="text" name="phone2" class="phone_inp form-control" data-input="phone" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Телефон 3</td>
                <td>
                    <input id="phone3" type="text" name="phone3" class="phone_inp form-control" data-input="phone" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Веб-сайт 1</td>
                <td>
                    <input id="website1" type="text" name="website1" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Веб-сайт 2</td>
                <td>
                    <input id="website2" type="text" name="website2" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Веб-сайт 3</td>
                <td>
                    <input id="website3" type="text" name="website3" class="form-control" placeholder="">
                </td>
            </tr>
            <tr>
                <td>Почта</td>
                <td>
                    <input id="email" type="text" name="email" class="check_in form-control" data-input="mail" data-regex="mail" placeholder="Эл. адрес">
                </td>
            </tr>
            <tr class="g-dnone" style="display: table-row;">
                <td>Широта</td>
                <!-- при сохранении - координаты нужно сохранять в виде числа т.к. в value  - оно хранится в строке -->
                <td>
                    <input id="latitude" type="text" name="latitude" class="form-control decimal_inp js-map js-map-lat">
                </td>
            </tr>
            <tr class="g-dnone" style="display: table-row;">
                <td>Долгота</td>
                <!-- при сохранении - координаты нужно сохранять в виде числа т.к. в value  - оно хранится в строке -->
                <td>
                    <input id="longitude" type="text" name="longitude" class="form-control decimal_inp js-map js-map-lon">
                </td>
            </tr>
            <tr class="b-map js-map js-map-row">
                <td>На карте</td>
                <td colspan="3">
                    <div class="b-map__prew js-map js-map-box" id="b-map"></div>
                </td>
            </tr>
            <tr>
                <td>ОГРН</td>
                <td>
                    <input id="ogrn" type="text" data-regex="number" name="ogrn" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ИНН</td>
                <td>
                    <input id="inn" type="text" data-regex="number" name="inn" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ОКВЭД</td>
                <td>
                    <input id="okved" type="text" name="okved" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ОКПО</td>
                <td>
                    <input id="okpo" type="text" data-regex="number" name="okpo" class="form-control">
                </td>
            </tr>
            <tr>
                <td>КПП</td>
                <td>
                    <input id="kpp" type="text" data-regex="number" name="kpp" class="form-control">
                </td>
            </tr>
            <tr>
                <td>ОКТМО</td>
                <td>
                    <input id="oktmo" type="text" data-regex="number" name="oktmo" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Расчетный счет</td>
                <td>
                    <input id="account" type="text" data-regex="number" name="account" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Банк</td>
                <td>
                    <input id="bank" type="text" name="bank" class="form-control">
                </td>
            </tr>
            <tr>
                <td>БИК</td>
                <td>
                    <input id="bik" type="text" data-regex="number" name="bik" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Главный врач / заведующий / администратор</td>
                <td>
                    <input id="physician" type="text" name="chief_physician" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Главный бухгалтер</td>
                <td>
                    <input id="accountant" type="text" name="chief_accountant" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Генеральный директор</td>
                <td>
                    <input id="ceo" type="text" name="ceo" class="form-control">
                </td>
            </tr>
<!--            <tr>-->
<!--                <td>График работы</td>-->
<!--                <td colspan="2" width="450">-->
<!--                    <textarea name="schedule" id="" cols="30" rows="5" class="form-control"></textarea>-->
<!--                </td>-->
<!--            </tr>-->
            <tr>
                <td>Рабочее время (ПН)</td>
                <td><input id="monday_from" type="text" placeholder="с" name="monday_from" class="form-control js-date-time"></td>
                <td><input id="monday_to" type="text" placeholder="до" name="monday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ПН)</td>
                <td><input id="monday_break_from" type="text" placeholder="с" name="monday_break_from" class="form-control js-date-time"></td>
                <td><input id="monday_break_to" type="text" placeholder="до" name="monday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (ВТ)</td>
                <td><input id="tuesday_from" type="text" placeholder="с" name="tuesday_from" class="form-control js-date-time"></td>
                <td><input id="tuesday_to" type="text" placeholder="до" name="tuesday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ВТ)</td>
                <td><input id="tuesday_break_from" type="text" placeholder="с" name="tuesday_break_from" class="form-control js-date-time"></td>
                <td><input id="tuesday_break_to" type="text" placeholder="до" name="tuesday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (СР)</td>
                <td><input id="wednesday_from" type="text" placeholder="с" name="wednesday_from" class="form-control js-date-time "></td>
                <td><input id="wednesday_to" type="text" placeholder="до" name="wednesday_to" class="form-control js-date-time "></td>

                <td>Перерыв (СР)</td>
                <td><input id="wednesday_break_from" type="text" placeholder="с" name="wednesday_break_from" class="form-control js-date-time "></td>
                <td><input id="wednesday_break_to" type="text" placeholder="до" name="wednesday_break_to" class="form-control js-date-time "></td>
            </tr>
            <tr>
                <td>Рабочее время (ЧТ)</td>
                <td><input id="thursday_from" type="text" placeholder="с" name="thursday_from" class="form-control js-date-time"></td>
                <td><input id="thursday_to" type="text" placeholder="до" name="thursday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ЧТ)</td>
                <td><input id="thursday_break_from" type="text" placeholder="с" name="thursday_break_from" class="form-control js-date-time"></td>
                <td><input id="thursday_break_to" type="text" placeholder="до" name="thursday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (ПТ)</td>
                <td><input id="friday_from" type="text" placeholder="с" name="friday_from" class="form-control js-date-time"></td>
                <td><input id="friday_to" type="text" placeholder="до" name="friday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ПТ)</td>
                <td><input id="friday_break_from" type="text" placeholder="с" name="friday_break_from" class="form-control js-date-time"></td>
                <td><input id="friday_break_to" type="text" placeholder="до" name="friday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (СБ)</td>
                <td><input id="saturday_from" type="text" placeholder="с" name="saturday_from" class="form-control js-date-time"></td>
                <td><input id="saturday_to" type="text" placeholder="до" name="saturday_to" class="form-control js-date-time"></td>

                <td>Перерыв (СБ)</td>
                <td><input id="saturday_break_from" type="text" placeholder="с" name="saturday_break_from" class="form-control js-date-time"></td>
                <td><input id="saturday_break_to" type="text" placeholder="до" name="saturday_break_to" class="form-control js-date-time"></td>
            </tr>
            <tr>
                <td>Рабочее время (ВС)</td>
                <td><input id="sunday_from" type="text" placeholder="с" name="sunday_from" class="form-control js-date-time"></td>
                <td><input id="sunday_to" type="text" placeholder="до" name="sunday_to" class="form-control js-date-time"></td>

                <td>Перерыв (ВС)</td>
                <td><input id="sunday_break_from" type="text" placeholder="с" name="sunday_break_from" class="form-control js-date-time"></td>
                <td><input id="sunday_break_to" type="text" placeholder="до" name="sunday_break_to" class="form-control js-date-time"></td>
            </tr>

            <tr>
                <td></td>
                <td>
                        <input type="checkbox" class="checkbox" id="input_1" value="true" name="state"/><label for="input_1">Государственная</label>
                        <input type="checkbox" class="checkbox" id="input_2" value="true" name="private"/><label for="input_2">Частная</label>
                        <input type="checkbox" class="checkbox" id="input_3" value="true" name="children"/><label for="input_3">Детское отделение</label>
                        <input type="checkbox" class="checkbox" id="input_4" value="true" name="ambulance"/><label for="input_4">Скорая</label>
                        <input type="checkbox" class="checkbox" id="input_5" value="true" name="house"/><label for="input_5">Выезд на дом</label>

                        <br>

                        <input type="checkbox" class="checkbox" id="input_1" value="true" name="booking"/><label for="input_1">Бронирование</label>
                        <input type="checkbox" class="checkbox" id="input_2" value="true" name="delivery"/><label for="input_2">С доставкой</label>
                        <input type="checkbox" class="checkbox" id="input_3" value="true" name="daynight"/><label for="input_3">Круглосуточно</label>
                        <input type="checkbox" class="checkbox" id="input_4" value="true" name="dms"/><label for="input_4">ДМС</label>
                        <input type="checkbox" class="checkbox" id="input_5" value="true" name="dlo"/><label for="input_5">ДЛО</label>
                        <input type="checkbox" class="checkbox" id="input_6" value="true" name="optics"/><label for="input_6">Отдел оптики</label>
                        <input type="checkbox" class="checkbox" id="input_7" value="true" name="rpo"/><label for="input_7">Рецептурно-производственный отдел</label>
                        <input type="checkbox" class="checkbox" id="input_6" value="true" name="homeopathy"/><label for="input_6">Отдел гомеопатии</label>

                </td>
            </tr>

            <tr>
                <td>Модерация</td>
                <td colspan="3">
                    <input id="moderated" type="checkbox" name="moderated" value="true" checked><label
                        for="moderated">Проверено и допущено к размещению на портале</label>
                </td>
            </tr>

            <tfoot>
            <tr>
                <td></td>
                <td><button type="submit" class="btn btn-success" name="submit">Добавить</button></td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>

<script src="/admin/js/filler.js">
</script>

<?php
$this->view('cp/footer');
?>	