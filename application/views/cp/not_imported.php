<?php
$this->view('cp/header', ['count' => $unreaded]);
?>

    <div class="container">
        <h2 class="sub-header mb_40">Импортированные лекарства</h2>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-16 col-sm-16 col-md-16">
                <div class="b-tab">
                    <ul class="list-group mt_20 categories">
                        <li id="cat_1" class="list-group-item">
                            <span class="icon_event cat_remove"><span class="icon_event_title">Привязать</span></span>
                            <span class="car_title">Исходное название</span>
                        </li>
                        <? foreach ($catalog as $item) { ?>
                            <li id="cat_1" class="list-group-item">
                                <a href="/cp/imported_atach/<?=$item['ID']?>?back_url=<?=urlencode($_SERVER['REQUEST_URI'])?>"><span class="icon_event cat_remove"><span class="icon_event_title">Привязать</span></span></a>
                                <span class="car_title"><?=$item['IMPORTED_NAME']?></span>
                            </li>
                        <? } ?>
                    </ul>
                </div>
            </div>
        </div>

        <?=$pagination?>

    </div>

<?php
$this->view('cp/footer');
?>