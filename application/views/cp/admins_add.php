
<?php
$this->view('cp/header', ['count' => $unreaded,'count2' => $unreaded2]);
?>
<div class="container">
    <h2 class="sub-header mb_40">Добавление администратора</h2>
</div>
<div class="container">
    <form action="" method="post" enctype="multipart/form-data">
        <table class="table_rightLeft mt_20 mb_40">
            <tbody>
            <tr>
                <td>Логин</td>
                <td colspan="2" width="450">
                    <input type="text" name="name" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Пароль</td>
                <td colspan="2">
                    <input type="password" name="password" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Повторите пароль</td>
                <td colspan="2">
                    <input type="password" name="confirmation" class="form-control">
                </td>
            </tr>
             <tr>
                <td>ФИО</td>
                <td colspan="2">
                    <input type="text" name="fio" class="form-control">
                </td>
            </tr>
             <tr>
                <td>Email</td>
                <td colspan="2">
                    <input type="text" name="email" class="form-control">
                </td>
            </tr>
            <tr>
                <td>Доступ</td>
                <td colspan="2">
                    <input id="admins" type="checkbox" name="rights[]" value="admins"><label for="admins" style="font-weight: normal;">Администраторы</label><br>
                    
                    <input id="content" type="checkbox" name="rights[]" value="content"><label for="content" style="font-weight: normal;">Контент</label><br>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td><button type="submit" class="btn btn-success" name="submit">Добавить</button></td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>
<script>
var checkbox1 = $('#admins');
var checkbox2 = $('#content');

checkbox2.click(function(){
   
   
        if (!checkbox2.is(':checked')) {
         
        checkbox1.attr("disabled", false);
        }
        else{
        checkbox1.attr("disabled", true);    
            
        }
        //checkbox2.attr("disabled", false);
    
})

checkbox1.click(function(){
   
   
        if (!checkbox1.is(':checked')) {
         
        checkbox2.attr("disabled", false);
        }
        else{
        checkbox2.attr("disabled", true);    
            
        }
        //checkbox2.attr("disabled", false);
    
})

</script>

<?php
$this->view('cp/footer');
?>