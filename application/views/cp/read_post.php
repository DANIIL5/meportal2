<?php
$this->view('cp/header', ['count' => $unreaded]);
?>

    <div class="container">
        <h2 class="sub-header mb_40">
            Сообщение
        </h2>
 
        <table class="add_pub_table">
            <tr>
                <td>
                    <? if ($type == 'recieved') {?>
                        Отправитель
                    <?} else if ($type == 'sent') {?>
                        Получатель
                    <?}?>
                </td>
                <td>
                    <input type="text" disabled placeholder="Заголовок публикации" class="form-control" value="<?=htmlspecialchars($info['NAME'])?> ">
                </td>
            </tr>
            <tr>
                <td>Тема</td>
                <td>
                    <input type="text" disabled placeholder="Заголовок публикации" class="form-control" value="<?=$post['THEME']?>">
                </td>
            </tr>
            <tr>
                <td>Сообщение</td>
                <td>                                                              
                    <textarea class="col-md-12" disabled rows="10" placeholder="Сообщение"><?=$post['MESSAGE']?></textarea>
                </td>
            </tr> 
            <?if(!empty($info['FILES'])):   ?>
            <? foreach ($info['FILES'] as $item) { ?>
                         
                                <tr>
                                    <td><img src="<?='https://'.$_SERVER['HTTP_HOST'].$item?>" alt="" style="max-width: 100px; max-height: 100px "></td>
                                   
                                </tr>
                            <? }?>
                             <?endif;   ?>
        </table>






        <h2 class="sub-header mb_40">
            <? if ($type == 'recieved') {?>
                Ответить
            <?} else if ($type == 'sent') {?>
                Написать еще одно сообщение
            <?}?>
        </h2>

        <form id="post_form" action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table">
                <tr>
                    <td>Кому</td>
                    <td>
                        <input type="text" placeholder="Заголовок публикации" class="form-control" name="destinations[]" value="<?=$info['EMAIL']?>">
                    </td>
                </tr>
                <tr>
                    <td>Тема</td>
                    <td>
                        <input type="text" placeholder="Заголовок публикации" class="form-control" name="theme" value="<?=$post['THEME']?>">
                    </td>
                </tr>
                <tr>
                    <td>Сообщение</td>
                    <td>
                        <textarea class="col-md-12" name="message" rows="10" placeholder="Сообщение"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="responded_post_id" value="<?=$post['ID']?>">
                        <button id="send_post" type="submit" class="btn btn-success" name="submit">Отправить сообщение</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <script>
        $('#send_post').on('click', function (e) {
            e.preventDefault();

            $.post('/cp/sendPost', $('#post_form').serialize(), function (data) {
                if (data.success !=  undefined && data.success == 'true') {
                    window.location.href = "/cp/posts/out";
                } else {
                    console.log(data);
                    alert(data.error);
                }
            }, 'json');
        });
    </script>

<?php
$this->view('cp/footer');
?>