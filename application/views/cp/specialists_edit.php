
<?php
$this->view('cp/header', ['count' => $unreaded]);
?>

<script>
    var data = [
        { label: "anders", category: "" },
        { label: "andreas", category: "" },
        { label: "antal", category: "" },
        { label: "annhhx10", category: "Products" },
        { label: "annk K12", category: "Products" },
        { label: "annttop C13", category: "Products" },
        { label: "anders andersson", category: "People" },
        { label: "andreas andersson", category: "People" },
        { label: "andreas johnson", category: "People" }
    ];
</script>
<div class="container">
    <h2 class="sub-header mb_40">Изменить специалиста</h2>
</div>
<div class="container">
    <h3 class="sub-header mb_40"><a href="/cp/specialists_add_photos/<?=$id?>">Добавить фото</a> | <a href="/cp/specialists_add_videos/<?=$id?>">Добавить видео</a> | <a href="/cp/articles_add?OBJECT=3&ID=<?=$id?>">Добавить статью</a></h3>
</div>

<form action="" method="post" enctype="multipart/form-data">
    <table class="table_rightLeft doct_tab">
        <tr>
            <td>Логотип</td>
            <td>
                <? if (!empty($item['PHOTO'])) {?>
                    <img src="<?=$item['PHOTO']?>" alt="" style="max-width: 160px; max-height: 160px;"><br>
                <? } ?>
                <span class="btn btn-default btn-file">
                    Выбрать файл
                    <input type="file" name="photo" accept="image/jpeg,image/png,image/gif">
                </span>
                <span class="res_file"></span>
            </td>
        </tr>

        <!--            <tr>-->
        <!--                <td>Пользователь</td>-->
        <!--                <td colspan="2">-->
        <!--                    <input type="text" name="user" class="check_in form-control" data-input="mail" data-regex="mail" placeholder="Эл. адрес">-->
        <!--                </td>-->
        <!--            </tr>-->

        <tr>
            <td>Фамилия</td>
            <td colspan="2"><input type="text" name="second_name" class="form-control" value="<?=!empty($item['SECOND_NAME']) ? $item['SECOND_NAME'] : ""?>"></td>
        </tr>
        <tr>
            <td>Имя</td>
            <td colspan="2"><input type="text" name="first_name" class="form-control" value="<?=!empty($item['FIRST_NAME']) ? $item['FIRST_NAME'] : ""?>"></td>
        </tr>
        <tr>
            <td>Отчество</td>
            <td colspan="2"><input type="text" name="last_name" class="form-control" value="<?=!empty($item['LAST_NAME']) ? $item['LAST_NAME'] : ""?>"></td>
        </tr>
        <tr>
            <td>Описание</td>
            <td colspan="2"><textarea name="description" id="" cols="30" rows="10" class="form-control"><?=!empty($item['DESCRIPTION']) ? $item['DESCRIPTION'] : ""?></textarea></td>
        </tr>
        <tr>
            <td>Место работы</td>
            <!--                <td class="autoCompWR">-->
            <!--                    <input type="text" name="branch" class="form-control autoComp" placeholder="Search">-->
            <!--                    <input type="hidden" name="branchID" >-->
            <!--                </td>-->
            <!--                <td>-->
            <!--                    Не приклеплено-->
            <!--                </td>-->
            <td colspan="2">
                <select class="form-control" name="branch_id" id="">
                    <option value="">Не выбрано</option>
                    <? foreach ($organisations as $organisation) { ?>
                        <option value="<?=$organisation['ID']?>" <?=(!empty($item['BRANCH_ID']) && $item['BRANCH_ID'] == $organisation['ID']) ? "selected" : ""?>><?=$organisation['NAME']."(".$organisation['ADDRESS'].")"?></option>
                    <? } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Позиция</td>
            <td colspan="2"><input type="text" name="position" class="form-control" value="<?=!empty($item['POSITION']) ? $item['POSITION'] : ""?>"></td>
        </tr>
        <tr>
            <td>Специальность</td>
            <td colspan="2">
                <select class="form-control" name="specialization" id="">
                    <option value="">Не выбрано</option>
                    <? foreach ($specializations as $specialization) { ?>
                        <option value="<?=$specialization['ID']?>" <?=(!empty($item['SPECIALIZATION_ID']) && $item['SPECIALIZATION_ID'] == $organisation['ID']) ? "selected" : ""?>><?=$specialization['NAME']?></option>
                    <? } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Стаж по специальности</td>
            <td colspan="2"><input type="text" name="specialization_experience" class="form-control" value="<?=!empty($item['SPECIALIZATION_EXPIIENCE']) ? $item['SPECIALIZATION_EXPIIENCE'] : ""?>"></td>
        </tr>
        <tr>
            <td>Общий стаж</td>
            <td colspan="2"><input type="text" data-regex="number" name="total_experience" class="form-control" value="<?=!empty($item['TOTAL_EXPERIENCE']) ? $item['TOTAL_EXPERIENCE'] : ""?>"></td>
        </tr>
        <tr>
            <td>Рабочий телефон</td>
            <td colspan="2">
                <input type="text" name="work_phone" class="check_in phone_inp form-control" data-input="phone" placeholder="Введите телефон" value="<?=!empty($item['WORK_PHONE']) ? $item['WORK_PHONE'] : ""?>">
            </td>
        </tr>
        <tr>
            <td>Мобильный телефон</td>
            <td colspan="2">
                <input type="text" name="mobile_phone" class="check_in phone_inp form-control" data-input="phone" placeholder="Введите мобильный телефон" value="<?=!empty($item['MOBILE_PHONE']) ? $item['MOBILE_PHONE'] : ""?>">
            </td>
        </tr>
        <tr>
            <td>Рабочее время (ПН)</td>
            <td><input type="text" placeholder="с" name="monday_from" class="form-control" value="<?=!empty($item['MONDAY_WORK_FROM']) ? $item['MONDAY_WORK_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="до" name="monday_to" class="form-control" value="<?=!empty($item['MONDAY_WORK_TO']) ? $item['MONDAY_WORK_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td>Рабочее время (ВТ)</td>
            <td><input type="text" placeholder="с" name="tuesday_from" class="form-control" value="<?=!empty($item['TUESDAY_WORK_FROM']) ? $item['TUESDAY_WORK_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="до" name="tuesday_to" class="form-control" value="<?=!empty($item['TUESDAY_WORK_TO']) ? $item['TUESDAY_WORK_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td>Рабочее время (СР)</td>
            <td><input type="text" placeholder="с" name="wednesday_from" class="form-control " value="<?=!empty($item['WEDNESDAY_WORK_FROM']) ? $item['WEDNESDAY_WORK_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="до" name="wednesday_to" class="form-control " value="<?=!empty($item['WEDNESDAY_WORK_TO']) ? $item['WEDNESDAY_WORK_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td>Рабочее время (ЧТ)</td>
            <td><input type="text" placeholder="с" name="thursday_from" class="form-control" value="<?=!empty($item['THURSDAY_WORK_FROM']) ? $item['THURSDAY_WORK_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="до" name="thursday_to" class="form-control" value="<?=!empty($item['THURSDAY_WORK_TO']) ? $item['THURSDAY_WORK_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td>Рабочее время (ПТ)</td>
            <td><input type="text" placeholder="с" name="friday_from" class="form-control" value="<?=!empty($item['FRIDAY_WORK_FROM']) ? $item['FRIDAY_WORK_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="до" name="friday_to" class="form-control" value="<?=!empty($item['FRIDAY_WORK_TO']) ? $item['FRIDAY_WORK_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td>Рабочее время (СБ)</td>
            <td><input type="text" placeholder="с" name="saturday_from" class="form-control" value="<?=!empty($item['SATURDAY_WORK_FROM']) ? $item['SATURDAY_WORK_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="до" name="saturday_to" class="form-control" value="<?=!empty($item['SATURDAY_WORK_TO']) ? $item['SATURDAY_WORK_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td>Рабочее время (ВС)</td>
            <td><input type="text" placeholder="с" name="sunday_from" class="form-control" value="<?=!empty($item['SUNDAY_WORK_FROM']) ? $item['SUNDAY_WORK_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="до" name="sunday_to" class="form-control" value="<?=!empty($item['SUNDAY_WORK_TO']) ? $item['SUNDAY_WORK_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td>Университет</td>
            <td colspan="2"><input type="text" name="university" class="form-control" value="<?=!empty($item['UNIVERSITY']) ? $item['UNIVERSITY'] : ""?>"></td>
        </tr>
        <tr>
            <td>Факультет</td>
            <td><input type="text" name="faculty" class="form-control" value="<?=!empty($item['FACULTY']) ? $item['FACULTY'] : ""?>"></td>
        </tr>
        <tr>
            <td>Серия и номер диплома</td>
            <td colspan="2"><input type="text" name="diploma_series_and_number" class="form-control" value="<?=!empty($item['DIPLOMA_SERIES_AND_NUMBER']) ? $item['DIPLOMA_SERIES_AND_NUMBER'] : ""?>"></td>
        </tr>
        <tr>
            <td>Дата окончания</td>
            <td><input type="text" name="graduation_date" placeholder="01.01.1970" class="form-control datepicker" value="<?=!empty($item['GRADUATION_DATE']) ? $item['GRADUATION_DATE'] : ""?>"></td>
        </tr>
        <tr>
            <td colspan="3" class="text-center"><i>Интернатура</i></td>
        </tr>
        <tr>
            <td>Заведение</td>
            <td colspan="2"><input type="text" name="intership_institution" class="form-control" value="<?=!empty($item['INTERSHIP_INSTITUTION']) ? $item['INTERSHIP_INSTITUTION'] : ""?>"></td>
        </tr>
        <tr>
            <td>Специальность</td>
            <td colspan="2"><input type="text" name="intership_speciality" class="form-control" value="<?=!empty($item['INTERSHIP_SPECIALITY']) ? $item['INTERSHIP_SPECIALITY'] : ""?>"></td>
        </tr>
        <tr>
            <td>Период</td>
            <td><input type="text" placeholder="01.01.1970" name="intership_date_from" class="form-control datepicker_from" value="<?=!empty($item['INTERSHIP_DATE_FROM']) ? $item['INTERSHIP_DATE_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="01.01.1970" name="intership_date_to" class="form-control datepicker_to" value="<?=!empty($item['INTERSHIP_DATE_TO']) ? $item['INTERSHIP_DATE_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td class="text-center"><i>Ординатура</i></td>
        </tr>
        <tr>
            <td>Заведение</td>
            <td colspan="2"><input type="text" name="residency_institution" class="form-control" value="<?=!empty($item['RESIDENCY_INSTITUTION']) ? $item['RESIDENCY_INSTITUTION'] : ""?>"></td>
        </tr>
        <tr>
            <td>Специальность</td>
            <td colspan="2"><input type="text" name="residency_speciality" class="form-control" value="<?=!empty($item['RESIDENCY_SPECIALITY']) ? $item['RESIDENCY_SPECIALITY'] : ""?>"></td>
        </tr>
        <tr>
            <td>Период</td>
            <td><input type="text" placeholder="01.01.1970" name="residency_date_from" class="form-control datepicker_from" value="<?=!empty($item['RESIDENCY_DATE_FROM']) ? $item['RESIDENCY_DATE_FROM'] : ""?>"></td>
            <td><input type="text" placeholder="01.01.1970" name="residency_date_to" class="form-control datepicker_to" value="<?=!empty($item['RESIDENCY_DATE_TO']) ? $item['RESIDENCY_DATE_TO'] : ""?>"></td>
        </tr>
        <tr>
            <td colspan="3" class="text-center">Профессиональная переподготовка</td>
        </tr>
        <tr>
            <td>Заведение</td>
            <td colspan="2"><input type="text" name="professional_retraining_institution" class="form-control" value="<?=!empty($item['PROFESSIONAL_RETRAINING_INSTITUTION']) ? $item['PROFESSIONAL_RETRAINING_INSTITUTION'] : ""?>"></td>
        </tr>
        <tr>
            <td>Специальность</td>
            <td colspan="2"><input type="text" name="professional_retraining_speciality" class="form-control" value="<?=!empty($item['PROFESSIONAL_RETRAINING_SPECIALITY']) ? $item['PROFESSIONAL_RETRAINING_SPECIALITY'] : ""?>"></td>
        </tr>
        <tr>
            <td>Лет обучения</td>
            <td colspan="2"><input type="text" data-regex="number" name="professional_retraining_years" class="form-control" value="<?=!empty($item['PROFESSIONAL_RETRAINING_YEARS']) ? $item['PROFESSIONAL_RETRAINING_YEARS'] : ""?>"></td>
        </tr>

        <tr>
            <td colspan="2" class="text-center"><i>Повышения квалификации</i></td>
            <td><button type="button" class="btn btn-info add_kval">Добавить "Повышения квалификации"</button></td>
        </tr>
        <tr>
            <td>Учреждение</td>
            <td colspan="2"><input type="text" name="refresher_training_institution" class="form-control"></td>
        </tr>
        <tr>
            <td>Срок</td>
            <td colspan="2"><input type="text" name="refresher_training_period" class="form-control"></td>
        </tr>
        <tr>
            <td>Специальность</td>
            <td colspan="2"><input type="text" name="refresher_training_speciality" class="form-control"></td>
        </tr>
        <tr>
            <td>Количество часов</td>
            <td colspan="2"><input type="text" data-regex="number" name="refresher_training_hours" class="form-control"></td>
        </tr>
        <tr>
            <td>Номер документа</td>
            <td colspan="2"> <input type="text" name="refresher_training_document_number" class="form-control"></td>
        </tr>
        <tr>
            <td>Дата окончания</td>
            <td colspan="2"><input type="text" name="refresher_training_graduation_date" class="form-control datepicker"></td>
        </tr>

        <tr>
            <td>Разрешения</td>
            <td>
                <label for=""><input type="checkbox" name="public" <? if (!empty($item['PUBLIC'])) {?> checked <? } ?> value="on">Публиковать?</label>
                <label for=""><input type="checkbox" name="active" <? if (!empty($item['ACTIVE'])) {?> checked <? } ?> value="on">Активно?</label>
                <label for=""><input type="checkbox" name="moderated" <? if (!empty($item['MODERATED'])) {?> checked <? } ?> value="on">Проверено модератором?</label>
            </td>
        </tr>

        <tr>
            <td colspan="3"></td>
        </tr>

        <tfoot>
        <tr>
            <td></td>
            <td><button type="submit" name="submit" class="btn btn-success">Изменить специалиста</button></td>
        </tr>
        </tfoot>
    </table>
</form>

<script>
    jQuery(document).ready(function($) {

        var kvalif = '<tr><td colspan="3" class="text-center"><i>Повышения квалификации</i></td></tr>';
        kvalif = kvalif +	'<tr><td>Учреждение</td><td colspan="2"><input type="text" name="refresher_training_institution" class="form-control"></td></tr>';
        kvalif = kvalif +	'<tr><td>Срок</td><td colspan="2"><input type="text" name="refresher_training_period" class="form-control"></td></tr>';
        kvalif = kvalif +	'<tr><td>Специальность</td><td colspan="2"><input type="text" name="refresher_training_speciality" class="form-control"></td></tr>';
        kvalif = kvalif +	'<tr><td>Количество часов</td><td colspan="2"><input type="text" data-regex="number" name="refresher_training_hours" class="form-control"></td></tr>';
        kvalif = kvalif +	'<tr><td>Номер документа</td><td colspan="2"> <input type="text" name="refresher_training_document_number" class="form-control"></td></tr>'
        kvalif = kvalif +	'<tr><td>Дата окончания</td><td colspan="2"><input type="text" name="refresher_training_graduation_date" class="form-control datepicker"></td></tr>';

        $('.add_kval').on('click', function() {
            $('.doct_tab tfoot').before(kvalif);
        })
        $('.datepicker').datepicker( 'enable' )
    });
</script>

<script>
    $('input[name="monday_from"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="tuesday_from"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="wednesday_from"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="thursday_from"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="friday_from"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="saturday_from"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="sunday_from"]').mask('99:99',  {placeholder: "09:00"});

    $('input[name="monday_to"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="tuesday_to"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="wednesday_to"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="thursday_to"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="friday_to"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="saturday_to"]').mask('99:99',  {placeholder: "09:00"});
    $('input[name="sunday_to"]').mask('99:99',  {placeholder: "09:00"});
</script>

<?php
$this->view('cp/footer');
?>