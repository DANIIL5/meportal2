<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
<script src="/admin/js/tinymce/tinymce.min.js"></script>

    <? if (!empty($_GET['OBJECT']) && !empty($_GET['ID'])) { ?>
    <div class="container">
        <h3 class="sub-header mb_40"><a href="/cp/<?=$back_link?>/<?=$id?>"><?=$back_link_title?></a> | <a href="/cp/branches_add_videos/<?=$id?>">Добавить видео</a> | <a href="/cp/branches_add_photos/<?=$id?>">Добавить фото</a></h3>
    </div>
    <? } ?>

<div class="container">
    <h2 class="sub-header mb_40">Добавить публикацию <?=(!empty($item) ? "для ".$item['NAME']." (".$item['SHORTNAME'].")" : "")?></h2>

    <form action="" method="post" enctype="multipart/form-data">
        <table class="add_pub_table">
            <tr>
                <td>Заголовок</td>
                <td><input type="text" placeholder="Заголовок публикации" class="form-control" name="name"></td>
            </tr>
            <tr>
                <td>Категория</td>
                <td>
                    <table class="col-12">
                        <tr>
                            <td>
                                <select class="form-control" name="category_id">
                                    <? foreach ($articles as $article) {?>
                                        <option value="<?=$article['ID']?>"><?=$article['NAME']?></option>
                                    <? } ?>
                                </select>
                            </td>
                            <td>
                                <a href="/cp/articles_category_add" target="_blank" onclick="window.open(this.href,this.target,'width=600,height=400,scrollbars=1');return false;">Добавить новую категорию</a>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>

            <tr>
                <td>Категория на сайте</td>
                <td>
                    <table class="col-12">
                        <tr>
                            <td>
                                <select class="form-control" name="site_category_id">
                                    <option value="">Все категории</option>
                                    <option value="1">Главная</option>
                                    <option value="2">Мед учреждения</option>
                                    <option value="3">Врачи</option>
                                    <option value="12">Первая помощь</option>
                                    <option value="4">Аптеки</option>
                                    <option value="5">Здоровье и красота</option>
                                    <option value="7">Мама и ребенок</option>
                                    <option value="9">Ветеренария</option>
                                    <option value="8">Реабилитация</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td></td>
                <td>
                    <textarea id="tinymce" name="text">

                    </textarea>
                </td>
            </tr>
<!--            <tr>-->
<!--                <td>Первая помощь?</td>-->
<!--                <td><input type="checkbox" name="first_help" value=""></td>-->
<!--            </tr>-->
            <tr>
                <td></td>
                <td><button type="submit" class="btn btn-success" name="submit">Добавить публикацию</button></td>
            </tr>
        </table>
    </form>
</div>

<script>
    tinymce.init({
        selector: '#tinymce',
        height: 500,
        apply_source_formatting : true,
        forced_root_block: false,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>

<?php
$this->view('cp/footer');
?>