<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/admin/css/bootstrap.css" />
    <link rel="stylesheet" href="/admin/css/plugins.css" />
    <link rel="stylesheet" href="/admin/css/style.css" />
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/admin/easy-autocomplete.min.css">
    <link rel="stylesheet" href="/admin/easy-autocomplete.themes.min.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/lib/jquery.maskedinput.js"></script>
    <script src="/admin/jquery.easy-autocomplete.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

</head>
<body>

<div class=" navbar navbar-inverse navbar-fixed-top" id="header" role="navigation">

    <div class="container">

        <div class="clearfix" >
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">МФП</a>
                </div>
               <?if(isset($_SESSION['role'])){
          
           $role = explode(",",  $_SESSION['role']);
          
           
       
        foreach ($role as $key=>$value){
            if($value=='content'){
              $accses=true;
              
            }
            //print_r($_SESSION);
            //unset($_SESSION['role']);
        }
        }
        ?>
        <?if(isset($accses) && $accses==true): ?>
         <ul class="nav navbar-nav">
                    <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Навигация <span class="caret"></span></a>
                        
                           
                           <ul class="dropdown-menu">
                     
                            <li class="menu-item dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Контент <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="menu-item "><a href="/cp/articles">Статьи и публикации</a></li>
                                    <li class="menu-item "><a href="/cp/header_carousel">Баннеры</a></li>
                                    <li class="menu-item "><a href="/cp/rss_news">RSS-новости </a></li>
                                    <li class="menu-item "><a href="/cp/actions">Акции </a></li>
                                    <li class="menu-item "><a href="/cp/pages/calculator">Калькуляторы</a></li>
                                    <li class="menu-item "><a href="/cp/pages/rating">Рейтинги</a></li>
                                    <li class="menu-item "><a href="/cp/vote">Голосование</a></li>
<!--                                    <li class="menu-item "><a href="/cp/organisations">Организации</a></li>-->
<!--                                    <li class="menu-item "><a href="/cp/specialists">Специалисты</a></li>-->

                                </ul>
                            </li>
                            

                        </ul>
                    </li>
                 </ul>
<ul class="nav navbar-nav  navbar-right left_top_nav">
                    <li class="menu-item dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Контент менеджер<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                    
                            <li><a href="/cp/admins_edit/<?=$_SESSION['idadmin']?>">Настройка</a></li>
                           
                            <li><a href="/cp/exits">Выйти</a></li>
                        </ul>
                    </li>
                </ul>
        <?else:?>
        <ul class="nav navbar-nav">
                    <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Навигация <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Рабочий стол</a></li>
                            <li class="menu-item dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Справочники  <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="menu-item "><a href="/cp/admins">Администраторы</a></li>
                                    <li class="menu-item dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Справочники лекарств  <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">РЛС</a></li>
                                            <li><a href="#">МФП</a></li>
                                            <li><a href="/cp/imported">Импортированные лекарства</a></li>
                                            <li><a href="/cp/not_imported">Не импортированные лекарства</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item "><a href="#">Справочники лекарств </a></li>
                                    <li class="menu-item "><a href="/cp/new_organizations">Организации</a></li>
                                    <li class="menu-item "><a href="/cp/specialists">Специалисты</a></li>

                                </ul>
                            </li>
                            <li class="menu-item dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Контент <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="menu-item "><a href="/cp/articles">Статьи и публикации</a></li>
                                    <li class="menu-item "><a href="/cp/header_carousel">Баннеры</a></li>
                                    <li class="menu-item "><a href="/cp/rss_news">RSS-новости </a></li>
                                    <li class="menu-item "><a href="/cp/actions">Акции </a></li>
                                    <li class="menu-item "><a href="/cp/pages/calculator">Калькуляторы</a></li>
                                    <li class="menu-item "><a href="/cp/pages/rating">Рейтинги</a></li>
                                    <li class="menu-item "><a href="/cp/vote">Голосование</a></li>
<!--                                    <li class="menu-item "><a href="/cp/organisations">Организации</a></li>-->
<!--                                    <li class="menu-item "><a href="/cp/specialists">Специалисты</a></li>-->

                                </ul>
                            </li>
                            <li><a href="#">Пользователи</a></li>
                            <li><a href="#">Аналитика</a></li>
                            <li><a href="#">Управления продажами</a></li>
                            <li><a href="#">Финансы</a></li>

                        </ul>
                    </li>
                </ul>

                <!-- <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul> -->
                <ul class="nav navbar-nav  navbar-right left_top_nav">
                    <li class="menu-item dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Администратор<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li  class="list-group-item ">
                                <a href="/cp/posts">
                                    <span class="badge"><?=$count?></span>
                                    Сообщения
                                </a>
                                 <a href="/cp/opinion">
                                    <span class="badge"><?=$count2?></span>
                                    Отзывы
                                </a>
                            </li>
                            <li><a href="#">Настройка</a></li>
                            <li><a href="#">Помощь</a></li>
                            <li><a href="/cp/exits">Выйти</a></li>
                        </ul>
                    </li>
                </ul>

                <form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Search...">
                </form>
        
        <?endif;?>
                



            </div>
        </div>
    </div>
</div>