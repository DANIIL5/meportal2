<?php
$this->view('cp/header', ['count' => $unreaded]);
?>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>

    <div class="container">
        <h3 class="sub-header mb_40"><a href="/cp/<?=$back_link?>/<?=$id?>"><?=$back_link_title?></a> | <a href="/cp/branches_add_videos/<?=$id?>">Добавить видео</a> | <a href="/cp/articles_add?OBJECT=2&ID=<?=$id?>">Добавить статью</a></h3>
    </div>

    <div class="container">
        <h2 class="sub-header mb_40">Добавить изображение для <?=$object?> <?=(!empty($item)) ? $item['NAME']." (".$item['SHORTNAME'].")" : ""?></h2>

        <form action="" method="post" enctype="multipart/form-data">
            <table class="add_pub_table">
                <tr>
                    <td width="100">Изображения</td>
                    <td>
                    	<span class="btn btn-default btn-file">
							Выбрать файл
							<input type="file" name="images[]" multiple accept="image/jpeg,image/png,image/gif">
						</span>
                        <span class="res_file"></span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button type="submit" class="btn btn-success" name="submit">Добавить фотографии</button></td>
                </tr>

                <tr>
                    <td>Загруженные фото</td>
                    <td>

                        <? if (!empty($images)) {
                            foreach ($images as $image) { ?>
                                <div class="b-img-prew">
                                    <img src="<?=$image['VALUE']?>" alt="" style="max-width: 160px; max-height: 160px;"><br>
                                </div>
                            <? }
                        } else { ?>
                            Фотографии отсутствуют
                        <? } ?>
                    </td>
                </tr>
            </table>
        </form>
    </div>

<?php
$this->view('cp/footer');
?>