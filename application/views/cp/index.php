<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Административная панель - вход</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }
        .w100 {
            width: 100%;
        }
        #header {
            height: 80px;
        }

        #header ul {
            list-style-type: none;
        }

        #header ul > li {
            padding-right: 20px;
            display: inline-block;
        }
    </style>
</head>
<body>
    <div id="header" class="w100">
        <ul>
            <li><a href="/cp/articles">Публикации</a></li>
            <li><a href="/cp/specialists">Специалисты</a></li>
            <li><a href="/cp/organisations">Организации</a></li>
        </ul>
    </div>
</body>
</html>