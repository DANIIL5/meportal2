<?php
$this->view('cp/header', ['count' => $unreaded,'count2' => $unreaded2]);
?>

    <div class="container">
        <h2 class="sub-header mb_40">Акции</h2>

        <a href="/cp/actions_add" class="btn btn-success">Добавить новую акцию</a>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="b-tab">
                    <ul class="list-group mt_20 categories">
                        <? foreach ($catalog as $item) { ?>
                            <li id="cat_1" class="list-group-item">
                                <span class="icon_event cat_dell" attr-id="<?=$item['ID']?>"><span class="icon_event_title">Удалить</span></span>
                                <a href="/cp/actions_edit/<?=$item['ID']?>"><span class="icon_event cat_remove" attr-id="1"><span class="icon_event_title">Изменить</span></span></a>
                                <span class="car_title"><?=$item['NAME']?></span>
                            </li>
                        <? } ?>
                    </ul>
                </div>
            </div>

            <div class="col-xs-4 col-md-4">
                <div class="mt_20">
                    <form action="" class="fillter_pub">
                        <h4>Компания</h4>
                        <select class="form-control input-lg" name="organization_id">
                            <option value="">Не выбрано</option>
                            <? foreach ($organisations as $organisation) { ?>
                                <option value="<?=$organisation['ID']?>" <? if (!empty($_GET['organization_id']) && $_GET['organization_id'] == $organisation['ID']) { ?> selected <? } ?>><?=$organisation['BRAND']?>(<?=$organisation['ENTITY']?>)</option>
                            <? } ?>
                        </select>
                        <button type="submit" class="btn btn-primary mt_20">Фильтровать</button>
                    </form>
                </div>
            </div>
        </div>

        <?=$pagination?>

    </div>

    <!-- Для удаления -->
    <div id="dialog-confirm" title="Удаление категории" style="display: none;">
        <p>
            Вы действительно хотите удалить акцию?
        </p>
    </div>
    <div id="dialog-message" title="категория удалена" style="display: none;">
        <p>
            Акция удалена
        </p>
    </div>
    <!-- end Для удаления -->

    <!-- для извменения -->
    <div id="dialog-confirm_2" title="Удаление категории" style="display: none;">
        <p>
            Вы действительно хотите изменить акцию?
        </p>
    </div>
    <div id="dialog-message_2" title="Изменения категории" style="display: none;">
        <p>
            Акция успешно изменена
        </p>
    </div>
    <!-- end для извменения -->


    <script src="/admin/js/js_pages/actions.js"></script>

<?php
$this->view('cp/footer');
?>