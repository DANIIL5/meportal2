<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="top">
    <div class="container menu">
        <ul class="list">
            <li><a href="/institutions">Мед. учреждения</a></li>
            <li><a href="/doctors">Врачи</a></li>
            <li><a href="/drugstores">Аптеки</a></li>
<!--            <li><a href="/hls">Зож</a></li>-->
            <li><a href="/beauty">Здоровье и красота</a></li>
<!--            <li><a href="">Справочники</a></li>-->
            <li><a href="/mother_and_child">Мама и ребенок</a></li>
            <li><a href="/rehabilitation">Реабилитация</a></li>
            <li><a href="/veterinary">Ветеренария</a></li>
        </ul>
    </div>
</div>
