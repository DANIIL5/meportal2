<div class="container" style="padding-bottom: 20px;">

    <div class="two-cols two-cols_newMed search-result search-result-list hospital">
        <div class="left-col">
            <div class="b-clinic_new">
                <div class="b-clinic_new__foto-or-logo">
                    <img src="<?=$item['LOGO']?>" alt="">
                    <ul class="b-rating">
                        <li class="b-rating__row b-rating__row_img">
                            <span class="b-rating__label">Рейтинг</span>
                            <ul class="b-rating__star j-rating-star">
                                <li class="g-dnone __active">
                                    <input class="g-dnone b-rating__input" name="field-1" val="0" type="text">
                                </li>
                                <li class="b-rating__star-i __active">1</li>
                                <li class="b-rating__star-i">2</li>
                                <li class="b-rating__star-i">3</li>
                                <li class="b-rating__star-i">4</li>
                                <li class="b-rating__star-i">5</li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <!--  блок с инфо  -->
                <div class="b-clinic_new__info">
                    <div class="b-info-clinic b-info-clinic_right-panel">
                        <div class="b-info-clinic__main">
                            <h2 class="h2"><?=$item['INSTITUTION_NAME']?></h2>
                            <span class="b-jur"><?=$item['INSTITUTION_LEGAL_NAME']?></span>
                            <table class="b-info-table">
                                <tbody>
                                <? if (!empty($item['ADDRESS_REGION']) || !empty($item['ADDRESS_CITY']) || !empty($item['ADDRESS_STREET']) || !empty($item['ADDRESS_HOUSE']) || !empty($item['ADDRESS_BUILDING']) || !empty($item['ADDRESS_OFFICE'])) { ?>
                                    <tr>
                                        <td> Адрес: </td>
                                        <td><?=(!empty($item['ADDRESS_REGION']) ? $item['ADDRESS_REGION'].", " : "" ).(!empty($item['ADDRESS_CITY']) ? $item['ADDRESS_CITY'].", " : "" ).(!empty($item['ADDRESS_STREET']) ? $item['ADDRESS_STREET'].", " : "" ).(!empty($item['ADDRESS_HOUSE']) ? $item['ADDRESS_HOUSE'].", " : "" ).(!empty($item['ADDRESS_BUILDING']) ? $item['ADDRESS_BUILDING'].", " : "" ).(!empty($item['ADDRESS_OFFICE']) ? $item['ADDRESS_OFFICE'] : "" )?>  <a class="g-link go-map" href="">Показать на карте</a></td>
                                    </tr>
                                <? } ?>
                                <? if (!empty($item_metro)) { ?>
                                    <tr>
                                        <td> Метро: </td>
                                        <td><span class="metro">Спортивная</span></td>
                                    </tr>
                                <? } ?>
                                <? if (!empty($item['PHONE'])) {?>
                                    <tr>
                                        <td> Телефон: </td>
                                        <td><?=(!empty($item['PHONE1']) ? $item['PHONE1']."<br>" : "").(!empty($item['PHONE2']) ? $item['PHONE2']."<br>" : "").(!empty($item['PHONE3']) ? $item['PHONE3']."<br>" : "")?></td>
                                    </tr>
                                <? } ?>
                                <? if (!empty($item['WEBSITES'])) { ?>
                                    <tr>
                                        <td> Веб-сайт: </td>
                                        <td><a class="g-link" href="<?=$item['WEBSITES']?>"><?=$item['WEBSITES']?></a></td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="b-info-clinics__RP">
                            <input style="width:195px;" value="Оставить отзыв" title="Оставить отзыв" class="submit b-btn b-btn_uc g-width100" type="submit">
                            <div class="b-btn-ico">
                                <span class="b-btn-ico__ico"><img src="/images/rating-star_def.png" alt=""></span>
                                <input value="Добавить в избранное" title="Добавить в избранное" class="submit b-btn b-btn_uc g-width100 b-btn__counter-gray-ico b-btn-ico__input" type="submit">
                            </div>
                        </div>
                    </div>
                    <div class="work-time">
                        <h3>График, <small><?=$month?></small></h3>
                    </div>
                    <ul class="b-work-time">
                        <li class="b-work-time__i b-work-time-cell">
                            <div class="b-work-time-cell__i">
                                <?=$week['mon']?>, <span class="g-label">Пн</span><br>
                                <? if (!empty($item['MONDAY_WORK_FROM']) && !empty($item['MONDAY_WORK_TO'])) {?><?=$item['MONDAY_WORK_FROM']?> - <?=$item['MONDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                            </div>
                            <? if (!empty($item['MONDAY_BREAK_FROM']) && !empty($item['MONDAY_BREAK_TO'])) {?>
                                <div class="b-work-time-cell__i">
                                    <span class="g-label">Перерыв</span><br>
                                    <?=$item['MONDAY_BREAK_FROM']?> - <?=$item['MONDAY_BREAK_TO']?>
                                </div>
                            <? } ?>
                        </li>
                        <li class="b-work-time__i b-work-time-cell">
                            <div class="b-work-time-cell__i">
                                <?=$week['tue']?>, <span class="g-label">Вт</span><br>
                                <? if (!empty($item['TUESDAY_WORK_FROM']) && !empty($item['TUESDAY_WORK_TO'])) {?><?=$item['TUESDAY_WORK_FROM']?> - <?=$item['TUESDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                            </div>
                            <? if (!empty($item['TUESDAY_BREAK_FROM']) && !empty($item['TUESDAY_BREAK_TO'])) {?>
                                <div class="b-work-time-cell__i">
                                    <span class="g-label">Перерыв</span><br>
                                    <?=$item['TUESDAY_BREAK_FROM']?> - <?=$item['TUESDAY_BREAK_TO']?>
                                </div>
                            <? } ?>
                        </li>
                        <li class="b-work-time__i b-work-time-cell">
                            <div class="b-work-time-cell__i">
                                <?=$week['wed']?>, <span class="g-label">Ср</span><br>
                                <? if (!empty($item['WEDNESDAY_WORK_FROM']) && !empty($item['WEDNESDAY_WORK_TO'])) {?><?=$item['WEDNESDAY_WORK_FROM']?> - <?=$item['WEDNESDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                            </div>
                            <? if (!empty($item['WEDNESDAY_BREAK_FROM']) && !empty($item['WEDNESDAY_BREAK_TO'])) {?>
                                <div class="b-work-time-cell__i">
                                    <span class="g-label">Перерыв</span><br>
                                    <?=$item['WEDNESDAY_BREAK_FROM']?> - <?=$item['WEDNESDAY_BREAK_TO']?>
                                </div>
                            <? } ?>
                        </li>
                        <li class="b-work-time__i b-work-time-cell">
                            <div class="b-work-time-cell__i">
                                <?=$week['thu']?>, <span class="g-label">Чт</span><br>
                                <? if (!empty($item['THURSDAY_WORK_FROM']) && !empty($item['THURSDAY_WORK_TO'])) {?><?=$item['THURSDAY_WORK_FROM']?> - <?=$item['THURSDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                            </div>
                            <? if (!empty($item['THURSDAY_BREAK_FROM']) && !empty($item['THURSDAY_BREAK_TO'])) {?>
                                <div class="b-work-time-cell__i">
                                    <span class="g-label">Перерыв</span><br>
                                    <?=$item['THURSDAY_BREAK_FROM']?> - <?=$item['THURSDAY_BREAK_TO']?>
                                </div>
                            <? } ?>
                        </li>
                        <li class="b-work-time__i b-work-time-cell">
                            <div class="b-work-time-cell__i">
                                <?=$week['fri']?>, <span class="g-label">Пт</span><br>
                                <? if (!empty($item['FRIDAY_WORK_FROM']) && !empty($item['FRIDAY_WORK_TO'])) {?><?=$item['FRIDAY_WORK_FROM']?> - <?=$item['FRIDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                            </div>
                            <? if (!empty($item['FRIDAY_BREAK_FROM']) && !empty($item['FRIDAY_BREAK_TO'])) {?>
                                <div class="b-work-time-cell__i">
                                    <span class="g-label">Перерыв</span><br>
                                    <?=$item['FRIDAY_BREAK_FROM']?> - <?=$item['FRIDAY_BREAK_TO']?>
                                </div>
                            <? } ?>
                        </li>
                        <li class="b-work-time__i b-work-time-cell">
                            <div class="b-work-time-cell__i">
                                <?=$week['sat']?>, <span class="g-label">Сб</span><br>
                                <? if (!empty($item['SATURDAY_WORK_FROM']) && !empty($item['SATURDAY_WORK_TO'])) {?><?=$item['SATURDAY_WORK_FROM']?> - <?=$item['SATURDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                            </div>
                            <? if (!empty($item['SATURDAY_BREAK_FROM']) && !empty($item['SATURDAY_BREAK_TO'])) {?>
                                <div class="b-work-time-cell__i">
                                    <span class="g-label">Перерыв</span><br>
                                    <?=$item['SATURDAY_BREAK_FROM']?> - <?=$item['SATURDAY_BREAK_TO']?>
                                </div>
                            <? } ?>
                        </li>
                        <li class="b-work-time__i b-work-time-cell">
                            <div class="b-work-time-cell__i">
                                <?=$week['sun']?>, <span class="g-label">Вс</span><br>
                                <? if (!empty($item['SUNDAY_WORK_FROM']) && !empty($item['SUNDAY_WORK_TO'])) {?><?=$item['SUNDAY_WORK_FROM']?> - <?=$item['SUNDAY_WORK_TO']?> <? } else { ?>Выходной<? } ?>
                            </div>
                            <? if (!empty($item['SUNDAY_BREAK_FROM']) && !empty($item['SUNDAY_BREAK_TO'])) {?>
                                <div class="b-work-time-cell__i">
                                    <span class="g-label">Перерыв</span><br>
                                    <?=$item['SUNDAY_BREAK_FROM']?> - <?=$item['SUNDAY_BREAK_TO']?>
                                </div>
                            <? } ?>
                        </li>
                    </ul>
                </div>
                <!-- // блок с инфо  -->

                <!-- блок с поиском -->
                <div class="search-in-card search_form">
                    <div class="wrapper">
                        <div id="organisation_search" class="forma">
                            <form action="/medicines/catalog" method="get" enctype="multipart/form-data">
                                <var class="title georgia transform">поиск</var>
                                <span class="for_input"><input id="organisation_search_query" attr-id="<?=$item['ID']?>" type="text" class="text" placeholder="например, аптека на Невском" name="query"></span>
                                <input type="hidden" name="id" value="1">
                                <input type="submit" class="submit btn" value="Найти" title="Найти">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- // блок с поиском-->
            </div>
        </div>

        <div class="right-col">
            <ul class="list-instruction">
                <li><a href="#" class="___active"описание</a></li>
                <li><a href="#">контакты</a></li>
                <li><a href="#">специалисты</a></li>
                <li><a href="/medicines/catalog?branch_id=<?=$item['ID']?>">Прайс-лист</a></li>
                <li><a href="#">публикации</a></li>
                <li><a href="#">акции</a></li>
                <li><a href="#">фото</a></li>
                <li><a href="#">видео</a></li>
                <li><a href="#">отзывы</a></li>
            </ul>
        </div>

    </div>

</div>