<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="theme">
    <h2>Популярные темы</h2>
    <div class="columns">
        <? foreach ($category as $item) { ?>
            <div class="column">
                <h4 class="title"><a href="/articles/category/<?=$item['ID']?>"><?=$item['NAME']?></a> (20)</h4>
                <? if (!empty($themes[$item['ID']])) {
                   foreach ($themes[$item['ID']] as $values) { ?>
                        <ul class="list">
                            <li><a href="/articles/item/<?=$values['ID']?>"><?=$values['NAME']?></a></li>
                        </ul>
                    <? }
                   } ?>

            </div>
        <? } ?>
<!--        <div class="column">-->
<!--            <h4 class="title"><a href="#">Предупреждение болезней</a> (20)</h4>-->
<!--            <ul class="list">-->
<!--                <li><a href="#">Декретный отпуск</a></li>-->
<!--                <li><a href="#">Образ жизни</a></li>-->
<!--                <li><a href="#">Ведение беременности</a></li>-->
<!--                <li><a href="#">Предвестники родов</a></li>-->
<!--                <li><a href="#">Декретный отпуск</a></li>-->
<!--                <li><a href="#">Образ жизни</a></li>-->
<!--                <li><a href="#">Ведение беременности</a></li>-->
<!--                <li><a href="#">Предвестники родов</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--        <div class="column">-->
<!--            <h4 class="title"><a href="#">Познание текста</a> (12)</h4>-->
<!--            <ul class="list">-->
<!--                <li><a href="#">Декретный отпуск</a></li>-->
<!--                <li><a href="#">Образ жизни</a></li>-->
<!--                <li><a href="#">Ведение беременности</a></li>-->
<!--                <li><a href="#">Предвестники родов</a></li>-->
<!--                <li><a href="#">Декретный отпуск</a></li>-->
<!--                <li><a href="#">Образ жизни</a></li>-->
<!--                <li><a href="#">Ведение беременности</a></li>-->
<!--                <li><a href="#">Предвестники родов</a></li>-->
<!--                <li><a href="#">Ведение беременности</a></li>-->
<!--                <li><a href="#">Предвестники родов</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--        <div class="column">-->
<!--            <h4 class="title"><a href="#">теоретически возможен</a> (20)</h4>-->
<!--            <ul class="list">-->
<!--                <li><a href="#">Декретный отпуск</a></li>-->
<!--                <li><a href="#">Образ жизни</a></li>-->
<!--                <li><a href="#">Ведение беременности</a></li>-->
<!--                <li><a href="#">Метафора уязвима. Ритм неизменяем</a></li>-->
<!--                <li><a href="#">Декретный отпуск</a></li>-->
<!--                <li><a href="#">Образ жизни</a></li>-->
<!--                <li><a href="#">Ведение беременности</a></li>-->
<!--                <li><a href="#">Предвестники родов</a></li>-->
<!--            </ul>-->
<!--        </div>-->
        <p style="text-align: center;"><a href="/articles/catalog">Все</a></p>
    </div>
</div>
