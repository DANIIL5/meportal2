<div class="popular popular-agency">
    <div class="container">
        <h2>Популярные медучреждения</h2>

        <? foreach ($institutions as $institution) { ?>
            <div class="item">
                <span class="ava"><img src="/images/organisations/<?=$institution['LOGO']?>" alt=""></span>
                <p><a href="#"><?=$institution['BRAND']?></a></p>
                <?=(!empty($institution['get_addresses']['actual'])) ? implode(',', array_filter($institution['get_addresses']['actual'])) : ""?>
            </div>
        <? } ?>
    </div>
</div>