<div class="publication">
    <div class="container_row">
        <h3>Публикации</h3>

        <div class="row">
            <div class="publication-item">
                <div>
                    <h3>Быличка иллюстрирует стих Быличка иллюстрирует стих </h3>
                    <p>Однако, исследователи постоянно сталкиваются с тем, что эстетическое воздействие интегрирует подтекст.</p>
                    <span class="date">12 июля</span>
                </div>
            </div>

            <div class="publication-item">
                <div>
                    <h3>Диалектический характер</h3>
                    <p>Композиционный анализ неоднороден по составу. Диалектический характер пространственно интегрирует ритмический рисунок.</p>
                    <span class="date">12 июля</span>
                </div>
            </div>

            <div class="publication-item">
                <div>
                    <h3>интегрирует ритмический рисунок</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur distinctio facilis
                        labore libero porro quam rerum, voluptatem! Alias dolores doloribus fugit laudantium quidem
                        sequi velit voluptatibus? Cumque earum ex vel.</p>
                    <span class="date">12 июля</span>
                </div>
            </div>

            <div class="publication-item">
                <div>
                    <h3>моделирует собственную реальность</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur distinctio facilis
                        labore libero porro quam rerum, voluptatem! Alias dolores doloribus fugit laudantium quidem
                        sequi velit voluptatibus? Cumque earum ex vel.</p>
                    <span class="date">12 июля</span>
                </div>
            </div>

        </div>

    </div>
</div>