<div class="b-clinic_new__foto-or-logo">
    <img src="<?=(!empty($item["LOGO"]) ? $item["LOGO"] : "/images/placeholders/popular_medical.jpg")?>" alt="">
    <ul class="b-rating">
        <li class="b-rating__row b-rating__row_img">
            <span class="b-rating__label">Рейтинг</span>
            <ul class="b-rating__star j-rating-star">
                <li class="g-dnone __active">
                    <input class="g-dnone b-rating__input" name="field-1" val="0" type="text">
                </li>
                <li class="b-rating__star-i __active">1</li>
                <li class="b-rating__star-i">2</li>
                <li class="b-rating__star-i">3</li>
                <li class="b-rating__star-i">4</li>
                <li class="b-rating__star-i">5</li>
            </ul>
        </li>
    </ul>
</div>

<!--  блок с инфо  -->
<div class="b-clinic_new__info">
    <div class="b-info-clinic b-info-clinic_right-panel">
        <div class="b-info-clinic__main" style="float: left;">
            <h2 class="h2"><?=$item['BRAND']?></h2>
            <span class="b-jur"><?=$item['ENTITY']?></span>
            <table class="b-info-table">
                <tbody>
                <? if (!empty($item['get_addresses']['actual'])) { ?>
                    <tr>
                        <td> Адрес: </td>
                        <td><?=implode(', ',$item['get_addresses']['actual'])?> <!-- <a class="g-link go-map" href="">Показать на карте</a> --></td>
                    </tr>
                <? } ?>
                <? if (!empty($item['get_metro'])) { ?>
                    <tr>
                        <td> Метро: </td>
                        <td><span class="metro"><?=implode(', ', array_column($item['get_metro'], 'NAME'))?></span></td>
                    </tr>
                <? } ?>
                <? if (!empty($item['get_contacts']['phones'])) { ?>
                    <tr>
                        <td> Телефон: </td>
                        <td><?=implode(', ', array_filter($item['get_contacts']['phones']))?></td>
                    </tr>
                <? } ?>
                <? if (!empty($item['get_contacts']['websites'])) { ?>
                    <tr>
                        <td> Веб-сайт: </td>
                        <td>
                            <? foreach ($item['get_contacts']['websites'] as $website) { ?>
                                <a class="g-link" href="<?=$website?>"><?=$website?></a><br>
                            <? } ?>
                        </td>
                    </tr>
                <? } ?>
                </tbody>
            </table>
        </div>
        <div class="b-info-clinics__RP">
            <a href="/profile/post<?=(!empty($item['WORK_EMAIL']) ? "?email=".$item['WORK_EMAIL'] : "/admin")?>" class="submit b-btn b-btn_uc g-width100"  style="width:180px; margin-bottom: 20px;" title="Написать сообщение" type="submit">Написать</a>
            <input class="submit b-btn b-btn_uc g-width100 js-pUp__openeer" style="width:180px;" data-pup-id="pUp-comment" value="Оставить отзыв" title="Оставить отзыв" class="submit b-btn b-btn_uc g-width100" type="submit">
            <div class="b-btn-ico">
                <span class="b-btn-ico__ico"><img src="/images/rating-star_def.png" alt=""></span>
                <input id="add_to_favorites" attr-id="<?=$item['ID']?>" attr-type="organisations" value="Добавить в избранное" title="Добавить в избранное" class="submit b-btn b-btn_uc g-width100 b-btn__counter-gray-ico b-btn-ico__input" type="submit">
            </div>
        </div>
    </div>
    <div class="work-time">
        <h3>График, <small><?=$month?></small></h3>
    </div>
    <ul class="b-work-time">
        <li class="b-work-time__i b-work-time-cell">
            <div class="b-work-time-cell__i">
                <?=$week['mon']?>, <span class="g-label">Пн</span><br>
                <? if (!empty($item['get_working_time'][1]['WORK_FROM']) && !empty($item['get_working_time'][1]['WORK_TO'])) {?>
                    <?=$item['get_working_time'][1]['WORK_FROM']?> - <?=$item['get_working_time'][1]['WORK_TO']?>
                <? } else { ?>
                    Выходной
                <? } ?>
            </div>
            <? if (!empty($item['get_working_time'][1]['BREAK_FROM']) && !empty($item['get_working_time'][1]['BREAK_TO'])) {?>
                <div class="b-work-time-cell__i">
                    <span class="g-label">Перерыв</span><br>
                    <?=$item['get_working_time'][1]['BREAK_FROM']?> - <?=$item['get_working_time'][1]['BREAK_TO']?>
                </div>
            <? } ?>
        </li>
        <li class="b-work-time__i b-work-time-cell">
            <div class="b-work-time-cell__i">
                <?=$week['tue']?>, <span class="g-label">Вт</span><br>
                <? if (!empty($item['get_working_time'][2]['WORK_FROM']) && !empty($item['get_working_time'][2]['WORK_TO'])) {?>
                    <?=$item['get_working_time'][2]['WORK_FROM']?> - <?=$item['get_working_time'][2]['WORK_TO']?>
                <? } else { ?>
                    Выходной
                <? } ?>
            </div>
            <? if (!empty($item['get_working_time'][2]['BREAK_FROM']) && !empty($item['get_working_time'][2]['BREAK_TO'])) {?>
                <div class="b-work-time-cell__i">
                    <span class="g-label">Перерыв</span><br>
                    <?=$item['get_working_time'][2]['BREAK_FROM']?> - <?=$item['get_working_time'][2]['BREAK_TO']?>
                </div>
            <? } ?>
        </li>
        <li class="b-work-time__i b-work-time-cell">
            <div class="b-work-time-cell__i">
                <?=$week['wed']?>, <span class="g-label">Ср</span><br>
                <? if (!empty($item['get_working_time'][3]['WORK_FROM']) && !empty($item['get_working_time'][3]['WORK_TO'])) {?>
                    <?=$item['get_working_time'][3]['WORK_FROM']?> - <?=$item['get_working_time'][3]['WORK_TO']?>
                <? } else { ?>
                    Выходной
                <? } ?>
            </div>
            <? if (!empty($item['get_working_time'][3]['BREAK_FROM']) && !empty($item['get_working_time'][3]['BREAK_TO'])) {?>
                <div class="b-work-time-cell__i">
                    <span class="g-label">Перерыв</span><br>
                    <?=$item['get_working_time'][3]['BREAK_FROM']?> - <?=$item['get_working_time'][3]['BREAK_TO']?>
                </div>
            <? } ?>
        </li>
        <li class="b-work-time__i b-work-time-cell">
            <div class="b-work-time-cell__i">
                <?=$week['thu']?>, <span class="g-label">Чт</span><br>
                <? if (!empty($item['get_working_time'][4]['WORK_FROM']) && !empty($item['get_working_time'][4]['WORK_TO'])) {?>
                    <?=$item['get_working_time'][4]['WORK_FROM']?> - <?=$item['get_working_time'][4]['WORK_TO']?>
                <? } else { ?>
                    Выходной
                <? } ?>
            </div>
            <? if (!empty($item['get_working_time'][4]['BREAK_FROM']) && !empty($item['get_working_time'][4]['BREAK_TO'])) {?>
                <div class="b-work-time-cell__i">
                    <span class="g-label">Перерыв</span><br>
                    <?=$item['get_working_time'][4]['BREAK_FROM']?> - <?=$item['get_working_time'][4]['BREAK_TO']?>
                </div>
            <? } ?>
        </li>
        <li class="b-work-time__i b-work-time-cell">
            <div class="b-work-time-cell__i">
                <?=$week['fri']?>, <span class="g-label">Пт</span><br>
                <? if (!empty($item['get_working_time'][5]['WORK_FROM']) && !empty($item['get_working_time'][5]['WORK_TO'])) {?>
                    <?=$item['get_working_time'][5]['WORK_FROM']?> - <?=$item['get_working_time'][5]['WORK_TO']?>
                <? } else { ?>
                    Выходной
                <? } ?>
            </div>
            <? if (!empty($item['get_working_time'][5]['BREAK_FROM']) && !empty($item['get_working_time'][5]['BREAK_TO'])) {?>
                <div class="b-work-time-cell__i">
                    <span class="g-label">Перерыв</span><br>
                    <?=$item['get_working_time'][5]['BREAK_FROM']?> - <?=$item['get_working_time'][5]['BREAK_TO']?>
                </div>
            <? } ?>
        </li>
        <li class="b-work-time__i b-work-time-cell">
            <div class="b-work-time-cell__i">
                <?=$week['sat']?>, <span class="g-label">Сб</span><br>
                <? if (!empty($item['get_working_time'][6]['WORK_FROM']) && !empty($item['get_working_time'][6]['WORK_TO'])) {?>
                    <?=$item['get_working_time'][6]['WORK_FROM']?> - <?=$item['get_working_time'][6]['WORK_TO']?>
                <? } else { ?>
                    Выходной
                <? } ?>
            </div>
            <? if (!empty($item['get_working_time'][6]['BREAK_FROM']) && !empty($item['get_working_time'][6]['BREAK_TO'])) {?>
                <div class="b-work-time-cell__i">
                    <span class="g-label">Перерыв</span><br>
                    <?=$item['get_working_time'][6]['BREAK_FROM']?> - <?=$item['get_working_time'][6]['BREAK_TO']?>
                </div>
            <? } ?>
        </li>
        <li class="b-work-time__i b-work-time-cell">
            <div class="b-work-time-cell__i">
                <?=$week['sun']?>, <span class="g-label">Вс</span><br>
                <? if (!empty($item['get_working_time'][7]['WORK_FROM']) && !empty($item['get_working_time'][7]['WORK_TO'])) {?>
                    <?=$item['get_working_time'][7]['WORK_FROM']?> - <?=$item['get_working_time'][7]['WORK_TO']?>
                <? } else { ?>
                    Выходной
                <? } ?>
            </div>
            <? if (!empty($item['get_working_time'][7]['BREAK_FROM']) && !empty($item['get_working_time'][7]['BREAK_TO'])) {?>
                <div class="b-work-time-cell__i">
                    <span class="g-label">Перерыв</span><br>
                    <?=$item['get_working_time'][7]['BREAK_FROM']?> - <?=$item['get_working_time'][7]['BREAK_TO']?>
                </div>
            <? } ?>
        </li>
    </ul>
</div>
<!-- // блок с инфо  -->