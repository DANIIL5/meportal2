<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="city" name="city">
    <select>
        <? foreach ($cities as $city) { ?>
            <option value="<?=$city['ID']?>" <?=($city['ID'] == $current_city) ? "selected" : ""?>><?=$city['NAME']?>
<!--                (--><?//=$city['REGION']?><!--)-->
            </option>
        <? } ?>
    </select>
</div>
