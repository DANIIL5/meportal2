<!DOCTYPE HTML>
<html lang="ru">
    <head>
        <title>МедФарПоратал - Личный кабинет</title>
        <!--  META BLOCK -->
        <meta charset="UTF-8">
        <meta name="description" content="MFP" >
        <meta name="keywords" content="MFP" >
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <!--  END META BLOCK -->
        <!--  import CSS -->
        	<link href="/css/lib/jquery-ui.css" media="screen" rel="stylesheet" type="text/css" >
        	<link href="/css/basic.css" media="screen" rel="stylesheet" type="text/css" >
        	<link href="/css/main.css" media="screen" rel="stylesheet" type="text/css" >
        <!--  END import CSS -->
    </head>
    <body class="body">
            <div class="body-content">
                <!--  MAIN MENU -->
                <div class="main-menu">
                    	<a class="main-menu__back-s" href="#">МЕДПОРТАЛ</a>
                    	<ul class="main-menu-list">
                    		<li class="main-menu-list__i main-menu-list__i_0 main-menu-list__i_active">
                    			<a href="#">ЛИЧНЫЕ ДАННЫЕ</a>
                    		</li>
                    		<li class="main-menu-list__i main-menu-list__i_2">
                    			<a href="#">СООБЩЕНИЯ</a>
                    		</li>
                            <li class="main-menu-list__i main-menu-list__i_8">
                                <a href="#">ИЗБРАННОЕ</a>
                            </li>
                    		<li class="main-menu-list__i main-menu-list__i_9">
                    			<a href="#">МЕДКАРТА</a>
                    		</li>
                    		<li class="main-menu-list__i main-menu-list__i_4">
                    			<a href="#">ЦЕЛИ</a>
                    		</li>
                    		<li class="main-menu-list__i main-menu-list__i_10">
                    			<a href="#">ГРАФИК</a>
                    		</li>
                    		<li class="main-menu-list__i main-menu-list__i_exit">
                    			<a href="#">ВЫХОД</a>
                    		</li>
                    	</ul>
                </div>