<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="right_block">
    <ul class="tabs">
        <li><a href="/calculators/catalog"><i class="icon culc"></i>Калькуляторы</a></li>
        <li><a href="/ratings/catalog"><i class="icon rating"></i>Рейтинги</a></li>
        <li class="selected"><a href="#"><i class="icon vote"></i>Голосование</a></li>
    </ul>
    <div class="container_block">
        <div class="vote">
            <form id="vote" onsubmit="send_vote(event, this)" action="#">
          
           <? if(!empty($info)): ?>
                <input type="hidden" name="vote_id" value="<?=$info['ID']?>">
                <p class="question"><?=$info['QUESTION']?></p>
                <ul>
                    <? if ($is_vote['is_vote']) { ?>
                        <?=$is_vote['html']?>
                    <? } else {?>
                        <? foreach ($options as $option) { ?>
                            <li><input type="radio" name="option_id" class="radio" value="<?=$option['ID']?>" id="result<?=$option['ID']?>"><label for="result<?=$option['ID']?>"><?=$option['NAME']?></label></li>
                        <? } ?>
                        <li><input type="submit" class="submit btn" value="Голосовать" title="Голосовать"/></li>
                    <? } ?>
                </ul>
                <? endif; ?>
            </form>
<!--            <p class="results"><a href="#">Результаты</a> (20)</p>-->
<!--            <div class="results-block">-->
<!--                <p class="question">Художественная гармония, не учитывая количества слогов, стоящих между ударениями, приводит скрытый смысл</p>-->
<!--                <ul>-->
<!--                    <li>90% - Да</li>-->
<!---->
<!--                    <li>5% - Нет</li>-->
<!---->
<!--                    <li>1% - Периодически</li>-->
<!--                </ul>-->
<!--            </div>-->
        </div>
    </div>
</div>
