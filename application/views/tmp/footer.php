<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="bottom">
    <div class="container">
        <div class="left_col">
            <div class="logo_footer"><a href="#"><img src="/images/logo-footer.png" alt="Медико Фармацевтический портал"/></a></div>
            <ul class="social">
                <li class="fb"><a href="#">fb</a></li>
                <li class="tw"><a href="#">twitter</a></li>
                <li class="ball"><a href="#">fb</a></li>
                <li class="video"><a href="#">video</a></li>
                <li class="google"><a href="#">google</a></li>
            </ul>
        </div>


        <div class="center_col">
            <a class="tel" href="tel:88003003232"><span>8 800</span> 555-31-91</a>
            <p class="copy">Распространение материалов только с согласия редакции. Все права защищены. &copy; 2015 <br/>Эл. почта для связи — <a href="mailto:hello@helpmed.com">hello@helpmed.com</a>.</p>
        </div>

        <div class="right_col">
            <ul class="submenu">
                <li><a href="/info/contacts">Контактная информация</a></li>
                <li><a href="/info/about">О портале</a></li>
                <li><a href="/info/feedback">Обратная связь</a></li>
            </ul>
            <? if (!$user) {?>
            <div class="subscribe">
                <span class="georgia title">Подписка</span>
                <div class="forma">
                    <form action="#">
                        <input type="text" class="text" placeholder="hello@helpmed.com"/>
                        <span class="button"><input type="submit" class="submit" value="Подписаться"></span>
                    </form>
                </div>
            </div>
            <? } ?>
        </div>
    </div>
</div>
