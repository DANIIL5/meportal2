            <div class="footer">
            	<hr>
            	<ul class="footer-zone">
            		<li class="footer-zone__i footer-zone__i_1">
            			<span class="footer-tel">8 800 300-32-32</span>
            		</li>
            		<li class="footer-zone__i footer-zone__i_2">
            			<span class="footer-copr"> Все права защищены. © 2015 Эл. почта для связи — </span>
            			<a class="footer-mailTo" href="mailto:hello@medportal.ru">hello@medportal.ru</a>
            		</li> 
            		</li>
            	</ul>
            </div>
            <!--  END FOOTER -->
        </div>
        <!--  import JS - lib -->
        <script type="text/javascript" src="/js/lib/jquery-2.0.3.js"></script>
        <script type="text/javascript" src="/js/lib/jquery-ui.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.formstyler.min.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="/js/profile-script.js"></script>
        <!--  END import JS - lib -->

        <!--  import JS - usr -->
        <script type="text/javascript" src="/js/usr/main.js"></script>
        <!--  END import JS - usr -->
    </body>
</html>