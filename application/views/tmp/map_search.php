<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="map_search" class="map_search <?=(!empty($green) && $green == true) ? "map-green" : "" ?>">
    <div class="container_row">
        <div class="b-search">
            <h3>Все что нужно для здоровья и красоты рядом c вами</h3>
            <div class="search_form">
                <form action="#">
                    <span class="for_input"><input type="text" class="text" placeholder="например, аптека на Невском"></span>
                    <input type="submit" class="submit btn" value="Найти" title="Найти">
                </form>
            </div>
        </div>
        <div class="left_block">
            <!-- 	<h3>Все что нужно для здоровья и красоты рядом c вами</h3> -->

            <div class="show_list">
                <p class="transform title"><b>Показать на карте</b></p>
                <ul class="map_search_categories">
                    <li class="active"><a class="map_types" attr-id="0" href="#" onclick="on_click_make_active(this)"><i class="icon icon-plus"></i>Все</a></li>
                    <? foreach ($types as $type) { ?>
                        <li><a class="map_types" attr-id="<?=$type['ID']?>" href="#" onclick="on_click_make_active(this)"><i class="icon icon-<?=$type['ICON']?>"></i><?=$type['NAME']?></a></li>
                    <? } ?>
                </ul>
            </div>

            <!-- <div class="search_form">
                <form action="#">
                    <span class="for_input"><input type="text" class="text" placeholder="например, аптека на Невском"></span>
                    <input type="submit" class="submit btn" value="Найти" title="Найти">
                </form>
            </div> -->


        </div>
        <div class="right_block">
            <div class="actions">
                <h2>Акции Рядом</h2>
                <? if (empty($actions)) {?>
                    <center>На данный момент нету актуальных акций</center>
                <? } else {
                    foreach ($actions as $action) { ?>
                        <div class="item">
                            <span class="image"><img src="<?=$action['THUMB']?>" alt=""></span>
								<span class="text">
									<p class="title"><a href="/actions/item/<?=$action['ID']?>"><?=$action['NAME']?></a></p>
									<p class="about"><?=$action['DESCRIPTION']?></p>
									<p class="price"><?=$action['PRICE']?> <span class="value">Р</span></p>
                                    <? if (!empty($action['DISCOUNT'])) {?>
                                        <span class="sale"><?=$action['DISCOUNT']?> <var>скидка</var></span>
                                    <? } ?>
								</span>
                        </div>
                    <? } ?>
                    <p class="all">Все <a href="/actions/catalog"><?=$quantity?> акции</a></p>
                <? } ?>
            </div>
        </div>
    </div>
    <div id="map"><img class="tmp" src="/temp/map.jpg" alt=""></div>
</div>