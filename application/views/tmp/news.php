<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="news">
    <div class="container_row">
        <h3>Новости</h3>
        <div class="new_block">
            <? foreach ($news as $item) { ?>
                <div class="item">
                    <div class="top">
                        <a href="/news/read/<?=$item["ID"]?>" title="Быличка иллюстрирует стих">
                            <img src="<?=(!empty($item["IMAGE"]) ? $item["IMAGE"] : "/images/placeholders/news.jpg")?>" alt="<?=$item["TITLE"]?>">
                            <div style="width: 100%;height: 100%;background: #000;opacity: 0.3; position: absolute; top: 0;left: 0;"></div>
                            <span class="title georgia">
                                <?=$item["TITLE"]?>
                            </span>
                        </a>
                    </div>
                    <div class="text">
                        <?
                        if (strlen($item["DESCRIPTION"]) > 297) {
                            $formated = mb_substr($item["DESCRIPTION"], 0, 300);
                            echo $formated."...";
                        } else {
                            echo $item["DESCRIPTION"];
                        }
                        ?>
                        <span class="date"><?=$item["DATE"]?></span>
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
</div>
