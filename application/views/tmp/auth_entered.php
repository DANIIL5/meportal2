<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="auth">
    <form action="/profile/logout?back_url=<?=$_SERVER['REQUEST_URI']?>" method="post" enctype="multipart/form-data">
        <div class="line"><a href="/profile"><img class="ava" style="max-height: 33px; max-width: 33px;" src="<?=(!empty($data['PHOTO']) ? $data['PHOTO'] : "/images/placeholders/user_chat.jpg" )?>" alt="ava"/></a></div>
        <div class="line"><a href="/profile" class="name"><?=$data['EMAIL']?></a></div>
        <div class="line message"><a href="/profile/posts"><b><?=$msg_count?> новых</b></a> сообщения</div>
        <div class="button"><input type="submit" class="submit" value="Выйти"></div>
    </form>
</div>
