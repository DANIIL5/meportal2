<!--  END - MAIN MENU -->
<div class="content"> 
    <div class="box box_work-graph">
        <span class="title title-box title_center">График</span>
        <div class="b-nav-calend js-nav-calend">
            <span class="b-nav-calend__crsl b-nav-calend__crsl_prew js-nav-calend__nav-i" id="crsl_prew"></span>
            <span class="b-nav-calend__crsl b-nav-calend__crsl_next js-nav-calend__nav-i" id="crsl_next"></span>
            <div class="b-nav-calend__dweek js-nav-calend__dWeek">
                <input value="ДЕНЬ" class="btn-lk btn-lk_inner btn-lk_inner_contour <?php if($controller_method=='/profile/schedule_day') echo 'b-nav-calend__dweek_active' ?>" type="submit" onclick="window.location.pathname='/profile/schedule_day'">
                <input value="НЕДЕЛя" class="btn-lk btn-lk_inner btn-lk_inner_contour <?php if($controller_method=='/profile/schedule_week') echo 'b-nav-calend__dweek_active' ?>" type="submit" onclick="window.location.pathname='/profile/schedule_week'">
                <input value="Месяц" class="btn-lk btn-lk_inner btn-lk_inner_contour <?php if($controller_method=='/profile/schedule_month') echo 'b-nav-calend__dweek_active' ?>" type="submit" onclick="window.location.pathname='/profile/schedule_month'">
            </div>
        </div>