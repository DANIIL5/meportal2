<?php
if(!empty($addressUsers))
{
	?><ul class="b-RP-adress-list__l">
		<li class="b-RP-adress-list__l-i b-RP-adress-list__l-i_name">Пользователи <span class="b-RP-adress-list__l-i_count"> <?=count($addressUsers)?></span>
		</li><?php
		foreach ($addressUsers as $userAddress)
		{
			?><li class="b-RP-adress-list__l-i">
				<span class="b-RP-adress-list__l-i_s" onclick="window.location.pathname='/profile/post/<?=$userAddress['ID_USER_ADDRESS']?>/user'"><?=$userAddress['NAME_USER']?></span>
			</li><?php
		}
	?></ul><?php
}
if(!empty($addressSpecialists))
{
	?><ul class="b-RP-adress-list__l">
		<li class="b-RP-adress-list__l-i b-RP-adress-list__l-i_name">Врачи <span class="b-RP-adress-list__l-i_count"> <?=count($addressSpecialists)?></span>
		</li><?php
		foreach ($addressSpecialists as $specialistAddress)
		{
			?><li class="b-RP-adress-list__l-i">
				<span class="b-RP-adress-list__l-i_s" onclick="window.location.pathname='/profile/post/<?=$specialistAddress['ID_SPECIALIST']?>/specialist'"><?=$specialistAddress['NAME_SPECIALIST']?></span>
			</li><?php
		}
	?></ul><?php
}
if(!empty($addressInstitutions))
{
	?><ul class="b-RP-adress-list__l">
		<li class="b-RP-adress-list__l-i b-RP-adress-list__l-i_name">Учреждения <span class="b-RP-adress-list__l-i_count"> <?=count($addressInstitutions)?></span>
		</li><?php
		foreach ($addressInstitutions as $institutionAddress)
		{
			?><li class="b-RP-adress-list__l-i">
				<span class="b-RP-adress-list__l-i_s" onclick="window.location.pathname='/profile/post/<?=$institutionAddress['ID_INSTITUTION']?>/institution'"><?=$institutionAddress['NAME_INSTITUTION']?></span>
			</li><?php
		}
	?></ul><?php
}?>