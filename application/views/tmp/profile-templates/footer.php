            <div class="footer">
            	<hr>
            	<ul class="footer-zone">
            		<li class="footer-zone__i footer-zone__i_1">
            			<span class="footer-tel">8-800-555-31-91</span>
            		</li>
            		<li class="footer-zone__i footer-zone__i_2">
            			<span class="footer-copr"> Все права защищены. © 2015 Эл. почта для связи — </span>
            			<a class="footer-mailTo" href="mailto:hello@medportal.ru">hello@medportal.ru</a>
            		</li>
            	</ul>
            </div>
            <!--  END FOOTER -->
        </div><?php
        if($controller_method != '/profile/pay')
        {
            
            ?><div <?if(isset($_SESSION['save']) && $_SESSION['save']==true):?>style="display:block"<?endif; ?> class="pUps js-pUp_m" data-redirect="_" >
                <div class="pUps__bg js-pUp_m__bg"></div>
                <div class="pUps__w pUps__w_nX">
                    <span class="pUps__x js-pUp_m__x"></span>
                    <div class="pUps__reg js-pUp_m__reg">
                        <!-- list popups of ID -->
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-add-fav">
                            <p>Врач <span class="pUps__item-fio js-pUp__x">Фамилия Имя</span> добавлен в ваш список избранного</p>
                            <input value="Закрыть" class="pUps__btn js-pUp__x" type="submit">
                        </div>
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-logout">
                            <p>Вы действительно хотите выйти?</p>
                            <input value="Да" class="pUps__btn _btn-logout" type="submit" style="margin-right: 20px;" data-target-id="logout" onclick="window.location.pathname='/profile/logout'">
                            <input value="Отмена" class="pUps__btn js-pUp__x" type="submit">
                        </div>
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item delete-posts" id="pUp-del">
                            <p>Вы действительно хотите удалить отзыв?</p>
                            <input value="Да" class="pUps__btn _btn-del" type="submit" style="margin-right: 20px;" data-target-id="del" >
                            <input value="Отмена" class="pUps__btn js-pUp__x" type="submit">
                        </div>
                       
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-save-thx">
                            <p></p>
                            <input value="Закрыть" class="pUps__btn js-pUp__x" type="submit">
                        </div>
                        
                         <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-save-thx"  <?if(isset($_SESSION['save']) && $_SESSION['save']==true):?><?unset($_SESSION['save']); ?>style="display:block";<?endif; ?>>
                            <p>Организация успешно сохранена</p>
                            <input value="Закрыть" class="pUps__btn js-pUp__x" type="submit">
                        </div>
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item delete-popup" id="del-default">
                            <p></p>
                            <input value="Да" class="pUps__btn _btn-del-target" type="submit" style="margin-right: 20px;" data-target-id="not-id" onclick="pUp_m.eHidePupDel(this);">
                            <input value="Отмена" class="pUps__btn js-pUp__x" type="submit">
                        </div>
                    </div>
                </div>
            </div><?php
        }
        else
        {   
            
            ?><div class="pUps pUps_pay js-pUp_m" data-redirect="_">
                <div class="pUps__bg js-pUp_m__bg"></div>
                <div class="pUps__w">
                    <span class="pUps__x js-pUp__x"></span>
                    <div class="pUps__reg js-pUp_m__reg">
                        <!-- list popups of ID -->
                        <div class="pUps__item pUps__item_add-fav js-pUp_m__item" id="pUp-logout">
                            <p>Вы действительно хотите выйти?</p>
                            <input value="Да" class="pUps__btn _btn-logout" type="submit" style="margin-right: 20px;" data-target-id="logout" onclick="window.location.pathname='/profile/logout'">
                            <input value="Отмена" class="pUps__btn js-pUp__x" type="submit">
                        </div>
                        <div class="pUps__item pUps__item_ORDER js-pUp_m__item" id="pUp-view-ORDER" style="background: white;">
                            <div class="pUps__item-title b-print_NO" style="text-align: center;margin-bottom: 10px;">
                               Инструкция по оплате и использованию 
                            </div>
                            <p class="pUps__item-defTxt pUps__item-defTxt_col-grey b-print_NO">
                                   <p style="text-align: justify;font-size: 16px;margin-bottom: 10px;"> 1. Загрузите данные плательщика  из личного кабинета,  нажав пункт  меню внизу счета.</p>
                                   <p style="text-align: justify;font-size: 16px;margin-bottom: 10px;"> 2. Если реквизиты плательщика отличаются от реквизитов компании, зарегистрировавшей личный кабинет, заполните реквизиты самостоятельно.</p>
                                     <p style="text-align: justify;font-size: 16px;margin-bottom: 10px;">3. Укажите количество отделений (филиалов).</p>
                                     <p style="text-align: justify;font-size: 16px;margin-bottom: 10px;">4. Распечатайте счет или сохраните в удобном формате.</p>

                            </p>
                           <!-- TABLE _ ORDER -->
                            <div class="b-order b-order_print b-print">
                            <p class="b-order__alert">
                              Внимание! Оплата данного счёта означачет согласие с условиями оказания услуг.
                              <br> Уведомление об оплате обязательно, в противном случае не гарантируется подключение услуги в течении 24 часов.
                              <br> Услуга оказывается по факту прихода денег на р/с Поставщика.
                            </p>
                            </br>
                            <form class="b-print" onsubmit="return mfp.checkPayAsMSWordDocx(this)" action="/profile/checkPayAsMSWordDocx">
                              <table class="b-order__r">
                                <tbody>
                                  <tr style="border-bottom: none;">
                                    <td colspan="4" style="width: 30%; border-right: 1px solid #000;">
                                      ОАО «Сбербанк России»
                                    </td>
                                    <td style="width: 7%; border-right: 1px solid #000; border-bottom: 1px solid #000">БИК</td>
                                    <td style="width: 24%; border-right: 1px solid #000;">044525225</td>
                                  </tr>
                                  <tr style="border-bottom:none">
                                    <td colspan="4" style="width: 30%; border-right: 1px solid;">
                                     
                                    </td>
                                    <td style="width: 7%; border-right: 1px solid;">Сч.№</td>
                                    <td  style="width: 24%; border-right: 1px solid;">30101810400000000225</td>
                                  </tr>
                                  <tr style="border-bottom:1px solid #000;">
                                    <td colspan="4" style="width: 30%; border-right: 1px solid #000;">
                                       Банк получателя
                                    </td>
                                    <td style="width: 7%; border-right: 1px solid #000;"></td>
                                    <td  style="width: 24%; border-right: 1px solid #000;"></td>
                                  </tr>
                                  <tr>
                                    <td style=" width:3%; border-bottom: 1px solid #000">
                                      ИНН
                                    </td>
                                    <td style=" width:18%; border-right: 1px solid #000; border-bottom: 1px solid #000">9705041355</td>
                                    <td style=" width:5%; border-bottom: 1px solid #000">
                                      КПП
                                    </td>
                                    <td style=" width:18%; border-right: 1px solid #000; border-bottom: 1px solid #000">7705501001</td>
                                    <td style="border-right: 1px solid #000;">Сч.№</td>
                                    <td>40702810138000047945</td>
                                  </tr>
                                  <tr style="border-bottom:none">
                                    <td colspan="4" style="width: 30%; border-right: 1px solid;">
                                      ООО «МЕД-ФАРМ ПОРТАЛ»
                                    </td>
                                    <td style="width: 7%; border-right: 1px solid;"></td>
                                    <td  style="width: 24%; border-right: 1px solid;"></td>
                                  </tr>
                                  <tr style="border-bottom:none">
                                    <td colspan="4" style="width: 30%; border-right: 1px solid;">
                                     
                                    </td>
                                    <td style="width: 7%; border-right: 1px solid;"></td>
                                    <td  style="width: 24%; border-right: 1px solid;"></td>
                                  </tr>
                                  <tr style="border-bottom:1px solid #000;">
                                    <td colspan="4" style="width: 30%; border-right: 1px solid;">
                                      Получатель
                                    </td>
                                    <td style="width: 7%; border-right: 1px solid;"></td>
                                    <td  style="width: 24%; border-right: 1px solid;"></td>
                                  </tr>
                                </tbody>
                              </table>
                              <table class="b-order__d">
                                <tr style="border-bottom:1px solid #000;">
                                  <td class="b-order__d-ttl">
                                    Счет на оплату № <div class="b-DIV-input js-emul-input _check_id_input" style="width:150px"></div><input type="hidden" name="check_id_input">
                                     от <div class="b-DIV-input js-emul-input _check_date_input" style="width:150px"></div><input type="hidden" name="check_date_input"> г.
                                  </td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom: 0;">
                                    <table class="b-order__req-list">
                                      <tbody>
                                        <tr>
                                          <td width="100">
                                            Поставщик
                                          </td>
                                          <td>
                                            ООО «МЕД-ФАРМ ПОРТАЛ», ИНН 9705041355, КПП 770501001, Адрес 115035, г. Москва, ул. Пятницкая, дом 6/1 стр.8, Телефон 8-800-555-31-91 
                                          </td>
                                        </tr>
                                        <tr>
                                          <td width="100">
                                            Покупатель
                                          </td>
                                          <td>
                                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <div style="display: inline-block;width: 22px;"></div><div class="b-DIV-input js-emul-input _input_name_institution_pay input_institution_pay" contenteditable="true"></div><input type="hidden" name="input_name_institution_pay"/>,
                                            <br>ИНН&nbsp;<div class="b-DIV-input js-emul-input _input_inn_institution_pay input_institution_pay" contenteditable="true" style="margin-left: 27px;"></div><input type="hidden" name="input_inn_institution_pay"/>,
                                            <br>КПП&nbsp;<div class="b-DIV-input js-emul-input _input_kpp_institution_pay input_institution_pay" contenteditable="true" style="margin-left: 29px;"></div><input type="hidden" name="input_kpp_institution_pay"/>,
                                            <br>Адрес&nbsp;<div class="b-DIV-input js-emul-input _input_address_institution_pay input_institution_pay" contenteditable="true" style="margin-left:18px;"></div><input type="hidden" name="input_address_institution_pay"/>,
                                            <div class="b-input-label_now-wrap">
                                                <label>Телефон <input class="js-tel _input_phone_institution_pay" name="input_phone_institution_pay" size="15" type="text"/></label>,
                                            </div>
                                            <div class="b-input-label_now-wrap">
                                                <label>Факс <input class="js-tel _input_fax_institution_pay" size="15" name="input_fax_institution_pay" type="text"/></label>
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td width="100" style="padding-top: 0;">
                                    <table class="b-order__TABLE-i">
                                      <thead>
                                        <tr>
                                          <td width="30" style="max-width:30px;">
                                            №
                                          </td>
                                          <td>
                                            Товары (работы, услуги)
                                          </td>
                                          <td width="80" style="max-width:50px;">
                                            Кол-во
                                          </td>
                                          <td width="50" style="max-width:50px;">
                                            Ед.
                                          </td>  
                                          <td width="85" style="max-width:85px;">
                                            Цена
                                          </td>
                                          <td width="105" style="max-width:105px;">
                                            Сумма
                                          </td>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>1</td>
                                          <td class="_name_cell _pay_data_cell"></td><input type="hidden" name="name_cell"/>
                                         
                                          <td><input type="text" name="m" class="myclass" value="1" style="width:45px"/></td>
                                           <td class="_months_cell _pay_data_cell" style="width:100px;"></td><input type="hidden" name="months_cell"/>
                                          <td class="_price_cell"></td>
                                          <td class="_special_price_cell"></td>
                                        </tr>
                                        <tr>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                      <tbody class="b-order__TABLE-i_result">
                                        <tr>
                                          <td colspan="5"></td>
                                          <td  width="105" style="max-width:105px;"></td>
                                        </tr>
                                        <tr>
                                          <td colspan="5">Итого:</td>
                                          <td width="105" style="max-width:105px;">
                                            <div class="b-DIV-input js-emul-input _price_input" style="min-width:20px"></div><input type="hidden" name="price_input"/>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td colspan="5">В том числе НДС:</td>
                                          <td  width="105" style="max-width:105px;">
                                            <div class="b-DIV-input js-emul-input _price_input_nds" style="min-width:20px"></div><input type="hidden" name="price_input_nds"/>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td colspan="5">Всего к оплате:</td>
                                          <td  width="105" style="max-width:105px;">
                                            <div class="b-DIV-input js-emul-input _price_input" style="min-width:20px"></div><input type="hidden"/>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table class="b-order__TABLE-i_result-italic">
                                       <tbody>
                                        <tr>
                                          <td>Всего&nbsp;наименований </td>
                                          <td>
                                            <div class="b-DIV-input js-emul-input" style="min-width:20px">1</div><input type="hidden"/>
                                          </td>
                                          <td>,&nbsp;на&nbsp;сумму
                                          </td>
                                          <td width="100%">
                                              <div class="b-DIV-input js-emul-input _price_input_nds" style="min-width:100%"></div><input type="hidden"/>
                                          </td>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                              <table class="b-order__foot">
                                <tr>
                                  <td>Генеральный директор</td>
                                  <td>Поздеева Т.А.</td>
                                  <td>Бухгалтер</td>
                                  <td>Поздеева Т.А.</td>
                                </tr>
                                <tr>
                                  <td>М.П.</td>
                                </tr>
                              </table>
                              <div class="b-order__submit b-print_NO">
                                <input value="загрузить данные плательщика из ЛК" class="pUps__btn" type="button" onclick="mfp.payFill(this);"/>
                                <input value="Скачать счет" class="pUps__btn" type="button" onclick="download_bill()"/>
                                <input value="Распечатать счет" class="pUps__btn" type="button" onclick="print();"/>
                              </div>
                            </form>
                           </div>
                          <!-- END - TABLE _ ORDER -->
                        </div>
                        <!-- END list popups -->
                    </div>
                </div>
            </div><?php
        }?><script type="text/javascript">
            var pageVariables={
                controllerMethod:'<?=$controller_method?>'
            }
        </script>
        <!--  import JS - lib -->
        <script type="text/javascript" src="/js/lib/jquery-3.0.0.min.js"></script>
        <script type="text/javascript" src="/js/html2canvas.js"></script>
        <script type="text/javascript" src="/js/lib/jquery-ui.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.formstyler.min.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.tablesorter.min.js"></script>
        <!--  END import JS - lib -->
        <!--  import JS - usr -->
        <script type="text/javascript" src="/js/usr/main.js"></script>
        <script type="text/javascript" src="/js/profile-script.js"></script>
        <!--  END import JS - usr -->
        <!--  SCRIPTS special for branches page -->
        <!-- YANDEX _ MAP  -->
        <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
        <script type="text/javascript" src="/js/usr/map.js"></script>
        <script>
            $(".phone_mask").mask("+7 (999) 999-99-99");
        </script>
        <script>
            $('.time').mask('99:99');
        </script>
         <!-- END YANDEX _ MAP  -->
    </body>
</html>