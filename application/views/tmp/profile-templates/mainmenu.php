<!DOCTYPE HTML>
<html lang="ru">
    <head>
        <title>МедФармПортал - Личный кабинет</title>
        <!--  META BLOCK -->
        <meta charset="UTF-8"/>
        <meta name="description" content="MFP"/>
        <meta name="keywords" content="MFP"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <!--  END META BLOCK -->
        <!--  import CSS -->
            <link href="/css/lib/jquery-ui.css" media="screen" rel="stylesheet" type="text/css"/>
            <link href="/css/basic.css" media="screen" rel="stylesheet" type="text/css"/>
            <link href="/css/main.css" media="screen" rel="stylesheet" type="text/css"/>
            <link rel="stylesheet" type="text/css" media="print" href="/css/print.css"/>
        <!--  END import CSS -->
    </head>
    <body class="body" id="body" <?php if($controller_method=='/profile/posts'):?> style="height:1059px"<?endif; ?>>
            <div class="body-content <?php if($controller_method=='/profile/posts' or $controller_method=='/profile/post') echo 'body-content_right-panel' ?><?php if($controller_method=='/profile/pay') echo 'b-print_NO' ?>">
                <!--  MAIN MENU -->
                <div class="main-menu">
                    <a class="main-menu__back-s" href="/">МЕДФАРМПОРТАЛ</a>
                    <ul class="main-menu-list"><?php
                        foreach ($mainMenuItems as $key => $item)
                        {
                            ?><li class="main-menu-list__i main-menu-list__i_<?=$item['style']?> <?php if($controller_method==$item['href'] or $controller_method==$item['search']) echo 'main-menu-list__i_active' ?>">
                               
                                <a href="<?=$item['href']?>" <?php if($item['href']=='javascript:void(0);') echo 'onclick="pUp_m.eGetId(this,0)" data-pup-id="pUp-logout"' ?>><?=$key?>
                                </a>
                          
                                <?
 
                                $key=str_replace(' ','',$key);
                                
                                ?>
                                <?if($key == 'ОТЗЫВЫ' && !empty($unreaded) && $unreaded[0]['COUNT(*)']!=0):?>
                                  
                               <span class="main-menu-new-item" style="position: absolute;left:55px;"><?=$unreaded[0]['COUNT(*)']?></span> 
                               
                                <?endif;?>
                                
                                <?php
                                    if(!empty($item['submenu']))
                                    {
                                        ?><ul class="main-menu-list-inner"><?php
                                            foreach ($item['submenu'] as $keySub => $point)
                                            {
                                                ?><li class="main-menu-list-inner__i <?php if($_SERVER['PHP_SELF']=='/index.php'.$point['href']) echo 'active'; ?>">
                                                    <a href="<?=$point['href']?>"><?=$keySub?></a><?php
                                                    if (!empty($point['unreaded']))
                                                    {
                                                        ?><span class="main-menu-new-item"><?=$point['unreaded']?></span>
                                                        
                                                        <?php
                                                    }
                                                  
                                                ?>
                                                </li><?php
                                            }
                                        ?></ul><?php
                                    }
                            ?></li><?php
                        }
                    ?></ul>
                </div>