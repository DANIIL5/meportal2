            <div id="js-event-desc" class="b-calend__event_content b-calend-e">
                <hr class="b-calend__event_content-top-hr">
                <span class="b-calend-e__arr js-event-arr"></span>
                <span class="b-calend-e__title"></span>
                <ul class="b-calend-e__time">
                    <li class="b-calend-e__time_i __time">
                        Время<br><span></span>
                    </li>
                    <li class="b-calend-e__time_i __date">
                        Дата<br><span></span>
                    </li>
                </ul>
                <p class="paragraph __desc"></p>
                <hr class="b-calend__event_content-bottom-hr">
            </div>
        </div>
    </div>
</div>
<!-- //= template/right-panel.html -->
<!--  FOOTER -->