<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? if(empty($data)) { ?>
    Такого набора лекарств нету в наших аптеках
<? } else { ?>
    <? foreach ($data as $item) { ?>
        <table class="results-table">
            <tr>
                <th class="name">Аптека</th>
                <th class="distance">Удаленность</th>
                <th class="price">Цена</th>
                <th class="value">Количество</th>
                <th class="on-map">На карте</th>
            </tr>
        </table>
        <div class="no-active">
            <table class="results-table">
                <tr>
                    <td class="name"><a href="#" class="dotted"><?=$item['DRUGSTORE']['NAME']?></a></td>
                    <td class="distance">~ 15 мин</td>
                    <td class="price"><span class="value"><?=$item['DRUGSTORE']['FULLPRICE']?></span></td>
                    <td class="value"></td>
                    <td class="on-map"><a href="#" class="link">На карте</a></td>
                </tr>
                <tr>
                    <td colspan="5" class="list-detail">
                        <div class="two-cols">
                            <table class="results-table">
                                <?
                                end($item['GOODS']);
                                $last_key = key($item['GOODS']);
                                reset($item['GOODS']);
                                foreach ($item['GOODS'] as $key => $good) { ?>
                                    <tr>
                                        <td class="name"><?=$good['NAME']?></td>
                                        <td class="distance"><?=$good['DRUGFORM']?></td>
                                        <td class="price"><?=$good['PRICE']?> Р <span class="num red">х<?=$good['COUNT']?></span></td>
                                        <td colspan="2"><?=($key == $last_key) ? $item['DRUGSTORE']['FULLPRICE'] : "&nbsp;" ;?></td>
                                    </tr>
                                <? }?>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    <? }?>
<? } ?>