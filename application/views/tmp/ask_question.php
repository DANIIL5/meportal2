<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="center_col ask-question">
    <h2>Задайте вопрос</h2>
    <div class="chat_top">
        <div class="ava">
            <span class="image"><span class="cell"><img src="/images/chat-pic-1.png" alt=""></span></span>
            <span class="image"><span class="cell"><img src="/images/chat-pic-2.png" alt=""></span></span>
            <span class="image active"><span class="cell"><img src="/images/chat-pic-3.png" alt=""></span></span>
        </div>
        <p class="note">Нашим врачам, фармаколагам и операторам</p>
    </div>
    <form action="#" class="ask">
        <div class="chat_bottom">
            <? if ($is_auth) {?>
                <span class="left_block">Вы</span>
                <span class="right_block">
                    <textarea cols="30" rows="4" placeholder="Здраствуйте, я ищу недорогую аптеку в районе Московского вокзала, желательно, чтобы она работала круглосуточно." name="question"></textarea>
                </span>
            <? } else {?>
                <p class="centered">Для обратной связи необходимо авторизоваться</p>
            <? } ?>
        </div>
        <? if ($is_auth) {?>
        <input type="submit" class="submit btn_white submit_question" value="Отправить" title="Отправить"/>
        <? } ?>
    </form>
</div>
