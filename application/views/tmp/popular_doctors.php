<div class="popular popular-doctors">
    <div class="container">
        <h2>Популярные врачи</h2>

        <? foreach ($doctors as $doctor) { ?>
            <div class="item">
                <span class="ava"><img src="<?=$doctor["PHOTO"]?>" alt=""></span>
                <p><a href="#"><?=$doctor["LAST_NAME"]?> <?=$doctor["FIRST_NAME"]?> <?=$doctor["SECOND_NAME"]?></a></p>
                Терапевт
            </div>
        <? } ?>

<!--        <div class="item">-->
<!--            <span class="ava"><img src="/temp/16.jpg" alt=""></span>-->
<!--            <p><a href="#">Иванов В. И.</a></p>-->
<!--            Терапевт-->
<!--        </div>-->
    </div>
</div>