<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
        <div class="b-logos-row">
            <div class="container">
                <div class="b-logos-box">
                    <ul class="b-logos-i">
                        <? foreach ($logos as $logo) { ?>
                            <div>
                                <li class="b-logos-i__i"><a href="/organisations/item/<?=$logo['ID']?>"><img src="/images/organisations/<?=str_replace(['.jpg','.png','.gif','.jpeg','.bmp'], ['_thumb.jpg','_thumb.png','_thumb.gif','_thumb.jpeg','_thumb.bmp'], $logo['LOGO'])?>" alt="<?=$logo['BRAND']?>"></a></li>
                                <div style="margin-top: 5px;">
                                    <a href="/organisations/item/<?=$logo['ID']?>"><p style="font-size: 11px; margin-bottom: 0px; font-weight: bold;"><?=$logo['BRAND']?></p></a>
                                    <p style="font-size: 10px; font-weight: bold;"><?=(!empty($logo ['get_addresses']['actual'])) ? implode(',', array_filter($logo['get_addresses']['actual'])) : ""?></p>
                                </div>
                            </div>
                        <? } ?>
                    </ul>
                </div>
            </div>
        </div>