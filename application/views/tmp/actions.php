<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="actions">
    <h2>Акции</h2>
	<? if (empty($actions)) {?>
		<center>На данный момент актуальных акций нет</center>
	<? } else {
		foreach ($actions as $action) { ?>
			<div class="item">
				<span class="image"><img src="<?=$action['THUMB']?>" alt=""></span>
								<span class="text">
									<p class="title"><a href="/actions/item/<?=$action['ID']?>"><?=$action['NAME']?></a></p>
									<p class="about"><?=$action['DESCRIPTION']?></p>
									<p class="price"><?=$action['PRICE']?> <span class="value">Р</span></p>
									<? if (!empty($action['DISCOUNT'])) {?>
										<span class="sale"><?=$action['DISCOUNT']?> <var>скидка</var></span>
									<? } ?>
								</span>
			</div>
		<? } ?>
		<p class="all">Все <a href="/actions/catalog"><?=$quantity?> акции</a></p>
	<? } ?>
</div>
