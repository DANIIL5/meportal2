<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="search" class="container">
    <div class="wrapper">
        <span class="title georgia">Найти</span>
        <div id="main_search" class="forma b-search-search-hint js-search-box">
            <form action="/search/result" method="get">
                <span class="for_input"><input id="search_query" type="text" class="text" placeholder="например, аптека на Невском" autocomplete="off" name="query" value="<?=!empty($_GET['query']) ? $_GET['query'] : ''?>"></span>
                <input type="submit" class="submit btn" value="Найти" title="Найти">
            </form>

        </div>
    </div>
</div>
