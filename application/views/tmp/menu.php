<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="menu container">
    <ul class="main-list list">
        <li <?=($controller == "institutions") ? "class=\"active\"" : ""?>><a href="/institutions">Мед. учреждения</a></li>
        <li <?=($controller == "doctors") ? "class=\"active\"" : ""?>><a href="/doctors">Врачи</a></li>
        <li <?=($controller == "drugstores") ? "class=\"active\"" : ""?>><a href="/drugstores">Аптеки</a></li>
<!--        <li --<?////=($controller == "hls") ? "class=\"active\"" : ""?><a href="/hls">Зож</a></li>-->
        <li <?=($controller == "beauty") ? "class=\"active\"" : ""?>><a href="/beauty">Здоровье и красота</a></li>
<!--        <li><a href="#">Справочники</a></li>-->
        <li <?=($controller == "mother_and_child") ? "class=\"active\"" : ""?>><a href="/profile/auth" class="in-development">Мама и ребенок</a></li>
        <li <?=($controller == "rehabilitation") ? "class=\"active\"" : ""?>><a href="/profile/auth" class="in-development">Реабилитация</a></li>
        <li <?=($controller == "veterinary") ? "class=\"active\"" : ""?>><a href="/profile/auth" class="in-development">Ветеренария</a></li>
    </ul>
</nav>
