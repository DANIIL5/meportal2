<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="auth">
    <h3>Авторизация</h3>
    <form action="/profile/auth?back_url=<?=$_SERVER['REQUEST_URI']?>" method="post" enctype="multipart/form-data">
        <span class="login"><input type="text" class="text" name="email" placeholder="Логин"/></span>
        <span class="pass"><input type="password" class="text" name="password" placeholder="Пароль"/></span>
        <input type="submit" class="submit" name="auth" value="Войти">
        <a href="/profile/registration" class="link">Регистрация</a>
    </form>
</div>
