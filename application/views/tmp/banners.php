<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="banners">
    <div class="container_row">
        <? foreach ($banners as $banner) { ?>
            <div class="banner"><a href="<?=$banner["HREF"]?>"><img src="<?=$banner["PATH"]?>" alt="<?=$banner["TITLE"]?>"></a></div>
        <? } ?>
        <?
        $unloaded_banners = 4 - count($banners);
        if ($unloaded_banners > 0) {
            for ($i = 0; $i < $unloaded_banners; $i++) {?>
                <div class="banner"><a href="#"><img src="/images/placeholders/banner.jpg" alt="Закать рекламу"></a></div>
            <?}
        }?>
    </div>
</div>
