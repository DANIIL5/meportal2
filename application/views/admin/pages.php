<div class="uk-width-1-1">
    <h1 class="uk-h1"><?=$page_type?></h1>

    <table class="uk-width-1-1 uk-table uk-table-hover small-font">
        <thead>
        <tr>
            <th style="width: 10%;">ID</th>
            <th style="width: 70%;">Вопрос</th>
            <th style="width: 20%;">Действия</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($catalog as $item) { ?>
            <tr>
                <td><?=$item['ID']?></td>
                <td><?=$item['NAME']?></td>
                <td>
                    <a href="/cp/page_edit/<?=$type?>/<?=$item['ID']?>"><i class="small-icon uk-icon-edit" data-uk-tooltip title="Редактировать"></i></a>
                    <a href="/cp/page_delete/<?=$type?>/<?=$item['ID']?>"><i class="small-icon uk-icon-trash-o" data-uk-tooltip title="Удалить"></i></a>
                </td>
            </tr>
        <? } ?>
        </tbody>
    </table>

    <a class="uk-button uk-button-primary" href="/cp/page_edit/<?=$type?>">Добавить страницу</a>

<!--    --><?//=$pagination?>

<!--    <ul class="uk-pagination">-->
<!--        <li><a href="">1</a></li>-->
<!--        <li class="uk-active"><span>2</span></li>-->
<!--        <li><a href="">3</a></li>-->
<!--        <li><a href="">4</a></li>-->
<!--    </ul>-->
</div>