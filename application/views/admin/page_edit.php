<div class="uk-width-1-1">
    <h1 class="uk-h1"><?=(!empty($page['NAME'])) ? $page['NAME'] : $type_name.": Новая страница"?></h1>

    <form id="form" enctype="multipart/form-data" method="post" action="/cp/page_save/<?=!empty($page['ID']) ? $page['ID'] : '0/'.$type_id?>" class="uk-form uk-form-horizontal uk-margin">
        <div class="uk-form uk-width-1-1" data-uk-margin>
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-s">Название</label>
                <div class="uk-form-controls">
                    <input class="uk-width-1-1" type="text" name="name" value="<?=(!empty($page['NAME'])) ? $page['NAME'] : ""?>">
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-s">Текст страницы</label>
                <div class="uk-form-controls">
                    <textarea id="tinymce" class="uk-width-1-1" name="text" id="" cols="30" rows="20"><?=(!empty($page['TEXT'])) ? $page['TEXT'] : ""?></textarea>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-s"></label>
                <div class="uk-form-controls">
                    <button type="submit" value="Сохранить изменения" name="submit" class="uk-button uk-button-primary">Сохранить изменения</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    tinymce.init({
        selector: '#tinymce',
        height: 500,
        apply_source_formatting : true,
        forced_root_block: false,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>