<div class="uk-width-1-1">
    <h1 class="uk-h1">Организации</h1>

    <table class="uk-width-1-1 uk-table uk-table-hover small-font">
        <thead>
        <tr>
            <th style="width: 3%;">ID</th>
            <th style="width: 7%;"></th>
            <th style="width: 20%;">Бренд</th>
            <th style="width: 20%;">Юр.лицо</th>
            <th style="width: 15%;">Категория</th>
            <th style="width: 15%;">Тип</th>
            <th style="width: 10%;">Последнее изменение</th>
            <th style="width: 10%;">Действия</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($catalog as $key => $item) { ?>
            <tr>
                <td><?=$item['ID']?></td>
                <td>
                    <? if (!empty($item['LOGO'])) { ?>
                        <img src="<?=$item['LOGO']?>" alt="">
                    <? } else { ?>
                        <img class="uk-thumbnail table-small-image">
                    <? } ?>
                </td>
                <td><p><?=$item['BRAND']?></p></td>
                <td><?=$item['ENTITY']?></td>
                <td><?=$item['CATEGORY']?></td>
                <td><?=$item['TYPE']?></td>
                <td><?=$item['UPDATED_AT']?></td>
                <td>
                    <? if (empty($item['PARENT_ID'])) { ?><a href="/cp/new_organizations_create/<?=$item['ID']?>"><i class="small-icon uk-icon-hospital-o" data-uk-tooltip title="Добавить отделение"></i></a><? } ?>
                    <a href="/cp/new_organizations_edit/<?=$item['ID']?>"><i class="small-icon uk-icon-edit" data-uk-tooltip title="Редактировать"></i></a>
                    <a href="/cp/new_organizations_delete/<?=$item['ID']?>"><i class="small-icon uk-icon-trash-o" data-uk-tooltip title="Удалить"></i></a>
                </td>
            </tr>
        <? } ?>
        </tbody>
    </table>

    <a class="uk-button uk-button-primary" href="/cp/new_organizations_create">Добавить организацию</a>

    <?=$pagination?>

<!--    <ul class="uk-pagination">-->
<!--        <li><a href="">1</a></li>-->
<!--        <li class="uk-active"><span>2</span></li>-->
<!--        <li><a href="">3</a></li>-->
<!--        <li><a href="">4</a></li>-->
<!--    </ul>-->
</div>