<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/admin2/css/uikit.gradient.min.css" />
    <link rel="stylesheet" href="/admin2/css/components/form-file.gradient.min.css" />
    <link rel="stylesheet" href="/admin2/css/components/tooltip.gradient.min.css" />
    <link rel="stylesheet" href="/admin2/css/components/datepicker.gradient.min.css" />
    <link rel="stylesheet" href="/admin2/css/style.css">
    <script src="/admin2/js/jquery-3.1.0.min.js"></script>
    <script src="/admin2/js/uikit.min.js"></script>
    <script src="/admin2/js/components/tooltip.min.js"></script>
    <script src="/admin2/js/components/datepicker.min.js"></script>
    <script src="/admin2/js/script.js"></script>
    <script src="/admin2/js/jquery.mask.min.js"></script>
    <script src="/admin/js/tinymce/tinymce.min.js"></script>
</head>

<body>
<div class="uk-container uk-container-center uk-margin-top uk-margin-large-bottom">
    <nav class="uk-navbar uk-margin-large-bottom">
        <a class="uk-navbar-brand" href="/">МФП</a>
        <ul class="uk-navbar-nav">
            <li>
                <a href="">Рабочий стол</a>
            </li>
            <li data-uk-dropdown="">
                <a>Справочники</a>
                <div class="uk-dropdown">
                    <ul class="uk-nav uk-nav-dropdown">
                        <li>
                            <a href="/cp/admins">Администраторы</a>
                            <a href="/cp/new_organizations">Организации</a>
                            <a href="/cp/specialists">Специалисты</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li data-uk-dropdown="">
                <a>Лекарства</a>
                <div class="uk-dropdown">
                    <ul class="uk-nav uk-nav-dropdown">
                        <li>
                            <a href="">РЛС</a>
                            <a href="">МФП</a>
                            <a href="">Импортированные</a>
                            <a href="">Отклоненные</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li data-uk-dropdown="">
                <a>Контент</a>
                <div class="uk-dropdown">
                    <ul class="uk-nav uk-nav-dropdown">
                        <li>
                            <a href="/cp/articles">Публикации</a>
                            <a href="/cp/banners">Баннеры</a>
                            <a href="/cp/actions">Акции</a>
                            <a href="/cp/rss_news">RSS-новости</a>
                            <a href="/cp/pages/calculator">Калькуляторы</a>
                            <a href="/cp/pages/rating">Рейтинги</a>
                            <a href="/cp/vote">Голосования</a>
                        </li>
                    </ul>
                </div>
            </li>
              <li>
                <a href="">Пользователи</a>
            </li>
            <li>
                <a href="">Аналитика</a>
            </li>
            <li>
                <a href="">Продажи</a>
            </li>
            <li>
                <a href="">Финансы</a>
            </li>
           
        </ul>
    </nav>

    <?=$content?>
</div>
</body>
</html>