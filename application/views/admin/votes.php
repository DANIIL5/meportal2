<div class="uk-width-1-1">
    <h1 class="uk-h1">Голосования</h1>

    <table class="uk-width-1-1 uk-table uk-table-hover small-font">
        <thead>
        <tr>
            <th style="width: 10%;">ID</th>
            <th style="width: 70%;">Вопрос</th>
            <th style="width: 20%;">Действия</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($catalog as $item) { ?>
            <tr>
                <td><?=$item['ID']?></td>
                <td><?=$item['QUESTION']?></td>
                <td>
                    <a href="/cp/vote_edit/<?=$item['ID']?>"><i class="small-icon uk-icon-edit" data-uk-tooltip title="Редактировать"></i></a>
                    <a href="/cp/vote_delete/<?=$item['ID']?>"><i class="small-icon uk-icon-trash-o" data-uk-tooltip title="Удалить"></i></a>
                </td>
            </tr>
        <? } ?>
        </tbody>
    </table>

    <a class="uk-button uk-button-primary" href="/cp/vote_edit">Добавить голосование</a>

<!--    --><?//=$pagination?>

<!--    <ul class="uk-pagination">-->
<!--        <li><a href="">1</a></li>-->
<!--        <li class="uk-active"><span>2</span></li>-->
<!--        <li><a href="">3</a></li>-->
<!--        <li><a href="">4</a></li>-->
<!--    </ul>-->
</div>