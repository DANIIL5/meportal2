<div class="uk-width-1-1">
    <h1 class="uk-h1">Добавление организации</h1>
    <ul class="uk-tab uk-tab-bottom" data-uk-tab="{connect: '#tabs'}">
        <li class="uk-active"><a href="">Основное</a></li>
        <li><a href="">Реквизиты</a></li>
        <li><a href="">Администрация</a></li>
        <li><a href="">Геолокация</a></li>
        <li><a href="">Адреса</a></li>
        <li><a href="">Контакты</a></li>
        <li><a href="">Время работы</a></li>
        <li><a href="">Признаки</a></li>
        <li><a href="">Контент</a></li>
    </ul>

    <form id="form" enctype="multipart/form-data" method="post" action=" <? if (!empty($id)) { ?>/cp/new_organizations_update/<?=$id?><? } else { ?>/cp/new_organizations_save<? } ?>" class="uk-form uk-form-horizontal uk-margin">
        <? if (!empty($parent_id)) { ?><input type="hidden" name="parent_id" value="<?=$parent_id?>"><? } ?>
        <? if (!empty($id)) { ?><input type="hidden" name="id" value="<?=$id?>"><? } ?>
        <div id="tabs" class="uk-switcher uk-margin">
            <!-- Основная информация -->
            <div class="uk-form uk-width-1-1" data-uk-margin>
             
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Изображение организации</label>
                    <div class="uk-form-controls">
                        <div class="uk-form-file">
                            <? if (!empty($organization['LOGO'])) { ?><img class="uk-thumbnail" src="<?=$organization['LOGO']?>" alt="" style="max-width: 240px; max-height: 240px;"><? } ?>
                            <br>
                            <? print_r($_SESSION) ?>
                           <p style="color:#bbbbbb">размер фото не должен превышать 3мб.</p>
                            
                            <button class="uk-button">Загрузить изображение</button>
                            <input type="file" name="logo" class="logo">
                        </div>
                    </div>
                </div>

                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Владелец организации</label>
                    <div class="uk-form-controls">
                        <select name="user_id" class="form-control" id="">
                            <option value="">Без владельца</option>
                            <? foreach ($users as $user) { ?>
                                <option value="<?= $user['ID'] ?>" <? if (!empty($organization['USER_ID']) && $organization['USER_ID']==$user['ID'] ) {?> selected <? } ?>><?= $user['EMAIL'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-it">Категория размещения</label>
                    <div class="uk-form-controls">
                        <select name="category_id">
                            <option value="2" <? if ($organization['CATEGORY_ID'] == 2) { ?> selected <? } ?>>Медицинские учреждения</option>
                            <option value="4" <? if ($organization['CATEGORY_ID'] == 4) { ?> selected <? } ?>>Аптеки</option>
                            <option value="5" <? if ($organization['CATEGORY_ID'] == 5) { ?> selected <? } ?>>Красота и здоровье</option>
                            <option value="7" <? if ($organization['CATEGORY_ID'] == 7) { ?> selected <? } ?>>Мама и ребенок</option>
                            <option value="8" <? if ($organization['CATEGORY_ID'] == 8) { ?> selected <? } ?>>Реабилитация</option>
                            <option value="9" <? if ($organization['CATEGORY_ID'] == 9) { ?> selected <? } ?>>Ветеринария</option>
                        </select>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-it">Специализация</label>
                    <div class="uk-form-controls">
                        <select name="specialization_id">
                            <option value="">Не выбрано</option>
                            <? foreach ($specializations as $specialization) { ?>
                                <option value="<?=$specialization['ID']?>" <?=(!empty($organization['SPECIALIZATION_ID']) && $organization['SPECIALIZATION_ID'] == $specialization['ID']) ? "selected" : ""?>><?=$specialization['NAME']?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-it">Тип организации</label>
                    <div class="uk-form-controls">
                        <select name="type_id">
                            <? foreach ($types as $type) { ?>
                                <option value="<?=$type['ID']?>" <?=(!empty($organization['TYPE_ID']) && $organization['TYPE_ID'] == $type['ID']) ? "selected" : ""?>><?=$type['NAME']?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Брэнд</label>
                    <div class="uk-form-controls">
                        <input type="text" name="brand" value="<?=(!empty($organization['BRAND'])) ? htmlentities($organization['BRAND']) : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Юр.лицо</label>
                    <div class="uk-form-controls">
                        <input type="text" name="entity" value="<?=(!empty($organization['ENTITY'])) ? htmlentities($organization['ENTITY']) : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-it">Тип налогооблажения</label>
                    <div class="uk-form-controls">
                        <select name="taxation_id">
                            <option value="1">УСН</option>
                            <option value="2">ОСНО</option>
                        </select>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Описание</label>
                    <div class="uk-form-controls">
                        <textarea name="description" cols="70" rows="6"><?=(!empty($organization['DESCRIPTION'])) ? $organization['DESCRIPTION'] : ""?></textarea>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Лицензия</label>
                    <div class="uk-form-controls">
                        <input type="text" name="license" value="<?=(!empty($organization['LICENSE'])) ? htmlentities($organization['LICENSE']) : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <span class="uk-form-label">Разрешения</span>
                    <div class="uk-form-controls uk-form-controls-text" data-uk-tooltip="{pos:'top-left'}" title='Провереный: если включен,ba означает что администратор проверил организацию. Активный: если выключен, показывает что организация "забанена". Опубликованный: если включен, пользователь хочет показывать свою организацию всем'>
                        <input type="checkbox" id="form-h-c0" name="popular" <?=(!empty($organization['POPULAR'])) ? "checked" : ""?>> <label for="form-h-c0">Показывать в популярных?</label>
                        <input type="checkbox" id="form-h-c1" name="moderated" <?=(!empty($organization['MODERATED'])) ? "checked" : ""?>> <label for="form-h-c1">Провереный</label>
                        <input type="checkbox" id="form-h-c2" name="active" <?=(!empty($organization['ACTIVE'])) ? "checked" : ""?>> <label for="form-h-c2">Активный</label><br>
                        <input type="checkbox" id="form-h-c3" name="public" <?=(!empty($organization['PUBLIC'])) ? "checked" : ""?>> <label for="form-h-c3">Опубликованый</label>
                    </div>
                </div>
            </div>

            <!-- Реквезиты -->
            <div class="uk-form uk-width-1-1" data-uk-margin>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">ОГРН</label>
                    <div class="uk-form-controls">
                        <input type="text" name="ogrn" value="<?=(!empty($organization['OGRN'])) ? $organization['OGRN'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">ИНН</label>
                    <div class="uk-form-controls">
                        <input type="text" name="inn" value="<?=(!empty($organization['INN'])) ? $organization['INN'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">ОКВЭД</label>
                    <div class="uk-form-controls">
                        <input type="text" name="okved" value="<?=(!empty($organization['OKVED'])) ? $organization['OKVED'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">ОКПО</label>
                    <div class="uk-form-controls">
                        <input type="text" name="okpo" value="<?=(!empty($organization['OKPO'])) ? $organization['OKPO'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">КПП</label>
                    <div class="uk-form-controls">
                        <input type="text" name="kpp" value="<?=(!empty($organization['KPP'])) ? $organization['KPP'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">ОКТМО</label>
                    <div class="uk-form-controls">
                        <input type="text" name="oktmo" value="<?=(!empty($organization['OKTMO'])) ? $organization['OKTMO'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Рассчетный счет</label>
                    <div class="uk-form-controls">
                        <input type="text" name="checking_account" value="<?=(!empty($organization['CHECKING_ACCOUNT'])) ? $organization['CHECKING_ACCOUNT'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Корреспондентский счет</label>
                    <div class="uk-form-controls">
                        <input type="text" name="corr_account" value="<?=(!empty($organization['CORR_ACCOUNT'])) ? $organization['CORR_ACCOUNT'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Наименование банка</label>
                    <div class="uk-form-controls">
                        <input type="text" name="bank" value="<?=(!empty($organization['BANK'])) ? htmlentities($organization['BANK']) : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">БИК</label>
                    <div class="uk-form-controls">
                        <input type="text" name="bik" value="<?=(!empty($organization['BIK'])) ? $organization['BIK'] : ""?>">
                    </div>
                </div>
            </div>

            <!-- Администрация -->
            <div class="uk-form uk-width-1-1" data-uk-margin>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Глав.врач / Администратор</label>
                    <div class="uk-form-controls">
                        <input type="text" name="administrator" value="<?=(!empty($organization['ADMINISTRATOR'])) ? $organization['ADMINISTRATOR'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Глав.бухгалтер</label>
                    <div class="uk-form-controls">
                        <input type="text" name="chief_accountant" value="<?=(!empty($organization['CHIEF_ACCOUNTANT'])) ? $organization['CHIEF_ACCOUNTANT'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Генеральный директор</label>
                    <div class="uk-form-controls">
                        <input type="text" name="ceo" value="<?=(!empty($organization['CEO'])) ? $organization['CEO'] : ""?>">
                    </div>
                </div>
            </div>

            <!-- Геолокация -->
            <div class="uk-form uk-width-1-1" data-uk-margin>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-it">Ближайшее метро</label>
                    <div class="uk-form-controls" data-uk-tooltip="{pos:'top-left'}" title='Для множественного выбора необходимо зажать клавишу ctrl и удерживая ее выбрать мышкой необходимые станции'>
                        <select name="metros[]" multiple size="12" style="width: 300px;">
                            <? foreach ($metro_list as $item) { ?>
                                <option value="<?=$item['ID']?>" <? if (!empty($metro) && in_array($item['ID'], array_column($metro, 'METRO_ID'))) { ?> selected <? } ?>><?=$item['NAME']?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Широта</label>
                    <div class="uk-form-controls">
                        <input type="text" name="latitude" value="<?=(!empty($organization['LATITUDE'])) ? $organization['LATITUDE'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Долгота</label>
                    <div class="uk-form-controls">
                        <input type="text" name="longitude" value="<?=(!empty($organization['LONGITUDE'])) ? $organization['LONGITUDE'] : ""?>">
                    </div>
                </div>
            </div>

            <!-- Геолокация -->
            <div class="uk-form uk-width-1-1" data-uk-margin>
                <!-- Юридический -->
                <div class="uk-margin">
                    <h3>Юридический адрес</h3>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Индекс</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[legal][index]" value="<?=(!empty($legal_address['INDEX'])) ? $legal_address['INDEX'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Область / Регион</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[legal][region]" value="<?=(!empty($legal_address['REGION'])) ? $legal_address['REGION'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Город / Село / Деревня</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[legal][city]" value="<?=(!empty($legal_address['CITY'])) ? $legal_address['CITY'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Улица</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[legal][street]" value="<?=(!empty($legal_address['STREET'])) ? $legal_address['STREET'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Здание</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[legal][house]" value="<?=(!empty($legal_address['HOUSE'])) ? $legal_address['HOUSE'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Строение / Корпус</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[legal][building]" value="<?=(!empty($legal_address['BUILDING'])) ? $legal_address['BUILDING'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Офис</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[legal][office]" value="<?=(!empty($legal_address['OFFICE'])) ? $legal_address['OFFICE'] : ""?>">
                        </div>
                    </div>
                </div>

                <!-- Фактический -->
                <div class="uk-margin">
                    <h3>Фактический адрес</h3>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Индекс</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[actual][index]" value="<?=(!empty($actual_address['INDEX'])) ? $actual_address['INDEX'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Область / Регион</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[actual][region]" value="<?=(!empty($actual_address['REGION'])) ? $actual_address['REGION'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Город / Село / Деревня</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[actual][city]" value="<?=(!empty($actual_address['CITY'])) ? $actual_address['CITY'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Улица</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[actual][street]" value="<?=(!empty($actual_address['STREET'])) ? $actual_address['STREET'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Здание</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[actual][house]" value="<?=(!empty($actual_address['HOUSE'])) ? $actual_address['HOUSE'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Строение / Корпус</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[actual][building]" value="<?=(!empty($actual_address['BUILDING'])) ? $actual_address['BUILDING'] : ""?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Офис</label>
                        <div class="uk-form-controls">
                            <input type="text" name="address[actual][office]" value="<?=(!empty($actual_address['OFFICE'])) ? $actual_address['OFFICE'] : ""?>">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Контакты -->
            <div class="uk-from uk-width-1-1" data-uk-margin>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Первый телефон </label>
                    <div class="uk-form-controls">
                        <input id="phone1" type="text" name="phone1" value="<?=(!empty($phone1['NAME'])) ? $phone1['NAME'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Второй телефон </label>
                    <div class="uk-form-controls">
                        <input id="phone2" type="text" name="phone2" value="<?=(!empty($phone2['NAME'])) ? $phone2['NAME'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Третий телефон </label>
                    <div class="uk-form-controls">
                        <input id="phone3" type="text" name="phone3" value="<?=(!empty($phone3['NAME'])) ? $phone3['NAME'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Первый сайт</label>
                    <div class="uk-form-controls">
                        <input type="text" name="website1" value="<?=(!empty($website1['NAME'])) ? $website1['NAME'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Второй сайт</label>
                    <div class="uk-form-controls">
                        <input type="text" name="website2" value="<?=(!empty($website2['NAME'])) ? $website2['NAME'] : ""?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Третий сайт</label>
                    <div class="uk-form-controls">
                        <input type="text" name="website3" value="<?=(!empty($website3['NAME'])) ? $website3['NAME'] : ""?>">
                    </div>
                </div>
            </div>

            <!-- Рабочее время -->
            <div class="uk-from uk-width-1-1" data-uk-margin>
                <div class="uk-margin">
                    <h3>Рабочее время</h3>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Понедельник</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="work[1][from]" value="<?=(!empty($work_time[1]['WORK_FROM'])) ? $work_time[1]['WORK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="work[1][to]" value="<?=(!empty($work_time[1]['WORK_TO'])) ? $work_time[1]['WORK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Вторник</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="work[2][from]" value="<?=(!empty($work_time[2]['WORK_FROM'])) ? $work_time[2]['WORK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="work[2][to]" value="<?=(!empty($work_time[2]['WORK_TO'])) ? $work_time[2]['WORK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Среда</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="work[3][from]" value="<?=(!empty($work_time[3]['WORK_FROM'])) ? $work_time[3]['WORK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="work[3][to]" value="<?=(!empty($work_time[3]['WORK_TO'])) ? $work_time[3]['WORK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Четверг</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="work[4][from]"  value="<?=(!empty($work_time[4]['WORK_FROM'])) ? $work_time[4]['WORK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="work[4][to]"  value="<?=(!empty($work_time[4]['WORK_TO'])) ? $work_time[4]['WORK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Пятница</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="work[5][from]" value="<?=(!empty($work_time[5]['WORK_FROM'])) ? $work_time[5]['WORK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="work[5][to]" value="<?=(!empty($work_time[5]['WORK_TO'])) ? $work_time[5]['WORK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Суббота</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="work[6][from]" value="<?=(!empty($work_time[6]['WORK_FROM'])) ? $work_time[6]['WORK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="work[6][to]" value="<?=(!empty($work_time[6]['WORK_TO'])) ? $work_time[6]['WORK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Воскресенье</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="work[7][from]" value="<?=(!empty($work_time[7]['WORK_FROM'])) ? $work_time[7]['WORK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="work[7][to]" value="<?=(!empty($work_time[7]['WORK_TO'])) ? $work_time[7]['WORK_TO'] : ""?>"></label>
                        </div>
                    </div>
                </div>
                <div class="uk-margin">
                    <h3>Перерыв</h3>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Понедельник</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="break[1][from]" value="<?=(!empty($work_time[1]['BREAK_FROM'])) ? $work_time[1]['BREAK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="break[1][to]" value="<?=(!empty($work_time[1]['BREAK_TO'])) ? $work_time[1]['BREAK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Вторник</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="break[2][from]" value="<?=(!empty($work_time[2]['BREAK_FROM'])) ? $work_time[2]['BREAK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="break[2][to]" value="<?=(!empty($work_time[2]['BREAK_TO'])) ? $work_time[2]['BREAK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Среда</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="break[3][from]" value="<?=(!empty($work_time[3]['BREAK_FROM'])) ? $work_time[3]['BREAK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="break[3][to]" value="<?=(!empty($work_time[3]['BREAK_TO'])) ? $work_time[3]['BREAK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Четверг</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="break[4][from]" value="<?=(!empty($work_time[4]['BREAK_FROM'])) ? $work_time[4]['BREAK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="break[4][to]" value="<?=(!empty($work_time[4]['BREAK_TO'])) ? $work_time[4]['BREAK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Пятница</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="break[5][from]" value="<?=(!empty($work_time[5]['BREAK_FROM'])) ? $work_time[5]['BREAK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="break[5][to]" value="<?=(!empty($work_time[5]['BREAK_TO'])) ? $work_time[5]['BREAK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Суббота </label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="break[6][from]" value="<?=(!empty($work_time[6]['BREAK_FROM'])) ? $work_time[6]['BREAK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="break[6][to]" value="<?=(!empty($work_time[6]['BREAK_TO'])) ? $work_time[6]['BREAK_TO'] : ""?>"></label>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-h-s">Воскресенье</label>
                        <div class="uk-form-controls">
                            <label>начало: <input style="width: 50px;" class='time' type="text" name="break[7][from]" value="<?=(!empty($work_time[7]['BREAK_FROM'])) ? $work_time[7]['BREAK_FROM'] : ""?>"> </label>
                            <label>окончание: <input style="width: 50px;" class='time' type="text" name="break[7][to]" value="<?=(!empty($work_time[7]['BREAK_TO'])) ? $work_time[7]['BREAK_TO'] : ""?>"></label>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Признаки -->
            <div class="uk-from uk-width-1-1" data-uk-margin>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Государственная</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_state" <?=(!empty($evidance['STATE']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Частная</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_private" <?=(!empty($evidance['PRIVATE']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Детское отделение</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_children" <?=(!empty($evidance['CHILDREN']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Скорая</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_ambulance" <?=(!empty($evidance['AMBULANCE']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Выезд на дом</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_house" <?=(!empty($evidance['HOUSE']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Бронирование</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_booking" <?=(!empty($evidance['BOOKING']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">С доставкой</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_delivery" <?=(!empty($evidance['DELIVERY']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Круглосуточно</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_daynight" <?=(!empty($evidance['DAYNIGHT']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">ДМС</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_dms" <?=(!empty($evidance['DMS']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">ДЛО</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_dlo" <?=(!empty($evidance['DLO']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Отдел оптики</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_optics" <?=(!empty($evidance['OPTICS']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">РПО</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_rpo" <?=(!empty($evidance['RPO']) ? "checked" : "")?> value="on">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="form-h-s">Отдел гомеопатии</label>
                    <div class="uk-form-controls">
                        <input type="checkbox" name="f_homeopathy" <?=(!empty($evidance['HOMEOPATHY']) ? "checked" : "")?> value="on">
                    </div>
                </div>
            </div>

            <!-- Контент -->

            <div class="uk-from uk-width-1-1" data-uk-margin>
                <div class="uk-form-row">
                    <a href="/cp/articles_add?OBJECT=1&ID=<?=$id?>"><p>Добавить публикацию</p></a>
                    <a href="/cp/organisation_add_photos/<?=$id?>"><p>Добавить фото</p></a>
                    <a href="/cp/organisation_add_videos/<?=$id?>"><p>Добавить видео</p></a>
                </div>
            </div>
        </div>
    </form>

    <button type="submit" form="form" value="Сохранить" name="submit" class="uk-button uk-button-primary">Сохранить организацию</button>
</div>

<script>
    $('#phone1').mask('+9 (999) 999-99-99',  {placeholder: "+7 999 888 77 66"});
    $('#phone2').mask('+9 (999) 999-99-99',  {placeholder: "+7 999 888 77 66"});
    $('#phone3').mask('+9 (999) 999-99-99',  {placeholder: "+7 999 888 77 66"});

    $('.time').mask('99:99',  {placeholder: "18:00"});
    
    
    $(document).on('change', '.logo', function(){
            //alert('вы загрузили изображение');
            var formData = new FormData($('.logo'));
            /*готовим передаваемые данные - вводим в них файл аватары*/
            formData.append('userfile', $('.logo')[0].files[0],$('.logo')[0].files[0].name);
         console.log(formData.getAll('userfile'));
          var xhr = new XMLHttpRequest();
          xhr.open('POST', '/index/addDocument/images/2048', true);
          xhr.onload = function(e)
            {
                var res = JSON.parse(e.currentTarget.response);
              
                 if(typeof res.success != 'undefined')
                {
                    //alert(pageVariables.controllerMethod);
                    var imgPath='/images/'+res.msg.upload_data.file_name;
                     //alert(imgPath); 
                     $('.uk-thumbnail').attr('src', imgPath);
                      var xhr = new XMLHttpRequest();
                    function some()
                    
                    {
                      xhr.open('POST', '/index/deletephoto', true);
                      xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                      xhr.send('path='+ imgPath);
                  
                    }          
                    setTimeout(some, 5000);
                    
                     
                }
                
            }            
           xhr.send(formData);
           
        });
    
    
</script>