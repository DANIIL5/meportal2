<div class="uk-width-1-1">
    <h1 class="uk-h1">Редактирование голосования</h1>

    <form id="form" enctype="multipart/form-data" method="post" action="/cp/vote_save/<? if (!empty($item['ID'])) { echo $item['ID']; }?>" class="uk-form uk-form-horizontal uk-margin">

        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s">Дата начала голосования</label>
            <div class="uk-form-controls">
                <input type="text" name="start_date" data-uk-datepicker="{format:'YYYY-MM-DD', minDate: '<?=date('Y-m-d')?>'}" value="<?=(!empty($item['START_DATE'])) ? ($item['START_DATE']) : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s">Дата окончания голосования</label>
            <div class="uk-form-controls">
                <input type="text" name="end_date" data-uk-datepicker="{format:'YYYY-MM-DD', minDate: '<?=date('Y-m-d')?>'}" value="<?=(!empty($item['END_DATE'])) ? ($item['END_DATE']) : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s">Изображение</label>
            <div class="uk-form-controls">
                <div class="uk-form-file">
                    <? if (!empty($ite['IMAGE'])) { ?><img class="uk-thumbnail" src="/images/voting/<?=$item['IMAGE']?>" alt="" style="max-width: 240px; max-height: 240px;"><? } ?>
                    <br>
                    <button class="uk-button">Загрузить изображение</button>
                    <input type="file" name="image">
                </div>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s">Вопрос</label>
            <div class="uk-form-controls">
                <input type="text" name="question" value="<?=(!empty($item['QUESTION'])) ? $item['QUESTION'] : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s">Показывать результаты после голосования</label>
            <div class="uk-form-controls">
                <input type="checkbox" name="show_result" value="on" <? if (!empty($item['SHOW_RESULT'])) { ?> checked <? }?>>
            </div>
        </div>

        <hr>

        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s">Варианты ответа</label>
            <div class="uk-form-controls">
                <input type="text" name="option[]" value="<?=(!empty($options[0]['NAME'])) ? $options[0]['NAME'] : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s"></label>
            <div class="uk-form-controls">
                <input type="text" name="option[]" value="<?=(!empty($options[1]['NAME'])) ? $options[1]['NAME'] : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s"></label>
            <div class="uk-form-controls">
                <input type="text" name="option[]" value="<?=(!empty($options[2]['NAME'])) ? $options[2]['NAME'] : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s"></label>
            <div class="uk-form-controls">
                <input type="text" name="option[]" value="<?=(!empty($options[3]['NAME'])) ? $options[3]['NAME'] : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s"></label>
            <div class="uk-form-controls">
                <input type="text" name="option[]" value="<?=(!empty($options[4]['NAME'])) ? $options[4]['NAME'] : ""?>">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label" for="form-h-s"></label>
            <div class="uk-form-controls">
                <button type="submit" value="Сохранить изменения" name="submit" class="uk-button uk-button-primary">Сохранить изменения</button>
            </div>
        </div>
    </form>
</div>