<?php

/**
 * Created by PhpStorm.
 * User: kulakov
 * Date: 22.12.16
 * Time: 17:03
 */
class Voting_model extends CI_Model
{
    public function get_vote()
    {
        $info = $this->db->query('SELECT * FROM VOTING ORDER BY ID DESC LIMIT 1')->row_array();
        if (!empty($info)) {
            $options = $this->db->query('SELECT * FROM VOTING_OPTION WHERE VOTE_ID = '.$info['ID'])->result_array();
            return [
                'info'      => $info,
                'options'   => $options
            ];
        }

        return null;
    }

    public function is_vote($vote_id, $user_id)
    {
        $answer = $this->db->where('USER_ID', $user_id)->where('VOTE_ID', $vote_id)->limit(1)->get('VOTING_RESULT')->row_array();

        if (!empty($answer)) {
            $results = $this->db
                ->select('VOTING_RESULT.OPTION_ID')
                ->select('VOTING_OPTION.NAME')
                ->select('COUNT(OPTION_ID) COUNT')
                ->where('VOTING_RESULT.VOTE_ID', $vote_id)
                ->group_by('VOTING_RESULT.OPTION_ID')
                ->join('VOTING_OPTION','VOTING_OPTION.ID = VOTING_RESULT.OPTION_ID', 'left')
                ->get('VOTING_RESULT')->result_array();

            $count = $this->db->where('VOTE_ID', $vote_id)->count_all_results('VOTING_RESULT');

            $temp = [];

            foreach ($results as $result) {
                $temp[] = '<li>'.round($result['COUNT']/$count*100) .'% - '.$result['NAME'].'</li>';
            }

            $response = [
                'is_vote'   => true,
                'html'      => '<p class="question">Благодарим за ответ!</p>
                                    <ul>
                                        '.(!empty($temp) ? implode('', $temp) : '<li>Нет ответов</li>').'
                                    </ul>'
            ];
        } else {
            $response = [
                'is_vote'   => false
            ];
        }

        return $response;
    }
}