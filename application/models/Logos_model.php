<?php

class Logos_model extends  CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_logos()
    {
//        return $this->db->query('SELECT ID, BRAND, LOGO FROM ORGANIZATIONS')->result_array();
        return $this->db
            ->select('ORGANIZATIONS.ID')
            ->select('ORGANIZATIONS.BRAND')
            ->select('ORGANIZATIONS.LOGO')
            ->select('ORGANIZATION_ADDRESSES.*')
            ->limit(4)
            ->join('ORGANIZATION_ADDRESSES', 'ORGANIZATION_ADDRESSES.ORGANIZATION_ID = ORGANIZATIONS.ID AND ORGANIZATION_ADDRESSES.TYPE_ID = 2', 'left')
            ->where('(ORGANIZATIONS.LOGO IS NOT NULL OR ORGANIZATIONS.LOGO != "") AND (ORGANIZATIONS.POPULAR)')
            ->get('ORGANIZATIONS')
            ->result_array();
    }
}