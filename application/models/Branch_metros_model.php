<?php

/**
 * Created by PhpStorm.
 * User: Артем
 * Date: 25.08.2016
 * Time: 15:05
 */
class Branch_metros_model extends CI_Model
{
    public function get_branch_metro($branch_id)
    {
        $branch_id = (int)$branch_id;
        $result = [];

        if (!empty($branch_id))
            $result = $this->db->query("SELECT * FROM BRANCH_METROS WHERE BRANCH_ID = $branch_id")->result_array();

        return $result;
    }

    public function drop_branch_metro($branch_id)
    {
        $branch_id = (int)$branch_id;

        if (!empty($branch_id))
            $this->db->query("DELETE FROM BRANCH_METROS WHERE BRANCH_ID = $branch_id");
    }

    public function add_metro($branch_id, $metro_id)
    {
        $branch_id = (int)$branch_id;
        $metro_id = (int)$metro_id;

        if (!empty($branch_id) && !empty($metro_id)) {
            $data = [
                'BRANCH_ID'         => $branch_id,
                'METRO_ID'          => $metro_id
            ];

            $this->db->insert("BRANCH_METROS", $data);
        } else {
            throw new Exception("Для создание связи обязательно должны быть переданы идентификаторы организации");
        }
    }

    public function delete_metro($branch_id, $metro_id)
    {

    }

    public function delete_metros($branch_id, $metro_ids)
    {
        $branch_id = (int)$branch_id;

        if (!empty($branch_id) && !empty($metro_ids))
        $in = implode(",",$metro_ids);

        $this->db->query("DELETE FROM BRANCH_METROS WHERE BRANCH_ID = $branch_id AND METRO_ID IN ($in)");
    }
}