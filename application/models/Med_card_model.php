<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Med_card_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_user_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM MED_CARDS WHERE USER_ID =".(int)$user_id)->row_array();
    }
    public function update($fields = [],$id)
    {
        if (!empty($id) and !empty($fields))
            $this->db->update('MED_CARDS', $fields,['ID'=>$id]);
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('MED_CARDS', $fields);
        return $this->db->insert_id();
    }
}