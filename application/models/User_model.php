<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM USERS WHERE ID =".(int)$user_id)->row_array();
    }

    public function get_by_email($email)
    {
        if (!empty($email))
            return $this->db->query("SELECT * FROM USERS WHERE EMAIL = \"".mysqli_real_escape_string($this->db->conn_id,$email)."\"")->row_array();
    }

    public function get_by_phone($phone)
    {
        if (!empty($phone))
            return $this->db->query("SELECT * FROM USERS WHERE PHONE = \"".mysqli_real_escape_string($this->db->conn_id,$phone)."\"")->row_array();
    }

    public function create($fields = array())
    {
        if (!empty($fields['TYPE']) && !empty($fields['EMAIL']) && !empty($fields['PASSWORD']) && $fields['TYPE']) {
            $user_type = $this->db->query("SELECT 1 FROM USER_TYPES WHERE ID = ".(int)$fields['TYPE'])->result_array();
            if(!empty($user_type)) {
                $this->db->insert('USERS', $fields);
                return $this->db->insert_id();
            } else
                throw new Exception('Non-existent user type');
        } else
            throw new Exception('The required fields are not transferred');
    }

    public function update($fields = [],$id)
    {
        if (!empty($id) and !empty($fields))
            $this->db->update('USERS', $fields,['ID'=>$id]);
    }

}