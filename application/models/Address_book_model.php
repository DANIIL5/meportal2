<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address_book_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
/**
 * [выводят списки для адресной книги]
 * @param  [type] $user_id [чья адресная книга]
 * @param  [type] $search  [поисковый запрос]
 * @return [type]          [результат SQL-запроса]
 */
    public function get_by_user_id_users($user_id,$search=false)
    {
        if (!empty($user_id))
        {
            if($search === false)
                $q = " ";
            else
                $q = " AND NAME_USER LIKE '%".$search."%' ";
            return $this->db->query("SELECT * FROM ADDRESS_USERS WHERE USER_ID =".(int)$user_id.$q."ORDER BY NAME_USER")->result_array();
        }
    }
    public function get_by_user_id_specialists($user_id,$search=false)
    {
        if (!empty($user_id))
        {
            if($search === false)
                $q = " ";
            else
                $q = " AND NAME_SPECIALIST LIKE '%".$search."%' ";
            return $this->db->query("SELECT * FROM ADDRESS_SPECIALISTS WHERE USER_ID =".(int)$user_id.$q." ORDER BY NAME_SPECIALIST")->result_array();
        }
    }
    public function get_by_user_id_institutions($user_id,$search=false)
    {
        if (!empty($user_id))
        {
            if($search === false)
                $q = " ";
            else
                $q = " AND NAME_INSTITUTION LIKE '%".$search."%' ";
            return $this->db->query("SELECT * FROM ADDRESS_INSTITUTIONS WHERE USER_ID =".(int)$user_id.$q." ORDER BY NAME_INSTITUTION")->result_array();
        }
    }
    public function get_user_by_user_id($id,$user_id)
    {
        if (!empty($id) and !empty($user_id))
            return $this->db->query("SELECT * FROM ADDRESS_USERS WHERE ID_USER_ADDRESS =".(int)$id." AND USER_ID=".(int)$user_id)->row_array();
    }
    public function get_specialist_by_user_id($id,$user_id)
    {
        if (!empty($id) and !empty($user_id))
            return $this->db->query("SELECT * FROM ADDRESS_SPECIALISTS WHERE ID_SPECIALIST =".(int)$id." AND USER_ID=".(int)$user_id)->row_array();
    }
    public function get_institution_by_user_id($id,$user_id)
    {
        if (!empty($id) and !empty($user_id))
            return $this->db->query("SELECT * FROM ADDRESS_INSTITUTIONS WHERE ID_INSTITUTION =".(int)$id." AND USER_ID=".(int)$user_id)->row_array();
    }
    public function create_user($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('ADDRESS_USERS', $fields);
        return $this->db->insert_id();
    }
    public function create_specialist($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('ADDRESS_SPECIALISTS', $fields);
        return $this->db->insert_id();
    }
    public function create_institution($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('ADDRESS_INSTITUTIONS', $fields);
        return $this->db->insert_id();
    }
}