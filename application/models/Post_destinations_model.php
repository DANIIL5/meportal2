<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_destinations_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('POST_DESTINATIONS', $fields);
		return $this->db->insert_id();
    }
    public function delete($post_id)
    {
        if (!empty($post_id))
            $this->db->delete('POST_DESTINATIONS', ['ID_POST' => $post_id]);
    }
    public function update($fields = [],$id,$user_id)
    {
        if (!empty($id) and !empty($user_id) and !empty($fields))
            $this->db->update('POST_DESTINATIONS', $fields,['ID_POST'=>$id,'ID_DESTINATION'=>$user_id]);
    }
    public function get_by_post_id($post_id)
    {
        if (!empty($post_id))
            return $this->db->query("SELECT * FROM POST_DESTINATIONS WHERE ID_POST =".(int)$post_id)->result_array();
    }
    public function delete_by_post_id_user_id($post_id,$user_id)
    {
        if (!empty($post_id) and !empty($user_id))
            $this->db->delete('POST_DESTINATIONS', ['ID_POST' => $post_id,'ID_DESTINATION'=>$user_id]);
    }
}