<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_files_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_specialist_id($specialist_id)
    {
        if (!empty($specialist_id))
            return $this->db->query("SELECT * FROM SPECIALIST_FILES WHERE SPECIALIST_ID =".(int)$specialist_id)->result_array();
    }
    public function get_file_by_specialist_id($specialist_id)
    {
        if (!empty($specialist_id))
            return $this->db->query("SELECT FILE FROM SPECIALIST_FILES WHERE SPECIALIST_ID =".(int)$specialist_id)->result_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('SPECIALIST_FILES', $fields);
		return $this->db->insert_id();
    }
    public function delete($specialist_id)
    {
        if (!empty($specialist_id))
            $this->db->delete('SPECIALIST_FILES', ['SPECIALIST_ID' => $specialist_id]);
    }
    public function delete_by_file($file)
    {
        if (!empty($file))
            $this->db->delete('SPECIALIST_FILES', ['FILE' => $file]);
    }
}