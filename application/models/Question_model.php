<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function create($fields = array())
    {
        if (!empty($fields))
        {
            $date = date('Y-m-d H:i:s');
            $fields['CREATED_AT'] = $date;
            $fields['UPDATED_AT'] = $date;
            $this->db->insert('QUESTIONS', $fields);
        }
        return $this->db->insert_id();
    }
}