<?php

/**
 * Created by PhpStorm.
 * User: Артем
 * Date: 25.08.2016
 * Time: 15:05
 */
class Institution_departments_model extends CI_Model
{
    public function get_institution_departments($institution_id)
    {
        $institution_id = (int)$institution_id;
        $result = [];

        if (!empty($institution_id))
            $result = $this->db->query("SELECT * FROM INSTITUTION_DEPARTMETS WHERE INSTITUTION_ID = $institution_id")->result_array();

        return $result;
    }

    public function drop_institution_departments($institution_id)
    {
        $institution_id = (int)$institution_id;

        if (!empty($institution_id))
            $this->db->query("DELETE FROM INSTITUTION_DEPARTMETS WHERE INSTITUTION_ID = $institution_id");
    }

    public function add_department($institution_id, $department_id)
    {
        $institution_id = (int)$institution_id;
        $department_id = (int)$department_id;

        if (!empty($institution_id) && !empty($department_id)) {
            $data = [
                'INSTITUTION_ID'         => $institution_id,
                'DEPARTMENT_ID'          => $department_id
            ];

            $this->db->insert("INSTITUTION_DEPARTMETS", $data);
        } else {
            throw new Exception("Для создание связи обязательно должны быть переданы идентификаторы организации");
        }
    }

    public function delete_department($institution_id, $department_id)
    {

    }

    public function delete_departments($institution_id, $department_ids)
    {
        $institution_id = (int)$institution_id;

        if (!empty($institution_id) && !empty($department_ids))
            $in = implode(",", $department_ids);

        $this->db->query("DELETE FROM INSTITUTION_DEPARTMETS WHERE INSTITUTION_ID = $institution_id AND DEPARTMENT_ID IN ($in)");
    }
}