<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soc_net_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_id($id)
    {
        if (!empty($id))
            return $this->db->query("SELECT * FROM SOC_NETS WHERE ID =".(int)$id)->row_array();
    }
    public function get()
    {
       return $this->db->query("SELECT * FROM SOC_NETS")->result_array();
    }
    public function get_by_user_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM USER_SOC_NETS WHERE USER_ID =".(int)$user_id)->result_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('USER_SOC_NETS', $fields);
        return $this->db->insert_id();
    }
    public function delete($user_id,$soc_net_id)
    {
        if (!empty($user_id) and !empty($soc_net_id))
            $this->db->delete('USER_SOC_NETS', ['USER_ID' => $user_id,'SOC_NET_ID' => $soc_net_id]);
    }
}