<?php

/**
 * Created by PhpStorm.
 * User: Артем
 * Date: 25.08.2016
 * Time: 15:05
 */
class Branch_departments_model extends CI_Model
{
    public function get_branch_departments($branch_id)
    {
        $branch_id = (int)$branch_id;
        $result = [];

        if (!empty($branch_id))
            $result = $this->db->query("SELECT * FROM BRANCHES_DEPARTMETS WHERE BRANCHES_ID = $branch_id")->result_array();

        return $result;
    }

    public function drop_branch_departments($branch_id)
    {
        $branch_id = (int)$branch_id;

        if (!empty($branch_id))
            $this->db->query("DELETE FROM BRANCHES_DEPARTMETS WHERE BRANCHES_ID = $branch_id");
    }

    public function add_department($branch_id, $department_id)
    {
        $branch_id = (int)$branch_id;
        $department_id = (int)$department_id;

        if (!empty($branch_id) && !empty($department_id)) {
            $data = [
                'BRANCHES_ID'         => $branch_id,
                'DEPARTMENT_ID'          => $department_id
            ];

            $this->db->insert("BRANCHES_DEPARTMETS", $data);
        } else {
            throw new Exception("Для создание связи обязательно должны быть переданы идентификаторы отделения");
        }
    }

    public function delete_department($branch_id, $department_id)
    {

    }

    public function delete_departments($branch_id, $department_ids)
    {
        $branch_id = (int)$branch_id;

        if (!empty($branch_id) && !empty($department_ids))
            $in = implode(",", $department_ids);

        $this->db->query("DELETE FROM BRANCHES_DEPARTMETS WHERE BRANCHES_ID = $branch_id AND DEPARTMENT_ID IN ($in)");
    }
}