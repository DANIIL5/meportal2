<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Specialist_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_user_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM SPECIALISTS WHERE USER_ID =".(int)$user_id)->row_array();
    }
    public function update($fields = [], $user_id)
    {
        $this->db->update('SPECIALISTS', $fields, ['USER_ID'=>$user_id]);
    }

    public function get_by_institution_id($id)
    {
        return $this->db->query("SELECT 
            SPECIALISTS.*,
            DOCTOR_TYPES.NAME LINKED_SPECIALIZATION
            FROM SPECIALISTS 
            LEFT JOIN DOCTOR_TYPES ON DOCTOR_TYPES.ID = SPECIALISTS.SPECIALIZATION_ID
            WHERE BRANCH_ID IN (SELECT ID FROM BRANCHES WHERE INSTITUTION_ID = $id)
            LIMIT 5")->result_array();
    }

    public function get_by_branch_id($id)
    {
        return $this->db->query("SELECT
            SPECIALISTS.*,
            DOCTOR_TYPES.NAME LINKED_SPECIALIZATION
            FROM SPECIALISTS 
            LEFT JOIN DOCTOR_TYPES ON DOCTOR_TYPES.ID = SPECIALISTS.SPECIALIZATION_ID
            WHERE BRANCH_ID = $id
            LIMIT 5")->result_array();
    }

    public function get_by_id($id)
    {
        return $this->db->query("SELECT
            SPECIALISTS.*,
            USERS.EMAIL WORK_EMAIL,
            DOCTOR_TYPES.NAME LINKED_SPECIALIZATION
            FROM SPECIALISTS 
            LEFT JOIN DOCTOR_TYPES ON DOCTOR_TYPES.ID = SPECIALISTS.SPECIALIZATION_ID
            LEFT JOIN USERS ON USERS.ID = SPECIALISTS.USER_ID
            WHERE SPECIALISTS.ID = $id
            LIMIT 1")->row_array();
    }
}