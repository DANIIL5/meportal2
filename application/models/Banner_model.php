<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends  CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->query("SELECT * FROM BANNERS")->result_array();
    }

    public function get_banners($category_id)
    {
        return $this->db->query("SELECT * FROM BANNERS WHERE CATEGORY_ID = $category_id ORDER BY RAND() LIMIT 4")->result_array();
    }
}