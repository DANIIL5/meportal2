<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institution_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_catalog($category = null, $type = null, $per_page = 10, $current_page = 0, $where = null, $order = "ID")
    {
        $where_array = [];
        $where_string = "";
        $order = "ORDER BY $order";
        $limit = "";
        $offset = "";

        if (!empty($category))
            $where_array[] = "(INSTITUTIONS.CATEGORY_ID = $category)";
        if (!empty($type))
            $where_array[] = "(INSTITUTIONS.TYPE_ID = $type)";
        if (!empty($where))
            $where = " AND ".$where;
        if (!empty($per_page))
            $limit = "LIMIT $per_page";
        if (!empty($current_page))
            $offset = "OFFSET $current_page";

        if (!empty($where_array))
            $where_string = "WHERE ".implode(' AND ',$where_array).$where;
        elseif (!empty($where))
            $where_string = "WHERE $where";

        return $this->db->query("SELECT
          BRANCHES.ID,
          INSTITUTIONS.NAME,
          INSTITUTIONS.SHORTNAME,
          INSTITUTIONS.DESCRIPTION,
          INSTITUTIONS.LOGO,
          BRANCHES.ADDRESS,
          METRO.NAME METRO,
          BRANCHES.PHONES,
          BRANCHES.WEBSITES,
          BRANCHES.LATITUDE,
          BRANCHES.SCHEDULE,
          BRANCHES.LONGITUDE
          FROM BRANCHES
          LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
          LEFT JOIN METRO ON METRO.ID = BRANCHES.METRO_ID
          $where_string
          $order
          $limit
          $offset")->result_array();exit();
    }

    public function get_item($id)
    {
        return $this->db->query("SELECT
          BRANCHES.ID,
          INSTITUTION_TYPES.NAME TYPE,
          CATEGORY.NAME CATEGORY,
          INSTITUTIONS.NAME,
          INSTITUTIONS.SHORTNAME,
          INSTITUTIONS.DESCRIPTION,
          INSTITUTIONS.LOGO,
          BRANCHES.ADDRESS,
          METRO.NAME METRO,
          BRANCHES.PHONES,
          BRANCHES.WEBSITES,
          BRANCHES.LATITUDE,
          BRANCHES.SCHEDULE,
          BRANCHES.LONGITUDE
          FROM BRANCHES
          LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
          LEFT JOIN METRO ON METRO.ID = BRANCHES.METRO_ID
          LEFT JOIN CATEGORY ON CATEGORY.ID = INSTITUTIONS.CATEGORY_ID
          LEFT JOIN INSTITUTION_TYPES ON INSTITUTION_TYPES.ID = INSTITUTIONS.TYPE_ID
          WHERE BRANCHES.ID = $id
          LIMIT 1")->row_array();
    }

    public function get_drugstore($id)
    {
        return $this->db->query("SELECT
          BRANCHES.ID,
          INSTITUTION_TYPES.NAME TYPE,
          CATEGORY.NAME CATEGORY,
          INSTITUTIONS.NAME,
          INSTITUTIONS.SHORTNAME,
          INSTITUTIONS.DESCRIPTION,
          INSTITUTIONS.LOGO,
          BRANCHES.ADDRESS,
          METRO.NAME METRO,
          BRANCHES.PHONES,
          BRANCHES.WEBSITES,
          BRANCHES.LATITUDE,
          BRANCHES.SCHEDULE,
          BRANCHES.LONGITUDE
          FROM BRANCHES
          LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
          LEFT JOIN METRO ON METRO.ID = BRANCHES.METRO_ID
          LEFT JOIN CATEGORY ON CATEGORY.ID = INSTITUTIONS.CATEGORY_ID
          LEFT JOIN INSTITUTION_TYPES ON INSTITUTION_TYPES.ID = INSTITUTIONS.TYPE_ID
          WHERE BRANCHES.ID = $id
          AND INSTITUTIONS.CATEGORY_ID = 4
          LIMIT 1")->row_array();
    }

    public function get_catalog_quantity($category = null, $type = null, $where = null)
    {
        $where_array = [];
        $where_string = "";

        if (!empty($category))
            $where_array[] = "(INSTITUTIONS.CATEGORY_ID = $category)";
        if (!empty($type))
            $where_array[] = "(INSTITUTIONS.TYPE_ID = $type)";

        if (!empty($where_array))
            $where_string = "WHERE ".implode(' AND ',$where_array).$where;

        return $this->db->query("SELECT
          COUNT(0) `COUNT`
          FROM BRANCHES
          LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
          LEFT JOIN METRO ON METRO.ID = BRANCHES.METRO_ID
          $where_string")->row_array()["COUNT"];
    }

    public function get_popular()
    {
        return $this->db
            ->select('ORGANIZATIONS.*')
            ->select('ORGANIZATION_ADDRESSES.*')
            ->where('ORGANIZATIONS.POPULAR', 1)
            ->where('ORGANIZATIONS.LOGO IS NOT NULL')
            ->join('ORGANIZATION_ADDRESSES', 'ORGANIZATION_ADDRESSES.ORGANIZATION_ID = ORGANIZATIONS.ID AND ORGANIZATION_ADDRESSES.TYPE_ID = 2')
            ->get('ORGANIZATIONS')->result_array();
    }

    public function get_types()
    {
        return $this->db->query("SELECT ID,NAME,ICON FROM INSTITUTION_TYPES WHERE ON_MAP = 1 ORDER BY NAME")->result_array();
    }
    public function get_category_name_by_id($id)
    {
        if (!empty($id))
            return $this->db->query("SELECT NAME FROM CATEGORY WHERE ID =".(int)$id)->row_array();
    }
    public function get_id_brand()
    {
        return $this->db->query("SELECT ID,SHORTNAME FROM INSTITUTIONS")->result_array();
    }
    public function get_type_name_by_id($id)
    {
        if (!empty($id))
            return $this->db->query("SELECT NAME FROM INSTITUTION_TYPES WHERE ID =".(int)$id)->row_array();
    }
    public function get_by_user_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM INSTITUTIONS WHERE USER_ID =".(int)$user_id)->row_array();
    }
    public function update($fields = [],$user_id)
    {
        if (!empty($user_id) and !empty($fields))
            $this->db->update('ORGANIZATIONS', $fields,['USER_ID'=>$user_id]);
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('INSTITUTIONS', $fields);
        return $this->db->insert_id();
    }
    public function get_by_email($email)
    {
        if (!empty($email))
            return $this->db->query("SELECT * FROM INSTITUTIONS WHERE EMAIL =\"".mysqli_real_escape_string($this->db->conn_id,$email)."\"")->row_array();
    }
    public function get_by_id($id)
    {
        if (!empty($id))
            return $this->db->query("SELECT * FROM INSTITUTIONS WHERE ID =".(int)$id)->row_array();
    }



    /*
     * Новый блок - опять изменились организации, чтобы не ломать всю структуру, сломаю только часть :)
     */

    public function new_get_catalog($category = null, $type = null, $per_page = 10, $current_page = 0, $where_array = [], $specialization_id = null, $order = "ID")
    {
        //TODO присвоить учреждения какому-то пользователю

        $where_string = "";
        $order = "ORDER BY $order";
        $limit = "";
        $offset = "";
        $spec = "";

        if (!empty($category))
            $where_array[] = "(i.CATEGORY_ID = $category)";
        if (!empty($type))
            $where_array[] = "(i.TYPE_ID = $type)";
        if (!empty($per_page))
            $limit = "LIMIT $per_page";
        if (!empty($current_page))
            $offset = "OFFSET $current_page";

        if (!empty($where_array))
            $where_string = "WHERE ".implode(' AND ',$where_array);

//        print_r($where_string);

//        return $this->db->query("SELECT
//          i.*,
//          GROUP_CONCAT(m.NAME ORDER BY m.NAME ASC SEPARATOR ', ') METROS
//          FROM INSTITUTIONS i
//          LEFT JOIN INSTITUTION_METROS im ON im.INSTITUTION_ID = i.ID
//          LEFT JOIN METRO m ON m.ID = im.METRO_ID
//          $where_string
//          GROUP BY i.ID
//          $order
//          $limit
//          $offset")->result_array();

        if ((!empty($specialization_id)))
            $spec = "AND ins.SPECIALIZATION_ID = $specialization_id";

        if (!empty($spec)) {
            $query = "SELECT 
            `i`.*,
            GROUP_CONCAT(m.NAME ORDER BY m.NAME ASC SEPARATOR ', ') METROS,
            GROUP_CONCAT(sp.NAME ORDER BY sp.NAME ASC SEPARATOR ', ') DOCTOR_TYPES
            FROM
            INSTITUTIONS i
            LEFT JOIN INSTITUTION_METROS im ON im.INSTITUTION_ID = i.ID
            LEFT JOIN METRO m ON m.ID = im.METRO_ID
            LEFT JOIN INSTITUTION_SPECIALIZATIONS ins on ins.INSTITUTION_ID = i.ID
            JOIN SPECIALIZATIONS sp on sp.ID = ins.SPECIALIZATION_ID ". $spec . "
            $where_string
            GROUP BY i.ID
            $order
            $limit
            $offset";
        } else {
            $query = "SELECT 
            `i`.*,
            GROUP_CONCAT(m.NAME ORDER BY m.NAME ASC SEPARATOR ', ') METROS
            FROM
            INSTITUTIONS i
            LEFT JOIN INSTITUTION_METROS im ON im.INSTITUTION_ID = i.ID
            LEFT JOIN METRO m ON m.ID = im.METRO_ID
            LEFT JOIN INSTITUTION_SPECIALIZATIONS ins on ins.INSTITUTION_ID = i.ID
            $where_string
            GROUP BY i.ID
            $order
            $limit
            $offset";
        }

        return $this->db->query($query)->result_array();
    }

    public function new_get_item($id)
    {
        return $this->db->query("SELECT 
          INSTITUTIONS.*,
          USERS.EMAIL WORK_EMAIL
          FROM INSTITUTIONS
          LEFT JOIN USERS ON USERS.ID = INSTITUTIONS.USER_ID
          WHERE INSTITUTIONS.ID = $id LIMIT 1")->row_array();
    }
}