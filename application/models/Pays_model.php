<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pays_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_price_by_pay_id($pay_id)
    {
        if (!empty($pay_id))
            return $this->db->query("SELECT * FROM PAY_PRICES WHERE PAY_ID =".(int)$pay_id)->result_array();
    }
    public function get_all()
    {
        return $this->db->query("SELECT * FROM PAYS")->result_array();
    }
}