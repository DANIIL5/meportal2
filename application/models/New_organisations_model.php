<?php

/**
 * Created by PhpStorm.
 * User: РђСЂС‚РµРј
 * Date: 16.11.2016
 * Time: 20:00
 */
class New_organisations_model extends CI_Model
{
    
    public function get_organisations_user () {
        
        
        
        
        
        
    }
    
    
    
    
    
    public function get_list($filter = [], $many_wheres = [], $limit = 10, $offset = 0, $get_all = false, $where_id = [])
    {
        $result = [];
        $where = [];

        if (!$get_all) {
            $where = [
                'o.ACTIVE'    => 1,
                'o.MODERATED' => 1,
                'o.PUBLIC'    => 1,
            ];
        }

        foreach ($filter as $key => $item) {
            $where[$key] = $item;
        }

        $this->db
            ->select('
                o.*,
                o_evidance.STATE F_STATE,
                o_evidance.PRIVATE F_PRIVATE,
                o_evidance.CHILDREN F_CHILDREN,
                o_evidance.AMBULANCE F_AMBULANCE,
                o_evidance.HOUSE F_HOUSE,
                o_evidance.BOOKING F_BOOKING,
                o_evidance.DELIVERY F_DELIVERY,
                o_evidance.DAYNIGHT F_DAYNIGHT,
                o_evidance.DMS F_DMS,
                o_evidance.DLO F_DLO,
                o_evidance.OPTICS F_OPTICS,
                o_evidance.RPO F_RPO,
                o_evidance.HOMEOPATHY F_HOMEOPATHY,
                cat.NAME CATEGORY,
                o_type.NAME TYPE,
                u.EMAIL WORK_EMAIL
            ')
            ->where($where)
            ->from('ORGANIZATIONS o')
            ->join('INSTITUTION_TYPES o_type', 'o_type.ID = o.TYPE_ID', 'left')
            ->join('ORGANIZATION_EVIDANCE o_evidance', 'o_evidance.ORGANIZATION_ID = o.ID', 'left')
            ->join('CATEGORY cat', 'cat.ID = o.CATEGORY_ID', 'left')
            ->join('USERS u', 'u.ID = o.USER_ID', 'left')
//            ->join('ORGANIZATION_CONTACTS o_c', 'o_c.ORGANIZATION_ID = o.ID AND o_c.TYPE_ID = 2 AND o_c.NAME = "+79998330010"', 'inner')
//            ->group_by('o.ID')
            ->limit($limit)
            ->offset($offset)
            ->order_by('o.BRAND');

        if (!empty($where_id))
            $this->db->where_in('o.ID', $where_id);

        $organizations = $this->db
            ->get()
            ->result_array();

        $ids = $this->db
            ->select('ID')
            ->get("ORGANIZATIONS")
            ->result_array();
        $ids = array_column($ids, 'ID'); 
        //искуственно исправил ошибку,при выборке,надо передлывать
         if(empty($ids)) {
           $ids[0]=0;  
         }
         
        $temp_contacts = $this->db
            ->where_in("ORGANIZATION_ID", $ids)
            ->get('ORGANIZATION_CONTACTS')
            ->result_array();

        $temp_addresses = $this->db
            ->where_in("ORGANIZATION_ID", $ids)
            ->get('ORGANIZATION_ADDRESSES')
            ->result_array();

        $temp_metros = $this->db->select('
                om.ORGANIZATION_ID,
                m.ID, 
                m.NAME
            ')
            ->from('ORGANIZATION_METRO om')
            ->join('METRO m', 'm.ID = om.METRO_ID', 'left')
            ->where_in("om.ORGANIZATION_ID", $ids)
            ->get()
            ->result_array();

        $temp_work_time = $this->db
            ->where_in("ORGANIZATION_ID", $ids)
            ->get('ORGANIZATION_WORKING_TIME')
            ->result_array();

        $contacts = [];
        foreach ($temp_contacts as $row) {
            if ($row['TYPE_ID'] == 2) {
                $contact_type   = 'phones';
            } elseif ($row['TYPE_ID'] == 3) {
                $contact_type   = 'websites';
            }

            if (!empty($contact_type) && !empty($row['NAME']))
                $contacts[(int)$row['ORGANIZATION_ID']][$contact_type][] = $row['NAME'];
        }

        $addresses = [];
        foreach ($temp_addresses as $row) {
            if ($row['TYPE_ID'] == 1) {
                $contact_type   = 'legal';
            } elseif ($row['TYPE_ID'] == 2) {
                $contact_type   = 'actual';
            }

            if (!empty($contact_type) &&
                (!empty($row['INDEX'])
                    || !empty($row['REGION'])
                    || !empty($row['CITY'])
                    || !empty($row['STREET'])
                    || !empty($row['HOUSE'])
                    || !empty($row['BUILDING'])
                    || !empty($row['OFFICE'])
                )) {
                if (!empty($row['INDEX']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['INDEX'] = $row['INDEX'];
                if (!empty($row['REGION']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['REGION'] = $row['REGION'];
                if (!empty($row['CITY']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['CITY'] = $row['CITY'];
                if (!empty($row['STREET']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['STREET'] = $row['STREET'];
                if (!empty($row['HOUSE']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['HOUSE'] = $row['HOUSE'];
                if (!empty($row['BUILDING']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['BUILDING'] = $row['BUILDING'];
                if (!empty($row['OFFICE']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['OFFICE'] = $row['OFFICE'];
            }
        }

        $metros = [];
        foreach ($temp_metros as $row) {
            $metros[(int)$row['ORGANIZATION_ID']][] = [
                'ID'    => $row['ID'],
                'NAME'  => $row['NAME']
            ];
        }

        $working_time = [];
        foreach ($temp_work_time as $row) {
            $working_time[(int)$row['ORGANIZATION_ID']][(int)$row['DAY_ID']] = [
                'WORK_FROM'     => !empty($row['WORK_FROM']) ? date('H:i', strtotime($row['WORK_FROM'])) : null,
                'WORK_TO'       => !empty($row['WORK_TO']) ? date('H:i', strtotime($row['WORK_TO'])) : null,
                'BREAK_FROM'    => !empty($row['BREAK_FROM']) ? date('H:i', strtotime($row['BREAK_FROM'])) : null,
                'BREAK_TO'      => !empty($row['BREAK_TO']) ? date('H:i', strtotime($row['BREAK_TO'])) : null
            ];
        }

        foreach ($organizations as $row) {
            $info = [
                'get_contacts'  => !empty($contacts[$row['ID']]) ? $contacts[$row['ID']] : [],
                'get_addresses' => !empty($addresses[$row['ID']]) ? $addresses[$row['ID']] : [],
                'get_metros'    => !empty($metros[$row['ID']]) ? $metros[$row['ID']] : [],
                'get_working_time'  => !empty($working_time[$row['ID']]) ? $working_time[$row['ID']] : [],
            ];
            $result_array = array_merge($row, $info);
            $result[] = $result_array;
        }

        return $result;
    }

    public function get_count($filter = [], $many_wheres = [], $get_all = false)
    {
        $result = [];
        $where = [];

        if (!$get_all) {
            $where = [
                'o.ACTIVE'    => 1,
                'o.MODERATED' => 1,
                'o.PUBLIC'    => 1,
            ];
        }

        foreach ($filter as $key => $item) {
            $where[$key] = $item;
        }

        $count = $this->db
            ->select('
                count(0) COUNT
            ')
            ->where($where)
            ->from('ORGANIZATIONS o')
            ->join('INSTITUTION_TYPES o_type', 'o_type.ID = o.TYPE_ID', 'left')
            ->join('ORGANIZATION_EVIDANCE o_evidance', 'o_evidance.ORGANIZATION_ID = o.ID', 'left')
            ->join('CATEGORY cat', 'cat.ID = o.CATEGORY_ID', 'left')
            ->join('USERS u', 'u.ID = o.USER_ID', 'left')
//            ->join('ORGANIZATION_CONTACTS o_c', 'o_c.ORGANIZATION_ID = o.ID AND o_c.TYPE_ID = 2 AND o_c.NAME = "+79998330010"', 'inner')
            ->get()
            ->row_array();

        return $count['COUNT'];
    }

    public function get_item($filter, $get_all = false)
    {
        $result = [];
        $where = [];

        if (!$get_all) {
            $where = [
                'o.ACTIVE'    => 1,
                'o.MODERATED' => 1,
                'o.PUBLIC'    => 1,
            ];
        }

        foreach ($filter as $key => $item) {
            $where[$key] = $item;
        }

        $organization = $this->db
            ->select('
                o.*, 
                o_evidance.STATE F_STATE,
                o_evidance.PRIVATE F_PRIVATE,
                o_evidance.CHILDREN F_CHILDREN,
                o_evidance.AMBULANCE F_AMBULANCE,
                o_evidance.HOUSE F_HOUSE,
                o_evidance.BOOKING F_BOOKING,
                o_evidance.DELIVERY F_DELIVERY,
                o_evidance.DAYNIGHT F_DAYNIGHT,
                o_evidance.DMS F_DMS,
                o_evidance.DLO F_DLO,
                o_evidance.OPTICS F_OPTICS,
                o_evidance.RPO F_RPO,
                o_evidance.HOMEOPATHY F_HOMEOPATHY,
                cat.NAME CATEGORY,
                o_type.NAME TYPE,
                u.EMAIL WORK_EMAIL
            ')
            ->where($where)
            ->from('ORGANIZATIONS o')
            ->join('INSTITUTION_TYPES o_type', 'o_type.ID = o.TYPE_ID', 'left')
            ->join('ORGANIZATION_EVIDANCE o_evidance', 'o_evidance.ORGANIZATION_ID = o.ID', 'left')
            ->join('CATEGORY cat', 'cat.ID = o.CATEGORY_ID', 'left')
            ->join('USERS u', 'u.ID = o.USER_ID', 'left')
//            ->join('ORGANIZATION_CONTACTS o_c', 'o_c.ORGANIZATION_ID = o.ID AND o_c.TYPE_ID = 2 AND o_c.NAME = "+79998330010"', 'inner')
            ->group_by('o.ID')
            ->limit(1)
            ->get()
            ->row_array();

        if (!empty($organization)) {
            $temp_contacts = $this->db
                ->where("ORGANIZATION_ID", $organization['ID'])
                ->get('ORGANIZATION_CONTACTS')
                ->result_array();

            $temp_addresses = $this->db
                ->where("ORGANIZATION_ID", $organization['ID'])
                ->get('ORGANIZATION_ADDRESSES')
                ->result_array();

            $temp_metros = $this->db->select('
                m.ID, 
                m.NAME
            ')
                ->from('ORGANIZATION_METRO om')
                ->join('METRO m', 'm.ID = om.METRO_ID', 'left')
                ->where("om.ORGANIZATION_ID", $organization['ID'])
                ->get()
                ->result_array();

            $temp_work_time = $this->db
                ->where("ORGANIZATION_ID", $organization['ID'])
                ->get('ORGANIZATION_WORKING_TIME')
                ->result_array();

            $contacts = [];
            foreach ($temp_contacts as $row) {
                if ($row['TYPE_ID'] == 2) {
                    $contact_type   = 'phones';
                } elseif ($row['TYPE_ID'] == 3) {
                    $contact_type   = 'websites';
                }

                if (!empty($contact_type))
                    $contacts[(int)$row['ORGANIZATION_ID']][$contact_type][] = $row['NAME'];
            }

            $addresses = [];
            foreach ($temp_addresses as $row) {
                if ($row['TYPE_ID'] == 1) {
                    $contact_type   = 'legal';
                } elseif ($row['TYPE_ID'] == 2) {
                    $contact_type   = 'actual';
                }

                if (!empty($contact_type) &&
                    (!empty($row['INDEX'])
                        || !empty($row['REGION'])
                        || !empty($row['CITY'])
                        || !empty($row['STREET'])
                        || !empty($row['HOUSE'])
                        || !empty($row['BUILDING'])
                        || !empty($row['OFFICE'])
                    )) {
                    if (!empty($row['INDEX']))
                        $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['INDEX'] = $row['INDEX'];
                    if (!empty($row['REGION']))
                        $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['REGION'] = $row['REGION'];
                    if (!empty($row['CITY']))
                        $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['CITY'] = $row['CITY'];
                    if (!empty($row['STREET']))
                        $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['STREET'] = $row['STREET'];
                    if (!empty($row['HOUSE']))
                        $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['HOUSE'] = $row['HOUSE'];
                    if (!empty($row['BUILDING']))
                        $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['BUILDING'] = $row['BUILDING'];
                    if (!empty($row['OFFICE']))
                        $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['OFFICE'] = $row['OFFICE'];
                }
            }

            $metros = [];

            foreach ($temp_metros as $row) {
                $metros[$organization['ID']][] = [
                    'ID'    => $row['ID'],
                    'NAME'  => $row['NAME']
                ];
            }

            //TODO РјРµС‚СЂРѕ РЅРµ РІС‹РІРѕРґРёС‚СЃСЏ. Р”РѕРґРµР»Р°С‚СЊ

            $working_time = [];
            foreach ($temp_work_time as $row) {
                $working_time[(int)$row['ORGANIZATION_ID']][(int)$row['DAY_ID']] = [
                    'WORK_FROM'     => !empty($row['WORK_FROM']) ? date('H:i', strtotime($row['WORK_FROM'])) : null,
                    'WORK_TO'       => !empty($row['WORK_TO']) ? date('H:i', strtotime($row['WORK_TO'])) : null,
                    'BREAK_FROM'    => !empty($row['BREAK_FROM']) ? date('H:i', strtotime($row['BREAK_FROM'])) : null,
                    'BREAK_TO'      => !empty($row['BREAK_TO']) ? date('H:i', strtotime($row['BREAK_TO'])) : null
                ];
            }

            $info = [
                'get_contacts'  => !empty($contacts[$organization['ID']]) ? $contacts[$organization['ID']] : null,
                'get_addresses' => !empty($addresses[$organization['ID']]) ? $addresses[$organization['ID']] : null,
                'get_metros'    => !empty($metros[$organization['ID']]) ? $metros[$organization['ID']] : null,
                'get_working_time'  => !empty($working_time[$organization['ID']]) ? $working_time[$organization['ID']] : null
            ];

            $result = array_merge($organization, $info);
        }

        return $result;
    }

    public function get_branches($id, $hidden = false, $limit = 20, $offset = 0)
    {
        if (empty($id))
            show_404();

        $result = [];

        if (!$hidden)
            $where = [
                'o.ACTIVE'    => 1,
                'o.MODERATED' => 1,
                'o.PUBLIC'    => 1,
            ];

        $where['o.PARENT_ID'] = $id;

        $organizations = $this->db
            ->select('
                o.*,
                o_evidance.STATE F_STATE,
                o_evidance.PRIVATE F_PRIVATE,
                o_evidance.CHILDREN F_CHILDREN,
                o_evidance.AMBULANCE F_AMBULANCE,
                o_evidance.HOUSE F_HOUSE,
                o_evidance.BOOKING F_BOOKING,
                o_evidance.DELIVERY F_DELIVERY,
                o_evidance.DAYNIGHT F_DAYNIGHT,
                o_evidance.DMS F_DMS,
                o_evidance.DLO F_DLO,
                o_evidance.OPTICS F_OPTICS,
                o_evidance.RPO F_RPO,
                o_evidance.HOMEOPATHY F_HOMEOPATHY,
                cat.NAME CATEGORY,
                o_type.NAME TYPE,
                u.EMAIL WORK_EMAIL
            ')
            ->where($where)
            ->from('ORGANIZATIONS o')
            ->join('INSTITUTION_TYPES o_type', 'o_type.ID = o.TYPE_ID', 'left')
            ->join('ORGANIZATION_EVIDANCE o_evidance', 'o_evidance.ORGANIZATION_ID = o.ID', 'left')
            ->join('CATEGORY cat', 'cat.ID = o.CATEGORY_ID', 'left')
            ->join('USERS u', 'u.ID = o.USER_ID', 'left')
//            ->join('ORGANIZATION_CONTACTS o_c', 'o_c.ORGANIZATION_ID = o.ID AND o_c.TYPE_ID = 2 AND o_c.NAME = "+79998330010"', 'inner')
            ->group_by('o.ID')
            ->limit($limit)
            ->offset($offset)
            ->get()
            ->result_array();

        $ids = $this->db
            ->select('ID')
            ->get("ORGANIZATIONS")
            ->result_array();
        $ids = array_column($ids, 'ID');

        $temp_contacts = $this->db
            ->where_in("ORGANIZATION_ID", $ids)
            ->get('ORGANIZATION_CONTACTS')
            ->result_array();

        $temp_addresses = $this->db
            ->where_in("ORGANIZATION_ID", $ids)
            ->get('ORGANIZATION_ADDRESSES')
            ->result_array();

        $temp_metros = $this->db->select('
                om.ORGANIZATION_ID,
                m.ID, 
                m.NAME
            ')
            ->from('ORGANIZATION_METRO om')
            ->join('METRO m', 'm.ID = om.METRO_ID', 'left')
            ->where_in("om.ORGANIZATION_ID", $ids)
            ->get()
            ->result_array();

        $temp_work_time = $this->db
            ->where_in("ORGANIZATION_ID", $ids)
            ->get('ORGANIZATION_WORKING_TIME')
            ->result_array();

        $contacts = [];
        foreach ($temp_contacts as $row) {
            if ($row['TYPE_ID'] == 2) {
                $contact_type   = 'phones';
            } elseif ($row['TYPE_ID'] == 3) {
                $contact_type   = 'websites';
            }

            if (!empty($contact_type))
                $contacts[(int)$row['ORGANIZATION_ID']][$contact_type][] = $row['NAME'];
        }

        $addresses = [];
        foreach ($temp_addresses as $row) {
            if ($row['TYPE_ID'] == 1) {
                $contact_type   = 'legal';
            } elseif ($row['TYPE_ID'] == 2) {
                $contact_type   = 'actual';
            }

            if (!empty($contact_type) &&
                (!empty($row['INDEX'])
                    || !empty($row['REGION'])
                    || !empty($row['CITY'])
                    || !empty($row['STREET'])
                    || !empty($row['HOUSE'])
                    || !empty($row['BUILDING'])
                    || !empty($row['OFFICE'])
                )) {
                if (!empty($row['INDEX']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['INDEX'] = $row['INDEX'];
                if (!empty($row['REGION']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['REGION'] = $row['REGION'];
                if (!empty($row['CITY']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['CITY'] = $row['CITY'];
                if (!empty($row['STREET']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['STREET'] = $row['STREET'];
                if (!empty($row['HOUSE']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['HOUSE'] = $row['HOUSE'];
                if (!empty($row['BUILDING']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['BUILDING'] = $row['BUILDING'];
                if (!empty($row['OFFICE']))
                    $addresses[(int)$row['ORGANIZATION_ID']][$contact_type]['OFFICE'] = $row['OFFICE'];
            }
        }

        $metros = [];
        foreach ($temp_metros as $row) {
            $metros[(int)$row['ORGANIZATION_ID']][] = [
                'ID'    => $row['ID'],
                'NAME'  => $row['NAME']
            ];
        }

        $working_time = [];
        foreach ($temp_work_time as $row) {
            $working_time[(int)$row['ORGANIZATION_ID']][(int)$row['DAY_ID']] = [
                'WORK_FROM'     => !empty($row['WORK_FROM']) ? date('H:i', strtotime($row['WORK_FROM'])) : null,
                'WORK_TO'       => !empty($row['WORK_TO']) ? date('H:i', strtotime($row['WORK_TO'])) : null,
                'BREAK_FROM'    => !empty($row['BREAK_FROM']) ? date('H:i', strtotime($row['BREAK_FROM'])) : null,
                'BREAK_TO'      => !empty($row['BREAK_TO']) ? date('H:i', strtotime($row['BREAK_TO'])) : null
            ];
        }

        foreach ($organizations as $row) {
            $info = [
                'get_contacts'  => !empty($contacts[$row['ID']]) ? $contacts[$row['ID']] : null,
                'get_addresses' => !empty($addresses[$row['ID']]) ? $addresses[$row['ID']] : null,
                'get_metros'    => !empty($metros[$row['ID']]) ? $metros[$row['ID']] : null,
                'get_working_time'  => !empty($working_time[$row['ID']]) ? $working_time[$row['ID']] : null,
            ];
            $result_array = array_merge($row, $info);
            $result[] = $result_array;
        }

        return $result;
    }

    public function create($fields) {
        $date = date('Y-m-d H:i:s');

        $organization = [
            'USER_ID'                       => !empty($fields['main']['user_id']) ? $fields['main']['user_id'] : null,
            'CATEGORY_ID'                   => !empty($fields['main']['category_id']) ? $fields['main']['category_id'] : null,
            'SPECIALIZATION_ID'             => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'TYPE_ID'                       => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'BRAND'                         => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'ENTITY'                        => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'TAXATION_ID'                   => !empty($fields['main']['taxation_id']) ? $fields['main']['taxation_id'] : null,
            'DESCRIPTION'                   => !empty($fields['main']['description']) ? $fields['main']['description'] : '',
            'LICENSE'                       => !empty($fields['main']['license']) ? $fields['main']['license'] : null,
            'OGRN'                          => !empty($fields['main']['ogrn']) ? $fields['main']['ogrn'] : null,
            'INN'                           => !empty($fields['main']['inn']) ? $fields['main']['inn'] : null,
            'OKVED'                         => !empty($fields['main']['okved']) ? $fields['main']['okved'] : null,
            'OKPO'                          => !empty($fields['main']['okpo']) ? $fields['main']['okpo'] : null,
            'KPP'                           => !empty($fields['main']['kpp']) ? $fields['main']['kpp'] : null,
            'OKTMO'                         => !empty($fields['main']['oktmo']) ? $fields['main']['oktmo'] : null,
            'CHECKING_ACCOUNT'              => !empty($fields['main']['checking_account']) ? $fields['main']['checking_account'] : null,
            'CORR_ACCOUNT'                  => !empty($fields['main']['corr_account']) ? $fields['main']['corr_account'] : null,
            'BANK'                          => !empty($fields['main']['bank']) ? $fields['main']['bank'] : null,
            'BIK'                           => !empty($fields['main']['bik']) ? $fields['main']['bik'] : null,
            'ADMINISTRATOR'                 => !empty($fields['main']['administrator']) ? $fields['main']['administrator'] : null,
            'CEO'                           => !empty($fields['main']['ceo']) ? $fields['main']['ceo'] : null,
            'CHIEF_ACCOUNTANT'              => !empty($fields['main']['chief_accountant']) ? $fields['main']['chief_accountant'] : null,
            'LATITUDE'                      => !empty($fields['main']['latitude']) ? $fields['main']['latitude'] : null,
            'LONGITUDE'                     => !empty($fields['main']['longitude']) ? $fields['main']['longitude'] : null,
            'IN_CAROUSEL'                   => !empty($fields['main']['in_carousel']) ? 1 : 0,
            'PUBLIC'                        => !empty($fields['main']['public']) ? 1 : 0,
            'ACTIVE'                        => !empty($fields['main']['active']) ? 1 : 0,
            'MODERATED'                     => !empty($fields['main']['moderated']) ? 1 : 0,
            'UPDATED_AT'                    => $date,
            'CREATED_AT'                    => $date,
            'CREATED_BY'                    => !empty($fields['main']['user_id']) ? $fields['main']['user_id'] : null,
            'PAYMENT_EXPIRES'               => !empty($fields['main']['payment_expires']) ? $fields['main']['payment_expires'] : null,
            'PARENT_ID'                     => !empty($fields['main']['parent_id']) ? $fields['main']['parent_id'] : null,
            'POPULAR'                       => !empty($fields['main']['popular']) ? 1 : 0,
        ];

        if (!empty($fields['main']['logo']))
            $organization['LOGO'] = $fields['main']['logo'];

        $this->db->insert('ORGANIZATIONS', $fields['main']);
        $organization_id = $this->db->insert_id();

        $filters = [
            'ORGANIZATION_ID'   => $organization_id,
            'STATE'             => !empty($fields['filter']['f_state']) ? 1 : 0,
            'PRIVATE'           => !empty($fields['filter']['f_private']) ? 1 : 0,
            'CHILDREN'          => !empty($fields['filter']['f_children']) ? 1 : 0,
            'AMBULANCE'         => !empty($fields['filter']['f_ambulance']) ? 1 : 0,
            'HOUSE'             => !empty($fields['filter']['f_house']) ? 1 : 0,
            'BOOKING'           => !empty($fields['filter']['f_booking']) ? 1 : 0,
            'DELIVERY'          => !empty($fields['filter']['f_delivery']) ? 1 : 0,
            'DAYNIGHT'          => !empty($fields['filter']['f_daynight']) ? 1 : 0,
            'DMS'               => !empty($fields['filter']['f_dms']) ? 1 : 0,
            'DLO'               => !empty($fields['filter']['f_dlo']) ? 1 : 0,
            'OPTICS'            => !empty($fields['filter']['f_optics']) ? 1 : 0,
            'RPO'               => !empty($fields['filter']['f_rpo']) ? 1 : 0,
            'HOMEOPATHY'        => !empty($fields['filter']['f_homeopathy']) ? 1 : 0
        ];

        $this->db->insert('ORGANIZATION_EVIDANCE', $filters);

        foreach ($fields['contacts'] as $contact) {
            $organization_contact = [
                'ORGANIZATION_ID'               => $organization_id,
                'TYPE_ID'                       => $contact['type_id'],
                'NAME'                          => $contact['value']
            ];

            $this->db->insert('ORGANIZATION_CONTACTS', $organization_contact);
        }

        foreach ($fields['addresses'] as $address) {
            $organization_address = [
                'ORGANIZATION_ID'               => $organization_id,
                'TYPE_ID'                       => $address['type_id'],
                'INDEX'                         => $address['index'],
                'REGION'                        => $address['region'],
                'CITY'                          => $address['city'],
                'STREET'                        => $address['street'],
                'HOUSE'                         => $address['house'],
                'BUILDING'                      => $address['building'],
                'OFFICE'                        => $address['office']
            ];

            $this->db->insert('ORGANIZATION_ADDRESSES', $organization_address);
        }

        foreach ($fields['working_times'] as $working_time) {
            $organization_address = [
                'ORGANIZATION_ID'               => $organization_id,
                'DAY_ID'                        => !empty($working_time['day_id']) ? $working_time['day_id'] : null,
                'WORK_FROM'                     => !empty($working_time['work_from']) ? $working_time['work_from'] : null,
                'WORK_TO'                       => !empty($working_time['work_to']) ? $working_time['work_to'] : null,
                'BREAK_FROM'                    => !empty($working_time['break_from']) ? $working_time['break_from'] : null,
                'BREAK_TO'                      => !empty($working_time['break_to']) ? $working_time['break_to'] : null,
            ];

            $this->db->insert('ORGANIZATION_WORKING_TIME', $organization_address);
        }

        foreach ($fields['metros'] as $metro) {
            $organization_address = [
                'ORGANIZATION_ID'               => $organization_id,
                'METRO_ID'                      => $metro['id']
            ];

            $this->db->insert('ORGANIZATION_METRO', $organization_address);
        }

        return $organization_id;
    }
    public function create2($fields) {
        $date = date('Y-m-d H:i:s');

        $organization = [
            'USER_ID'                       => !empty($fields['main']['user_id']) ? $fields['main']['user_id'] : null,
            'CATEGORY_ID'                   => !empty($fields['main']['category_id']) ? $fields['main']['category_id'] : null,
            'SPECIALIZATION_ID'             => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'TYPE_ID'                       => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'BRAND'                         => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'ENTITY'                        => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'TAXATION_ID'                   => !empty($fields['main']['taxation_id']) ? $fields['main']['taxation_id'] : null,
            'DESCRIPTION'                   => !empty($fields['main']['description']) ? $fields['main']['description'] : '',
            'LICENSE'                       => !empty($fields['main']['license']) ? $fields['main']['license'] : null,
            'OGRN'                          => !empty($fields['main']['ogrn']) ? $fields['main']['ogrn'] : null,
            'INN'                           => !empty($fields['main']['inn']) ? $fields['main']['inn'] : null,
            'OKVED'                         => !empty($fields['main']['okved']) ? $fields['main']['okved'] : null,
            'OKPO'                          => !empty($fields['main']['okpo']) ? $fields['main']['okpo'] : null,
            'KPP'                           => !empty($fields['main']['kpp']) ? $fields['main']['kpp'] : null,
            'OKTMO'                         => !empty($fields['main']['oktmo']) ? $fields['main']['oktmo'] : null,
            'CHECKING_ACCOUNT'              => !empty($fields['main']['checking_account']) ? $fields['main']['checking_account'] : null,
            'CORR_ACCOUNT'                  => !empty($fields['main']['corr_account']) ? $fields['main']['corr_account'] : null,
            'BANK'                          => !empty($fields['main']['bank']) ? $fields['main']['bank'] : null,
            'BIK'                           => !empty($fields['main']['bik']) ? $fields['main']['bik'] : null,
            'ADMINISTRATOR'                 => !empty($fields['main']['administrator']) ? $fields['main']['administrator'] : null,
            'CEO'                           => !empty($fields['main']['ceo']) ? $fields['main']['ceo'] : null,
            'CHIEF_ACCOUNTANT'              => !empty($fields['main']['chief_accountant']) ? $fields['main']['chief_accountant'] : null,
            'LATITUDE'                      => !empty($fields['main']['latitude']) ? $fields['main']['latitude'] : null,
            'LONGITUDE'                     => !empty($fields['main']['longitude']) ? $fields['main']['longitude'] : null,
            'IN_CAROUSEL'                   => !empty($fields['main']['in_carousel']) ? 1 : 0,
            'PUBLIC'                        => !empty($fields['main']['public']) ? 1 : 0,
            'ACTIVE'                        => !empty($fields['main']['active']) ? 1 : 0,
            'MODERATED'                     => !empty($fields['main']['moderated']) ? 1 : 0,
            'UPDATED_AT'                    => $date,
            'CREATED_AT'                    => $date,
            'CREATED_BY'                    => !empty($fields['main']['user_id']) ? $fields['main']['user_id'] : null,
            'PAYMENT_EXPIRES'               => !empty($fields['main']['payment_expires']) ? $fields['main']['payment_expires'] : null,
            'PARENT_ID'                     => !empty($fields['main']['parent_id']) ? $fields['main']['parent_id'] : null,
            'POPULAR'                       => !empty($fields['main']['popular']) ? 1 : 0,
        ];

        if (!empty($fields['main']['logo']))
            $organization['LOGO'] = $fields['main']['logo'];

        $this->db->insert('ORGANIZATIONS', $fields['main']);
        $organization_id = $this->db->insert_id();

        $filters = [
            'ORGANIZATION_ID'   => $organization_id,
            'STATE'             => !empty($fields['filter']['f_state']) ? 1 : 0,
            'PRIVATE'           => !empty($fields['filter']['f_private']) ? 1 : 0,
            'CHILDREN'          => !empty($fields['filter']['f_children']) ? 1 : 0,
            'AMBULANCE'         => !empty($fields['filter']['f_ambulance']) ? 1 : 0,
            'HOUSE'             => !empty($fields['filter']['f_house']) ? 1 : 0,
            'BOOKING'           => !empty($fields['filter']['f_booking']) ? 1 : 0,
            'DELIVERY'          => !empty($fields['filter']['f_delivery']) ? 1 : 0,
            'DAYNIGHT'          => !empty($fields['filter']['f_daynight']) ? 1 : 0,
            'DMS'               => !empty($fields['filter']['f_dms']) ? 1 : 0,
            'DLO'               => !empty($fields['filter']['f_dlo']) ? 1 : 0,
            'OPTICS'            => !empty($fields['filter']['f_optics']) ? 1 : 0,
            'RPO'               => !empty($fields['filter']['f_rpo']) ? 1 : 0,
            'HOMEOPATHY'        => !empty($fields['filter']['f_homeopathy']) ? 1 : 0
        ];

        $this->db->insert('ORGANIZATION_EVIDANCE', $filters);

        foreach ($fields['contacts'] as $contact) {
            $organization_contact = [
                'ORGANIZATION_ID'               => $organization_id,
                'TYPE_ID'                       => $contact['type_id'],
                'NAME'                          => $contact['value']
            ];

            $this->db->insert('ORGANIZATION_CONTACTS', $organization_contact);
        }

        foreach ($fields['addresses'] as $address) {
            $organization_address = [
                'ORGANIZATION_ID'               => $organization_id,
                'TYPE_ID'                       => $address['type_id'],
                'INDEX'                         => $address['index'],
                'REGION'                        => $address['region'],
                'CITY'                          => $address['city'],
                'STREET'                        => $address['street'],
                'HOUSE'                         => $address['house'],
                'BUILDING'                      => $address['building'],
                'OFFICE'                        => $address['office']
            ];

            $this->db->insert('ORGANIZATION_ADDRESSES', $organization_address);
        }

        foreach ($fields['working_times'] as $working_time) {
            $organization_address = [
                'ORGANIZATION_ID'               => $organization_id,
                'DAY_ID'                        => !empty($working_time['day_id']) ? $working_time['day_id'] : null,
                'WORK_FROM'                     => !empty($working_time['work_from']) ? $working_time['work_from'] : null,
                'WORK_TO'                       => !empty($working_time['work_to']) ? $working_time['work_to'] : null,
                'BREAK_FROM'                    => !empty($working_time['break_from']) ? $working_time['break_from'] : null,
                'BREAK_TO'                      => !empty($working_time['break_to']) ? $working_time['break_to'] : null,
            ];

            $this->db->insert('ORGANIZATION_WORKING_TIME', $organization_address);
        }

        foreach ($fields['metros'] as $metro) {
            $organization_address = [
                'ORGANIZATION_ID'               => $organization_id,
                'METRO_ID'                      => $metro['id']
            ];

            $this->db->insert('ORGANIZATION_METRO', $organization_address);
        }

        return true;
    }
    public function update($fields) {
        if (empty($fields['id']))
            throw new Exception('РћР±СЏР·Р°С‚РµР»СЊРЅС‹Рµ РїРѕР»СЏ РЅРµ Р·Р°РїРѕР»РЅРµРЅС‹');

        $date = date('Y-m-d H:i:s');

        $organization = [
            'CATEGORY_ID'                   => ($fields['main']['category_id']),
            'TYPE_ID'                       => ($fields['main']['type_id']),
            'SPECIALIZATION_ID'             => !empty($fields['main']['specialization_id']) ? $fields['main']['specialization_id'] : null,
            'BRAND'                         => ($fields['main']['brand']),
            'ENTITY'                        => ($fields['main']['entity']),
            'TAXATION_ID'                   => !empty($fields['main']['taxation_id']) ? $fields['main']['taxation_id'] : null,
            'DESCRIPTION'                   => !empty($fields['main']['description']) ? $fields['main']['description'] : null,
            'LICENSE'                       => !empty($fields['main']['license']) ? $fields['main']['license'] : null,
            'OGRN'                          => !empty($fields['main']['ogrn']) ? $fields['main']['ogrn'] : null,
            'INN'                           => !empty($fields['main']['inn']) ? $fields['main']['inn'] : null,
            'OKVED'                         => !empty($fields['main']['okved']) ? $fields['main']['okved'] : null,
            'OKPO'                          => !empty($fields['main']['okpo']) ? $fields['main']['okpo'] : null,
            'KPP'                           => !empty($fields['main']['kpp']) ? $fields['main']['kpp'] : null,
            'OKTMO'                         => !empty($fields['main']['oktmo']) ? $fields['main']['oktmo'] : null,
            'CHECKING_ACCOUNT'              => !empty($fields['main']['checking_account']) ? $fields['main']['checking_account'] : null,
            'CORR_ACCOUNT'                  => !empty($fields['main']['corr_account']) ? $fields['main']['corr_account'] : null,
            'BANK'                          => !empty($fields['main']['bank']) ? $fields['main']['bank'] : null,
            'BIK'                           => !empty($fields['main']['bik']) ? $fields['main']['bik'] : null,
            'ADMINISTRATOR'                 => !empty($fields['main']['administrator']) ? $fields['main']['administrator'] : null,
            'CEO'                           => !empty($fields['main']['ceo']) ? $fields['main']['ceo'] : null,
            'CHIEF_ACCOUNTANT'              => !empty($fields['main']['chief_accountant']) ? $fields['main']['chief_accountant'] : null,
            'LATITUDE'                      => !empty($fields['main']['latitude']) ? $fields['main']['latitude'] : null,
            'LONGITUDE'                     => !empty($fields['main']['longitude']) ? $fields['main']['longitude'] : null,
            'IN_CAROUSEL'                   => !empty($fields['main']['in_carousel']) ? 1 : 0,
            'PUBLIC'                        => !empty($fields['main']['public']) ? 1 : 0,
            'ACTIVE'                        => !empty($fields['main']['active']) ? 1 : 0,
            'MODERATED'                     => !empty($fields['main']['moderated']) ? 1 : 0,
            'UPDATED_AT'                    => $date,
            'PAYMENT_EXPIRES'               => !empty($fields['main']['payment_expires']) ? $fields['main']['payment_expires'] : null,
            'POPULAR'                       => !empty($fields['main']['popular']) ? 1 : 0,
        ];

        if (!empty($fields['main']['user_id']))
            $organization['USER_ID'] = (int)$fields['main']['user_id'];
        if (!empty($fields['main']['logo'])){
           $organization['LOGO'] = $fields['main']['logo'];
            $fields['main']['logo']='/images/'.$fields['main']['logo'];
          
        }
            

        $this->db->where(['ID' => $fields['id']]);
        
        $this->db->update('ORGANIZATIONS', $fields['main']);

        $this->db->delete('ORGANIZATION_EVIDANCE', ['ORGANIZATION_ID' => $fields['id']]);

        $filters = [
            'ORGANIZATION_ID'   => $fields['id'],
            'STATE'             => !empty($fields['filter']['f_state']) ? 1 : 0,
            'PRIVATE'           => !empty($fields['filter']['f_private']) ? 1 : 0,
            'CHILDREN'          => !empty($fields['filter']['f_children']) ? 1 : 0,
            'AMBULANCE'         => !empty($fields['filter']['f_ambulance']) ? 1 : 0,
            'HOUSE'             => !empty($fields['filter']['f_house']) ? 1 : 0,
            'BOOKING'           => !empty($fields['filter']['f_booking']) ? 1 : 0,
            'DELIVERY'          => !empty($fields['filter']['f_delivery']) ? 1 : 0,
            'DAYNIGHT'          => !empty($fields['filter']['f_daynight']) ? 1 : 0,
            'DMS'               => !empty($fields['filter']['f_dms']) ? 1 : 0,
            'DLO'               => !empty($fields['filter']['f_dlo']) ? 1 : 0,
            'OPTICS'            => !empty($fields['filter']['f_optics']) ? 1 : 0,
            'RPO'               => !empty($fields['filter']['f_rpo']) ? 1 : 0,
            'HOMEOPATHY'        => !empty($fields['filter']['f_homeopathy']) ? 1 : 0
        ];

        $this->db->insert('ORGANIZATION_EVIDANCE', $filters);

        $this->db->delete('ORGANIZATION_CONTACTS', ['ORGANIZATION_ID' => $fields['id']]);

        foreach ($fields['contacts'] as $contact) {
            $organization_contact = [
                'ORGANIZATION_ID'               => $fields['id'],
                'TYPE_ID'                       => $contact['type_id'],
                'NAME'                          => $contact['value']
            ];

            $this->db->insert('ORGANIZATION_CONTACTS', $organization_contact);
        }

        $this->db->delete('ORGANIZATION_ADDRESSES', ['ORGANIZATION_ID' => $fields['id']]);

        foreach ($fields['addresses'] as $address) {
            $organization_address = [
                'ORGANIZATION_ID'               => $fields['id'],
                'TYPE_ID'                       => $address['type_id'],
                'INDEX'                         => $address['index'],
                'REGION'                        => $address['region'],
                'CITY'                          => $address['city'],
                'STREET'                        => $address['street'],
                'HOUSE'                         => $address['house'],
                'BUILDING'                      => $address['building'],
                'OFFICE'                        => $address['office']
            ];

            $this->db->insert('ORGANIZATION_ADDRESSES', $organization_address);
        }

        $this->db->delete('ORGANIZATION_WORKING_TIME', ['ORGANIZATION_ID' => $fields['id']]);

        foreach ($fields['working_times'] as $working_time) {
            $organization_address = [
                'ORGANIZATION_ID'               => $fields['id'],
                'DAY_ID'                        => !empty($working_time['day_id']) ? $working_time['day_id'] : null,
                'WORK_FROM'                     => !empty($working_time['work_from']) ? $working_time['work_from'] : null,
                'WORK_TO'                       => !empty($working_time['work_to']) ? $working_time['work_to'] : null,
                'BREAK_FROM'                    => !empty($working_time['break_from']) ? $working_time['break_from'] : null,
                'BREAK_TO'                      => !empty($working_time['break_to']) ? $working_time['break_to'] : null,
            ];

            $this->db->insert('ORGANIZATION_WORKING_TIME', $organization_address);
        }

        $this->db->delete('ORGANIZATION_METRO', ['ORGANIZATION_ID' => $fields['id']]);

        foreach ($fields['metros'] as $metro) {
            $organization_address = [
                'ORGANIZATION_ID'               => $fields['id'],
                'METRO_ID'                      => $metro['id']
            ];

            $this->db->insert('ORGANIZATION_METRO', $organization_address);
        }
        
        return true;
    }

    public function get_specialisations()
    {
        return $this->db->get('SPECIALIZATIONS')->result_array();
    }
}