<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Geo_model extends CI_Model
{
    public function get_countries($id = null, $like = null)
    {
        $id     = (int)$id;
        $where  = "";

        if (!empty($id)) {
            $wheres[] = "(COUNTRY_ID = $id)";
        }
        if (!empty($like)) {
            $like   = mysqli_real_escape_string($this->db->conn_id, $like);
            $wheres = "(NAME LIKE '%".$like."%')";
        }

        $where = "WHERE ".implode(' AND ', $wheres);

        $result = $this->db->query("SELECT * FROM COUNTRY $where ORDER BY NAME");

        if (!empty($id))
            return $result->row_array();
        else
            return $result->result_array();
    }

    public function get_region($id = null, $like = null)
    {
        $id     = (int)$id;
        $where  = "";

        if (!empty($id)) {
            $wheres[] = "(REGION_ID = $id)";
        }
        if (!empty($like)) {
            $like   = mysqli_real_escape_string($this->db->conn_id, $like);
            $wheres = "(NAME LIKE '%".$like."%')";
        }

        $where = "WHERE ".implode(' AND ', $wheres);

        $result = $this->db->query("SELECT * FROM REGION $where ORDER BY NAME");
        //TODO убрать группировку и вывести регионы в плашке

        if (!empty($id))
            return $result->row_array();
        else
            return $result->result_array();
    }

    public function get_cities($id = null, $like = null, $region_id = null, $country_id = null)
    {
        $id         = (int)$id;
        $region_id  = (int)$region_id;
        $country_id = (int)$country_id;
        $where      = "";

        if (!empty($id)) {
            $wheres[] = "(CITY.ID = $id)";
        }
        if (!empty($region_id)) {
            $wheres[] = "(CITY.REGION_ID = $region_id)";
        }
        if (!empty($country_id)) {
            $wheres[] = "(CITY.COUNTRY_ID = $country_id)";
        }
        if (!empty($like)) {
            $like   = mysqli_real_escape_string($this->db->conn_id, $like);
            $wheres = "(CITY.NAME LIKE '%".$like."%')";
        }

        $where = "WHERE ".implode(' AND ', $wheres);

        $result = $this->db->query("SELECT
          CITY.CITY_ID ID,
          CITY.NAME,
          REGION.NAME REGION
          FROM CITY
          LEFT JOIN REGION ON CITY.REGION_ID = REGION.REGION_ID
          $where
          GROUP BY NAME
          ORDER BY NAME");

        if (!empty($id))
            return $result->row_array();
        else
            return $result->result_array();
    }
}