<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institution_photos_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_institution_id($institution_id)
    {
        if (!empty($institution_id))
            return $this->db->query("SELECT * FROM INSTITUTION_PHOTOS WHERE INSTITUTION_ID =".(int)$institution_id)->result_array();
    }
    public function get_file_by_institution_id($institution_id)
    {
        if (!empty($institution_id))
            return $this->db->query("SELECT FILE FROM INSTITUTION_PHOTOS WHERE INSTITUTION_ID =".(int)$institution_id)->result_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('INSTITUTION_PHOTOS', $fields);
		return $this->db->insert_id();
    }
    public function delete($institution_id)
    {
        if (!empty($institution_id))
            $this->db->delete('INSTITUTION_PHOTOS', ['INSTITUTION_ID' => $institution_id]);
    }
    public function delete_by_file($file)
    {
        if (!empty($file))
            $this->db->delete('INSTITUTION_PHOTOS', ['FILE' => $file]);
    }
}