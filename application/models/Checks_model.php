<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checks_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_fin_id()
	{
		return $this->db->query("SELECT * FROM CHECK_IDS ORDER BY ID DESC")->row_array();
	}
	public function create($fields=[])
	{
		if(!empty($fields))
		{
			$this->db->insert("CHECK_IDS",$fields);
			return $this->db->insert_id();
		}
	}

}