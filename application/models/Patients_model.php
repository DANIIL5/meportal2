<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patients_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_specialist_id($specialist_id)
    {
        if (!empty($specialist_id))
            return $this->db->query("SELECT * FROM PATIENTS WHERE SPECIALIST_ID =".(int)$specialist_id)->result_array();
    }
    public function get_by_specialist_id_user_id($specialist_id,$user_id)
    {
        if (!empty($specialist_id) and !empty($user_id))
            return $this->db->query("SELECT * FROM PATIENTS WHERE SPECIALIST_ID =".(int)$specialist_id." AND USER_ID =".(int)$user_id)->row_array();
    }
    public function get_by_institution_id($institution_id)
    {
        if (!empty($institution_id))
            return $this->db->query("SELECT * FROM PATIENTS WHERE INSTITUTION_ID =".(int)$institution_id)->result_array();
    }
    public function get_by_institution_id_user_id($institution_id,$user_id)
    {
        if (!empty($institution_id) and !empty($user_id))
            return $this->db->query("SELECT * FROM PATIENTS WHERE INSTITUTION_ID =".(int)$institution_id." AND USER_ID =".(int)$user_id)->row_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('PATIENTS', $fields);
        return $this->db->insert_id();
    }
}