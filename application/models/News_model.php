<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->query("SELECT * FROM RSS_NEWS ORDER BY DATE DESC")->result_array();
    }

    public function get_last()
    {
        return $this->db->query("SELECT * FROM RSS_NEWS ORDER BY DATE DESC LIMIT 4")->result_array();
    }
}