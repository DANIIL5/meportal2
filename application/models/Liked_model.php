<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liked_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_liked($user_id, $i ,$y)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM ".(string)$i." WHERE USER_ID =".(int)$user_id.(string)$y)->result_array();
    }

    /*
     * Моя дописка
     */

    public function get_liked_specialists($user_id)
    {
        $user_id = (int)$user_id;

        return $this->db->query("
            SELECT 
            SPECIALISTS.*,
            DOCTOR_TYPES.NAME SPECIALIZATION,
            ORGANIZATIONS.BRAND WORK
            FROM LIKED_SPECIALISTS
            LEFT JOIN SPECIALISTS ON SPECIALISTS.ID = LIKED_SPECIALISTS.SPECIALIST_ID
            LEFT JOIN DOCTOR_TYPES ON DOCTOR_TYPES.ID = SPECIALISTS.SPECIALIZATION_ID
            LEFT JOIN ORGANIZATIONS ON ORGANIZATIONS.ID = SPECIALISTS.BRANCH_ID
            WHERE LIKED_SPECIALISTS.USER_ID = $user_id")->result_array();
    }

    public function get_liked_institutions($user_id)
    {
        $user_id = (int)$user_id;

        return $this->db->query("
            SELECT 
            ORGANIZATIONS.*,
            INSTITUTION_TYPES.NAME TYPE
            FROM LIKED_INSTITUTIONS
            LEFT JOIN ORGANIZATIONS ON ORGANIZATIONS.ID = LIKED_INSTITUTIONS.INSTITUTION_ID
            LEFT JOIN INSTITUTION_TYPES ON INSTITUTION_TYPES.ID = ORGANIZATIONS.TYPE_ID 
            WHERE LIKED_INSTITUTIONS.USER_ID = $user_id")->result_array();
    }

    public function get_liked_preps($user_id)
    {
        $user_id = (int)$user_id;

        // Здесь допилить джоины по лекарствам

        return $this->db->query("
            SELECT 
            NOMEN.*,
            IDENT_WIND_STR.IWID IMAGE
            FROM LIKED_PREPS
            LEFT JOIN NOMEN ON NOMEN.ID = LIKED_PREPS.PREPS_ID
            LEFT JOIN IDENT_WIND_STR ON IDENT_WIND_STR.NOMENID = NOMEN.ID
            WHERE LIKED_PREPS.USER_ID = $user_id")->result_array();
    }

    public function get_liked_articles($user_id)
    {
        $user_id = (int)$user_id;

        // Здесь допилить джоины по статьям

        return $this->db->query("
            SELECT 
            ARTICLES.*
            FROM LIKED_ARTICLES
            LEFT JOIN ARTICLES ON ARTICLES.ID = LIKED_ARTICLES.ARTICLES_ID
            WHERE LIKED_ARTICLES.USER_ID = $user_id")->result_array();
    }

    public function get_by_count_liked($user_id, $i)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(*) AS COUNT FROM ".(string)$i." WHERE USER_ID =".(int)$user_id)->result_array();
    }
    public function get_by_count_liked_specialists_specialization($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(SPECIALIST_SPECIALIZATION) AS COUNT, SPECIALIST_SPECIALIZATION FROM LIKED_SPECIALISTS WHERE USER_ID =".(int)$user_id." GROUP BY SPECIALIST_SPECIALIZATION")->result_array();
    }
    public function get_by_count_liked_specialists_specialization_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(SPECIALIST_SPECIALIZATION_ID) AS COUNT, SPECIALIST_SPECIALIZATION_ID FROM LIKED_SPECIALISTS WHERE USER_ID =".(int)$user_id." GROUP BY SPECIALIST_SPECIALIZATION_ID")->result_array();
    }
    public function get_by_count_liked_institutions_type($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(INSTITUTION_TYPE) AS COUNT, INSTITUTION_TYPE FROM LIKED_INSTITUTIONS WHERE USER_ID =".(int)$user_id." GROUP BY INSTITUTION_TYPE")->result_array();
    }
    public function get_by_count_liked_institutions_type_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(INSTITUTION_TYPE_ID) AS COUNT, INSTITUTION_TYPE_ID FROM LIKED_INSTITUTIONS WHERE USER_ID =".(int)$user_id." GROUP BY INSTITUTION_TYPE_ID")->result_array();
    }
    public function get_by_count_liked_preps_type($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(PREPS_TYPE) AS COUNT, PREPS_TYPE FROM LIKED_PREPS WHERE USER_ID =".(int)$user_id." GROUP BY PREPS_TYPE")->result_array();
    }
    public function get_by_count_liked_preps_type_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(PREPS_TYPE_ID) AS COUNT, PREPS_TYPE_ID FROM LIKED_PREPS WHERE USER_ID =".(int)$user_id." GROUP BY PREPS_TYPE_ID")->result_array();
    }
    public function get_by_count_liked_articles_category($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(ARTICLES_CATEGORY) AS COUNT, ARTICLES_CATEGORY FROM LIKED_ARTICLES WHERE USER_ID =".(int)$user_id." GROUP BY ARTICLES_CATEGORY")->result_array();
    }
    public function get_by_count_liked_articles_category_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT COUNT(ARTICLES_CATEGORY_ID) AS COUNT, ARTICLES_CATEGORY_ID FROM LIKED_ARTICLES WHERE USER_ID =".(int)$user_id." GROUP BY ARTICLES_CATEGORY_ID")->result_array();
    }
    public function deleteSpecialist($id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_SPECIALISTS', ['ID' => $id]);
    }
    public function deleteInstitution($id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_INSTITUTIONS', ['ID' => $id]);
    }
    public function deleteArticle($id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_ARTICLES', ['ID' => $id]);
    }
    public function deletePrep($id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_PREPS', ['ID' => $id]);
    }
    public function deleteSpecialistByObject($id, $user_id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_SPECIALISTS', ['SPECIALIST_ID' => $id,'USER_ID' =>$user_id]);
    }
    public function deleteInstitutionByObject($id, $user_id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_INSTITUTIONS', ['INSTITUTION_ID' => $id, 'USER_ID' => $user_id]);
    }
    public function deleteArticleByObject($id, $user_id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_ARTICLES', ['ARTICLES_ID' => $id, 'USER_ID' => $user_id]);
    }
    public function deletePrepByObject($id, $user_id)
    {
        if (!empty($id))
            $this->db->delete('LIKED_PREPS', ['PREPS_ID' => $id, 'USER_ID' => $user_id]);
    }
}