<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institution_specialists_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_by_institution_id($user_id)
	{
		if (!empty($user_id))
			return $this->db->query("SELECT ID FROM INSTITUTIONS WHERE USER_ID =".(int)$user_id)->row_array();
	}
	public function get_by_institution_specialists($institution_id, $y)
	{
		if (!empty($institution_id))
			return $this->db->query("SELECT * FROM INSTITUTION_SPECIALISTS WHERE INSTITUTION_ID =".(int)$institution_id.(string)$y)->result_array();
	}
	public function get_by_count_institution_specialists_specialization($institution_id)
	{
		if (!empty($institution_id))
			return $this->db->query("SELECT COUNT(SPECIALIST_SPECIALIZATION) AS COUNT, SPECIALIST_SPECIALIZATION FROM INSTITUTION_SPECIALISTS WHERE INSTITUTION_ID =".(int)$institution_id." GROUP BY SPECIALIST_SPECIALIZATION")->result_array();
	}
	public function get_by_count_institution_specialists_specialization_id($institution_id)
	{
		if (!empty($institution_id))
			return $this->db->query("SELECT COUNT(SPECIALIST_SPECIALIZATION_ID) AS COUNT, SPECIALIST_SPECIALIZATION_ID FROM INSTITUTION_SPECIALISTS WHERE INSTITUTION_ID =".(int)$institution_id." GROUP BY SPECIALIST_SPECIALIZATION_ID")->result_array();
	}
	public function delete($id)
	{
		if (!empty($id))
			$this->db->delete('INSTITUTION_SPECIALISTS', ['ID' => $id]);
	}
}