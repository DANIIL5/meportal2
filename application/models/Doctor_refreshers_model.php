<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_refreshers_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_specialist_id($specialist_id)
    {
        if (!empty($specialist_id))
            return $this->db->query("SELECT * FROM SPECIALIST_REFRESHER_TRAININGS WHERE SPECIALIST_ID =".(int)$specialist_id)->result_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('SPECIALIST_REFRESHER_TRAININGS', $fields);
		return $this->db->insert_id();
    }
    public function delete($id)
    {
        if (!empty($id))
            $this->db->delete('SPECIALIST_REFRESHER_TRAININGS', ['ID' => $id]);
    }
    public function update($fields = [],$id)
    {
        if (!empty($id) and !empty($fields))
            $this->db->update('SPECIALIST_REFRESHER_TRAININGS',$fields,['ID'=>$id]);
    }
}