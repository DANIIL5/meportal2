<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carousel_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->query("SELECT * FROM HEADER_CAROUSEL")->result_array();
    }

    public function get_images($category_id)
    {
        return $this->db->query("SELECT * FROM HEADER_CAROUSEL WHERE CATEGORY_ID = $category_id")->result_array();
    }
}