<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_by_user_id_out($user_id,$search = false)
	{
		if (!empty($user_id))
		{
			if($search === false)
				$q = " ";
			else
				$q = " AND (POSTS.THEME LIKE '%".$search."%'
					OR POSTS.MESSAGE LIKE '%".$search."%'
					OR POST_DESTINATIONS.DESTINATION_NAME LIKE '%".$search."%') ";
			return $this->db->query("SELECT POSTS.*,
					POST_DESTINATIONS.ID_DESTINATION,
					POST_DESTINATIONS.DESTINATION_NAME,
					POST_DESTINATIONS.ID_POST,
					POST_DESTINATIONS.DESTINATION_READED,
					POST_DESTINATIONS.TO_ADMIN
				FROM POSTS INNER JOIN POST_DESTINATIONS
				ON POSTS.ID = POST_DESTINATIONS.ID_POST
				WHERE POSTS.ID_SENDER=".$user_id."
					AND POSTS.DELETED <> 1
					AND POSTS.FINAL_DELETED <> 1".$q."
				ORDER BY POSTS.TIME DESC")->result_array();
		}
	}
	public function get_by_user_id_in($user_id,$search = false)
	{
		if (!empty($user_id))
		{
			if($search === false)
				$q = " ";
			else
				$q = " AND (POSTS.THEME LIKE '%".$search."%'
					OR POSTS.MESSAGE LIKE '%".$search."%'
					OR POSTS.NAME_SENDER LIKE '%".$search."%') ";
			return $this->db->query("SELECT POSTS.*,
					POST_DESTINATIONS.ID_DESTINATION,
					POST_DESTINATIONS.ID_POST,
					POST_DESTINATIONS.DESTINATION_READED,
					POST_DESTINATIONS.TO_ADMIN
				FROM POSTS INNER JOIN POST_DESTINATIONS
				ON POST_DESTINATIONS.ID_POST = POSTS.ID
				WHERE POST_DESTINATIONS.ID_DESTINATION=".$user_id."
					AND POST_DESTINATIONS.DELETED <> 1
					AND POST_DESTINATIONS.FINAL_DELETED<>1".$q."
				ORDER BY POSTS.TIME DESC")->result_array();
		}
	}
	public function get_by_user_id_in_count_unread($user_id)
	{
		if (!empty($user_id))
			return $this->db->query("SELECT COUNT(*)
			    FROM POSTS INNER JOIN POST_DESTINATIONS
				ON POST_DESTINATIONS.ID_POST = POSTS.ID
				WHERE POST_DESTINATIONS.ID_DESTINATION=".$user_id."
					AND POST_DESTINATIONS.DELETED <> 1
					AND POST_DESTINATIONS.FINAL_DELETED<>1
					AND POST_DESTINATIONS.DESTINATION_READED<>1")->result_array();
	}
	public function get_by_user_id_all($user_id,$search = false)
	{
		if (!empty($user_id))
		{
			if($search === false)
				$q = " ";
			else
				$q = " AND (POSTS.THEME LIKE '%".$search."%'
					OR POSTS.MESSAGE LIKE '%".$search."%'
					OR POSTS.NAME_SENDER LIKE '%".$search."%'
					OR POST_DESTINATIONS.DESTINATION_NAME LIKE '%".$search."%') ";
			return $this->db->query("SELECT POSTS.*,
					POST_DESTINATIONS.ID_DESTINATION,
					POST_DESTINATIONS.ID_POST,
					POST_DESTINATIONS.DESTINATION_NAME,
					POST_DESTINATIONS.DESTINATION_READED,
					POST_DESTINATIONS.TO_ADMIN
				FROM POSTS INNER JOIN POST_DESTINATIONS
				ON POSTS.ID = POST_DESTINATIONS.ID_POST
				WHERE (POSTS.ID_SENDER=".$user_id."
					AND POSTS.DELETED <> 1
					AND POSTS.FINAL_DELETED <> 1".$q.")
				OR (POST_DESTINATIONS.ID_DESTINATION=".$user_id."
					AND POST_DESTINATIONS.DELETED <> 1
					AND POST_DESTINATIONS.FINAL_DELETED<>1".$q.")
				ORDER BY TIME DESC")->result_array();
		}
	}
	public function get_by_user_id_delete($user_id,$search = false)
	{
		if (!empty($user_id))
		{
			if($search === false)
				$q = " ";
			else
				$q = " AND (POSTS.THEME LIKE '%".$search."%'
					OR POSTS.MESSAGE LIKE '%".$search."%'
					OR POSTS.NAME_SENDER LIKE '%".$search."%'
					OR POST_DESTINATIONS.DESTINATION_NAME LIKE '%".$search."%') ";
			return $this->db->query("SELECT POSTS.*,
					POST_DESTINATIONS.ID_DESTINATION,
					POST_DESTINATIONS.ID_POST,
					POST_DESTINATIONS.DESTINATION_NAME,
					POST_DESTINATIONS.DESTINATION_READED,
					POST_DESTINATIONS.TO_ADMIN
				FROM POSTS INNER JOIN POST_DESTINATIONS
				ON POST_DESTINATIONS.ID_POST = POSTS.ID
				WHERE (POST_DESTINATIONS.ID_DESTINATION=".$user_id."
					AND POST_DESTINATIONS.DELETED = 1
					AND POST_DESTINATIONS.FINAL_DELETED<>1".$q.")
				OR (POSTS.ID_SENDER=".$user_id."
					AND POSTS.DELETED = 1
					AND POSTS.FINAL_DELETED <> 1".$q.")
				ORDER BY TIME DESC")->result_array();
		}
	}
	public function get_by_user_id_liked($user_id,$search = false)
	{
		if (!empty($user_id))
		{
			if($search === false)
				$q = " ";
			else
				$q = " AND (POSTS.THEME LIKE '%".$search."%'
					OR POSTS.MESSAGE LIKE '%".$search."%'
					OR POSTS.NAME_SENDER LIKE '%".$search."%'
					OR POST_DESTINATIONS.DESTINATION_NAME LIKE '%".$search."%') ";
			return $this->db->query("SELECT POSTS.*,
					POST_DESTINATIONS.ID_DESTINATION,
					POST_DESTINATIONS.ID_POST,
					POST_DESTINATIONS.DESTINATION_NAME,
					POST_DESTINATIONS.DESTINATION_READED,
					POST_DESTINATIONS.LIKED,
					POST_DESTINATIONS.TO_ADMIN
				FROM POSTS INNER JOIN POST_DESTINATIONS
				ON POST_DESTINATIONS.ID_POST = POSTS.ID
				WHERE (POST_DESTINATIONS.ID_DESTINATION=".$user_id."
					AND POST_DESTINATIONS.DELETED <> 1
					AND POST_DESTINATIONS.FINAL_DELETED<>1
					AND POST_DESTINATIONS.LIKED = 1".$q.")
				OR (POSTS.ID_SENDER=".$user_id."
					AND POSTS.DELETED <> 1
					AND POSTS.FINAL_DELETED <> 1".$q."
					AND POSTS.LIKED = 1)
				ORDER BY TIME DESC")->result_array();
		}
	}
	public function update($fields = [],$id)
	{
		if (!empty($id) and !empty($fields))
			$this->db->update('POSTS', $fields,['ID'=>$id]);
	}
	public function create($fields = [])
	{
		if (!empty($fields))
		{
			$this->db->insert('POSTS', $fields);
			return $this->db->insert_id();
		}
	}
	public function get_by_id($id)
	{
		if (!empty($id))
			return $this->db->query("SELECT * FROM POSTS WHERE ID =".(int)$id)->row_array();
	}
	public function delete($id)
	{
		if (!empty($id))
			$this->db->delete('POSTS', ['ID' => $id]);
	}

	public function search($q,$categ)
	{
		# code...
	}

}