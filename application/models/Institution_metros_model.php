<?php

/**
 * Created by PhpStorm.
 * User: Артем
 * Date: 25.08.2016
 * Time: 15:05
 */
class Institution_metros_model extends CI_Model
{
    public function get_institution_metro($institution_id)
    {
        $institution_id = (int)$institution_id;
        $result = [];

        if (!empty($institution_id))
            $result = $this->db->query("SELECT * FROM INSTITUTION_METROS WHERE INSTITUTION_ID = $institution_id")->result_array();

        return $result;
    }

    public function drop_institution_metro($institution_id)
    {
        $institution_id = (int)$institution_id;

        if (!empty($institution_id))
            $this->db->query("DELETE FROM INSTITUTION_METROS WHERE INSTITUTION_ID = $institution_id");
    }

    public function add_metro($institution_id, $metro_id)
    {
        $institution_id = (int)$institution_id;
        $metro_id = (int)$metro_id;

        if (!empty($institution_id) && !empty($metro_id)) {
            $data = [
                'INSTITUTION_ID'         => $institution_id,
                'METRO_ID'          => $metro_id
            ];

            $this->db->insert("INSTITUTION_METROS", $data);
        } else {
            throw new Exception("Для создание связи обязательно должны быть переданы идентификаторы организации");
        }
    }

    public function delete_metro($institution_id, $metro_id)
    {

    }

    public function delete_metros($institution_id, $metro_ids)
    {
        $institution_id = (int)$institution_id;

        if (!empty($institution_id) && !empty($metro_ids))
        $in = implode(",",$metro_ids);

        $this->db->query("DELETE FROM INSTITUTION_METROS WHERE INSTITUTION_ID = $institution_id AND METRO_ID IN ($in)");
    }
}