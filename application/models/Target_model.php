<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_user_id($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM TARGETS WHERE USER_ID =".(int)$user_id." ORDER BY TIME")->result_array();
    }
    public function get_by_id($id)
    {
        if (!empty($id))
            return $this->db->query("SELECT * FROM TARGETS WHERE ID =".(int)$id." ORDER BY START")->row_array();
    }
    public function get_by_user_id_liked($user_id)
    {
        if (!empty($user_id))
            return $this->db->query("SELECT * FROM TARGETS WHERE USER_ID =".(int)$user_id." AND LIKED = 1 ORDER BY START")->result_array();
    }
    public function update($fields = [],$id)
    {
        if (!empty($id) and !empty($fields))
            $this->db->update('TARGETS', $fields,['ID'=>$id]);
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('TARGETS', $fields);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        if (!empty($id))
            $this->db->delete('TARGETS', ['ID' => $id]);
    }
    public function get_by_user_id_time($user_id,$date_from,$date_to)
    {
        if (!empty($user_id) and !empty($date_from) and !empty($date_to))
            return $this->db->query("SELECT * FROM TARGETS WHERE USER_ID =".(int)$user_id." AND START>='".$date_from."' AND TIME<='".$date_to."' ORDER BY TIME")->result_array();
    }
}