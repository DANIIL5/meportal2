<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Med_card_files_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_med_card_id($med_card_id)
    {
        if (!empty($med_card_id))
            return $this->db->query("SELECT * FROM MED_CARD_FILES WHERE MED_CARD_ID =".(int)$med_card_id)->result_array();
    }
    public function get_file_by_med_card_id($med_card_id)
    {
        if (!empty($med_card_id))
            return $this->db->query("SELECT FILE FROM MED_CARD_FILES WHERE MED_CARD_ID =".(int)$med_card_id)->result_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('MED_CARD_FILES', $fields);
		return $this->db->insert_id();
    }
    public function delete($med_card_id)
    {
        if (!empty($med_card_id))
            $this->db->delete('MED_CARD_FILES', ['MED_CARD_ID' => $med_card_id]);
    }
    public function delete_by_file($file)
    {
        if (!empty($file))
            $this->db->delete('MED_CARD_FILES', ['FILE' => $file]);
    }
}