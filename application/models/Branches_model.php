<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branches_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_institution_id($institution_id)
    {
        if (!empty($institution_id))
            return $this->db->query("SELECT * FROM BRANCHES WHERE INSTITUTION_ID =".(int)$institution_id)->result_array();
    }
    public function get_by_id($id)
    {
        if (!empty($id))
            return $this->db->query("SELECT * FROM BRANCHES WHERE ID =".(int)$id)->row_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('BRANCHES', $fields);
        return $this->db->insert_id();
    }
    public function delete($id)
    {
        if (!empty($id))
            $this->db->delete('BRANCHES', ['ID' => $id]);
    }
    public function update($fields = [],$id)
    {
        if (!empty($id) and !empty($fields))
            $this->db->update('BRANCHES', $fields,['ID'=>$id]);
    }
    public function get_id_name()
    {
        return $this->db->query("SELECT ID,NAME FROM BRANCHES")->result_array();
    }

    public function get_list()
    {
        return $this->db->query("SELECT
            b.*,
            i.SHORTNAME
            FROM BRANCHES b
            LEFT JOIN INSTITUTIONS i ON i.ID = b.INSTITUTION_ID")->result_array();
    }

    public function new_get_item($id)
    {
        return $this->db->query("SELECT
            BRANCHES.*,
            INSTITUTIONS.LOGO,
            INSTITUTIONS.NAME INSTITUTION_NAME,
            INSTITUTIONS.DESCRIPTION INSTITUTION_DESCRIPTION,
            INSTITUTIONS.SHORTNAME INSTITUTION_LEGAL_NAME
            FROM BRANCHES
            LEFT JOIN INSTITUTIONS ON INSTITUTIONS.ID = BRANCHES.INSTITUTION_ID
            WHERE BRANCHES.ID = $id
            LIMIT 1")->row_array();
    }
}