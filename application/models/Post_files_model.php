<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_files_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_post_id($post_id)
    {
        if (!empty($post_id))
            return $this->db->query("SELECT * FROM POST_FILES WHERE POST_ID =".(int)$post_id)->result_array();
    }
    public function get_file_by_post_id($post_id)
    {
        if (!empty($post_id))
            return $this->db->query("SELECT FILE FROM POST_FILES WHERE POST_ID =".(int)$post_id)->result_array();
    }
    public function create($fields = [])
    {
        if (!empty($fields))
            $this->db->insert('POST_FILES', $fields);
		return $this->db->insert_id();
    }
    public function delete($post_id)
    {
        if (!empty($post_id))
            $this->db->delete('POST_FILES', ['POST_ID' => $post_id]);
    }
    public function delete_by_file($file)
    {
        if (!empty($file))
            $this->db->delete('POST_FILES', ['FILE' => $file]);
    }
}