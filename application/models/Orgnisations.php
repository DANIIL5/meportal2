<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orgnisations extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_organisations_id($institution_id)
    {
        if (!empty($institution_id)){
            
         return $this->db->query("
         SELECT POST_DESTINATIONS.DESTINATION_READED,POSTS.MODERATED,POSTS.ID,POSTS.MESSAGE,POSTS.THEME,POSTS.TIME 
         FROM `ORGANIZATIONS` 
         
         RIGHT OUTER JOIN `POSTS` ON (`POSTS`.`ID_COMPANY` = `ORGANIZATIONS`.`ID`)
         
          JOIN `POST_DESTINATIONS` ON (`POST_DESTINATIONS`.`ID_POST` = `POSTS`.`ID`)
                              
          WHERE USER_ID=".$institution_id." 
          AND `POSTS`.`ID_COMPANY` IS NOT NULL
          
          ORDER BY  DESTINATION_READED ASC
          ")->result_array();
         
         
         
         }   
        
    } 
    
    
    
     public function get_by_organisation_user_id($institution_id)
    {
        if (!empty($institution_id)){
            
         return $this->db->query("SELECT ID,LOGO FROM `ORGANIZATIONS`  
         
         
         
         
          WHERE USER_ID=".$institution_id." ")->result_array();
         
         
         
         }   
        
    }    
            
    
}