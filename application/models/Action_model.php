<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_random($quantity)
    {
//        return $this->db->query("SELECT * FROM ACTIONS ORDER BY RAND() LIMIT $quantity")->result_array();

        return $this->db->limit($quantity)
            ->where('MODERATED', 1)
            ->where('ACTIVE', 1)
            ->where('PUBLIC', 1)
            ->order_by('rand()')->get('ACTIONS')->result_array();
    }

    public function get_random_map($quantity, $category = null)
    {
        $this->db->where('MAP', 1)->where('ACTIVE', 1)->where('PUBLIC', 1)->where('MODERATED', 1)->limit($quantity)->order_by('rand()');

        if (!empty($category))
            $this->db->where('CATEGORY_ID', $category)
             ->or_where('CATEGORY_ID IS NULL');
        return $this->db->get('ACTIONS')->result_array();
    }

    public function get_random_list($quantity, $category = null)
    {
        $this->db->where('LIST', 1)->where('ACTIVE', 1)->where('PUBLIC', 1)->where('MODERATED', 1)->limit($quantity)->order_by('rand()');

        if (!empty($category))
            $this->db->where('CATEGORY_ID', $category)
                ->or_where('CATEGORY_ID IS NULL');

        return $this->db->get('ACTIONS')->result_array();
    }

    public function get($quantity, $order = "ID", $asc = "DESC", $list = true, $map = true)
    {

        return $this->db->limit($quantity)->order_by($order, $asc)->where('ACTIVE', 1)->where('PUBLIC',1)->where('MODERATED', 1)->get('ACTIONS')->result_array();
    }

    public function get_item($id)
    {
        return $this->db->query("SELECT * FROM ACTIONS WHERE ID = $id")->row_array();
    }

    public function count()
    {
        return $this->db->query("SELECT COUNT(0) QUANTITY FROM ACTIONS")->row_array()['QUANTITY'];
    }
}