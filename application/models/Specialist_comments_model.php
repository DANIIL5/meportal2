<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Specialist_comments_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_specialist_id($specialist_id)
    {
        if (!empty($specialist_id))
            return $this->db->query("SELECT * FROM SPECIALIST_COMMENTS WHERE SPECIALIST_ID =".(int)$specialist_id)->result_array();
    }
}