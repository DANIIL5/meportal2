<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metro_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_id_name()
    {
        return $this->db->query("SELECT ID,NAME FROM METRO")->result_array();
    }
    public function get_by_id($id)
    {
        if (!empty($id))
            return $this->db->query("SELECT * FROM METRO WHERE ID =".(int)$id)->row_array();
    }
}