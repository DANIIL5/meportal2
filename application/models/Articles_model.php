<?php

/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 23.06.2016
 * Time: 18:12
 */
class Articles_model extends CI_Model
{
    public function get_random_themes()
    {
        return $this->db->query('SELECT * FROM ARTICLES_CATEGORY ORDER BY RAND() LIMIT 3')->result_array();
    }

    public function get_articles_by_ids_array($ids)
    {
        $where = "WHERE LOADER_ID IS NULL";
        if (!empty($ids))
            $where .= " AND CATEGORY_ID IN (".implode(',', $ids).")";
        return $this->db->query("SELECT * FROM ARTICLES $where")->result_array();
    }

    public function get_articles_for($object, $id)
    {
        $object = (int)$object;
        $id = (int)$id;

        if (!empty($object) && !empty($id))
            return $this->db->query("SELECT * FROM ARTICLES WHERE OBJECT = $object AND LOADER_ID = $id")->result_array();
        else
            return [];
    }
}